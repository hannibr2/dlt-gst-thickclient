﻿Public Class ShipmentValidationRules

#Region "           Declarations            "

    Dim m_bHasBaseRec As Boolean = False

    Dim m_bSite As Boolean
    Dim m_sSiteValue As String
    Dim m_sSiteColumn As String = "SITE_CODE"

    Dim m_bCity As Boolean
    Dim m_sCityValue As String
    Dim m_sCityColumn As String = "CITY"

    Dim m_bCountry As Boolean
    Dim m_sCountryValue As String
    Dim m_sCountryColumn As String = "COUNTRY_NAME"

    Dim m_sCompanyValue As String
    Dim m_sCompanyColumn As String = "COMPANY"

    Dim m_bDestination As Boolean
    Dim m_sDestinationValue As String
    Dim m_sDestinationColumn As String = "MAIL_ADDRESS"

#End Region

#Region "           Properties          "


    Public Property HasBase() As Boolean
        Get
            Return m_bHasBaseRec
        End Get
        Set(ByVal value As Boolean)
            m_bHasBaseRec = value
        End Set
    End Property

    Public Property Site() As Boolean
        Get
            Return m_bSite
        End Get
        Set(ByVal value As Boolean)
            m_bSite = value
            If value Then
                m_sSiteValue = String.Empty
            End If
        End Set
    End Property

    Public Property SiteValue() As String
        Get
            Return m_sSiteValue
        End Get
        Set(ByVal value As String)
            m_sSiteValue = value
        End Set
    End Property

    Public ReadOnly Property SiteColumn() As String
        Get
            Return m_sSiteColumn
        End Get
    End Property


    Public Property CompanyValue() As String
        Get
            Return m_sCompanyValue
        End Get
        Set(ByVal value As String)
            m_sCompanyValue = value
        End Set
    End Property

    Public ReadOnly Property CompanyColumn() As String
        Get
            Return m_sCompanyColumn
        End Get
    End Property



    Public Property Country() As Boolean
        Get
            Return m_bCountry
        End Get
        Set(ByVal value As Boolean)
            m_bCountry = value
            If value Then
                m_sCountryValue = String.Empty
            End If
        End Set
    End Property

    Public Property CountryValue() As String
        Get
            Return m_sCountryValue
        End Get
        Set(ByVal value As String)
            m_sCountryValue = value
        End Set
    End Property

    Public ReadOnly Property CountryColumn() As String
        Get
            Return m_sCountryColumn
        End Get
    End Property

    Public Property City() As Boolean
        Get
            Return m_bCity
        End Get
        Set(ByVal value As Boolean)
            m_bCity = value
            If value Then
                m_sCityValue = String.Empty
            End If
        End Set
    End Property

    Public Property CityValue() As String
        Get
            Return m_sCityValue
        End Get
        Set(ByVal value As String)
            m_sCityValue = value
        End Set
    End Property

    Public ReadOnly Property CityColumn() As String
        Get
            Return m_sCityColumn
        End Get
    End Property





    Public Property Destination() As Boolean
        Get
            Return m_bDestination
        End Get
        Set(ByVal value As Boolean)
            m_bDestination = value
            If value Then
                m_sDestinationValue = String.Empty
            End If
        End Set
    End Property

    Public Property DestinationValue() As String
        Get
            Return m_sDestinationValue
        End Get
        Set(ByVal value As String)
            m_sDestinationValue = value
        End Set
    End Property

    Public ReadOnly Property DestinationColumn() As String
        Get
            Return m_sDestinationColumn
        End Get
    End Property


#End Region





End Class
