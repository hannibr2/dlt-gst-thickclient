﻿Imports System.Configuration
Imports ExceptionsHandlerLib
Imports System.Threading.Tasks
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Soap
Imports System.IO

'Imports System.Threading.Tasks
'Imports System.Linq


Module ChronosServiceCalls
    Dim sw As New Stopwatch()
    Dim cMessageLog As New CMessageLog()
    Public Function ChronosRemarkService(ByVal ItemBarcode As String, ByVal Location As String, ByVal UserFullName As String, ByVal RemarkText As String) As Boolean
        Dim TempRemarksSaved As Boolean = False

        Dim TempRemarkRequest As New ChronosOrderItemFeedbackService.Envelope
        Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService.Body()
        Dim TempRemarkRequestBodyItem As New ChronosOrderItemFeedbackService.Item()
        Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService.Item) = New List(Of ChronosOrderItemFeedbackService.Item)

        'Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse

        TempRemarkRequestBodyItem = ChronosRemarkServiceItem(ItemBarcode, Location, UserFullName, RemarkText)
        TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
        TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
        TempRemarkRequest.body = TempRemarkRequestBody

        Try
            TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            ' Try a 2nd attempt
            If Not TempRemarksSaved Then
                TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            End If
        Catch ex As Exception
            TempRemarksSaved = False
        End Try

        Return TempRemarksSaved
    End Function

    Public Function BulkChronosRemark(ByVal Containers As Containers, ByVal Location As String, ByVal UserFullName As String, ByVal RemarkText As String) As Boolean
        sw.Restart()
        Dim TempRemarksSaved As Boolean = False

        Dim TempRemarkRequest As New ChronosOrderItemFeedbackService.Envelope
        Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService.Body()
        Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService.Item) = New List(Of ChronosOrderItemFeedbackService.Item)
        Dim TempRemarkRequestBodyItem As ChronosOrderItemFeedbackService.Item

        'Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse


        For Each TempContainer As Container In Containers
            TempRemarkRequestBodyItem = ChronosRemarkServiceItem(TempContainer.Barcode, Location, UserFullName, RemarkText)
            TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
        Next

        TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
        TempRemarkRequest.body = TempRemarkRequestBody


        Try
            TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            ' Try a 2nd attempt
            If Not TempRemarksSaved Then
                TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            End If
        Catch ex As Exception
            TempRemarksSaved = False
        End Try
        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)
        Return TempRemarksSaved
    End Function

    Public Function BulkChronosRemark(ByVal BarcodeList As List(Of String), ByVal Location As String, ByVal UserFullName As String, ByVal RemarkText As String) As Boolean
        Dim TempRemarksSaved As Boolean = False

        Dim TempRemarkRequest As New ChronosOrderItemFeedbackService.Envelope
        Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService.Body()
        Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService.Item) = New List(Of ChronosOrderItemFeedbackService.Item)
        Dim TempRemarkRequestBodyItem As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse


        For Each TempBarcode As String In BarcodeList
            TempRemarkRequestBodyItem = ChronosRemarkServiceItem(TempBarcode, Location, UserFullName, RemarkText)
            TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
        Next

        TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
        TempRemarkRequest.body = TempRemarkRequestBody

        Try
            TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            ' Try a 2nd attempt
            If Not TempRemarksSaved Then
                TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            End If
        Catch ex As Exception
            TempRemarksSaved = False
        End Try

        Return TempRemarksSaved
    End Function
    Public Function DeSerializePackage(ByVal Request As ChronosOrderItemFeedbackService.Envelope)
        Try
            Dim envelopeItems As String = ""
            Dim firstRow As Boolean = True
            For Each item As ChronosOrderItemFeedbackService.Item In Request.body.items
                If (firstRow) Then
                    If (item.properties(1).value.Trim.Length > 0) Then
                        envelopeItems += "Package:" + item.properties(1).family + " Barcode:" + item.properties(1).value + vbCrLf
                    End If
                End If
                envelopeItems += "family:" + item.properties(0).family + "  Barcode:" + item.properties(0).value + vbCrLf
                firstRow = False
            Next
            cMessageLog.LogBenchMarks(envelopeItems + " Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)
            Return envelopeItems
        Catch ex As Exception
        End Try
    End Function
    Private Function SendChronosFeedbackAsync(ByVal RemarkRequest As ChronosOrderItemFeedbackService.Envelope) As Task(Of Boolean)

        Return Task.Factory.StartNew(Of Boolean)(
          Function()

              DeSerializePackage(RemarkRequest)

              Dim writeLockOuter As New Object
              Dim writeLockInner As New Object
              Dim TempRemarksSaved As Boolean = False
              Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse


              Using client As ChronosOrderItemFeedbackService.OrderStatusUpdaterClient = New ChronosOrderItemFeedbackService.OrderStatusUpdaterClient

                  Try
                      TempRemarkResponse = client.envelope(RemarkRequest)
                      TempRemarksSaved = True
                  Catch ex As Exception
                      SyncLock writeLockOuter
                          Try
                              SyncLock writeLockInner
                                  TempRemarkResponse = client.envelope(RemarkRequest)
                                  TempRemarksSaved = True
                              End SyncLock
                          Catch exInner As Exception
                              cMessageLog.LogBenchMarks("SendChronosFeedbackAsync FAILED!! " + System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)
                              TempRemarksSaved = False
                          End Try
                      End SyncLock
                  End Try
              End Using

              Return TempRemarksSaved

          End Function)
    End Function

    Private Function SendChronosFeedback(ByVal RemarkRequest As ChronosOrderItemFeedbackService.Envelope) As Boolean

        Dim updateTask As Task(Of Boolean) = SendChronosFeedbackAsync(RemarkRequest)
        Return True


        Dim TempRemarksSaved As Boolean = False
        Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse


        sw.Restart()

        Using client As ChronosOrderItemFeedbackService.OrderStatusUpdaterClient = New ChronosOrderItemFeedbackService.OrderStatusUpdaterClient

            Try
                TempRemarkResponse = client.envelope(RemarkRequest)
                TempRemarksSaved = True
            Catch ex As Exception
                TempRemarksSaved = False
            End Try

        End Using

        sw.Stop()
        Dim mytime As Double = sw.Elapsed.Seconds

        Return TempRemarksSaved



    End Function


    Private Function ChronosRemarkServiceItem(ByVal ItemBarcode As String, ByVal Location As String, ByVal UserFullName As String, ByVal RemarkText As String) As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkRequestBodyItem As New ChronosOrderItemFeedbackService.Item()
        Dim TempRemarkRequestBodyItemPropertyBarcode As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemPropertyRemark As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemProperties As List(Of ChronosOrderItemFeedbackService.Property) = New List(Of ChronosOrderItemFeedbackService.Property)

        Dim CurrentDateTime As DateTime = DateTime.Now
        Dim FullDateTime As String = CurrentDateTime.ToString("yyyy-MM-ddTHH:mm:ssK")

        TempRemarkRequestBodyItemPropertyBarcode.family = "delivery-container-barcode"
        TempRemarkRequestBodyItemPropertyBarcode.value = ItemBarcode
        TempRemarkRequestBodyItemPropertyBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyBarcode)

        RemarkText = String.Format("GST/{0}: {1}", UserFullName, RemarkText)

        TempRemarkRequestBodyItemPropertyRemark.family = "remark"
        TempRemarkRequestBodyItemPropertyRemark.value = String.Format("{0}. {1}. {2}", FullDateTime, Location, RemarkText)
        TempRemarkRequestBodyItemPropertyRemark.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyRemark)


        TempRemarkRequestBodyItem.properties = TempRemarkRequestBodyItemProperties.ToArray()


        Return TempRemarkRequestBodyItem
    End Function



    'Public Function ChronosServiceFinalizePackage(ByVal ItemBarcode As String, ByVal TrackingBarcode As String, ByVal PackageBarcode As String, ByVal ShipmentType As String, ByVal RemarkText As String) As Boolean


    '    Dim TempRemarkSaved As Boolean = False

    '    Dim TempRemarkRequest As New ChronosOrderItemFeedbackService_2_0.Envelope
    '    Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService_2_0.Body()
    '    Dim TempRemarkRequestBodyItem As New ChronosOrderItemFeedbackService_2_0.Item()
    '    Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService_2_0.Item) = New List(Of ChronosOrderItemFeedbackService_2_0.Item)

    '    Dim TempRemarkResponse As ChronosOrderItemFeedbackService_2_0.EnvelopeResponse

    '    TempRemarkRequestBodyItem = ChronosServiceItemExtended(ItemBarcode, TrackingBarcode, PackageBarcode, ShipmentType, RemarkText)
    '    TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
    '    TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
    '    TempRemarkRequest.body = TempRemarkRequestBody

    '    Using client As ChronosOrderItemFeedbackService_2_0.OrderStatusUpdaterService = New ChronosOrderItemFeedbackService_2_0.OrderStatusUpdaterService()
    '        TempRemarkResponse = client.envelope(TempRemarkRequest)

    '        If Not TempRemarkResponse Is Nothing AndAlso TempRemarkResponse.result = String.Empty Then
    '            TempRemarkSaved = True
    '        Else
    '            TempRemarkSaved = False
    '        End If
    '    End Using

    '    'TempRemarkSaved = True
    '    Return TempRemarkSaved

    'End Function

    Private Function ChronosFinalizedServiceItem(ByVal ItemBarcode As String, ByVal TrackingBarcode As String, ByVal PackageBarcode As String, ByVal ShipmentType As String, ByVal Location As String, ByVal RemarkText As String) As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkRequestBodyItem As New ChronosOrderItemFeedbackService.Item()
        Dim TempRemarkRequestBodyItemPropertyBarcode As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemPackageBarcode As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemShipmentType As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemShipmentTrackingURL As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemTrackingBarcode As New ChronosOrderItemFeedbackService.Property()

        Dim TempRemarkRequestBodyItemPropertyRemark As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemProperties As List(Of ChronosOrderItemFeedbackService.Property) = New List(Of ChronosOrderItemFeedbackService.Property)

        Dim CurrentDateTime As DateTime = DateTime.Now
        Dim FullDateTime As String = CurrentDateTime.ToString("yyyy-MM-ddTHH:mm:ssK")


        'If My.Settings.Environment.ToUpper = "TEST" Then
        '    TempRemarkRequestBodyItem.phase = "FULFILLED"
        'End If

        'TempRemarkRequestBodyItem.phase = "READY FOR PICKUP"

        TempRemarkRequestBodyItemPropertyBarcode.family = "delivery-container-barcode"
        TempRemarkRequestBodyItemPropertyBarcode.value = ItemBarcode
        TempRemarkRequestBodyItemPropertyBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyBarcode)

        TempRemarkRequestBodyItemTrackingBarcode.family = "shipment-name"
        TempRemarkRequestBodyItemTrackingBarcode.value = TrackingBarcode
        TempRemarkRequestBodyItemTrackingBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemTrackingBarcode)

        TempRemarkRequestBodyItemPackageBarcode.family = "package-barcode"
        TempRemarkRequestBodyItemPackageBarcode.value = PackageBarcode
        TempRemarkRequestBodyItemPackageBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPackageBarcode)

        TempRemarkRequestBodyItemShipmentType.family = "shipment-type"
        TempRemarkRequestBodyItemShipmentType.value = ShipmentType
        TempRemarkRequestBodyItemShipmentType.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemShipmentType)





        'TempRemarkRequestBodyItemPropertyRemark.family = "remark"
        'TempRemarkRequestBodyItemPropertyRemark.value = String.Format("{0}. {1}. {2}", FullDateTime, Location, RemarkText)
        'TempRemarkRequestBodyItemPropertyRemark.unit = String.Empty
        'TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyRemark)


        TempRemarkRequestBodyItem.properties = TempRemarkRequestBodyItemProperties.ToArray()


        Return TempRemarkRequestBodyItem
    End Function

    Private Function ChronosFinalizedPackageItem(ByVal ItemBarcode As String, ByVal PackageBarcode As String) As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkRequestBodyItem As New ChronosOrderItemFeedbackService.Item()
        Dim TempRemarkRequestBodyItemPropertyBarcode As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemPackageBarcode As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemShipmentType As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemShipmentTrackingURL As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemTrackingBarcode As New ChronosOrderItemFeedbackService.Property()

        Dim TempRemarkRequestBodyItemPropertyRemark As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemProperties As List(Of ChronosOrderItemFeedbackService.Property) = New List(Of ChronosOrderItemFeedbackService.Property)



        TempRemarkRequestBodyItemPropertyBarcode.family = "delivery-container-barcode"
        TempRemarkRequestBodyItemPropertyBarcode.value = ItemBarcode
        TempRemarkRequestBodyItemPropertyBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyBarcode)


        TempRemarkRequestBodyItemPackageBarcode.family = "package-barcode"
        TempRemarkRequestBodyItemPackageBarcode.value = PackageBarcode
        TempRemarkRequestBodyItemPackageBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPackageBarcode)


        TempRemarkRequestBodyItem.properties = TempRemarkRequestBodyItemProperties.ToArray()


        Return TempRemarkRequestBodyItem
    End Function


    Public Function ChronosTrackingURLTest()
        Dim TempRemarksSaved As Boolean = False

        Dim TempRemarkRequest As New ChronosOrderItemFeedbackService.Envelope
        Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService.Body()
        Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService.Item) = New List(Of ChronosOrderItemFeedbackService.Item)
        Dim TempRemarkRequestBodyItem As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse

        Dim TempBarcode As String = "14973154"
        Dim TempTrackingURL As String = "http://www.google.com"
        Dim TempExternalReference As String = "467509213980"

        TempRemarkRequestBodyItem = ChronosTrackingItem(TempBarcode, TempExternalReference, TempTrackingURL)
        TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)

        TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
        TempRemarkRequest.body = TempRemarkRequestBody

        Try
            TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            ' Try a 2nd attempt
            If Not TempRemarksSaved Then
                TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            End If
        Catch ex As Exception
            TempRemarksSaved = False
        End Try

        Return TempRemarksSaved
    End Function

    Public Function ChronosTrackingUpdate(ByVal Shipment As Shipment, ByVal ExternalReference As String, ByVal TrackingURL As String) As Boolean
        sw.Restart()
        Dim TempRemarksSaved As Boolean = False

        Dim TempRemarkRequest As New ChronosOrderItemFeedbackService.Envelope
        Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService.Body()
        Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService.Item) = New List(Of ChronosOrderItemFeedbackService.Item)
        Dim TempRemarkRequestBodyItem As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkResponse As New ChronosOrderItemFeedbackService.EnvelopeResponse


        For Each TempContainer As Container In Shipment.Containers
            TempRemarkRequestBodyItem = ChronosTrackingItem(TempContainer.Barcode, ExternalReference, TrackingURL)
            TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
        Next

        TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
        TempRemarkRequest.body = TempRemarkRequestBody

        Try
            TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            ' Try a 2nd attempt
            If Not TempRemarksSaved Then
                TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            End If
        Catch ex As Exception
            TempRemarksSaved = False
        End Try
        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)
        Return TempRemarksSaved
    End Function

    Private Function ChronosTrackingItem(ByVal ItemBarcode As String, ByVal TrackingBarcode As String, ByVal ShipmentURL As String) As ChronosOrderItemFeedbackService.Item
        Dim TempRemarkRequestBodyItem As New ChronosOrderItemFeedbackService.Item()
        Dim TempRemarkRequestBodyItemPropertyBarcode As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemShipmentTrackingURL As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemTrackingBarcode As New ChronosOrderItemFeedbackService.Property()

        Dim TempRemarkRequestBodyItemProperties As List(Of ChronosOrderItemFeedbackService.Property) = New List(Of ChronosOrderItemFeedbackService.Property)

        Dim CurrentDateTime As DateTime = DateTime.Now
        Dim FullDateTime As String = CurrentDateTime.ToString("yyyy-MM-ddTHH:mm:ssK")


        TempRemarkRequestBodyItemPropertyBarcode.family = "delivery-container-barcode"
        TempRemarkRequestBodyItemPropertyBarcode.value = ItemBarcode
        TempRemarkRequestBodyItemPropertyBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyBarcode)


        TempRemarkRequestBodyItemShipmentTrackingURL.family = "shipment-tracking"
        TempRemarkRequestBodyItemShipmentTrackingURL.value = ShipmentURL
        TempRemarkRequestBodyItemShipmentTrackingURL.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemShipmentTrackingURL)

        TempRemarkRequestBodyItemTrackingBarcode.family = "tracking-barcode"
        TempRemarkRequestBodyItemTrackingBarcode.value = TrackingBarcode
        TempRemarkRequestBodyItemTrackingBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemTrackingBarcode)

        'TempRemarkRequestBodyItemPropertyRemark.family = "remark"
        'TempRemarkRequestBodyItemPropertyRemark.value = String.Format("{0}. {1}. {2}", FullDateTime, "Cambridge United States", RemarkText)
        'TempRemarkRequestBodyItemPropertyRemark.unit = String.Empty
        'TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyRemark)


        TempRemarkRequestBodyItem.properties = TempRemarkRequestBodyItemProperties.ToArray()


        Return TempRemarkRequestBodyItem
    End Function


    Private Function PassChronosRemark() As Boolean

        Dim TempRemarksSaved As Boolean = False

        Dim TempRemarkRequest As New ChronosOrderItemFeedbackService.Envelope
        Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService.Body()
        Dim TempRemarkRequestBodyItem As New ChronosOrderItemFeedbackService.Item()
        Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService.Item) = New List(Of ChronosOrderItemFeedbackService.Item)

        Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse



        Using client As ChronosOrderItemFeedbackService.OrderStatusUpdaterClient = New ChronosOrderItemFeedbackService.OrderStatusUpdaterClient


            Try
                TempRemarkResponse = client.envelope(TempRemarkRequest)
                TempRemarksSaved = True
            Catch ex As Exception
                TempRemarksSaved = False
            End Try

        End Using

        'TempRemarkSaved = True
        Return TempRemarksSaved
    End Function

    Public Function ChronosFinalizePackage(ByVal PackageContainers As Containers, ByVal PackageBarcode As String) As Boolean
        sw.Start()
        Dim TempRemarksSaved As Boolean = False

        Dim TempRemarkRequest As New ChronosOrderItemFeedbackService.Envelope
        Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService.Body()
        Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService.Item) = New List(Of ChronosOrderItemFeedbackService.Item)
        Dim TempRemarkRequestBodyItem As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse

        For Each TempContainer As Container In PackageContainers
            TempRemarkRequestBodyItem = ChronosFinalizedPackageItem(TempContainer.Barcode, PackageBarcode)
            TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
        Next


        TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
        TempRemarkRequest.body = TempRemarkRequestBody

        Try
            TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            ' Try a 2nd attempt
            If Not TempRemarksSaved Then
                TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            End If
        Catch ex As Exception
            TempRemarksSaved = False
        End Try
        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString() + " PackageBarcode: " + PackageBarcode, ConfigurationManager.AppSettings.Get("Environment").ToUpper)

        Return TempRemarksSaved
    End Function

    Public Function ChronosRemarkShipment(ByVal Shipment As Shipment, ByVal Location As String, ByVal RemarkText As String, ByVal IsFinalized As Boolean) As Boolean
        sw.Restart()
        Dim TempRemarksSaved As Boolean = False

        Dim TempRemarkRequest As New ChronosOrderItemFeedbackService.Envelope
        Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService.Body()
        Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService.Item) = New List(Of ChronosOrderItemFeedbackService.Item)
        Dim TempRemarkRequestBodyItem As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse
        ' Removed 2015.May.27 - Trying to improve
        'For Each TempPackage As Package In Shipment.Packages
        '    Dim TempPackageName As String = TempPackage.Barcode

        '    For Each TempContainer As Container In TempPackage.Containers
        '        If IsFinalized Then
        '            TempRemarkRequestBodyItem = ChronosFinalizedServiceItem(TempContainer.Barcode, Shipment.ExternalReference, TempPackageName, "", Location, RemarkText)
        '        Else
        '            TempRemarkRequestBodyItem = ChronosFinalizedServiceItem(TempContainer.Barcode, "", "", "", Location, String.Format("{0} {1}", ShipmentInfo.SenderFullName, RemarkText))
        '        End If
        '        'TempRemarkRequestBodyItem = ChronosRemarkServiceItem(TempContainer.Barcode, Location, RemarkText)
        '        TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
        '    Next
        'Next

        For Each TempShipmentItem As ShipmentContent In Shipment.ShipmentContents
            If IsFinalized Then
                TempRemarkRequestBodyItem = ChronosFinalizedServiceItem(TempShipmentItem.ContainerBarcode, Shipment.ShipmentName, TempShipmentItem.PackageBarcode, "", Location, RemarkText)
            Else
                TempRemarkRequestBodyItem = ChronosFinalizedServiceItem(TempShipmentItem.ContainerBarcode, "", "", "", Location, String.Format("{0} {1}", ShipmentInfo.SenderFullName, RemarkText))
            End If
            TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
        Next


        TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
        TempRemarkRequest.body = TempRemarkRequestBody

        Try
            TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            ' Try a 2nd attempt
            If Not TempRemarksSaved Then
                TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            End If
        Catch ex As Exception
            TempRemarksSaved = False
        End Try
        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)

        Return TempRemarksSaved
    End Function


    Public Function ChronosReadyForPickupRemark(ByVal Containers As Containers, ByVal PackageBarcode As String) As Boolean
        sw.Start()
        Dim TempRemarksSaved As Boolean = False

        Dim TempRemarkRequest As New ChronosOrderItemFeedbackService.Envelope
        Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService.Body()
        Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService.Item) = New List(Of ChronosOrderItemFeedbackService.Item)
        Dim TempRemarkRequestBodyItem As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse


        For Each TempContainer As Container In Containers
            TempRemarkRequestBodyItem = ChronosReadyForPickupServiceItem(TempContainer.Barcode, PackageBarcode)
            TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
        Next

        TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
        TempRemarkRequest.body = TempRemarkRequestBody

        Try
            TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            ' Try a 2nd attempt
            If Not TempRemarksSaved Then
                TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            End If
        Catch ex As Exception
            TempRemarksSaved = False
        End Try
        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)
        Return TempRemarksSaved
    End Function



    Public Function ChronosArrivedRemark(ByVal Containers As Containers, ByVal Location As String, ByVal UserFullName As String, ByVal RemarkText As String, Optional ByVal UpdatePhase As Boolean = True) As Boolean
        sw.Restart()
        Dim TempRemarksSaved As Boolean = False

        Dim TempRemarkRequest As New ChronosOrderItemFeedbackService.Envelope
        Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService.Body()
        Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService.Item) = New List(Of ChronosOrderItemFeedbackService.Item)
        Dim TempRemarkRequestBodyItem As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkResponse As ChronosOrderItemFeedbackService.EnvelopeResponse


        For Each TempContainer As Container In Containers
            TempRemarkRequestBodyItem = ChronosArrivedServiceItem(TempContainer.Barcode, Location, UserFullName, RemarkText, UpdatePhase)
            TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
        Next

        TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
        TempRemarkRequest.body = TempRemarkRequestBody

        Try
            TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            ' Try a 2nd attempt
            If Not TempRemarksSaved Then
                TempRemarksSaved = SendChronosFeedback(TempRemarkRequest)
            End If
        Catch ex As Exception
            TempRemarksSaved = False
        End Try
        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)
        Return TempRemarksSaved
    End Function



    Private Function ChronosReadyForPickupServiceItem(ByVal ItemBarcode As String, ByVal PackageBarcode As String) As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkRequestBodyItem As New ChronosOrderItemFeedbackService.Item()
        Dim TempRemarkRequestBodyItemPropertyBarcode As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemPackageBarcode As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemProperties As List(Of ChronosOrderItemFeedbackService.Property) = New List(Of ChronosOrderItemFeedbackService.Property)

        Dim CurrentDateTime As DateTime = DateTime.Now
        Dim FullDateTime As String = CurrentDateTime.ToString("yyyy-MM-ddTHH:mm:ssK")

        TempRemarkRequestBodyItem.phase = "READY FOR PICKUP"

        TempRemarkRequestBodyItemPropertyBarcode.family = "delivery-container-barcode"
        TempRemarkRequestBodyItemPropertyBarcode.value = ItemBarcode
        TempRemarkRequestBodyItemPropertyBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyBarcode)

        TempRemarkRequestBodyItemPackageBarcode.family = "package-barcode"
        TempRemarkRequestBodyItemPackageBarcode.value = PackageBarcode
        TempRemarkRequestBodyItemPackageBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPackageBarcode)

        'RemarkText = String.Format("GST/{0}: {1}", ShipmentInfo.FullName, RemarkText)

        'TempRemarkRequestBodyItemPropertyRemark.family = "remark"
        'TempRemarkRequestBodyItemPropertyRemark.value = String.Format("{0}. {1}. {2}", FullDateTime, Location, RemarkText)
        'TempRemarkRequestBodyItemPropertyRemark.unit = String.Empty
        'TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyRemark)


        TempRemarkRequestBodyItem.properties = TempRemarkRequestBodyItemProperties.ToArray()


        Return TempRemarkRequestBodyItem
    End Function


    Private Function ChronosArrivedServiceItem(ByVal ItemBarcode As String, ByVal Location As String, ByVal UserFullName As String, ByVal RemarkText As String, Optional ByVal UpdatePhase As Boolean = True) As ChronosOrderItemFeedbackService.Item

        Dim TempRemarkRequestBodyItem As New ChronosOrderItemFeedbackService.Item()
        Dim TempRemarkRequestBodyItemPropertyBarcode As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemPropertyRemark As New ChronosOrderItemFeedbackService.Property()
        Dim TempRemarkRequestBodyItemProperties As List(Of ChronosOrderItemFeedbackService.Property) = New List(Of ChronosOrderItemFeedbackService.Property)

        Dim CurrentDateTime As DateTime = DateTime.Now
        Dim FullDateTime As String = CurrentDateTime.ToString("yyyy-MM-ddTHH:mm:ssK")

        If UpdatePhase Then
            TempRemarkRequestBodyItem.phase = "ARRIVED"
        End If

        TempRemarkRequestBodyItemPropertyBarcode.family = "delivery-container-barcode"
        TempRemarkRequestBodyItemPropertyBarcode.value = ItemBarcode
        TempRemarkRequestBodyItemPropertyBarcode.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyBarcode)

        RemarkText = String.Format("GST/{0}: {1}", UserFullName, RemarkText)

        TempRemarkRequestBodyItemPropertyRemark.family = "remark"
        TempRemarkRequestBodyItemPropertyRemark.value = String.Format("{0}. {1}. {2}", FullDateTime, Location, RemarkText)
        TempRemarkRequestBodyItemPropertyRemark.unit = String.Empty
        TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyRemark)


        TempRemarkRequestBodyItem.properties = TempRemarkRequestBodyItemProperties.ToArray()


        Return TempRemarkRequestBodyItem
    End Function



    'Private Function ChronosArrivedServiceItemOld(ByVal ItemBarcode As String, ByVal Location As String, ByVal UserFullName As String, ByVal RemarkText As String, Optional ByVal UpdatePhase As Boolean = True) As ChronosOrderItemFeedbackService_2_0.Item

    '    Dim TempRemarkRequestBodyItem As New ChronosOrderItemFeedbackService_2_0.Item()
    '    Dim TempRemarkRequestBodyItemPropertyBarcode As New ChronosOrderItemFeedbackService_2_0.Property()
    '    Dim TempRemarkRequestBodyItemPropertyRemark As New ChronosOrderItemFeedbackService_2_0.Property()
    '    Dim TempRemarkRequestBodyItemProperties As List(Of ChronosOrderItemFeedbackService_2_0.Property) = New List(Of ChronosOrderItemFeedbackService_2_0.Property)

    '    Dim CurrentDateTime As DateTime = DateTime.Now
    '    Dim FullDateTime As String = CurrentDateTime.ToString("yyyy-MM-ddTHH:mm:ssK")

    '    If UpdatePhase Then
    '        TempRemarkRequestBodyItem.phase = "ARRIVED"
    '    End If

    '    TempRemarkRequestBodyItemPropertyBarcode.family = "delivery-container-barcode"
    '    TempRemarkRequestBodyItemPropertyBarcode.value = ItemBarcode
    '    TempRemarkRequestBodyItemPropertyBarcode.unit = String.Empty
    '    TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyBarcode)

    '    RemarkText = String.Format("GST/{0}: {1}", UserFullName, RemarkText)

    '    TempRemarkRequestBodyItemPropertyRemark.family = "remark"
    '    TempRemarkRequestBodyItemPropertyRemark.value = String.Format("{0}. {1}. {2}", FullDateTime, Location, RemarkText)
    '    TempRemarkRequestBodyItemPropertyRemark.unit = String.Empty
    '    TempRemarkRequestBodyItemProperties.Add(TempRemarkRequestBodyItemPropertyRemark)


    '    TempRemarkRequestBodyItem.properties = TempRemarkRequestBodyItemProperties.ToArray()


    '    Return TempRemarkRequestBodyItem
    'End Function

    'Public Function ChronosArrivedRemarkOld(ByVal Containers As Containers, ByVal Location As String, ByVal UserFullName As String, ByVal RemarkText As String, Optional ByVal UpdatePhase As Boolean = True) As Boolean
    '    Dim TempRemarksSaved As Boolean = False

    '    Dim TempRemarkRequest As New ChronosOrderItemFeedbackService_2_0.Envelope
    '    Dim TempRemarkRequestBody As New ChronosOrderItemFeedbackService_2_0.Body()
    '    Dim TempRemarkRequestBodyItems As List(Of ChronosOrderItemFeedbackService_2_0.Item) = New List(Of ChronosOrderItemFeedbackService_2_0.Item)
    '    Dim TempRemarkRequestBodyItem As ChronosOrderItemFeedbackService_2_0.Item

    '    Dim TempRemarkResponse As ChronosOrderItemFeedbackService_2_0.EnvelopeResponse


    '    For Each TempContainer As Container In Containers
    '        TempRemarkRequestBodyItem = ChronosArrivedServiceItem(TempContainer.Barcode, Location, UserFullName, RemarkText, UpdatePhase)
    '        TempRemarkRequestBodyItems.Add(TempRemarkRequestBodyItem)
    '    Next

    '    TempRemarkRequestBody.items = TempRemarkRequestBodyItems.ToArray()
    '    TempRemarkRequest.body = TempRemarkRequestBody

    '    Using client As ChronosOrderItemFeedbackService_2_0.OrderStatusUpdaterService = New ChronosOrderItemFeedbackService_2_0.OrderStatusUpdaterService()
    '        TempRemarkResponse = client.envelope(TempRemarkRequest)

    '        If Not TempRemarkResponse Is Nothing AndAlso TempRemarkResponse.result = String.Empty Then
    '            TempRemarksSaved = True
    '        Else
    '            TempRemarksSaved = False
    '        End If
    '    End Using

    '    Return TempRemarksSaved
    'End Function

End Module
