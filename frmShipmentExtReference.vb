Public Class frmShipmentExtReference

    Private m_sShipmentName As String
    Private m_sShipmentExternalReference As String
    Private m_bCancel As Boolean = False
    Private m_bOk As Boolean = False

    Public ReadOnly Property Cancel() As Boolean
        Get
            Return m_bCancel
        End Get
    End Property

    Public ReadOnly Property Ok() As Boolean
        Get
            Return m_bOk
        End Get
    End Property

    Public ReadOnly Property ShipmentExternalReference As String
        Get
            Return m_sShipmentExternalReference
        End Get
    End Property

    Public Sub New(ByVal ShipmentName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_sShipmentName = ShipmentName
    End Sub



    Public Function GetShipmentExternalReference() As String
        Dim ExternalReference As String = String.Empty

        If Not String.IsNullOrEmpty(txtExternalReference.Text) Then
            ExternalReference = txtExternalReference.Text
        End If

        Return ExternalReference
    End Function

    Private Sub txtExternalReference_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtExternalReference.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnOK.PerformClick()
        End If
    End Sub

    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        Me.m_bCancel = False
        Me.m_bOk = True
        m_sShipmentExternalReference = GetShipmentExternalReference()
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click

        Me.m_bCancel = True
        Me.m_bOk = False
        m_sShipmentExternalReference = String.Empty
        Me.Close()
    End Sub

    Private Sub btnSetShipmentName_Click(sender As System.Object, e As System.EventArgs) Handles btnSetShipmentName.Click
        Me.m_bCancel = False
        Me.m_bOk = True
        m_sShipmentExternalReference = m_sShipmentName
        Me.Close()
    End Sub

End Class