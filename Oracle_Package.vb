﻿Imports Oracle.ManagedDataAccess.Client

Public Class Oracle_Package
    Inherits Oracle_General

#Region "           Packages            "

    ''' <summary>
    ''' Show pending packages to be shipped
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowFinalizedPackages() 'ByVal ValidationRules As ShipmentValidationRules
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select parent_container_id, barcode, origin_id, phase_id, container_count, date_modified, " & _
                    "first_name, last_name, company, country_name, mail_address, site_code, city, leaving_date, date_created " & _
                    "from v_package_finalized  "

            If frmMain.lkFinPkgTimeline.EditValue > 0 Then
                query += " where date_created > sysdate - " & frmMain.lkFinPkgTimeline.EditValue
            End If

            m_DataSet = ExecuteQuery(query)
            frmMain.grdFinalizedPackages.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting finalized packages")
        End Try

    End Sub

    ''' <summary>
    ''' Show pending packages to be shipped
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowPackageInFinalizedPackageGrid(ByVal PackageBarcode As String) 'ByVal ValidationRules As ShipmentValidationRules
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select parent_container_id, barcode, origin_id, phase_id, container_count, date_modified, " & _
                    "first_name, last_name, company, country_name, mail_address, site_code, city, leaving_date, date_created " & _
                    "from v_package  " & _
                    "where barcode = '" & PackageBarcode & "'"

            m_DataSet = ExecuteQuery(query)
            frmMain.grdFinalizedPackages.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting finalized packages")
        End Try

    End Sub

    ''' <summary>
    ''' Show pending packages to be shipped
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowPendingPackages(ByVal ValidationRules As ShipmentValidationRules, ByVal UseRules As Boolean) 'ByVal ValidationRules As ShipmentValidationRules
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select parent_container_id, barcode, origin_id, phase_id, container_count, date_modified, " & _
                    "receiver_id, first_name, last_name, company, country_name, mail_address, site_code, city, leaving_date " & _
                    "from v_package_finalized  "

            If UseRules Then

                query += " where 1 = 1"

                If ValidationRules.Site Then

                    If Not String.IsNullOrEmpty(ValidationRules.CompanyValue) Then
                        query += String.Format(" and upper({0}) = '{1}' ", ValidationRules.CompanyColumn, ValidationRules.CompanyValue.ToUpper)
                    End If

                    If Not String.IsNullOrEmpty(ValidationRules.CountryValue) Then
                        query += String.Format(" and upper( {0} ) = '{1}' ", ValidationRules.CountryColumn, ValidationRules.CountryValue.ToUpper)
                    End If

                    If Not String.IsNullOrEmpty(ValidationRules.CityValue) Then
                        query += String.Format(" and upper( {0} ) like '{1}%' ", ValidationRules.CityColumn, ValidationRules.CityValue.ToUpper)
                    End If

                End If

                If ValidationRules.Destination Then

                    If Not String.IsNullOrEmpty(ValidationRules.DestinationValue) Then
                        query += String.Format(" and upper({0}) = '{1}' ", ValidationRules.DestinationColumn, ValidationRules.DestinationValue.ToUpper)
                    End If

                End If

            End If


            m_DataSet = ExecuteQuery(query)
            frmMain.grdPendingPackages.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting pending packages")
        End Try

    End Sub

    ''' <summary>
    ''' Show pending packages to be shipped
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowScannedPendingPackages(ByVal PackageBarcode As String) 'ByVal ValidationRules As ShipmentValidationRules
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select parent_container_id, barcode, origin_id, phase_id, container_count, date_modified, " & _
                    "receiver_id, first_name, last_name, company, country_name, mail_address, site_code, city, leaving_date " & _
                    "from v_package_finalized  " & _
                    "where barcode = '" & PackageBarcode.ToString & "'"


            m_DataSet = ExecuteQuery(query)
            frmMain.grdPendingPackages.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting pending packages")
        End Try

    End Sub

    Public Function AddPackages(ByVal ParentID As Long, ByVal TypeID As Long, ByVal Origin As String, ByVal Ranking As Integer, ByVal PhaseID As Long, ByVal Note As String, ByVal ReceiverID As Long, ByVal Workstation As String) As Long
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        Dim TempContainerID As Int64 = -1

        Try

            m_Trans = DBConnection.BeginTransaction()

            m_command = New OracleCommand("p_container.addpackage", DBConnection)
            m_command.CommandType = CommandType.StoredProcedure
            m_command.Transaction = m_Trans
            If ParentID = 0 Then
                m_command.Parameters.Add("i_parent_container_id", OracleDbType.Long).Value = DBNull.Value
            Else
                m_command.Parameters.Add("i_parent_container_id", OracleDbType.Long).Value = ParentID
            End If


            m_command.Parameters.Add("i_type_id", OracleDbType.Int64).Value = TypeID 'Package Container
            m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = Origin
            m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = DBNull.Value '""
            m_command.Parameters.Add("i_ranking", OracleDbType.Int32).Value = Ranking
            m_command.Parameters.Add("i_phase_id", OracleDbType.Int32).Value = PhaseID 'Package Created
            m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Note
            m_command.Parameters.Add("i_receiver_id", OracleDbType.Int64).Value = ReceiverID
            m_command.Parameters.Add("i_workstation", OracleDbType.Varchar2).Value = Workstation

            m_command.Parameters.Add("o_container_id", OracleDbType.Int64).Direction = ParameterDirection.Output
            m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
            m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output

            m_command.ExecuteNonQuery()

            Console.WriteLine(m_command.Parameters("o_container_id").Value)
            Console.WriteLine(m_command.Parameters("o_error_code").Value)
            Console.WriteLine(m_command.Parameters("o_error_msg").Value)

            If m_command.Parameters("o_error_code").Value = 0 Then

                m_Trans.Commit()
                TempContainerID = Convert.ToInt64(m_command.Parameters("o_container_id").Value.ToString())
            Else
                m_Trans.Rollback()
                'Throw New Exception("Error adding package - " + m_command.Parameters("o_error_msg").Value)
                Throw New Exception("Error adding package - " + m_command.Parameters("o_error_msg").Value + vbCrLf & _
                    "ParentID = " + ParentID.ToString("F0") & _
                    ", TypeID = " + TypeID.ToString() & _
                    ", Origin = " + Origin.ToString() & _
                    ", Ranking = " + Ranking.ToString() & _
                    ", PhaseID = " + PhaseID.ToString() & _
                    ", Note = " + Note.ToString() & _
                    " ReceiverID = " + ReceiverID.ToString("F0"))
            End If
        Catch ex As Exception
            m_Trans.Rollback()
            Throw ex
        Finally
            m_command = Nothing
        End Try


        Return TempContainerID
    End Function


    Public Function AddPackageToShipment(ByVal ShipmentID As Decimal, ByVal ParentID As Decimal) As Boolean
        Dim m_IsAdded As Boolean = False
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_shipment.attachcontainer", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans

        m_command.Parameters.Add("i_shipment_id", OracleDbType.Decimal).Value = ShipmentID 'Package Container
        m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ParentID
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
            m_IsAdded = True
        Else
            m_Trans.Rollback()
            m_IsAdded = False
            'Throw New Exception("Error adding package - " + m_command.Parameters("o_error_msg").Value)
            Throw New Exception("Error adding package to shipment - " + m_command.Parameters("o_error_msg").Value.ToString + vbCrLf & _
                "ShipmentID = " + ShipmentID.ToString("F0") + ", ParentID = " + ParentID.ToString("F0"))
        End If
        Return m_IsAdded
    End Function

    Public Sub RemovePackageFromShipment(ByVal ShipmentID As Decimal, ByVal ParentID As Decimal)
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_shipment.detachcontainer", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans

        m_command.Parameters.Add("i_shipment_id", OracleDbType.Decimal).Value = ShipmentID 'Package Container
        m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ParentID
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            'Throw New Exception("Error adding package - " + m_command.Parameters("o_error_msg").Value)
            Throw New Exception("Error adding package to shipment - " + m_command.Parameters("o_error_msg").Value.ToString + vbCrLf & _
                "ShipmentID = " + ShipmentID.ToString("F0") + ", ParentID = " + ParentID.ToString("F0"))
        End If

    End Sub

    ''' <summary>
    ''' Show details of existing open packages
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowExistingPackages(ByVal Workstation As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            'query = "select parent_container_id, barcode, origin_id, phase_id, container_count, date_modified from v_package"


            query = "select parent_container_id, barcode, origin_id, phase_id, container_count, date_modified, " & _
                    "first_name, last_name, company, country_name, mail_address, site_code, city, leaving_date " & _
                    "from v_package_created " & _
                    "where upper(workstation) = '" & Workstation.ToUpper & "'"

            m_DataSet = ExecuteQuery(query)
            frmMain.grdExistingPackages.DataSource = m_DataSet.Tables(0)
        Catch ex As Exception
            Throw New Exception("Error getting existing packages")
        End Try
    End Sub

    Public Function GetNumberOfCreatedPhasePackages(ByVal Workstation As String) As Integer
        Dim query As String
        Dim m_DataSet As DataSet

        query = "select count(container_id) from v_package_created where upper(workstation) = '" & Workstation.ToUpper() & "'"

        m_DataSet = ExecuteQuery(query)

        'Return the number of unique destinations in the view
        Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
    End Function


    Public Sub UpdatePackage(ByVal ContainerID As Decimal, ByVal TypeID As Integer, ByVal Origin As String, ByVal Barcode As String, ByVal Ranking As Integer, ByVal PhaseID As Integer, ByVal Note As String, ByVal ReceiverID As Decimal)
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        Dim tempcontainerid As Object
        Dim tempparentid As Object
        Dim temptypeid As Object
        Dim temporigin As Object
        Dim tempranking As Object
        Dim tempphaseid As Object
        Dim tempnote As Object
        Dim tempreceiverid As Object

        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_container.updatecontainer", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans

        If ContainerID <> 0 Then
            m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = ContainerID.ToString("F0")
            tempcontainerid = ContainerID.ToString("F0")
        Else
            m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = DBNull.Value
            tempcontainerid = DBNull.Value
        End If

        'm_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ParentID.ToString("F0")
        'm_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = DBNull.Value
        'tempparentid = DBNull.Value
        m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ContainerID.ToString("F0")
        tempparentid = ContainerID.ToString("F0")

        If TypeID <> 0 Then
            m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = TypeID
            temptypeid = TypeID.ToString()
        Else
            m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = DBNull.Value
            temptypeid = DBNull.Value
        End If
        If String.IsNullOrEmpty(Origin) Then
            m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = Origin
            temporigin = Origin
        Else
            m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = DBNull.Value
            temporigin = DBNull.Value
        End If

        'm_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = Barcode
        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = DBNull.Value
        'If Ranking <> 0 Then
        '    m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = Ranking
        '    tempranking = Ranking.ToString()
        'Else
        m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = DBNull.Value
        tempranking = DBNull.Value
        'End If

        If PhaseID <> 0 Then
            m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = PhaseID
            tempphaseid = PhaseID.ToString()
        Else
            m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = DBNull.Value
            tempphaseid = DBNull.Value
        End If

        If String.IsNullOrEmpty(Note) Then
            m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Note
            tempnote = Note
        Else
            m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = DBNull.Value
            tempnote = DBNull.Value
        End If

        If ReceiverID <> 0 Then
            m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = ReceiverID.ToString("F0")
            tempreceiverid = ReceiverID.ToString("F0")
        Else
            m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = DBNull.Value
            tempreceiverid = DBNull.Value
        End If

        'm_command.Parameters.Add("o_container_id", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        'Console.WriteLine(m_command.Parameters("o_container_id").Value)
        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            'ShowMessage("Error updating container - " + m_command.Parameters("o_error_msg").Value)
            Throw New Exception("Error updating package - " + m_command.Parameters("o_error_msg").Value + vbCrLf & _
                "Container ID = " + tempcontainerid.ToString("F0") & _
                ", Parent Container ID = " + tempparentid.ToString("F0") & _
                ", Type ID = " + temptypeid.ToString() & _
                ", Origin = " + temporigin.ToString() & _
                ", Barcode = " & _
                ", Ranking = " + tempranking.ToString() & _
                ", Phase ID = " + tempphaseid.ToString() & _
                ", Note = " + tempnote.ToString() & _
                ", Receiver ID = " + tempreceiverid.ToString()) '+ Barcode & _
        End If

    End Sub

    Public Sub UpdateFinalPackageDetails(ByVal ParentContainerID As Decimal)

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select vc.container_id, vc.barcode, vc.first_name, vc.last_name, vc.company, vc.mail_address, vc.city, vc.country_name, vc.site_code, vc.account, vc.email, vc.order_name, vc.delivery_form_type_name, vc.protocol_name, vc.destination_rack_group_name " & _
                    "from v_container_details vc where parent_container_id = " + ParentContainerID.ToString("F0") + " " & _
                    "and parent_container_id != container_id order by ranking"

            m_DataSet = ExecuteQuery(query)
            frmMain.grdFinalPackageDetails.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error updating package details")
        End Try

    End Sub

    Public Sub UpdateActivePackageDetails(ByVal ParentContainerID As Decimal)

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select vc.container_id, vc.barcode, vc.first_name, vc.last_name, vc.company, vc.mail_address, vc.city, vc.country_name, vc.site_code, vc.account, vc.email, vc.order_name, vc.delivery_form_type_name, vc.protocol_name, vc.destination_rack_group_name " & _
                    "from v_container_details vc where parent_container_id = " + ParentContainerID.ToString("F0") + " " & _
                    "and parent_container_id != container_id order by ranking"

            m_DataSet = ExecuteQuery(query)
            frmMain.grdActivePackageDetails.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error updating package details")
        End Try

    End Sub


    ''' <summary>
    ''' Update the phase of package to 50101 (Package created)
    ''' </summary>
    ''' <param name="PackageID"></param>
    ''' <remarks></remarks>
    Public Sub UnfinalizePackage(ByVal PackageID As Decimal, ByVal Workstation As String)

        'UpdatePackage(PackageID, 0, String.Empty, String.Empty, 0, 50101, String.Empty, 0)

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction


        Try

            m_Trans = DBConnection.BeginTransaction()

            m_command = New OracleCommand("p_container.unfinalizepackage", DBConnection)
            m_command.CommandType = CommandType.StoredProcedure
            m_command.Transaction = m_Trans

            m_command.Parameters.Add("i_container_id", OracleDbType.Long).Value = PackageID
            m_command.Parameters.Add("i_workstation", OracleDbType.Varchar2).Value = Workstation


            m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
            m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output

            m_command.ExecuteNonQuery()

            Console.WriteLine(m_command.Parameters("o_error_code").Value)
            Console.WriteLine(m_command.Parameters("o_error_msg").Value)

            If m_command.Parameters("o_error_code").Value = 0 Then

                m_Trans.Commit()
            Else
                m_Trans.Rollback()
                'Throw New Exception("Error adding package - " + m_command.Parameters("o_error_msg").Value)
                Throw New Exception("Error unfinalizing package - " + m_command.Parameters("o_error_msg").Value + vbCrLf)
            End If
        Catch ex As Exception
            m_Trans.Rollback()
            Throw ex
        Finally
            m_command = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Update the phase of package to 50102 (Package finalized)
    ''' </summary>
    ''' <param name="PackageID"></param>
    ''' <remarks></remarks>
    Public Sub FinalizePackage(ByVal PackageID As Decimal)

        '   Need to update the db package so all children containers are updated
        'UpdatePackage(PackageID, 0, String.Empty, String.Empty, 0, 50102, String.Empty, 0)

        'UpdatePackage(PackageID, 0, String.Empty, String.Empty, 0, 50102, String.Empty, 0)

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction


        Try

            m_Trans = DBConnection.BeginTransaction()

            m_command = New OracleCommand("p_container.finalizepackage", DBConnection)
            m_command.CommandType = CommandType.StoredProcedure
            m_command.Transaction = m_Trans

            m_command.Parameters.Add("i_container_id", OracleDbType.Long).Value = PackageID


            m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
            m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output

            m_command.ExecuteNonQuery()

            Console.WriteLine(m_command.Parameters("o_error_code").Value)
            Console.WriteLine(m_command.Parameters("o_error_msg").Value)

            If m_command.Parameters("o_error_code").Value = 0 Then

                m_Trans.Commit()
            Else
                m_Trans.Rollback()
                'Throw New Exception("Error adding package - " + m_command.Parameters("o_error_msg").Value)
                Throw New Exception("Error finalizing package - " + m_command.Parameters("o_error_msg").Value + vbCrLf)
            End If
        Catch ex As Exception
            m_Trans.Rollback()
            Throw ex
        Finally
            m_command = Nothing
        End Try

    End Sub

    ''' <summary>
    ''' Update the phase of package to 50107 (Package arrived)
    ''' </summary>
    ''' <param name="PackageID"></param>
    ''' <remarks></remarks>
    Public Sub ArrivePackage(ByVal PackageID As Decimal)

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction = DBConnection.BeginTransaction()
        Try

            m_command = New OracleCommand("p_container.arrivepackage", DBConnection)
            m_command.CommandType = CommandType.StoredProcedure
            m_command.Transaction = m_Trans

            m_command.Parameters.Add("i_container_id", OracleDbType.Long).Value = PackageID


            m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
            m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output

            m_command.ExecuteNonQuery()

            Console.WriteLine(m_command.Parameters("o_error_code").Value)
            Console.WriteLine(m_command.Parameters("o_error_msg").Value)

            If m_command.Parameters("o_error_code").Value = 0 Then

                m_Trans.Commit()
            Else
                m_Trans.Rollback()
                Throw New Exception("Error arriving package - " + m_command.Parameters("o_error_msg").Value + vbCrLf)
            End If
        Catch ex As Exception
            m_Trans.Rollback()
            Throw ex
        Finally
            m_command = Nothing
        End Try

    End Sub

    ''' <summary>
    ''' Update the phase of package to 50109 (Package delivered)
    ''' </summary>
    ''' <param name="PackageID"></param>
    ''' <remarks></remarks>
    Public Sub DeliverPackage(ByVal PackageID As Decimal)

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction = DBConnection.BeginTransaction()
        Try

            m_command = New OracleCommand("p_container.deliverpackage", DBConnection)
            m_command.CommandType = CommandType.StoredProcedure
            m_command.Transaction = m_Trans

            m_command.Parameters.Add("i_container_id", OracleDbType.Long).Value = PackageID


            m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
            m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output

            m_command.ExecuteNonQuery()

            Console.WriteLine(m_command.Parameters("o_error_code").Value)
            Console.WriteLine(m_command.Parameters("o_error_msg").Value)

            If m_command.Parameters("o_error_code").Value = 0 Then

                m_Trans.Commit()
            Else
                m_Trans.Rollback()
                Throw New Exception("Error delivering package - " + m_command.Parameters("o_error_msg").Value + vbCrLf)
            End If
        Catch ex As Exception
            m_Trans.Rollback()
            Throw ex
        Finally
            m_command = Nothing
        End Try

    End Sub

    ''' <summary>
    ''' Update the pacakge to make containers ready to repack
    ''' </summary>
    ''' <param name="PackageID"></param>
    ''' <remarks></remarks>
    Public Sub RepackPackage(ByVal PackageID As Decimal)

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction = DBConnection.BeginTransaction()
        Try

            m_command = New OracleCommand("p_container.repackpackage", DBConnection)
            m_command.CommandType = CommandType.StoredProcedure
            m_command.Transaction = m_Trans

            m_command.Parameters.Add("i_container_id", OracleDbType.Long).Value = PackageID


            m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
            m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output

            m_command.ExecuteNonQuery()

            Console.WriteLine(m_command.Parameters("o_error_code").Value)
            Console.WriteLine(m_command.Parameters("o_error_msg").Value)

            If m_command.Parameters("o_error_code").Value = 0 Then

                m_Trans.Commit()
            Else
                m_Trans.Rollback()
                Throw New Exception("Error repackaging package - " + m_command.Parameters("o_error_msg").Value + vbCrLf)
            End If
        Catch ex As Exception
            m_Trans.Rollback()
            Throw ex
        Finally
            m_command = Nothing
        End Try

    End Sub


    Public Function GetPackageValidationRules(ByVal ParentContainerID As Decimal) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = " select rule_id " & _
                    " from t_container_rule " & _
                    " where container_id = " & ParentContainerID
            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)
        Catch ex As Exception
            Console.WriteLine(Err.ToString())
            Throw New Exception("Error getting the package validation rules")
        End Try
    End Function

    Public Sub SetPackageValidationRules(ByVal PackageID As Decimal, ByVal RulesIndex As List(Of Integer))
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            For Each Index As Integer In RulesIndex
                query = "insert into t_container_rule (rule_id, container_id) values (" & Index & ", " & PackageID & ")"
                m_DataSet = ExecuteQuery(query)
            Next
        Catch err As Exception
            Console.WriteLine(err.ToString())
            Throw New Exception("Error setting package validation rules")
        End Try
    End Sub

    Public Sub RemovePackage(ByVal PackageID As Decimal)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "delete from t_container where container_id = " & PackageID
            m_DataSet = ExecuteQuery(query)
        Catch err As Exception
            Console.WriteLine(err.ToString())
            Throw New Exception("Error finalizing the package")
        End Try
    End Sub

    Public Function GetPackagesByShipmentID(ByVal ShipmentID As Decimal) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            'query = "select vsp.parent_container_id, vsp.barcode, vpe.first_name, vpe.last_name, vpe.company, vpe.country_name, vpe.mail_address " & _
            '        "from v_shipment vs, v_shipment_package vsp, v_person vpe where vs.receiver_id = VPE.PERSON_ID " & _
            '        "and vs.shipment_name = vsp.shipment_name and vsp.shipment_name = '" + ShipmentBarcode + "'"

            query = "select parent_container_id, barcode, first_name, last_name, company, country_name, mail_address, site_code, package_phase_id as phase_id " & _
                "from v_shipment_package_details where shipment_id = " + ShipmentID.ToString("F0")

            'query = "select vsp.parent_container_id, vsp.barcode, vpe.first_name, vpe.last_name, vpe.company, vpe.country_name, vpe.mail_address " & _
            '        "from v_shipment vs, v_package vp, v_shipment_package vsp, v_person vpe where vp.receiver_id = VPE.PERSON_ID " & _
            '        "and vp.parent_container_id = vsp.parent_container_id and vs.shipment_name = vsp.shipment_name and vsp.shipment_name = '" + ShipmentBarcode + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)

        Catch ex As Exception
            Throw New Exception("Error getting packages by Shipment ID")
        End Try
    End Function

    Public Function GetPackagesByParentContainerID(ByVal ParentContainerID As Decimal) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select parent_container_id, barcode, first_name, last_name, company, country_name, mail_address, site_code " & _
                "from v_shipment_package_details where parent_container_id = " + ParentContainerID.ToString("F0")

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)

        Catch ex As Exception
            Throw New Exception("Error getting Pacakges by parent container id")
        End Try
    End Function

    Public Function GetPackageByBarcode(ByVal Barcode As String) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select parent_container_id, barcode, first_name, last_name, company, country_name, mail_address, site_code, package_phase_id as phase_id " & _
                "from v_shipment_package_details where barcode = '" & Barcode & "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)

        Catch ex As Exception
            Throw New Exception("Error getting Pacakges by parent container id")
        End Try
    End Function

    Public Function GetBinEligiblePackageByBarcode(ByVal PackageBarcode As String) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select * " & _
                    "from v_package_binnable " & _
                    "where upper( barcode )= '" & PackageBarcode.ToUpper & "'"

            m_DataSet = ExecuteQuery(query)

            'If m_DataSet.Tables.Count > 0 And m_DataSet.Tables(0).Rows.Count > 0 Then
            Return m_DataSet.Tables(0)
            'Else
            'Return Nothing
            'End If

        Catch ex As Exception
            Throw New Exception("Error getting package by barcode")
        End Try
    End Function

    Public Function GetPackageItemCount(ByVal PackageID As Decimal) As Integer

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select container_count from v_package where container_id = " & PackageID

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting package item count")
            Return -1
        End Try

    End Function



#End Region

    Public Sub New()
        MyBase.New()
    End Sub
End Class
