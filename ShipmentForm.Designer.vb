<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ShipmentForm
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.grpSenderInfo = New DevExpress.XtraEditors.GroupControl()
        Me.lblShipFormCostCenter = New DevExpress.XtraEditors.LabelControl()
        Me.txtCostCenter = New DevExpress.XtraEditors.TextEdit()
        Me.txtSenderZip = New DevExpress.XtraEditors.TextEdit()
        Me.lblShipFormSenderZip = New DevExpress.XtraEditors.LabelControl()
        Me.txtSenderAddress = New DevExpress.XtraEditors.TextEdit()
        Me.txtSenderDetailsDept = New DevExpress.XtraEditors.TextEdit()
        Me.txtSenderDetailsCompany = New DevExpress.XtraEditors.TextEdit()
        Me.txtSenderDetailsLast = New DevExpress.XtraEditors.TextEdit()
        Me.txtSenderDetailsFirst = New DevExpress.XtraEditors.TextEdit()
        Me.txtSenderCountry = New DevExpress.XtraEditors.TextEdit()
        Me.txtSenderState = New DevExpress.XtraEditors.TextEdit()
        Me.txtSenderCity = New DevExpress.XtraEditors.TextEdit()
        Me.txtSenderEmail = New DevExpress.XtraEditors.TextEdit()
        Me.txtSenderPhone = New DevExpress.XtraEditors.TextEdit()
        Me.lblShipFormSenderEmail = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormSenderLast = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormSenderCompany = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormSenderDept = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormSenderAddress = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormSenderCity = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormSenderState = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormSenderCountry = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormSenderPhone = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormSenderFirst = New DevExpress.XtraEditors.LabelControl()
        Me.grpRecipientInfo = New DevExpress.XtraEditors.GroupControl()
        Me.txtRecipientZip = New DevExpress.XtraEditors.TextEdit()
        Me.lblShipFormReceiverZip = New DevExpress.XtraEditors.LabelControl()
        Me.txtRecipientAddress = New DevExpress.XtraEditors.TextEdit()
        Me.txtRecipientDetailsDept = New DevExpress.XtraEditors.TextEdit()
        Me.txtRecipientDetailsCompany = New DevExpress.XtraEditors.TextEdit()
        Me.txtRecipientDetailsLast = New DevExpress.XtraEditors.TextEdit()
        Me.txtRecipientDetailsFirst = New DevExpress.XtraEditors.TextEdit()
        Me.txtRecipientCountry = New DevExpress.XtraEditors.TextEdit()
        Me.txtRecipientState = New DevExpress.XtraEditors.TextEdit()
        Me.txtRecipientCity = New DevExpress.XtraEditors.TextEdit()
        Me.txtRecipientEmail = New DevExpress.XtraEditors.TextEdit()
        Me.txtRecipientPhone = New DevExpress.XtraEditors.TextEdit()
        Me.lblShipFormReceiverEmail = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormReceiverLast = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormReceiverCompany = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormReceiverDept = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormReceiverAddress = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormReceiverCity = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormReceiverState = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormReceiverCountry = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormReceiverPhone = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormReceiverFirst = New DevExpress.XtraEditors.LabelControl()
        Me.txtCourier = New DevExpress.XtraEditors.TextEdit()
        Me.txtRefNum = New DevExpress.XtraEditors.TextEdit()
        Me.grpCourierSettings = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtShpType = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtShpVal = New DevExpress.XtraEditors.TextEdit()
        Me.txtShpDest = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCourAcct = New DevExpress.XtraEditors.TextEdit()
        Me.lblShipFormCourier = New DevExpress.XtraEditors.LabelControl()
        Me.lblShipFormDeliveryOption = New DevExpress.XtraEditors.LabelControl()
        Me.grpPackages = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.txtSldVal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtSldAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.txtSldType = New DevExpress.XtraEditors.TextEdit()
        Me.txtSldQty = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.txtSolVal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtSolAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.txtSolType = New DevExpress.XtraEditors.TextEdit()
        Me.txtSolQty = New DevExpress.XtraEditors.TextEdit()
        Me.btnCancelPrint = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPrint = New DevExpress.XtraEditors.SimpleButton()
        Me.btnOpenTemplate = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.chkIsHaz = New DevExpress.XtraEditors.CheckEdit()
        Me.chkIsDryIce = New DevExpress.XtraEditors.CheckEdit()
        Me.chkIsWetIce = New DevExpress.XtraEditors.CheckEdit()
        Me.chkIsGelIce = New DevExpress.XtraEditors.CheckEdit()
        Me.chkIsAmbient = New DevExpress.XtraEditors.CheckEdit()
        Me.btnAttachment = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpSenderInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpSenderInfo.SuspendLayout()
        CType(Me.txtCostCenter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderZip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderDetailsDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderDetailsCompany.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderDetailsLast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderDetailsFirst.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderCountry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderCity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSenderPhone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpRecipientInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpRecipientInfo.SuspendLayout()
        CType(Me.txtRecipientZip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientDetailsDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientDetailsCompany.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientDetailsLast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientDetailsFirst.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientCountry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientCity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientPhone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCourier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRefNum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCourierSettings, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCourierSettings.SuspendLayout()
        CType(Me.txtShpType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShpVal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShpDest.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCourAcct.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPackages.SuspendLayout()
        CType(Me.txtSldVal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSldAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSldType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSldQty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSolVal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSolAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSolType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSolQty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkIsHaz.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIsDryIce.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIsWetIce.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIsGelIce.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIsAmbient.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 738)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(780, 31)
        '
        'RibbonControl
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.ExpandCollapseItem.Name = ""
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 1
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Size = New System.Drawing.Size(780, 49)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'grpSenderInfo
        '
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormCostCenter)
        Me.grpSenderInfo.Controls.Add(Me.txtCostCenter)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderZip)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderZip)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderAddress)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderDetailsDept)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderDetailsCompany)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderDetailsLast)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderDetailsFirst)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderCountry)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderState)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderCity)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderEmail)
        Me.grpSenderInfo.Controls.Add(Me.txtSenderPhone)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderEmail)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderLast)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderCompany)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderDept)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderAddress)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderCity)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderState)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderCountry)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderPhone)
        Me.grpSenderInfo.Controls.Add(Me.lblShipFormSenderFirst)
        Me.grpSenderInfo.Location = New System.Drawing.Point(12, 54)
        Me.grpSenderInfo.Name = "grpSenderInfo"
        Me.grpSenderInfo.Size = New System.Drawing.Size(758, 146)
        Me.grpSenderInfo.TabIndex = 5
        Me.grpSenderInfo.Text = "Shipper Information"
        '
        'lblShipFormCostCenter
        '
        Me.lblShipFormCostCenter.Location = New System.Drawing.Point(376, 60)
        Me.lblShipFormCostCenter.Name = "lblShipFormCostCenter"
        Me.lblShipFormCostCenter.Size = New System.Drawing.Size(58, 13)
        Me.lblShipFormCostCenter.TabIndex = 69
        Me.lblShipFormCostCenter.Text = "Cost Center"
        '
        'txtCostCenter
        '
        Me.txtCostCenter.EditValue = "Cost Center"
        Me.txtCostCenter.Location = New System.Drawing.Point(440, 57)
        Me.txtCostCenter.Name = "txtCostCenter"
        Me.txtCostCenter.Size = New System.Drawing.Size(115, 20)
        Me.txtCostCenter.TabIndex = 68
        '
        'txtSenderZip
        '
        Me.txtSenderZip.EditValue = "Zip"
        Me.txtSenderZip.Location = New System.Drawing.Point(440, 86)
        Me.txtSenderZip.Name = "txtSenderZip"
        Me.txtSenderZip.Size = New System.Drawing.Size(115, 20)
        Me.txtSenderZip.TabIndex = 66
        '
        'lblShipFormSenderZip
        '
        Me.lblShipFormSenderZip.Location = New System.Drawing.Point(389, 89)
        Me.lblShipFormSenderZip.Name = "lblShipFormSenderZip"
        Me.lblShipFormSenderZip.Size = New System.Drawing.Size(14, 13)
        Me.lblShipFormSenderZip.TabIndex = 65
        Me.lblShipFormSenderZip.Text = "Zip"
        '
        'txtSenderAddress
        '
        Me.txtSenderAddress.EditValue = "Address"
        Me.txtSenderAddress.Location = New System.Drawing.Point(71, 57)
        Me.txtSenderAddress.Name = "txtSenderAddress"
        Me.txtSenderAddress.Size = New System.Drawing.Size(301, 20)
        Me.txtSenderAddress.TabIndex = 64
        '
        'txtSenderDetailsDept
        '
        Me.txtSenderDetailsDept.EditValue = "Department / Org Unit"
        Me.txtSenderDetailsDept.Location = New System.Drawing.Point(628, 29)
        Me.txtSenderDetailsDept.Name = "txtSenderDetailsDept"
        Me.txtSenderDetailsDept.Size = New System.Drawing.Size(115, 20)
        Me.txtSenderDetailsDept.TabIndex = 63
        '
        'txtSenderDetailsCompany
        '
        Me.txtSenderDetailsCompany.EditValue = "Company"
        Me.txtSenderDetailsCompany.Location = New System.Drawing.Point(440, 29)
        Me.txtSenderDetailsCompany.Name = "txtSenderDetailsCompany"
        Me.txtSenderDetailsCompany.Size = New System.Drawing.Size(115, 20)
        Me.txtSenderDetailsCompany.TabIndex = 62
        '
        'txtSenderDetailsLast
        '
        Me.txtSenderDetailsLast.EditValue = "Last Name"
        Me.txtSenderDetailsLast.Location = New System.Drawing.Point(257, 29)
        Me.txtSenderDetailsLast.Name = "txtSenderDetailsLast"
        Me.txtSenderDetailsLast.Size = New System.Drawing.Size(115, 20)
        Me.txtSenderDetailsLast.TabIndex = 61
        '
        'txtSenderDetailsFirst
        '
        Me.txtSenderDetailsFirst.EditValue = "First Name"
        Me.txtSenderDetailsFirst.Location = New System.Drawing.Point(71, 29)
        Me.txtSenderDetailsFirst.Name = "txtSenderDetailsFirst"
        Me.txtSenderDetailsFirst.Size = New System.Drawing.Size(115, 20)
        Me.txtSenderDetailsFirst.TabIndex = 60
        '
        'txtSenderCountry
        '
        Me.txtSenderCountry.EditValue = "Country"
        Me.txtSenderCountry.Location = New System.Drawing.Point(628, 86)
        Me.txtSenderCountry.Name = "txtSenderCountry"
        Me.txtSenderCountry.Size = New System.Drawing.Size(115, 20)
        Me.txtSenderCountry.TabIndex = 59
        '
        'txtSenderState
        '
        Me.txtSenderState.EditValue = "State / Province"
        Me.txtSenderState.Location = New System.Drawing.Point(257, 86)
        Me.txtSenderState.Name = "txtSenderState"
        Me.txtSenderState.Size = New System.Drawing.Size(115, 20)
        Me.txtSenderState.TabIndex = 58
        '
        'txtSenderCity
        '
        Me.txtSenderCity.EditValue = "City"
        Me.txtSenderCity.Location = New System.Drawing.Point(71, 86)
        Me.txtSenderCity.Name = "txtSenderCity"
        Me.txtSenderCity.Size = New System.Drawing.Size(115, 20)
        Me.txtSenderCity.TabIndex = 57
        '
        'txtSenderEmail
        '
        Me.txtSenderEmail.EditValue = "Email"
        Me.txtSenderEmail.Location = New System.Drawing.Point(71, 114)
        Me.txtSenderEmail.Name = "txtSenderEmail"
        Me.txtSenderEmail.Size = New System.Drawing.Size(301, 20)
        Me.txtSenderEmail.TabIndex = 56
        '
        'txtSenderPhone
        '
        Me.txtSenderPhone.EditValue = "Phone"
        Me.txtSenderPhone.Location = New System.Drawing.Point(440, 114)
        Me.txtSenderPhone.Name = "txtSenderPhone"
        Me.txtSenderPhone.Size = New System.Drawing.Size(115, 20)
        Me.txtSenderPhone.TabIndex = 55
        '
        'lblShipFormSenderEmail
        '
        Me.lblShipFormSenderEmail.Location = New System.Drawing.Point(14, 117)
        Me.lblShipFormSenderEmail.Name = "lblShipFormSenderEmail"
        Me.lblShipFormSenderEmail.Size = New System.Drawing.Size(24, 13)
        Me.lblShipFormSenderEmail.TabIndex = 54
        Me.lblShipFormSenderEmail.Text = "Email"
        '
        'lblShipFormSenderLast
        '
        Me.lblShipFormSenderLast.Location = New System.Drawing.Point(201, 32)
        Me.lblShipFormSenderLast.Name = "lblShipFormSenderLast"
        Me.lblShipFormSenderLast.Size = New System.Drawing.Size(50, 13)
        Me.lblShipFormSenderLast.TabIndex = 53
        Me.lblShipFormSenderLast.Text = "Last Name"
        '
        'lblShipFormSenderCompany
        '
        Me.lblShipFormSenderCompany.Location = New System.Drawing.Point(389, 32)
        Me.lblShipFormSenderCompany.Name = "lblShipFormSenderCompany"
        Me.lblShipFormSenderCompany.Size = New System.Drawing.Size(45, 13)
        Me.lblShipFormSenderCompany.TabIndex = 52
        Me.lblShipFormSenderCompany.Text = "Company"
        '
        'lblShipFormSenderDept
        '
        Me.lblShipFormSenderDept.Location = New System.Drawing.Point(563, 32)
        Me.lblShipFormSenderDept.Name = "lblShipFormSenderDept"
        Me.lblShipFormSenderDept.Size = New System.Drawing.Size(57, 13)
        Me.lblShipFormSenderDept.TabIndex = 51
        Me.lblShipFormSenderDept.Text = "Department"
        '
        'lblShipFormSenderAddress
        '
        Me.lblShipFormSenderAddress.Location = New System.Drawing.Point(14, 60)
        Me.lblShipFormSenderAddress.Name = "lblShipFormSenderAddress"
        Me.lblShipFormSenderAddress.Size = New System.Drawing.Size(39, 13)
        Me.lblShipFormSenderAddress.TabIndex = 50
        Me.lblShipFormSenderAddress.Text = "Address"
        '
        'lblShipFormSenderCity
        '
        Me.lblShipFormSenderCity.Location = New System.Drawing.Point(14, 89)
        Me.lblShipFormSenderCity.Name = "lblShipFormSenderCity"
        Me.lblShipFormSenderCity.Size = New System.Drawing.Size(19, 13)
        Me.lblShipFormSenderCity.TabIndex = 49
        Me.lblShipFormSenderCity.Text = "City"
        '
        'lblShipFormSenderState
        '
        Me.lblShipFormSenderState.Location = New System.Drawing.Point(201, 89)
        Me.lblShipFormSenderState.Name = "lblShipFormSenderState"
        Me.lblShipFormSenderState.Size = New System.Drawing.Size(26, 13)
        Me.lblShipFormSenderState.TabIndex = 48
        Me.lblShipFormSenderState.Text = "State"
        '
        'lblShipFormSenderCountry
        '
        Me.lblShipFormSenderCountry.Location = New System.Drawing.Point(563, 89)
        Me.lblShipFormSenderCountry.Name = "lblShipFormSenderCountry"
        Me.lblShipFormSenderCountry.Size = New System.Drawing.Size(39, 13)
        Me.lblShipFormSenderCountry.TabIndex = 47
        Me.lblShipFormSenderCountry.Text = "Country"
        '
        'lblShipFormSenderPhone
        '
        Me.lblShipFormSenderPhone.Location = New System.Drawing.Point(389, 117)
        Me.lblShipFormSenderPhone.Name = "lblShipFormSenderPhone"
        Me.lblShipFormSenderPhone.Size = New System.Drawing.Size(30, 13)
        Me.lblShipFormSenderPhone.TabIndex = 46
        Me.lblShipFormSenderPhone.Text = "Phone"
        '
        'lblShipFormSenderFirst
        '
        Me.lblShipFormSenderFirst.Location = New System.Drawing.Point(14, 32)
        Me.lblShipFormSenderFirst.Name = "lblShipFormSenderFirst"
        Me.lblShipFormSenderFirst.Size = New System.Drawing.Size(51, 13)
        Me.lblShipFormSenderFirst.TabIndex = 45
        Me.lblShipFormSenderFirst.Text = "First Name"
        '
        'grpRecipientInfo
        '
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientZip)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverZip)
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientAddress)
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientDetailsDept)
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientDetailsCompany)
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientDetailsLast)
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientDetailsFirst)
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientCountry)
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientState)
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientCity)
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientEmail)
        Me.grpRecipientInfo.Controls.Add(Me.txtRecipientPhone)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverEmail)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverLast)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverCompany)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverDept)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverAddress)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverCity)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverState)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverCountry)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverPhone)
        Me.grpRecipientInfo.Controls.Add(Me.lblShipFormReceiverFirst)
        Me.grpRecipientInfo.Location = New System.Drawing.Point(12, 206)
        Me.grpRecipientInfo.Name = "grpRecipientInfo"
        Me.grpRecipientInfo.Size = New System.Drawing.Size(758, 151)
        Me.grpRecipientInfo.TabIndex = 6
        Me.grpRecipientInfo.Text = "Recipient Information"
        '
        'txtRecipientZip
        '
        Me.txtRecipientZip.EditValue = "Zip"
        Me.txtRecipientZip.Location = New System.Drawing.Point(440, 86)
        Me.txtRecipientZip.Name = "txtRecipientZip"
        Me.txtRecipientZip.Size = New System.Drawing.Size(115, 20)
        Me.txtRecipientZip.TabIndex = 76
        '
        'lblShipFormReceiverZip
        '
        Me.lblShipFormReceiverZip.Location = New System.Drawing.Point(389, 89)
        Me.lblShipFormReceiverZip.Name = "lblShipFormReceiverZip"
        Me.lblShipFormReceiverZip.Size = New System.Drawing.Size(14, 13)
        Me.lblShipFormReceiverZip.TabIndex = 75
        Me.lblShipFormReceiverZip.Text = "Zip"
        '
        'txtRecipientAddress
        '
        Me.txtRecipientAddress.EditValue = "Address"
        Me.txtRecipientAddress.Location = New System.Drawing.Point(71, 57)
        Me.txtRecipientAddress.Name = "txtRecipientAddress"
        Me.txtRecipientAddress.Size = New System.Drawing.Size(301, 20)
        Me.txtRecipientAddress.TabIndex = 74
        '
        'txtRecipientDetailsDept
        '
        Me.txtRecipientDetailsDept.EditValue = "Department / Org Unit"
        Me.txtRecipientDetailsDept.Location = New System.Drawing.Point(629, 29)
        Me.txtRecipientDetailsDept.Name = "txtRecipientDetailsDept"
        Me.txtRecipientDetailsDept.Size = New System.Drawing.Size(114, 20)
        Me.txtRecipientDetailsDept.TabIndex = 73
        '
        'txtRecipientDetailsCompany
        '
        Me.txtRecipientDetailsCompany.EditValue = "Company"
        Me.txtRecipientDetailsCompany.Location = New System.Drawing.Point(440, 29)
        Me.txtRecipientDetailsCompany.Name = "txtRecipientDetailsCompany"
        Me.txtRecipientDetailsCompany.Size = New System.Drawing.Size(115, 20)
        Me.txtRecipientDetailsCompany.TabIndex = 72
        '
        'txtRecipientDetailsLast
        '
        Me.txtRecipientDetailsLast.EditValue = "Last Name"
        Me.txtRecipientDetailsLast.Location = New System.Drawing.Point(257, 29)
        Me.txtRecipientDetailsLast.Name = "txtRecipientDetailsLast"
        Me.txtRecipientDetailsLast.Size = New System.Drawing.Size(115, 20)
        Me.txtRecipientDetailsLast.TabIndex = 71
        '
        'txtRecipientDetailsFirst
        '
        Me.txtRecipientDetailsFirst.EditValue = "First Name"
        Me.txtRecipientDetailsFirst.Location = New System.Drawing.Point(71, 29)
        Me.txtRecipientDetailsFirst.Name = "txtRecipientDetailsFirst"
        Me.txtRecipientDetailsFirst.Size = New System.Drawing.Size(115, 20)
        Me.txtRecipientDetailsFirst.TabIndex = 70
        '
        'txtRecipientCountry
        '
        Me.txtRecipientCountry.EditValue = "Country"
        Me.txtRecipientCountry.Location = New System.Drawing.Point(628, 86)
        Me.txtRecipientCountry.Name = "txtRecipientCountry"
        Me.txtRecipientCountry.Size = New System.Drawing.Size(115, 20)
        Me.txtRecipientCountry.TabIndex = 69
        '
        'txtRecipientState
        '
        Me.txtRecipientState.EditValue = "State / Province"
        Me.txtRecipientState.Location = New System.Drawing.Point(257, 86)
        Me.txtRecipientState.Name = "txtRecipientState"
        Me.txtRecipientState.Size = New System.Drawing.Size(115, 20)
        Me.txtRecipientState.TabIndex = 68
        '
        'txtRecipientCity
        '
        Me.txtRecipientCity.EditValue = "City"
        Me.txtRecipientCity.Location = New System.Drawing.Point(71, 86)
        Me.txtRecipientCity.Name = "txtRecipientCity"
        Me.txtRecipientCity.Size = New System.Drawing.Size(115, 20)
        Me.txtRecipientCity.TabIndex = 67
        '
        'txtRecipientEmail
        '
        Me.txtRecipientEmail.EditValue = "Email"
        Me.txtRecipientEmail.Location = New System.Drawing.Point(71, 115)
        Me.txtRecipientEmail.Name = "txtRecipientEmail"
        Me.txtRecipientEmail.Size = New System.Drawing.Size(301, 20)
        Me.txtRecipientEmail.TabIndex = 66
        '
        'txtRecipientPhone
        '
        Me.txtRecipientPhone.EditValue = "Phone"
        Me.txtRecipientPhone.Location = New System.Drawing.Point(440, 115)
        Me.txtRecipientPhone.Name = "txtRecipientPhone"
        Me.txtRecipientPhone.Size = New System.Drawing.Size(115, 20)
        Me.txtRecipientPhone.TabIndex = 65
        '
        'lblShipFormReceiverEmail
        '
        Me.lblShipFormReceiverEmail.Location = New System.Drawing.Point(14, 118)
        Me.lblShipFormReceiverEmail.Name = "lblShipFormReceiverEmail"
        Me.lblShipFormReceiverEmail.Size = New System.Drawing.Size(24, 13)
        Me.lblShipFormReceiverEmail.TabIndex = 64
        Me.lblShipFormReceiverEmail.Text = "Email"
        '
        'lblShipFormReceiverLast
        '
        Me.lblShipFormReceiverLast.Location = New System.Drawing.Point(201, 32)
        Me.lblShipFormReceiverLast.Name = "lblShipFormReceiverLast"
        Me.lblShipFormReceiverLast.Size = New System.Drawing.Size(50, 13)
        Me.lblShipFormReceiverLast.TabIndex = 63
        Me.lblShipFormReceiverLast.Text = "Last Name"
        '
        'lblShipFormReceiverCompany
        '
        Me.lblShipFormReceiverCompany.Location = New System.Drawing.Point(389, 32)
        Me.lblShipFormReceiverCompany.Name = "lblShipFormReceiverCompany"
        Me.lblShipFormReceiverCompany.Size = New System.Drawing.Size(45, 13)
        Me.lblShipFormReceiverCompany.TabIndex = 62
        Me.lblShipFormReceiverCompany.Text = "Company"
        '
        'lblShipFormReceiverDept
        '
        Me.lblShipFormReceiverDept.Location = New System.Drawing.Point(563, 32)
        Me.lblShipFormReceiverDept.Name = "lblShipFormReceiverDept"
        Me.lblShipFormReceiverDept.Size = New System.Drawing.Size(57, 13)
        Me.lblShipFormReceiverDept.TabIndex = 61
        Me.lblShipFormReceiverDept.Text = "Department"
        '
        'lblShipFormReceiverAddress
        '
        Me.lblShipFormReceiverAddress.Location = New System.Drawing.Point(14, 60)
        Me.lblShipFormReceiverAddress.Name = "lblShipFormReceiverAddress"
        Me.lblShipFormReceiverAddress.Size = New System.Drawing.Size(39, 13)
        Me.lblShipFormReceiverAddress.TabIndex = 60
        Me.lblShipFormReceiverAddress.Text = "Address"
        '
        'lblShipFormReceiverCity
        '
        Me.lblShipFormReceiverCity.Location = New System.Drawing.Point(14, 89)
        Me.lblShipFormReceiverCity.Name = "lblShipFormReceiverCity"
        Me.lblShipFormReceiverCity.Size = New System.Drawing.Size(19, 13)
        Me.lblShipFormReceiverCity.TabIndex = 59
        Me.lblShipFormReceiverCity.Text = "City"
        '
        'lblShipFormReceiverState
        '
        Me.lblShipFormReceiverState.Location = New System.Drawing.Point(201, 89)
        Me.lblShipFormReceiverState.Name = "lblShipFormReceiverState"
        Me.lblShipFormReceiverState.Size = New System.Drawing.Size(26, 13)
        Me.lblShipFormReceiverState.TabIndex = 58
        Me.lblShipFormReceiverState.Text = "State"
        '
        'lblShipFormReceiverCountry
        '
        Me.lblShipFormReceiverCountry.Location = New System.Drawing.Point(563, 89)
        Me.lblShipFormReceiverCountry.Name = "lblShipFormReceiverCountry"
        Me.lblShipFormReceiverCountry.Size = New System.Drawing.Size(39, 13)
        Me.lblShipFormReceiverCountry.TabIndex = 57
        Me.lblShipFormReceiverCountry.Text = "Country"
        '
        'lblShipFormReceiverPhone
        '
        Me.lblShipFormReceiverPhone.Location = New System.Drawing.Point(389, 118)
        Me.lblShipFormReceiverPhone.Name = "lblShipFormReceiverPhone"
        Me.lblShipFormReceiverPhone.Size = New System.Drawing.Size(30, 13)
        Me.lblShipFormReceiverPhone.TabIndex = 56
        Me.lblShipFormReceiverPhone.Text = "Phone"
        '
        'lblShipFormReceiverFirst
        '
        Me.lblShipFormReceiverFirst.Location = New System.Drawing.Point(14, 32)
        Me.lblShipFormReceiverFirst.Name = "lblShipFormReceiverFirst"
        Me.lblShipFormReceiverFirst.Size = New System.Drawing.Size(51, 13)
        Me.lblShipFormReceiverFirst.TabIndex = 55
        Me.lblShipFormReceiverFirst.Text = "First Name"
        '
        'txtCourier
        '
        Me.txtCourier.EditValue = ""
        Me.txtCourier.Location = New System.Drawing.Point(71, 28)
        Me.txtCourier.Name = "txtCourier"
        Me.txtCourier.Size = New System.Drawing.Size(115, 20)
        Me.txtCourier.TabIndex = 23
        '
        'txtRefNum
        '
        Me.txtRefNum.EditValue = ""
        Me.txtRefNum.Location = New System.Drawing.Point(469, 28)
        Me.txtRefNum.Name = "txtRefNum"
        Me.txtRefNum.Size = New System.Drawing.Size(115, 20)
        Me.txtRefNum.TabIndex = 25
        '
        'grpCourierSettings
        '
        Me.grpCourierSettings.Controls.Add(Me.LabelControl2)
        Me.grpCourierSettings.Controls.Add(Me.txtShpType)
        Me.grpCourierSettings.Controls.Add(Me.LabelControl3)
        Me.grpCourierSettings.Controls.Add(Me.LabelControl4)
        Me.grpCourierSettings.Controls.Add(Me.txtShpVal)
        Me.grpCourierSettings.Controls.Add(Me.txtShpDest)
        Me.grpCourierSettings.Controls.Add(Me.LabelControl1)
        Me.grpCourierSettings.Controls.Add(Me.txtCourAcct)
        Me.grpCourierSettings.Controls.Add(Me.lblShipFormCourier)
        Me.grpCourierSettings.Controls.Add(Me.lblShipFormDeliveryOption)
        Me.grpCourierSettings.Controls.Add(Me.txtRefNum)
        Me.grpCourierSettings.Controls.Add(Me.txtCourier)
        Me.grpCourierSettings.Location = New System.Drawing.Point(12, 363)
        Me.grpCourierSettings.Name = "grpCourierSettings"
        Me.grpCourierSettings.Size = New System.Drawing.Size(758, 113)
        Me.grpCourierSettings.TabIndex = 7
        Me.grpCourierSettings.Text = "Shipment Service"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(195, 74)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl2.TabIndex = 76
        Me.LabelControl2.Text = "Priority"
        '
        'txtShpType
        '
        Me.txtShpType.EditValue = ""
        Me.txtShpType.Location = New System.Drawing.Point(257, 71)
        Me.txtShpType.Name = "txtShpType"
        Me.txtShpType.Size = New System.Drawing.Size(115, 20)
        Me.txtShpType.TabIndex = 27
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(14, 74)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl3.TabIndex = 74
        Me.LabelControl3.Text = "Destination"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(389, 74)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl4.TabIndex = 73
        Me.LabelControl4.Text = "Ins. Value"
        '
        'txtShpVal
        '
        Me.txtShpVal.EditValue = ""
        Me.txtShpVal.Location = New System.Drawing.Point(469, 71)
        Me.txtShpVal.Name = "txtShpVal"
        Me.txtShpVal.Size = New System.Drawing.Size(115, 20)
        Me.txtShpVal.TabIndex = 28
        '
        'txtShpDest
        '
        Me.txtShpDest.EditValue = ""
        Me.txtShpDest.Location = New System.Drawing.Point(71, 71)
        Me.txtShpDest.Name = "txtShpDest"
        Me.txtShpDest.Size = New System.Drawing.Size(115, 20)
        Me.txtShpDest.TabIndex = 26
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(195, 31)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl1.TabIndex = 70
        Me.LabelControl1.Text = "Courier Acct"
        '
        'txtCourAcct
        '
        Me.txtCourAcct.EditValue = ""
        Me.txtCourAcct.Location = New System.Drawing.Point(257, 28)
        Me.txtCourAcct.Name = "txtCourAcct"
        Me.txtCourAcct.Size = New System.Drawing.Size(115, 20)
        Me.txtCourAcct.TabIndex = 24
        '
        'lblShipFormCourier
        '
        Me.lblShipFormCourier.Location = New System.Drawing.Point(14, 31)
        Me.lblShipFormCourier.Name = "lblShipFormCourier"
        Me.lblShipFormCourier.Size = New System.Drawing.Size(35, 13)
        Me.lblShipFormCourier.TabIndex = 68
        Me.lblShipFormCourier.Text = "Courier"
        '
        'lblShipFormDeliveryOption
        '
        Me.lblShipFormDeliveryOption.Location = New System.Drawing.Point(389, 31)
        Me.lblShipFormDeliveryOption.Name = "lblShipFormDeliveryOption"
        Me.lblShipFormDeliveryOption.Size = New System.Drawing.Size(74, 13)
        Me.lblShipFormDeliveryOption.TabIndex = 66
        Me.lblShipFormDeliveryOption.Text = "Reference Num"
        '
        'grpPackages
        '
        Me.grpPackages.Controls.Add(Me.LabelControl5)
        Me.grpPackages.Controls.Add(Me.txtSldVal)
        Me.grpPackages.Controls.Add(Me.LabelControl6)
        Me.grpPackages.Controls.Add(Me.txtSldAmt)
        Me.grpPackages.Controls.Add(Me.LabelControl7)
        Me.grpPackages.Controls.Add(Me.LabelControl11)
        Me.grpPackages.Controls.Add(Me.txtSldType)
        Me.grpPackages.Controls.Add(Me.txtSldQty)
        Me.grpPackages.Controls.Add(Me.LabelControl12)
        Me.grpPackages.Controls.Add(Me.txtSolVal)
        Me.grpPackages.Controls.Add(Me.LabelControl8)
        Me.grpPackages.Controls.Add(Me.txtSolAmt)
        Me.grpPackages.Controls.Add(Me.LabelControl9)
        Me.grpPackages.Controls.Add(Me.LabelControl10)
        Me.grpPackages.Controls.Add(Me.txtSolType)
        Me.grpPackages.Controls.Add(Me.txtSolQty)
        Me.grpPackages.Location = New System.Drawing.Point(12, 482)
        Me.grpPackages.Name = "grpPackages"
        Me.grpPackages.Size = New System.Drawing.Size(758, 145)
        Me.grpPackages.TabIndex = 17
        Me.grpPackages.Text = "Shipment Contents"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(567, 90)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl5.TabIndex = 100
        Me.LabelControl5.Text = "Sld. Mat. Value"
        '
        'txtSldVal
        '
        Me.txtSldVal.EditValue = ""
        Me.txtSldVal.Location = New System.Drawing.Point(643, 87)
        Me.txtSldVal.Name = "txtSldVal"
        Me.txtSldVal.Size = New System.Drawing.Size(115, 20)
        Me.txtSldVal.TabIndex = 93
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(195, 90)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl6.TabIndex = 98
        Me.LabelControl6.Text = "Solid Amount"
        '
        'txtSldAmt
        '
        Me.txtSldAmt.EditValue = ""
        Me.txtSldAmt.Location = New System.Drawing.Point(279, 87)
        Me.txtSldAmt.Name = "txtSldAmt"
        Me.txtSldAmt.Size = New System.Drawing.Size(93, 20)
        Me.txtSldAmt.TabIndex = 91
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(14, 90)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl7.TabIndex = 96
        Me.LabelControl7.Text = "Solid Qty."
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(378, 90)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl11.TabIndex = 95
        Me.LabelControl11.Text = "Solid Container"
        '
        'txtSldType
        '
        Me.txtSldType.EditValue = ""
        Me.txtSldType.Location = New System.Drawing.Point(472, 87)
        Me.txtSldType.Name = "txtSldType"
        Me.txtSldType.Size = New System.Drawing.Size(86, 20)
        Me.txtSldType.TabIndex = 92
        '
        'txtSldQty
        '
        Me.txtSldQty.EditValue = ""
        Me.txtSldQty.Location = New System.Drawing.Point(83, 87)
        Me.txtSldQty.Name = "txtSldQty"
        Me.txtSldQty.Size = New System.Drawing.Size(103, 20)
        Me.txtSldQty.TabIndex = 90
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(567, 47)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl12.TabIndex = 92
        Me.LabelControl12.Text = "Sol. Mat. Value"
        '
        'txtSolVal
        '
        Me.txtSolVal.EditValue = ""
        Me.txtSolVal.Location = New System.Drawing.Point(643, 44)
        Me.txtSolVal.Name = "txtSolVal"
        Me.txtSolVal.Size = New System.Drawing.Size(115, 20)
        Me.txtSolVal.TabIndex = 83
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(195, 47)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl8.TabIndex = 82
        Me.LabelControl8.Text = "Solution Amount"
        '
        'txtSolAmt
        '
        Me.txtSolAmt.EditValue = ""
        Me.txtSolAmt.Location = New System.Drawing.Point(279, 44)
        Me.txtSolAmt.Name = "txtSolAmt"
        Me.txtSolAmt.Size = New System.Drawing.Size(93, 20)
        Me.txtSolAmt.TabIndex = 81
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(14, 47)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl9.TabIndex = 80
        Me.LabelControl9.Text = "Solution Qty."
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(378, 47)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(88, 13)
        Me.LabelControl10.TabIndex = 79
        Me.LabelControl10.Text = "Solution Container"
        '
        'txtSolType
        '
        Me.txtSolType.EditValue = ""
        Me.txtSolType.Location = New System.Drawing.Point(472, 44)
        Me.txtSolType.Name = "txtSolType"
        Me.txtSolType.Size = New System.Drawing.Size(86, 20)
        Me.txtSolType.TabIndex = 82
        '
        'txtSolQty
        '
        Me.txtSolQty.EditValue = ""
        Me.txtSolQty.Location = New System.Drawing.Point(83, 44)
        Me.txtSolQty.Name = "txtSolQty"
        Me.txtSolQty.Size = New System.Drawing.Size(103, 20)
        Me.txtSolQty.TabIndex = 80
        '
        'btnCancelPrint
        '
        Me.btnCancelPrint.Location = New System.Drawing.Point(622, 707)
        Me.btnCancelPrint.Name = "btnCancelPrint"
        Me.btnCancelPrint.Size = New System.Drawing.Size(148, 25)
        Me.btnCancelPrint.TabIndex = 20
        Me.btnCancelPrint.Text = "Cancel"
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(452, 707)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(148, 25)
        Me.btnPrint.TabIndex = 21
        Me.btnPrint.Text = "Print"
        '
        'btnOpenTemplate
        '
        Me.btnOpenTemplate.Location = New System.Drawing.Point(283, 707)
        Me.btnOpenTemplate.Name = "btnOpenTemplate"
        Me.btnOpenTemplate.Size = New System.Drawing.Size(148, 25)
        Me.btnOpenTemplate.TabIndex = 24
        Me.btnOpenTemplate.Text = "Open Template"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.chkIsHaz)
        Me.GroupControl1.Controls.Add(Me.chkIsDryIce)
        Me.GroupControl1.Controls.Add(Me.chkIsWetIce)
        Me.GroupControl1.Controls.Add(Me.chkIsGelIce)
        Me.GroupControl1.Controls.Add(Me.chkIsAmbient)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 633)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(758, 52)
        Me.GroupControl1.TabIndex = 27
        Me.GroupControl1.Text = "Package Classification"
        '
        'chkIsHaz
        '
        Me.chkIsHaz.Location = New System.Drawing.Point(501, 28)
        Me.chkIsHaz.MenuManager = Me.RibbonControl
        Me.chkIsHaz.Name = "chkIsHaz"
        Me.chkIsHaz.Properties.Caption = "Hazardous"
        Me.chkIsHaz.Size = New System.Drawing.Size(75, 19)
        Me.chkIsHaz.TabIndex = 4
        '
        'chkIsDryIce
        '
        Me.chkIsDryIce.Location = New System.Drawing.Point(420, 28)
        Me.chkIsDryIce.MenuManager = Me.RibbonControl
        Me.chkIsDryIce.Name = "chkIsDryIce"
        Me.chkIsDryIce.Properties.Caption = "Dry Ice"
        Me.chkIsDryIce.Size = New System.Drawing.Size(75, 19)
        Me.chkIsDryIce.TabIndex = 3
        '
        'chkIsWetIce
        '
        Me.chkIsWetIce.Location = New System.Drawing.Point(339, 28)
        Me.chkIsWetIce.MenuManager = Me.RibbonControl
        Me.chkIsWetIce.Name = "chkIsWetIce"
        Me.chkIsWetIce.Properties.Caption = "Wet Ice"
        Me.chkIsWetIce.Size = New System.Drawing.Size(75, 19)
        Me.chkIsWetIce.TabIndex = 2
        '
        'chkIsGelIce
        '
        Me.chkIsGelIce.Location = New System.Drawing.Point(255, 28)
        Me.chkIsGelIce.MenuManager = Me.RibbonControl
        Me.chkIsGelIce.Name = "chkIsGelIce"
        Me.chkIsGelIce.Properties.Caption = "Gel Packs"
        Me.chkIsGelIce.Size = New System.Drawing.Size(75, 19)
        Me.chkIsGelIce.TabIndex = 1
        '
        'chkIsAmbient
        '
        Me.chkIsAmbient.Location = New System.Drawing.Point(173, 28)
        Me.chkIsAmbient.MenuManager = Me.RibbonControl
        Me.chkIsAmbient.Name = "chkIsAmbient"
        Me.chkIsAmbient.Properties.Caption = "Ambient"
        Me.chkIsAmbient.Size = New System.Drawing.Size(75, 19)
        Me.chkIsAmbient.TabIndex = 0
        '
        'btnAttachment
        '
        Me.btnAttachment.Location = New System.Drawing.Point(115, 707)
        Me.btnAttachment.Name = "btnAttachment"
        Me.btnAttachment.Size = New System.Drawing.Size(148, 25)
        Me.btnAttachment.TabIndex = 30
        Me.btnAttachment.Text = "Open Attachment"
        '
        'ShipmentForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(780, 769)
        Me.Controls.Add(Me.btnAttachment)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnOpenTemplate)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnCancelPrint)
        Me.Controls.Add(Me.grpPackages)
        Me.Controls.Add(Me.grpCourierSettings)
        Me.Controls.Add(Me.grpRecipientInfo)
        Me.Controls.Add(Me.grpSenderInfo)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.Name = "ShipmentForm"
        Me.Ribbon = Me.RibbonControl
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "Shipment Request Form"
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpSenderInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpSenderInfo.ResumeLayout(False)
        Me.grpSenderInfo.PerformLayout()
        CType(Me.txtCostCenter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderZip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderDetailsDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderDetailsCompany.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderDetailsLast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderDetailsFirst.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderCountry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderCity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSenderPhone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpRecipientInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpRecipientInfo.ResumeLayout(False)
        Me.grpRecipientInfo.PerformLayout()
        CType(Me.txtRecipientZip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientDetailsDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientDetailsCompany.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientDetailsLast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientDetailsFirst.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientCountry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientCity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientPhone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCourier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRefNum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpCourierSettings, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCourierSettings.ResumeLayout(False)
        Me.grpCourierSettings.PerformLayout()
        CType(Me.txtShpType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShpVal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShpDest.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCourAcct.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpPackages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPackages.ResumeLayout(False)
        Me.grpPackages.PerformLayout()
        CType(Me.txtSldVal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSldAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSldType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSldQty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSolVal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSolAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSolType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSolQty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.chkIsHaz.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIsDryIce.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIsWetIce.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIsGelIce.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIsAmbient.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents grpSenderInfo As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grpRecipientInfo As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtCourier As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRefNum As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grpCourierSettings As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grpPackages As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCancelPrint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPrint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblShipFormSenderFirst As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormSenderEmail As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormSenderLast As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormSenderCompany As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormSenderDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormSenderAddress As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormSenderCity As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormSenderState As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormSenderCountry As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormSenderPhone As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormReceiverEmail As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormReceiverLast As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormReceiverCompany As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormReceiverDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormReceiverAddress As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormReceiverCity As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormReceiverState As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormReceiverCountry As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormReceiverPhone As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormReceiverFirst As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSenderAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSenderDetailsDept As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSenderDetailsCompany As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSenderDetailsLast As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSenderDetailsFirst As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSenderCountry As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSenderState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSenderCity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSenderEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSenderPhone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientDetailsDept As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientDetailsCompany As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientDetailsLast As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientDetailsFirst As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientCountry As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientCity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientPhone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblShipFormCourier As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShipFormDeliveryOption As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSenderZip As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblShipFormSenderZip As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtRecipientZip As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblShipFormReceiverZip As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnOpenTemplate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblShipFormCostCenter As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCostCenter As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCourAcct As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtShpType As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtShpVal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtShpDest As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSolVal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSolAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSolType As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSolQty As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSldVal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSldAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSldType As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSldQty As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkIsAmbient As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkIsHaz As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkIsDryIce As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkIsWetIce As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkIsGelIce As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents btnAttachment As DevExpress.XtraEditors.SimpleButton


End Class
