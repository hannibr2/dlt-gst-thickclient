Imports System.IO
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data.OracleClient
Imports System.Threading
Imports System.Text
Imports System.Net.NetworkCredential
Imports ExceptionsHandlerLib
Imports GST.NibrIAMAuth

Imports DevExpress.XtraGrid.Menu
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Export
Imports DevExpress.XtraBars

Imports DevExpress.XtraGrid.Columns
Imports DevExpress.Utils.Menu

Imports DevExpress.XtraEditors.Controls
Imports System.Xml
Imports BarcodeUtilityDLL

Imports System.Configuration
Imports System.Reflection
Imports System.Threading.Tasks
Imports Microsoft.Win32

'Imports ScannerLib


Public Class frmMain
    Public m_DecodingStopped = False
    Friend WithEvents m_objScanner As New CScanner
    Private oFormSingle As frmImageDisplay
    Public objContext As New DecodeLib.CDecodeLib()
    Private m_strBarcodesList As List(Of String)
    Private m_objDecode As DecodeLib.CDecodeLib
    Private m_strAppPath As String = String.Empty
    Dim m_ValForm As New Object

    Public m_frmRecipientInfo As RecipientInfo
    Private m_bLoggedIn As Boolean
    Private m_bSimpleLoggedIn As Boolean
    'Private m_sSimpleUsername As String
    Private m_bAddItemFromList As Boolean
    Private m_bAddItemFromScanner As Boolean
    Private m_sCurrentCountry As String
    Private m_MyRightClickMenu As ContextMenu
    Private m_AltRightClickMenu As ContextMenuStrip
    Private m_ScannedBarcode1 As DataTable
    Private m_ScannedBarcode2 As DataTable
    Private m_ScannedBarcode3 As DataTable
    Private m_aScannedBarcode As ArrayList
    Private m_ScannedPlate1 As BarcodeUtilityDLL.cPlate
    Private m_ScannedPlate2 As BarcodeUtilityDLL.cPlate
    Private m_ScannedPlate3 As BarcodeUtilityDLL.cPlate
    Private m_aScannedPlates As BarcodeUtilityDLL.cPlateList

    Private m_aLoadedPlates As ArrayList
    Private m_bIsCreatePackageInitialized As Boolean
    'Private m_bIsCreateShipmentInitialized As Boolean
    Private m_bIsViewShipmentInitialized As Boolean
    Private m_Labeler As LabelerUtility.cLabeler
    Private m_LabelerForm As LabelerUtility.cShippingForm
    Private m_Sender521 As String

    Private m_HasInvalidItems As Boolean

    Private m_frmPackageValidationRules As PackageValidationRules
    Private m_frmShipmentValidationRules As ShipmentValidationRules

    Friend WithEvents m_BarcodeReader As BarcodeUtilityDLL.cPlateReader

    Private m_bHasSelectedItem As Boolean
    Private m_bHasSelectedPackage As Boolean

    Private m_bIsSampleTransferMachine As Boolean
    Private m_bHasSTIBins As Boolean
    Private m_oSTIPackage As Package
    Private m_oSTIUser As STIUser
    'Private m_sSTIAccount As String
    Private m_bHasDropoffPackage As Boolean
    Private m_sDropoffPackageBarcode As String


    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'CREATE directories, if they don't exist, for configuration and data



        ' UnhandledExceptionManager.AddHandler()

        If Not Directory.Exists("C:\NIBR\GST") Then
            Directory.CreateDirectory("C:\NIBR\GST")
        End If

        'Dim x As Specialized.NameValueCollection
        'MessageBox.Show(x.Count.ToString)

        'Set initial layout
        Me.TabLogin.PageVisible = False
        Me.TabCreatePackage.PageVisible = False
        Me.TabCreateShipment.PageVisible = False
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabViewShipment.PageVisible = False
        Me.TabReceive.PageVisible = False
        Me.TabSettings.PageVisible = False
        Me.TabSampleTransfer.PageVisible = False
        Me.tabMain.Enabled = True
        'Me.NavSelectRecipient.Enabled = False
        'Me.NavCreatePackage.Enabled = False
        'Me.NavPackageMaintenance.Enabled = False
        'Me.NavCreateShipment.Enabled = False
        'Me.NavViewShipment.Enabled = False
        'Me.NavReceive.Enabled = False
        'Me.NavRepackage.Enabled = False
        'Me.NavSettings.Enabled = False
        'Me.NavShipmentPlanner.Enabled = False
        'Me.NavSampleDropoff.Visible = True

        m_bLoggedIn = False
        m_bIsCreatePackageInitialized = False
        'm_bIsCreateShipmentInitialized = False
        m_bIsViewShipmentInitialized = False
        m_bAddItemFromList = False
        m_bAddItemFromScanner = False
        m_sCurrentCountry = ""


        'Create a new and single instance of RecipientInfo

        'm_frmRecipientInfo = New RecipientInfo()

        m_frmPackageValidationRules = New PackageValidationRules()
        m_frmShipmentValidationRules = New ShipmentValidationRules()

        'Create a context menu
        m_MyRightClickMenu = New ContextMenu
        m_MyRightClickMenu.MenuItems.Add("Delete")

        m_AltRightClickMenu = New ContextMenuStrip()
        m_AltRightClickMenu.Items.Add("Unfinalize")

        Me.txtUsername.Text = System.Environment.UserName.ToUpper
        txtUsername.Select()

        If ConfigurationManager.AppSettings.Get("Environment").ToUpper = "PROD" Then
            Me.Text = "Global Shipping and Tracking - PROD"
        ElseIf ConfigurationManager.AppSettings.Get("Environment").ToUpper = "INT" Then
            Me.Text = "Global Shipping and Tracking - INT"
        Else
            Me.Text = "Global Shipping and Tracking - TEST"
        End If

        'Connect to PROD for validation
        Try
            Dim OracleDAL As New Oracle_General
            OracleDAL.GetDomains(System.Environment.UserDomainName.ToUpper)
            OracleDAL.ConnectToDatabase()
            'UpdateShipmentReferences()




            Dim STIDal As New Oracle_STI
            Dim TempWorkstation As String = System.Environment.MachineName
            Dim WorkstationBinCount As Integer = STIDal.GetBinCount(TempWorkstation)

            If WorkstationBinCount < 0 Then
                m_bIsSampleTransferMachine = False
                DisplayLoginTab()
            Else

                m_bIsSampleTransferMachine = True

                DisplaySampleTransferTab()

                If WorkstationBinCount = 0 Then
                    m_bHasSTIBins = False
                Else
                    m_bHasSTIBins = True
                End If
            End If

            SetSimpleLogin()
            'Me.NavSampleDropoff.Visible = m_bIsSampleTransferMachine
            DisplayNavigationRibbon(m_bLoggedIn)

        Catch err As Exception
            ShowMessage(err.Message)
        End Try


    End Sub


    Private Sub frmMain_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Oracle_General.DisconnectFromDatabase()
    End Sub

    Private Sub NavSelectRecipient_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        RecipientInfo.RecipientID = ""
        RecipientInfo.RecipientSelected = False
        RecipientInfo.ShowDialog()
    End Sub

    Private Sub DisplayNavigationRibbon(ByVal IsLoggedIn As Boolean)

        Me.bbtnCreatePackage.Enabled = IsLoggedIn
        Me.bbtnPackageMaintenance.Enabled = IsLoggedIn

        Me.bbtnCreateShipment.Enabled = IsLoggedIn
        Me.bbtnShipmentPlanner.Enabled = IsLoggedIn
        Me.bbtnReceiveShipment.Enabled = IsLoggedIn

        Me.bbtnViewDetails.Enabled = IsLoggedIn

        Me.bbtnSettings.Enabled = IsLoggedIn

        Me.bbtnSampleTransfer.Enabled = True

        Me.rpSampleTransfer.Visible = m_bIsSampleTransferMachine

    End Sub

    Private Sub RibbonControl_Click(sender As System.Object, e As System.EventArgs) Handles RibbonControl.Click
        If sender Is Nothing Then
            Dim test As Boolean = True
        End If

    End Sub

    Private Sub RibbonControl_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles RibbonControl.ItemClick
        'TODO: Add Logic to include the buttons

        If e.Item.Name = "" Then

        End If

    End Sub

    Private Sub bbtnLogin_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnLogin.ItemClick
        DisplayLoginTab()
    End Sub

    Private Sub bbtnPackageMaintenance_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnPackageMaintenance.ItemClick
        DisplayPackageMaintenanceTab()
    End Sub

    Private Sub bbtnCreateShipment_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnCreateShipment.ItemClick
        DisplayCreateShipmentTab()
    End Sub

    Private Sub bbtnShipmentPlanner_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnShipmentPlanner.ItemClick
        DisplayShipmentPlannerTab()
    End Sub

    Private Sub bbtnViewDetails_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnViewDetails.ItemClick
        DisplayViewShipmentTab()
    End Sub

    Private Sub bbtnSettings_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnSettings.ItemClick
        DisplaySettingsTab()
    End Sub

    Private Sub bbtnSampleTransfer_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnSampleTransfer.ItemClick
        DisplaySampleTransferTab()
    End Sub

    Private Sub DisplayLoginTab()
        Me.TabLogin.PageVisible = True
        Me.TabCreatePackage.PageVisible = False
        Me.TabPackageMaintenance.PageVisible = False
        Me.TabCreateShipment.PageVisible = False
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabViewShipment.PageVisible = False
        Me.TabReceive.PageVisible = False
        Me.TabSettings.PageVisible = False
        Me.TabSampleTransfer.PageVisible = False

        Me.RibbonControl.SelectedPage = rpLogin

        Me.tabMain.Enabled = True
    End Sub

    Private Sub bbtnCreatePackage_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnCreatePackage.ItemClick
        DisplayCreatePackageTab()
    End Sub

    Private Sub NavCreatePackage_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        DisplayCreatePackageTab()
    End Sub

    Private Sub DisplayCreatePackageTab()
        Me.TabLogin.PageVisible = False
        Me.TabCreatePackage.PageVisible = True
        Me.TabPackageMaintenance.PageVisible = False
        Me.TabCreateShipment.PageVisible = False
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabViewShipment.PageVisible = False
        Me.TabReceive.PageVisible = False
        Me.TabSettings.PageVisible = False
        Me.TabSampleTransfer.PageVisible = False

        Me.RibbonControl.SelectedPage = rpPackage

        RefreshCreatePackageDatagrids()
        RefreshCreatePackageDropdowns()
        m_bIsCreatePackageInitialized = True
    End Sub

    Private Sub NavPackageMaintenance_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        DisplayPackageMaintenanceTab()
    End Sub

    Private Sub DisplayPackageMaintenanceTab()
        Me.TabLogin.PageVisible = False
        Me.TabCreatePackage.PageVisible = False
        Me.TabPackageMaintenance.PageVisible = True
        Me.TabCreateShipment.PageVisible = False
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabViewShipment.PageVisible = False
        Me.TabReceive.PageVisible = False
        Me.TabSettings.PageVisible = False
        Me.TabSampleTransfer.PageVisible = False

        Me.RibbonControl.SelectedPage = rpPackage


        RefreshPackageMaintenanceDatagrids()
    End Sub

    Private Sub NavCreateShipment_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        DisplayCreateShipmentTab()
    End Sub

    Private Sub DisplayCreateShipmentTab()
        Me.TabLogin.PageVisible = False
        Me.TabCreatePackage.PageVisible = False
        Me.TabPackageMaintenance.PageVisible = False
        Me.TabCreateShipment.PageVisible = True
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabViewShipment.PageVisible = False
        Me.TabReceive.PageVisible = False
        Me.TabSettings.PageVisible = False
        Me.TabSampleTransfer.PageVisible = False

        Me.RibbonControl.SelectedPage = rpShipment

        RefreshCreateShipmentDatagrids()
        RefreshCreateShipmentDropdowns()
        'm_bIsCreateShipmentInitialized = True

        'Set the Shipment Calendar start date to today
        'schShipmentPlanner.Start = Today

    End Sub

    Private Sub NavViewShipment_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        DisplayViewShipmentTab()
    End Sub

    Private Sub DisplayViewShipmentTab()
        Me.TabLogin.PageVisible = False
        Me.TabCreatePackage.PageVisible = False
        Me.TabPackageMaintenance.PageVisible = False
        Me.TabCreateShipment.PageVisible = False
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabViewShipment.PageVisible = True
        Me.TabReceive.PageVisible = False
        Me.TabSettings.PageVisible = False
        Me.TabSampleTransfer.PageVisible = False

        Me.RibbonControl.SelectedPage = rpStats

        RefreshViewShipmentDropdowns()
        m_bIsViewShipmentInitialized = True
    End Sub

    Private Sub bbtnReceiveShipment_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnReceiveShipment.ItemClick
        DisplayReceiveTab()
    End Sub

    Private Sub DisplayReceiveTab()
        Me.TabCreatePackage.PageVisible = False
        Me.TabPackageMaintenance.PageVisible = False
        Me.TabCreateShipment.PageVisible = False
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabViewShipment.PageVisible = False
        Me.TabReceive.PageVisible = True
        Me.TabSettings.PageVisible = False
        Me.TabSampleTransfer.PageVisible = False

        Me.RibbonControl.SelectedPage = rpShipment

        RefreshReceiveDatagrids()
    End Sub

    Private Sub NavReceived_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        DisplayReceiveTab()
    End Sub

    Private Sub NavRepackage_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        Me.TabCreatePackage.PageVisible = False
        Me.TabPackageMaintenance.PageVisible = False
        Me.TabCreateShipment.PageVisible = False
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabViewShipment.PageVisible = False
        Me.TabReceive.PageVisible = False
        Me.TabSettings.PageVisible = False
        Me.TabSampleTransfer.PageVisible = False

        'RefreshReceiveDatagrids()
    End Sub

    Private Sub NavSettings_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        DisplaySettingsTab()
        'ChronosServiceTest()
    End Sub

    Private Sub DisplaySettingsTab()
        Me.TabLogin.PageVisible = False
        Me.TabCreatePackage.PageVisible = False
        Me.TabPackageMaintenance.PageVisible = False
        Me.TabCreateShipment.PageVisible = False
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabViewShipment.PageVisible = False
        Me.TabReceive.PageVisible = False
        Me.TabSettings.PageVisible = True
        Me.TabSampleTransfer.PageVisible = False

        Me.RibbonControl.SelectedPage = rpSettings

        RefreshSettings()
    End Sub

    Private Sub NavShipmentPlanner_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        DisplayShipmentPlannerTab()
    End Sub

    Private Sub DisplayShipmentPlannerTab()
        Me.TabLogin.PageVisible = False
        Me.TabCreatePackage.PageVisible = False
        Me.TabPackageMaintenance.PageVisible = False
        Me.TabCreateShipment.PageVisible = False
        Me.TabShipmentPlanner.PageVisible = True
        Me.TabViewShipment.PageVisible = False
        Me.TabReceive.PageVisible = False
        Me.TabSettings.PageVisible = False
        Me.TabSampleTransfer.PageVisible = False

        Me.RibbonControl.SelectedPage = rpShipment

        RefreshShipmentPlanner()
    End Sub

    Private Sub NavContacts_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        ShowMessage("Please contact Aaron Hohos (x43519) or Kai Chan (x45089)" + vbCrLf + "for any GST related issues.")
    End Sub

    Private Sub NavAbout_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs)
        ShowMessage("Global Shipping and Tracking" + vbCrLf + "Version " + Application.ProductVersion + vbCrLf + vbCrLf & _
                    "� Novartis Institutes for BioMedical Research, Inc.. All rights reserved.")
    End Sub

    Private Sub btnSelectRecipient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'ShowRecipientInfo()
        RecipientInfo.RecipientID = ""
        RecipientInfo.RecipientSelected = False
        RecipientInfo.ShowDialog()
    End Sub



    'Private Sub btnViewShipment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.txtTrackingNumber.Text = "123456" Then
    '        Me.picTrackingDemo.Visible = True
    '    Else
    '        Me.picTrackingDemo.Visible = False
    '    End If
    'End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        Try

            If Not m_bLoggedIn Then

                m_Sender521 = NibrIAMAuth.GetUser521(txtUsername.Text, txtUserPassword.Text, lkLoginDomains.EditValue)
                'If ValidateUser(txtUserPassword.Text.ToUpper) Then
                'm_Sender521 = txtUsername.Text
                'If ValidatePassword(m_Sender521, txtUserPassword.Text) Then

                If (Not String.IsNullOrEmpty(m_Sender521)) AndAlso String.Compare(m_Sender521, "Error") Then
                    Dim OracleDAL As New Oracle_General
                    Dim IsAuthorized As Boolean = OracleDAL.AuthorizeUser(m_Sender521.ToUpper)

                    If IsAuthorized Then

                        Me.GrpActiveUser.Text = System.Environment.UserDomainName + "/" + m_Sender521 + " logged in"
                        Me.tabMain.Enabled = True
                        Me.TabCreatePackage.Focus()
                        'Me.NavBarServices.Enabled = True
                        'Me.NavSelectRecipient.Enabled = True
                        'Me.NavCreatePackage.Enabled = True
                        'Me.NavPackageMaintenance.Enabled = True
                        'Me.NavCreateShipment.Enabled = True
                        'Me.NavViewShipment.Enabled = True
                        'Me.NavReceive.Enabled = True
                        'Me.NavRepackage.Enabled = True
                        'Me.NavSettings.Enabled = True
                        'Me.NavShipmentPlanner.Enabled = True
                        Me.txtUserPassword.Text = ""

                        'Change environment, user is already logged in to PROD for validation at this point
                        'Dim OracleDAL As New Oracle_General
                        'OracleDAL.DisconnectFromDatabase()

                        'Try
                        '    OracleDAL.ConnectToDatabase()

                        'Catch err As Exception
                        '    ShowMessage(err.Message)
                        'End Try

                        'Get the current country of the user to break down the number of pending items
                        OracleDAL.GetCurrentCountry(m_Sender521)

                        RefreshSettings()
                        'RefreshCreatePackageDropdowns()
                        'RefreshCreatePackageDatagrids()

                        'Setting sender information
                        SetSender(m_Sender521)

                        m_bLoggedIn = True
                        SetLogin(m_bLoggedIn)
                        DisplayNavigationRibbon(m_bLoggedIn)
                        If Me.TabSampleTransfer.PageVisible Then
                            SetSimpleLogin()
                        End If
                        DisplayCreatePackageTab()
                    Else
                        ShowMessage("User is not authorized")
                    End If
                Else
                    Me.GrpActiveUser.Text = "Active User:"
                    m_sCurrentCountry = ""
                    Me.tabMain.Enabled = False
                    'Me.NavSelectRecipient.Enabled = False
                    'Me.NavCreatePackage.Enabled = False
                    'Me.NavPackageMaintenance.Enabled = False
                    'Me.NavCreateShipment.Enabled = False
                    'Me.NavViewShipment.Enabled = False
                    'Me.NavReceive.Enabled = False
                    'Me.NavRepackage.Enabled = False
                    'Me.NavSettings.Enabled = False
                    'Me.NavShipmentPlanner.Enabled = False
                    Me.txtUserPassword.Text = ""
                    m_bLoggedIn = False
                    SetLogin(m_bLoggedIn)
                    DisplayNavigationRibbon(m_bLoggedIn)
                    Throw New Exception("Failed to validate user")
                End If
            Else
                If NibrIAMAuth.LogoutUser Then
                    Me.GrpActiveUser.Text = "Active User:"
                    m_sCurrentCountry = ""
                    Me.tabMain.Enabled = False
                    'Me.NavSelectRecipient.Enabled = False
                    'Me.NavCreatePackage.Enabled = False
                    'Me.NavPackageMaintenance.Enabled = False
                    'Me.NavCreateShipment.Enabled = False
                    'Me.NavViewShipment.Enabled = False
                    'Me.NavReceive.Enabled = False
                    'Me.NavRepackage.Enabled = False
                    'Me.NavSettings.Enabled = False
                    'Me.NavShipmentPlanner.Enabled = False
                    Me.txtUserPassword.Text = ""
                    m_bLoggedIn = False
                    SetLogin(m_bLoggedIn)
                    DisplayNavigationRibbon(m_bLoggedIn)
                    If Me.TabSampleTransfer.PageVisible Then
                        SetSimpleLogin()
                    End If
                Else
                    ShowMessage("Can't logout user")
                End If
            End If

            SetSimpleLogin()
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Sub txtPackageItemBarcode_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPackageItemBarcode.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            'GetTRTContainerInfo(txtPackageItemBarcode.Text
            btn_LookupScan.PerformClick()
        End If
    End Sub

    'Private Sub lkPackageContent_Closed(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs)
    '    'Get available container origin
    '    Dim OracleDAL As New Oracle_General
    '    OracleDAL.GetOriginCode(lkPackageContent.EditValue)
    'End Sub

    Private Sub lkPendingTimeframe_Closed(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles lkPendingTimeframe.Closed
        Dim ContainerDAL As New Oracle_Container
        Dim TempFulfillmentSite As String = lkFulfillmentSite.EditValue

        ContainerDAL.ShowPendingItems(m_frmPackageValidationRules, TempFulfillmentSite)
    End Sub

    Private Sub GrdViewExistingPackages_FocusedRowChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GrdViewExistingPackages.FocusedRowChanged
        Try
            Dim PackageDAL As New Oracle_Package
            Dim ContainerDAL As New Oracle_Container
            'Dim TempPackageBarcode As String
            Dim TempParentID As Decimal
            'txtPackageBarcode.Text = GrdViewExistingPackages.GetFocusedRowCellValue("BARCODE")
            'TempPackageBarcode = txtPackageBarcode.Text
            'TempParentID = GetParentID(txtPackageBarcode.Text)
            TempParentID = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")

            PackageDAL.UpdateActivePackageDetails(TempParentID)

            AssignPackageValidation(PackageDAL.GetPackageValidationRules(TempParentID))
            SetPackageValidationRules()
            DisplayPackageValidation()
            Dim TempFulfillmentSite As String = lkFulfillmentSite.EditValue
            ContainerDAL.ShowPendingItems(m_frmPackageValidationRules, TempFulfillmentSite)
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Sub GrdViewPendingItems_CustomDrawCell(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs) Handles GrdViewPendingItems.CustomDrawCell
        If e.RowHandle >= 0 Then

            If Not GrdViewPendingItems.GetRowCellValue(e.RowHandle, "LEAVING_DATE") Is DBNull.Value Then
                e.Appearance.BackColor = Color.Yellow
            End If

        End If
    End Sub

    Private Sub GrdViewExistingPackages_CustomDrawCell(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs) Handles GrdViewExistingPackages.CustomDrawCell
        If e.RowHandle >= 0 Then

            If Not GrdViewExistingPackages.GetRowCellValue(e.RowHandle, "LEAVING_DATE") Is DBNull.Value Then
                e.Appearance.BackColor = Color.Yellow
            End If

        End If
    End Sub

    Private Sub grdViewPendingPackages_FocusedRowChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
        Try
            'Dim TempShipmentBarcode As String
            'txtShipmentPackageBarcode.Text = GrdViewPendingPackages.GetFocusedRowCellValue("BARCODE")
            'TempShipmentBarcode = txtShipmentBarcode.Text
            'UpdateActiveShipmentDetails(txtShipmentPackageBarcode.Text)
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Sub grdViewExistingShipments_FocusedRowChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
        Try
            'Dim TempShipmentBarcode As String
            'txtShipmentBarcode.Text = grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_NAME")
            'TempShipmentBarcode = txtShipmentBarcode.Text

            Dim ShipmentDAL As New Oracle_Shipment
            Dim OracleDAL As New Oracle_General

            Dim TempReceiverID As Integer = -1
            ShipmentDAL.UpdateActiveShipmentDetails(grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_ID"))

            TempReceiverID = grdViewExistingShipments.GetFocusedRowCellValue("RECEIVER_ID")
            If TempReceiverID > 0 Then
                OracleDAL.SetReceiverInformation(grdViewExistingShipments.GetFocusedRowCellValue("RECEIVER_ID"))
            End If

            SetShipmentValidationRules()

        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub



    Private Sub txtUsername_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUsername.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Or e.KeyChar = Chr(Keys.Tab) Then
            txtUserPassword.Select()
        End If
    End Sub

    Private Sub txtUserPassword_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUserPassword.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnLogin.PerformClick()
        End If
    End Sub

    Private Sub txtPackageBarcode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'txtPackageBarcode.Text = ""
    End Sub

    Private Sub txtPackageItemBarcode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txtPackageItemBarcode.Text = ""
    End Sub

    Private Sub GrdViewPendingItems_BeforeLeaveRow(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowAllowEventArgs)
        m_bAddItemFromList = True
    End Sub

    Private Sub menuGridRightClick_ItemClicked(sender As System.Object, e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles menuGridRightClick.ItemClicked



        If Not menuGridRightClick.SourceControl Is Nothing Then
            If menuGridRightClick.SourceControl.Name = "grdExistingPackages" Then
                RemoveActivePackage()
            ElseIf menuGridRightClick.SourceControl.Name Is "grdActiveShipmentDetails" Then
                RemovePackages()
            ElseIf menuGridRightClick.SourceControl.Name Is "grdExistingShipments" Then
                RemoveShipment()
            End If
        End If

        menuRemoveItem.HideDropDown()
    End Sub

    Private Sub menuGridRightClick_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles menuGridRightClick.MouseClick
        'Check the source and see who is calling this

    End Sub

    'Private Sub UpdateShipmentReferences()
    '    Dim ServiceShipmentSAL As New Service_Shipment
    '    ServiceShipmentSAL.UpdateShipmentReferences()
    'End Sub


    Private Sub SetSender(ByVal Sender521 As String)
        Dim OracleDAL As New Oracle_General
        OracleDAL.SetSenderInformation(OracleDAL.GetSenderID(m_Sender521))
    End Sub

    Private Sub SetLogin(ByVal LoggedIn As Boolean)
        Me.lkLoginDomains.Visible = Not LoggedIn
        Me.lblDomain.Visible = Not LoggedIn
        Me.lblUsername.Visible = Not LoggedIn
        Me.txtUsername.Visible = Not LoggedIn
        Me.lblPassword.Visible = Not LoggedIn
        Me.txtUserPassword.Visible = Not LoggedIn


        If LoggedIn Then
            Me.btnLogin.Text = "Logout"
        Else
            Me.btnLogin.Text = "Login"
        End If
    End Sub


    Private Sub btnBrowseLabelTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        diaLabelTemplate.SelectedPath = txtLabelTemplatePath.Text
        diaLabelTemplate.ShowDialog()
        txtLabelTemplatePath.Text = diaLabelTemplate.SelectedPath
        diaLabelTemplate.Dispose()
    End Sub

    Private Sub btnBrowseShipmentTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        diaShipmentTemplate.FileName = txtShipmentTemplate.Text
        diaShipmentTemplate.ShowDialog()
        txtShipmentTemplate.Text = diaShipmentTemplate.FileName
        diaShipmentTemplate.Dispose()
    End Sub



#Region "           Create Package  Tab          "
    'Disables Selecting of the Pending Items



    Private Sub GrdViewPendingItems_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs)

        'Dim test As Boolean = False
        If (Not GrdViewPendingItems.ActiveFilter.IsEmpty) AndAlso e.Y > 99 Then
            'If there is a filter, allow clicking
            DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = False
        ElseIf e.Y < 20 Then
            'Allow clicking on the headers
            DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = False
        Else
            'Don't allow selecting data rows
            DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = True
        End If

    End Sub

    'Private Sub RemoveContainersBulkProc()
    '    Dim ContainerDAL As New Oracle_Container
    '    Dim PackageDAL As New Oracle_Package

    '    Dim TempContainerBarcodes As List(Of String) = New List(Of String)
    '    'Dim TempContainerIDs As List(Of Decimal) = New List(Of Decimal)
    '    'Dim TempContainerPhaseIDs As List(Of Integer) = New List(Of Integer)
    '    Dim TempPackageID As Decimal
    '    Dim TempIsRemoved As Boolean

    '    Dim TempUpdateContainerTable As UpdateContainerTable = New UpdateContainerTable
    '    Dim TempUpdateContainerResponseTable As UpdateContainerResponseTable = New UpdateContainerResponseTable
    '    Const m_RemoveContainerPhaseID As Decimal = 50002

    '    Try

    '        Dim RemoveIndex As Array = GrdViewActivePackage.GetSelectedRows
    '        TempPackageID = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")

    '        For Each CurrentIndex As Integer In RemoveIndex
    '            Dim CurrentBarcode As String = GrdViewActivePackage.GetRowCellValue(CurrentIndex, "BARCODE")
    '            Dim CurrentContainerID As Decimal = GrdViewActivePackage.GetRowCellValue(CurrentIndex, "CONTAINER_ID")
    '            TempContainerBarcodes.Add(CurrentBarcode)
    '            TempUpdateContainerTable.Add(New UpdateContainerObject(CurrentContainerID, CurrentContainerID, m_RemoveContainerPhaseID))
    '            TempUpdateContainerResponseTable.Add(New UpdateContainerResponseObject())
    '        Next

    '        PackageDAL.UnfinalizePackage(TempPackageID)
    '        TempIsRemoved = ContainerDAL.BulkProcedureRemoveContainers(TempUpdateContainerTable, TempUpdateContainerResponseTable)

    '        If TempIsRemoved Then
    '            If PackageDAL.GetPackageItemCount(TempPackageID) <= 0 Then
    '                PackageDAL.RemovePackage(TempPackageID)
    '            End If
    '            BulkChronosRemark(TempContainerBarcodes, UserSite, String.Format("Removed from Package {0}", TempPackageID))
    '        End If
    '        RefreshCreatePackageDatagrids()
    '    Catch err As Exception
    '        ShowMessage(err.Message)
    '    End Try

    'End Sub

    Private Sub RemoveContainersBulk()
        Dim ContainerDAL As New Oracle_Container
        Dim PackageDAL As New Oracle_Package

        Dim TempContainerBarcodes As List(Of String) = New List(Of String)
        Dim TempContainerIDs As List(Of Decimal) = New List(Of Decimal)
        Dim TempContainerPhaseIDs As List(Of Integer) = New List(Of Integer)
        Dim TempPackageID As Decimal
        Dim TempIsRemoved As Boolean

        Dim TempWorkstation As String = System.Environment.MachineName

        Try

            Dim RemoveIndex As Array = GrdViewActivePackage.GetSelectedRows
            TempPackageID = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")

            For Each CurrentIndex As Integer In RemoveIndex
                Dim CurrentBarcode As String = GrdViewActivePackage.GetRowCellValue(CurrentIndex, "BARCODE")
                Dim CurrentContainerID As Decimal = GrdViewActivePackage.GetRowCellValue(CurrentIndex, "CONTAINER_ID")
                TempContainerBarcodes.Add(CurrentBarcode)
                TempContainerIDs.Add(CurrentContainerID) 'ContainerDAL.GetContainerID(CurrentBarcode)
                TempContainerPhaseIDs.Add(50002)
            Next

            'PackageDAL.UnfinalizePackage(TempPackageID, TempWorkstation)
            TempIsRemoved = ContainerDAL.BulkRemoveContainersFromPackage(TempContainerIDs)

            'ContainerDAL.RemoveContainersFromPackage(TempContainerIDs, TempContainerPhaseIDs)
            'BulkRemoveContainersFromPackage
            'BulkProcedureRemoveContainers
            If TempIsRemoved Then
                If PackageDAL.GetPackageItemCount(TempPackageID) <= 0 Then
                    PackageDAL.RemovePackage(TempPackageID)
                End If
                'BulkChronosRemark(TempContainerBarcodes, UserSite, String.Format("Removed from Package {0}", TempPackageID))
            End If
            RefreshCreatePackageDatagrids()
        Catch err As Exception
            ShowMessage(err.Message)
        End Try

    End Sub

    'Private Sub RemoveContainers()
    '    Dim TempContainerID As Decimal
    '    Dim TempContainerBarcode As String
    '    'Dim TempParentID As Double
    '    Dim TempPackageOrigin As String
    '    'Dim TempPackageBarcode As String = txtPackageBarcode.Text
    '    Dim TempPackageID As Decimal
    '    Dim i As Integer
    '    Dim RemoveIndex As Array
    '    Dim IsFirst As Boolean = True

    '    Dim TempRemovedBarcodes As List(Of String) = New List(Of String)
    '    Dim TempIsRemoved As Boolean

    '    Try
    '        Dim ContainerDAL As New Oracle_Container
    '        Dim PackageDAL As New Oracle_Package

    '        TempPackageID = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")
    '        'Remove items from selected rows
    '        RemoveIndex = GrdViewActivePackage.GetSelectedRows
    '        For Each CurrentIndex As Integer In RemoveIndex

    '            TempIsRemoved = False

    '            TempContainerBarcode = GrdViewActivePackage.GetRowCellValue(CurrentIndex, "BARCODE")
    '            TempContainerID = GrdViewActivePackage.GetRowCellValue(CurrentIndex, "CONTAINER_ID") 'ContainerDAL.GetContainerID(TempContainerBarcode)
    '            'Assign Container ID as Parent ID to remove it from the current Parent
    '            'TempParentID = TempContainerID
    '            TempPackageOrigin = lkPackageOrigin.EditValue
    '            'Phase ID = 50002 (Container Arrived) => update phase from Container Packed to Arrived (5000350002)
    '            'UpdateContainers => ContainerID, ParentID, TypeID, Origin, Barcode, Ranking, PhaseID, Note, ReceiverID
    '            If IsFirst Then
    '                PackageDAL.UnfinalizePackage(TempPackageID)
    '                IsFirst = False
    '            End If

    '            TempIsRemoved = ContainerDAL.RemoveContainersFromPackage(TempContainerID, 50002)

    '            If TempIsRemoved Then
    '                TempRemovedBarcodes.Add(TempContainerBarcode)
    '            End If
    '            'ChronosRemarkService(TempContainerBarcode, UserSite, String.Format("Removed from Package {0}", TempPackageID))
    '        Next

    '        If PackageDAL.GetPackageItemCount(TempPackageID) <= 0 Then
    '            PackageDAL.RemovePackage(TempPackageID)
    '        End If

    '        If TempRemovedBarcodes.Count > 0 Then
    '            BulkChronosRemark(TempRemovedBarcodes, UserSite, String.Format("Removed from Package {0}", TempPackageID))
    '        End If

    '        RefreshCreatePackageDatagrids()
    '    Catch err As Exception
    '        ShowMessage(err.Message)
    '    End Try

    'End Sub

    Private Sub RefreshCreatePackageDatagrids(Optional ByVal NewPackageID As Long = -1)
        Try
            Dim OracleDAL As New Oracle_General
            Dim PackageDAL As New Oracle_Package
            Dim ContainerDAL As New Oracle_Container
            Dim TempWorkstation As String = System.Environment.MachineName

            Cursor.Current = Cursors.WaitCursor

            m_bHasSelectedItem = False
            'Store the current selected package

            Dim TempSelectedPackage As Boolean = False
            Dim TempPreRefreshPackageID As Long = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")

            If NewPackageID > 0 Then
                TempPreRefreshPackageID = CType(NewPackageID, Long)
            Else
                TempPreRefreshPackageID = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")
            End If


            grdExistingPackages.DataSource = Nothing
            grdActivePackageDetails.DataSource = Nothing


            'UpdateActivePackageDetails(GrdViewExistingPackages.GetRowCellValue(0, "PACKAGE_ID"))
            'GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")
            'txtPendingItems.Text = OracleDAL.GetTotalPendingItems().ToString()
            'txtDestinations.Text = OracleDAL.GetNumberOfDestinations().ToString()
            'txtDomestic.Text = OracleDAL.GetNumberOfDomesticItems(UserCountryName).ToString()
            'txtInternational.Text = OracleDAL.GetNumberOfInternationalItems(UserCountryName).ToString()

            OracleDAL.GetTimeframes()

            PackageDAL.ShowExistingPackages(TempWorkstation)

            If TempPreRefreshPackageID > 0 Then
                Dim TempNewRow As Integer = GrdViewExistingPackages.LocateByValue("PARENT_CONTAINER_ID", TempPreRefreshPackageID)
                'Dim TempVal As Integer = GrdViewExistingPackages.LocateByValue("PARENT_CONTAINER_ID", TempPreRefreshPackageID)
                If TempNewRow >= 0 Then
                    GrdViewExistingPackages.FocusedRowHandle = TempNewRow
                    TempSelectedPackage = True
                End If
            End If


            Dim TempParentID As Decimal = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")

            'If TempSelectedPackage Then
            PackageDAL.UpdateActivePackageDetails(TempParentID)
            'End If

            If GrdViewExistingPackages.RowCount > 0 Then
                AssignPackageValidation(PackageDAL.GetPackageValidationRules(TempParentID))
                SetPackageValidationRules()
                DisplayPackageValidation()
                Dim TempFulfillmentSite As String = lkFulfillmentSite.EditValue
                ContainerDAL.ShowPendingItems(m_frmPackageValidationRules, TempFulfillmentSite)
                Me.btnScanItems.Enabled = False
            Else
                grdPendingItems.DataSource = Nothing
                Me.btnScanItems.Enabled = True
            End If

            txtPackageItemBarcode.Text = ""
            txtPackageItemBarcode.Focus()

            Cursor.Current = Cursors.Default
        Catch err As Exception
            Cursor.Current = Cursors.Default
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Sub DisplayPackageValidation()
        Dim TempIsPackageValid As Boolean = True

        If GrdViewActivePackage.RowCount > 1 Then
            For CurrentRow As Integer = 1 To GrdViewActivePackage.RowCount - 1
                If Not ValidateActivePackageRow(CurrentRow) Then
                    TempIsPackageValid = False
                    Exit For
                End If
            Next
        End If

        If TempIsPackageValid Then
            lblPackageErrors.Visible = False
        Else
            lblPackageErrors.Visible = True
        End If

    End Sub

    Private Sub RefreshCreatePackageDropdowns()
        If Not m_bIsCreatePackageInitialized Then
            Try
                'Get available container types
                Dim ContainerDAL As New Oracle_Container
                ContainerDAL.GetContainerType()
                m_bIsCreatePackageInitialized = True
            Catch err As Exception
                ShowMessage(err.Message)
                m_bIsCreatePackageInitialized = False
            End Try
        End If
    End Sub

    Private Function ValidatePackageItems(ByVal PendingItemRow As Integer) As ValidationResult

        Dim CurrentValidationResult As New ValidationResult()

        Dim IsValid As Boolean = True
        Dim IsSameSite As Boolean = True
        Dim IsSameRecipient As Boolean = True
        Dim IsSameDestination As Boolean = True
        Dim IsSameContentType As Boolean = True
        Dim IsSameRequestNumber As Boolean = True
        Dim IsSameDeliveryForm As Boolean = True
        Dim IsSameProtocol As Boolean = True
        Dim IsSameDestRackGroupName As Boolean = True


        Dim ValidationMessageStub As String = String.Format("Cannot add item {0}to package {1}", GrdViewPendingItems.GetRowCellValue(PendingItemRow, "BARCODE"), GrdViewActivePackage.GetRowCellValue(0, "BARCODE"))

        If m_frmPackageValidationRules.Site Then
            Dim TempValidationSite As String = m_frmPackageValidationRules.SiteValue
            Dim TempItemSite As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationSite) Then
                If Not String.IsNullOrEmpty(GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.SiteColumn)) Then
                    TempItemSite = GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.SiteColumn)
                End If

                If TempValidationSite = TempItemSite Then
                    IsSameSite = True
                Else
                    IsSameSite = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails site validation")
                End If
            End If
        End If

        If m_frmPackageValidationRules.Recipient Then
            Dim TempValidationAccount As String = m_frmPackageValidationRules.RecipientValue
            Dim TempItemAccount As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationAccount) Then
                If Not String.IsNullOrEmpty(GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.RecipientColumn)) Then
                    TempItemAccount = GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.RecipientColumn)
                End If

                If TempValidationAccount = TempItemAccount Then
                    IsSameRecipient = True
                Else
                    IsSameRecipient = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails recipient validation")
                End If
            End If
        End If

        If m_frmPackageValidationRules.Destination Then
            Dim TempValidationDestination As String = m_frmPackageValidationRules.DestinationValue
            Dim TempItemDestination As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationDestination) Then
                If Not String.IsNullOrEmpty(GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.DestinationColumn)) Then
                    TempItemDestination = GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.DestinationColumn)
                End If

                If TempValidationDestination = TempItemDestination Then
                    IsSameDestination = True
                Else
                    IsSameDestination = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails destination validation")
                End If
            End If
        End If

        If m_frmPackageValidationRules.RequestNumber Then
            Dim TempRequestNumber As String = m_frmPackageValidationRules.RequestNumberValue
            Dim TempItemRequestNumber As String = String.Empty

            If Not String.IsNullOrEmpty(TempRequestNumber) Then
                If Not String.IsNullOrEmpty(GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.RequestNumberColumn)) Then
                    TempItemRequestNumber = GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.RequestNumberColumn)
                End If

                If TempRequestNumber = TempItemRequestNumber Then
                    IsSameRequestNumber = True
                Else
                    IsSameRequestNumber = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails request number validation")
                End If
            End If
        End If

        If m_frmPackageValidationRules.DeliveryForm Then
            Dim TempDeliveryForm As String = m_frmPackageValidationRules.DeliveryFormValue
            Dim TempItemDeliveryForm As String = String.Empty

            If Not String.IsNullOrEmpty(TempDeliveryForm) Then
                If Not String.IsNullOrEmpty(GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.DeliveryFormColumn)) Then
                    TempItemDeliveryForm = GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.DeliveryFormColumn)
                End If

                If TempDeliveryForm = TempItemDeliveryForm Then
                    IsSameDeliveryForm = True
                Else
                    IsSameDeliveryForm = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails delivery form validation")
                End If
            End If
        End If

        If m_frmPackageValidationRules.Protocol Then
            Dim TempValidationProtocol As String = m_frmPackageValidationRules.ProtocolValue
            Dim TempItemProtocol As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationProtocol) Then
                If Not String.IsNullOrEmpty(GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.ProtocolColumn)) Then
                    TempItemProtocol = GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.ProtocolColumn)
                End If

                If TempValidationProtocol = TempItemProtocol Then
                    IsSameProtocol = True
                Else
                    IsSameProtocol = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails protocol validation")
                End If
            End If
        End If

        If m_frmPackageValidationRules.DestRackGroupName Then
            Dim TempValidationDestRackGroupName As String = m_frmPackageValidationRules.DestRackGroupNameValue
            Dim TempItemDestRackGroupName As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationDestRackGroupName) Then
                If Not String.IsNullOrEmpty(GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.DestRackGroupNameColumn)) Then
                    TempItemDestRackGroupName = GrdViewPendingItems.GetRowCellValue(PendingItemRow, m_frmPackageValidationRules.DestRackGroupNameColumn)
                End If

                If TempValidationDestRackGroupName = TempItemDestRackGroupName Then
                    IsSameDestRackGroupName = True
                Else
                    IsSameDestRackGroupName = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails dest. rack group name validation")
                End If
            End If
        End If

        CurrentValidationResult.IsValid = IsSameSite AndAlso IsSameRecipient AndAlso IsSameDestination AndAlso IsSameContentType AndAlso IsSameRequestNumber AndAlso IsSameDeliveryForm AndAlso IsSameProtocol AndAlso IsSameDestRackGroupName

        Return CurrentValidationResult
    End Function

#Region "           Bulk Adding/Validation             "

    Private Sub btnCreatePackage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AddCreatePackage()
        'Dim TempPackageBarcode As String
        'Dim NewPackageID As Decimal = -1
        'Dim TempRowIndex As Integer()
        'Dim TempNote As String = String.Empty
        'Dim TempIsAdded As Boolean = False

        'Try
        '    'Removed the comment pop up for 1.0
        '    'AddComment(TempNote)

        '    TempRowIndex = GrdViewPendingItems.GetSelectedRows

        '    '#If CONFIG = "Debug" Then
        '    '            m_bHasSelectedItem = True
        '    '#End If

        '    If Not m_bHasSelectedItem OrElse TempRowIndex.Length = 0 Then
        '        ShowMessage("You must select an item before creating a package")
        '        'ElseIf String.IsNullOrEmpty(txtPackageBarcode.Text) OrElse txtPackageBarcode.Text = "Scan Package Barcode" Then
        '        '    ShowMessage("Invalid Package Barcode")
        '    Else
        '        'TempPackageBarcode = txtPackageBarcode.Text
        '        Dim TempDeliveryFormTypeName As String = GrdViewPendingItems.GetRowCellValue(TempRowIndex(0), "DELIVERY_FORM_TYPE_NAME")

        '        Dim ValForm As New frmValidationRules(TempDeliveryFormTypeName)
        '        ValForm.ShowDialog()
        '        Dim TempValidationIndex As List(Of Integer) = ValForm.SelectedRules


        '        NewPackageID = CreatePackage(TempNote, TempValidationIndex)



        '        'Add Items to package
        '        If NewPackageID > 0 Then
        '            'AddItemsToPackages(NewPackageID, TempRowIndex, TempNote)
        '            TempIsAdded = AddBulkItemsToPackage(NewPackageID, TempRowIndex, TempNote)
        '        End If

        '        If Not TempIsAdded Then
        '            RemovePackage(NewPackageID)
        '        End If
        '    End If
        'Catch err As Exception
        '    ShowMessage(err.Message)
        'End Try
        'RefreshCreatePackageDatagrids(NewPackageID)
    End Sub

    Private Sub btnAddItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        AddCreatePackage()
        'Dim TempPackageID As Long = -1
        ''Dim TempItemBarcode As String
        'Dim TempNote As String = String.Empty
        'Dim TempRowIndex As Integer()

        'Dim TempHasItems As Boolean = GrdViewPendingItems.SelectedRowsCount > 0
        'Dim TempHasPackage As Boolean = GrdViewExistingPackages.SelectedRowsCount > 0

        ''#If CONFIG = "Debug" Then
        ''        m_bHasSelectedItem = True
        ''#End If

        'If m_bHasSelectedItem AndAlso TempHasItems Then

        '    'Removed the comment pop up for 1.0
        '    'AddComment(TempNote)
        '    TempRowIndex = GrdViewPendingItems.GetSelectedRows

        '    If TempHasPackage Then
        '        TempPackageID = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID") 'PACKAGE_ID
        '    Else
        '        Dim TempDeliveryFormTypeName As String = GrdViewPendingItems.GetRowCellValue(TempRowIndex(0), "DELIVERY_FORM_TYPE_NAME")
        '        Dim ValForm As New frmValidationRules(TempDeliveryFormTypeName)
        '        ValForm.ShowDialog()
        '        Dim TempValidationIndex As List(Of Integer) = ValForm.SelectedRules

        '        TempPackageID = CreatePackage(TempNote, TempValidationIndex)
        '    End If

        '    If TempPackageID > 0 Then


        '        If TempRowIndex.Length > 0 Then
        '            'AddItemsToPackages(TempPackageID, TempPendingItems, TempNote)
        '            AddBulkItemsToPackage(TempPackageID, TempRowIndex, TempNote)
        '        End If
        '    Else
        '        ShowMessage("No selected package")
        '    End If
        'Else
        '    ShowMessage("Scan items to add")
        'End If


        ''Old Clean Up
        'm_aScannedBarcode = Nothing
        'm_bAddItemFromList = False
        'm_bAddItemFromScanner = False

        ''Refresh the data views
        'RefreshCreatePackageDatagrids(TempPackageID)
    End Sub

    Private Sub AddCreatePackage()

        Dim TempPackageID As Long = -1
        'Dim TempItemBarcode As String
        Dim TempNote As String = String.Empty
        Dim TempRowIndex As Integer()

        Dim TempHasItems As Boolean = GrdViewPendingItems.SelectedRowsCount > 0
        Dim TempHasPackage As Boolean = GrdViewExistingPackages.SelectedRowsCount > 0
        Dim TempCancelled As Boolean = True

        '#If CONFIG = "Debug" Then
        '        m_bHasSelectedItem = True
        '#End If

        If m_bHasSelectedItem AndAlso TempHasItems Then

            'Removed the comment pop up for 1.0
            'AddComment(TempNote)
            TempRowIndex = GrdViewPendingItems.GetSelectedRows

            If TempHasPackage Then
                TempPackageID = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID") 'PACKAGE_ID
            Else
                Dim TempDeliveryFormTypeName As String = GrdViewPendingItems.GetRowCellValue(TempRowIndex(0), "DELIVERY_FORM_TYPE_NAME").ToString
                Dim TempDestinationRackGroupName As String = GrdViewPendingItems.GetRowCellValue(TempRowIndex(0), "DESTINATION_RACK_GROUP_NAME").ToString
                Dim ValForm As New frmValidationRules(TempDeliveryFormTypeName, TempDestinationRackGroupName)
                ValForm.ShowDialog()

                If Not ValForm.SelectedRules Is Nothing Then
                    TempCancelled = False
                    Dim TempValidationIndex As List(Of Integer) = ValForm.SelectedRules
                    TempPackageID = CreatePackage(TempNote, TempValidationIndex)
                End If
            End If


            If TempPackageID > 0 Then
                If TempRowIndex.Length > 0 Then
                    'AddItemsToPackages(TempPackageID, TempPendingItems, TempNote)
                    AddBulkItemsToPackage(TempPackageID, TempRowIndex, TempNote)
                End If
            Else
                If Not TempCancelled Then
                    ShowMessage("No selected package")
                End If
            End If

        Else
            ShowMessage("Scan items to add")
        End If


        'Old Clean Up
        m_aScannedBarcode = Nothing
        m_bAddItemFromList = False
        m_bAddItemFromScanner = False

        'Refresh the data views
        RefreshCreatePackageDatagrids(TempPackageID)

    End Sub

    'Private Sub AddItemsToPackages(ByVal PackageID As Decimal, ByVal PendingItemIndex As Integer(), ByVal Note As String)

    '    Dim PackageDAL As New Oracle_Package
    '    Dim ContainerDAL As New Oracle_Container

    '    Dim TempContainerID As Decimal
    '    Dim TempPackageBarcode As String '= GetPackageBarcode(TempPackageID)
    '    Dim TempPackageID As Decimal = PackageID
    '    Dim TempContainerTypeID As Integer
    '    Dim TempPackageOrigin As String
    '    Dim TempContainerBarcode As String
    '    Dim TempRanking As Integer
    '    Dim TempNote As String = Note
    '    Dim TempRecipientID As Decimal
    '    Dim TempDeliveryFormTypeName As String

    '    Dim TempValidationResult As ValidationResult
    '    Dim TempValidationErrors As List(Of String) = New List(Of String)

    '    Dim IsFirstInsert As Boolean = True

    '    Dim TempAddedBarcodes As List(Of String) = New List(Of String)
    '    Dim TempIsAdded As Boolean

    '    TempPackageBarcode = PackageDAL.GetPackageBarcode(TempPackageID)

    '    For Each currentIndex As Integer In PendingItemIndex
    '        TempContainerID = GrdViewPendingItems.GetRowCellValue(currentIndex, "CONTAINER_ID")
    '        TempContainerTypeID = GrdViewPendingItems.GetRowCellValue(currentIndex, "TYPE_ID")
    '        TempPackageOrigin = GrdViewPendingItems.GetRowCellValue(currentIndex, "TYPE_NAME") 'lkPackageOrigin.EditValue
    '        TempContainerBarcode = GrdViewPendingItems.GetRowCellValue(currentIndex, "BARCODE")
    '        TempRecipientID = GrdViewPendingItems.GetRowCellValue(currentIndex, "RECEIVER_ID")
    '        TempDeliveryFormTypeName = GrdViewPendingItems.GetRowCellValue(currentIndex, "DELIVERY_FORM_TYPE_NAME")

    '        TempRanking = PackageDAL.GetPackageItemCount(TempPackageID) + 1


    '        TempValidationResult = ValidatePackageItems(currentIndex)

    '        TempIsAdded = False



    '        If TempRanking = 1 OrElse TempValidationResult.IsValid Then  'Overrides the validation result if it is a new package
    '            'Phase ID = 50003 (Container Packed)
    '            'UpdateContainers => ContainerID, ParentID, TypeID, Origin, Barcode, Ranking, PhaseID, Note, ReceiverID

    '            If IsFirstInsert Then
    '                PackageDAL.UnfinalizePackage(TempPackageID)
    '                SetPackageValidationCheckList(TempDeliveryFormTypeName)
    '                IsFirstInsert = False
    '            End If

    '            TempIsAdded = ContainerDAL.UpdateContainers(TempContainerID, TempPackageID, TempContainerTypeID, TempPackageOrigin, TempContainerBarcode, TempRanking, 50003, TempNote, TempRecipientID)
    '            'ChronosRemarkService(TempContainerBarcode, UserSite, String.Format("Added to Package {0}", TempPackageBarcode))

    '            If TempIsAdded Then
    '                TempAddedBarcodes.Add(TempContainerBarcode)
    '            End If
    '        Else
    '            TempIsAdded = False
    '            Dim ValidationMessage As String = "Barcode " & TempContainerBarcode & " not added because validation failures: " & vbCrLf

    '            For Each ValidationError As String In TempValidationResult.ValidationMessages
    '                ValidationMessage += ValidationError & vbCrLf
    '            Next

    '            TempValidationErrors.Add(ValidationMessage)
    '        End If

    '        If TempRanking = 1 Then
    '            PackageDAL.UpdateActivePackageDetails(TempPackageID)
    '            AssignPackageValidation(PackageDAL.GetPackageValidationRules(TempPackageID))
    '            SetPackageValidationRules()
    '        End If

    '    Next

    '    If TempAddedBarcodes.Count > 0 Then
    '        BulkChronosRemark(TempAddedBarcodes, UserSite, String.Format("Added to Package {0}", TempPackageBarcode))
    '    End If

    '    If TempValidationErrors.Count > 0 Then
    '        Dim FullMessage As String = String.Empty
    '        For Each CurrentError As String In TempValidationErrors
    '            FullMessage += CurrentError & vbCrLf
    '        Next
    '        ShowMessage(FullMessage)
    '    End If
    'End Sub

    Private Function CreateNonChronosPackage(ByVal Note) As Long
        Dim TempPackageID As Long

        'TODO: Modify this to add Chronos Package
        TempPackageID = -1

        Return TempPackageID
    End Function

    Private Function CreatePackage(ByVal Note As String, ByVal ValidationRulesIndex As List(Of Integer)) As Long

        Dim TempRecipientID As Long
        Dim TempContainerTypeID As Integer
        Dim TempPackageOrigin As String
        Dim TempPackageID As Long
        Dim TempRanking As Integer
        Dim TempWorkstation As String
        Dim TempNote As String = Note

        Dim TempRowIndex As Integer()


        Try
            Dim PackageDAL As New Oracle_Package

            TempRowIndex = GrdViewPendingItems.GetSelectedRows

            If TempRowIndex.Length = 0 Then
                ShowMessage("You must select an item before creating a package")
            Else

                TempRecipientID = GrdViewPendingItems.GetRowCellValue(TempRowIndex(0), "RECEIVER_ID")
                TempContainerTypeID = 130104 'GetContainerTypeID(lkPackageContent.EditValue)
                TempPackageOrigin = "PC"    'lkPackageOrigin.EditValue is not used, since empty package has an origin of PC
                'TempPackageBarcode = NewPackageBarcode
                'TempParentID = Nothing
                'TempParentID = txtPackageBarcode.Text
                'TempContainerBarcode = NewPackageBarcode
                TempRanking = 1
                'Phase ID = 50003 (Container Packed)
                'Package Barcode, Container Barcode, Request, Recipient ID
                'AddContainers(TempParentID, TempContainerTypeID, TempPackageOrigin, TempContainerBarcode, TempRanking, 50004, "KAI TEST", TempRecipientID)
                TempWorkstation = System.Environment.MachineName
                TempPackageID = PackageDAL.AddPackages(Nothing, 130104, "PC", TempRanking, 50101, TempNote, TempRecipientID, TempWorkstation)
                'TempParentID = GetParentID(TempContainerBarcode)
                'TempPackageID = GetParentID(TempContainerBarcode)
                PackageDAL.SetPackageValidationRules(TempPackageID, ValidationRulesIndex)
                AssignPackageValidation(PackageDAL.GetPackageValidationRules(TempPackageID))
            End If
        Catch err As Exception
            ShowMessage(err.Message)
            'Return String.Empty
        End Try

        Return TempPackageID
    End Function

    Private Function AddBulkItemsToPackage(ByVal PackageID As Decimal, ByVal PendingItemIndex As Integer(), ByVal Note As String) As Boolean
        Dim TempPackageID As Decimal = PackageID
        Dim TempIsAdded As Boolean = False
        Dim TempIsValid As Boolean = True

        Dim TempValidationErrors As List(Of String) = New List(Of String)
        Dim TempValidationResult As ValidationResult
        Dim IsFirstInsert As Boolean = True

        Dim TempAddedBarcodes As List(Of String) = New List(Of String)

        'Dim TempContainers As Containers = New Containers

        Dim PackageDAL As New Oracle_Package
        Dim ContainerDAL As New Oracle_Container

        Dim TempContainer As ContainerInput
        Dim TempContainerList As New List(Of ContainerInput)

        Dim TempPackageBarcode As String
        Dim TempRanking As Integer


        TempPackageBarcode = PackageDAL.GetPackageBarcode(TempPackageID)
        TempRanking = PackageDAL.GetPackageItemCount(TempPackageID)

        For Each currentIndex As Integer In PendingItemIndex
            Dim CurrentRow As DataRowView = GrdViewPendingItems.GetRow(currentIndex)
            TempContainer = New ContainerInput(CurrentRow)
            TempContainer.ParentID = TempPackageID
            TempContainer.PhaseID = 50003
            TempContainer.Ranking = TempRanking + 1

            If TempContainer.Ranking = 1 Then
                SetPackageValidationRules(CurrentRow)
                TempIsValid = True ' First item will always be valid
            Else
                TempValidationResult = ValidatePackageItems(CurrentRow, TempPackageBarcode)

                If Not TempValidationResult.IsValid Then
                    TempIsValid = False

                    Dim ValidationMessage As String = "Barcode " & TempContainer.Barcode & " not added because validation failures: " & vbCrLf
                    For Each ValidationError As String In TempValidationResult.ValidationMessages
                        ValidationMessage += ValidationError & vbCrLf
                    Next
                    TempValidationErrors.Add(ValidationMessage)
                End If
            End If

            If TempIsValid Then
                TempContainerList.Add(TempContainer)
                TempAddedBarcodes.Add(TempContainer.Barcode)
                TempRanking = TempRanking + 1
            Else
                Dim FullMessage As String = String.Empty
                For Each CurrentError As String In TempValidationErrors
                    FullMessage += CurrentError & vbCrLf
                Next
                Using FrmInvalid As New frmInvalidItem(TempContainer.Barcode, FullMessage)
                    FrmInvalid.ShowDialog()
                End Using

                Exit For
            End If
        Next

        If TempIsValid Then

            TempIsAdded = ContainerDAL.UpdateContainers(TempContainerList)
            'No longer updating Remark
            'If TempIsAdded Then
            '    BulkChronosRemark(TempAddedBarcodes, UserSite, String.Format("Added to Package {0}", TempPackageBarcode))
            'End If
        Else
            'Dim FullMessage As String
            'For Each CurrentError As String In TempValidationErrors
            '    FullMessage += CurrentError & vbCrLf
            'Next
            'ShowMessage(FullMessage)
        End If

        Return TempIsAdded
    End Function

    Private Sub SetPackageValidationRules(ByVal ValidationResult As DataRowView)

        m_frmPackageValidationRules.Site = ChkLstPackageValidationRules.Items("Novartis Site").CheckState
        m_frmPackageValidationRules.Recipient = ChkLstPackageValidationRules.Items("Recipient").CheckState
        m_frmPackageValidationRules.RequestNumber = ChkLstPackageValidationRules.Items("Request Number").CheckState
        m_frmPackageValidationRules.Destination = ChkLstPackageValidationRules.Items("Destination").CheckState
        m_frmPackageValidationRules.DeliveryForm = ChkLstPackageValidationRules.Items("Delivery Form").CheckState
        m_frmPackageValidationRules.Protocol = ChkLstPackageValidationRules.Items("Protocol").CheckState
        m_frmPackageValidationRules.DestRackGroupName = ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState

        m_frmPackageValidationRules.HasBase = True


        If m_frmPackageValidationRules.Site Then
            m_frmPackageValidationRules.SiteValue = ValidationResult.Item(m_frmPackageValidationRules.SiteColumn).ToString()
        Else
            m_frmPackageValidationRules.SiteValue = String.Empty
        End If

        If m_frmPackageValidationRules.Recipient Then
            m_frmPackageValidationRules.RecipientValue = ValidationResult.Item(m_frmPackageValidationRules.RecipientColumn).ToString()
        Else
            m_frmPackageValidationRules.RecipientValue = String.Empty
        End If

        If m_frmPackageValidationRules.RequestNumber Then
            m_frmPackageValidationRules.RequestNumberValue = ValidationResult.Item(m_frmPackageValidationRules.RequestNumberColumn).ToString()
        Else
            m_frmPackageValidationRules.RequestNumberValue = String.Empty
        End If

        If m_frmPackageValidationRules.Destination Then
            m_frmPackageValidationRules.DestinationValue = ValidationResult.Item(m_frmPackageValidationRules.DestinationColumn).ToString()
        Else
            m_frmPackageValidationRules.DestinationValue = String.Empty
        End If

        If m_frmPackageValidationRules.DeliveryForm Then
            m_frmPackageValidationRules.DeliveryFormValue = ValidationResult.Item(m_frmPackageValidationRules.DeliveryFormColumn).ToString()
        Else
            m_frmPackageValidationRules.DeliveryFormValue = String.Empty
        End If

        If m_frmPackageValidationRules.Protocol Then
            m_frmPackageValidationRules.ProtocolValue = ValidationResult.Item(m_frmPackageValidationRules.ProtocolColumn).ToString()
        Else
            m_frmPackageValidationRules.ProtocolValue = String.Empty
        End If

        If m_frmPackageValidationRules.DestRackGroupName Then
            m_frmPackageValidationRules.DestRackGroupNameValue = ValidationResult.Item(m_frmPackageValidationRules.DestRackGroupNameColumn).ToString()
        Else
            m_frmPackageValidationRules.DestRackGroupNameValue = String.Empty
        End If

    End Sub

    Private Function ValidatePackageItems(ByVal PendingItemRow As DataRowView, ByVal PackageName As String) As ValidationResult

        Dim CurrentValidationResult As New ValidationResult()

        Dim IsValid As Boolean = True
        Dim IsSameSite As Boolean = True
        Dim IsSameRecipient As Boolean = True
        Dim IsSameDestination As Boolean = True
        Dim IsSameContentType As Boolean = True
        Dim IsSameRequestNumber As Boolean = True
        Dim IsSameDeliveryForm As Boolean = True
        Dim IsSameProtocol As Boolean = True
        Dim IsSameDestRackGroupName As Boolean = True

        Dim TempItemBarcode As String = PendingItemRow.Item("BARCODE")

        'To Record

        Dim TempAccount As String = m_Sender521
        Dim ContainerDAL As New Oracle_Container

        Dim ValidationErrorInputs As New List(Of ValidationErrorInput)


        Dim ValidationMessageStub As String = String.Format("Cannot add item {0} to package {1}", TempItemBarcode, PackageName)

        If m_frmPackageValidationRules.Site Then
            Dim TempValidationSite As String = m_frmPackageValidationRules.SiteValue
            Dim TempItemSite As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationSite) Then
                If Not String.IsNullOrEmpty(PendingItemRow.Item(m_frmPackageValidationRules.SiteColumn).ToString()) Then
                    TempItemSite = PendingItemRow.Item(m_frmPackageValidationRules.SiteColumn).ToString()
                End If

                If TempValidationSite = TempItemSite Then
                    IsSameSite = True
                Else
                    IsSameSite = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails site validation")
                    ValidationErrorInputs.Add(New ValidationErrorInput(TempItemBarcode, PackageName, TempAccount, m_frmPackageValidationRules.SiteRuleID, TempValidationSite, TempItemSite))
                End If
            End If
        End If

        If m_frmPackageValidationRules.Recipient Then
            Dim TempValidationAccount As String = m_frmPackageValidationRules.RecipientValue
            Dim TempItemAccount As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationAccount) Then
                If Not String.IsNullOrEmpty(PendingItemRow.Item(m_frmPackageValidationRules.RecipientColumn).ToString()) Then
                    TempItemAccount = PendingItemRow.Item(m_frmPackageValidationRules.RecipientColumn).ToString()
                End If

                If TempValidationAccount = TempItemAccount Then
                    IsSameRecipient = True
                Else
                    IsSameRecipient = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails recipient validation")
                    ValidationErrorInputs.Add(New ValidationErrorInput(TempItemBarcode, PackageName, TempAccount, m_frmPackageValidationRules.RecipientRuleID, TempValidationAccount, TempItemAccount))
                End If
            End If
        End If

        If m_frmPackageValidationRules.Destination Then
            Dim TempValidationDestination As String = m_frmPackageValidationRules.DestinationValue
            Dim TempItemDestination As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationDestination) Then
                If Not String.IsNullOrEmpty(PendingItemRow.Item(m_frmPackageValidationRules.DestinationColumn).ToString()) Then
                    TempItemDestination = PendingItemRow.Item(m_frmPackageValidationRules.DestinationColumn).ToString()
                End If

                If TempValidationDestination = TempItemDestination Then
                    IsSameDestination = True
                Else
                    IsSameDestination = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails destination validation")
                    ValidationErrorInputs.Add(New ValidationErrorInput(TempItemBarcode, PackageName, TempAccount, m_frmPackageValidationRules.DestinationRuleID, TempValidationDestination, TempItemDestination))
                End If
            End If
        End If

        If m_frmPackageValidationRules.RequestNumber Then
            Dim TempRequestNumber As String = m_frmPackageValidationRules.RequestNumberValue
            Dim TempItemRequestNumber As String = String.Empty

            If Not String.IsNullOrEmpty(TempRequestNumber) Then
                If Not String.IsNullOrEmpty(PendingItemRow.Item(m_frmPackageValidationRules.RequestNumberColumn).ToString()) Then
                    TempItemRequestNumber = PendingItemRow.Item(m_frmPackageValidationRules.RequestNumberColumn).ToString()
                End If

                If TempRequestNumber = TempItemRequestNumber Then
                    IsSameRequestNumber = True
                Else
                    IsSameRequestNumber = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails request number validation")
                    ValidationErrorInputs.Add(New ValidationErrorInput(TempItemBarcode, PackageName, TempAccount, m_frmPackageValidationRules.RequestNumberRuleID, TempRequestNumber, TempItemRequestNumber))
                End If
            End If
        End If

        If m_frmPackageValidationRules.DeliveryForm Then
            Dim TempDeliveryForm As String = m_frmPackageValidationRules.DeliveryFormValue
            Dim TempItemDeliveryForm As String = String.Empty

            If Not String.IsNullOrEmpty(TempDeliveryForm) Then
                If Not String.IsNullOrEmpty(PendingItemRow.Item(m_frmPackageValidationRules.DeliveryFormColumn).ToString()) Then
                    TempItemDeliveryForm = PendingItemRow.Item(m_frmPackageValidationRules.DeliveryFormColumn).ToString()
                End If

                If TempDeliveryForm = TempItemDeliveryForm Then
                    IsSameDeliveryForm = True
                Else
                    IsSameDeliveryForm = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails delivery form validation")
                    ValidationErrorInputs.Add(New ValidationErrorInput(TempItemBarcode, PackageName, TempAccount, m_frmPackageValidationRules.DeliveryFormRuleID, TempDeliveryForm, TempItemDeliveryForm))
                End If
            End If
        End If

        If m_frmPackageValidationRules.Protocol Then
            Dim TempValidationProtocol As String = m_frmPackageValidationRules.ProtocolValue
            Dim TempItemProtocol As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationProtocol) Then
                If Not String.IsNullOrEmpty(PendingItemRow.Item(m_frmPackageValidationRules.ProtocolColumn).ToString()) Then
                    TempItemProtocol = PendingItemRow.Item(m_frmPackageValidationRules.ProtocolColumn).ToString()
                End If

                If TempValidationProtocol = TempItemProtocol Then
                    IsSameProtocol = True
                Else
                    IsSameProtocol = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails protocol validation")
                    ValidationErrorInputs.Add(New ValidationErrorInput(TempItemBarcode, PackageName, TempAccount, m_frmPackageValidationRules.ProtocolRuleID, TempValidationProtocol, TempItemProtocol))
                End If
            End If
        End If

        If m_frmPackageValidationRules.DestRackGroupName Then
            Dim TempValidationDestRackGroupName As String = m_frmPackageValidationRules.DestRackGroupNameValue
            Dim TempItemDestRackGroupName As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationDestRackGroupName) Then
                If Not String.IsNullOrEmpty(PendingItemRow.Item(m_frmPackageValidationRules.DestRackGroupNameColumn).ToString()) Then
                    TempItemDestRackGroupName = PendingItemRow.Item(m_frmPackageValidationRules.DestRackGroupNameColumn).ToString()
                End If

                If TempValidationDestRackGroupName = TempItemDestRackGroupName Then
                    IsSameDestRackGroupName = True
                Else
                    IsSameDestRackGroupName = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails dest. rack group name validation")
                    ValidationErrorInputs.Add(New ValidationErrorInput(TempItemBarcode, PackageName, TempAccount, m_frmPackageValidationRules.DestRackGroupNameRuleID, TempValidationDestRackGroupName, TempItemDestRackGroupName))
                End If
            End If
        End If

        CurrentValidationResult.IsValid = IsSameSite AndAlso IsSameRecipient AndAlso IsSameDestination AndAlso IsSameContentType AndAlso IsSameRequestNumber AndAlso IsSameDeliveryForm AndAlso IsSameProtocol AndAlso IsSameDestRackGroupName

        If ValidationErrorInputs.Count > 0 Then
            ContainerDAL.AddValidationErrors(ValidationErrorInputs)
        End If

        Return CurrentValidationResult
    End Function

    Private Sub RemovePackage(ByVal PackageID As Decimal)

        Dim PackageDAL As New Oracle_Package
        Dim PackageItems As Integer

        PackageItems = PackageDAL.GetPackageItemCount(PackageID)

        If Not PackageItems > 0 Then
            PackageDAL.RemovePackage(PackageID)
        End If
    End Sub



#End Region


    'Private Sub ChkLstPackageValidationRules_ItemCheck(ByVal sender As System.Object, ByVal e As ItemCheckEventArgs) Handles ChkLstPackageValidationRules.ItemCheck

    '    Dim PackageDAL As New Oracle_Package
    '    Dim ContainerDAL As New Oracle_Container


    '    Dim TempParentID As Decimal
    '    TempParentID = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")

    '    PackageDAL.UpdateActivePackageDetails(TempParentID)

    '    AssignPackageValidation(PackageDAL.GetPackageValidationRules(TempParentID))
    '    SetPackageValidationRules()
    '    DisplayPackageValidation()
    '    Dim TempFulfillmentSite As String = lkFulfillmentSite.EditValue
    '    ContainerDAL.ShowPendingItems(m_frmPackageValidationRules, TempFulfillmentSite)

    '    m_bHasSelectedItem = False
    'End Sub

    Private Sub AssignPackageValidation(ByVal ValidationRules As DataTable)
        Dim CurrentValidationRules As New PackageValidationRules


        ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked



        For Each CurrentRow As DataRow In ValidationRules.Rows

            Dim CurrentRuleID As Integer = CurrentRow.Item("RULE_ID")

            Select Case CurrentRuleID
                Case CurrentValidationRules.ProtocolRuleID
                    ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked

                Case CurrentValidationRules.RequestNumberRuleID
                    ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Checked

                Case CurrentValidationRules.RecipientRuleID
                    ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked

                Case CurrentValidationRules.SiteRuleID
                    ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Checked

                Case CurrentValidationRules.DestinationRuleID
                    ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Checked

                Case CurrentValidationRules.DeliveryFormRuleID
                    ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Checked

                Case CurrentValidationRules.DestRackGroupNameRuleID
                    ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Checked
                Case Else
            End Select

        Next

    End Sub

    Private Sub SetPackageValidationRules()

        m_frmPackageValidationRules.Site = ChkLstPackageValidationRules.Items("Novartis Site").CheckState
        m_frmPackageValidationRules.Recipient = ChkLstPackageValidationRules.Items("Recipient").CheckState
        m_frmPackageValidationRules.RequestNumber = ChkLstPackageValidationRules.Items("Request Number").CheckState
        m_frmPackageValidationRules.Destination = ChkLstPackageValidationRules.Items("Destination").CheckState
        m_frmPackageValidationRules.DeliveryForm = ChkLstPackageValidationRules.Items("Delivery Form").CheckState
        m_frmPackageValidationRules.Protocol = ChkLstPackageValidationRules.Items("Protocol").CheckState
        m_frmPackageValidationRules.DestRackGroupName = ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState

        m_frmPackageValidationRules.HasBase = GrdViewActivePackage.RowCount > 0


        If m_frmPackageValidationRules.Site AndAlso GrdViewActivePackage.RowCount > 0 Then
            m_frmPackageValidationRules.SiteValue = GrdViewActivePackage.GetRowCellValue(0, m_frmPackageValidationRules.SiteColumn).ToString()
        Else
            m_frmPackageValidationRules.SiteValue = String.Empty
        End If

        If m_frmPackageValidationRules.Recipient AndAlso GrdViewActivePackage.RowCount > 0 Then
            m_frmPackageValidationRules.RecipientValue = GrdViewActivePackage.GetRowCellValue(0, m_frmPackageValidationRules.RecipientColumn).ToString()
        Else
            m_frmPackageValidationRules.RecipientValue = String.Empty
        End If

        If m_frmPackageValidationRules.RequestNumber AndAlso GrdViewActivePackage.RowCount > 0 Then
            m_frmPackageValidationRules.RequestNumberValue = GrdViewActivePackage.GetRowCellValue(0, m_frmPackageValidationRules.RequestNumberColumn).ToString()
        Else
            m_frmPackageValidationRules.RequestNumberValue = String.Empty
        End If

        If m_frmPackageValidationRules.Destination AndAlso GrdViewActivePackage.RowCount > 0 Then
            m_frmPackageValidationRules.DestinationValue = GrdViewActivePackage.GetRowCellValue(0, m_frmPackageValidationRules.DestinationColumn).ToString()
        Else
            m_frmPackageValidationRules.DestinationValue = String.Empty
        End If

        If m_frmPackageValidationRules.DeliveryForm AndAlso GrdViewActivePackage.RowCount > 0 Then
            m_frmPackageValidationRules.DeliveryFormValue = GrdViewActivePackage.GetRowCellValue(0, m_frmPackageValidationRules.DeliveryFormColumn).ToString()
        Else
            m_frmPackageValidationRules.DeliveryFormValue = String.Empty
        End If

        If m_frmPackageValidationRules.Protocol AndAlso GrdViewActivePackage.RowCount > 0 Then
            m_frmPackageValidationRules.ProtocolValue = GrdViewActivePackage.GetRowCellValue(0, m_frmPackageValidationRules.ProtocolColumn).ToString()
        Else
            m_frmPackageValidationRules.ProtocolValue = String.Empty
        End If

        If m_frmPackageValidationRules.DestRackGroupName AndAlso GrdViewActivePackage.RowCount > 0 Then
            m_frmPackageValidationRules.DestRackGroupNameValue = GrdViewActivePackage.GetRowCellValue(0, m_frmPackageValidationRules.DestRackGroupNameColumn).ToString()
        Else
            m_frmPackageValidationRules.DestRackGroupNameValue = String.Empty
        End If

    End Sub

    Private Sub GrdViewActivePackage_RowStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs)
        Dim View As DevExpress.XtraGrid.Views.Grid.GridView = sender
        'Dim IsValid As Boolean = True

        'Dim CurrentValidationResult As ValidationResult

        If (e.RowHandle >= 0) Then

            'CurrentValidationResult = ValidatePackageItems(e.RowHandle)

            'If Not CurrentValidationResult.IsValid Then
            '    e.Appearance.BackColor = Color.Salmon
            'End If

            'Site
            'If m_frmPackageValidationRules.Site Then
            '    If Not String.IsNullOrEmpty(m_frmPackageValidationRules.SiteValue) AndAlso Not m_frmPackageValidationRules.SiteValue = View.GetRowCellValue(e.RowHandle, m_frmPackageValidationRules.SiteColumn) Then
            '        e.Appearance.BackColor = Color.Salmon
            '        e.Appearance.BackColor2 = Color.SeaShell
            '    End If
            'End If

            ''Recipient
            'If m_frmPackageValidationRules.Recipient Then
            '    If Not String.IsNullOrEmpty(m_frmPackageValidationRules.RecipientValue) AndAlso Not m_frmPackageValidationRules.RecipientValue = View.GetRowCellValue(e.RowHandle, m_frmPackageValidationRules.RecipientColumn) Then
            '        e.Appearance.BackColor = Color.Salmon
            '        e.Appearance.BackColor2 = Color.SeaShell
            '    End If
            'End If

            ''Destination
            'If m_frmPackageValidationRules.Destination Then
            '    If Not String.IsNullOrEmpty(m_frmPackageValidationRules.DestinationValue) AndAlso Not m_frmPackageValidationRules.DestinationValue = View.GetRowCellValue(e.RowHandle, m_frmPackageValidationRules.DestinationColumn) Then
            '        e.Appearance.BackColor = Color.Salmon
            '        e.Appearance.BackColor2 = Color.SeaShell
            '    End If
            'End If


            ''Request Number
            'If m_frmPackageValidationRules.RequestNumber Then
            '    If Not String.IsNullOrEmpty(m_frmPackageValidationRules.RequestNumberValue) AndAlso Not m_frmPackageValidationRules.RequestNumberValue = View.GetRowCellValue(e.RowHandle, m_frmPackageValidationRules.RequestNumberColumn) Then
            '        e.Appearance.BackColor = Color.Salmon
            '        e.Appearance.BackColor2 = Color.SeaShell
            '    End If
            'End If

            ''Delivery Form
            'If m_frmPackageValidationRules.DeliveryForm Then
            '    If Not String.IsNullOrEmpty(m_frmPackageValidationRules.DeliveryFormValue) AndAlso Not m_frmPackageValidationRules.DeliveryFormValue = View.GetRowCellValue(e.RowHandle, m_frmPackageValidationRules.DeliveryFormColumn) Then
            '        e.Appearance.BackColor = Color.Salmon
            '        e.Appearance.BackColor2 = Color.SeaShell
            '    End If
            'End If

            ''Protocol
            'If m_frmPackageValidationRules.Protocol Then
            '    If Not String.IsNullOrEmpty(m_frmPackageValidationRules.ProtocolValue) AndAlso Not m_frmPackageValidationRules.ProtocolValue = View.GetRowCellValue(e.RowHandle, m_frmPackageValidationRules.ProtocolColumn) Then
            '        e.Appearance.BackColor = Color.Salmon
            '        e.Appearance.BackColor2 = Color.SeaShell
            '    End If
            'End If

            ''Dest. Rack Group Name
            'If m_frmPackageValidationRules.DestRackGroupName Then
            '    If Not String.IsNullOrEmpty(m_frmPackageValidationRules.DestRackGroupNameValue) AndAlso Not m_frmPackageValidationRules.DestRackGroupNameValue = View.GetRowCellValue(e.RowHandle, m_frmPackageValidationRules.DestRackGroupNameColumn) Then
            '        e.Appearance.BackColor = Color.Salmon
            '        e.Appearance.BackColor2 = Color.SeaShell
            '    End If
            'End If

            If Not ValidateActivePackageRow(e.RowHandle) Then
                e.Appearance.BackColor = Color.Salmon
                e.Appearance.BackColor2 = Color.SeaShell
            End If

        End If

    End Sub

    Private Function ValidateActivePackageRow(ByVal CurrentRow As Integer) As Boolean
        Dim IsValid As Boolean = True

        If m_frmPackageValidationRules.Site Then
            If Not String.IsNullOrEmpty(m_frmPackageValidationRules.SiteValue) AndAlso Not m_frmPackageValidationRules.SiteValue = GrdViewActivePackage.GetRowCellValue(CurrentRow, m_frmPackageValidationRules.SiteColumn) Then
                IsValid = False
            End If
        End If

        'Recipient
        If m_frmPackageValidationRules.Recipient Then
            If Not String.IsNullOrEmpty(m_frmPackageValidationRules.RecipientValue) AndAlso Not m_frmPackageValidationRules.RecipientValue = GrdViewActivePackage.GetRowCellValue(CurrentRow, m_frmPackageValidationRules.RecipientColumn) Then
                IsValid = False
            End If
        End If

        'Destination
        If m_frmPackageValidationRules.Destination Then
            If Not String.IsNullOrEmpty(m_frmPackageValidationRules.DestinationValue) AndAlso Not m_frmPackageValidationRules.DestinationValue = GrdViewActivePackage.GetRowCellValue(CurrentRow, m_frmPackageValidationRules.DestinationColumn) Then
                IsValid = False
            End If
        End If


        'Request Number
        If m_frmPackageValidationRules.RequestNumber Then
            If Not String.IsNullOrEmpty(m_frmPackageValidationRules.RequestNumberValue) AndAlso Not m_frmPackageValidationRules.RequestNumberValue = GrdViewActivePackage.GetRowCellValue(CurrentRow, m_frmPackageValidationRules.RequestNumberColumn) Then
                IsValid = False
            End If
        End If

        'Delivery Form
        If m_frmPackageValidationRules.DeliveryForm Then
            If Not String.IsNullOrEmpty(m_frmPackageValidationRules.DeliveryFormValue) AndAlso Not m_frmPackageValidationRules.DeliveryFormValue = GrdViewActivePackage.GetRowCellValue(CurrentRow, m_frmPackageValidationRules.DeliveryFormColumn) Then
                IsValid = False
            End If
        End If

        'Protocol
        If m_frmPackageValidationRules.Protocol Then
            If Not String.IsNullOrEmpty(m_frmPackageValidationRules.ProtocolValue) AndAlso Not m_frmPackageValidationRules.ProtocolValue = GrdViewActivePackage.GetRowCellValue(CurrentRow, m_frmPackageValidationRules.ProtocolColumn) Then
                IsValid = False
            End If
        End If

        'Dest. Rack Group Name
        If m_frmPackageValidationRules.DestRackGroupName Then
            If Not String.IsNullOrEmpty(m_frmPackageValidationRules.DestRackGroupNameValue) AndAlso Not m_frmPackageValidationRules.DestRackGroupNameValue = GrdViewActivePackage.GetRowCellValue(CurrentRow, m_frmPackageValidationRules.DestRackGroupNameColumn) Then
                IsValid = False
            End If
        End If

        Return IsValid
    End Function

    Private Sub btnShowAllPending_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowAllPending.Click
        Dim ContainerDAL As New Oracle_Container
        Dim TempFulfillmentSite As String = lkFulfillmentSite.EditValue
        ContainerDAL.ShowPendingItems(m_frmPackageValidationRules, TempFulfillmentSite, False)
        m_bHasSelectedItem = False
    End Sub

    Private Function ValidateContainersToAdd(ByVal InputContainer As List(Of ContainerInput)) As Boolean

    End Function

    Private Sub SetPackageValidationCheckList(ByVal DeliveryForm As String)

        If String.Compare(DeliveryForm, "Powder", True) Then
            ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
        ElseIf String.Compare(DeliveryForm, "Solution", True) Then
            ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
        ElseIf String.Compare(DeliveryForm, "Plate", True) Then
            ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
        Else
            ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
        End If

    End Sub
    ''' <summary>
    ''' Add container/item to the selected package
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 


    Private Sub btnUpdatePackage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim TempRecipientID As Decimal
        Dim TempContainerTypeID As Integer
        Dim TempPackageOrigin As String = String.Empty
        Dim TempPackageBarcode As String = String.Empty
        Dim TempContainerID As Decimal
        'Dim TempPackageBarcode As String
        Dim TempRanking As Integer
        Dim TempNote As String = String.Empty

        'Removed the comment pop up for 1.0
        'AddComment(TempNote)

        Try
            Dim OracleDAL As New Oracle_General
            Dim PackageDAL As New Oracle_Package

            If RecipientInfo.RecipientSelected Then
                TempRecipientID = CDbl(RecipientInfo.RecipientID)
                'TempPackageBarcode = txtPackageItemBarcode.Text
            Else
                Try
                    TempRecipientID = OracleDAL.GetRecipientID(GrdViewPendingItems.GetFocusedRowCellValue("EMAIL").ToString())
                    'TempPackageBarcode = GrdViewPendingItems.GetFocusedRowCellValue("BARCODE")
                Catch
                    TempRecipientID = 0
                End Try
            End If
            'TempPackageBarcode = txtPackageBarcode.Text
            TempContainerID = OracleDAL.GetParentID(TempPackageBarcode)
            Try
                Dim ContainerDAL As New Oracle_Container

                TempContainerTypeID = ContainerDAL.GetContainerTypeID(lkPackageContent.EditValue)
                TempPackageOrigin = lkPackageOrigin.EditValue
                'txtPackageBarcode.Text = GrdViewExistingPackages.GetFocusedRowCellValue("BARCODE")

                'TempPackageBarcode = txtPackageBarcode.Text

                'TempContainerID = GetParentID(TempPackageBarcode)
                'TempContainerID = TempParentID
                TempRanking = PackageDAL.GetPackageItemCount(TempPackageBarcode) + 1
                'Phase ID = 50003 (Container Packed)
            Catch
                'Do nothing 
            End Try
            'Package Barcode, Container Barcode, Request, Recipient ID
            PackageDAL.UpdatePackage(TempContainerID, TempContainerTypeID, TempPackageOrigin, TempPackageBarcode, TempRanking, 50003, TempNote, TempRecipientID)

            RefreshCreatePackageDatagrids()
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Create package in SMF_SHIPPING
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>


    Private Sub btn_LookupScan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_LookupScan.Click
        Dim TempItemBarcode As String = String.Empty
        Dim CurrentRow As Integer = -1

        Dim ContainerDAL As New Oracle_Container


        If Not String.IsNullOrEmpty(txtPackageItemBarcode.Text) Then
            TempItemBarcode = txtPackageItemBarcode.Text.ToUpper()

            GrdViewPendingItems.ClearColumnsFilter()
            GrdViewPendingItems.ClearSorting()
            GrdViewPendingItems.ClearGrouping()

            Dim TempLookupList As ArrayList = New ArrayList
            TempLookupList.Add(TempItemBarcode)
            ContainerDAL.ShowScannedItems(TempLookupList)
            'ShowPendingItems(m_frmPackageValidationRules)
            m_bHasSelectedItem = True

            If GrdViewPendingItems.RowCount <= 0 Then
                ShowMessage("Item " + TempItemBarcode + " not found in pending items")
                Dim TempFulfillmentSite As String = lkFulfillmentSite.EditValue
                ContainerDAL.ShowPendingItems(m_frmPackageValidationRules, TempFulfillmentSite)
                txtPackageItemBarcode.Text = ""
                txtPackageItemBarcode.Focus()
            Else
                GrdViewPendingItems.SelectAll()
                AddCreatePackage()
            End If
        Else
            ShowMessage("Please enter an item barcode")
        End If
    End Sub

    Private Sub btnFinalizePackage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalizePackage.Click
        Dim TempPackageID As Decimal
        Dim TempPackageBarcode As String
        Dim TempPackageContainers As Containers

        Try
            Dim ChronosUpdateSuccess As Boolean = False

            Dim PackageDAL As New Oracle_Package
            Dim ContainerDAL As New Oracle_Container

            TempPackageID = GrdViewExistingPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")
            TempPackageBarcode = GrdViewExistingPackages.GetFocusedRowCellValue("BARCODE")
            PackageDAL.FinalizePackage(TempPackageID)
            TempPackageContainers = New Containers(ContainerDAL.GetContainersByParentContainerID(TempPackageID))
            'BulkChronosRemark(TempPackageContainers, UserSite, String.Format("Package {0} Finalized", TempPackageBarcode))

            Dim LabelFields As Dictionary(Of String, String) = ExistingPackageLabelFields()
            PrintLabel(LabelFields)
            ChronosUpdateSuccess = ChronosFinalizePackage(TempPackageContainers, TempPackageBarcode)

            ' Dim updateTask As Task(Of Boolean) = CallChronosAsnyc(TempPackageContainers, TempPackageBarcode)

            If Not ChronosUpdateSuccess Then
                ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
            End If

            RefreshCreatePackageDatagrids(TempPackageID)
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub
    Private Function CallChronosAsnyc(ByVal PackageContainers As Containers, ByVal PackageBarcode As String) As Task(Of Boolean)

        Return Task.Factory.StartNew(Of Boolean)(
          Function()
              Dim ChronosUpdateSuccess As Boolean = ChronosFinalizePackage(PackageContainers, PackageBarcode)

              If Not ChronosUpdateSuccess Then
                  ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
              End If

              Return ChronosUpdateSuccess
          End Function)
    End Function

    Private Function ExistingPackageLabelFields() As Dictionary(Of String, String)
        Dim MyFields As New Dictionary(Of String, String)

        MyFields.Clear()
        MyFields.Add("sBarcode", GrdViewExistingPackages.GetFocusedRowCellValue("BARCODE").ToString())
        MyFields.Add("sRequestor", GrdViewExistingPackages.GetFocusedRowCellValue("FIRST_NAME").ToString() + " " + GrdViewExistingPackages.GetFocusedRowCellValue("LAST_NAME").ToString())
        MyFields.Add("sDestAddress", GrdViewExistingPackages.GetFocusedRowCellValue("MAIL_ADDRESS").ToString())
        MyFields.Add("sDestination", GrdViewExistingPackages.GetFocusedRowCellValue("CITY").ToString() + "," + GrdViewExistingPackages.GetFocusedRowCellValue("COUNTRY_NAME").ToString())
        MyFields.Add("sDate", GrdViewExistingPackages.GetFocusedRowCellValue("DATE_MODIFIED"))

        Dim TempDescription As String = String.Empty
        If Not String.IsNullOrEmpty(GrdViewActivePackage.GetRowCellValue(0, "PROTOCOL_NAME").ToString) Then
            Dim TempProtocol As String = GrdViewActivePackage.GetRowCellValue(0, "PROTOCOL_NAME").ToString()
            If TempProtocol.Length > 20 Then
                TempProtocol = TempProtocol.Remove(20)
            End If
            TempDescription = "Protocol: " & TempProtocol


        End If

        MyFields.Add("sDescription", TempDescription)

        Dim TempRequestorGroupName As String = String.Empty
        If Not String.IsNullOrEmpty(GrdViewActivePackage.GetRowCellValue(0, "DESTINATION_RACK_GROUP_NAME").ToString) Then
            TempRequestorGroupName = GrdViewActivePackage.GetRowCellValue(0, "DESTINATION_RACK_GROUP_NAME").ToString()
        End If

        MyFields.Add("sDestRack", TempRequestorGroupName)
        MyFields.Add("sQuantity", GrdViewExistingPackages.GetFocusedRowCellValue("CONTAINER_COUNT").ToString() + " Items")
        MyFields.Add("sSite", UserSiteCode) 'GrdViewExistingPackages.GetFocusedRowCellValue("SITE")

        Return MyFields
    End Function



    Private Sub PrintLabel(ByVal LabelFields As Dictionary(Of String, String))

        Try

            'Fields - sBarcode, sRequestor, sDestAddress, sDestination, sDate, sDescription, sQuantity, sSite
            Dim m_LabelTemplatePath As String = Path.Combine(System.Windows.Forms.Application.StartupPath, "Resources\GSTTemplates\Labels\") 'Configuration.LabelTemplatePath

            Dim m_Labeler = New LabelerUtility.cLabeler(m_LabelTemplatePath)
            Dim m_Printer As String = cboPrinter.Text
            Dim PrintReturnCode As Integer
            Select Case m_Printer
                Case "Zebra"
                    Dim m_PrinterDPI As String = cboPrinterDPI.Text
                    If String.IsNullOrEmpty(txtPrinterPort.Text) Then
                        ShowMessage("Please select the printer port in the settings and retry")
                    Else
                        Dim m_PrinterPort As New Ports.SerialPort() With {.PortName = "COM" + txtPrinterPort.Text}
                        PrintReturnCode = m_Labeler.PrintLabel(m_Printer, String.Format("GSTContainerLabel_V1_{0}.zlb", m_PrinterDPI), LabelFields, m_PrinterPort)
                    End If
                Case "Brother"
                    PrintReturnCode = m_Labeler.PrintLabel(m_Printer, "GSTContainer.lbx", LabelFields)
                Case Else
                    ShowMessage("Please select a printer in the settings and retry")
            End Select
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub PrintShipmentInteriorLabel()

        Dim MyFields As New Dictionary(Of String, String)
        Try
            MyFields.Clear()
            MyFields.Add("sDestination", grdViewExistingShipments.GetFocusedRowCellValue("CITY").ToString() + "," + grdViewExistingShipments.GetFocusedRowCellValue("COUNTRY_NAME").ToString())
            MyFields.Add("sBarcode", grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_NAME").ToString())
            MyFields.Add("sDate", Date.Now.ToString())
            MyFields.Add("sSite", UserSiteCode)

            'Fields - sBarcode, sRequestor, sDestAddress, sDestination, sDate, sDescription, sQuantity, sSite
            Dim m_LabelTemplatePath As String = Path.Combine(System.Windows.Forms.Application.StartupPath, "Resources\GSTTemplates\Labels\") 'Configuration.LabelTemplatePath

            Dim m_Labeler = New LabelerUtility.cLabeler(m_LabelTemplatePath)
            Dim m_Printer As String = cboPrinter.Text
            Dim PrintReturnCode As Integer
            Select Case m_Printer
                Case "Zebra"
                    Dim m_PrinterDPI As String = cboPrinterDPI.Text
                    If String.IsNullOrEmpty(txtPrinterPort.Text) Then
                        ShowMessage("Please select the printer port in the settings and retry")
                    Else
                        Dim m_PrinterPort As New Ports.SerialPort() With {.PortName = "COM" + txtPrinterPort.Text}
                        PrintReturnCode = m_Labeler.PrintLabel(m_Printer, String.Format("GSTShipmentIntLabel_V1_{0}.zlb", m_PrinterDPI), MyFields, m_PrinterPort)
                    End If
                Case "Brother"
                    PrintReturnCode = m_Labeler.PrintLabel(m_Printer, "GSTShipmentInt.lbx", MyFields)
                Case Else
                    ShowMessage("Please select a printer in the settings and retry")
            End Select
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub btnRefreshPackageGrids_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshPackageGrids.Click
        RefreshCreatePackageDatagrids()
    End Sub

    Private Sub RemoveActivePackage()
        Dim TempPackageRow As Integer
        Dim TempPackageID As Decimal

        TempPackageRow = GrdViewExistingPackages.GetSelectedRows(0)

        TempPackageID = GrdViewExistingPackages.GetRowCellValue(TempPackageRow, "PARENT_CONTAINER_ID")  'GetRowCellValue(TempPackageRow, "PARENT_CONTAINER_ID")
        GrdViewActivePackage.SelectAll()
        RemoveContainersBulk()
        'RemovePackage(TempPackageID)

    End Sub


    Private Sub txtRemoveItem_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRemoveItem.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnRemoveItem.PerformClick()
        End If
    End Sub

    Private Sub btnRemoveItems_Click(sender As System.Object, e As System.EventArgs) Handles btnRemoveItem.Click

        Dim TempItemToRemove As String = txtRemoveItem.Text
        Dim TempPackageRow As Integer
        Dim TempItemRow As Integer

        If String.IsNullOrEmpty(TempItemToRemove) Then
            ShowMessage("Please enter the barcode of the item/package to remove")
        Else
            TempPackageRow = GrdViewExistingPackages.LocateByValue("BARCODE", TempItemToRemove)
            TempItemRow = GrdViewActivePackage.LocateByValue("BARCODE", TempItemToRemove)

            If TempPackageRow >= 0 Then
                'Remove Package
                Try
                    GrdViewExistingPackages.SelectRow(TempPackageRow)
                    RemoveActivePackage()
                Catch ex As Exception
                    ShowMessage("Error Deleting Package - " & ex.Message)
                End Try
                txtRemoveItem.Text = String.Empty
                txtRemoveItem.Focus()
            ElseIf TempItemRow >= 0 Then
                'Remove Row
                Try
                    GrdViewActivePackage.ClearSelection()
                    GrdViewActivePackage.SelectRow(TempItemRow)
                    RemoveContainersBulk()
                Catch ex As Exception
                    ShowMessage("Error Deleting Package - " & ex.Message)
                End Try
                '                RefreshCreatePackageDatagrids()
                txtRemoveItem.Text = String.Empty
                txtRemoveItem.Focus()

            Else
                ShowMessage("Barcode not associated with existing package or package items")
            End If
        End If

    End Sub

    Private Sub ActivePackageDetailsToolTip_GetActiveObjectInfo(sender As System.Object, e As DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs) Handles ActivePackageDetailsToolTip.GetActiveObjectInfo
        If Not e.SelectedControl Is grdActivePackageDetails Then Return

        Dim info As DevExpress.Utils.ToolTipControlInfo = Nothing
        'Get the view at the current mouse position
        Dim view As GridView = grdActivePackageDetails.GetViewAt(e.ControlMousePosition)
        If view Is Nothing Then Return
        'Get the view's element information that resides at the current position
        Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = view.CalcHitInfo(e.ControlMousePosition)
        'Display a hint for row indicator cells
        If hi.HitTest = DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.RowIndicator Then
            'An object that uniquely identifies a row indicator cell
            Dim o As Object = hi.HitTest.ToString() + hi.RowHandle.ToString()
            Dim row As Integer = hi.RowHandle + 1
            Dim text As String = row.ToString()
            info = New DevExpress.Utils.ToolTipControlInfo(o, text)
        End If
        'Supply tooltip information if applicable, otherwise preserve default tooltip (if any)
        If Not info Is Nothing Then e.Info = info
    End Sub


    Private Sub FinalPackageToolTip_GetActiveObjectInfo(sender As System.Object, e As DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs) Handles FinalPackageToolTip.GetActiveObjectInfo
        If Not e.SelectedControl Is grdFinalPackageDetails Then Return

        Dim info As DevExpress.Utils.ToolTipControlInfo = Nothing
        'Get the view at the current mouse position
        Dim view As GridView = grdFinalPackageDetails.GetViewAt(e.ControlMousePosition)
        If view Is Nothing Then Return
        'Get the view's element information that resides at the current position
        Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = view.CalcHitInfo(e.ControlMousePosition)
        'Display a hint for row indicator cells
        If hi.HitTest = DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.RowIndicator Then
            'An object that uniquely identifies a row indicator cell
            Dim o As Object = hi.HitTest.ToString() + hi.RowHandle.ToString()
            Dim row As Integer = hi.RowHandle + 1
            Dim text As String = row.ToString()
            info = New DevExpress.Utils.ToolTipControlInfo(o, text)
        End If
        'Supply tooltip information if applicable, otherwise preserve default tooltip (if any)
        If Not info Is Nothing Then e.Info = info
    End Sub

    Private Sub ActiveShipmentToolTip_GetActiveObjectInfo(sender As System.Object, e As DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs) Handles ActiveShipmentToolTip.GetActiveObjectInfo
        If Not e.SelectedControl Is grdActiveShipmentDetails Then Return

        Dim info As DevExpress.Utils.ToolTipControlInfo = Nothing
        'Get the view at the current mouse position
        Dim view As GridView = grdActiveShipmentDetails.GetViewAt(e.ControlMousePosition)
        If view Is Nothing Then Return
        'Get the view's element information that resides at the current position
        Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = view.CalcHitInfo(e.ControlMousePosition)
        'Display a hint for row indicator cells
        If hi.HitTest = DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.RowIndicator Then
            'An object that uniquely identifies a row indicator cell
            Dim o As Object = hi.HitTest.ToString() + hi.RowHandle.ToString()
            Dim row As Integer = hi.RowHandle + 1
            Dim text As String = row.ToString()
            info = New DevExpress.Utils.ToolTipControlInfo(o, text)
        End If
        'Supply tooltip information if applicable, otherwise preserve default tooltip (if any)
        If Not info Is Nothing Then e.Info = info
    End Sub

#End Region

#Region "           Package Maintenace Tab          "

    Private Sub lkFinPkgTimeline_Closed(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ClosedEventArgs) Handles lkFinPkgTimeline.Closed
        RefreshPackageMaintenanceDatagrids()
    End Sub

    Private Sub RefreshPackageMaintenanceDatagrids()
        Try
            Dim OracleDAL As New Oracle_General
            Dim PackageDAL As New Oracle_Package
            Dim ContainerDAL As New Oracle_Container
            'Dim TempWorkstation As String = System.Environment.MachineName

            Cursor.Current = Cursors.WaitCursor

            OracleDAL.GetPackageTimeframes()

            m_bHasSelectedItem = False
            'Store the current selected package
            PackageDAL.ShowFinalizedPackages()
            Dim TempParentID As Decimal = GrdViewFinalizedPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")

            PackageDAL.UpdateFinalPackageDetails(TempParentID)

            txtFnlPackageBarcode.Text = String.Empty
            txtFnlPackageBarcode.Focus()

            txtDeliverPackageBarcode.Text = String.Empty

            txtReprintPackageLabel.Text = String.Empty

            Cursor.Current = Cursors.Default
        Catch err As Exception
            Cursor.Current = Cursors.Default
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Sub btnUnfinalizePackage_Click(sender As System.Object, e As System.EventArgs) Handles btnUnfinalizePackage.Click

        Dim TempPackageToRemove As String = txtFnlPackageBarcode.Text.ToUpper()
        Dim TempPackageRow As Integer
        Dim TempPackageID As Decimal
        Dim TempWorkstation As String = System.Environment.MachineName
        Dim TempPackageContainers As Containers

        If String.IsNullOrEmpty(TempPackageToRemove) Then
            ShowMessage("Please enter the barcode of the package to unfinalize")
        Else
            TempPackageRow = GrdViewFinalizedPackages.LocateByValue("BARCODE", TempPackageToRemove)

            If TempPackageRow >= 0 Then
                Try
                    Dim PackageDAL As New Oracle_Package
                    Dim ContainerDAL As New Oracle_Container
                    Dim ChronosUpdateSuccess As Boolean = False

                    If PackageDAL.GetNumberOfCreatedPhasePackages(TempWorkstation) = 0 Then
                        TempPackageID = GrdViewFinalizedPackages.GetRowCellValue(TempPackageRow, "PARENT_CONTAINER_ID")
                        PackageDAL.UnfinalizePackage(TempPackageID, TempWorkstation)
                        TempPackageContainers = New Containers(ContainerDAL.GetContainersByParentContainerID(TempPackageID))
                        ChronosUpdateSuccess = ChronosFinalizePackage(TempPackageContainers, " ") 'This will put a space in the package Barcode field

                        If Not ChronosUpdateSuccess Then
                            ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
                        End If

                        DisplayCreatePackageTab()
                    Else
                        ShowMessage("Only one package can be unfinalized at a time")
                    End If
                    'RefreshPackageMaintenanceDatagrids()

                Catch ex As Exception
                    ShowMessage("Error Unfinalizing Package - " & ex.Message)
                End Try
            Else
                ShowMessage("Barcode not associated with an available package")
            End If
        End If

    End Sub

    'OnClick Event to load details

    Private Sub GrdViewFinalizedPackages_FocusedRowChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GrdViewFinalizedPackages.FocusedRowChanged

        Try
            Dim PackageDAL As New Oracle_Package
            Dim ContainerDAL As New Oracle_Container
            Dim TempParentID As Decimal

            TempParentID = GrdViewFinalizedPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")
            PackageDAL.UpdateFinalPackageDetails(TempParentID)
        Catch err As Exception
            ShowMessage(err.Message)
        End Try

    End Sub

    Private Sub txtFnlPackageBarcode_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtFnlPackageBarcode.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            'GetTRTContainerInfo(txtPackageItemBarcode.Text
            btnUnfinalizePackage.PerformClick()
        End If
    End Sub

    Private Sub menuFinalizedPackagesRightClick_ItemClicked(sender As System.Object, e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles menuFinalizedPackagesRightClick.ItemClicked
        If Not menuFinalizedPackagesRightClick.SourceControl Is Nothing Then
            If menuFinalizedPackagesRightClick.SourceControl.Name Is "grdFinalizedPackages" Then
                If e.ClickedItem.Text = "Print Label" Then
                    PrintFinalizedPackageLabel()
                Else
                    ShowMessage("Function Not Available")
                End If
            End If
        End If
    End Sub

    Private Sub PrintFinalizedPackageLabel()
        Try
            Dim LabelFields As Dictionary(Of String, String) = FinalizedPackageLabelFields()
            PrintLabel(LabelFields)
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Function FinalizedPackageLabelFields() As Dictionary(Of String, String)
        Dim MyFields As New Dictionary(Of String, String)

        MyFields.Clear()
        MyFields.Add("sBarcode", GrdViewFinalizedPackages.GetFocusedRowCellValue("BARCODE").ToString())
        MyFields.Add("sRequestor", GrdViewFinalizedPackages.GetFocusedRowCellValue("FIRST_NAME").ToString() + " " + GrdViewFinalizedPackages.GetFocusedRowCellValue("LAST_NAME").ToString())
        MyFields.Add("sDestAddress", GrdViewFinalizedPackages.GetFocusedRowCellValue("MAIL_ADDRESS").ToString())
        MyFields.Add("sDestination", GrdViewFinalizedPackages.GetFocusedRowCellValue("CITY").ToString() + "," + GrdViewFinalizedPackages.GetFocusedRowCellValue("COUNTRY_NAME").ToString())
        MyFields.Add("sDate", GrdViewFinalizedPackages.GetFocusedRowCellValue("DATE_MODIFIED"))

        Dim TempDescription As String = String.Empty
        If Not String.IsNullOrEmpty(GrdViewFinalPackage.GetRowCellValue(0, "PROTOCOL_NAME").ToString) Then
            Dim TempProtocol As String = GrdViewFinalPackage.GetRowCellValue(0, "PROTOCOL_NAME").ToString()
            If TempProtocol.Length > 20 Then
                TempProtocol = TempProtocol.Remove(20)
            End If
            TempDescription = TempProtocol


        End If

        MyFields.Add("sDescription", TempDescription)

        Dim TempRequestorGroupName As String = String.Empty
        If Not String.IsNullOrEmpty(GrdViewFinalPackage.GetRowCellValue(0, "DESTINATION_RACK_GROUP_NAME").ToString) Then
            TempRequestorGroupName = GrdViewFinalPackage.GetRowCellValue(0, "DESTINATION_RACK_GROUP_NAME").ToString()
        End If

        MyFields.Add("sDestRack", TempRequestorGroupName)
        MyFields.Add("sQuantity", GrdViewFinalizedPackages.GetFocusedRowCellValue("CONTAINER_COUNT").ToString() + " Items")
        MyFields.Add("sSite", UserSiteCode) 'GrdViewExistingPackages.GetFocusedRowCellValue("SITE")

        Return MyFields
    End Function

    Private Sub DeliverPackage()
        Try
            Dim TempPackageID As Decimal
            Dim PackageDAL As New Oracle_Package

            TempPackageID = GrdViewFinalizedPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")
            PackageDAL.DeliverPackage(TempPackageID)

            RefreshPackageMaintenanceDatagrids()
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Sub btnDeliverPackage_Click(sender As System.Object, e As System.EventArgs) Handles btnDeliverPackage.Click

        Dim TempPackageToRemove As String = txtDeliverPackageBarcode.Text.ToUpper()
        Dim TempPackageRow As Integer
        Dim TempPackageID As Decimal

        If String.IsNullOrEmpty(TempPackageToRemove) Then
            ShowMessage("Please enter the barcode of the package to unfinalize")
        Else
            TempPackageRow = GrdViewFinalizedPackages.LocateByValue("BARCODE", TempPackageToRemove)

            If TempPackageRow >= 0 Then
                Try
                    Dim PackageDAL As New Oracle_Package
                    TempPackageID = GrdViewFinalizedPackages.GetRowCellValue(TempPackageRow, "PARENT_CONTAINER_ID")
                    PackageDAL.DeliverPackage(TempPackageID)
                    RefreshPackageMaintenanceDatagrids()

                Catch ex As Exception
                    ShowMessage("Error Delivering Package - " & ex.Message)
                End Try
            Else
                ShowMessage("Barcode not associated with an available package")
            End If
        End If

    End Sub

    Private Sub btnReprintPackageLabel_Click(sender As System.Object, e As System.EventArgs) Handles btnReprintPackageLabel.Click

        Dim TempPackageToPrint As String = txtReprintPackageLabel.Text.ToUpper

        If String.IsNullOrEmpty(TempPackageToPrint) Then
            ShowMessage("Please enter a barcode to reprint label")
        Else
            Dim PackageDAL As New Oracle_Package
            PackageDAL.ShowPackageInFinalizedPackageGrid(TempPackageToPrint)

            Dim TempParentID As Decimal = GrdViewFinalizedPackages.GetFocusedRowCellValue("PARENT_CONTAINER_ID")
            If TempParentID > 0 Then
                PackageDAL.UpdateFinalPackageDetails(TempParentID)
                PrintFinalizedPackageLabel()
            Else
                ShowMessage("Cannot find a package matching that barcode")
            End If
        End If

        RefreshPackageMaintenanceDatagrids()

    End Sub

    Private Sub txtReprintPackageLabel_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtReprintPackageLabel.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnReprintPackageLabel.PerformClick()
        End If
    End Sub
#End Region

#Region "           Create Shipment Tab        "

    Private Sub GrdViewPendingPackages_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        'DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = True

        'Dim test As Boolean = False
        If (Not GrdViewPendingPackages.ActiveFilter.IsEmpty) AndAlso e.Y > 99 Then
            'If there is a filter, allow clicking
            DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = False
        ElseIf e.Y < 20 Then
            'Allow clicking on the headers
            DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = False
        Else
            'Don't allow selecting data rows
            DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = True
        End If

    End Sub

    Private Sub RemovePackages()
        'RemovePackageFromShipment()
        Dim TempShipmentID As Decimal
        Dim TempParentContainerID As Decimal
        Dim i As Integer
        Dim rowindex As Array

        Try
            Dim ShipmentDAL As New Oracle_Shipment
            Dim PackageDAL As New Oracle_Package
            'Remove items from selected rows
            rowindex = GrdViewActiveShipment.GetSelectedRows
            For i = 0 To rowindex.Length - 1

                'TempContainerID = GetContainerID(grdViewExistingShipments.GetRowCellValue(rowindex(i), "SHIPMENT_NAME"))
                'Assign Container ID as Parent ID to remove it from the current Parent
                'TempParentID = TempContainerID
                TempShipmentID = ShipmentDAL.GetShipmentID(grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_NAME"))
                TempParentContainerID = GrdViewActiveShipment.GetRowCellValue(rowindex(i), "PARENT_CONTAINER_ID")

                'Phase ID = 50002 (Container Arrived) => update phase from Container Packed to Arrived (5000350002)
                'UpdateContainers => ContainerID, ParentID, TypeID, Origin, Barcode, Ranking, PhaseID, Note, ReceiverID
                PackageDAL.RemovePackageFromShipment(TempShipmentID, TempParentContainerID)
            Next
            RefreshCreateShipmentDatagrids()
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Sub RemoveShipment()
        Dim TempShipmentName As String
        Dim TempPackageCount As Decimal

        Try
            Dim ShipmentDAL As New Oracle_Shipment
            TempShipmentName = grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_NAME").ToString()
            TempPackageCount = grdViewExistingShipments.GetFocusedRowCellValue("PACKAGE_COUNT")

            If TempPackageCount <= 0 Then
                ShipmentDAL.RemoveShipment(TempShipmentName)
                RefreshCreateShipmentDatagrids()

            Else
                ShowMessage("Remove Packages before deleting a shipment")
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub RefreshCreateShipmentDatagrids(Optional ByVal NewShipmentID As Long = -1)
        Try
            Dim ShipmentDAL As New Oracle_Shipment
            Dim PackageDAL As New Oracle_Package

            Cursor.Current = Cursors.WaitCursor

            m_bHasSelectedPackage = False

            Dim TempSelectedShipment As Boolean = False
            Dim TempPreRefreshShipmentID As Long '= grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_ID")

            If NewShipmentID > 0 Then
                TempPreRefreshShipmentID = CType(NewShipmentID, Long)
            Else
                TempPreRefreshShipmentID = grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_ID")
            End If

            grdExistingShipments.DataSource = Nothing
            grdActiveShipmentDetails.DataSource = Nothing

            'Show pending/available packages that are not being added into a shipment
            'Show existing shipments
            ShipmentDAL.ShowExistingShipments()


            If TempPreRefreshShipmentID > 0 Then
                Dim TempNewRow As Integer = grdViewExistingShipments.LocateByValue("SHIPMENT_ID", TempPreRefreshShipmentID)
                If TempNewRow >= 0 Then
                    grdViewExistingShipments.FocusedRowHandle = TempNewRow
                    TempSelectedShipment = True
                End If
            End If

            'grdViewExistingShipments.SelectRow(0)

            ShipmentDAL.UpdateActiveShipmentDetails(grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_ID"))

            SetShipmentValidationRules()
            'ShowPendingPackages(m_frmShipmentValidationRules)
            PackageDAL.ShowPendingPackages(m_frmShipmentValidationRules, True)

            txtShipmentPackageBarcode.Text = ""
            txtShipmentPackageBarcode.Focus()
            Cursor.Current = Cursors.Default
        Catch err As Exception
            Cursor.Current = Cursors.Default
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Sub RefreshCreateShipmentDropdowns()
        'If Not m_bIsCreateShipmentInitialized Then
        Try

            Dim TempCarrier As String = String.Empty
            Dim TempPriority As String = String.Empty

            If grdViewExistingShipments.RowCount > 0 Then
                TempCarrier = grdViewExistingShipments.GetRowCellValue(0, "EXTERNAL_REFERENCE_TYPE_NAME").ToString()
                TempPriority = grdViewExistingShipments.GetRowCellValue(0, "PRIORITY").ToString()
            End If

            Dim ShippingDAL As New Oracle_Shipment
            'Get available shipping carriers from SMF_SHIPPING
            ShippingDAL.GetShippingCarrier(TempCarrier)
            'Get available shipping priorities from SMF_SHIPPING



            ShippingDAL.GetShippingPriority(TempPriority)
            'Get available shipmet types from SMF_SHIPPING
            ShippingDAL.GetShipmentType()
            'Get available filling material from SMF_SHIPPING
            ShippingDAL.GetFillingMaterial()
            'm_bIsCreateShipmentInitialized = True
        Catch err As Exception
            ShowMessage(err.Message)
            'm_bIsCreateShipmentInitialized = False
        End Try
        'End If
    End Sub

    Private Function ValidateShipmentPackages(ByVal PendingPackageRow As Integer) As ValidationResult
        Dim CurrentValidationResult As New ValidationResult()

        Dim IsSameSite As Boolean = True
        Dim IsSameDestination As Boolean = True

        Dim ValidationMessageStub As String = String.Format("Cannot add package {0} to shipment {1}", GrdViewPendingPackages.GetRowCellValue(PendingPackageRow, "BARCODE"), GrdViewActiveShipment.GetRowCellValue(0, "BARCODE"))

        If m_frmShipmentValidationRules.Site Then
            'Dim TempValidationSite As String = m_frmShipmentValidationRules.SiteValue
            Dim TempValidationCountry As String = m_frmShipmentValidationRules.CountryValue
            Dim TempValidationCity As String = m_frmShipmentValidationRules.CityValue
            Dim TempValidationCompany As String = m_frmShipmentValidationRules.CompanyValue
            'Dim TempItemSite As String = String.Empty

            Dim TempItemCountry As String = String.Empty
            Dim TempItemCity As String = String.Empty
            Dim TempItemCompany As String = String.Empty

            If (Not String.IsNullOrEmpty(TempValidationCountry) AndAlso Not String.IsNullOrEmpty(TempValidationCity)) OrElse Not String.IsNullOrEmpty(TempValidationCompany) Then
                If Not String.IsNullOrEmpty(GrdViewPendingPackages.GetRowCellValue(PendingPackageRow, m_frmShipmentValidationRules.CountryColumn).ToString) Then
                    TempItemCountry = GrdViewPendingPackages.GetRowCellValue(PendingPackageRow, m_frmShipmentValidationRules.CountryColumn).ToString
                End If


                If Not String.IsNullOrEmpty(GrdViewPendingPackages.GetRowCellValue(PendingPackageRow, m_frmShipmentValidationRules.CityColumn).ToString) Then

                    Dim TempCityValue As String = GrdViewPendingPackages.GetRowCellValue(PendingPackageRow, m_frmShipmentValidationRules.CityColumn).ToString.ToUpper
                    Dim TempCityValues As String() = TempCityValue.Split(",")

                    If TempCityValues.Length > 0 Then
                        TempItemCity = TempCityValues(0).Trim()
                    Else
                        TempItemCity = TempCityValue.Trim()
                    End If
                End If

                If Not String.IsNullOrEmpty(GrdViewPendingPackages.GetRowCellValue(PendingPackageRow, m_frmShipmentValidationRules.CompanyColumn).ToString) Then
                    TempItemCompany = GrdViewPendingPackages.GetRowCellValue(PendingPackageRow, m_frmShipmentValidationRules.CompanyColumn).ToString
                End If

                If String.Compare(TempValidationCity, TempItemCity, True) = 0 AndAlso String.Compare(TempValidationCountry, TempItemCountry, True) = 0 AndAlso String.Compare(TempValidationCompany, TempItemCompany, True) = 0 Then
                    IsSameSite = True
                Else
                    IsSameSite = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails company/site validation")
                End If
            End If
        End If

        If m_frmShipmentValidationRules.Destination Then
            Dim TempValidationDestination As String = m_frmShipmentValidationRules.DestinationValue
            Dim TempItemDestination As String = String.Empty

            If Not String.IsNullOrEmpty(TempValidationDestination) Then
                If Not String.IsNullOrEmpty(GrdViewPendingPackages.GetRowCellValue(PendingPackageRow, m_frmShipmentValidationRules.DestinationColumn)) Then
                    TempItemDestination = GrdViewPendingPackages.GetRowCellValue(PendingPackageRow, m_frmShipmentValidationRules.DestinationColumn)
                End If

                If String.Compare(TempValidationDestination, TempItemDestination, True) = 0 Then
                    IsSameDestination = True
                Else
                    IsSameDestination = False
                    CurrentValidationResult.ValidationMessages.Add(ValidationMessageStub & " because it fails destination validation")
                End If
            End If
        End If

        CurrentValidationResult.IsValid = IsSameSite AndAlso IsSameDestination

        Return CurrentValidationResult
    End Function

    Private Sub ChkLstShipmentValidationRules_ItemCheck(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ItemCheckEventArgs) Handles ChkLstShipmentValidationRules.ItemCheck
        Dim PackageDAL As New Oracle_Package
        Dim ShipmentDAL As New Oracle_Shipment


        Dim TempShipmentID As Decimal
        SetShipmentValidationRules()
        'ShowPendingPackages(m_frmShipmentValidationRules)
        PackageDAL.ShowPendingPackages(m_frmShipmentValidationRules, True)
        TempShipmentID = grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_ID")
        ShipmentDAL.UpdateActiveShipmentDetails(TempShipmentID)
        m_bHasSelectedPackage = False
    End Sub

    Private Sub SetNewShipmentValidationRules(ByVal ShipmentID As Long)

        Dim ShipmentDAL As New Oracle_Shipment
        ShipmentDAL.ShowExistingShipments()


        Dim TempNewRow As Integer = grdViewExistingShipments.LocateByValue("SHIPMENT_ID", ShipmentID)

        If TempNewRow >= 0 Then
            grdViewExistingShipments.FocusedRowHandle = TempNewRow
        End If


        SetShipmentValidationRules()

    End Sub

    Private Sub SetShipmentValidationRules()

        m_frmShipmentValidationRules.Site = ChkLstShipmentValidationRules.Items("Same Company Same Site").CheckState
        m_frmShipmentValidationRules.Destination = ChkLstShipmentValidationRules.Items("Same Destination").CheckState

        m_frmShipmentValidationRules.HasBase = GrdViewActiveShipment.RowCount > 0

        If m_frmShipmentValidationRules.Site AndAlso grdViewExistingShipments.RowCount > 0 Then
            'No longer looking at site code
            'If Not grdViewExistingShipments.GetFocusedRowCellValue(m_frmShipmentValidationRules.SiteColumn) Is Nothing Then
            '    m_frmShipmentValidationRules.SiteValue = grdViewExistingShipments.GetFocusedRowCellValue("m_frmShipmentValidationRules.SiteColumn")
            'Else
            '    m_frmShipmentValidationRules.SiteValue = String.Empty
            'End If

            If Not grdViewExistingShipments.GetFocusedRowCellValue(m_frmShipmentValidationRules.CompanyColumn) Is Nothing Then
                m_frmShipmentValidationRules.CompanyValue = grdViewExistingShipments.GetFocusedRowCellValue(m_frmShipmentValidationRules.CompanyColumn).ToString
            Else
                m_frmShipmentValidationRules.CompanyValue = String.Empty
            End If

            If Not grdViewExistingShipments.GetFocusedRowCellValue(m_frmShipmentValidationRules.CountryColumn) Is Nothing Then
                m_frmShipmentValidationRules.CountryValue = grdViewExistingShipments.GetFocusedRowCellValue(m_frmShipmentValidationRules.CountryColumn).ToString
            Else
                m_frmShipmentValidationRules.CountryValue = String.Empty
            End If

            If Not grdViewExistingShipments.GetFocusedRowCellValue(m_frmShipmentValidationRules.CityColumn) Is Nothing Then
                Dim TempCityValue As String = grdViewExistingShipments.GetFocusedRowCellValue(m_frmShipmentValidationRules.CityColumn).ToString.ToUpper
                Dim TempCityValues As String() = TempCityValue.Split(",")

                If TempCityValues.Length > 0 Then
                    m_frmShipmentValidationRules.CityValue = TempCityValues(0).Trim()
                Else
                    m_frmShipmentValidationRules.CityValue = TempCityValue.Trim()
                End If
            Else
                m_frmShipmentValidationRules.CityValue = String.Empty
            End If



        Else
            'm_frmShipmentValidationRules.SiteValue = String.Empty
            m_frmShipmentValidationRules.CompanyValue = String.Empty
            m_frmShipmentValidationRules.CountryValue = String.Empty
            m_frmShipmentValidationRules.CityValue = String.Empty
        End If

        If m_frmShipmentValidationRules.Destination AndAlso GrdViewActiveShipment.RowCount > 0 Then
            m_frmShipmentValidationRules.DestinationValue = GrdViewActiveShipment.GetRowCellValue(0, m_frmShipmentValidationRules.DestinationColumn).ToString()
        Else
            m_frmShipmentValidationRules.DestinationValue = String.Empty
        End If

    End Sub

    Private Sub GrdViewActiveShipment_CustomDrawCell(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs) Handles GrdViewActiveShipment.CustomDrawCell
        Dim View As DevExpress.XtraGrid.Views.Grid.GridView = sender
        Dim IsValid As Boolean = True

        If (e.RowHandle >= 0) Then
            Dim TempCompanyValue As String = String.Empty
            Dim TempCountryValue As String = String.Empty
            Dim TempCityValue As String = String.Empty

            Dim TempDestinationValue As String = String.Empty


            If Not View.GetRowCellValue(e.RowHandle, m_frmShipmentValidationRules.CompanyColumn) Is Nothing Then
                TempCompanyValue = View.GetRowCellValue(e.RowHandle, m_frmShipmentValidationRules.CompanyColumn).ToString()
            End If

            If Not View.GetRowCellValue(e.RowHandle, m_frmShipmentValidationRules.CountryColumn) Is Nothing Then
                TempCountryValue = View.GetRowCellValue(e.RowHandle, m_frmShipmentValidationRules.CountryColumn).ToString()
            End If

            If Not View.GetRowCellValue(e.RowHandle, m_frmShipmentValidationRules.CityColumn) Is Nothing Then
                TempCityValue = View.GetRowCellValue(e.RowHandle, m_frmShipmentValidationRules.CityColumn).ToString()

                Dim TempCityValues As String() = TempCityValue.Split(",")

                If TempCityValues.Length > 0 Then
                    TempCityValue = TempCityValues(0).Trim()
                Else
                    TempCityValue = TempCityValue.Trim()
                End If
            End If




            'Site
            If m_frmShipmentValidationRules.Site Then
                Dim MatchCompany As Boolean = (String.Compare(m_frmShipmentValidationRules.CompanyValue, TempCompanyValue, True) = 0)
                Dim MatchCountry As Boolean = (String.Compare(m_frmShipmentValidationRules.CountryValue, TempCountryValue, True) = 0)
                Dim MatchCity As Boolean = (String.Compare(m_frmShipmentValidationRules.CityValue, TempCityValue, True) = 0)


                If Not MatchCompany OrElse Not MatchCountry OrElse Not MatchCity Then
                    e.Appearance.BackColor = Color.Salmon
                    e.Appearance.BackColor2 = Color.SeaShell
                End If
            End If

            'Destination
            If m_frmShipmentValidationRules.Destination Then
                If Not String.IsNullOrEmpty(m_frmShipmentValidationRules.DestinationValue) AndAlso Not m_frmShipmentValidationRules.DestinationValue = View.GetRowCellValue(e.RowHandle, m_frmShipmentValidationRules.DestinationColumn) Then
                    e.Appearance.BackColor = Color.Salmon
                    e.Appearance.BackColor2 = Color.SeaShell
                End If
            End If
        End If
    End Sub

    Private Sub btnCreateShipment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateShipment.Click

        Dim TempShipmentID As Long = CreateShipment(-1)
        RefreshCreateShipmentDatagrids(TempShipmentID)

    End Sub

    Private Function CreateShipment(ByVal ShipmentRecipientID As Double) As Long

        Dim TempShipmentID As Long = -1
        Dim TempShipmentTypeID As Integer
        Dim TempPriorityID As Integer
        Dim TempPhaseID As Integer
        Dim TempNote As String = String.Empty
        Dim TempSenderID As Decimal
        Dim TempRecipientID As Decimal
        Dim TempExtReferenceTypeID As Integer
        Dim TempFillingMaterialID As Integer
        Dim TempWorkstation As String

        Dim ShipmentID As Decimal


        Try
            Dim OracleDAL As New Oracle_General
            Dim ShipmentDAL As New Oracle_Shipment



            RecipientInfo.HighlightedRecipientID = ShipmentRecipientID
            RecipientInfo.RecipientSelected = False
            RecipientInfo.ShowDialog()

            If RecipientInfo.RecipientSelected Then
                TempRecipientID = CDbl(RecipientInfo.RecipientID)

                'Removed the comment pop up for 1.0
                'AddComment(TempNote)

                TempShipmentTypeID = 60000 ' OracleDAL.GetShipmentTypeID(lkShipmentType.EditValue)
                TempPriorityID = OracleDAL.GetPriorityID(lkShippingPriority.EditValue)
                TempPhaseID = 50201 'Package Wrapped phase
                TempSenderID = m_SenderID
                TempExtReferenceTypeID = OracleDAL.GetExtReferenceTypeID(lkShippingCourier.EditValue)
                'TempExtReference = Guid.NewGuid().ToString  'txtShipmentBarcode.Text
                TempFillingMaterialID = 100000 'OracleDAL.GetFillingMaterialID(lkFillingMaterial.EditValue)
                TempWorkstation = System.Environment.MachineName

                ShipmentID = ShipmentDAL.AddShipment(TempShipmentTypeID, TempPriorityID, TempPhaseID, TempNote, TempSenderID, TempRecipientID, TempExtReferenceTypeID, TempFillingMaterialID, TempWorkstation)
                'TempShipmentBarcode = GetParentID(TempContainerBarcode)
                'RefreshCreateShipmentDatagrids(ShipmentID)
            End If
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
        Return ShipmentID
    End Function

    Private Sub btnUpdateShipment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnAddCreateShipment_Click(sender As System.Object, e As System.EventArgs) Handles btnAddPackageShipment.Click

        Dim TempPackageBarcode As String = txtShipmentPackageBarcode.Text.ToUpper()
        Dim CurrentRow As Integer = -1

        GrdViewPendingPackages.ClearColumnsFilter()
        GrdViewPendingPackages.ClearSorting()
        GrdViewPendingPackages.ClearGrouping()

        'Verify the Package exists

        If Not String.IsNullOrEmpty(TempPackageBarcode) Then
            CurrentRow = GrdViewPendingPackages.LocateByValue("BARCODE", TempPackageBarcode)
            If CurrentRow < 0 Then
                ShowMessage("Package not found in pending items")
                m_bHasSelectedPackage = False
            Else
                GrdViewPendingPackages.ClearSelection()
                GrdViewPendingPackages.SelectRow(CurrentRow)
                GrdViewPendingPackages.MakeRowVisible(CurrentRow)
                m_bHasSelectedPackage = True

                AddCreateShipment()
            End If
        Else
            ShowMessage("Please enter a package barcode")
        End If


    End Sub

    Private Sub AddCreateShipment()
        Dim TempShipmentID As Long = -1
        Dim TempNote As String = String.Empty
        Dim TempRowIndex As Integer()

        Dim TempHasPackage As Boolean = GrdViewPendingPackages.SelectedRowsCount > 0
        Dim TempHasShipment As Boolean = grdViewExistingShipments.SelectedRowsCount > 0
        Dim TempCancelled As Boolean = True


        Try

            If m_bHasSelectedPackage AndAlso TempHasPackage Then

                TempRowIndex = GrdViewPendingPackages.GetSelectedRows

                If TempHasShipment Then
                    TempShipmentID = grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_ID")
                Else
                    Dim TempPackageRecipientID As Decimal = GrdViewPendingPackages.GetRowCellValue(GrdViewPendingPackages.GetSelectedRows(0), "RECEIVER_ID")
                    Dim TempPackageSiteCode As String = GrdViewPendingPackages.GetRowCellValue(GrdViewPendingPackages.GetSelectedRows(0), "SITE_CODE").ToString()

                    Dim ShipmentDAL As New Oracle_Shipment
                    Dim TempReceiverID As Decimal = ShipmentDAL.SuggestedShipmentReceiverID(TempPackageRecipientID, TempPackageSiteCode)

                    TempShipmentID = CreateShipment(TempReceiverID)

                    SetNewShipmentValidationRules(TempShipmentID)

                End If

                If TempShipmentID > 0 Then
                    AddPackagesToShipment(TempShipmentID, TempRowIndex)
                Else
                    ShowMessage("No Shipment to add packages")
                End If
                RefreshCreateShipmentDatagrids()
            Else
                ShowMessage("Scan package to add to shipment")
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try



    End Sub

    Private Sub btnLookupPackage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim TempPackageBarcode As String = String.Empty
        Dim CurrentRow As Integer = -1

        If Not String.IsNullOrEmpty(txtShipmentPackageBarcode.Text) Then
            TempPackageBarcode = txtShipmentPackageBarcode.Text
            CurrentRow = GrdViewPendingPackages.LocateByValue("BARCODE", TempPackageBarcode)
            If CurrentRow < 0 Then
                ShowMessage("Package not found in pending items")
                m_bHasSelectedPackage = False
            Else
                GrdViewPendingPackages.ClearSelection()
                GrdViewPendingPackages.SelectRow(CurrentRow)
                GrdViewPendingPackages.MakeRowVisible(CurrentRow)
                m_bHasSelectedPackage = True
            End If
        Else
            ShowMessage("Please enter a package barcode")
        End If

    End Sub


    'Private Sub btnAddPackage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim TempShipmentID As Decimal
    '    Dim TempRowIndex As Integer()


    '    'Add packages from selected rows
    '    Try
    '        Dim ShipmentDAL As New Oracle_Shipment

    '        TempRowIndex = GrdViewPendingPackages.GetSelectedRows

    '        If m_bHasSelectedPackage AndAlso TempRowIndex.Count > 0 Then

    '            TempShipmentID = ShipmentDAL.GetShipmentID(grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_NAME"))
    '            AddPackagesToShipment(TempShipmentID, TempRowIndex)

    '        Else
    '            ShowMessage("Scan packages to add")
    '        End If

    '        RefreshCreateShipmentDatagrids()
    '    Catch err As Exception
    '        ShowMessage(err.Message)
    '    End Try

    'End Sub



    Private Sub AddPackagesToShipment(ByVal ShipmentID As Decimal, ByVal PendingPackageIndex As Integer())

        Dim ShipmentDAL As New Oracle_Shipment
        Dim PackageDAL As New Oracle_Package
        Dim ContainerDAL As New Oracle_Container


        Dim TempNote As String = String.Empty
        Dim TempParentID As Decimal
        Dim TempPackageBarcode As String
        'Dim TempPackageContainers As Containers

        Dim TempShipmentBarcode As String = String.Empty

        Dim TempValidationResult As ValidationResult
        Dim TempValidationErrors As List(Of String) = New List(Of String)

        Dim TempAddedBarcodes As Containers = New Containers()
        Dim TempIsAdded As Boolean

        'Removed the comment pop up for 1.0
        'AddComment(TempNote)

        For Each currentIndex In PendingPackageIndex

            TempIsAdded = False
            TempValidationResult = ValidateShipmentPackages(currentIndex)

            TempParentID = GrdViewPendingPackages.GetRowCellValue(currentIndex, "PARENT_CONTAINER_ID")
            TempPackageBarcode = GrdViewPendingPackages.GetRowCellValue(currentIndex, "BARCODE")

            TempShipmentBarcode = ShipmentDAL.GetShipmentName(ShipmentID)

            If TempValidationResult.IsValid Then
                TempIsAdded = PackageDAL.AddPackageToShipment(ShipmentID, TempParentID)

                If TempIsAdded Then
                    TempAddedBarcodes.AddRange(New Containers(ContainerDAL.GetContainersByParentContainerID(TempParentID)))
                End If
            Else
                Dim ValidationMessage As String = "Barcode " & TempPackageBarcode & " not added because validation failures: " & vbCrLf
                For Each ValidationError As String In TempValidationResult.ValidationMessages
                    ValidationMessage += ValidationError & vbCrLf
                Next
                TempValidationErrors.Add(ValidationMessage)
            End If

            If Not m_frmShipmentValidationRules.HasBase Then
                ShipmentDAL.UpdateActiveShipmentDetails(grdViewExistingShipments.GetRowCellValue(0, "SHIPMENT_ID"))
                SetShipmentValidationRules()
            End If
        Next

        'If TempAddedBarcodes.Count > 0 Then
        '    BulkChronosRemark(TempAddedBarcodes, UserSite, String.Format("Added to Shipment {0}", TempShipmentBarcode))
        'End If

        If TempValidationErrors.Count > 0 Then
            Dim FullMessage As String = String.Empty
            For Each CurrentError As String In TempValidationErrors
                FullMessage += CurrentError & vbCrLf
            Next
            ShowMessage(FullMessage)
        End If

    End Sub

    Private Sub RefreshViewShipmentDropdowns()
        If Not m_bIsViewShipmentInitialized Then
            Try

                Dim GeneralDAL As New Oracle_General
                GeneralDAL.GetSites()

                dtEditToDate.DateTime = DateTime.Now
                dtEditFromDate.DateTime = DateTime.Now.AddDays(1)

                'Dim ShippingDAL As New Oracle_Shipment
                ''Get available shipping carriers from SMF_SHIPPING
                'ShippingDAL.GetShippingCarrier()
                m_bIsViewShipmentInitialized = True
            Catch err As Exception
                ShowMessage(err.Message)
                m_bIsViewShipmentInitialized = False
            End Try
        End If
    End Sub

    Private Sub btnFinalizeShipment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalizeShipment.Click
        Try
            FinalizeShipment()
            RefreshCreateShipmentDatagrids()
        Catch ex As Exception
            ShowMessage(String.Format("Error Finalizing Shipment. {0}", ex.Message))
        End Try
    End Sub

    Private Sub FinalizeShipment()
        Dim TempShipmentID As Decimal = -1
        Dim TempShipment As New Shipment()
        TempShipmentID = grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_ID")




        If TempShipmentID < 0 Then
            ShowMessage("Please select a shipment to finalize")
        Else
            Dim ShipmentDAL As New Oracle_Shipment
            Dim OracleDAL As New Oracle_General
            Dim ChronosUpdateSuccess As Boolean = False

            Dim TempPriorityID As Integer
            Dim TempExtReferenceTypeID As Integer
            TempPriorityID = OracleDAL.GetPriorityID(lkShippingPriority.EditValue)
            TempExtReferenceTypeID = OracleDAL.GetExtReferenceTypeID(lkShippingCourier.EditValue)

            ShipmentDAL.FinalizeShipment(TempShipmentID, TempPriorityID, TempExtReferenceTypeID)
            TempShipment = New Shipment(ShipmentDAL.ShowShipmentByShipmentID(TempShipmentID).Rows(0))
            PrintShipmentInteriorLabel()

            ChronosUpdateSuccess = ChronosRemarkShipment(TempShipment, UserSite, String.Format("Shipment {0} Finalized", TempShipment.ShipmentName), True)

            If Not ChronosUpdateSuccess Then
                ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
            End If

        End If
    End Sub

    Private Sub btnViewShipmentForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewShipmentForm.Click

        m_bHasSelectedPackage = False
        Dim TempShipmentName As String = grdViewExistingShipments.GetFocusedRowCellValue("SHIPMENT_NAME").ToString()

        If String.IsNullOrEmpty(TempShipmentName) Then
            ShowMessage("You must select a shipment before you can print")
        Else
            FinalizeShipment()
            RefreshCreateShipmentDatagrids()
            DisplayPrintForm(TempShipmentName)
        End If

    End Sub

    Private Sub DisplayPrintForm(ByVal ShipmentName As String)
        Dim ShipmentDAL As New Oracle_Shipment
        Dim CurrentShipment As Shipment = New Shipment(ShipmentDAL.ShowShipmentByShipmentName(ShipmentName).Rows(0))
        'SetReceiverInformation(CurrentShipment)
        Dim PrintShipment As New ShipmentForm(CurrentShipment)
        'm_ReferenceNumber = ShipmentName
        'm_PowderContainerCount = CurrentShipment.PowderContainerCount
        'm_SolutionContainerCount = CurrentShipment.SolutionContainerCount
        'ShipmentForm.Show()
        PrintShipment.Show()
    End Sub

    Private Sub btnRefreshShipment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshShipment.Click
        RefreshCreateShipmentDatagrids()
    End Sub

    Private Sub btnShowPendingPackages_Click(sender As System.Object, e As System.EventArgs) Handles btnShowPendingPackages.Click
        Dim PackageDAL As New Oracle_Package
        PackageDAL.ShowPendingPackages(m_frmShipmentValidationRules, False)
    End Sub

    Private Sub txtShipmentPackageBarcode_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtShipmentPackageBarcode.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnAddPackageShipment.PerformClick()
        End If
    End Sub
#End Region

#Region "           Shipment Planner Tab            "

    Private Sub RefreshShipmentPlanner(Optional ByVal SelectedShipmentID As Long = -1)


        Try
            Dim ShipmentDAL As New Oracle_Shipment
            ShipmentDAL.ShowFinalizedShipments()

            If SelectedShipmentID > 0 Then
                Dim TempNewRow As Integer = GridViewFinalizedShipments.LocateByValue("SHIPMENT_ID", SelectedShipmentID)
                If TempNewRow >= 0 Then
                    GridViewFinalizedShipments.FocusedRowHandle = TempNewRow
                End If
            End If

        Catch err As Exception
            Cursor.Current = Cursors.Default
            ShowMessage(err.Message)
        End Try

    End Sub

    Private Sub altMenuGridRightClick_ItemClicked(sender As System.Object, e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles altMenuGridRightClick.ItemClicked

        If Not altMenuGridRightClick.SourceControl Is Nothing Then
            If altMenuGridRightClick.SourceControl.Name Is "grdFinalizedShipments" Then
                If e.ClickedItem.Text = "Unfinalize" Then
                    UnfinalizeShipment()
                ElseIf e.ClickedItem.Text = "Print Form" Then
                    PrintShipmentForm()
                ElseIf e.ClickedItem.Text = "Update Tracking" Then
                    UpdateTrackingForm()
                Else
                    ShowMessage("Function Not Available")
                End If
            End If
        End If
        altMenuGridRightClick.Hide()
    End Sub

    'Private Sub altMenuGridRightClick_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles altMenuGridRightClick.MouseClick

    '    If Not altMenuGridRightClick.SourceControl Is Nothing Then
    '        If altMenuGridRightClick.SourceControl.Name Is "grdFinalizedShipments" Then
    '            Dim CurrentContextMenu As ContextMenuStrip = CType(sender, ContextMenuStrip)


    '            'If sender.Item.Name =  Then

    '            'End If
    '            UnfinalizeShipment()
    '        End If
    '    End If
    '    altMenuGridRightClick.Hide()
    'End Sub

    Private Sub UnfinalizeShipment()

        Dim TempShipmentID As Decimal
        Dim TempShipment As New Shipment()
        Dim TempWorkstation As String = String.Empty

        Try

            TempShipmentID = GridViewFinalizedShipments.GetFocusedRowCellValue("SHIPMENT_ID")
            TempWorkstation = System.Environment.MachineName
            If TempShipmentID < 1 Then
                ShowMessage("Please select a shipment to unfinalize")
            Else
                'Dim OracleDAL As New Oracle_General
                Dim ShipmentDAL As New Oracle_Shipment
                Dim ChronosUpdateSuccess As Boolean = False

                If ShipmentDAL.GetNumberOfCreatedShipments(TempWorkstation) = 0 Then
                    ShipmentDAL.UnfinalizeShipment(TempShipmentID, TempWorkstation)
                    TempShipment = New Shipment(ShipmentDAL.ShowShipmentByShipmentID(TempShipmentID).Rows(0))
                    'BulkChronosRemark(TempShipment.Containers, UserSite, String.Format("Shipment {0} Unfinalized", TempShipmentName))
                    ChronosUpdateSuccess = ChronosRemarkShipment(TempShipment, UserSite, String.Format("Shipment {0} Unfinalized", TempShipmentID), False)

                    If Not ChronosUpdateSuccess Then
                        ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
                    End If
                Else
                    ShowMessage("Only one shipment can be unfinalized at a time")
                End If
                DisplayCreateShipmentTab()
            End If
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Sub PrintShipmentForm()
        Dim TempShipmentName As String = String.Empty
        TempShipmentName = GridViewFinalizedShipments.GetFocusedRowCellValue("SHIPMENT_NAME").ToString()
        If String.IsNullOrEmpty(TempShipmentName) Then
            ShowMessage("Please select a shipment to finalize")
        Else
            DisplayPrintForm(TempShipmentName)
        End If
    End Sub

    Private Sub UpdateTrackingForm()

        Dim TempShipmentName As String
        Dim TempShipmentID As Decimal

        Try

            TempShipmentID = GridViewFinalizedShipments.GetFocusedRowCellValue("SHIPMENT_ID")
            TempShipmentName = GridViewFinalizedShipments.GetFocusedRowCellValue("SHIPMENT_NAME").ToString()

            Using TempExternalReferenceForm As New frmShipmentExtReference(TempShipmentName)
                TempExternalReferenceForm.ShowDialog()
                If TempExternalReferenceForm.Ok Then
                    If String.IsNullOrEmpty(TempExternalReferenceForm.ShipmentExternalReference) Then
                        ShowMessage("Can't Update External Reference to blank!")
                    Else
                        Dim ShipmentDAL As New Oracle_Shipment
                        Dim CurrentShipment As Shipment = New Shipment(ShipmentDAL.ShowShipmentByShipmentID(TempShipmentID).Rows(0))
                        Dim TempTrackingURL As String = String.Empty

                        Dim TempNewReference As String = TempExternalReferenceForm.ShipmentExternalReference.ToUpper

                        If String.Compare(TempShipmentName, TempNewReference, True) <> 0 Then
                            If CurrentShipment.ExternalReferenceTypeID = 90004 Then
                                TempTrackingURL = String.Format("http://www.fedex.com/Tracking?tracknumber_list={0}", TempExternalReferenceForm.ShipmentExternalReference)
                            ElseIf CurrentShipment.ExternalReferenceTypeID = 90005 Then
                                TempTrackingURL = String.Format("http://www.quickonline.com/cgi-bin/WebObjects/BOLSearch?bolNumber={0}", TempExternalReferenceForm.ShipmentExternalReference)
                            End If
                        Else
                            TempTrackingURL = " "
                        End If

                        ShipmentDAL.UpdateShipmentReference(TempShipmentID, TempExternalReferenceForm.ShipmentExternalReference)

                        Dim ChronosUpdateSuccess As Boolean = False

                        ChronosUpdateSuccess = ChronosTrackingUpdate(CurrentShipment, TempExternalReferenceForm.ShipmentExternalReference, TempTrackingURL)

                        If Not ChronosUpdateSuccess Then
                            ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
                        End If

                    End If
                End If
            End Using
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try

        RefreshShipmentPlanner(TempShipmentID)
    End Sub
#End Region

#Region "           Receive Tab             "

    Private Sub RefreshReceiveDatagrids()
        Dim TempFulfillmentSite As String = lkFulfillmentSite.EditValue
        Dim ShipmentDAL As New Oracle_Shipment
        ShipmentDAL.ShowAnticipatedShipments(TempFulfillmentSite)
        Me.grpReceiveActions.Visible = False
        Me.txtReceivedBarcode.Text = String.Empty

        grdReceiveShipment.Visible = False
        grdRcvPackageView.Visible = False

        'm_sReceiveShipmentName = String.Empty
        m_dReceiveID = -1
        m_ReceivedContainers = Nothing
        m_bFoundReceiveContainers = False
        m_bIsArrived = False

    End Sub

    Private m_ReceivedContainers As Containers
    Private m_dReceiveID As Decimal
    Private m_bFoundReceiveContainers As Boolean
    Private m_bIsArrived As Boolean

    Private Sub txtReceivedBarcode_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtReceivedBarcode.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnLookupShipment.PerformClick()
        End If
    End Sub

    Private Sub btnLookupShipment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLookupShipment.Click
        Dim TempFoundItems As Boolean = False
        Dim TempReceivedBarcode As String = String.Empty
        Dim TempFoundShipment As Boolean = False
        Dim TempFoundPackage As Boolean = False

        Dim TempShipments As Shipments = New Shipments()
        Dim TempPackages As Packages = New Packages()
        Dim TempContainers As Containers = New Containers()

        Dim TempShipmentID As Decimal = -1

        Dim ShipmentDAL As New Oracle_Shipment
        Dim PackageDAL As New Oracle_Package


        If String.IsNullOrEmpty(txtReceivedBarcode.Text) Then
            ShowMessage("Please scan or enter a Shipment ID")
        Else
            TempReceivedBarcode = txtReceivedBarcode.Text
            TempShipments = New Shipments(ShipmentDAL.ShowShipmentByShipmentName(TempReceivedBarcode))

            If TempShipments.Count > 0 Then
                TempFoundItems = True
                TempFoundShipment = True

                For Each TempShipment As Shipment In TempShipments


                    For Each TempPackage As Package In TempShipment.Packages
                        TempContainers.AddRange(TempPackage.Containers)
                    Next
                    TempShipmentID = TempShipment.ShipmentID
                    m_dReceiveID = TempShipmentID

                Next


            Else

                'Dim TempParentContainerID As Decimal = CType(txtReceivedBarcode.Text, Decimal)
                TempPackages = New Packages(PackageDAL.GetPackageByBarcode(TempReceivedBarcode))


                If TempPackages.Count > 0 Then


                    TempFoundItems = True
                    TempFoundPackage = True
                    For Each TempPackage As Package In TempPackages
                        TempContainers.AddRange(TempPackage.Containers)
                        If TempPackage.PhaseID = 50106 Then
                            m_bIsArrived = False
                        Else
                            m_bIsArrived = True
                        End If
                        m_dReceiveID = TempPackage.ParentContainerID
                    Next
                Else
                    TempFoundItems = False
                    TempFoundPackage = False
                    TempFoundShipment = False
                    m_bIsArrived = False
                End If
            End If

            If TempFoundItems Then
                Me.grpReceiveActions.Visible = True

                m_bFoundReceiveContainers = True

                m_ReceivedContainers = New Containers
                m_ReceivedContainers = TempContainers

                If TempFoundShipment Then
                    grdReceiveShipment.DataSource = TempShipments

                    grdReceiveShipment.Visible = True
                    grdRcvPackageView.Visible = False

                    Me.btnReceiveItems.Enabled = True
                    Me.btnReceiveComment.Enabled = True
                    Me.btnRepackageItems.Enabled = False
                Else
                    grdRcvPackageView.DataSource = TempPackages

                    grdReceiveShipment.Visible = False
                    grdRcvPackageView.Visible = True

                    Me.btnReceiveItems.Enabled = True
                    Me.btnReceiveComment.Enabled = False
                    Me.btnRepackageItems.Enabled = True
                End If
            Else
                Me.grpReceiveActions.Visible = False

                m_bFoundReceiveContainers = False
                m_ReceivedContainers = Nothing

                grdReceiveShipment.DataSource = Nothing

                grdReceiveShipment.Visible = False
                grdRcvPackageView.Visible = False

                ShowMessage("No Package or shipment matched that barcode")
            End If

        End If
    End Sub

    Private Sub ReceiveShipment(ByVal CommentID As Integer)
        If m_bFoundReceiveContainers AndAlso m_dReceiveID > 0 Then
            Try
                Dim TempContainers As New Containers
                TempContainers = m_ReceivedContainers

                If grdReceiveShipment.Visible Then
                    Dim ShipmentDAL As New Oracle_Shipment
                    ShipmentDAL.ArriveShipment(m_dReceiveID)

                    If CommentID > 0 Then
                        ShipmentDAL.AddShipmentComment(m_dReceiveID, CommentID)
                    End If

                End If

                If grdRcvPackageView.Visible Then
                    Dim PackageDAL As New Oracle_Package
                    PackageDAL.ArrivePackage(m_dReceiveID)
                End If

                Dim ChronosUpdateSuccess As Boolean = False
                ChronosUpdateSuccess = BulkChronosRemark(TempContainers, UserSite, ShipmentInfo.SenderFullName, "Shipment arrived at recipient on shipment form")

                If Not ChronosUpdateSuccess Then
                    ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
                End If



            Catch ex As Exception
                ShowMessage(String.Format("Error Rereceiving: {0}", ex.Message))
            End Try

        Else
            ShowMessage("Nothing to receive")
        End If

        RefreshReceiveDatagrids()
    End Sub


    Private Sub btnReceiveItems_Click(sender As System.Object, e As System.EventArgs) Handles btnReceiveItems.Click
        ReceiveShipment(-1)
    End Sub


    Private Sub btnReceiveComment_Click(sender As System.Object, e As System.EventArgs) Handles btnReceiveComment.Click


        Using TempCommentForm As New frmShipmentComment
            TempCommentForm.ShowDialog()

            If TempCommentForm.SelectedComment > 0 Then
                ReceiveShipment(TempCommentForm.SelectedComment)
            Else
                ShowMessage("No Comment Selected!")
            End If
        End Using
    End Sub

    Private Sub btnRepackageItems_Click(sender As System.Object, e As System.EventArgs) Handles btnRepackageItems.Click

        If m_bFoundReceiveContainers AndAlso m_dReceiveID > 0 Then
            Try
                Dim TempContainers As New Containers
                TempContainers = m_ReceivedContainers


                If grdRcvPackageView.Visible Then
                    Dim PackageDAL As New Oracle_Package
                    Dim TempPackageID As Decimal = m_dReceiveID
                    Dim ChronosUpdateSuccess As Boolean = False

                    If Not m_bIsArrived Then
                        PackageDAL.ArrivePackage(TempPackageID)


                        ChronosUpdateSuccess = BulkChronosRemark(TempContainers, UserSite, ShipmentInfo.SenderFullName, "Shipment arrived at recipient on shipment form")
                        If Not ChronosUpdateSuccess Then
                            ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
                        End If
                    End If

                    PackageDAL.RepackPackage(TempPackageID)


                    ChronosUpdateSuccess = BulkChronosRemark(TempContainers, UserSite, ShipmentInfo.SenderFullName, "Container ready to be repackaged")
                    If Not ChronosUpdateSuccess Then
                        ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
                    End If
                End If
            Catch ex As Exception
                ShowMessage(String.Format("Error Repackaging: {0}", ex.Message))
            End Try


        Else
            ShowMessage("Nothing to repackage")
        End If

        RefreshReceiveDatagrids()
    End Sub

    Private Sub btnCancelRecieve_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelRecieve.Click

        RefreshReceiveDatagrids()

    End Sub


#End Region

#Region "           Imitate scanner             "


    Private Sub btnLoadBarcode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        Try

            Dim ValForm As New frmValidationRules("Solution", String.Empty)
            ValForm.ShowDialog()

            If Not ValForm.SelectedRules Is Nothing Then
                Dim TempValidationIndex As List(Of Integer) = ValForm.SelectedRules
                Dim ContainerDAL As New Oracle_Container

                LoadItems()

                Dim ScannedContainers As Containers = New Containers(ContainerDAL.GetScannedItems(m_aScannedBarcode))

                'Dim ExistsDictionary As Dictionary(Of String, Boolean) = ExistScan(m_aScannedPlates(0).dtFullPlate, ScannedContainers)
                Dim IsValidDictionary As Dictionary(Of String, ScanValidationResult) = ValidateScan(m_aScannedPlates(0).dtFullPlate, ScannedContainers, TempValidationIndex)
                DisplayBarcodeForm(IsValidDictionary, TempValidationIndex)

                m_aScannedPlates = Nothing
                m_aScannedBarcode = Nothing

            End If

        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub LoadItems()

        Dim alPlate2 As ArrayList = New ArrayList()

        m_aLoadedPlates = New ArrayList()


        Dim testDT As DataTable = New DataTable()
        testDT.Columns.Add(New DataColumn("C01"))
        testDT.Columns.Add(New DataColumn("C02"))
        testDT.Columns.Add(New DataColumn("C03"))
        testDT.Columns.Add(New DataColumn("C04"))
        testDT.Columns.Add(New DataColumn("C05"))
        testDT.Columns.Add(New DataColumn("C06"))
        testDT.Columns.Add(New DataColumn("C07"))
        testDT.Columns.Add(New DataColumn("C08"))
        testDT.Columns.Add(New DataColumn("C09"))
        testDT.Columns.Add(New DataColumn("C10"))
        testDT.Columns.Add(New DataColumn("C11"))
        testDT.Columns.Add(New DataColumn("C12"))

        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '
        '


        Dim row As DataRow = testDT.NewRow()
        row(0) = "1066250786"
        row(1) = "1066250776"
        row(2) = "1066250777"
        row(3) = "0120450620"
        row(4) = "0120449756"
        row(5) = "0120449757"
        row(6) = "1066975914"
        row(7) = "0120450234"
        row(8) = "1066248365"
        row(9) = "1066249138"
        row(10) = "0120450602"
        row(11) = "0120449738"
        testDT.Rows.Add(row)


        row = testDT.NewRow()
        row(0) = "0120449868"
        row(1) = "0120450148"
        row(2) = "1066248193"
        row(3) = "1066250763"
        row(4) = "0120450597"
        row(5) = "0120449733"
        row(6) = "0120449734"
        row(7) = "0120450187"
        row(8) = "1066248194"
        row(9) = "1066250781"
        row(10) = "0120450578"
        row(11) = "0120449714"
        testDT.Rows.Add(row)


        row = testDT.NewRow()
        row(0) = "0120449713"
        row(1) = "0120450172"
        row(2) = "1066248329"
        row(3) = "1066250782"
        row(4) = "0120450573"
        row(5) = "0120449709"
        row(6) = "0120449710"
        row(7) = "0120450163"
        row(8) = "1066248203"
        row(9) = "1066250783"
        row(10) = "0120450554"
        row(11) = "0120449690"
        testDT.Rows.Add(row)

        row = testDT.NewRow()
        row(0) = "0120449689"
        row(1) = "0120450220"
        row(2) = "1066248217"
        row(3) = "1066250787"
        row(4) = "0120450549"
        row(5) = "0120449685"
        row(6) = "0120449686"
        row(7) = "0120450211"
        row(8) = "1066248218"
        row(9) = "1066249116"
        row(10) = "0120450530"
        row(11) = "0120449666"
        testDT.Rows.Add(row)


        row = testDT.NewRow()
        row(0) = "0120449665"
        row(1) = "0120450196"
        row(2) = ""
        row(3) = ""
        row(4) = ""
        row(5) = ""
        row(6) = ""
        row(7) = ""
        row(8) = ""
        row(9) = ""
        row(10) = ""
        row(11) = ""
        testDT.Rows.Add(row)


        row = testDT.NewRow()
        row(0) = ""
        row(1) = ""
        row(2) = ""
        row(3) = ""
        row(4) = ""
        row(5) = ""
        row(6) = ""
        row(7) = ""
        row(8) = ""
        row(9) = ""
        row(10) = ""
        row(11) = ""
        testDT.Rows.Add(row)


        row = testDT.NewRow()
        row(0) = ""
        row(1) = ""
        row(2) = ""
        row(3) = ""
        row(4) = ""
        row(5) = ""
        row(6) = ""
        row(7) = ""
        row(8) = ""
        row(9) = ""
        row(10) = ""
        row(11) = ""
        testDT.Rows.Add(row)



        row = testDT.NewRow()
        row(0) = ""
        row(1) = ""
        row(2) = ""
        row(3) = ""
        row(4) = ""
        row(5) = ""
        row(6) = ""
        row(7) = ""
        row(8) = ""
        row(9) = ""
        row(10) = ""
        row(11) = ""
        testDT.Rows.Add(row)

        Dim fakePlate As cPlate = New cPlate(testDT, 96)
        Dim fakePlates As cPlateList = New cPlateList()
        fakePlates.Add(fakePlate)



        m_aScannedPlates = fakePlates

        GetScannedBarcodes()

        'Dim ScannedContainers As Containers = New Containers(SelectScannedItems(m_aScannedBarcode))

        'Dim ExistsDictionary As Dictionary(Of String, Boolean) = ExistScan(m_aScannedPlates(0).dtFullPlate, ScannedContainers)


        'DisplayBarcodeForm(ExistsDictionary)
    End Sub


    Private Sub GetLoadedBarcodes()
        m_aScannedBarcode = New ArrayList

        For Each currPlate As ArrayList In m_aLoadedPlates

            For Each currRow As RackRow In currPlate

                If Not String.IsNullOrEmpty(currRow.Col1) Then
                    m_aScannedBarcode.Add(currRow.Col1)
                End If
                If Not String.IsNullOrEmpty(currRow.Col2) Then
                    m_aScannedBarcode.Add(currRow.Col2)
                End If
                If Not String.IsNullOrEmpty(currRow.Col3) Then
                    m_aScannedBarcode.Add(currRow.Col3)
                End If
                If Not String.IsNullOrEmpty(currRow.Col4) Then
                    m_aScannedBarcode.Add(currRow.Col4)
                End If
                If Not String.IsNullOrEmpty(currRow.Col5) Then
                    m_aScannedBarcode.Add(currRow.Col5)
                End If
                If Not String.IsNullOrEmpty(currRow.Col6) Then
                    m_aScannedBarcode.Add(currRow.Col6)
                End If
                If Not String.IsNullOrEmpty(currRow.Col7) Then
                    m_aScannedBarcode.Add(currRow.Col7)
                End If
                If Not String.IsNullOrEmpty(currRow.Col8) Then
                    m_aScannedBarcode.Add(currRow.Col8)
                End If
                If Not String.IsNullOrEmpty(currRow.Col9) Then
                    m_aScannedBarcode.Add(currRow.Col9)
                End If
                If Not String.IsNullOrEmpty(currRow.Col10) Then
                    m_aScannedBarcode.Add(currRow.Col10)
                End If
                If Not String.IsNullOrEmpty(currRow.Col11) Then
                    m_aScannedBarcode.Add(currRow.Col11)
                End If
                If Not String.IsNullOrEmpty(currRow.Col12) Then
                    m_aScannedBarcode.Add(currRow.Col12)
                End If


            Next

        Next


        m_bAddItemFromList = False
        m_bAddItemFromScanner = True
        txtPackageItemBarcode.Text = m_aScannedBarcode.Item(0).ToString()


    End Sub


#Region "           Commented out loading plates            "
    'alPlate1.Add(New RackRow("1036027946", _
    '                         "1036027352", _
    '                         "1036027863", _
    '                         "1036027873", _
    '                         "1036027884", _
    '                         "1036027323", _
    '                         "1036027948", _
    '                         "1036027922", _
    '                         "1036027921", _
    '                         "1036027898", _
    '                         "1036027924", _
    '                         "1036027947"))

    'alPlate1.Add(New RackRow("6009278891", _
    '                         "1036027886", _
    '                         "1036027923", _
    '                         "1036027897", _
    '                         "1036027900", _
    '                         "1036027887", _
    '                         "1036027331", _
    '                         "1036027885", _
    '                         "1036027889", _
    '                         "1036027888", _
    '                         "1036027349", _
    '                         "1036027945"))

    'alPlate1.Add(New RackRow("1036027351", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         ""))

    'alPlate1.Add(New RackRow("", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         ""))

    'alPlate1.Add(New RackRow("", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         ""))

    'alPlate1.Add(New RackRow("", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         ""))

    'alPlate1.Add(New RackRow("", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         ""))

    'alPlate1.Add(New RackRow("", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         ""))


    'Plate 2

    ''Plate 3

    'alPlate3.Add(New RackRow("0078678021", _
    '                         "0078678022", _
    '                         "0078678435", _
    '                         "0078678434", _
    '                         "0078678433", _
    '                         "0078678432", _
    '                         "0078678431", _
    '                         "0078678430", _
    '                         "0078678415", _
    '                         "0078678416", _
    '                         "0078678417", _
    '                         "0078678418"))

    'alPlate3.Add(New RackRow("0078678028", _
    '                         "0078678023", _
    '                         "0078678421", _
    '                         "0078678422", _
    '                         "0078678423", _
    '                         "0078678424", _
    '                         "0078678414", _
    '                         "0078678413", _
    '                         "0078678412", _
    '                         "0078678411", _
    '                         "0078678027", _
    '                         "0078678409"))

    'alPlate3.Add(New RackRow("0078678408", _
    '                         "0078678407", _
    '                         "0078678406", _
    '                         "0078678405", _
    '                         "0078678391", _
    '                         "0078678392", _
    '                         "0078678393", _
    '                         "0078678394", _
    '                         "0078678395", _
    '                         "0078678396", _
    '                         "0078678397", _
    '                         "0078678398"))

    'alPlate3.Add(New RackRow("0078678399", _
    '                         "0078678400", _
    '                         "0078678390", _
    '                         "0078678389", _
    '                         "0078678388", _
    '                         "0078678387", _
    '                         "0078678386", _
    '                         "0078678385", _
    '                         "0078678384", _
    '                         "0078678383", _
    '                         "0078678382", _
    '                         "0078678381"))

    'alPlate3.Add(New RackRow("0078678367", _
    '                         "0078678368", _
    '                         "0078678369", _
    '                         "0078678370", _
    '                         "0078678371", _
    '                         "0078678372", _
    '                         "0078678373", _
    '                         "0078678026", _
    '                         "0078678375", _
    '                         "0078678376", _
    '                         "0078678343", _
    '                         "0078678365"))

    'alPlate3.Add(New RackRow("0078678364", _
    '                         "0078678363", _
    '                         "0078678362", _
    '                         "0078678361", _
    '                         "0078678360", _
    '                         "0078678359", _
    '                         "0078678358", _
    '                         "0078678357", _
    '                         "0078678366", _
    '                         "0078678344", _
    '                         "0078678345", _
    '                         "0078678346"))

    'alPlate3.Add(New RackRow("0078678347", _
    '                         "0078678348", _
    '                         "0078678349", _
    '                         "0078678350", _
    '                         "0078678013", _
    '                         "0078678429", _
    '                         "6009799938", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         ""))

    'alPlate3.Add(New RackRow("", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         "", _
    '                         ""))

    'm_aLoadedPlates.Add(alPlate1)
    'm_aLoadedPlates.Add(alPlate2)
    'm_aLoadedPlates.Add(alPlate3)

#End Region

#End Region

#Region "          Intelliscan Scanner and Decoding Logic           "

    Class AsynchReturnMSG
        Public barcodes As String
        Public messages As New List(Of String)
        Public image As Byte()
    End Class
    ''' <summary>
    ''' BackgroundWorker1_DoWork
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Asynchronous Entry Point for scanning</remarks>
    Public Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

        Dim msg As AsynchReturnMSG = DirectCast(e.Argument, AsynchReturnMSG)
        Dim objDecode As DecodeLib.CDecodeLib = DecodImage(msg.image)

        e.Result = objDecode
    End Sub

    ''' <summary>
    ''' BackgroundWorker1_RunWorkerCompleted
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Executed when the Async thread is complete</remarks>

    Public Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        ' Called when the BackgroundWorker is completed.
        Dim objDecode As DecodeLib.CDecodeLib = DirectCast(e.Result, DecodeLib.CDecodeLib)
        BackgroundWorker1.Dispose()

        oFormSingle.lblDecodeStatus.ForeColor = Color.Green
        oFormSingle.lblDecodeStatus.Text = "Decoding Complete."
        oFormSingle.ProgressBar1.MarqueeAnimationSpeed = 0
        oFormSingle.ProgressBar1.Style = ProgressBarStyle.Marquee
        oFormSingle.lblDecodeStatus.Refresh()
        Thread.Sleep(TimeSpan.FromSeconds(2))
        oFormSingle.Close()
        oFormSingle.Dispose()

        If (objDecode.g_CdecodeLibImageObject.Item(0).ErrorList.Length > 0) Then
            Dim msg_error As String = ""
            For Each msg_item As String In objDecode.g_CdecodeLibImageObject.Item(0).ErrorList
                msg_error = msg_error + msg_item + Environment.NewLine
            Next
            MessageBox.Show(msg_error, "Scanning Error!")
        End If

        Refresh()
        ProcessResults(objDecode)

    End Sub

    ''' <remarks>Decode the scan image and get the returned barcodes</remarks>
    Private Function DecodImage(ByRef image As Byte()) As DecodeLib.CDecodeLib
        Dim sw As New Stopwatch()
        sw.Start()
        objContext.imageObject.User_Name = m_Sender521
        Dim objDecode As New DecodeLib.CDecodeLib()
        objDecode.FileByteArray = image
        objDecode.InitDegreeRotaion = 13
        objDecode.StepRotation = 1
        objDecode.NumberOfAttemps = 2
        objDecode.TotalTubes = 96
        objDecode.imageObject = objContext.imageObject
        objDecode.g_CdecodeLibImageObject = objDecode.DecodeImage(objDecode.imageObject, False)
        m_strBarcodesList = objDecode.FormattedOutput()
        objDecode.g_OutputBarcodeList = m_strBarcodesList
        sw.Stop()
        objDecode.DecodingBenchmark = sw.Elapsed
        m_objDecode = objDecode
        Return objDecode

    End Function
    ''' <summary>
    ''' SelectScanner
    ''' </summary>
    ''' <param name="Scanner"></param>
    ''' <remarks>Set the scanner source for input</remarks>
    Private Sub SelectScanner(ByVal Scanner As String)

        Select Case Scanner
            Case "WIA_SINGLE"
                m_objScanner.m_objScanner = New ScannerLib.CScanner_WIA()
                m_objScanner.eScanner = CScanner.Scanner.WIASingle
            Case "Simulator"
                m_objScanner.m_objScanner = New ScannerLib.CScanner_Simulator()
                m_objScanner.eScanner = CScanner.Scanner.Simulator
        End Select

        ' Save the settings to the app.config settings
        My.Settings.Save()

    End Sub
    ''' <summary>
    ''' ScanAndDisplay
    ''' </summary>
    ''' <returns>AsynchReturnMSG</returns>
    ''' <remarks>Initiates the scan and displays the rack on the screen</remarks>
    Private Function ScanAndDisplay() As AsynchReturnMSG

        SelectScanner(Me.cboScanner.SelectedItem)

        Dim msg As AsynchReturnMSG = m_objScanner.MutliScan()
        oFormSingle = New frmImageDisplay()
        Dim pictureBytes As New MemoryStream(msg.image)
        Dim picture As System.Drawing.Image = Image.FromStream(pictureBytes)
        picture.RotateFlip(RotateFlipType.Rotate180FlipY)
        oFormSingle.PictureBox.SizeMode = PictureBoxSizeMode.Zoom
        oFormSingle.PictureBox.Image = picture
        oFormSingle.Show()
        oFormSingle.ProgressBar1.MarqueeAnimationSpeed = 10
        oFormSingle.ProgressBar1.Style = ProgressBarStyle.Marquee

        Return msg

    End Function
    ''' <summary>
    ''' VerifyCropSettingsFromRegistry
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <remarks>
    ''' Verifies that user has created a valid crop setting unless this they
    ''' are in simulation mode which uses a static image.
    ''' </remarks>
    Private Function VerifyCropSettingsFromRegistry()
        Try
            If (Me.cboScanner.SelectedItem = "Simulator") Then
                Return True
            End If

            Dim test As String = Registry.GetValue("HKEY_CURRENT_USER\software\classes\Intelliscan", "Crop", Nothing)
            If (test Is Nothing) Then
                Return False
            End If
            Dim testArray() As String = test.Split(CChar(","))
            If (testArray.Length <> 4) Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    ''' <summary>
    ''' ProcessResults
    ''' </summary>
    ''' <param name="objDecode"></param>
    ''' <remarks>
    ''' Called after the scan and decoding of barcodes. This takes
    ''' the decoder output and formats into readble format for GST.
    ''' Displays and validates barcodes.
    ''' </remarks>
    Public Sub ProcessResults(ByVal objDecode As DecodeLib.CDecodeLib)
        Try

            Dim bCodes As String = objDecode.g_OutputBarcodeList(0).Replace("""", "")
            Dim arrList As New ArrayList(bCodes.Split(","))
            Dim bcodeList = arrList.Cast(Of String)().ToList()
            Dim dataTable As New DataTable

            dataTable = ConvertToDataTable(bcodeList)

            Dim newPlate As cPlate = New cPlate(dataTable, 96)
            m_aScannedPlates = New cPlateList
            m_aScannedPlates.Add(newPlate)

            bcodeList.RemoveAll(Function(str) String.IsNullOrEmpty(str))
            Dim arrayListTemp As New System.Collections.ArrayList(bcodeList)
            m_aScannedBarcode = arrayListTemp

            If Not m_ValForm.SelectedRules Is Nothing Then
                Dim TempValidationIndex As List(Of Integer) = m_ValForm.SelectedRules
                Dim ContainerDAL As New Oracle_Container

                System.Windows.Forms.Application.DoEvents()

                Dim ScannedContainers As Containers = New Containers(ContainerDAL.GetScannedItems(m_aScannedBarcode))

                'Dim ExistsDictionary As Dictionary(Of String, Boolean) = ExistScan(m_aScannedPlates(0).dtFullPlate, ScannedContainers)
                Dim IsValidDictionary As Dictionary(Of String, ScanValidationResult) = ValidateScan(m_aScannedPlates(0).dtFullPlate, ScannedContainers, TempValidationIndex)
                DisplayBarcodeForm(IsValidDictionary, TempValidationIndex)

                m_aScannedPlates = Nothing
                m_aScannedBarcode = Nothing

            End If

        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub
    ''' <summary>
    ''' btnScannerHelp_Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Displays the help screen for setting up a new scanner.</remarks>
    Private Sub btnScannerHelp_Click(sender As Object, e As EventArgs) Handles btnScannerHelp.Click
        Dim frm As frmScannerConfiguration = New frmScannerConfiguration
        frm.Show()
    End Sub
    ''' <summary>
    ''' btnTestScan_Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Runs the scanner configuration screen for setting crop and testing scanner.</remarks>
    Private Sub btnTestScan_Click(sender As Object, e As EventArgs) Handles btnTestScan.Click
        ShowConfigureScanner()
    End Sub
    ''' <summary>
    ''' ShowConfigureScanner
    '''    - 	Display the ScanTest form for users to configure the scanner
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowConfigureScanner()
        Dim m_strCropSettingsFile As String = ""
        Dim strPath As String = System.IO.Path.Combine(m_strAppPath, m_strCropSettingsFile)

        Dim frm As New ScanTest.frmMain()
        frm.AppPath = m_strAppPath
        frm.SettingsFile = m_strCropSettingsFile
        frm.ShowDialog()
    End Sub
#End Region

#Region "               Scanner Functionality                               "

    ''' <summary>
    ''' Scan 2D barcodes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnScanItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnScanItems.Click
        'ShowPendingItems(m_frmPackageValidationRules, False)
        'See if an instance of the Barcode reader is already instantiated
        Try

            If (Not VerifyCropSettingsFromRegistry()) Then
                MessageBox.Show("No crop settings found. Please create and save crop settings.", "Crop Settings Missing")
                Return
            End If

            GrdViewPendingItems.ClearColumnsFilter()
            GrdViewPendingItems.ClearSorting()
            GrdViewPendingItems.ClearGrouping()

            Dim ch As New DecodeLib.CDecodeLib()
            Dim cScaner As New CScanner

            Dim ValForm As New frmValidationRules("Solution", String.Empty)

            ValForm.ShowDialog()
            m_ValForm = ValForm

            objContext.imageObject = ch.GetContextHandle()
            'Start scan and kick off async decoding.
            m_DecodingStopped = False
            BackgroundWorker1.WorkerSupportsCancellation = True
            BackgroundWorker1.WorkerReportsProgress = True
            BackgroundWorker1.RunWorkerAsync(ScanAndDisplay)
            Return

        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub
    Public Shared Function ConvertToDataTable(Of T)(ByVal list As IList(Of T)) As DataTable
        Dim table As New DataTable()
        Dim itemCount As Int16 = 0
        Dim fields() As FieldInfo = GetType(T).GetFields()
        For col = 1 To 12
            table.Columns.Add(col.ToString, Type.GetType("System.String"))
        Next
        For rowItem = 1 To 8
            Dim row As DataRow = table.NewRow()
            For col = 1 To 12
                row(col.ToString) = list(itemCount)
                itemCount += 1
            Next
            table.Rows.Add(row)
        Next
        Return table
    End Function
    Private Sub ScanItems()
        Try
            If m_BarcodeReader Is Nothing Then
                m_BarcodeReader = New BarcodeUtilityDLL.cPlateReader
            End If
            m_BarcodeReader.Read(1)
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Event handler for ReadComplete. This is raised when barcode read is completed.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub MyBarcodeEvent() Handles m_BarcodeReader.ReadComplete
        'Dim strbarcode1 As DataTable
        'Dim strbarcode2 As DataTable
        'Dim strbarcode3 As DataTable
        'Dim ResultArray As Array


        'ResultArray in (0,0,0) format - rack 0-2, row 0-7, column 0-11 (0-based)
        'm_aScannedBarcode = m_BarcodeReader.GetAllPlatesBarcodes()
        m_aScannedPlates = m_BarcodeReader.GetPlates()

        GetScannedBarcodes()

        'ShowScannedItems(m_aScannedBarcode)
        'Dim ScannedDataTable As DataTable = 
        'Me.grdPendingItems.DataSource = ScannedDataTable

    End Sub


    Private Sub GetScannedBarcodes()
        m_aScannedBarcode = New ArrayList



        For Each CurrentPlate As cPlate In m_aScannedPlates
            If Not CurrentPlate.dtFullPlate Is Nothing Then
                For Each CurrentPlateRow As DataRow In CurrentPlate.dtFullPlate.Rows
                    For Each CurrentBarcode As String In CurrentPlateRow.ItemArray
                        m_aScannedBarcode.Add(CurrentBarcode)
                    Next
                Next
            End If
        Next
    End Sub

    Public Sub DisplayBarcodeForm(ByVal IsValidDictionary As Dictionary(Of String, ScanValidationResult), ByVal ValidationRulesID As List(Of Integer))
        Dim frmBarcode As BarcodeForm = New BarcodeForm(IsValidDictionary)
        frmBarcode.grdBarcodeResult1.DataSource = m_aScannedPlates(0).dtFullPlate

        frmBarcode.GrdViewBarcodeResult1.PopulateColumns()
        frmBarcode.GrdViewBarcodeResult1.Columns(0).Caption = "1"
        frmBarcode.GrdViewBarcodeResult1.Columns(1).Caption = "2"
        frmBarcode.GrdViewBarcodeResult1.Columns(2).Caption = "3"
        frmBarcode.GrdViewBarcodeResult1.Columns(3).Caption = "4"
        frmBarcode.GrdViewBarcodeResult1.Columns(4).Caption = "5"
        frmBarcode.GrdViewBarcodeResult1.Columns(5).Caption = "6"
        frmBarcode.GrdViewBarcodeResult1.Columns(6).Caption = "7"
        frmBarcode.GrdViewBarcodeResult1.Columns(7).Caption = "8"
        frmBarcode.GrdViewBarcodeResult1.Columns(8).Caption = "9"
        frmBarcode.GrdViewBarcodeResult1.Columns(9).Caption = "10"
        frmBarcode.GrdViewBarcodeResult1.Columns(10).Caption = "11"
        frmBarcode.GrdViewBarcodeResult1.Columns(11).Caption = "12"

        'frmBarcode.TubeCount = m_aScannedPlates(0).intTubeCount
        frmBarcode.lblTubeCount.Text = "Tubes scanned: " & m_aScannedPlates(0).intTubeCount.ToString()
        'm_BarcodeReader.Unload()
        'm_BarcodeReader = Nothing
        frmBarcode.ShowDialog()


        If frmBarcode.Cancel Then
            frmBarcode.Dispose()
            GrdViewPendingItems.ClearSelection()
        ElseIf frmBarcode.Rescan Then
            frmBarcode.Dispose()
            btnScanItems_Click(sender:=btnScanItems, e:=New System.EventArgs())
        ElseIf frmBarcode.Ok Then
            frmBarcode.Dispose()
            SelectPending()
            CreatePackageAfterScan(ValidationRulesID) 'CreatePackage("", ValidationRulesID)
        Else
            frmBarcode.Dispose()
        End If
    End Sub

    Public Sub CreatePackageAfterScan(ByVal ValidationRulesID As List(Of Integer))

        Dim NewPackageID As Decimal = -1
        Dim TempRowIndex As Integer()
        Dim TempNote As String = String.Empty
        Dim TempIsAdded As Boolean = False

        Try

            TempRowIndex = GrdViewPendingItems.GetSelectedRows

            If Not m_bHasSelectedItem OrElse TempRowIndex.Length = 0 Then
                ShowMessage("No items scanned")
            Else
                NewPackageID = CreatePackage(TempNote, ValidationRulesID)

                If NewPackageID > 0 Then
                    'AddItemsToPackages(NewPackageID, TempRowIndex, TempNote)
                    TempIsAdded = AddBulkItemsToPackage(NewPackageID, TempRowIndex, TempNote)
                End If

                If Not TempIsAdded Then
                    RemovePackage(NewPackageID)
                End If
            End If
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
        RefreshCreatePackageDatagrids(NewPackageID)
    End Sub

    Private Function ExistScan(ByVal ScannedRack As DataTable, ByVal PendingContainers As Containers) As Dictionary(Of String, Boolean)

        Dim ScannedFound As Dictionary(Of String, Boolean) = New Dictionary(Of String, Boolean)
        'Dim ScannedValidationRules As New PackageValidationRules
        'Dim IsFirst As Boolean = True

        For Each CurrentRackRow As DataRow In ScannedRack.Rows
            For Each CurrentRackWellBarcode As String In CurrentRackRow.ItemArray
                If PendingContainers.HasBarcode(CurrentRackWellBarcode) Then
                    ScannedFound(CurrentRackWellBarcode) = True
                Else
                    ScannedFound(CurrentRackWellBarcode) = False
                End If
            Next
        Next

        Return ScannedFound
    End Function

    Private Function ValidateScan(ByVal ScannedRack As DataTable, ByVal PendingContainers As Containers, ByVal ValidationRuleIDs As List(Of Integer)) As Dictionary(Of String, ScanValidationResult)

        Dim DictValidate As Dictionary(Of String, ScanValidationResult) = New Dictionary(Of String, ScanValidationResult)
        Dim ScannedValidationRules As New PackageValidationRules
        Dim ValidationErrors As New List(Of ValidationErrorInput)
        Dim IsFirst As Boolean = True

        For Each CurrentRackRow As DataRow In ScannedRack.Rows
            For Each CurrentRackWellBarcode As String In CurrentRackRow.ItemArray

                Dim CurrentWellValidation As ScanValidationResult = New ScanValidationResult()
                Dim CurrentContainer As Container = PendingContainers.GetContainerByBarcode(CurrentRackWellBarcode)


                If CurrentContainer Is Nothing Then
                    CurrentWellValidation.Exists = False
                    CurrentWellValidation.IsBase = False
                    CurrentWellValidation.IsValid = False
                Else
                    CurrentWellValidation.Exists = True
                    If IsFirst Then
                        IsFirst = False
                        ScannedValidationRules = SetScanValidationRules(CurrentContainer, ValidationRuleIDs)
                        CurrentWellValidation.IsBase = True
                        CurrentWellValidation.IsValid = True
                    Else
                        CurrentWellValidation.IsBase = False
                        CurrentWellValidation.IsValid = ValidateContainer(CurrentContainer, ScannedValidationRules, ValidationErrors)
                    End If
                End If
                DictValidate(CurrentRackWellBarcode) = CurrentWellValidation
            Next
        Next

        If ValidationErrors.Count > 0 Then
            Dim ContainerDAL As New Oracle_Container
            ContainerDAL.AddValidationErrors(ValidationErrors)
        End If

        Return DictValidate
    End Function

    Private Function SetScanValidationRules(ByVal BaseContainer As Container, ByVal Rules As List(Of Integer)) As PackageValidationRules

        Dim ScanValidationRules As New PackageValidationRules


        For Each RuleInt As Integer In Rules

            Select Case RuleInt
                Case ScanValidationRules.SiteRuleID
                    ScanValidationRules.Site = True
                Case ScanValidationRules.RecipientRuleID
                    ScanValidationRules.Recipient = True
                Case ScanValidationRules.RequestNumberRuleID
                    ScanValidationRules.RequestNumber = True
                Case ScanValidationRules.DestinationRuleID
                    ScanValidationRules.Destination = True
                Case ScanValidationRules.DeliveryFormRuleID
                    ScanValidationRules.DeliveryForm = True
                Case ScanValidationRules.ProtocolRuleID
                    ScanValidationRules.Protocol = True
                Case ScanValidationRules.DestRackGroupNameRuleID
                    ScanValidationRules.DestRackGroupName = True
            End Select

        Next

        ScanValidationRules.HasBase = True


        If ScanValidationRules.Site AndAlso Not String.IsNullOrEmpty(BaseContainer.SiteCode) Then
            ScanValidationRules.SiteValue = BaseContainer.SiteCode
        Else
            ScanValidationRules.SiteValue = String.Empty
        End If

        If ScanValidationRules.Recipient AndAlso Not String.IsNullOrEmpty(BaseContainer.Email) Then
            ScanValidationRules.RecipientValue = BaseContainer.Email
        Else
            ScanValidationRules.RecipientValue = String.Empty
        End If

        If ScanValidationRules.RequestNumber AndAlso Not String.IsNullOrEmpty(BaseContainer.OrderName) Then
            ScanValidationRules.RequestNumberValue = BaseContainer.OrderName
        Else
            ScanValidationRules.RequestNumberValue = String.Empty
        End If

        If ScanValidationRules.Destination AndAlso Not String.IsNullOrEmpty(BaseContainer.MailAddress) Then
            ScanValidationRules.DestinationValue = BaseContainer.MailAddress
        Else
            ScanValidationRules.DestinationValue = String.Empty
        End If

        If ScanValidationRules.DeliveryForm AndAlso Not String.IsNullOrEmpty(BaseContainer.DeliveryFormTypeName) Then
            ScanValidationRules.DeliveryFormValue = BaseContainer.DeliveryFormTypeName
        Else
            ScanValidationRules.DeliveryFormValue = String.Empty
        End If

        If ScanValidationRules.Protocol AndAlso Not String.IsNullOrEmpty(BaseContainer.ProtocolName) Then
            ScanValidationRules.ProtocolValue = BaseContainer.ProtocolName
        Else
            ScanValidationRules.ProtocolValue = String.Empty
        End If

        If ScanValidationRules.DestRackGroupName AndAlso Not String.IsNullOrEmpty(BaseContainer.DestRackGroupName) Then
            ScanValidationRules.DestRackGroupNameValue = BaseContainer.DestRackGroupName
        Else
            ScanValidationRules.DestRackGroupNameValue = String.Empty
        End If

        Return ScanValidationRules
    End Function

    Private Function ValidateContainer(ByVal ScannedContainer As Container, ByVal ScannedValidationRules As PackageValidationRules, ByRef ValidationErrors As List(Of ValidationErrorInput)) As Boolean
        Dim IsValid As Boolean = True
        Dim IsSameSite As Boolean = True
        Dim IsSameRecipient As Boolean = True
        Dim IsSameDestination As Boolean = True
        Dim IsSameContentType As Boolean = True
        Dim IsSameRequestNumber As Boolean = True
        Dim IsSameDeliveryForm As Boolean = True
        Dim IsSameProtocol As Boolean = True
        Dim IsSameDestRackGroupName As Boolean = True

        Dim TempAccount As String = m_Sender521
        Dim ContainerDAL As New Oracle_Container

        If ScannedValidationRules.Site Then
            Dim TempValidationSite As String = ScannedValidationRules.SiteValue
            Dim TempItemSite As String = ScannedContainer.SiteCode

            If Not String.IsNullOrEmpty(TempValidationSite) Then
                If TempValidationSite = TempItemSite Then
                    IsSameSite = True
                Else
                    IsSameSite = False
                    ValidationErrors.Add(New ValidationErrorInput(ScannedContainer.Barcode, String.Empty, TempAccount, ScannedValidationRules.SiteRuleID, TempValidationSite, TempItemSite))
                End If
            End If

        End If

        'm_frmPackageValidationRules.RecipientValue = BaseContainer.Email
        If ScannedValidationRules.Recipient Then
            Dim TempValidationAccount As String = ScannedValidationRules.RecipientValue
            Dim TempItemAccount As String = ScannedContainer.Email

            If Not String.IsNullOrEmpty(TempValidationAccount) Then
                If TempValidationAccount = TempItemAccount Then
                    IsSameRecipient = True
                Else
                    IsSameRecipient = False
                    ValidationErrors.Add(New ValidationErrorInput(ScannedContainer.Barcode, String.Empty, TempAccount, ScannedValidationRules.RecipientRuleID, TempValidationAccount, TempItemAccount))
                End If
            End If
        End If


        'm_frmPackageValidationRules.RequestNumberValue = BaseContainer.OrderName
        If ScannedValidationRules.RequestNumber Then
            Dim TempRequestNumber As String = ScannedValidationRules.RequestNumberValue
            Dim TempItemRequestNumber As String = ScannedContainer.OrderName

            If Not String.IsNullOrEmpty(TempRequestNumber) Then

                If TempRequestNumber = TempItemRequestNumber Then
                    IsSameRequestNumber = True
                Else
                    IsSameRequestNumber = False
                    ValidationErrors.Add(New ValidationErrorInput(ScannedContainer.Barcode, String.Empty, TempAccount, ScannedValidationRules.RequestNumberRuleID, TempRequestNumber, TempItemRequestNumber))
                End If
            End If
        End If


        'm_frmPackageValidationRules.DestinationValue = BaseContainer.MailAddress
        If ScannedValidationRules.Destination Then
            Dim TempValidationDestination As String = ScannedValidationRules.DestinationValue
            Dim TempItemDestination As String = ScannedContainer.MailAddress

            If Not String.IsNullOrEmpty(TempValidationDestination) Then

                If TempValidationDestination = TempItemDestination Then
                    IsSameDestination = True
                Else
                    IsSameDestination = False
                    ValidationErrors.Add(New ValidationErrorInput(ScannedContainer.Barcode, String.Empty, TempAccount, ScannedValidationRules.DestinationRuleID, TempValidationDestination, TempItemDestination))
                End If
            End If
        End If


        'm_frmPackageValidationRules.DeliveryFormValue = BaseContainer.DeliveryFormTypeName
        If ScannedValidationRules.DeliveryForm Then
            Dim TempDeliveryForm As String = ScannedValidationRules.DeliveryFormValue
            Dim TempItemDeliveryForm As String = ScannedContainer.DeliveryFormTypeName

            If Not String.IsNullOrEmpty(TempDeliveryForm) Then

                If TempDeliveryForm = TempItemDeliveryForm Then
                    IsSameDeliveryForm = True
                Else
                    IsSameDeliveryForm = False
                    ValidationErrors.Add(New ValidationErrorInput(ScannedContainer.Barcode, String.Empty, TempAccount, ScannedValidationRules.DeliveryFormRuleID, TempDeliveryForm, TempItemDeliveryForm))
                End If
            End If
        End If


        'm_frmPackageValidationRules.ProtocolValue = BaseContainer.ProtocolName
        If ScannedValidationRules.Protocol Then
            Dim TempValidationProtocol As String = ScannedValidationRules.ProtocolValue
            Dim TempItemProtocol As String = ScannedContainer.ProtocolName

            If Not String.IsNullOrEmpty(TempValidationProtocol) Then
                If TempValidationProtocol = TempItemProtocol Then
                    IsSameProtocol = True
                Else
                    IsSameProtocol = False
                    ValidationErrors.Add(New ValidationErrorInput(ScannedContainer.Barcode, String.Empty, TempAccount, ScannedValidationRules.ProtocolRuleID, TempValidationProtocol, TempItemProtocol))
                End If
            End If
        End If

        If ScannedValidationRules.DestRackGroupName Then
            Dim TempValidationDestRackGroupName As String = ScannedValidationRules.DestRackGroupNameValue
            Dim TempItemDestRackGroupName As String = ScannedContainer.DestRackGroupName

            If Not String.IsNullOrEmpty(TempValidationDestRackGroupName) Then
                If TempValidationDestRackGroupName = TempItemDestRackGroupName Then
                    IsSameDestRackGroupName = True
                Else
                    IsSameDestRackGroupName = False
                    ValidationErrors.Add(New ValidationErrorInput(ScannedContainer.Barcode, String.Empty, TempAccount, ScannedValidationRules.DestRackGroupNameRuleID, TempValidationDestRackGroupName, TempItemDestRackGroupName))
                End If
            End If
        End If

        IsValid = IsSameSite AndAlso IsSameRecipient AndAlso IsSameDestination AndAlso IsSameContentType AndAlso IsSameRequestNumber AndAlso IsSameDeliveryForm AndAlso IsSameProtocol AndAlso IsSameDestRackGroupName

        Return IsValid
    End Function

    Private Sub SelectPending()
        Cursor.Current = Cursors.WaitCursor
        grdPendingItems.DataSource = Nothing
        Dim ContainerDAL As New Oracle_Container
        ContainerDAL.ShowScannedItems(m_aScannedBarcode)
        'ShowPendingItems(m_frmPackageValidationRules)
        GrdViewPendingItems.SelectAll()
        Cursor.Current = Cursors.Default
        m_bHasSelectedItem = True
    End Sub

#End Region


#Region "           Settings Tab                "

    Private Sub RefreshSettings()

        LoadConfiguration(m_Sender521)
        Me.txtCostCenter.Text = Configuration.CostCenter
        If Not String.IsNullOrEmpty(Configuration.Printer) Then
            Me.cboPrinter.Text = Configuration.Printer
        Else
            Me.cboPrinter.SelectedIndex = 0
        End If

        If Not String.IsNullOrEmpty(Configuration.PrinterDPI) Then
            Me.cboPrinterDPI.Text = Configuration.PrinterDPI
        Else
            Me.cboPrinterDPI.SelectedIndex = 0
        End If

        Me.txtPrinterPort.Text = Configuration.PrinterPort
        Me.txtLabelTemplatePath.Text = Configuration.LabelTemplatePath
        Me.txtShipmentTemplate.Text = Configuration.ShipmentTemplatePath
        'Me.cboEnvironment.Text = Configuration.Environment
        Me.txtFedExAccount.Text = Configuration.FedExAccount
        Me.txtQuickInternational.Text = Configuration.QuickIntlAccount

    End Sub

    Private Sub btnSaveSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSettings.Click

        Configuration.UserAccount = txtUsername.Text
        Configuration.CostCenter = txtCostCenter.Text
        Configuration.Printer = cboPrinter.Text
        Configuration.PrinterDPI = cboPrinterDPI.Text
        Configuration.PrinterPort = txtPrinterPort.Text
        Configuration.LabelTemplatePath = txtLabelTemplatePath.Text
        Configuration.ShipmentTemplatePath = txtShipmentTemplate.Text
        'Configuration.Environment = cboEnvironment.Text
        Configuration.FulfillmentSite = lkFulfillmentSite.EditValue
        Configuration.FedExAccount = txtFedExAccount.Text
        Configuration.QuickIntlAccount = txtQuickInternational.Text
        'Configuration.DefaultLogin = lkDefaultLogin.EditValue
        SaveConfiguration()
    End Sub

    'Private Sub btnTestScan_Click(sender As System.Object, e As System.EventArgs) Handles btnTestScan.Click

    '    Dim m_objScanner As New CScanner

    '    'm_objScanner.Init()
    '    'm_objScanner.SetSource()
    '    m_objScanner.ScanRegion()
    'End Sub
#End Region

#Region "           Sample Transfer Tab         "

#Region "           General             "

    'Private Sub NavSampleDropoff_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs)

    '    DisplaySampleTransferTab()

    'End Sub

    Private Sub InitializeSampleTransferTab()
        Me.TabLogin.PageVisible = False
        Me.TabCreatePackage.PageVisible = False
        Me.TabPackageMaintenance.PageVisible = False
        Me.TabCreateShipment.PageVisible = False
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabViewShipment.PageVisible = False
        Me.TabReceive.PageVisible = False
        Me.TabSettings.PageVisible = False

        Me.TabSampleTransfer.PageVisible = True
        Me.tabMain.Enabled = True

        Me.tabSTISampleDropoff.PageVisible = False
        Me.tabSTIPackagePickup.PageVisible = False
        Me.tabSTIPackageDropoff.PageVisible = False
        Me.tabSTIBinMaintenance.PageVisible = False
        Me.tabSTILog.PageVisible = False

        Me.RibbonControl.SelectedPage = rpSampleTransfer
    End Sub

    Private Sub DisplaySampleTransferTab()
        InitializeSampleTransferTab()
        SetSimpleLogin()
        ResetInactivityTimer()
    End Sub

    Private Sub bbtnSTISampleDropoff_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnSTISampleDropoff.ItemClick
        DisplaySampleDropoff()
    End Sub

    Private Sub NavSTISampleDropoff_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavSTISampleDropoff.LinkClicked
        DisplaySampleDropoff()
    End Sub

    Private Sub DisplaySampleDropoff()

        InitializeSampleTransferTab()

        tabSTISampleDropoff.PageVisible = True
        'tabSTIPackagePickup.PageVisible = False
        'tabSTIPackageDropoff.PageVisible = False
        'tabSTIBinMaintenance.PageVisible = False

        InitializeSampleDropoff()
        ResetInactivityTimer()
    End Sub

    Private Sub bbtnSTIPackageDropoff_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnSTIPackageDropoff.ItemClick
        DisplayPackageDropoff()
    End Sub

    Private Sub NavSTIPackageDropoff_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavSTIPackageDropoff.LinkClicked
        DisplayPackageDropoff()
    End Sub

    Private Sub DisplayPackageDropoff()
        InitializeSampleTransferTab()

        tabSTIPackageDropoff.PageVisible = True

        InitializePackageDropoff()
        ResetInactivityTimer()
    End Sub

    Private Sub bbtnSTIPackagePickup_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnSTIPackagePickup.ItemClick
        DisplayPackagePickup()
    End Sub

    Private Sub NavSTIPackagePickup_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavSTIPackagePickup.LinkClicked
        DisplayPackagePickup()
    End Sub

    Private Sub DisplayPackagePickup()
        InitializeSampleTransferTab()

        tabSTIPackagePickup.PageVisible = True

        InitializePackagePickup()
        ResetInactivityTimer()
    End Sub

    Private Sub bbtnSTIBinMaintenance_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnSTIBinMaintenance.ItemClick
        DisplayBinMaintenance()
    End Sub


    Private Sub NavSTIBinMaintenance_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavSTIBinMaintenance.LinkClicked
        DisplayBinMaintenance()
    End Sub

    Private Sub DisplayBinMaintenance()
        InitializeSampleTransferTab()

        tabSTIBinMaintenance.PageVisible = True

        InitializeBinMaintenance()
        ResetInactivityTimer()
    End Sub

    Private Sub NavSTILog_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavSTILog.LinkClicked
        DisplayLog()
    End Sub

    Private Sub bbtnSTILog_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbtnSTILog.ItemClick
        DisplayLog()
    End Sub

    Private Sub DisplayLog()
        InitializeSampleTransferTab()

        tabSTILog.PageVisible = True


        InitializeLog()
        ResetInactivityTimer()
    End Sub

#Region "           Login Section           "

    Private Sub ClearSTIFilters()
        GridViewPackagePickup.ClearSorting()
        GridViewPackagePickup.ClearColumnsFilter()
        GridViewPackagePickup.ClearGrouping()

        grdViewPackageDropoff.ClearSorting()
        grdViewPackageDropoff.ClearColumnsFilter()
        grdViewPackageDropoff.ClearGrouping()

        GridViewPackagePickup.ClearSorting()
        GridViewPackagePickup.ClearColumnsFilter()
        GridViewPackagePickup.ClearGrouping()

        GridViewPackagePickupHistory.ClearSorting()
        GridViewPackagePickupHistory.ClearColumnsFilter()
        GridViewPackagePickupHistory.ClearGrouping()

        GridViewBinMaintenance.ClearSorting()
        GridViewBinMaintenance.ClearColumnsFilter()
        GridViewBinMaintenance.ClearGrouping()

        GridViewSTILog.ClearSorting()
        GridViewSTILog.ClearColumnsFilter()
        GridViewSTILog.ClearGrouping()
    End Sub

    Private Sub SetSimpleLogin()

        If m_bLoggedIn Then
            m_oSTIUser = New STIUser(ShipmentInfo.m_SenderAccountNumber, ShipmentInfo.m_SenderFirstName, ShipmentInfo.m_SenderLastName, ShipmentInfo.m_SenderLastName, ShipmentInfo.m_SenderCompany)

            Me.GrpSimpleLogin.Text = m_oSTIUser.Account
            Me.btnSimpleLogin.Enabled = False
            Me.txtSimpleUsername.Visible = False
            Me.lblSimpleUsername.Visible = False
            Me.btnSimpleLogin.Text = "Logout"

        Else
            If m_bSimpleLoggedIn Then
                Me.GrpSimpleLogin.Text = m_oSTIUser.Account
                Me.txtSimpleUsername.Visible = False
                Me.lblSimpleUsername.Visible = False
                Me.btnSimpleLogin.Text = "Logout"
                Me.btnSimpleLogin.Enabled = True

                Me.GrpActiveUser.Enabled = False

            Else
                Me.txtSimpleUsername.Visible = True
                Me.lblSimpleUsername.Visible = True
                Me.GrpSimpleLogin.Text = "Simple Login"
                Me.btnSimpleLogin.Text = "Login"
                Me.btnSimpleLogin.Enabled = True
                Me.txtSimpleUsername.Text = String.Empty

                m_oSTIUser = Nothing

                Me.GrpActiveUser.Enabled = True

                Me.txtSimpleUsername.Focus()
            End If
        End If
        DisplaySTINavLinks()
    End Sub

    Private Sub txtSimpleUsername_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSimpleUsername.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnSimpleLogin.PerformClick()
        End If
    End Sub

    Private m_allowedInactivityTime As Integer = 300
    Private m_currentInactivityTime As Integer = 0

    Private Sub ResetInactivityTimer()
        m_currentInactivityTime = 0
    End Sub

    Private Sub STITimer_Tick(sender As System.Object, e As System.EventArgs) Handles STITimer.Tick

        m_currentInactivityTime += 1

        If m_currentInactivityTime >= m_allowedInactivityTime Then
            btnSimpleLogin.PerformClick()
        End If
    End Sub

    Private Sub btnSimpleLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnSimpleLogin.Click

        If m_bSimpleLoggedIn Then
            m_bSimpleLoggedIn = False
            'm_sSimpleUsername = String.Empty
            STITimer.Enabled = False
        Else

            Dim User521 As String = Me.txtSimpleUsername.Text

            If String.IsNullOrEmpty(User521) Then

                m_bSimpleLoggedIn = False
                m_oSTIUser = Nothing

                ShowMessage("Please enter a username")
            Else
                Try
                    Dim StiDAL As New Oracle_STI
                    Dim CurrentUser As STIUser = StiDAL.VerifySTIUser(User521)

                    If Not CurrentUser Is Nothing Then

                        m_bSimpleLoggedIn = True
                        'm_oSTIUser = New STIUser(ShipmentInfo.m_SenderID, ShipmentInfo.m_SenderFirstName, ShipmentInfo.m_SenderLastName, ShipmentInfo.m_SenderLastName, ShipmentInfo.m_SenderCompany)
                        'm_sSimpleUsername = User521
                        m_oSTIUser = CurrentUser
                        STITimer.Enabled = True
                        ResetInactivityTimer()
                        STITimer.Start()
                    Else

                        m_bSimpleLoggedIn = False
                        m_oSTIUser = Nothing
                        ShowMessage("Unable to verify. Please try again")
                    End If
                Catch ex As Exception
                    ShowMessage(ex.Message)
                End Try
            End If
        End If
        ClearSTIFilters()
        SetSimpleLogin()
    End Sub

    Private Sub DisplaySTINavLinks()

        If Not m_bIsSampleTransferMachine OrElse m_oSTIUser Is Nothing Then
            tabSTISampleDropoff.PageVisible = False
            tabSTIPackageDropoff.PageVisible = False
            tabSTIPackagePickup.PageVisible = False
            tabSTIBinMaintenance.PageVisible = False
            tabSTILog.PageVisible = False

            Me.NavSTISampleDropoff.Enabled = False
            Me.NavSTIPackageDropoff.Enabled = False
            Me.NavSTIPackagePickup.Enabled = False
            Me.NavSTIBinMaintenance.Enabled = False
            Me.NavSTILog.Enabled = False

            Me.bbtnSTISampleDropoff.Enabled = False
            Me.bbtnSTIPackageDropoff.Enabled = False
            Me.bbtnSTIPackagePickup.Enabled = False
            Me.bbtnSTIBinMaintenance.Enabled = False
            Me.bbtnSTILog.Enabled = False

        Else
            If m_oSTIUser.IsSMG Then

                Me.NavSTISampleDropoff.Enabled = True
                Me.bbtnSTISampleDropoff.Enabled = True

                Me.NavSTILog.Enabled = True
                Me.bbtnSTILog.Enabled = True

                If m_bHasSTIBins Then

                    Me.NavSTIPackageDropoff.Enabled = True
                    Me.NavSTIPackagePickup.Enabled = True
                    Me.NavSTIBinMaintenance.Enabled = True

                    Me.bbtnSTIPackageDropoff.Enabled = True
                    Me.bbtnSTIPackagePickup.Enabled = True
                    Me.bbtnSTIBinMaintenance.Enabled = True

                Else
                    Me.NavSTIPackageDropoff.Enabled = False
                    Me.NavSTIPackagePickup.Enabled = False
                    Me.NavSTIBinMaintenance.Enabled = False

                    Me.bbtnSTIPackageDropoff.Enabled = False
                    Me.bbtnSTIPackagePickup.Enabled = False
                    Me.bbtnSTIBinMaintenance.Enabled = False

                End If


            ElseIf m_oSTIUser.IsInternal Then

                Me.NavSTISampleDropoff.Enabled = True
                Me.NavSTIPackageDropoff.Enabled = False
                Me.NavSTIBinMaintenance.Enabled = False
                Me.NavSTILog.Enabled = False

                Me.bbtnSTISampleDropoff.Enabled = True
                Me.bbtnSTIPackageDropoff.Enabled = False
                Me.bbtnSTIBinMaintenance.Enabled = False
                Me.bbtnSTILog.Enabled = False


                If m_bHasSTIBins Then
                    Me.NavSTIPackagePickup.Enabled = True
                    Me.bbtnSTIPackagePickup.Enabled = True
                Else
                    Me.NavSTIPackagePickup.Enabled = False
                    Me.bbtnSTIPackagePickup.Enabled = False
                End If



            ElseIf m_oSTIUser.IsContractor Then
                Me.NavSTISampleDropoff.Enabled = False
                Me.NavSTIPackageDropoff.Enabled = False
                Me.NavSTIBinMaintenance.Enabled = False
                Me.NavSTILog.Enabled = False

                Me.bbtnSTISampleDropoff.Enabled = False
                Me.bbtnSTIPackageDropoff.Enabled = False
                Me.bbtnSTIBinMaintenance.Enabled = False
                Me.bbtnSTILog.Enabled = False

                If m_bHasSTIBins Then
                    Me.NavSTIPackagePickup.Enabled = True
                    Me.bbtnSTIPackagePickup.Enabled = True
                Else
                    Me.NavSTIPackagePickup.Enabled = False
                    Me.bbtnSTIPackagePickup.Enabled = False
                End If
            Else
                Me.NavSTISampleDropoff.Enabled = False
                Me.NavSTIPackageDropoff.Enabled = False
                Me.NavSTIPackagePickup.Enabled = False
                Me.NavSTIBinMaintenance.Enabled = False
                Me.NavSTILog.Enabled = False

                Me.bbtnSTISampleDropoff.Enabled = False
                Me.bbtnSTIPackageDropoff.Enabled = False
                Me.bbtnSTIPackagePickup.Enabled = False
                Me.bbtnSTIBinMaintenance.Enabled = False
                Me.bbtnSTILog.Enabled = False
            End If
        End If



    End Sub


#End Region

#End Region

#Region "           Dropoff Samples         "



    'Private Sub DisplaySimpleLogin(ByVal Display As Boolean)
    '    GrpActiveUser.Enabled = Not Display
    '    GrpSimpleLogin.Enabled = Display
    'End Sub

    Private WithEvents listOfSamples As SampleSubmissions

    Private Sub InitializeSampleDropoff()

        listOfSamples = New SampleSubmissions

        Me.ListBoxControl1.DataSource = listOfSamples
        Me.ListBoxControl1.DisplayMember = "Barcode"

        Me.txtSTISampleSubmission.Text = String.Empty
        Me.txtSTISampleSubmission.Focus()

        Me.txtSampleDropMessage.Text = String.Empty

        Me.btnSTISampleSubmit.Enabled = False
        'Me.GridSampleDropoff.Visible = True
        'Me.GridSampleDropoff.DataSource = listOfSamples  'New BindingList(Of SampleSubmission) With {.AllowNew = True}
        'Me.btnSampleSubmit.Visible = True

    End Sub

    'Private Sub btnSampleSubmit_Click(sender As System.Object, e As System.EventArgs) Handles btnSampleSubmit.Click
    '    Try

    '        Dim UserAccount As String = m_oSTIUser.Account
    '        Dim Workstation As String = System.Environment.MachineName
    '        Dim StiDAL As New Oracle_STI

    '        For Each CurrentSample As SampleSubmission In GridSampleDropoff.DataSource
    '            StiDAL.AddSampleSubmission(UserAccount, Workstation, CurrentSample.Barcode)
    '        Next

    '        GridSampleDropoff.DataSource = Nothing

    '    Catch err As Exception
    '        ShowMessage(err.Message)
    '    End Try
    'End Sub

    Private Sub listOfParts_AddingNew(ByVal sender As Object, ByVal e As AddingNewEventArgs) Handles listOfSamples.AddingNew
        Dim TempSampleBarcode As String = txtSTISampleSubmission.Text.Trim()
        e.NewObject = New SampleSubmission(TempSampleBarcode)

    End Sub

    Private Sub txtSTISampleSubmission_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSTISampleSubmission.KeyPress

        If btnSTISampleSubmit.Enabled AndAlso e.KeyChar = Chr(Keys.Enter) Then
            btnSTISampleSubmit.PerformClick()
        End If

    End Sub

    Private Sub txtSTISampleSubmission_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles txtSTISampleSubmission.EditValueChanged

        If txtSTISampleSubmission.Text.Trim() <> String.Empty Then
            btnSTISampleSubmit.Enabled = True
        Else
            btnSTISampleSubmit.Enabled = False
        End If

    End Sub

    Private Sub btnSTISampleSubmit_Click(sender As System.Object, e As System.EventArgs) Handles btnSTISampleSubmit.Click

        Dim TempSample As SampleSubmission = listOfSamples.AddNew()
        Dim UserAccount As String = m_oSTIUser.Account
        Dim Workstation As String = System.Environment.MachineName
        ResetInactivityTimer()

        Try
            If IsRegisteredSample(TempSample) Then
                Dim StiDAL As New Oracle_STI
                StiDAL.AddSampleSubmission(UserAccount, Workstation, TempSample.Barcode.ToUpper)
                txtSampleDropMessage.Text = "Sample submission successful. Place in dropoff tray!"
                txtSTISampleSubmission.Text = String.Empty
                btnSTISampleSubmit.Enabled = False
            Else
                listOfSamples.CancelNew(listOfSamples.IndexOf(TempSample))
                Dim TempError As String
                If TempSample.Barcode = String.Empty Then
                    TempError = "Sample barcode cannot be blank. Please enter a barcode!"
                Else
                    TempError = "Sample not registered! Please register before dropping off!"
                End If

                txtSampleDropMessage.Text = TempError
                ShowMessage(TempError)
            End If
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    'txtSTISampleSubmission


    Private Function IsRegisteredSample(ByVal Sample As SampleSubmission)
        Dim TempIsRegistered As Boolean = False
        Dim UserAccount As String = m_oSTIUser.Account

        Try
            TempIsRegistered = IsValidSingleContainer(UserAccount, Sample.Barcode)
        Catch ex As Exception
            TempIsRegistered = False
        End Try

        Return TempIsRegistered  'True
    End Function
#End Region

#Region "           Package Dropoff         "

    'On load, only display the package scan

    'On Scan, lookup package information
    'If found suggest a bin to enter the package
    'User scans the bin to verify
#Region "           Chronos Test Functions          "

    Private Function SimpleGetPackage() As Package
        Dim TempPackageBarcode As String = txtPackageDropoff.Text.Trim()
        Dim ReturnPackage As Package

        If Not String.IsNullOrEmpty(txtPackageDropoff.Text) Then

            If TempPackageBarcode.StartsWith("PKG") Then
                Dim PackageDAL As New Oracle_Package
                Dim TempPackageTable As DataTable = PackageDAL.GetBinEligiblePackageByBarcode(TempPackageBarcode)
                'Dim CurrentPackage As Package

                If TempPackageTable.Rows.Count = 1 Then
                    ReturnPackage = New Package(TempPackageTable.Rows(0))
                    TempPackageTable.Dispose()
                End If
            End If
        End If
        Return ReturnPackage
    End Function

    Private Sub btnReadyForPickup_Click(sender As System.Object, e As System.EventArgs) Handles btnReadyForPickup.Click
        Dim CurrentPackage As Package = SimpleGetPackage()
        Dim TempDropoffMessage As String = String.Empty

        If CurrentPackage Is Nothing Then
            ShowMessage("Error")
        Else
            ChronosReadyForPickupRemark(CurrentPackage.Containers, m_sDropoffPackageBarcode)
        End If

    End Sub

    Private Sub btnArrived_Click(sender As System.Object, e As System.EventArgs) Handles btnArrived.Click
        Dim CurrentPackage As Package = SimpleGetPackage()

        If CurrentPackage Is Nothing Then
            ShowMessage("Error")
        Else
            Dim TempAccount As String = "Testing"
            Dim TempMessage As String = String.Empty
            Dim TempChronosRemark As String = String.Format("Package picked up by {0}", TempAccount)
            ChronosArrivedRemark(CurrentPackage.Containers, UserSite, TempAccount, TempChronosRemark)
        End If
    End Sub

    Private Sub btnArrivedNoPhase_Click(sender As System.Object, e As System.EventArgs) Handles btnArrivedNoPhase.Click
        Dim CurrentPackage As Package = SimpleGetPackage()

        If CurrentPackage Is Nothing Then
            ShowMessage("Error")
        Else
            Dim TempAccount As String = "Testing"
            Dim TempMessage As String = String.Empty
            Dim TempChronosRemark As String = String.Format("Package picked up by {0}; No Phase change", TempAccount)
            ChronosArrivedRemark(CurrentPackage.Containers, UserSite, TempChronosRemark, False)
        End If
    End Sub

    Private Sub btnTrackingURL_Click(sender As System.Object, e As System.EventArgs) Handles btnTrackingURL.Click
        ChronosTrackingURLTest()
    End Sub
#End Region


    Private Sub InitializePackageDropoff()

        grpPackageDropoff.Visible = True
        btnPackageDropoff.Enabled = True
        txtPackageDropoff.Enabled = True
        txtPackageDropoff.Text = String.Empty
        txtPackageDropoff.Focus()

        Me.txtPckDropMessage.Text = "Scan package to begin"

        txtBinDropoff.Text = String.Empty

        grpBinDropoff.Visible = False

        m_bHasDropoffPackage = False
        m_sDropoffPackageBarcode = String.Empty
        m_oSTIPackage = Nothing

        Dim TempWorkstation As String = System.Environment.MachineName

        Try
            Dim STIDal As New Oracle_STI
            STIDal.ShowBinStatus(TempWorkstation)
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try

    End Sub


    Private Sub btnPackageDropoff_Click(sender As System.Object, e As System.EventArgs) Handles btnPackageDropoff.Click

        Dim TempPackageBarcode As String = txtPackageDropoff.Text.Trim()
        Dim TempPackageDropoffScanned As Boolean = False
        Dim TempWorkstation As String = System.Environment.MachineName

        If Not String.IsNullOrEmpty(txtPackageDropoff.Text) Then

            If TempPackageBarcode.StartsWith("PKG", StringComparison.OrdinalIgnoreCase) Then
                Dim PackageDAL As New Oracle_Package
                Dim TempPackageTable As DataTable = PackageDAL.GetBinEligiblePackageByBarcode(TempPackageBarcode)
                Dim CurrentPackage As Package

                If TempPackageTable.Rows.Count = 1 Then
                    CurrentPackage = New Package(TempPackageTable.Rows(0))


                    TempPackageTable.Dispose()

                    If SuggestBin(CurrentPackage) Then
                        TempPackageDropoffScanned = True
                        m_bHasDropoffPackage = True
                        m_sDropoffPackageBarcode = TempPackageBarcode
                        m_oSTIPackage = CurrentPackage

                        Dim STIDAL As New Oracle_STI

                        If Me.txtPckDropMessage.Text = "This shipment requires a courier. Please scan CourierBin and place the package in there." Then
                            STIDAL.ShowCourierBins(TempWorkstation)
                        Else
                            STIDAL.ShowEligibleBins(TempWorkstation, CurrentPackage)
                        End If


                        DisplayBinEntry()
                    Else
                        TempPackageDropoffScanned = False
                        m_bHasDropoffPackage = False
                        txtPackageDropoff.Text = String.Empty
                    End If
                Else
                    ShowMessage("Only finalized package can be added to bins")
                    txtPackageDropoff.Text = String.Empty
                End If

            Else
                ShowMessage("This is not a GST package")
                txtPackageDropoff.Text = String.Empty
            End If

        Else
            ShowMessage("Please enter a package barcode")
        End If

    End Sub

    Private Function SuggestBin(ByVal CurrentPackage As Package) As Boolean
        Dim HasSuggestion As Boolean = False
        Dim HasEmpty As Boolean = False
        Dim HasBackup As Boolean = False
        Dim HasBoth As Boolean = False

        Dim IsCourier As Boolean = False
        Dim TempBinSuggest As String = String.Empty
        Dim TempBinSuggestBackup As String = String.Empty

        Dim TempWorkstationName As String = System.Environment.MachineName

        Try
            Dim StiDAL As New Oracle_STI
            Dim TempWorkstation As STIWorkstation = StiDAL.GetWorkstation(TempWorkstationName)

            If String.Compare(CurrentPackage.SiteCode, TempWorkstation.SiteCode, True) = 0 Then

                If String.Compare(TempWorkstation.SiteCode, "usem", True) = 0 OrElse (TempWorkstation.SiteCode = "usca" AndAlso CurrentPackage.MailAddress.Length > 3 AndAlso TempWorkstation.MailAddress.Length > 3 AndAlso CurrentPackage.MailAddress.Substring(0, 3) = TempWorkstation.MailAddress.Substring(0, 3)) Then
                    Dim TempBins As DataTable = GridPackageDropoff.DataSource

                    For Each CurrentRow As DataRow In TempBins.Rows

                        If String.Compare(CurrentRow("BIN_NAME"), "CourierBin", True) <> 0 Then

                            If Not HasEmpty Then
                                If CurrentRow("PACKAGE_COUNT") = 0 Then
                                    TempBinSuggest = CurrentRow("BIN_NAME")
                                    'Me.txtPckDropMessage.Text = TempBinSuggest & " is empty. Please scan the bin and place the package inside."
                                    HasEmpty = True
                                End If
                            End If

                            If Not HasBackup Then
                                If Not String.IsNullOrEmpty(CurrentRow("FIRST_NAME").ToString) AndAlso Not String.IsNullOrEmpty(CurrentRow("LAST_NAME").ToString) Then
                                    If String.Compare(CurrentRow("FIRST_NAME"), CurrentPackage.FirstName, True) = 0 AndAlso String.Compare(CurrentRow("LAST_NAME"), CurrentPackage.LastName, True) = 0 Then
                                        TempBinSuggestBackup = CurrentRow("BIN_NAME")
                                        HasBackup = True
                                    End If
                                End If
                            End If

                            If HasEmpty AndAlso HasBackup Then
                                HasBoth = True
                                Exit For
                            End If
                        End If

                    Next

                    If HasBoth Then
                        Me.txtPckDropMessage.Text = TempBinSuggest & " is empty. " & TempBinSuggestBackup & " also can be used because it contains packages for the same recipient. Please scan the bin and place the package inside."
                        HasSuggestion = True
                    Else
                        If HasEmpty Then
                            Me.txtPckDropMessage.Text = TempBinSuggest & " is empty. Please scan the bin and place the package inside."
                            HasSuggestion = True
                        Else
                            If HasBackup Then
                                Me.txtPckDropMessage.Text = "No empty bins exist, however " & TempBinSuggestBackup & " can be used because it contains packages for the same recipient."
                                HasSuggestion = True
                            Else
                                ShowMessage("Run maintenance. All bins are marked as filled!")
                                HasSuggestion = False
                            End If
                        End If
                    End If
                Else
                    TempBinSuggest = "CourierBin"
                    Me.txtPckDropMessage.Text = "This shipment requires a courier. Please scan CourierBin and place the package in there."

                    HasSuggestion = True
                End If
            Else
                ShowMessage("Package is addressed to a different site. Please ship!")
                HasSuggestion = False
            End If

        Catch ex As Exception
            ShowMessage("Error determining bin. Please rescan the package")
            HasSuggestion = False
        End Try

        Return HasSuggestion
    End Function

    Private Sub txtPackageDropoff_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPackageDropoff.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnPackageDropoff.PerformClick()
        End If
    End Sub



    Private Sub DisplayBinEntry()

        btnPackageDropoff.Enabled = False
        txtPackageDropoff.Enabled = False

        grpBinDropoff.Visible = True

        txtBinDropoff.Focus()
    End Sub

    Private Sub btnBinDropoff_Click(sender As System.Object, e As System.EventArgs) Handles btnBinDropoff.Click
        Dim TempBinName As String = txtBinDropoff.Text.Trim()
        Dim TempDropoffMessage As String = String.Empty

        If Not String.IsNullOrEmpty(TempBinName) Then
            Try
                Dim TempPackageBarcode As String = m_sDropoffPackageBarcode.ToUpper
                Dim TempWorkstation As String = System.Environment.MachineName.ToUpper
                Dim TempAccount As String = m_oSTIUser.Account.ToUpper

                Dim StiDAL As New Oracle_STI
                StiDAL.AddBinContent(TempBinName, TempWorkstation, TempAccount, TempPackageBarcode)

                TempDropoffMessage = String.Format("Package: {0} added to bin {1} ", m_sDropoffPackageBarcode, TempBinName)
                Me.txtPckDropMessage.Text = TempDropoffMessage

                If String.Compare(TempBinName, "CourierBin", True) <> 0 Then
                    Dim ChronosUpdateSuccess As Boolean = False
                    ChronosUpdateSuccess = ChronosReadyForPickupRemark(m_oSTIPackage.Containers, m_sDropoffPackageBarcode)
                    If Not ChronosUpdateSuccess Then
                        ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
                    End If
                End If
                
                InitializePackageDropoff()
            Catch ex As Exception
                ShowMessage(ex.Message)
                txtBinDropoff.Text = String.Empty
            End Try
        Else
            ShowMessage("Please scan a bin")
        End If

    End Sub

    Private Sub txtBinDropoff_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBinDropoff.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnBinDropoff.PerformClick()
        End If
    End Sub


#End Region

#Region "           Package Pickup          "

    Private Sub InitializePackagePickup()
        Me.txtBinPickup.Text = String.Empty
        Me.txtBinPickup.Focus()

        If Not m_oSTIUser Is Nothing Then
            Try
                Dim STIDal As New Oracle_STI
                Dim TempWorkstation As String = System.Environment.MachineName
                If m_oSTIUser.IsContractor Then
                    STIDal.ShowPickupBinContractor(TempWorkstation)
                    GridPackagePickupHistory.Visible = False
                    grpPreviousPackageHistory.Visible = False
                Else
                    STIDal.ShowPickupBin(m_oSTIUser.Account, TempWorkstation)
                    STIDal.ShowPickupHistory(m_oSTIUser.Account)
                End If
            Catch ex As Exception
                ShowMessage(ex.Message)
            End Try
        End If

    End Sub

    Private Sub btnBinPickup_Click(sender As System.Object, e As System.EventArgs) Handles btnBinPickup.Click
        Dim TempBinName As String = txtBinPickup.Text.Trim()
        ResetInactivityTimer()

        If Not String.IsNullOrEmpty(TempBinName) Then
            Try
                Dim TempWorkstation As String = System.Environment.MachineName
                Dim TempAccount As String = m_oSTIUser.Account
                Dim TempMessage As String = String.Empty
                Dim BinPackages As Packages

                Dim StiDAL As New Oracle_STI

                'If TempBinName.StartsWith("PKG") Then
                '    TempMessage = "Remove all packages from bin containing " & TempBinName
                '    BinPackages = New Packages(StiDAL.GetPackagesInBinByPackageName(TempBinName))
                '    StiDAL.RemoveBinContentByPackage(TempBinName, TempWorkstation, TempAccount)
                'Else
                TempMessage = "Removing contents of " & TempBinName
                BinPackages = New Packages(StiDAL.GetPackagesInBinByBinName(TempBinName, TempWorkstation))
                StiDAL.RemoveBinContent(TempBinName, TempWorkstation, TempAccount, TempMessage)
                'End If

                If Not BinPackages Is Nothing Then
                    Dim TempChronosRemark As String = String.Format("Package picked up by {0}", TempAccount)

                    If String.Compare(TempBinName, "CourierBin", True) <> 0 Then
                        Dim ChronosUpdateSuccess As Boolean = False
                        ChronosUpdateSuccess = ChronosArrivedRemark(BinPackages.Containers, UserSite, m_oSTIUser.FullName, TempChronosRemark)
                        If Not ChronosUpdateSuccess Then
                            ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
                        End If
                    End If
                End If

                ShowMessage(TempMessage)
            Catch ex As Exception
                ShowMessage(ex.Message)
            End Try
        Else
            ShowMessage("Please scan a bin to pickup")
        End If

        InitializePackagePickup()
    End Sub

    Private Sub txtBinPickup_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBinPickup.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnBinPickup.PerformClick()
        End If
    End Sub

#End Region

#Region "           Bin Maintenance         "

    Private Sub InitializeBinMaintenance()
        Me.txtBinMaintain.Text = String.Empty
        Me.txtBinMaintain.Focus()

        Dim TempWorkstation As String = System.Environment.MachineName

        Try
            Dim STIDal As New Oracle_STI
            STIDal.ShowBinMaintenance(TempWorkstation)
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try

    End Sub

    Private Sub btnEmptyBin_Click(sender As System.Object, e As System.EventArgs) Handles btnEmptyBin.Click

        Dim TempBinName As String = txtBinMaintain.Text.Trim()
        ResetInactivityTimer()

        If Not String.IsNullOrEmpty(TempBinName) Then
            Try
                Dim TempWorkstation As String = System.Environment.MachineName
                Dim TempAccount As String = m_oSTIUser.Account
                Dim TempMessage As String = String.Empty

                Dim BinPackages As Packages

                Dim StiDAL As New Oracle_STI

                TempMessage = String.Format("Marking bin {0} empty", TempBinName)
                BinPackages = New Packages(StiDAL.GetPackagesInBinByBinName(TempBinName, TempWorkstation))
                StiDAL.RemoveBinContent(TempBinName, TempWorkstation, TempAccount, TempMessage)


                If Not BinPackages Is Nothing Then
                    Dim TempChronosRemark As String = String.Format("Package picked up by {0}", m_oSTIUser.FullName)

                    Dim ChronosUpdateSuccess As Boolean = False
                    ChronosUpdateSuccess = ChronosArrivedRemark(BinPackages.Containers, UserSite, m_oSTIUser.FullName, TempChronosRemark)
                    If Not ChronosUpdateSuccess Then
                        ShowMessage(" Chronos Service Call Failed. Please notify Brendan Hannigan")
                    End If

                End If

                ShowMessage(TempMessage)
            Catch ex As Exception
                ShowMessage(ex.Message)
            End Try
        Else
            ShowMessage("Please scan a bin to mark empty")
        End If

        InitializeBinMaintenance()
    End Sub

    Private Sub txtBinMaintain_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBinMaintain.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnEmptyBin.PerformClick()
        End If
    End Sub

#End Region


#Region "           STI Log             "



    Private Sub InitializeLog()
        Me.txtBinMaintain.Text = String.Empty
        Me.txtBinMaintain.Focus()

        Dim TempWorkstation As String = System.Environment.MachineName

        Try
            Dim STIDal As New Oracle_STI
            STIDal.ShowLog(TempWorkstation)
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try

    End Sub
#End Region


#End Region


#Region "           View Shipment Tab           "



    Private Sub txtViewShipment_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtViewShipment.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnContainerBarcodeLookup.PerformClick()
        End If
    End Sub

    Private Sub btnViewShipmentLookup_Click(sender As System.Object, e As System.EventArgs) Handles btnContainerBarcodeLookup.Click

        Dim TempLookupBarcode As String = txtViewShipment.Text.Trim()

        If Not String.IsNullOrEmpty(TempLookupBarcode) Then
            Try

                Dim ShipmentDAL As New Oracle_Shipment
                ShipmentDAL.ShowShipmentDetails(TempLookupBarcode)

                'Dim Export As New ImplementExportClass(RibbonControl.Manager, Nothing, GridViewShipmentDetails)
            Catch ex As Exception
                ShowMessage(ex.Message)
            End Try
        Else
            ShowMessage("Please scan a barcode to find shipment info")
        End If
    End Sub

    Private Sub dtEditFromDate_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles dtEditToDate.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            dtEditToDate.Focus()
        End If
    End Sub

    Private Sub dtEditToDate_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles dtEditToDate.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            btnSiteContainers.PerformClick()
        End If
    End Sub

    Private Sub btnSiteContainers_Click(sender As System.Object, e As System.EventArgs) Handles btnSiteContainers.Click

        Dim TempLookupSite As String = lkSites.EditValue
        Dim TempFromDate As DateTime
        Dim TempToDate As DateTime
        Dim HasError As Boolean = False

        txtViewShipment.Text = String.Empty

        If String.IsNullOrEmpty(TempLookupSite) Then
            HasError = True
        End If

        If dtEditFromDate.DateTime <= DateTime.MinValue Then
            HasError = True
        Else
            TempFromDate = dtEditFromDate.DateTime
        End If

        If dtEditFromDate.DateTime <= DateTime.MinValue Then
            HasError = True
        Else
            TempToDate = dtEditToDate.DateTime
        End If

        If Not HasError Then
            Try

                Dim ShipmentDAL As New Oracle_Shipment
                ShipmentDAL.ShowShipmentDetails(TempLookupSite, TempFromDate, TempToDate)

                'Dim Export As New ImplementExportClass(RibbonControl.Manager, Nothing, GridViewShipmentDetails)
            Catch ex As Exception
                ShowMessage(ex.Message)
            End Try
        Else
            ShowMessage("Please check that you have valid inputs")
        End If

    End Sub
#End Region








    Private Sub btnExportXSL_Click(sender As System.Object, e As System.EventArgs) Handles btnExportXSL.Click
        If Not GridViewShipmentDetails Is Nothing Then
            Dim ReportPath As String = "GSTContainerDetails.xlsx"
            GridViewShipmentDetails.ExportToXlsx(ReportPath)

            OpenExport(ReportPath)
        Else
            ShowMessage("Nothing to export!")
        End If
    End Sub

    Private Sub btnPDFExport_Click(sender As System.Object, e As System.EventArgs)
        If Not GridViewShipmentDetails Is Nothing Then
            Dim ReportPath As String = "GSTContainerDetails.pdf"
            GridViewShipmentDetails.ExportToPdf(ReportPath)


            OpenExport(ReportPath)
        Else
            ShowMessage("Nothing to export!")
        End If
    End Sub

    Private Sub btnTXTExport_Click(sender As System.Object, e As System.EventArgs) Handles btnTXTExport.Click
        If Not GridViewShipmentDetails Is Nothing Then
            Dim ReportPath As String = "GSTContainerDetails.txt"
            GridViewShipmentDetails.ExportToText(ReportPath)

            OpenExport(ReportPath)

        Else
            ShowMessage("Nothing to export!")
        End If
    End Sub

    Private Sub OpenExport(ByVal Path As String)
        Using process As New Process()
            process.StartInfo.FileName = Path
            process.Start()
            process.WaitForInputIdle()
        End Using
    End Sub





End Class

