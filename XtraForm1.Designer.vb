<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraForm1
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.INTELLISCAN_SOLUTIONS_MV3BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colREQUEST_NR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPOUND1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMATRIXBC1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBORN_ON_DATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPHASE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPROGRAM_NAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPROGRAM_CODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDA = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDATE_REQUESTED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUSERNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colREQUEST_ON_BEHALF_OF_NAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPROTOCOL_NAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTARGET = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPRIORITY_NAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCPH_TYPE_NAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDESTINATION_NAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSITE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colREQUEST_COMMENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPOUND_ARRIVED_DATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHT_EQ_SOL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMOLECULAR_WEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMOLECULAR_FORMULA = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBATCH_DATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCLOGP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSMILES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHT_KIN_SOL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDISBURSED_AMOUNT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colASSAY_REQUIRED_AMOUNT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDELTA_UL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.INTELLISCAN_RACKS_REGISTEREDBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colRACK_ID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUSER_GROUP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDTS_ISCAN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOUNT_ITEMS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colWELL_SEQ = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colWELL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPOUND = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMATRIXBC = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PERSILOG_V_PERSON_DETAILSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.XtraTabControl2 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.INTELLISCAN_SOLUTIONS_MV3BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.INTELLISCAN_RACKS_REGISTEREDBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PERSILOG_V_PERSON_DETAILSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl2.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl4.SuspendLayout()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.GridControl1
        Me.GridView3.Name = "GridView3"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.INTELLISCAN_SOLUTIONS_MV3BindingSource
        GridLevelNode1.LevelTemplate = Me.GridView3
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl1.Location = New System.Drawing.Point(308, 12)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(292, 322)
        Me.GridControl1.TabIndex = 4
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1, Me.GridView3})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colREQUEST_NR, Me.colCOMPOUND1, Me.colMATRIXBC1, Me.colBORN_ON_DATE, Me.colPHASE, Me.colPROGRAM_NAME, Me.colPROGRAM_CODE, Me.colDA, Me.colDATE_REQUESTED, Me.colUSERNAME, Me.colREQUEST_ON_BEHALF_OF_NAME, Me.colPROTOCOL_NAME, Me.colTARGET, Me.colPRIORITY_NAME, Me.colCPH_TYPE_NAME, Me.colDESTINATION_NAME, Me.colSITE, Me.colREQUEST_COMMENT, Me.colCOMPOUND_ARRIVED_DATE, Me.colHT_EQ_SOL, Me.colMOLECULAR_WEIGHT, Me.colMOLECULAR_FORMULA, Me.colBATCH_DATE, Me.colCLOGP, Me.colSMILES, Me.colHT_KIN_SOL, Me.colDISBURSED_AMOUNT, Me.colASSAY_REQUIRED_AMOUNT, Me.colDELTA_UL})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'colREQUEST_NR
        '
        Me.colREQUEST_NR.Caption = "REQUEST_NR"
        Me.colREQUEST_NR.FieldName = "REQUEST_NR"
        Me.colREQUEST_NR.Name = "colREQUEST_NR"
        Me.colREQUEST_NR.Visible = True
        Me.colREQUEST_NR.VisibleIndex = 0
        '
        'colCOMPOUND1
        '
        Me.colCOMPOUND1.Caption = "COMPOUND"
        Me.colCOMPOUND1.FieldName = "COMPOUND"
        Me.colCOMPOUND1.Name = "colCOMPOUND1"
        Me.colCOMPOUND1.Visible = True
        Me.colCOMPOUND1.VisibleIndex = 1
        '
        'colMATRIXBC1
        '
        Me.colMATRIXBC1.Caption = "MATRIXBC"
        Me.colMATRIXBC1.FieldName = "MATRIXBC"
        Me.colMATRIXBC1.Name = "colMATRIXBC1"
        Me.colMATRIXBC1.Visible = True
        Me.colMATRIXBC1.VisibleIndex = 2
        '
        'colBORN_ON_DATE
        '
        Me.colBORN_ON_DATE.Caption = "BORN_ON_DATE"
        Me.colBORN_ON_DATE.FieldName = "BORN_ON_DATE"
        Me.colBORN_ON_DATE.Name = "colBORN_ON_DATE"
        Me.colBORN_ON_DATE.Visible = True
        Me.colBORN_ON_DATE.VisibleIndex = 3
        '
        'colPHASE
        '
        Me.colPHASE.Caption = "PHASE"
        Me.colPHASE.FieldName = "PHASE"
        Me.colPHASE.Name = "colPHASE"
        Me.colPHASE.Visible = True
        Me.colPHASE.VisibleIndex = 4
        '
        'colPROGRAM_NAME
        '
        Me.colPROGRAM_NAME.Caption = "PROGRAM_NAME"
        Me.colPROGRAM_NAME.FieldName = "PROGRAM_NAME"
        Me.colPROGRAM_NAME.Name = "colPROGRAM_NAME"
        Me.colPROGRAM_NAME.Visible = True
        Me.colPROGRAM_NAME.VisibleIndex = 5
        '
        'colPROGRAM_CODE
        '
        Me.colPROGRAM_CODE.Caption = "PROGRAM_CODE"
        Me.colPROGRAM_CODE.FieldName = "PROGRAM_CODE"
        Me.colPROGRAM_CODE.Name = "colPROGRAM_CODE"
        Me.colPROGRAM_CODE.Visible = True
        Me.colPROGRAM_CODE.VisibleIndex = 6
        '
        'colDA
        '
        Me.colDA.Caption = "DA"
        Me.colDA.FieldName = "DA"
        Me.colDA.Name = "colDA"
        Me.colDA.Visible = True
        Me.colDA.VisibleIndex = 7
        '
        'colDATE_REQUESTED
        '
        Me.colDATE_REQUESTED.Caption = "DATE_REQUESTED"
        Me.colDATE_REQUESTED.FieldName = "DATE_REQUESTED"
        Me.colDATE_REQUESTED.Name = "colDATE_REQUESTED"
        Me.colDATE_REQUESTED.Visible = True
        Me.colDATE_REQUESTED.VisibleIndex = 8
        '
        'colUSERNAME
        '
        Me.colUSERNAME.Caption = "USERNAME"
        Me.colUSERNAME.FieldName = "USERNAME"
        Me.colUSERNAME.Name = "colUSERNAME"
        Me.colUSERNAME.Visible = True
        Me.colUSERNAME.VisibleIndex = 9
        '
        'colREQUEST_ON_BEHALF_OF_NAME
        '
        Me.colREQUEST_ON_BEHALF_OF_NAME.Caption = "REQUEST_ON_BEHALF_OF_NAME"
        Me.colREQUEST_ON_BEHALF_OF_NAME.FieldName = "REQUEST_ON_BEHALF_OF_NAME"
        Me.colREQUEST_ON_BEHALF_OF_NAME.Name = "colREQUEST_ON_BEHALF_OF_NAME"
        Me.colREQUEST_ON_BEHALF_OF_NAME.Visible = True
        Me.colREQUEST_ON_BEHALF_OF_NAME.VisibleIndex = 10
        '
        'colPROTOCOL_NAME
        '
        Me.colPROTOCOL_NAME.Caption = "PROTOCOL_NAME"
        Me.colPROTOCOL_NAME.FieldName = "PROTOCOL_NAME"
        Me.colPROTOCOL_NAME.Name = "colPROTOCOL_NAME"
        Me.colPROTOCOL_NAME.Visible = True
        Me.colPROTOCOL_NAME.VisibleIndex = 11
        '
        'colTARGET
        '
        Me.colTARGET.Caption = "TARGET"
        Me.colTARGET.FieldName = "TARGET"
        Me.colTARGET.Name = "colTARGET"
        Me.colTARGET.Visible = True
        Me.colTARGET.VisibleIndex = 12
        '
        'colPRIORITY_NAME
        '
        Me.colPRIORITY_NAME.Caption = "PRIORITY_NAME"
        Me.colPRIORITY_NAME.FieldName = "PRIORITY_NAME"
        Me.colPRIORITY_NAME.Name = "colPRIORITY_NAME"
        Me.colPRIORITY_NAME.Visible = True
        Me.colPRIORITY_NAME.VisibleIndex = 13
        '
        'colCPH_TYPE_NAME
        '
        Me.colCPH_TYPE_NAME.Caption = "CPH_TYPE_NAME"
        Me.colCPH_TYPE_NAME.FieldName = "CPH_TYPE_NAME"
        Me.colCPH_TYPE_NAME.Name = "colCPH_TYPE_NAME"
        Me.colCPH_TYPE_NAME.Visible = True
        Me.colCPH_TYPE_NAME.VisibleIndex = 14
        '
        'colDESTINATION_NAME
        '
        Me.colDESTINATION_NAME.Caption = "DESTINATION_NAME"
        Me.colDESTINATION_NAME.FieldName = "DESTINATION_NAME"
        Me.colDESTINATION_NAME.Name = "colDESTINATION_NAME"
        Me.colDESTINATION_NAME.Visible = True
        Me.colDESTINATION_NAME.VisibleIndex = 15
        '
        'colSITE
        '
        Me.colSITE.Caption = "SITE"
        Me.colSITE.FieldName = "SITE"
        Me.colSITE.Name = "colSITE"
        Me.colSITE.Visible = True
        Me.colSITE.VisibleIndex = 16
        '
        'colREQUEST_COMMENT
        '
        Me.colREQUEST_COMMENT.Caption = "REQUEST_COMMENT"
        Me.colREQUEST_COMMENT.FieldName = "REQUEST_COMMENT"
        Me.colREQUEST_COMMENT.Name = "colREQUEST_COMMENT"
        Me.colREQUEST_COMMENT.Visible = True
        Me.colREQUEST_COMMENT.VisibleIndex = 17
        '
        'colCOMPOUND_ARRIVED_DATE
        '
        Me.colCOMPOUND_ARRIVED_DATE.Caption = "COMPOUND_ARRIVED_DATE"
        Me.colCOMPOUND_ARRIVED_DATE.FieldName = "COMPOUND_ARRIVED_DATE"
        Me.colCOMPOUND_ARRIVED_DATE.Name = "colCOMPOUND_ARRIVED_DATE"
        Me.colCOMPOUND_ARRIVED_DATE.Visible = True
        Me.colCOMPOUND_ARRIVED_DATE.VisibleIndex = 18
        '
        'colHT_EQ_SOL
        '
        Me.colHT_EQ_SOL.Caption = "HT_EQ_SOL"
        Me.colHT_EQ_SOL.FieldName = "HT_EQ_SOL"
        Me.colHT_EQ_SOL.Name = "colHT_EQ_SOL"
        Me.colHT_EQ_SOL.Visible = True
        Me.colHT_EQ_SOL.VisibleIndex = 19
        '
        'colMOLECULAR_WEIGHT
        '
        Me.colMOLECULAR_WEIGHT.Caption = "MOLECULAR_WEIGHT"
        Me.colMOLECULAR_WEIGHT.FieldName = "MOLECULAR_WEIGHT"
        Me.colMOLECULAR_WEIGHT.Name = "colMOLECULAR_WEIGHT"
        Me.colMOLECULAR_WEIGHT.Visible = True
        Me.colMOLECULAR_WEIGHT.VisibleIndex = 20
        '
        'colMOLECULAR_FORMULA
        '
        Me.colMOLECULAR_FORMULA.Caption = "MOLECULAR_FORMULA"
        Me.colMOLECULAR_FORMULA.FieldName = "MOLECULAR_FORMULA"
        Me.colMOLECULAR_FORMULA.Name = "colMOLECULAR_FORMULA"
        Me.colMOLECULAR_FORMULA.Visible = True
        Me.colMOLECULAR_FORMULA.VisibleIndex = 21
        '
        'colBATCH_DATE
        '
        Me.colBATCH_DATE.Caption = "BATCH_DATE"
        Me.colBATCH_DATE.FieldName = "BATCH_DATE"
        Me.colBATCH_DATE.Name = "colBATCH_DATE"
        Me.colBATCH_DATE.Visible = True
        Me.colBATCH_DATE.VisibleIndex = 22
        '
        'colCLOGP
        '
        Me.colCLOGP.Caption = "CLOGP"
        Me.colCLOGP.FieldName = "CLOGP"
        Me.colCLOGP.Name = "colCLOGP"
        Me.colCLOGP.Visible = True
        Me.colCLOGP.VisibleIndex = 23
        '
        'colSMILES
        '
        Me.colSMILES.Caption = "SMILES"
        Me.colSMILES.FieldName = "SMILES"
        Me.colSMILES.Name = "colSMILES"
        Me.colSMILES.Visible = True
        Me.colSMILES.VisibleIndex = 24
        '
        'colHT_KIN_SOL
        '
        Me.colHT_KIN_SOL.Caption = "HT_KIN_SOL"
        Me.colHT_KIN_SOL.FieldName = "HT_KIN_SOL"
        Me.colHT_KIN_SOL.Name = "colHT_KIN_SOL"
        Me.colHT_KIN_SOL.Visible = True
        Me.colHT_KIN_SOL.VisibleIndex = 25
        '
        'colDISBURSED_AMOUNT
        '
        Me.colDISBURSED_AMOUNT.Caption = "DISBURSED_AMOUNT"
        Me.colDISBURSED_AMOUNT.FieldName = "DISBURSED_AMOUNT"
        Me.colDISBURSED_AMOUNT.Name = "colDISBURSED_AMOUNT"
        Me.colDISBURSED_AMOUNT.Visible = True
        Me.colDISBURSED_AMOUNT.VisibleIndex = 26
        '
        'colASSAY_REQUIRED_AMOUNT
        '
        Me.colASSAY_REQUIRED_AMOUNT.Caption = "ASSAY_REQUIRED_AMOUNT"
        Me.colASSAY_REQUIRED_AMOUNT.FieldName = "ASSAY_REQUIRED_AMOUNT"
        Me.colASSAY_REQUIRED_AMOUNT.Name = "colASSAY_REQUIRED_AMOUNT"
        Me.colASSAY_REQUIRED_AMOUNT.Visible = True
        Me.colASSAY_REQUIRED_AMOUNT.VisibleIndex = 27
        '
        'colDELTA_UL
        '
        Me.colDELTA_UL.Caption = "DELTA_UL"
        Me.colDELTA_UL.FieldName = "DELTA_UL"
        Me.colDELTA_UL.Name = "colDELTA_UL"
        Me.colDELTA_UL.Visible = True
        Me.colDELTA_UL.VisibleIndex = 28
        '
        'GridView4
        '
        Me.GridView4.GridControl = Me.GridControl2
        Me.GridView4.Name = "GridView4"
        '
        'GridControl2
        '
        Me.GridControl2.DataSource = Me.INTELLISCAN_RACKS_REGISTEREDBindingSource
        GridLevelNode2.RelationName = "INTELLISCAN_RACKS_REGISTERED_INTELLISCAN_SOLUTIONS_MV3"
        Me.GridControl2.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.GridControl2.Location = New System.Drawing.Point(12, 12)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(292, 322)
        Me.GridControl2.TabIndex = 5
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2, Me.GridView4})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colRACK_ID, Me.colUSER_GROUP, Me.colDTS_ISCAN, Me.colCOUNT_ITEMS, Me.colWELL_SEQ, Me.colWELL, Me.colCOMPOUND, Me.colMATRIXBC})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        '
        'colRACK_ID
        '
        Me.colRACK_ID.Caption = "RACK_ID"
        Me.colRACK_ID.FieldName = "RACK_ID"
        Me.colRACK_ID.Name = "colRACK_ID"
        Me.colRACK_ID.Visible = True
        Me.colRACK_ID.VisibleIndex = 0
        '
        'colUSER_GROUP
        '
        Me.colUSER_GROUP.Caption = "USER_GROUP"
        Me.colUSER_GROUP.FieldName = "USER_GROUP"
        Me.colUSER_GROUP.Name = "colUSER_GROUP"
        Me.colUSER_GROUP.Visible = True
        Me.colUSER_GROUP.VisibleIndex = 1
        '
        'colDTS_ISCAN
        '
        Me.colDTS_ISCAN.Caption = "DTS_ISCAN"
        Me.colDTS_ISCAN.FieldName = "DTS_ISCAN"
        Me.colDTS_ISCAN.Name = "colDTS_ISCAN"
        Me.colDTS_ISCAN.Visible = True
        Me.colDTS_ISCAN.VisibleIndex = 2
        '
        'colCOUNT_ITEMS
        '
        Me.colCOUNT_ITEMS.Caption = "COUNT_ITEMS"
        Me.colCOUNT_ITEMS.FieldName = "COUNT_ITEMS"
        Me.colCOUNT_ITEMS.Name = "colCOUNT_ITEMS"
        Me.colCOUNT_ITEMS.Visible = True
        Me.colCOUNT_ITEMS.VisibleIndex = 3
        '
        'colWELL_SEQ
        '
        Me.colWELL_SEQ.Caption = "WELL_SEQ"
        Me.colWELL_SEQ.FieldName = "WELL_SEQ"
        Me.colWELL_SEQ.Name = "colWELL_SEQ"
        Me.colWELL_SEQ.Visible = True
        Me.colWELL_SEQ.VisibleIndex = 4
        '
        'colWELL
        '
        Me.colWELL.Caption = "WELL"
        Me.colWELL.FieldName = "WELL"
        Me.colWELL.Name = "colWELL"
        Me.colWELL.Visible = True
        Me.colWELL.VisibleIndex = 5
        '
        'colCOMPOUND
        '
        Me.colCOMPOUND.Caption = "COMPOUND"
        Me.colCOMPOUND.FieldName = "COMPOUND"
        Me.colCOMPOUND.Name = "colCOMPOUND"
        Me.colCOMPOUND.Visible = True
        Me.colCOMPOUND.VisibleIndex = 6
        '
        'colMATRIXBC
        '
        Me.colMATRIXBC.Caption = "MATRIXBC"
        Me.colMATRIXBC.FieldName = "MATRIXBC"
        Me.colMATRIXBC.Name = "colMATRIXBC"
        Me.colMATRIXBC.Visible = True
        Me.colMATRIXBC.VisibleIndex = 7
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup3
        Me.LayoutControl1.Size = New System.Drawing.Size(180, 120)
        Me.LayoutControl1.TabIndex = 0
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup4
        Me.LayoutControl2.Size = New System.Drawing.Size(180, 120)
        Me.LayoutControl2.TabIndex = 0
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.PanelControl1)
        Me.LayoutControl3.Controls.Add(Me.XtraTabControl2)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.Root = Me.LayoutControlGroup1
        Me.LayoutControl3.Size = New System.Drawing.Size(850, 398)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "LayoutControl3"
        '
        'PanelControl1
        '
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(204, 374)
        Me.PanelControl1.TabIndex = 5
        '
        'XtraTabControl2
        '
        Me.XtraTabControl2.Location = New System.Drawing.Point(220, 12)
        Me.XtraTabControl2.Name = "XtraTabControl2"
        Me.XtraTabControl2.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl2.Size = New System.Drawing.Size(618, 374)
        Me.XtraTabControl2.TabIndex = 4
        Me.XtraTabControl2.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.LayoutControl4)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(612, 346)
        Me.XtraTabPage1.Text = "XtraTabPage1"
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.GridControl2)
        Me.LayoutControl4.Controls.Add(Me.GridControl1)
        Me.LayoutControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.Root = Me.LayoutControlGroup2
        Me.LayoutControl4.Size = New System.Drawing.Size(612, 346)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl4"
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CustomizationFormText = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem4})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(612, 346)
        Me.LayoutControlGroup2.Text = "LayoutControlGroup2"
        Me.LayoutControlGroup2.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.GridControl1
        Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(296, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(296, 326)
        Me.LayoutControlItem3.Text = "LayoutControlItem3"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextToControlDistance = 0
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.GridControl2
        Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(296, 326)
        Me.LayoutControlItem4.Text = "LayoutControlItem4"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextToControlDistance = 0
        Me.LayoutControlItem4.TextVisible = False
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(612, 346)
        Me.XtraTabPage2.Text = "XtraTabPage2"
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(850, 398)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.XtraTabControl2
        Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(208, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(622, 378)
        Me.LayoutControlItem1.Text = "LayoutControlItem1"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextToControlDistance = 0
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.PanelControl1
        Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(208, 378)
        Me.LayoutControlItem2.Text = "LayoutControlItem2"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextToControlDistance = 0
        Me.LayoutControlItem2.TextVisible = False
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.Size = New System.Drawing.Size(300, 300)
        Me.XtraTabControl1.TabIndex = 0
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.CustomizationFormText = "LayoutControlGroup3"
        Me.LayoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup3.GroupBordersVisible = False
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(180, 120)
        Me.LayoutControlGroup3.Text = "LayoutControlGroup3"
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.CustomizationFormText = "LayoutControlGroup4"
        Me.LayoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup4.GroupBordersVisible = False
        Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(180, 120)
        Me.LayoutControlGroup4.Text = "LayoutControlGroup4"
        '
        'XtraForm1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(850, 398)
        Me.Controls.Add(Me.LayoutControl3)
        Me.Name = "XtraForm1"
        Me.Text = "XtraForm1"
        Me.TopMost = True
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.INTELLISCAN_SOLUTIONS_MV3BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.INTELLISCAN_RACKS_REGISTEREDBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PERSILOG_V_PERSON_DETAILSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl2.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl4.ResumeLayout(False)
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents XtraTabControl2 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents INTELLISCAN_RACKS_REGISTEREDBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents colRACK_ID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUSER_GROUP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDTS_ISCAN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOUNT_ITEMS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colWELL_SEQ As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colWELL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPOUND As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMATRIXBC As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents INTELLISCAN_SOLUTIONS_MV3BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents colREQUEST_NR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPOUND1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMATRIXBC1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBORN_ON_DATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPHASE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPROGRAM_NAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPROGRAM_CODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDATE_REQUESTED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUSERNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colREQUEST_ON_BEHALF_OF_NAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPROTOCOL_NAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTARGET As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPRIORITY_NAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCPH_TYPE_NAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDESTINATION_NAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSITE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colREQUEST_COMMENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPOUND_ARRIVED_DATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHT_EQ_SOL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMOLECULAR_WEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMOLECULAR_FORMULA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBATCH_DATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCLOGP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSMILES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHT_KIN_SOL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDISBURSED_AMOUNT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colASSAY_REQUIRED_AMOUNT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDELTA_UL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PERSILOG_V_PERSON_DETAILSBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents PERSILOG_V_PERSON_DETAILSTableAdapter As GTS.DataSet1TableAdapters.PERSILOG_V_PERSON_DETAILSTableAdapter
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
End Class
