﻿Imports System.Web.Services.Protocols
Imports GST.FedExTrackingService


Public Class FedExServiceCalls

    Private Function CreateTrackRequest(ByVal ShipmentReference As String, ByVal ShipmentDestZip As String, ByVal ShipmentCountryCode As String) As TrackRequest
        'Build the TrackRequest
        Dim request As TrackRequest = New TrackRequest()

        request.WebAuthenticationDetail = New WebAuthenticationDetail()
        request.WebAuthenticationDetail.UserCredential = New WebAuthenticationCredential()
        request.WebAuthenticationDetail.UserCredential.Key = "XXX" ' Replace "XXX" with the Key
        request.WebAuthenticationDetail.UserCredential.Password = "XXX" ' Replace "XXX" with the Password

        request.ClientDetail = New ClientDetail()
        request.ClientDetail.AccountNumber = "XXX" ' Replace "XXX" with client's account number
        request.ClientDetail.MeterNumber = "XXX"   ' Replace "XXX" with client's meter number

        request.TransactionDetail = New TransactionDetail()
        request.TransactionDetail.CustomerTransactionId = Guid.NewGuid().ToString  'The client will get the same value back in the response

        request.Version = New VersionId()

        request.PackageIdentifier = New TrackPackageIdentifier() 'Tracking information

        request.PackageIdentifier.Type = TrackIdentifierType.FREE_FORM_REFERENCE
        request.PackageIdentifier.Value = ShipmentReference ' "CPD: 0033930"


        request.ShipDateRangeBeginSpecified = False
        request.ShipDateRangeEndSpecified = False

        request.IncludeDetailedScans = True ' Optional Use if all scans should be returned
        request.IncludeDetailedScansSpecified = True

        request.Destination = New Address()
        request.Destination.CountryCode = ShipmentCountryCode


        If ShipmentCountryCode = "US" Then
            request.Destination.PostalCode = ShipmentDestZip.Substring(0, 5)
        ElseIf ShipmentCountryCode = "TW" OrElse ShipmentCountryCode = "CN" OrElse ShipmentCountryCode = "PT" Then
            'Do NOthing (these coutries don't have or use postal codes)
        Else
            request.Destination.PostalCode = ShipmentDestZip
        End If

        Return request
    End Function

    Public Function GetTrackingInfo(ByVal CurrentShipment As Shipment) As FedExResponse

        Dim ShipmentReference As String = CurrentShipment.ExternalReference
        Dim ShipmentZip As String = CurrentShipment.ReceiverPostalcode
        Dim ShipmentCountryCode As String = GetCountryCode(CurrentShipment.ReceiverCountryName)
        Dim ShipmentTrackingCode As String = String.Empty

        Dim ShipmentInfo As FedExResponse

        Dim request As TrackRequest = CreateTrackRequest(ShipmentReference, ShipmentZip, ShipmentCountryCode)
        Dim service As TrackService = New TrackService()
        Dim IsSuccessful As Boolean = False

        Try
            Dim reply As TrackReply = service.track(request)
            ShipmentInfo = New FedExResponse(reply)

        Catch se As SoapException
            IsSuccessful = False
            Console.WriteLine(se.Detail.ToString())
        Catch e As Exception
            IsSuccessful = False
            Console.WriteLine(e.Message)
        End Try

        Return ShipmentInfo
    End Function

    'Public Function FakeTrackingInfo(ByVal CurrentShipment As Shipment) As String

    '    Dim ShipmentReference As String = CurrentShipment.ExternalReference  '"CPD: 0033930" ' CurrentShipment.ShipmentName String.Empty
    '    Dim ShipmentZip As String = CurrentShipment.ReceiverPostalcode ' "07936"              'CurrentShipment.ShipmentName String.Empty
    '    Dim ShipmentTrackingCode As String = String.Empty

    '    If ShipmentReference = "GST0000000002" Then
    '        ShipmentTrackingCode = "467509212641"
    '    End If

    '    Return ShipmentTrackingCode
    'End Function

    Private Shared Function ParseTrackingNumber(ByVal TrackReply As TrackReply) As String
        Dim TrackingCode As String = String.Empty
        If (Not TrackReply.TrackDetails Is Nothing) Then
            For Each trackDetail As TrackDetail In TrackReply.TrackDetails
                If Not String.IsNullOrEmpty(Trim(trackDetail.TrackingNumber)) Then
                    ' HasTrackingCode = True
                    TrackingCode = trackDetail.TrackingNumber
                    Exit For
                End If
            Next
        End If
        Return TrackingCode
    End Function

    Public Function GetCountryCode(ByVal CountryName As String) As String
        Dim CurrentCountryCode As String = String.Empty

        Select Case CountryName
            Case "United States", "USA", "US", "U.S."
                CurrentCountryCode = "US"
            Case "UK", "GB"
                CurrentCountryCode = "GB"
            Case "The Netherlands"
                CurrentCountryCode = "NL"
            Case "Taiwan", "TAIWAN"
                CurrentCountryCode = "TW"
                'No POstal Code
            Case "Switzerland"
                CurrentCountryCode = "CH"
            Case "Republic of China"
                CurrentCountryCode = "CN"
                'NO PostalCode
            Case "Portugal"
                CurrentCountryCode = "PT"
            Case "Italy"
                CurrentCountryCode = "IT"
            Case "Germany"
                CurrentCountryCode = "DE"
            Case "France"
                CurrentCountryCode = "FR"
            Case "Canada"
                CurrentCountryCode = "CA"
            Case "Brasil"
                CurrentCountryCode = "BR"
            Case "Belgium"
                CurrentCountryCode = "BE"
            Case "Australia"
                CurrentCountryCode = "AU"
            Case Else
                CurrentCountryCode = "US"

        End Select

        Return CurrentCountryCode
    End Function
End Class
