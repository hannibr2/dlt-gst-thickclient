﻿Imports Oracle.ManagedDataAccess.Client

Public Class Oracle_STI
    Inherits Oracle_General

    ''' <summary>
    ''' Show bin status for workstation
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowBinStatus(ByVal Workstation As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select bin_name, workstation, mail_address, site_code, site, package_count, last_name, first_name " & _
                    "from v_bin_status " & _
                    "where upper(workstation) = '" & Workstation.ToUpper() & "' "


            m_DataSet = ExecuteQuery(query)
            frmMain.GridPackageDropoff.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting dropoff bin status")
        End Try

    End Sub

    ''' <summary>
    ''' Show eligible bins for workstation and user
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowEligibleBins(ByVal Workstation As String, ByVal Package As Package)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select bin_name, workstation, mail_address, site_code, site, package_count, last_name, first_name " & _
                    "from v_bin_status " & _
                    "where upper(workstation) = '" & Workstation.ToUpper() & "' and upper(bin_name) <> 'COURIERBIN' " & _
                    "      and ( package_count = 0 or ( last_name = q'[" & Package.LastName & "]' and first_name = q'[" & Package.FirstName & "]' ) ) " & _
                    "order by last_name, bin_name "


            m_DataSet = ExecuteQuery(query)
            frmMain.GridPackageDropoff.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting eligible bins")
        End Try

    End Sub

    ''' <summary>
    ''' Show eligible bins for workstation and user
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ShowCourierBins(ByVal Workstation As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select bin_name, workstation, mail_address, site_code, site, package_count, last_name, first_name " & _
                    "from v_bin_status " & _
                    "where upper(workstation) = '" & Workstation.ToUpper() & "' " & _
                    "      and upper(bin_name) = 'COURIERBIN' "

            m_DataSet = ExecuteQuery(query)
            frmMain.GridPackageDropoff.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting courier bin")
        End Try

    End Sub

    Public Sub ShowPickupBin(ByVal Username As String, ByVal Workstation As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try

            query = "select bd.bin_name, bd.barcode as package_barcode, bd.first_name || ' ' || bd.last_name as package_recipient, ar.priority " & _
                    "from v_bin_detail bd inner join " & _
                    "v_sti_account_relationship ar on bd.receiver_account = ar.account " & _
                    "where upper(bd.workstation) = '" & Workstation.ToUpper() & "' " & _
                    "and upper(base_user_name) = '" & Username.ToUpper & "' " & _
                    "order by ar.priority "

            m_DataSet = ExecuteQuery(query)
            frmMain.GridPackagePickup.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting pickup bin status")
        End Try

    End Sub

    Public Sub ShowPickupHistory(ByVal Username As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try

            query = "select recipient_first_name || ' ' || recipient_last_name as intended_recipient, first_name || ' ' || last_name as actual_recipient, barcode, date_created " & _
                    "from(v_sti_history_detail) " & _
                    "where(date_created > sysdate - 14) " & _
                    "    and type_id = 150006 " & _
                    "    and upper(recipient_account) = '" & Username.ToUpper & "' " & _
                    "order by date_created "

            m_DataSet = ExecuteQuery(query)
            frmMain.GridPackagePickupHistory.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting pickup bin status")
        End Try

    End Sub

    Public Sub ShowLog(ByVal Workstation As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try

            query = " select shd.account, shd.last_name, shd.first_name, shd.note,  shd.workstation,  shd.site,  shd.barcode,  shd.recipient_last_name,  shd.recipient_first_name, shd.recipient_account,  shd.date_created " & _
                    " from v_sti_history_detail shd" & _
                    "   inner join t_sti_workstation w on w.site_code = shd.site_code " & _
                    " where upper( w.workstation ) = '" & Workstation.ToUpper & "'"

            m_DataSet = ExecuteQuery(query)
            frmMain.GridSTILog.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting STI Log")
        End Try

    End Sub

    Public Sub ShowBinMaintenance(ByVal Workstation As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try

            query = "select bd.bin_name, bd.barcode as package_barcode, bd.first_name || ' ' || bd.last_name as package_recipient " & _
                    "from v_bin_detail bd " & _
                    "where upper(bd.workstation) = '" & Workstation.ToUpper() & "' "

            m_DataSet = ExecuteQuery(query)
            frmMain.GridBinMaintenance.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting pickup bin status")
        End Try

    End Sub


    Public Sub ShowPickupBinContractor(ByVal Workstation As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try

            query = "select bd.bin_name, bd.barcode as package_barcode, bd.first_name || ' ' || bd.last_name as package_recipient, 1 as priority " & _
                    "from v_bin_detail bd " & _
                    "where upper(bd.workstation) = '" & Workstation.ToUpper() & "'"

            m_DataSet = ExecuteQuery(query)
            frmMain.GridPackagePickup.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting pickup bin status")
        End Try

    End Sub

    Public Function VerifySTIUser(ByVal Username As String) As STIUser

        Dim query As String
        Dim m_DataSet As DataSet
        Dim m_StiUser As STIUser
        Try

           
            query = "select account, first_name, last_name, company, email, person_type from v_sti_user where leaving_date is null and upper(account) = '" & Username.ToUpper() & "'"

            m_DataSet = ExecuteQuery(query)

            If m_DataSet.Tables.Count > 0 And m_DataSet.Tables(0).Rows.Count = 1 Then
                m_StiUser = New STIUser(m_DataSet.Tables(0).Rows(0))
            Else
                m_StiUser = Nothing
                Throw New Exception()
            End If

            Return m_StiUser

        Catch ex As Exception
            Throw New Exception("Error getting STI user")
        End Try

    End Function

    Public Function GetWorkstation(ByVal Workstation As String) As STIWorkstation
        Dim query As String
        Dim m_DataSet As DataSet
        Dim m_StiWorkstation As STIWorkstation
        Try


            query = "select workstation, mail_address, site_code, site from t_sti_workstation where  upper(workstation) = '" & Workstation.ToUpper() & "'"

            m_DataSet = ExecuteQuery(query)

            If m_DataSet.Tables.Count > 0 And m_DataSet.Tables(0).Rows.Count = 1 Then
                m_StiWorkstation = New STIWorkstation(m_DataSet.Tables(0).Rows(0))
            Else
                m_StiWorkstation = Nothing
                Throw New Exception()
            End If

            Return m_StiWorkstation

        Catch ex As Exception
            Throw New Exception("Error getting STI user")
        End Try
    End Function

    Public Sub AddSampleSubmission(ByVal Account As String, ByVal WorkStation As String, ByVal Barcode As String)

        Dim Note As String = "Sample Submission " & Barcode
        Dim TypeID As Integer = 150001

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction


        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_sti.addstiaction", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_account", OracleDbType.Varchar2).Value = Account
        m_command.Parameters.Add("i_workstation", OracleDbType.Varchar2).Value = WorkStation
        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = Barcode
        m_command.Parameters.Add("i_type_id", OracleDbType.Int64).Value = TypeID
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Note

        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            Throw New Exception("Error adding sample submission - " + m_command.Parameters("o_error_msg").Value.ToString())
        End If

    End Sub

    Public Sub AddBinContent(ByVal BinName As String, Workstation As String, ByVal Account As String, ByVal PackageBarcode As String)

        Dim Note As String = "Adding " & PackageBarcode & " to " & BinName
        'Dim TypeID As Integer = 150001

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction


        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_sti.addbincontent", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_bin_name", OracleDbType.Varchar2).Value = BinName
        m_command.Parameters.Add("i_workstation", OracleDbType.Varchar2).Value = Workstation
        m_command.Parameters.Add("i_account", OracleDbType.Varchar2).Value = Account
        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = PackageBarcode
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Note

        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            Throw New Exception("Error adding content to bin - " + m_command.Parameters("o_error_msg").Value.ToString())
        End If

    End Sub

    Public Sub RemoveBinContent(ByVal BinName As String, Workstation As String, ByVal Account As String, ByVal Note As String)

        'Dim Note As String = "Removing contents of bin " & BinName

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction


        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_sti.removebincontent", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_bin_name", OracleDbType.Varchar2).Value = BinName
        m_command.Parameters.Add("i_workstation", OracleDbType.Varchar2).Value = Workstation
        m_command.Parameters.Add("i_account", OracleDbType.Varchar2).Value = Account
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Note

        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            Throw New Exception("Error removing content from bin - " + m_command.Parameters("o_error_msg").Value.ToString())
        End If

    End Sub

    Public Sub RemoveBinContentByPackage(ByVal PackageBarcode As String, Workstation As String, ByVal Account As String)

        Dim Note As String = "Removing package " & PackageBarcode & " from bins"

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction


        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_sti.removebincontentbypackage", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = PackageBarcode
        m_command.Parameters.Add("i_workstation", OracleDbType.Varchar2).Value = Workstation
        m_command.Parameters.Add("i_account", OracleDbType.Varchar2).Value = Account
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = String.Empty

        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            Throw New Exception("Error removing content from bin - " + m_command.Parameters("o_error_msg").Value.ToString())
        End If

    End Sub

    Public Function GetPackagesInBinByBinName(ByVal BinName As String, ByVal Workstation As String) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try

            query = "select parent_container_id, barcode, first_name, last_name, company, country_name, mail_address, site_code, phase_id " & _
                "from v_bin_detail where upper(bin_name) = '" + BinName.ToUpper + "' and upper(workstation) = '" + Workstation.ToUpper + "'"


            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)

        Catch ex As Exception
            Throw New Exception("Error getting packages by bin name")
        End Try
    End Function

    Public Function GetPackagesInBinByPackageName(ByVal PackageName As String) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try

            query = "select bd2.parent_container_id, bd2.barcode, bd2.first_name, bd2.last_name, bd2.company, bd2.country_name, bd2.mail_address, bd2.site_code, bd2.phase_id " & _
                "from v_bin_detail bd1 inner join " & _
                "        v_bin_detail bd2 on bd1.bin_name = bd2.bin_name and bd1.workstation = bd2.workstation " & _
                "where upper (bd1.barcode) = '" + PackageName.ToUpper + "'"


            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)

        Catch ex As Exception
            Throw New Exception("Error getting packages in bin by package name")
        End Try
    End Function

    Public Function GetBinCount(ByVal Workstation As String) As Integer

        Dim query As String
        Dim m_DataSet As DataSet
        Dim binCount As Integer = -1

        Try
            query = "select bin_count from v_sti_machine where upper(workstation) = '" & Workstation.ToUpper & "'"


            m_DataSet = ExecuteQuery(query)

            If m_DataSet.Tables(0).Rows.Count = 0 Then
                binCount = -1
            Else
                binCount = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
            End If

            Return binCount
        Catch err As Exception
            Throw New Exception("Error getting bin count")
            Return -1
        End Try

    End Function

    Public Sub New()
        MyBase.New()
    End Sub
End Class
