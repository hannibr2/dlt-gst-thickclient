﻿Imports System.IO
Imports System.Environment

Module Configuration

    Private m_UserAccount As String
    Private m_CostCenter As String
    Private m_Printer As String
    Private m_PrinterDPI As String
    Private m_PrinterPort As String
    Private m_LabelTemplatePath As String
    Private m_ShipmentTemplatePath As String
    Private m_Environment As String
    Private m_FulfillmentSite As String
    'Private m_DefaultLogin As String

    Private m_FedExAccount As String
    Private m_QuickIntlAccount As String



    'Private m_ConfigurationPath As String = My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData + "\SOA.cfg"
    Private m_ConfigurationPath As String = "C:\NIBR\GST\"


    Public Property UserAccount() As String
        Get
            Return m_UserAccount
        End Get
        Set(ByVal value As String)
            m_UserAccount = value
        End Set
    End Property

    Public Property CostCenter() As String
        Get
            Return m_CostCenter
        End Get
        Set(ByVal value As String)
            m_CostCenter = value
        End Set
    End Property

    Public Property Printer() As String
        Get
            Return m_Printer
        End Get
        Set(ByVal value As String)
            m_Printer = value
        End Set
    End Property

    Public Property PrinterDPI() As String
        Get
            Return m_PrinterDPI
        End Get
        Set(ByVal value As String)
            m_PrinterDPI = value
        End Set
    End Property

    Public Property PrinterPort() As String
        Get
            Return m_PrinterPort
        End Get
        Set(ByVal value As String)
            m_PrinterPort = value
        End Set
    End Property

    Public Property LabelTemplatePath() As String
        Get
            Return m_LabelTemplatePath
        End Get
        Set(ByVal value As String)
            m_LabelTemplatePath = value
        End Set
    End Property

    Public Property ShipmentTemplatePath() As String
        Get
            Return m_ShipmentTemplatePath
        End Get
        Set(ByVal value As String)
            m_ShipmentTemplatePath = value
        End Set
    End Property

    'Public Property Environment() As String
    '    Get
    '        Return m_Environment
    '    End Get
    '    Set(ByVal value As String)
    '        m_Environment = value
    '    End Set
    'End Property

    Public Property FulfillmentSite As String
        Get
            Return m_FulfillmentSite
        End Get
        Set(value As String)
            m_FulfillmentSite = value
        End Set
    End Property

    Public Property FedExAccount As String
        Get
            Return m_FedExAccount
        End Get
        Set(value As String)
            m_FedExAccount = value
        End Set
    End Property

    Public Property QuickIntlAccount As String
        Get
            Return m_QuickIntlAccount
        End Get
        Set(value As String)
            m_QuickIntlAccount = value
        End Set
    End Property

    'Public Property DefaultLogin As String
    '    Get
    '        Return m_DefaultLogin
    '    End Get
    '    Set(value As String)
    '        m_DefaultLogin = value
    '    End Set
    'End Property



    Public Sub SaveConfiguration()

        Dim ConfigurationName As String = m_ConfigurationPath + m_UserAccount + ".cfg"
        Dim TempFile As TextWriter = New StreamWriter(ConfigurationName)
        TempFile.WriteLine("----- User Configuration for Global Shipping and Tracking -----")
        TempFile.Close()

        Dim SW As StreamWriter
        SW = File.AppendText(ConfigurationName)
        SW.WriteLine("UserAccount=" + m_UserAccount)
        SW.WriteLine("CostCenter=" + m_CostCenter)
        SW.WriteLine("Printer=" + m_Printer)
        SW.WriteLine("DPI=" + m_PrinterDPI)
        SW.WriteLine("Port=" + m_PrinterPort)
        SW.WriteLine("LabelTemplatePath=" + m_LabelTemplatePath)
        SW.WriteLine("ShipmentTemplatePath=" + m_ShipmentTemplatePath)
        SW.WriteLine("Environment=" + m_Environment)
        SW.WriteLine("FulfillmentSite=" + m_FulfillmentSite)
        SW.WriteLine("FedExAccount=" + m_FedExAccount)
        SW.WriteLine("QuickIntlAccount=" + m_QuickIntlAccount)
        'SW.WriteLine("DefaultLogin=" + m_DefaultLogin)

        SW.Close()

    End Sub

    Public Sub LoadConfiguration(ByVal Username As String)
        Dim ConfigurationName As String = m_ConfigurationPath + Username + ".cfg"
        If (File.Exists(ConfigurationName)) Then
            Dim TempStream As String()
            Dim SR As StreamReader
            SR = File.OpenText(ConfigurationName)
            Dim Header As String = SR.ReadLine()

            TempStream = SR.ReadToEnd().Split(vbCrLf.ToCharArray())
            'TempLine = SR.ReadLine()
            For Each TempLine As String In TempStream
                If InStr(TempLine, "UserAccount", CompareMethod.Text) <> 0 Then
                    m_UserAccount = Mid(TempLine, 13)
                ElseIf InStr(TempLine, "CostCenter", CompareMethod.Text) <> 0 Then
                    m_CostCenter = Mid(TempLine, 12)
                ElseIf InStr(TempLine, "Printer", CompareMethod.Text) <> 0 Then
                    m_Printer = Mid(TempLine, 9)
                ElseIf InStr(TempLine, "DPI", CompareMethod.Text) <> 0 Then
                    m_PrinterDPI = Mid(TempLine, 5)
                ElseIf InStr(TempLine, "Port", CompareMethod.Text) <> 0 Then
                    m_PrinterPort = Mid(TempLine, 6)
                ElseIf InStr(TempLine, "LabelTemplatePath", CompareMethod.Text) <> 0 Then
                    m_LabelTemplatePath = Mid(TempLine, 19)
                ElseIf InStr(TempLine, "ShipmentTemplatePath", CompareMethod.Text) <> 0 Then
                    m_ShipmentTemplatePath = Mid(TempLine, 22)
                ElseIf InStr(TempLine, "Environment", CompareMethod.Text) <> 0 Then
                    m_Environment = Mid(TempLine, 13)
                ElseIf InStr(TempLine, "FulfillmentSite", CompareMethod.Text) <> 0 Then
                    m_FulfillmentSite = Mid(TempLine, 17)
                ElseIf InStr(TempLine, "FedExAccount", CompareMethod.Text) <> 0 Then
                    m_FedExAccount = Mid(TempLine, 14)
                ElseIf InStr(TempLine, "QuickIntlAccount", CompareMethod.Text) <> 0 Then
                    m_QuickIntlAccount = Mid(TempLine, 18)
                    'ElseIf InStr(TempLine, "DefaultLogin", CompareMethod.Text) <> 0 Then
                    '    m_DefaultLogin = Mid(TempLine, 14)
                End If

            Next
            SR.Close()



        Else

            m_UserAccount = ""
            m_CostCenter = ""
            m_Printer = ""
            m_PrinterDPI = "600"
            m_PrinterPort = ""
            m_FulfillmentSite = "Cambridge United States"
            m_FedExAccount = ""
            m_QuickIntlAccount = ""
        End If

        If String.IsNullOrEmpty(m_FulfillmentSite) Then
            m_FulfillmentSite = "Cambridge United States"
        End If

        Dim ContainerDAL As New Oracle_Container
        ContainerDAL.GetFulfillmentSite(m_FulfillmentSite)

        'If String.IsNullOrEmpty(m_DefaultLogin) Then
        '    m_DefaultLogin = "General"
        'End If

        'ContainerDAL.GetDefaultLogin(m_DefaultLogin)
    End Sub

End Module
