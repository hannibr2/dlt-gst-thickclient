﻿Public Class STIWorkstation


    Private m_workstation As String
    Private m_mail_address As String
    Private m_site_code As String
    Private m_site As String

    Public Property Workstation As String
        Get
            Return m_workstation
        End Get
        Set(value As String)
            If m_workstation = value Then
                Return
            End If
            m_workstation = value
        End Set
    End Property
    Public Property MailAddress As String
        Get
            Return m_mail_address
        End Get
        Set(value As String)
            If m_mail_address = value Then
                Return
            End If
            m_mail_address = value
        End Set
    End Property
    Public Property SiteCode As String
        Get
            Return m_site_code
        End Get
        Set(value As String)
            If m_site_code = value Then
                Return
            End If
            m_site_code = value
        End Set
    End Property
    Public Property Site As String
        Get
            Return m_site
        End Get
        Set(value As String)
            If m_site = value Then
                Return
            End If
            m_site = value
        End Set
    End Property



    Public Sub New(ByVal WorkstationRow As DataRow)

        If Not IsDBNull(WorkstationRow.Item("workstation")) Then
            Workstation = WorkstationRow.Item("workstation")
        End If

        If Not IsDBNull(WorkstationRow.Item("mail_address")) Then
            MailAddress = WorkstationRow.Item("mail_address")
        End If

        If Not IsDBNull(WorkstationRow.Item("site_code")) Then
            SiteCode = WorkstationRow.Item("site_code")
        End If

        If Not IsDBNull(WorkstationRow.Item("site")) Then
            Site = WorkstationRow.Item("site")
        End If

    End Sub





End Class
