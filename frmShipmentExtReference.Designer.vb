<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmShipmentExtReference
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSetShipmentName = New DevExpress.XtraEditors.SimpleButton()
        Me.txtExternalReference = New DevExpress.XtraEditors.TextEdit()
        Me.lblExternalReference = New DevExpress.XtraEditors.LabelControl()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExternalReference.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.ExpandCollapseItem.Name = ""
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 1
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Size = New System.Drawing.Size(402, 27)
        Me.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(12, 78)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(107, 26)
        Me.btnOK.TabIndex = 20
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(283, 78)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(107, 26)
        Me.btnCancel.TabIndex = 24
        Me.btnCancel.Text = "Cancel"
        '
        'btnSetShipmentName
        '
        Me.btnSetShipmentName.Location = New System.Drawing.Point(125, 78)
        Me.btnSetShipmentName.Name = "btnSetShipmentName"
        Me.btnSetShipmentName.Size = New System.Drawing.Size(152, 26)
        Me.btnSetShipmentName.TabIndex = 33
        Me.btnSetShipmentName.Text = "Reset to Shipment Name"
        '
        'txtExternalReference
        '
        Me.txtExternalReference.EditValue = ""
        Me.txtExternalReference.Location = New System.Drawing.Point(127, 48)
        Me.txtExternalReference.Name = "txtExternalReference"
        Me.txtExternalReference.Size = New System.Drawing.Size(263, 20)
        Me.txtExternalReference.TabIndex = 34
        '
        'lblExternalReference
        '
        Me.lblExternalReference.Location = New System.Drawing.Point(26, 51)
        Me.lblExternalReference.Name = "lblExternalReference"
        Me.lblExternalReference.Size = New System.Drawing.Size(93, 13)
        Me.lblExternalReference.TabIndex = 36
        Me.lblExternalReference.Text = "External Reference"
        '
        'frmShipmentExtReference
        '
        Me.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(402, 116)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblExternalReference)
        Me.Controls.Add(Me.txtExternalReference)
        Me.Controls.Add(Me.btnSetShipmentName)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.RibbonControl)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmShipmentExtReference"
        Me.Ribbon = Me.RibbonControl
        Me.Text = "Set Shipment External Reference"
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExternalReference.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSetShipmentName As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtExternalReference As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblExternalReference As DevExpress.XtraEditors.LabelControl



End Class
