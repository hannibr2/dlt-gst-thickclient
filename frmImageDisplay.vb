﻿Imports System.Threading
Imports DecodeLib

Public Class frmImageDisplay
    ''' <summary>
    ''' btnCancel_Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Hand the click even for the cancel button and attempt to cancel the async web service thread</remarks>
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        lblDecodeStatus.ForeColor = Color.Red
        lblDecodeStatus.Text = "Stoping Decoding Service.."

        frmMain.m_DecodingStopped = True
        frmMain.BackgroundWorker1.CancelAsync()

        Dim decodeLib As New DecodeLib.CDecodeLib

        Dim objDecode As New DecodeLib.CDecodeLib()
        objDecode.InitDegreeRotaion = 13
        objDecode.StepRotation = 1
        objDecode.NumberOfAttemps = 2
        objDecode.TotalTubes = 96
        frmMain.objContext.imageObject.StopDecoding = True
        objDecode.DecodeImage(frmMain.objContext.imageObject, False)


        ProgressBar1.MarqueeAnimationSpeed = 30
        ProgressBar1.Style = ProgressBarStyle.Marquee

        lblDecodeStatus.Refresh()
        Dim x As Int16 = 1
        With ProgressBar1
            .Style = ProgressBarStyle.Blocks
            .Step = 1
            .Minimum = 0
            .Maximum = 100
            .Value = 0
            .ForeColor = Color.Red
        End With

        While (frmMain.BackgroundWorker1.IsBusy)
            x = x + 1
            Try
                frmMain.BackgroundWorker1.ReportProgress(x)
                ProgressBar1.PerformStep()
                Threading.Thread.Sleep(1000)
                Me.Refresh()
            Catch ex As Exception
                Exit While
            End Try

        End While

        Me.Close()
        Me.Dispose()

    End Sub
End Class