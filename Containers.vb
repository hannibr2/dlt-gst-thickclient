﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel

Public Class Containers
    Inherits Collection(Of Container)

    Private m_iOtherCount As Integer = 0
    Private m_iPowderCount As Integer = 0
    Private m_iSolutionCount As Integer = 0
    Private m_bContainerTypeCount As Boolean = False

    Public ReadOnly Property HasAttachments As Boolean
        Get
            HasAttachments = False
            For Each CurrentContainer In Me
                If CurrentContainer.HasAttachments Then
                    HasAttachments = True
                    Exit For
                End If
            Next
            Return HasAttachments
        End Get
    End Property

    Public ReadOnly Property PowderCount() As Integer
        Get
            If Not m_bContainerTypeCount Then
                SetContainerTypeCounts()
            End If
            Return m_iPowderCount
        End Get
    End Property

    Public ReadOnly Property SolutionCount() As Integer
        Get
            If Not m_bContainerTypeCount Then
                SetContainerTypeCounts()
            End If
            Return m_iSolutionCount
        End Get
    End Property

    Public Function GetContainerByBarcode(ByVal Barcode As String) As Container
        Dim FoundContainer As Container
        For Each CurrentContainer In Me
            If CurrentContainer.Barcode = Barcode Then
                FoundContainer = CurrentContainer
                Exit For
            End If
        Next
        Return FoundContainer
    End Function

    Public Function HasBarcode(ByVal Barcode As String) As Boolean
        Dim FoundBarcode As Boolean
        For Each CurrentContainer In Me
            If CurrentContainer.Barcode = Barcode Then
                FoundBarcode = True
                Exit For
            End If
        Next
        Return FoundBarcode
    End Function


    Private Sub SetContainerTypeCounts()
        Dim CurrentContainer As Container
        For Each CurrentContainer In Me
            If String.Compare(CurrentContainer.DeliveryFormTypeName, "Solution", True) = 0 Then
                m_iSolutionCount = m_iSolutionCount + 1
            ElseIf String.Compare(CurrentContainer.DeliveryFormTypeName, "Powder", True) = 0 Then
                m_iPowderCount = m_iPowderCount + 1
            Else
                m_iOtherCount = m_iOtherCount + 1
            End If
        Next
        m_bContainerTypeCount = True
    End Sub

    Public Sub AddRange(ByVal ContainerCollection As Containers)

        For Each TempContainer As Container In ContainerCollection
            Me.Add(TempContainer)
        Next


    End Sub

    Public Sub New(ByVal Items As DataTable)
        For Each Row As DataRow In Items.Rows
            Me.Add(New Container(Row))
        Next
    End Sub

    Public Sub New()

    End Sub

    Public Sub New(ByVal list As IList(Of Containers))
        MyBase.New(list)
    End Sub

End Class
