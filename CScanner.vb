﻿'*********************************************************************************
' Company:          Novartis - NIBR IT
' Copyright:        Copyright © 2013
' Author:           Erik Hesse
' Date:             04/03/2013
'
' Project:          IntelliScan       
' Application:      InstrumentNotificationTool
' File:             CScanner.cs
'
' Comment:         
'
' Purpose:          This class wraps the scanner functionality.
'
'
' History:        
'
' Date:         Ver:    Description:
' 05-15-2013    3.1.1   Added event to send notifications to main form.
' 09-03-2013    3.1.11  Removed the single quotes from the barcode string returned 
'                       from the VisionMate scanner.
' 04-23-2014	3.1.16	Added VisionMate SR control.
'
'*********************************************************************************
Imports ScannerLib
Imports DecodeLib
Imports System.IO
Imports System.Threading
Imports ExceptionsHandlerLib


Public Class CScanner

    Public Event evtScan(ByVal sender As Object, ByVal e As String)

    Protected Overridable Sub OnScan(ByVal e As String)
        RaiseEvent evtScan(Me, e)
    End Sub


    Public Enum Scanner
        WIA = 0
        Twain = 1
        VisionMate = 2
        Simulator = 3
        VisionMateSR = 4
        WIASingle = 5
        Micronic = 6
    End Enum

#Region "Member Variables"


    Public eScanner As Scanner
    Public m_objScanner As CScanner_Base
    Public m_strSettingsFile As String
    Public m_strBarcodes As String

#End Region

    ''' <summary>
    ''' CScanner constructor
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        m_objScanner = New CScanner_Simulator
    End Sub
    Public Function MutliScan() As frmMain.AsynchReturnMSG
        Dim msg As frmMain.AsynchReturnMSG = Scan()
        Dim wrapper As frmMain.AsynchReturnMSG = New frmMain.AsynchReturnMSG()
        wrapper.barcodes = msg.barcodes
        wrapper.image = msg.image
        wrapper.messages = msg.messages
        Return wrapper
    End Function
    ''' <summary>
    ''' Scan
    ''' </summary>
    ''' <remarks></remarks>
    Public Function Scan() As frmMain.AsynchReturnMSG
        Dim sw As New Stopwatch()
        Dim BenchmarkLog As New CMessageLog()
        Dim asynchReturnMSG As New frmMain.AsynchReturnMSG()

        Try

            Select Case (eScanner)



                Case Scanner.WIASingle, Scanner.Twain, Scanner.Micronic
                    sw.Restart()
                    sw.Start()
                    m_objScanner.Init()
                    m_objScanner.SetSource()
                    m_objScanner.Scan()

                    sw.Restart()
                    sw.Start()
                    Dim baImage() As Byte = m_objScanner.GetImage()
                    baImage = DecodeLib.CImageHelper.CropImage(baImage)
                    asynchReturnMSG.image = baImage
                    sw.Stop()

                Case Scanner.Simulator
                    '----------------------------------------------------------------------------------

                    Dim baImage() As Byte = m_objScanner.GetImage()
                    asynchReturnMSG.barcodes = Nothing
                    asynchReturnMSG.image = baImage

                    '' Get the barcodes

                    'm_strBarcodes = "1073520443"

                    'm_strBarcodes = "1075165915,0075054515,1066251363,0075039779,0086165039,1061803496,0086165027,0086165066,1066249820,1066249815,0072543376,0072543345," _
                    '+ "0072694396,0072694517,0072694420,0072694496,0072694377,1061803322,1070400596,1058518153,0072694467,0072694401,0072543364,0075054328," _
                    '+ "0072692716,0072692697,0072692692,0072692673,0072693427,0072690544,1075165837,0072692441,1066250574,1066251362,0072543033,0086165044," _
                    '+ "0086165075,0086165062,0086165019,0086165002,0086165050,0072542979,0072542962,0072542908,0072543868,0072542889,0075054530,0086165090," _
                    '+ "0072694265,0120448312,0120448303,0120448288,0120447905,1075167353,1075166918,1075166846,1075165280,1075165313,1075165442,1075165312," _
                    '+ "1075165676,1075165189,1075165396,1075165126,1075165277,1075165673,1075165264,1075165288,1075165349,1075165214,1075165325,1075165276," _
                    '+ "1075164679,1075165473,,,,,,1072766575,1072765680,1052907824,,1072749544," _
                    '+ "1072744261,1066978350,1066978355,0115745408,1072766607,1072766608,1072766609,0075029788,0075029793,0075029812,1061803506,1072749543"

            End Select

        Catch ex As Exception
            ' Write the exception to the exception log
            'Throw New CException(Me.GetType().Name + ":Scan not successful: " + ex.Message, ex)
        End Try

        Return asynchReturnMSG

    End Function

    Public Delegate Sub AppendTextBoxDelegate(ByVal TB As RichTextBox, ByVal txt As String)
    <STAThread()> _
    Public Sub AppendTextBox(ByVal TB As RichTextBox, ByVal txt As String)
        If TB.InvokeRequired Then
            TB.Invoke(New AppendTextBoxDelegate(AddressOf AppendTextBox), New Object() {TB, txt})
        Else
            TB.AppendText(txt)


        End If
    End Sub
End Class
