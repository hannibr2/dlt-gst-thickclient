﻿Imports GST.FedExTrackingService

Public Class FedExResponse

    Private m_sTrackingCode As String = String.Empty
    Private m_bHasErrors As Boolean = True
    Private m_bIsScheduled As Boolean = False
    Private m_bIsPickedUp As Boolean = False
    Private m_bIsDelivered As Boolean = False

    Public Property TrackingCode As String
        Get
            Return m_sTrackingCode
        End Get
        Set(value As String)
            If m_sTrackingCode = value Then
                Return
            End If
            m_sTrackingCode = value
        End Set
    End Property
    Public Property HasErrors As Boolean
        Get
            Return m_bHasErrors
        End Get
        Set(value As Boolean)
            If m_bIsScheduled = value Then
                Return
            End If
            m_bIsScheduled = value
        End Set
    End Property

    Public Property IsScheduled As Boolean
        Get
            Return m_bIsScheduled
        End Get
        Set(value As Boolean)
            If m_bIsScheduled = value Then
                Return
            End If
            m_bIsScheduled = value
        End Set
    End Property
    Public Property IsPickedUp As Boolean
        Get
            Return m_bIsPickedUp
        End Get
        Set(value As Boolean)
            If m_bIsPickedUp = value Then
                Return
            End If
            m_bIsPickedUp = value
        End Set
    End Property
    Public Property IsDelivered As Boolean
        Get
            Return m_bIsDelivered
        End Get
        Set(value As Boolean)
            If m_bIsDelivered = value Then
                Return
            End If
            m_bIsDelivered = value
        End Set
    End Property

    Public ReadOnly Property TrackingURL As String
        Get
            If String.IsNullOrEmpty(m_sTrackingCode) Then
                Return String.Empty
            Else
                Return "http://www.fedex.com/Tracking?tracknumber_list=" & m_sTrackingCode
            End If
        End Get
    End Property



    Public Sub New(ByVal FedExResponse As TrackReply)
        ParseFedExResponse(FedExResponse)
    End Sub

    Private Sub ParseFedExResponse(Response As TrackReply)

        If ((Not Response.HighestSeverity = NotificationSeverityType.ERROR) And (Not Response.HighestSeverity = NotificationSeverityType.FAILURE)) Then
            m_bHasErrors = False

            If (Not Response.TrackDetails Is Nothing) Then
                For Each trackDetail As TrackDetail In Response.TrackDetails
                    If Not String.IsNullOrEmpty(Trim(trackDetail.TrackingNumber)) Then
                        TrackingCode = trackDetail.TrackingNumber
                        m_bIsScheduled = True
                    End If

                    For Each CurrentTrackEvent As TrackEvent In trackDetail.Events

                        If CurrentTrackEvent.EventType = "DL" Then
                            m_bIsDelivered = True
                        End If

                        If CurrentTrackEvent.EventType = "PU" Then
                            m_bIsPickedUp = True
                        End If

                        If CurrentTrackEvent.EventType = "OC" Then
                            m_bIsScheduled = True
                        End If

                    Next
                Next
            End If
        End If

        

    End Sub
End Class
