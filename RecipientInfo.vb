Public Class RecipientInfo 

    Private m_RecipientID As String
    Private m_RecipientSelected As Boolean

    Private m_HighlightedRecipientID As Decimal = -1


    Public Property HighlightedRecipientID As Decimal
        Get
            Return m_HighlightedRecipientID
        End Get
        Set(value As Decimal)
            If m_HighlightedRecipientID = value Then
                Return
            End If
            m_HighlightedRecipientID = value
        End Set
    End Property


    Public Property RecipientID() As String
        Get
            Return m_RecipientID
        End Get
        Set(ByVal value As String)
            m_RecipientID = value
        End Set
    End Property

    Public Property RecipientSelected() As Boolean
        Get
            Return m_RecipientSelected
        End Get
        Set(ByVal value As Boolean)
            m_RecipientSelected = value
        End Set
    End Property

    Private Sub btnCancelUpdateRecipient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelUpdateRecipient.Click
        'm_RecipientID = ""
        'm_RecipientSelected = False
        Me.Hide()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim OracleDAL As New Oracle_General
        OracleDAL.ShowRecipientSearch(txtRecipientFirstName.Text, txtRecipientLastName.Text, txtRecipientCompany.Text)

        'Set focus to row 1 after the search
        GrdViewRecipientInfo.SelectRow(1)

        'Update text fields according to the search
        ShowSelectedRow()

    End Sub

    Private Sub ShowSelectedRow()

        'Refresh all text fields
        txtRecipientDetailsFirst.Text = ""
        txtRecipientDetailsLast.Text = ""
        txtRecipientDetailsCompany.Text = ""
        txtRecipientDetailsDept.Text = ""
        txtRecipientAddress.Text = ""
        txtRecipientCity.Text = ""
        txtRecipientState.Text = ""
        txtPostalCode.Text = ""
        txtRecipientCountry.Text = ""
        txtRecipientPhone.Text = ""
        txtRecipientEmail.Text = ""

        If Not GrdViewRecipientInfo.GetFocusedRow Is Nothing Then

            'Update all text fields with selected row
            txtRecipientDetailsFirst.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("FIRST_NAME").ToString()
            txtRecipientDetailsLast.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("LAST_NAME").ToString()
            txtRecipientDetailsCompany.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("COMPANY").ToString()
            txtRecipientDetailsDept.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("DEPARTMENT").ToString()
            txtRecipientAddress.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("MAIL_ADDRESS").ToString()

            'if Province is empty
            If GrdViewRecipientInfo.GetFocusedRowCellValue("PROVINCE").ToString() = "" Then
                Dim TempIndex As Integer
                'Check if City container ","
                TempIndex = InStr(GrdViewRecipientInfo.GetFocusedRowCellValue("CITY").ToString(), ",")
                'if City doesn't contain ",", then use the return result for City and Province
                If TempIndex = 0 Then
                    txtRecipientCity.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("CITY").ToString()
                    txtRecipientState.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("PROVINCE").ToString()
                Else
                    txtRecipientCity.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("CITY").ToString().Substring(0, TempIndex - 1)
                    txtRecipientState.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("CITY").ToString().Substring(TempIndex + 1)
                End If
            Else
                txtRecipientCity.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("CITY").ToString()
                txtRecipientState.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("PROVINCE").ToString()
            End If

            txtPostalCode.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("POSTALCODE").ToString()
            txtRecipientCountry.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("COUNTRY_NAME").ToString()
            txtRecipientPhone.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("PHONE").ToString()
            txtRecipientEmail.Text = GrdViewRecipientInfo.GetFocusedRowCellValue("EMAIL").ToString()

            grdRecipientInfo.ForceInitialize()
            GrdViewRecipientInfo.MakeRowVisible(GrdViewRecipientInfo.FocusedRowHandle)
        End If

    End Sub

    Private Sub GrdViewRecipientInfo_FocusedRowChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GrdViewRecipientInfo.FocusedRowChanged
        ShowSelectedRow()
    End Sub

    Private Sub btnAddRecipient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddRecipient.Click
        Try
            Dim OracleDAL As New Oracle_General
            OracleDAL.AddContact(txtRecipientDetailsCompany.Text, txtRecipientDetailsDept.Text, txtRecipientDetailsLast.Text, txtRecipientDetailsFirst.Text, _
                       txtRecipientAddress.Text, txtRecipientCity.Text, txtRecipientState.Text, txtPostalCode.Text, txtRecipientCountry.Text, _
                       txtRecipientPhone.Text, txtRecipientEmail.Text)
        Catch err As Exception
            ShowMessage(err.Message)
        End Try
    End Sub

    Private Sub btnUpdateRecipient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateRecipient.Click
        If GrdViewRecipientInfo.GetFocusedRowCellValue("PERSON_TYPE") = "external" Then
            Try
                Dim OracleDAL As New Oracle_General
                OracleDAL.UpdateContact(GrdViewRecipientInfo.GetFocusedRowCellValue("PERSON_ID"), txtRecipientDetailsCompany.Text, txtRecipientDetailsDept.Text, txtRecipientDetailsLast.Text, _
                              txtRecipientDetailsFirst.Text, txtRecipientAddress.Text, txtRecipientCity.Text, txtRecipientState.Text, txtPostalCode.Text, txtRecipientCountry.Text, _
                                   txtRecipientPhone.Text, txtRecipientEmail.Text)
            Catch err As Exception
                ShowMessage(err.Message)
            End Try
        Else
            ShowMessage("You cannot update an internal contact.")
        End If
    End Sub

    Private Sub btnRemoveRecipient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveRecipient.Click
        If GrdViewRecipientInfo.GetFocusedRowCellValue("PERSON_TYPE") = "external" Then
            Try
                Dim OracleDAL As New Oracle_General
                OracleDAL.DeleteContact(GrdViewRecipientInfo.GetFocusedRowCellValue("PERSON_ID"))
            Catch err As Exception
                ShowMessage(err.Message)
            End Try
        Else
            ShowMessage("You cannot remove an internal contact.")
        End If
    End Sub

    Private Sub btnSelectRecipient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectRecipient.Click
        m_RecipientID = GrdViewRecipientInfo.GetFocusedRowCellValue("PERSON_ID").ToString()

        'Setting receiver information
        m_ReceiverFirstName = txtRecipientDetailsFirst.Text
        m_ReceiverLastName = txtRecipientDetailsLast.Text
        m_ReceiverCompany = txtRecipientDetailsCompany.Text
        m_ReceiverDepartment = txtRecipientDetailsDept.Text
        m_ReceiverAddress = txtRecipientAddress.Text
        m_ReceiverCity = txtRecipientCity.Text
        m_ReceiverState = txtRecipientState.Text
        m_ReceiverZip = txtPostalCode.Text
        m_ReceiverCountry = txtRecipientCountry.Text
        m_ReceiverEmail = txtRecipientEmail.Text
        m_ReceiverPhone = txtRecipientPhone.Text

        m_RecipientSelected = True
        Me.Hide()
    End Sub

    Private Sub RecipientInfo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim OracleDAL As New Oracle_General
        OracleDAL.ShowRecipients()
        'Set focus to row 1 after the search

        If HighlightedRecipientID > 0 Then
            Dim TempSelectedRow As Integer = GrdViewRecipientInfo.LocateByValue("PERSON_ID", Convert.ToInt64(HighlightedRecipientID))
            'GrdViewRecipientInfo.SelectRow(TempSelectedRow)

            If TempSelectedRow > 0 Then
                GrdViewRecipientInfo.ClearSelection()
                GrdViewRecipientInfo.FocusedRowHandle = TempSelectedRow
                GrdViewRecipientInfo.SelectRow(TempSelectedRow)
                GrdViewRecipientInfo.MakeRowVisible(TempSelectedRow)

            End If
        Else
            GrdViewRecipientInfo.SelectRow(1)
            GrdViewRecipientInfo.FocusedRowHandle = 1
        End If
        
        'Update text fields according to the search
        ShowSelectedRow()

    End Sub

    'Public Sub New(ByVal ReceiverID As Decimal)

    '    ' This call is required by the designer.
    '    InitializeComponent()

    '    ' Add any initialization after the InitializeComponent() call.
    '    m_HighlightedRecipientID = ReceiverID
    'End Sub
End Class