﻿Public Class RackRow

    Dim _col1, _col2, _col3, _col4, _col5, _col6, _col7, _col8, _col9, _col10, _col11, _col12 As String

    Public ReadOnly Property Col1() As String
        Get
            Return _col1
        End Get
    End Property

    Public ReadOnly Property Col2() As String
        Get
            Return _col2
        End Get
    End Property
    Public ReadOnly Property Col3() As String
        Get
            Return _col3
        End Get
    End Property
    Public ReadOnly Property Col4() As String
        Get
            Return _col4
        End Get
    End Property
    Public ReadOnly Property Col5() As String
        Get
            Return _col5
        End Get
    End Property
    Public ReadOnly Property Col6() As String
        Get
            Return _col6
        End Get
    End Property
    Public ReadOnly Property Col7() As String
        Get
            Return _col7
        End Get
    End Property
    Public ReadOnly Property Col8() As String
        Get
            Return _col8
        End Get
    End Property

    Public ReadOnly Property Col9() As String
        Get
            Return _col9
        End Get
    End Property

    Public ReadOnly Property Col10() As String
        Get
            Return _col10
        End Get
    End Property

    Public ReadOnly Property Col11() As String
        Get
            Return _col11
        End Get
    End Property

    Public ReadOnly Property Col12() As String
        Get
            Return _col12
        End Get
    End Property
    Public Sub New(ByVal col1 As String, ByVal col2 As String, ByVal col3 As String, ByVal col4 As String, ByVal col5 As String, ByVal col6 As String, ByVal col7 As String, ByVal col8 As String, ByVal col9 As String, ByVal col10 As String, ByVal col11 As String, ByVal col12 As String)

        _col1 = col1
        _col2 = col2
        _col3 = col3
        _col4 = col4
        _col5 = col5
        _col6 = col6
        _col7 = col7
        _col8 = col8

        _col9 = col9
        _col10 = col10
        _col11 = col11
        _col12 = col12

    End Sub
End Class
