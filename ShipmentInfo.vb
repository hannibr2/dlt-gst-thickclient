﻿Module ShipmentInfo

    Public m_SenderID As Decimal
    Public m_SenderFirstName As String
    Public m_SenderLastName As String
    Public m_SenderCompany As String
    Public m_SenderDepartment As String
    Public m_SenderAddress As String
    Public m_SenderCity As String
    Public m_SenderState As String
    Public m_SenderZip As String
    Public m_SenderCountry As String
    Public m_SenderAccountNumber As String
    Public m_SenderCostCenter As String
    Public m_SenderPhone As String
    Public m_SenderEmail As String

    Public m_ReceiverID As Decimal
    Public m_ReceiverFirstName As String
    Public m_ReceiverLastName As String
    Public m_ReceiverCompany As String
    Public m_ReceiverDepartment As String
    Public m_ReceiverAddress As String
    Public m_ReceiverCity As String
    Public m_ReceiverState As String
    Public m_ReceiverZip As String
    Public m_ReceiverCountry As String
    Public m_ReceiverPhone As String
    Public m_ReceiverEmail As String

    Public m_ShipmentDescription As String
    Public m_ShipmentOption As String

    Public m_Courier As String

    Public m_ReferenceNumber As String

    'Public m_PowderContainerCount As Integer
    'Public m_SolutionContainerCount As Integer

    Public ReadOnly Property SenderFullName() As String
        Get
            Return String.Format("{0}, {1}", m_SenderLastName, m_SenderFirstName)
        End Get
    End Property


End Module
