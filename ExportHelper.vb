﻿Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Export
Imports DevExpress.XtraBars

Imports System.Globalization
Imports System.Windows.Forms

Public Class ExportHelper

    Private Const STR_CSV_DOC As String = "CSV Document"
    Private Const STR_CSV_FILTER As String = "CSV Documents|*.csv"
    Private Const STR_XML_DOC As String = "XML Document"
    Private Const STR_XML_FILTER As String = "XML Documents|*.xml"
    Private Const STR_HTML_DOC As String = "HTML Document"
    Private Const STR_HTML_FILTER As String = "HTML Documents|*.html"
    Private Const STR_XLS_DOC As String = "Microsoft Excel Document"
    Private Const STR_XLS_FILTER As String = "Microsoft Excel|*.xls"
    Private Const STR_TXT_DOC As String = "Text Document"
    Private Const STR_TXT_FILTER As String = "Text Files|*.txt"
    Private Const STR_EXPORT_TO As String = "Export To"

    Public Enum ExportType
        XLS
        TXT
        HTML
        XML
        CSV
    End Enum

    Public Shared ReadOnly Property ExportTypes As ExportType()
        Get
            Return New ExportType() {ExportType.XLS, ExportType.TXT, ExportType.XML, ExportType.CSV}
        End Get
    End Property


    Public Shared Function DocTitle(ByVal Type As ExportType) As String

        Dim ReturnString As String = String.Empty

        Select Case Type
            Case ExportType.CSV
                ReturnString = STR_CSV_DOC
            Case ExportType.XLS
                ReturnString = STR_XLS_DOC
            Case ExportType.TXT
                ReturnString = STR_TXT_DOC
            Case ExportType.HTML
                ReturnString = STR_HTML_DOC
            Case ExportType.XML
                ReturnString = STR_XML_DOC
        End Select

        Return ReturnString
    End Function

    Public Shared Function DocFilter(ByVal Type As ExportType) As String

        Dim ReturnString As String = String.Empty

        Select Case Type
            Case ExportType.CSV
                ReturnString = STR_CSV_DOC
            Case ExportType.XLS
                ReturnString = STR_XLS_DOC
            Case ExportType.TXT
                ReturnString = STR_TXT_DOC
            Case ExportType.HTML
                ReturnString = STR_HTML_DOC
            Case ExportType.XML
                ReturnString = STR_XML_DOC
        End Select

        Return ReturnString
    End Function

    Public Shared Sub ExportTo(Type As ExportType, FileName As String, GridView As GridView)

        Application.DoEvents()

        Select Case Type
            Case ExportType.CSV
                GridView.ExportToText(FileName, New TextExportOptions(CultureInfo.CurrentCulture.TextInfo.ListSeparator))
            Case ExportType.XLS
                Dim sheetName As String = System.IO.Path.GetFileNameWithoutExtension(FileName)
                GridView.ExportToXls(FileName, New XlsExportOptions(sheetName = sheetName))
            Case ExportType.TXT
                GridView.ExportToText(FileName, New TextExportOptions("\t"))

            Case ExportType.HTML
                GridView.ExportToHtml(FileName)
            Case ExportType.XML
                Using provider As New DevExpress.XtraExport.ExportXmlProvider(FileName)
                    Dim Link As BaseExportLink = GridView.CreateExportLink(provider)
                    Link.ExportTo(True)
                End Using
        End Select

    End Sub

    Public Shared Function ExportFileDialog(Types As ExportType(), DocName As String) As String
        Dim ReturnString As String = String.Empty

        Dim dlg As SaveFileDialog = New SaveFileDialog()

        If Types.Length = 1 Then
            dlg.Title = String.Format("{0} {1}", STR_EXPORT_TO, DocTitle(Types(0)))
        Else
            dlg.Title = String.Format("{0} {1}", STR_EXPORT_TO, "...")
        End If

        dlg.FileName = DocName

        Dim s As String = String.Empty


        For i As Integer = 0 To Types.Length Step 1
            s += DocFilter(Types(i))

            If i = Types.Length - 1 Then
                s += ""
            Else
                s += "|"
            End If

        Next

        dlg.Filter = s
        dlg.FilterIndex = 0

        If dlg.ShowDialog() = DialogResult.OK Then
            ReturnString = dlg.FileName
        End If

        Return ReturnString
    End Function

    Public Shared Sub Export(ByVal DocName As String, ByVal Type As ExportType, ByVal GridView As GridView)

        Try
            Dim FileName As String = ExportFileDialog(New ExportType() {Type}, DocName)

            If FileName.Length > 0 Then

                ExportTo(Type, FileName, GridView)
                'ViewFIle(FileName, True)

            End If
        Catch ex As Exception
            ShowMessage(String.Format("Failed to export.{0}", ex.InnerException))
        End Try
        
    End Sub
End Class

Public Class ImplementExportClass

    Private _GridView As GridView

    Public ReadOnly Property GridView As GridView
        Get
            Return _GridView
        End Get
        'Set(value As GridView)
        '    If _GridView = value Then
        '        Return
        '    End If
        '    _GridView = value
        'End Set
    End Property



    Public Sub New(ByVal MainBarManager As BarManager, ByVal ExportSubMenuItem As BarSubItem, ByVal GridView As GridView)

        If ExportSubMenuItem Is Nothing Then
            Return
        End If

        _GridView = GridView

        Dim types As ExportHelper.ExportType() = ExportHelper.ExportTypes

        For i As Integer = 0 To types.Length Step 1

            Dim button As BarButtonItem = New BarButtonItem(MainBarManager, ExportHelper.DocTitle(types(i)) + "...") With {.Tag = types(i)}

            AddHandler button.ItemClick, AddressOf button_ItemClick
            ExportSubMenuItem.AddItem(button)

        Next
    End Sub

    Public Sub button_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)

        Dim item As BarButtonItem = e.Item

        If item Is Nothing Then
            Return
        End If

        Dim Type As ExportHelper.ExportType = DirectCast(item.Tag, ExportHelper.ExportType)

        If Not GridView Is Nothing Then
            ExportHelper.Export(Nothing, Type, _GridView)
        End If

    End Sub

End Class
