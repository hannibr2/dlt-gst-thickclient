﻿Imports System.IO

Module ChronosAttachmentCalls

    Public Function GetChronosAttachments(ByVal Containers As Containers) As Boolean

        Dim TempAttachmentRequested As Boolean = False

        Try
            Dim TempChronosOrderProtocol As New List(Of String)
            Dim TempAttachmentRequest As New ChronosAttachmentService.attachmentRequestsRequest

            Dim TempAttachmentRequestItems As List(Of ChronosAttachmentService.AttachmentRequestReqEl) = New List(Of ChronosAttachmentService.AttachmentRequestReqEl)
            Dim TempAttachmentRequestItem As ChronosAttachmentService.AttachmentRequestReqEl
            Dim TempAttachmentResponseItem As ChronosAttachmentService.AttachmentRequestResEl()
            'Dim TempAttachmentResponse As ChronosAttachmentService.attachmentRequestsResponse

            For Each TempContainer As Container In Containers

                If TempContainer.HasAttachments Then

                    Dim TempOrderProtocol As String = String.Format("{0}_{1}", TempContainer.OrderName, TempContainer.ProtocolName)

                    If Not TempChronosOrderProtocol.Contains(TempOrderProtocol) Then
                        TempChronosOrderProtocol.Add(TempOrderProtocol)

                        TempAttachmentRequestItem = ChronosAttachmentRequestItem(TempContainer.Ranking, TempContainer.OrderName, String.Empty, TempContainer.Barcode)
                        TempAttachmentRequestItems.Add(TempAttachmentRequestItem)
                    End If

                End If
            Next

            TempAttachmentRequest.attachmentRequestsRequest1 = TempAttachmentRequestItems.ToArray()

            Dim client As ChronosAttachmentService.attachmentRequestsServiceClient = New ChronosAttachmentService.attachmentRequestsServiceClient
            TempAttachmentResponseItem = client.attachmentRequests(TempAttachmentRequest.attachmentRequestsRequest1)

            If Not TempAttachmentResponseItem Is Nothing Then
                TempAttachmentRequested = True
            Else
                TempAttachmentRequested = False
            End If

            For Each currentItem As ChronosAttachmentService.AttachmentRequestResEl In TempAttachmentResponseItem
                For Each currentAttachment As ChronosAttachmentService.Attachment In currentItem.attachment


                    Dim FileName As String = Path.GetTempPath + currentAttachment.filename
                    SaveDocument(FileName, currentAttachment.document)

                Next

            Next


        Catch ex As Exception
            TempAttachmentRequested = True
            Throw New Exception("Error getting attachments - " & ex.Message)
        End Try

        Return TempAttachmentRequested
    End Function

    Public Function ChronosAttachmentRequestItem(ByVal Ranking As Integer, ByVal OrderName As String, ByVal OrderItemRef As String, ByVal DeliveryContainerBarcode As String) As ChronosAttachmentService.AttachmentRequestReqEl

        Dim TempAttachmentRequestItem As New ChronosAttachmentService.AttachmentRequestReqEl()

        TempAttachmentRequestItem.ranking = Ranking
        TempAttachmentRequestItem.ordername = OrderName
        TempAttachmentRequestItem.orderitemref = OrderItemRef
        TempAttachmentRequestItem.deliverycontainerbarcode = DeliveryContainerBarcode

        Return TempAttachmentRequestItem

    End Function

    Private Sub ConvertBase64toFile(ByVal AttachmentFile As ChronosAttachmentService.Attachment)

        '        Dim FileBytes() As Byte
        '        FileBytes = Convert.FromBase64String(FileBase)

        Dim stream As MemoryStream = New MemoryStream()

        'Using stream As MemoryStream = New MemoryStream()
        stream.Write(AttachmentFile.document, 0, 4)
        'End Using
        stream.Close()
    End Sub


    Private Sub SaveDocument(ByRef labelFileName As String, ByRef labelBuffer() As Byte)
        ' Save label buffer to file
        Dim LabelFile As FileStream = New FileStream(labelFileName, FileMode.Create)
        LabelFile.Write(labelBuffer, 0, labelBuffer.Length)
        LabelFile.Close()
        ' Display label in Acrobat
        OpenDocument(labelFileName)
    End Sub

    Private Sub OpenDocument(ByRef labelFileName As String)
        Dim info As System.Diagnostics.ProcessStartInfo = New System.Diagnostics.ProcessStartInfo(labelFileName)
        info.UseShellExecute = True
        info.Verb = "open"
        System.Diagnostics.Process.Start(info)
    End Sub

End Module
