﻿
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports Oracle.ManagedDataAccess


'Imports System.IO
'Imports System.Threading
'Imports System.Text

'Imports GST.NibrIAMAuth.NibrIAMAuth
'Imports System.Net
'Imports System.Xml

Public Class Oracle_Container
    Inherits Oracle_General

#Region "           Containers                  "

#Region "           Get Functions           "

    Public Function GetPendingByBarcode(ByVal Barcode As String) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select * from v_container_details where phase_id = 50002 and barcode = '" & Barcode & "'"
            m_DataSet = ExecuteQuery(query)


        Catch err As Exception
            Throw New Exception("Error getting pending items")
        End Try

        Return m_DataSet.Tables(0)
    End Function

    Public Function GetContainersByParentContainerID(ByVal ParentContainerID As Decimal) As DataTable

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select * " & _
                    "from v_container_details vc where parent_container_id = " + ParentContainerID.ToString("F0") + " " & _
                    "and parent_container_id != container_id order by ranking"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error updating package details")
        End Try

    End Function

    Public Function GetScannedItems(ByVal ScannedBarcodes As ArrayList) As DataTable

        Dim query As String
        Dim m_DataSet As DataSet
        Dim m_bHasWhere As Boolean = False
        Dim b_FirstBarcode As Boolean = True

        Try
            'Type ID = 130103 => Global Solution Flask (tubes from CPH), Phase ID = 50002 => Container Arrived
            'Type ID 130102 = NBA
            'Old view pointing to TRT
            'query = "select * from v_container where type_id = 130103 and phase_id = 50002 and rownum <= 500"
            'New view pointing to Chronos
            query = "select * from v_container_details where phase_id = 50002 and "

            'If ValidationRules.Timeframe > 0 Then
            '    query += " and order_date_created > sysdate - " & ValidationRules.Timeframe
            'End If

            query += " barcode in ( " 'and receiver_id IN (18287, 14343803, 14316311, 93851)

            For Each CurrentBarcode As String In ScannedBarcodes
                If b_FirstBarcode Then
                    query += "'" & CurrentBarcode & "'"
                    b_FirstBarcode = False
                Else
                    query += ", '" & CurrentBarcode & "'"
                End If
            Next

            query += " ) "

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting Initial Scanning items")
        End Try

    End Function

    Public Sub ShowScannedItems(ByVal ScannedBarcodes As ArrayList)


        Dim query As String
        Dim m_DataSet As DataSet
        Dim m_bHasWhere As Boolean = False
        Dim b_FirstBarcode As Boolean = True

        Try
            'Type ID = 130103 => Global Solution Flask (tubes from CPH), Phase ID = 50002 => Container Arrived
            'Type ID 130102 = NBA
            'Old view pointing to TRT
            'query = "select * from v_container where type_id = 130103 and phase_id = 50002 and rownum <= 500"
            'New view pointing to Chronos
            query = "select * from v_container_details where phase_id = 50002 and "

            'If ValidationRules.Timeframe > 0 Then
            '    query += " and order_date_created > sysdate - " & ValidationRules.Timeframe
            'End If

            query += " upper( barcode )  in ( " 'and receiver_id IN (18287, 14343803, 14316311, 93851)

            For Each CurrentBarcode As String In ScannedBarcodes
                If b_FirstBarcode Then
                    query += "'" & CurrentBarcode & "'"
                    b_FirstBarcode = False
                Else
                    query += ", '" & CurrentBarcode & "'"
                End If
            Next

            query += " ) "

            m_DataSet = ExecuteQuery(query)
            frmMain.grdPendingItems.DataSource = m_DataSet.Tables(0)
            'Dim DtPendingItems As DataTable = GetScannedItems(ScannedBarcodes)
            'frmMain.grdPendingItems.DataSource = DtPendingItems
        Catch err As Exception
            Throw New Exception("Error getting scanned items")
        End Try

    End Sub

    Public Sub ShowPendingItems(ByVal ValidationRules As PackageValidationRules, ByVal FulfillmentSite As String, Optional ByVal UseRules As Boolean = True)
        Dim query As String
        Dim m_DataSet As DataSet
        Dim m_bHasWhere As Boolean = False


        Try
            'Type ID = 130103 => Global Solution Flask (tubes from CPH), Phase ID = 50002 => Container Arrived
            'Type ID 130102 = NBA
            'Old view pointing to TRT
            'query = "select * from v_container where type_id = 130103 and phase_id = 50002 and rownum <= 500"
            'New view pointing to Chronos
            query = "select * from v_container_details where phase_id = 50002 "

            If ValidationRules.Timeframe > 0 Then
                query += " and order_date_created > sysdate - " & ValidationRules.Timeframe
            End If

            If Not String.IsNullOrEmpty(FulfillmentSite) Then
                query += " and fulfillment_site = '" & FulfillmentSite & "' "
            End If

            If UseRules Then

                'Site Rules
                If ValidationRules.Site AndAlso Not String.IsNullOrEmpty(ValidationRules.SiteValue) Then
                    query += " and " & ValidationRules.SiteColumn & " = '" & ValidationRules.SiteValue & "' "
                End If

                'Recipient
                If ValidationRules.Recipient AndAlso Not String.IsNullOrEmpty(ValidationRules.RecipientValue) Then
                    query += " and " & ValidationRules.RecipientColumn & " = '" & ValidationRules.RecipientValue & "'"
                    m_bHasWhere = True
                End If

                'Request Number
                If ValidationRules.RequestNumber AndAlso Not String.IsNullOrEmpty(ValidationRules.RequestNumberValue) Then

                    query += " and " & ValidationRules.RequestNumberColumn & " = '" & ValidationRules.RequestNumberValue & "'"
                    m_bHasWhere = True
                End If

                'Destination
                If ValidationRules.Destination AndAlso Not String.IsNullOrEmpty(ValidationRules.DestinationValue) Then
                    query += " and " & ValidationRules.DestinationColumn & " = '" & ValidationRules.DestinationValue & "'"
                    m_bHasWhere = True
                End If


                'Delivery Form
                If ValidationRules.DeliveryForm AndAlso Not String.IsNullOrEmpty(ValidationRules.DeliveryFormValue) Then
                    query += " and " & ValidationRules.DeliveryFormColumn & " = '" & ValidationRules.DeliveryFormValue & "'"
                    m_bHasWhere = True
                End If

                'Protocol

                If ValidationRules.Protocol AndAlso Not String.IsNullOrEmpty(ValidationRules.ProtocolValue) Then
                    query += " and " & ValidationRules.ProtocolColumn & " = '" & ValidationRules.ProtocolValue & "'"
                    m_bHasWhere = True
                End If

                'Dest. Rack Group Name
                If ValidationRules.DestRackGroupName AndAlso Not String.IsNullOrEmpty(ValidationRules.DestRackGroupNameValue) Then
                    query += " and " & ValidationRules.DestRackGroupNameColumn & " = '" & ValidationRules.DestRackGroupNameValue & "'"
                    m_bHasWhere = True
                End If
            End If

            'query += " order by item_latest_date desc "

            'If Not m_bHasWhere Then
            '    query = "select * from (" & query & ") items where rownum <= 2500 order by rownum"
            'End If

            m_DataSet = ExecuteQuery(query)
            frmMain.grdPendingItems.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Throw New Exception("Error getting pending items")
        End Try

    End Sub


#End Region

#Region "           Add Functions           "

    Public Sub AddContainers(ByVal ParentID As Decimal, ByVal TypeID As Integer, ByVal Origin As String, ByVal Barcode As String, ByVal Ranking As Integer, ByVal PhaseID As Integer, ByVal Note As String, ByVal ReceiverID As Decimal)
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_container.addcontainer", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        'The value is null, then it cannot be set to Nothing or VBNullString, it has to be set to DBNull.Value
        If ParentID = Nothing Then
            m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = DBNull.Value

        Else
            m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ParentID.ToString("F0")
        End If

        m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = TypeID
        m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = Origin
        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = Barcode
        m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = Ranking
        m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = PhaseID
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Note
        m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = ReceiverID.ToString("F0")
        m_command.Parameters.Add("o_container_id", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_container_id").Value)
        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            'ShowMessage("Error adding container - " + m_command.Parameters("o_error_msg").Value)
            Throw New Exception("Error adding container - " + m_command.Parameters("o_error_msg").Value + vbCrLf & _
                "Parent Container ID = " + ParentID.ToString("F0") & _
                ", Type ID = " + TypeID.ToString() & _
                ", Origin = " + Origin.ToString() & _
                ", Barcode = " + Barcode.ToString() & _
                ", Ranking = " + Ranking.ToString() & _
                ", Phase ID = " + PhaseID.ToString() & _
                ", Note = " + Note.ToString() & _
                ", Receiver ID = " + ReceiverID.ToString("F0"))
        End If

    End Sub

    Public Sub AddContainers(ByVal Containers As List(Of ContainerInput))
        Dim m_IsAdded As Boolean = False
        Dim m_ErrorMessage As String = String.Empty

        Try
            Using m_Trans As OracleTransaction = DBConnection.BeginTransaction()
                For Each CurrentContainer As ContainerInput In Containers
                    Using m_command As OracleCommand = New OracleCommand("p_container.addcontainer", DBConnection)

                        m_command.CommandType = CommandType.StoredProcedure
                        m_command.Transaction = m_Trans
                        'The value is null, then it cannot be set to Nothing or VBNullString, it has to be set to DBNull.Value
                        If CurrentContainer.ParentID = Nothing Then
                            m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = DBNull.Value
                        Else
                            m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = CurrentContainer.ParentID.ToString("F0")
                        End If

                        m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = CurrentContainer.TypeID
                        m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = CurrentContainer.Origin
                        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = CurrentContainer.Barcode
                        m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = CurrentContainer.Ranking
                        m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = CurrentContainer.PhaseID
                        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = CurrentContainer.Note
                        m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = CurrentContainer.ReceiverID.ToString("F0")
                        m_command.Parameters.Add("o_container_id", OracleDbType.Decimal).Direction = ParameterDirection.Output
                        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
                        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
                        m_command.ExecuteNonQuery()

                        Console.WriteLine(m_command.Parameters("o_container_id").Value)
                        Console.WriteLine(m_command.Parameters("o_error_code").Value)
                        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

                        If m_command.Parameters("o_error_code").Value = 0 Then
                            m_IsAdded = True

                            'm_Trans.Commit()
                        Else
                            'm_Trans.Rollback()
                            m_IsAdded = False
                            m_ErrorMessage = "Error adding container - " + m_command.Parameters("o_error_msg").Value + vbCrLf & _
                                "Parent Container ID = " + CurrentContainer.ParentID.ToString("F0") & _
                                ", Type ID = " + CurrentContainer.TypeID.ToString() & _
                                ", Origin = " + CurrentContainer.Origin.ToString() & _
                                ", Barcode = " + CurrentContainer.Barcode.ToString() & _
                                ", Ranking = " + CurrentContainer.Ranking.ToString() & _
                                ", Phase ID = " + CurrentContainer.PhaseID.ToString() & _
                                ", Note = " + CurrentContainer.Note.ToString() & _
                                ", Receiver ID = " + CurrentContainer.ReceiverID.ToString("F0")
                        End If
                    End Using

                    If Not m_IsAdded Then
                        Exit For
                    End If
                Next

                If m_IsAdded Then
                    m_Trans.Commit()
                Else
                    m_Trans.Rollback()
                End If

            End Using
        Catch ex As Exception
            Throw New Exception("Error updating container - " + ex.Message)
        End Try

        If Not m_IsAdded Then
            Throw New Exception("Error updating container - " + m_ErrorMessage)
        End If
    End Sub

    Public Sub AddValidationError(ByVal Barcode As String, ByVal PackageName As String, ByVal Account As String, ByVal RuleID As Integer, ByVal ExpectedValue As String, ByVal ActualValue As String)

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = " insert into t_rule_error ( child_barcode, parent_barcode , account , rule_id, expected_value, actual_value) " & _
                    " values ('" & Barcode & "', '" & IIf(String.IsNullOrEmpty(PackageName) = True, DBNull.Value, PackageName.ToString) & "', '" & Account & "'," & RuleID & "  , '" & ExpectedValue & "', '" & ActualValue & "')"

            m_DataSet = ExecuteQuery(query)
        Catch err As Exception
            Console.WriteLine(err.ToString())
            'ShowMessage("Error finalizing the shipment")
            'Throw New Exception("Error recording validation error")
        End Try
    End Sub

    Public Sub AddValidationErrors(ByVal ValidationErrors As List(Of ValidationErrorInput))

        Const commandString As String = " insert into t_rule_error ( child_barcode, parent_barcode , account , rule_id, expected_value, actual_value) values ( :i_child_barcode, :i_parent_barcode , :i_account , :i_rule_id, :i_expected_value, :i_actual_value)"

        Dim ChildBarcodes As New List(Of String)
        Dim ParentBarcodes As New List(Of String)
        Dim Accounts As New List(Of String)
        Dim RuleIds As New List(Of Decimal)
        Dim ExpectedValues As New List(Of String)
        Dim ActualValues As New List(Of String)

        For Each ValidationError As ValidationErrorInput In ValidationErrors

            ChildBarcodes.Add(ValidationError.ChildBarcode)
            ParentBarcodes.Add(ValidationError.ParentBarcode)
            Accounts.Add(ValidationError.Account)
            RuleIds.Add(ValidationError.RuleID)
            ExpectedValues.Add(ValidationError.ExpectedValue)
            ActualValues.Add(ValidationError.ActualValue)

        Next


        Using m_Trans As OracleTransaction = DBConnection.BeginTransaction()

            Using m_command As OracleCommand = New OracleCommand(commandString, DBConnection)

                m_command.CommandType = CommandType.Text
                m_command.Transaction = m_Trans

                m_command.BindByName = True
                m_command.ArrayBindCount = ValidationErrors.Count

                m_command.Parameters.Add("i_child_barcode", OracleDbType.Varchar2).Value = ChildBarcodes.ToArray
                m_command.Parameters.Add("i_parent_barcode", OracleDbType.Varchar2).Value = ParentBarcodes.ToArray
                m_command.Parameters.Add("i_account", OracleDbType.Varchar2).Value = Accounts.ToArray
                m_command.Parameters.Add("i_rule_id", OracleDbType.Decimal).Value = RuleIds.ToArray
                m_command.Parameters.Add("i_expected_value", OracleDbType.Varchar2).Value = ExpectedValues.ToArray
                m_command.Parameters.Add("i_actual_value", OracleDbType.Varchar2).Value = ActualValues.ToArray
                m_command.ExecuteNonQuery()
            End Using
            m_Trans.Commit()
        End Using

    End Sub
#End Region

#Region "           Update Functions            "


    Public Function UpdateBulkContainers(ByVal ContainerIDs As List(Of Decimal), ByVal ParentIDs As List(Of Decimal), ByVal TypeIDs As List(Of Integer), ByVal Origins As List(Of String), ByVal Barcodes As List(Of String), ByVal Rankings As List(Of Integer), ByVal PhaseIDs As List(Of Decimal), ByVal Notes As List(Of String), ByVal ReceiverIDs As List(Of Decimal)) As Boolean

        ' NOT WORKING -- I think I need to use Associative arrays

        Dim m_HasErrors As Boolean = False
        Dim m_IsAdded As Boolean = False
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        Dim ErrorCodes As List(Of Decimal)
        Dim ErrorMessages As List(Of String)


        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_container.updatecontainer", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans

        m_command.ArrayBindCount = ContainerIDs.Count

        m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = ContainerIDs.ToArray()
        m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ParentIDs.ToArray
        m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = TypeIDs.ToArray()
        m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = Origins.ToArray()
        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = Barcodes.ToArray()
        m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = Rankings.ToArray()
        m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = PhaseIDs.ToArray()
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Notes.ToArray()
        m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = ReceiverIDs.ToArray()
        'm_command.Parameters.Add("o_container_id", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()


        ErrorCodes = m_command.Parameters("o_error_code").Value
        ErrorMessages = m_command.Parameters("o_error_msg").Value

        For Each CurrentCode As Decimal In ErrorCodes
            If CurrentCode <> 0 Then
                m_HasErrors = True
                Exit For
            End If
        Next

        If Not m_HasErrors Then
            m_Trans.Commit()
            m_IsAdded = True
        Else
            m_Trans.Rollback()
            m_IsAdded = False
            'ShowMessage("Error updating container - " + m_command.Parameters("o_error_msg").Value)
            Throw New Exception("Error updating containers ")
        End If
        Return m_IsAdded

    End Function

    'Public Function UpdateBulkContainers(ByVal ParentID As Decimal, ByVal Containers As Containers) As Boolean
    '    Dim ContainerIDs As List(Of Decimal)
    '    Dim ParentIDs As List(Of Decimal)
    '    Dim TypeIDs As List(Of Decimal)
    '    Dim Origins As List(Of String)
    '    Dim Barcodes As List(Of String)
    '    Dim Rankings As List(Of Decimal)
    '    Dim PhaseIDs As List(Of Decimal)
    '    Dim Notes As List(Of String)
    '    Dim ReceiverIDs As List(Of Decimal)


    '    For Each CurrentContainer As Container In Containers
    '        ContainerIDs.Add(CurrentContainer.ContainerID)
    '        ParentIDs.Add(CurrentContainer.ParentContainerID)
    '        'TypeIDs.Add(CurrentContainer.TypeID)
    '        'Origins.Add(CurrentContainer.TypeName)
    '        Barcodes.Add(CurrentContainer.Barcode)
    '        Rankings.Add(CurrentContainer.Ranking)
    '        'PhaseIDs.Add(CurrentContainer.PhaseID)
    '        'Notes.Add(CurrentContainer.Note)
    '        'ReceiverIDs.Add(CurrentContainer.ReceiverID)
    '    Next

    '    Dim m_IsAdded As Boolean = False
    '    Dim m_command As OracleCommand
    '    Dim m_Trans As OracleTransaction

    '    m_Trans = DBConnection.BeginTransaction()

    '    m_command = New OracleCommand("p_container.updatecontainer", DBConnection)
    '    m_command.CommandType = CommandType.StoredProcedure
    '    m_command.Transaction = m_Trans

    '    m_command.ArrayBindCount = Containers.Count

    '    m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = ContainerIDs
    '    m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ParentIDs
    '    m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = TypeIDs.ToArray()
    '    m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = Origins
    '    m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = Barcodes
    '    m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = Rankings
    '    m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = PhaseIDs
    '    m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Notes
    '    m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = ReceiverIDs
    '    'm_command.Parameters.Add("o_container_id", OracleDbType.Decimal).Direction = ParameterDirection.Output
    '    m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
    '    m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
    '    m_command.ExecuteNonQuery()

    '    'Console.WriteLine(m_command.Parameters("o_container_id").Value)
    '    Console.WriteLine(m_command.Parameters("o_error_code").Value)
    '    Console.WriteLine(m_command.Parameters("o_error_msg").Value)

    '    If m_command.Parameters("o_error_code").Value = 0 Then
    '        m_Trans.Commit()
    '        m_IsAdded = True
    '    Else
    '        m_Trans.Rollback()
    '        m_IsAdded = False
    '        'ShowMessage("Error updating container - " + m_command.Parameters("o_error_msg").Value)
    '        Throw New Exception("Error updating container - " + m_command.Parameters("o_error_msg").Value)
    '    End If
    '    Return m_IsAdded

    'End Function

    Public Function UpdateContainers(ByVal ContainerID As Decimal, ByVal ParentID As Decimal, ByVal TypeID As Integer, ByVal Origin As String, ByVal Barcode As String, ByVal Ranking As Integer, ByVal PhaseID As Integer, ByVal Note As String, ByVal ReceiverID As Decimal) As Boolean
        Dim m_IsAdded As Boolean = False
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_container.updatecontainer", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans

        m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = ContainerID.ToString("F0")
        m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ParentID.ToString("F0")
        m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = TypeID
        m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = Origin
        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = Barcode
        m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = Ranking
        m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = PhaseID
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Note
        m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = ReceiverID.ToString("F0")
        'm_command.Parameters.Add("o_container_id", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        'Console.WriteLine(m_command.Parameters("o_container_id").Value)
        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
            m_IsAdded = True
        Else
            m_Trans.Rollback()
            m_IsAdded = False
            'ShowMessage("Error updating container - " + m_command.Parameters("o_error_msg").Value)
            Throw New Exception("Error updating container - " + m_command.Parameters("o_error_msg").Value)
        End If
        Return m_IsAdded
    End Function

    Public Function UpdateContainers(ByVal Containers As List(Of ContainerInput)) As Boolean
        Dim m_IsUpdated As Boolean = False
        Dim m_ErrorMessage As String = String.Empty

        Try
            Using m_Trans As OracleTransaction = DBConnection.BeginTransaction()
                For Each CurrentContainer As ContainerInput In Containers
                    Using m_command As OracleCommand = New OracleCommand("p_container.updatecontainer", DBConnection)

                        m_command.CommandType = CommandType.StoredProcedure
                        m_command.Transaction = m_Trans

                        m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = CurrentContainer.ContainerID.ToString("F0")
                        m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = CurrentContainer.ParentID.ToString("F0")
                        m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = CurrentContainer.TypeID
                        m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = CurrentContainer.Origin
                        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = CurrentContainer.Barcode
                        m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = CurrentContainer.Ranking
                        m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = CurrentContainer.PhaseID
                        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = CurrentContainer.Note
                        m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = CurrentContainer.ReceiverID.ToString("F0")

                        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
                        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
                        m_command.ExecuteNonQuery()

                        Console.WriteLine(m_command.Parameters("o_error_code").Value)
                        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

                        If m_command.Parameters("o_error_code").Value = 0 Then
                            m_IsUpdated = True

                            'm_Trans.Commit()
                        Else
                            'm_Trans.Rollback()
                            m_IsUpdated = False
                            m_ErrorMessage = "Error adding container - " + m_command.Parameters("o_error_msg").Value.ToString() + vbCrLf + _
                                "Parent Container ID = " + CurrentContainer.ParentID.ToString("F0") + _
                                ", Type ID = " + CurrentContainer.TypeID.ToString() + _
                                ", Origin = " + CurrentContainer.Origin.ToString() + _
                                ", Barcode = " + CurrentContainer.Barcode.ToString() + _
                                ", Ranking = " + CurrentContainer.Ranking.ToString() + _
                                ", Phase ID = " + CurrentContainer.PhaseID.ToString() + _
                                ", Note = " + CurrentContainer.Note.ToString() + _
                                ", Receiver ID = " + CurrentContainer.ReceiverID.ToString("F0")
                        End If
                    End Using

                    If Not m_IsUpdated Then
                        Exit For
                    End If
                Next

                If m_IsUpdated Then
                    m_Trans.Commit()
                Else
                    m_Trans.Rollback()
                End If

            End Using
        Catch ex As Exception
            Throw New Exception("Error updating container - " + ex.Message)
        End Try

        If Not m_IsUpdated Then
            Throw New Exception("Error updating container - " + m_ErrorMessage)
        End If

        Return m_IsUpdated
    End Function

#End Region

#Region "           Remove Functions            "

    Public Function RemoveContainersFromPackage(ByVal ContainerID As Decimal, ByVal PhaseID As Integer) As Boolean
        Dim m_IsRemoved As Boolean = False
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_container.updatecontainer", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans

        m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = ContainerID.ToString("F0")
        m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ContainerID.ToString("F0")
        m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = DBNull.Value 'TypeID
        m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = DBNull.Value 'Origin
        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = DBNull.Value 'Barcode
        m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = DBNull.Value 'Ranking
        m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = PhaseID
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = DBNull.Value 'Note
        m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = DBNull.Value 'ReceiverID.ToString("F0")
        'm_command.Parameters.Add("o_container_id", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()


        'Console.WriteLine(m_command.Parameters("o_container_id").Value)
        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
            m_IsRemoved = True
        Else
            m_Trans.Rollback()
            'ShowMessage("Error updating container - " + m_command.Parameters("o_error_msg").Value)
            m_IsRemoved = False
            Throw New Exception("Error updating container - " + m_command.Parameters("o_error_msg").Value)
        End If

        Return m_IsRemoved
    End Function

    Public Function RemoveContainersFromPackage(ByVal ContainerIDs As List(Of Decimal), ByVal PhaseIDs As List(Of Integer)) As Boolean

        'NOT WORKING - Throws an Oracle Exception. I need to use associative arrays.

        Dim m_HasErrors As Boolean = False
        Dim m_IsRemoved As Boolean = False

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        Dim ErrorCodes As List(Of Decimal)
        Dim ErrorMessages As List(Of String)

        m_Trans = DBConnection.BeginTransaction()

        Dim NullDecimal As New List(Of Decimal)
        Dim Nulls As New List(Of DBNull)

        Try

            'm_Trans = AppConnectionObjects.m_OracleConnection_G.BeginTransaction()
            m_command = New OracleCommand("p_container.updatecontainer", DBConnection)
            m_command.CommandType = CommandType.StoredProcedure
            m_command.Transaction = m_Trans

            m_command.ArrayBindCount = ContainerIDs.Count

            For i As Integer = 0 To ContainerIDs.Count - 1
                Nulls.Add(DBNull.Value)
            Next

            'Dim ContainerIDParm As New OracleParameter("i_container_id", OracleDbType.Decimal, ParameterDirection.Input)
            'ContainerIDParm.Value = ContainerIDs.ToArray()
            'ContainerIDParm.ArrayBindSize = New Integer()
            'ContainerIDParm.ArrayBindSize


            m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = ContainerIDs.ToArray()
            m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ContainerIDs.ToArray()
            m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = DBNull.Value 'TypeID
            m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = DBNull.Value 'Origin
            m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = DBNull.Value 'Barcode
            m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = DBNull.Value 'Ranking
            m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = PhaseIDs.ToArray()
            m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = DBNull.Value 'Note
            m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = DBNull.Value 'ReceiverID.ToString("F0")
            'm_command.Parameters.Add("o_container_id", OracleDbType.Decimal).Direction = ParameterDirection.Output
            m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output

            Dim ErrorParm As New OracleParameter("o_error_msg", OracleDbType.Varchar2, 1000, ParameterDirection.Output)
            ErrorParm.ArrayBindSize = New Integer() {1000, 1000, 1000}
            'm_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
            m_command.ExecuteNonQuery()

            'Console.WriteLine(m_command.Parameters("o_container_id").Value)
            'Console.WriteLine(m_command.Parameters("o_error_code").Value)
            'Console.WriteLine(m_command.Parameters("o_error_msg").Value)

            ErrorCodes = m_command.Parameters("o_error_code").Value
            ErrorMessages = m_command.Parameters("o_error_msg").Value

            For Each CurrentCode As Decimal In ErrorCodes
                If CurrentCode <> 0 Then
                    m_HasErrors = True
                    Exit For
                End If
            Next

            If Not m_HasErrors Then
                'm_Trans.Commit()
                m_IsRemoved = True
            Else
                'm_Trans.Rollback()
                'ShowMessage("Error updating container - " + m_command.Parameters("o_error_msg").Value)
                m_IsRemoved = False
                Throw New Exception("Error updating containers - ")
            End If

        Catch o_ex As OracleException

            ShowMessage(o_ex.Message)
            m_IsRemoved = False
            m_HasErrors = True
        Catch ex As Exception

            ShowMessage(ex.Message)
            m_IsRemoved = False
            m_HasErrors = True
        Finally

            If Not m_HasErrors Then
                m_Trans.Commit()
            Else
                m_Trans.Rollback()
                'ShowMessage("Error updating container - " + m_command.Parameters("o_error_msg").Value)
            End If
        End Try

        Return m_IsRemoved

    End Function

    Public Function BulkRemoveContainersFromPackage(ByVal ContainerIDs As List(Of Decimal)) As Boolean

        Const m_RemoveContainerPhaseID As Integer = 50002
        Dim m_IsRemoved As Boolean = False
        Dim m_ErrorMessage As String = String.Empty

        Try
            Using m_Trans As OracleTransaction = DBConnection.BeginTransaction()
                For Each ContainerID As Decimal In ContainerIDs

                    Using m_command As OracleCommand = New OracleCommand("p_container.updatecontainer", DBConnection)

                        m_command.CommandType = CommandType.StoredProcedure
                        m_command.Transaction = m_Trans

                        m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = ContainerID.ToString("F0")
                        m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ContainerID.ToString("F0")
                        m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = DBNull.Value 'TypeID
                        m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = DBNull.Value 'Origin
                        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = DBNull.Value 'Barcode
                        m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = DBNull.Value 'Ranking
                        m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = m_RemoveContainerPhaseID
                        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = DBNull.Value 'Note
                        m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = DBNull.Value 'ReceiverID.ToString("F0")
                        'm_command.Parameters.Add("o_container_id", OracleDbType.Decimal).Direction = ParameterDirection.Output
                        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
                        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
                        m_command.ExecuteNonQuery()

                        'Console.WriteLine(m_command.Parameters("o_error_code").Value)
                        'Console.WriteLine(m_command.Parameters("o_error_msg").Value)

                        If m_command.Parameters("o_error_code").Value = 0 Then
                            m_IsRemoved = True
                        Else
                            m_IsRemoved = False
                            m_ErrorMessage = m_command.Parameters("o_error_msg").Value.ToString
                        End If
                    End Using

                    If Not m_IsRemoved Then
                        Exit For
                    End If
                Next

                If m_IsRemoved Then
                    m_Trans.Commit()
                Else
                    m_Trans.Rollback()
                End If

            End Using

        Catch ex As Exception
            Throw New Exception("Error updating container - " + ex.Message)
        End Try

        If Not m_IsRemoved Then
            Throw New Exception("Error updating container - " + m_ErrorMessage)
        End If

        Return m_IsRemoved

    End Function

    'Public Function BulkProcedureRemoveContainers(ByVal InputContainerTable As UpdateContainerTable, ByVal OutputUpdateContainerResponseTable As UpdateContainerResponseTable) As Boolean 'ByVal ContainerIDs As List(Of Decimal)

    '    Dim m_IsRemoved As Boolean = False
    '    Dim m_ErrorMessage As String = String.Empty

    '    Try
    '        Using m_Trans As OracleTransaction = DBConnection.BeginTransaction()

    '            Using m_command As OracleCommand = New OracleCommand("bulk_update_container", DBConnection)

    '                m_command.CommandType = CommandType.StoredProcedure
    '                m_command.Transaction = m_Trans


    '                Dim i_container_list As OracleParameter = New OracleParameter() With {.ParameterName = "i_container_list", .UdtTypeName = "T_UPDATE_CONTAINER_TAB", .OracleDbType = OracleDbType.Clob, .Value = InputContainerTable, .Direction = ParameterDirection.Input, .Size = InputContainerTable.Value.Length}
    '                m_command.Parameters.Add(i_container_list)
    '                Dim o_container_list As OracleParameter = New OracleParameter() With {.ParameterName = "o_container_list_resp", .OracleDbType = OracleDbType.Clob, .UdtTypeName = "T_UPDATE_CONTAINER_RESP_TAB", .Direction = ParameterDirection.InputOutput, .Value = OutputUpdateContainerResponseTable, .Size = OutputUpdateContainerResponseTable.Value.Length}
    '                m_command.Parameters.Add(o_container_list)

    '                m_command.ExecuteNonQuery()

    '                For Each TempResponseObject As UpdateContainerResponseObject In OutputUpdateContainerResponseTable.Value
    '                    If TempResponseObject.ERROR_CODE = 0 Then
    '                        m_IsRemoved = True
    '                    Else
    '                        m_IsRemoved = False
    '                        m_ErrorMessage += TempResponseObject.ERROR_MSG & vbCrLf
    '                    End If
    '                Next

    '            End Using

    '            If m_IsRemoved Then
    '                m_Trans.Commit()
    '            Else
    '                m_Trans.Rollback()
    '            End If

    '        End Using
    '    Catch ex As Exception
    '        Throw New Exception("Error updating container via bulk procedure - " + ex.Message)
    '    End Try

    '    If Not m_IsRemoved Then
    '        Throw New Exception("Error updating container - " + m_ErrorMessage)
    '    End If

    '    Return m_IsRemoved
    'End Function

#End Region

#Region "           Create Functions            "


#End Region

#End Region

    ''' <summary>
    ''' Get the current list of shipping carrier from SMF_SHIPPING
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetFulfillmentSite(ByVal FulfillmentSite As String)

        Dim FulfillmentSites As List(Of String) = New List(Of String)

        FulfillmentSites.Add("Basel Switzerland")
        FulfillmentSites.Add("Cambridge United States")
        FulfillmentSites.Add("Emeryville United States")
        FulfillmentSites.Add("Horsham England")
        FulfillmentSites.Add("Shanghai China")
        FulfillmentSites.Add("Singapore")




        Try


            frmMain.lkFulfillmentSite.Properties.DataSource = FulfillmentSites
            'frmMain.lkFulfillmentSite.Properties.DisplayMember = "FULFILLMENT_SITE"
            'frmMain.lkFulfillmentSite.Properties.ValueMember = "FULFILLMENT_SITE"
            'frmMain.lkFulfillmentSite.Properties.PopulateColumns()

            frmMain.lkFulfillmentSite.EditValue = FulfillmentSite

        Catch
            Throw New Exception("Error getting fulfillment site")
        End Try
    End Sub

    ' ''' <summary>
    ' ''' Get the list of potential logins
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Sub GetDefaultLogin(ByVal DefaultLogin As String)

    '    Dim GSTLogins As List(Of String) = New List(Of String)

    '    GSTLogins.Add("General")
    '    GSTLogins.Add("Sample Transfer")

    '    Try

    '        frmMain.lkDefaultLogin.Properties.DataSource = GSTLogins

    '        frmMain.lkDefaultLogin.EditValue = DefaultLogin

    '    Catch
    '        Throw New Exception("Error getting default function")
    '    End Try
    'End Sub


    Public Sub New()
        MyBase.New()
    End Sub
End Class
