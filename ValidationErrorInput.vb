

Public Class ValidationErrorInput

    Private m_sChildBarcode As String
    Private m_sParentBarcode As String
    Private m_sAccount As String
    Private m_dRuleID As Decimal
    Private m_sExpectedValue As String
    Private m_sActualValue As String

    Public Property ChildBarcode As String
        Get
            Return m_sChildBarcode
        End Get
        Set(value As String)
            If m_sChildBarcode = value Then
                Return
            End If
            m_sChildBarcode = value
        End Set
    End Property
    Public Property ParentBarcode As String
        Get
            Return m_sParentBarcode
        End Get
        Set(value As String)
            If m_sParentBarcode = value Then
                Return
            End If
            m_sParentBarcode = value
        End Set
    End Property
    Public Property Account As String
        Get
            Return m_sAccount
        End Get
        Set(value As String)
            If m_sAccount = value Then
                Return
            End If
            m_sAccount = value
        End Set
    End Property
    Public Property RuleID As Decimal
        Get
            Return m_dRuleID
        End Get
        Set(value As Decimal)
            If m_dRuleID = value Then
                Return
            End If
            m_dRuleID = value
        End Set
    End Property
    Public Property ExpectedValue As String
        Get
            Return m_sExpectedValue
        End Get
        Set(value As String)
            If m_sExpectedValue = value Then
                Return
            End If
            m_sExpectedValue = value
        End Set
    End Property
    Public Property ActualValue As String
        Get
            Return m_sActualValue
        End Get
        Set(value As String)
            If m_sActualValue = value Then
                Return
            End If
            m_sActualValue = value
        End Set
    End Property

    Public Sub New(ByVal ChildBarcode As String, ByVal ParentBarcode As String, ByVal Account As String, ByVal RuleID As Decimal, ByVal ExpectedValue As String, ByVal ActualValue As String)
        Me.ChildBarcode = ChildBarcode
        Me.ParentBarcode = ParentBarcode
        Me.Account = Account
        Me.RuleID = RuleID
        Me.ExpectedValue = ExpectedValue
        Me.ActualValue = ActualValue
    End Sub
End Class