Public Class frmInvalidItem

    Private m_bOk As Boolean = False

    Private m_sBarcode As String

    Public ReadOnly Property Barcode As String
        Get
            Return m_sBarcode
        End Get
    End Property

    Public ReadOnly Property Ok() As Boolean
        Get
            Return m_bOk
        End Get
    End Property

    Public Sub New(ByVal Barcode As String, ByVal Message As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_sBarcode = Barcode
        Dim TempMessage As String = Message
        txtInfo.Text = TempMessage
        Me.lblInvalidBarcode.Text = "Remove Item and Rescan"
        Me.lblInvalidBarcode.Visible = True
        txtBarcode.Focus()
    End Sub


    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        Dim TempBarcodeInput As String = txtBarcode.Text

        If String.Compare(TempBarcodeInput, Barcode, True) = 0 Then
            Me.Close()
        Else
            Me.lblInvalidBarcode.Text = "Invalid Barcode. Please Try Again"
            Me.lblInvalidBarcode.Visible = True
            txtBarcode.Text = String.Empty
            txtBarcode.Focus()
        End If

    End Sub

End Class