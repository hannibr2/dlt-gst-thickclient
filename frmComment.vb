Public Class frmComment

    Private m_comment As String

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        m_comment = txtComment.Text
        Me.Close()
        Me.Dispose()
    End Sub

    Public ReadOnly Property Comment() As String
        Get
            Return m_comment
        End Get
    End Property

End Class