﻿Public Class Package


#Region "           Declarations            "

    Private m_parent_container_id As Decimal
    Private m_barcode As String = String.Empty
    Private m_first_name As String = String.Empty
    Private m_last_name As String = String.Empty
    Private m_company As String = String.Empty
    Private m_country_name As String = String.Empty
    Private m_mail_address As String = String.Empty
    Private m_site_code As String = String.Empty
    Private m_phase_id As Decimal

    Private m_containers As Containers
#End Region



#Region "           Properties          "
    Public Property ParentContainerID() As Decimal
        Get
            Return m_parent_container_id
        End Get
        Set(ByVal value As Decimal)
            If m_parent_container_id = value Then
                Return
            End If
            m_parent_container_id = value
        End Set
    End Property

    Public Property Barcode() As String
        Get
            Return m_barcode
        End Get
        Set(ByVal value As String)
            If m_barcode = value Then
                Return
            End If
            m_barcode = value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return m_first_name
        End Get
        Set(ByVal value As String)
            If m_first_name = value Then
                Return
            End If
            m_first_name = value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return m_last_name
        End Get
        Set(ByVal value As String)
            If m_last_name = value Then
                Return
            End If
            m_last_name = value
        End Set
    End Property
    Public Property Company() As String
        Get
            Return m_company
        End Get
        Set(ByVal value As String)
            If m_company = value Then
                Return
            End If
            m_company = value
        End Set
    End Property
    Public Property CountryName() As String
        Get
            Return m_country_name
        End Get
        Set(ByVal value As String)
            If m_country_name = value Then
                Return
            End If
            m_country_name = value
        End Set
    End Property
    Public Property MailAddress() As String
        Get
            Return m_mail_address
        End Get
        Set(ByVal value As String)
            If m_mail_address = value Then
                Return
            End If
            m_mail_address = value
        End Set
    End Property
    Public Property SiteCode() As String
        Get
            Return m_site_code
        End Get
        Set(ByVal value As String)
            If m_site_code = value Then
                Return
            End If
            m_site_code = value
        End Set
    End Property

    Public Property PhaseID As Decimal
        Get
            Return m_phase_id
        End Get
        Set(value As Decimal)
            If m_phase_id = value Then
                Return
            End If
            m_phase_id = value
        End Set
    End Property

    Public ReadOnly Property Containers() As Containers
        Get
            If m_containers Is Nothing Then
                Dim ContainerDAL As New Oracle_Container
                m_containers = New Containers(ContainerDAL.GetContainersByParentContainerID(ParentContainerID))
            End If
            Return m_containers
        End Get
    End Property

    Public ReadOnly Property SolutionContainerCount() As Integer
        Get
            Return Containers.SolutionCount
        End Get
    End Property

    Public ReadOnly Property PowderContainerCount() As Integer
        Get
            Return Containers.PowderCount
        End Get
    End Property

    Public ReadOnly Property HasAttachments() As Boolean
        Get
            Return Me.Containers.HasAttachments
        End Get
    End Property

#End Region

    Public Sub New(ByVal PackageRow As DataRow)

        If Not IsDBNull(PackageRow.Item("parent_container_id")) Then
            ParentContainerID = PackageRow.Item("parent_container_id")
        End If

        If Not IsDBNull(PackageRow.Item("barcode")) Then
            Barcode = PackageRow.Item("barcode").ToString()
        End If

        If Not IsDBNull(PackageRow.Item("first_name")) Then
            FirstName = PackageRow.Item("first_name").ToString()
        End If

        If Not IsDBNull(PackageRow.Item("last_name")) Then
            LastName = PackageRow.Item("last_name").ToString()
        End If

        If Not IsDBNull(PackageRow.Item("company")) Then
            Company = PackageRow.Item("company").ToString()
        End If

        If Not IsDBNull(PackageRow.Item("country_name")) Then
            CountryName = PackageRow.Item("country_name").ToString()
        End If


        If Not IsDBNull(PackageRow.Item("mail_address")) Then
            MailAddress = PackageRow.Item("mail_address").ToString()
        End If


        If Not IsDBNull(PackageRow.Item("site_code")) Then
            SiteCode = PackageRow.Item("site_code").ToString()
        End If

        If Not IsDBNull(PackageRow.Item("phase_id")) Then
            PhaseID = PackageRow.Item("phase_id")
        End If

    End Sub

End Class
