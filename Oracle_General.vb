﻿'Imports System.Data


'Imports Oracle.DataAccess.Client
'Imports Oracle.DataAccess.Types
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports ExceptionsHandlerLib
'Imports System.Data.OracleClient
Imports System.IO
Imports System.Threading
Imports System.Text
Imports GST.NibrIAMAuth.NibrIAMAuth
Imports System.Net
Imports System.Xml
Imports System.Configuration


Public Class Oracle_General

#Region "       Declarations"


    Private m_OracleConnection As OracleConnection

    Private m_CompoundFilePath As String ' = My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData + "\\compounds.txt"
    Private m_sMachineName As String

    Private m_sSystemDescription As String

    Public m_OracleConnected As Boolean
    Public sw As New Stopwatch
    Dim cMessageLog As New CMessageLog()


#End Region

#Region "       Properties      "


    Public Property OracleConnected() As Boolean

        Get
            ' Return m_OracleConnected
            Return AppConnectionObjects.m_OracleConnected_G
        End Get
        Set(ByVal value As Boolean)

            'm_OracleConnected = value
            AppConnectionObjects.m_OracleConnected_G = value
        End Set
    End Property

    Public ReadOnly Property DBConnection As OracleConnection
        Get
            If AppConnectionObjects.m_OracleConnection_G Is Nothing Then
                ConnectToDatabase()
            End If
            Return AppConnectionObjects.m_OracleConnection_G
        End Get
    End Property
#End Region

#Region "       Connection      "

    ''' <summary>
    ''' Open Oracle connections to required databases
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ConnectToDatabase()      'ByVal Env As String

        sw.Restart()
        AppConnectionObjects.m_OracleConnection_G = New OracleConnection()


        Dim OracleCredentials As New Oracle_Credentials
        AppConnectionObjects.m_OracleConnection_G.ConnectionString = OracleCredentials.ConnectionString

        'Try connecting to SMF_SHIPPING
        Try
            AppConnectionObjects.m_OracleConnection_G.Open()
            OracleConnected = True
            m_OracleConnected = True
        Catch err As Exception
            m_OracleConnected = False
            OracleConnected = False
            Console.WriteLine(err.ToString())
            Throw New Exception("Error connecting to database SMF_SHIPPING. Please check your Oracle connection.")
            'ShowMessage("Error connecting to SMF_SHIPPING")
        End Try
        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)

    End Sub

    ''' <summary>
    ''' Disconnect all Oracle connections
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub DisconnectFromDatabase()


        AppConnectionObjects.m_OracleConnection_G.Close()
        AppConnectionObjects.m_OracleConnection_G = Nothing

    End Sub
#End Region


    ''' <summary>
    ''' Execute the query and return the result as a data set
    ''' </summary>
    ''' <param name="Query">Query to be executed</param>
    ''' <returns>Result Dataset</returns>
    ''' <remarks></remarks>
    Public Function ExecuteQuery(ByVal Query As String) As DataSet
        Dim m_DataSet As DataSet
        Dim m_command As OracleCommand
        Dim m_OracleAdapter As OracleDataAdapter
        sw.Restart()
        Try
            If Not OracleConnected Then
                ConnectToDatabase()
            End If
            m_command = New OracleCommand(Query, AppConnectionObjects.m_OracleConnection_G)
            'm_command.FetchSize = 1024 * 1024 * 10 ' 10 MB
            'm_command.FetchSize = m_command.RowSize * 1000
            m_OracleAdapter = New OracleDataAdapter(m_command)
            m_DataSet = New DataSet()
            m_OracleAdapter.Fill(m_DataSet)
            m_OracleAdapter = Nothing
            sw.Stop()
            ThreadPool.UnsafeQueueUserWorkItem(New WaitCallback(AddressOf logMessageAsynch), Query)
            Return m_DataSet
        Catch ex As Exception
            cMessageLog.SendMail("Oracle Error:" + ex.Message + vbCrLf + Query)
            cMessageLog.LogBenchMarks("Oracle Error:" + ex.Message + " Query:" + Query, ConfigurationManager.AppSettings.Get("Environment").ToUpper)
        End Try
    End Function

    
    Public Function logMessageAsynch(ByVal query As String)
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString() + " Query: " + query, ConfigurationManager.AppSettings.Get("Environment").ToUpper)
    End Function

    

#Region "Old SOA functions"
    Private Sub GetSystemInfo()
        m_sMachineName = System.Environment.MachineName
        m_sUserName = System.Environment.UserDomainName
        'm_sSystemDescription = System.Environment.
    End Sub

    Private Function BreakdownSamplesList(ByVal ListOfSamples As String) As ArrayList

        Dim ResultArray As String()
        Dim TempArray As ArrayList

        TempArray = New ArrayList()

        ResultArray = ListOfSamples.Split(vbCrLf.ToCharArray())

        For Each temp As String In ResultArray
            temp = Trim(temp)
            If temp <> "" Then TempArray.Add(temp)
        Next

        Return TempArray

    End Function

    Public Function IsAdminMode(ByVal Username As String, ByVal Password As String) As Boolean

        Dim query As String
        Dim m_DataSet As DataSet
        Dim m_command As OracleCommand
        Dim m_OracleAdapter As OracleDataAdapter
        Dim iReturnValue As Integer

        'Check whether username is in the customer table
        query = "select count(*) from v_customer_user where user_role_id >= 610 and customer_id = 4 and unique_id = '" + UCase(Username) + "'"
        m_command = New OracleCommand(query, AppConnectionObjects.m_OracleConnection_G)
        m_OracleAdapter = New OracleDataAdapter(m_command)
        m_DataSet = New DataSet()
        m_OracleAdapter.Fill(m_DataSet)
        m_OracleAdapter = Nothing
        iReturnValue = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        If iReturnValue <> 1 Then Return False

        'Check password authentication
        query = "select ur_access.raas.authenticate( '" + Username + "', '" + Password + "' ) as is_authenticated from dual"
        m_command = New OracleCommand(query, AppConnectionObjects.m_OracleConnection_G)
        m_OracleAdapter = New OracleDataAdapter(m_command)
        m_DataSet = New DataSet()
        m_OracleAdapter.Fill(m_DataSet)
        m_OracleAdapter = Nothing
        iReturnValue = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        If iReturnValue = 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Sub ShowUserQuery(ByVal AdminMode As Boolean)

        Dim query As String
        Dim m_DataSet As DataSet
        Dim m_command As OracleCommand
        Dim m_OracleAdapter As OracleDataAdapter

        If AdminMode Then
            query = "select query_name, description from v_user_query"
        Else
            query = "select query_name, description from v_user_query where user_role_id < 610"
        End If

        m_command = New OracleCommand(query, AppConnectionObjects.m_OracleConnection_G)
        m_OracleAdapter = New OracleDataAdapter(m_command)
        m_DataSet = New DataSet()
        m_OracleAdapter.Fill(m_DataSet)
        m_OracleAdapter = Nothing
        'frmMain.grdQuery.DataSource = m_DataSet.Tables(0)

    End Sub
#End Region


    ''' <summary>
    ''' Get current country of the user
    ''' </summary>
    ''' <param name="UserName">User's 5-2-1</param>
    ''' <remarks></remarks>
    Public Sub GetCurrentCountry(ByVal UserName As String)
        Dim query As String
        Dim m_DataSet As DataSet



        query = "select site, country_name, first_name, last_name, person_id, site_code from v_person where account = '" + UserName.ToUpper + "'"
        m_DataSet = ExecuteQuery(query)
        'Check if the user name exists in the database
        m_sUserSite = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        m_sUserCountryName = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(1)
        m_sUserFullName = String.Format("{0} {1}", m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(2), m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(3))
        m_sUserID = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(4)
        m_sUserSiteCode = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(5)



    End Sub

    ''' <summary>
    ''' Validate current user
    ''' </summary>
    ''' <param name="UserName">User's 5-2-1</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateUser(ByVal UserName As String) As Boolean
        Dim query As String
        Dim m_DataSet As DataSet

        'Look up user name
        query = "select count(*) from v_person where account = '" + UserName + "'"
        m_DataSet = ExecuteQuery(query)

        'Check if the user name exists in the database
        If m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0) = "1" Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function AuthorizeUser(ByVal UserName As String) As Boolean
        Dim query As String
        Dim m_DataSet As DataSet

        'Look up user name
        query = "select count(*) from v_user_role where unique_id = '" + UserName + "'"
        m_DataSet = ExecuteQuery(query)

        'Check if the user name exists in the database
        If m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0) = "1" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ValidatePassword(ByVal Username As String, ByVal Password As String) As Boolean
        Dim query As String
        Dim m_DataSet As DataSet

        'Check password authentication
        query = "select ur_access.raas.authenticate( '" + Username.ToUpper + "', '" + Password + "' ) as is_authenticated from dual"
        m_DataSet = ExecuteQuery(query)

        If m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0) = 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function ValidatePasswordIAM(ByVal Username As String, ByVal Password As String, ByVal Domain As String) As String
        Dim IAMUrl As String = "http://nebulacdn.na.novartis.net/sec/util/auth.php"
        'Dim Domain As String = ""

        Dim ReturnString As String = String.Empty
        Dim Req As HttpWebRequest = CType(HttpWebRequest.Create(IAMUrl), HttpWebRequest)
        Req.Method = "POST"


        Try
            NibrIAMAuth.NibrIAMAuth.InitIAM(New Uri(IAMUrl), Username, Password, Domain)

            Dim res As HttpWebResponse = CType(Req.GetAuthResponse(), HttpWebResponse)
            Dim resCont As Stream = res.GetResponseStream()
            Dim tempString As String = String.Empty
            Dim count As Integer = 0
            Dim buf(8192) As Byte

            Dim sb As StringBuilder = New StringBuilder()

            'Do
            '    count = resCont.Read(buf, 0, buf.Length)
            '    If Not count = 0 Then
            '        tempString = Encoding.ASCII.GetString(buf, 0, count)
            '        sb.Append(tempString)
            '    End If
            '    If count <= 0 Then
            '        Exit Do
            '    End If
            'Loop

            Using reader As XmlReader = XmlReader.Create(resCont)
                reader.ReadToFollowing("userid")
                ReturnString = reader.ReadElementContentAsString()
            End Using

            resCont.Close()
            res.Close()
        Catch ex As Exception
            ReturnString = "Error"
        End Try

        Return ReturnString
    End Function

    Public Sub ShowRecipientInfo()
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            'query = "select first_name, last_name, company, department, country_name from v_person where person_id is not null"
            query = "select * from v_person where person_id is not null"

            m_DataSet = ExecuteQuery(query)
            RecipientInfo.grdRecipientInfo.DataSource = m_DataSet.Tables(0)
        Catch
            Throw New Exception("Error getting recipient list")
        End Try

    End Sub


    Public Sub ShowRecipientSearch(ByVal FIRSTNAME As String, ByVal LASTNAME As String, ByVal COMPANY As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select * from v_person where upper(first_name) like '" + FIRSTNAME.ToUpper() + _
                    "%' and upper(last_name) like '" + LASTNAME.ToUpper() + _
                    "%' and upper(company) like '" + COMPANY.ToUpper() + "%'" + _
                    " and person_type = 'external' "

            m_DataSet = ExecuteQuery(query)
            RecipientInfo.grdRecipientInfo.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Console.WriteLine(err.ToString())
            Throw New Exception("Error searching for recipient")
        End Try

    End Sub

    Public Sub ShowRecipients()
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select * from v_person where person_type = 'external' order by last_name asc "

            m_DataSet = ExecuteQuery(query)
            RecipientInfo.grdRecipientInfo.DataSource = m_DataSet.Tables(0)
        Catch err As Exception
            Console.WriteLine(err.ToString())
            Throw New Exception("Error searching for recipient")
        End Try

    End Sub

    ''' <summary>
    ''' Get container information from TRT based on the matrix tube barcode
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetTRTContainerInfo()
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            'New Query point to SMF_SHIPPING.T_SMF_V_CONTAINER_QUEUE
            ' limit result to 500 for faster performance
            'query = "SELECT request_name, barcode, location_name, location_responsible, sample_name, request_date_created, " & _
            '        "delivery_from_type_name, location_responsible_email from t_smf_v_container_queue where rownum <= 1000"
            query = "SELECT request_name, barcode, location_name, location_responsible, sample_name, request_date_created, " & _
                "delivery_form_type_name, location_responsible_email from v_container_details where rownum <= 500"

            m_DataSet = ExecuteQuery(query)
            frmMain.grdPendingItems.DataSource = m_DataSet.Tables(0)
        Catch
            Throw New Exception("Error getting TRT container information")
        End Try

    End Sub



#Region "           CONTACT                 "
    Public Sub AddContact(ByVal Company As String, ByVal Department As String, ByVal LastName As String, ByVal FirstName As String, ByVal Address As String, ByVal City As String, ByVal Province As String, ByVal PostalCode As String, ByVal Country As String, ByVal Phone As String, ByVal Email As String)
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction
        sw.Restart()
        m_Trans = AppConnectionObjects.m_OracleConnection_G.BeginTransaction()

        m_command = New OracleCommand("p_contact.addcontact", AppConnectionObjects.m_OracleConnection_G)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans

        m_command.Parameters.Add("i_company", OracleDbType.Varchar2).Value = Company
        m_command.Parameters.Add("i_department", OracleDbType.Varchar2).Value = Department
        m_command.Parameters.Add("i_last_name", OracleDbType.Varchar2).Value = LastName
        m_command.Parameters.Add("i_first_name", OracleDbType.Varchar2).Value = FirstName
        m_command.Parameters.Add("i_mail_address", OracleDbType.Varchar2).Value = Address
        m_command.Parameters.Add("i_city", OracleDbType.Varchar2).Value = City
        m_command.Parameters.Add("i_province", OracleDbType.Varchar2).Value = Province
        m_command.Parameters.Add("i_postalcode", OracleDbType.Varchar2).Value = PostalCode
        m_command.Parameters.Add("i_country_name", OracleDbType.Varchar2).Value = Country
        m_command.Parameters.Add("i_phone", OracleDbType.Varchar2).Value = Phone
        m_command.Parameters.Add("i_email", OracleDbType.Varchar2).Value = Email
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            Throw New Exception("Error adding contact - " + m_command.Parameters("o_error_msg").Value)
        End If

        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)


    End Sub

    Public Sub UpdateContact(ByVal ContactID As Decimal, ByVal Company As String, ByVal Department As String, ByVal LastName As String, ByVal FirstName As String, ByVal Address As String, ByVal City As String, ByVal Province As String, ByVal PostalCode As String, ByVal Country As String, ByVal Phone As String, ByVal Email As String)
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction
        sw.Restart()
        m_Trans = AppConnectionObjects.m_OracleConnection_G.BeginTransaction()

        m_command = New OracleCommand("p_contact.updatecontact", AppConnectionObjects.m_OracleConnection_G)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_contact_id", OracleDbType.Decimal).Value = ContactID.ToString("F0")
        m_command.Parameters.Add("i_company", OracleDbType.Varchar2).Value = Company
        m_command.Parameters.Add("i_department", OracleDbType.Varchar2).Value = Department
        m_command.Parameters.Add("i_last_name", OracleDbType.Varchar2).Value = LastName
        m_command.Parameters.Add("i_first_name", OracleDbType.Varchar2).Value = FirstName
        m_command.Parameters.Add("i_mail_address", OracleDbType.Varchar2).Value = Address
        m_command.Parameters.Add("i_city", OracleDbType.Varchar2).Value = City
        m_command.Parameters.Add("i_province", OracleDbType.Varchar2).Value = Province
        m_command.Parameters.Add("i_postalcode", OracleDbType.Varchar2).Value = PostalCode
        m_command.Parameters.Add("i_country_name", OracleDbType.Varchar2).Value = Country
        m_command.Parameters.Add("i_phone", OracleDbType.Varchar2).Value = Phone
        m_command.Parameters.Add("i_email", OracleDbType.Varchar2).Value = Email
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            Throw New Exception("Error updating contact - " + m_command.Parameters("o_error_msg").Value)
        End If

        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)

    End Sub

    Public Sub DeleteContact(ByVal ContactID As Decimal)
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction
        sw.Restart()
        m_Trans = AppConnectionObjects.m_OracleConnection_G.BeginTransaction()

        m_command = New OracleCommand("p_contact.deletecontact", AppConnectionObjects.m_OracleConnection_G)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_contact_id", OracleDbType.Decimal).Value = ContactID.ToString("F0")
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            Throw New Exception("Error deleting contact - " + m_command.Parameters("o_error_msg").Value)
        End If

        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper)

    End Sub
#End Region








    Public Function GetSenderID(ByVal Sender521 As String) As Decimal
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select person_id from v_person where account = '" + Sender521.ToUpper() + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting sender ID")
            Return -1
        End Try

    End Function

    Public Function GetRecipientID(ByVal EmailAddress As String) As Decimal

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select person_id from v_person where upper(email) = '" + EmailAddress.ToUpper() + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting recipient ID")
            Return -1
        End Try

    End Function

    Public Function GetRecipientName(ByVal RecipientID As Decimal) As String

        Dim query As String
        Dim m_DataSet As DataSet
        Dim TempName As String

        Try
            query = "select first_name, last_name from v_person where person_id = " + RecipientID

            m_DataSet = ExecuteQuery(query)
            TempName = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0) + " " + m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(1)
            Return TempName
        Catch err As Exception
            Throw New Exception("Error getting recipient name")
            Return Nothing
        End Try

    End Function

    Public Function GetRecipientCity(ByVal RecipientID As Decimal) As String

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select city from v_person where person_id = " + RecipientID

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting recipient city")
            Return Nothing
        End Try
        sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + "Time: " + sw.Elapsed.ToString(), ConfigurationManager.AppSettings.Get("Environment").ToUpper + " Query:" + query)

    End Function

    ''' <summary>
    ''' Get the supported container list from SMF_SHIPPING
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetContainerType()
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select unique(type_type_note) from v_type_container order by type_type_note"

            m_DataSet = ExecuteQuery(query)
            frmMain.lkPackageContent.Properties.DataSource = m_DataSet.Tables(0)
            frmMain.lkPackageContent.Properties.DisplayMember = "TYPE_TYPE_NOTE"
            frmMain.lkPackageContent.Properties.ValueMember = "TYPE_TYPE_NOTE"
        Catch
            Throw New Exception("Error getting container type list")
        End Try
    End Sub

    Public Function GetContainerTypeID(ByVal ContainerType As String) As Decimal

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            'query = "select type_id from v_type_container where type_name = '" + ContainerType + "'"
            query = "select type_type_id from v_type_container where type_type_note = '" + ContainerType + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting container type ID")
            Return -1
        End Try

    End Function

    Public Function GetContainerID(ByVal ContainerBarcode As String) As Decimal

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select container_id from v_container_details where barcode = '" + ContainerBarcode + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting container type ID")
            Return -1
        End Try

    End Function

    ''' <summary>
    ''' Get the supported origin code from SMF_SHIPPING
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetOriginCode(ByVal ContainerType As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            'query = "select origin_code from smf_core_origin"
            query = "select type_name from v_type_container where type_type_note = '" + ContainerType + "' order by type_id"

            m_DataSet = ExecuteQuery(query)
            frmMain.lkPackageOrigin.Properties.DataSource = m_DataSet.Tables(0)
            frmMain.lkPackageOrigin.Properties.DisplayMember = "TYPE_NAME"
            frmMain.lkPackageOrigin.Properties.ValueMember = "TYPE_NAME"
        Catch
            Throw New Exception("Error getting origin code")
        End Try

    End Sub

    ''' <summary>
    ''' Get the supported origin type ID from SMF_SHIPPING
    ''' </summary>
    ''' <remarks></remarks>
    Public Function GetOriginTypeID(ByVal ContainerType As String) As Integer
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select type_id from v_type_container where type_name = '" + ContainerType + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting Origin Type ID")
            Return -1
        End Try

    End Function




    Public Function GetPriorityID(ByVal Priority As String) As Decimal

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select priority_id from v_priority where priority_name = '" + Priority + "'"

            m_DataSet = ExecuteQuery(query)

            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting priority ID")
            Return -1
        End Try

    End Function

    ''' <summary>
    ''' Get the shipment type from SMF_SHIPPING
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetShipmentType()
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select type_name from v_type_shipment"

            m_DataSet = ExecuteQuery(query)
            frmMain.lkShipmentType.Properties.DataSource = m_DataSet.Tables(0)
            frmMain.lkShipmentType.Properties.DisplayMember = "TYPE_NAME"
            frmMain.lkShipmentType.Properties.ValueMember = "TYPE_NAME"
            frmMain.lkShipmentType.ItemIndex = 0
        Catch
            Throw New Exception("Error getting shipment type list")
        End Try
    End Sub

    Public Function GetShipmentTypeID(ByVal ShipmentType As String) As Decimal

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select type_id from v_type_shipment where type_name = '" + ShipmentType + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting shipment type ID")
            Return -1
        End Try

    End Function

    ''' <summary>
    ''' Get the filling material from SMF_SHIPPING
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetFillingMaterial()
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select filling_material_name from v_filling_material"

            m_DataSet = ExecuteQuery(query)
            frmMain.lkFillingMaterial.Properties.DataSource = m_DataSet.Tables(0)
            frmMain.lkFillingMaterial.Properties.DisplayMember = "FILLING_MATERIAL_NAME"
            frmMain.lkFillingMaterial.Properties.ValueMember = "FILLING_MATERIAL_NAME"
            frmMain.lkFillingMaterial.ItemIndex = 0
        Catch
            Throw New Exception("Error getting filling material list")
        End Try
    End Sub

    Public Function GetFillingMaterialID(ByVal FillingMaterial As String) As Decimal

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select filling_material_id from v_filling_material where filling_material_name = '" + FillingMaterial + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting filling material ID")
            Return -1
        End Try

    End Function

    Public Function GetParentID(ByVal PackageBarcode As String) As Decimal

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select parent_container_id from v_package where barcode = '" + PackageBarcode + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting parent container ID for the container")
            Return -1
        End Try

    End Function

    Public Function GetPackageBarcode(ByVal ContainerID As Decimal) As String

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select barcode from v_package where container_id = " + ContainerID.ToString("F0")

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0).ToString()
        Catch err As Exception
            Throw New Exception("Error getting package barcode for the container")
            Return -1
        End Try

    End Function



    ''' <summary>
    ''' Get external reference type ID from SMF_SHIPPING, this ID refers to the shipping carrier
    ''' </summary>
    ''' <param name="ShippingCarrier"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetExtReferenceTypeID(ByVal ShippingCarrier As String) As Decimal

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select carrier_id from v_carrier where carrier_name = '" + ShippingCarrier + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting external reference ID")
            Return -1
        End Try

    End Function



    Public Function GetTotalPendingItems() As Integer
        'Dim query As String
        'Dim m_DataSet As DataSet

        'query = "select count(*) from v_container"

        'm_DataSet = ExecuteQuery(query)
        'Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Return 0
    End Function

    Public Function GetNumberOfInternationalItems(ByVal CountryName As String) As Integer
        'Dim query As String
        'Dim m_DataSet As DataSet
        Dim m_LocationName As String

        If CountryName = "UNITED STATES" Then
            m_LocationName = "CPH Cambridge"
        ElseIf CountryName = "SWITZERLAND" Then
            m_LocationName = "CPH Basel"
        ElseIf CountryName = "GB" Then
            m_LocationName = "CPH Horsham"
        Else
            m_LocationName = "CPH"
        End If

        'query = "select count(*) from v_container where country_name != '" + m_LocationName + "'"

        'm_DataSet = ExecuteQuery(query)
        'Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)

        Return 0
    End Function

    Public Function GetNumberOfDomesticItems(ByVal CountryName As String) As Integer
        'Dim query As String
        'Dim m_DataSet As DataSet
        Dim m_LocationName As String

        If CountryName = "UNITED STATES" Then
            m_LocationName = "CPH Cambridge"
        ElseIf CountryName = "SWITZERLAND" Then
            m_LocationName = "CPH Basel"
        ElseIf CountryName = "GB" Then
            m_LocationName = "CPH Horsham"
        Else
            m_LocationName = ""
        End If

        'query = "select count(*) from v_container where country_name = '" + m_LocationName + "'"

        'm_DataSet = ExecuteQuery(query)
        'Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Return 0
    End Function

    Public Function GetNumberOfDestinations() As Integer
        'Dim query As String
        'Dim m_DataSet As DataSet

        'query = "select count(unique(country_name)) from v_container"

        'm_DataSet = ExecuteQuery(query)

        ''Return the number of unique destinations in the view
        'Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)

        Return 0

    End Function

    Public Shared Sub GetSites()

        Dim Sites As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))

        Sites.Add(New KeyValuePair(Of String, String)("Basel", "chbs"))
        Sites.Add(New KeyValuePair(Of String, String)("Cambridge", "usca"))
        Sites.Add(New KeyValuePair(Of String, String)("Emeryville", "usem"))
        Sites.Add(New KeyValuePair(Of String, String)("Horsham", "gbho"))
        Sites.Add(New KeyValuePair(Of String, String)("Shanghai", "cnsh"))
        Sites.Add(New KeyValuePair(Of String, String)("Singapore", "sgsg"))

        frmMain.lkSites.Properties.DataSource = Sites
        frmMain.lkSites.Properties.DisplayMember = "Key"
        frmMain.lkSites.Properties.ValueMember = "Value"
        frmMain.lkSites.Properties.PopulateColumns()


        frmMain.lkSites.Properties.Columns("Key").Visible = True
        frmMain.lkSites.Properties.Columns("Value").Visible = False

        If frmMain.lkSites.ItemIndex < 0 Then
            frmMain.lkSites.ItemIndex = 1
        End If

    End Sub

    Public Sub GetTimeframes()

        Dim Timeframes As List(Of KeyValuePair(Of String, Integer)) = New List(Of KeyValuePair(Of String, Integer))

        Timeframes.Add(New KeyValuePair(Of String, Integer)("Today", 1))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last 2 Days", 2))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last 3 Days", 3))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last Week", 7))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last 2 Weeks", 14))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last Month", 30))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last 3 Months", 90))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("All", 0))


        frmMain.lkPendingTimeframe.Properties.DataSource = Timeframes
        frmMain.lkPendingTimeframe.Properties.DisplayMember = "Key"
        frmMain.lkPendingTimeframe.Properties.ValueMember = "Value"
        frmMain.lkPendingTimeframe.Properties.PopulateColumns()


        frmMain.lkPendingTimeframe.Properties.Columns("Key").Visible = True
        frmMain.lkPendingTimeframe.Properties.Columns("Value").Visible = False

        If frmMain.lkPendingTimeframe.ItemIndex < 0 Then
            frmMain.lkPendingTimeframe.ItemIndex = 3
        End If

    End Sub

    Public Sub GetPackageTimeframes()

        Dim Timeframes As List(Of KeyValuePair(Of String, Integer)) = New List(Of KeyValuePair(Of String, Integer))

        Timeframes.Add(New KeyValuePair(Of String, Integer)("Today", 1))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last 3 Days", 3))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last Week", 7))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last 2 Weeks", 14))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last Month", 30))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("Last 3 Months", 90))
        Timeframes.Add(New KeyValuePair(Of String, Integer)("All", 0))


        frmMain.lkFinPkgTimeline.Properties.DataSource = Timeframes
        frmMain.lkFinPkgTimeline.Properties.DisplayMember = "Key"
        frmMain.lkFinPkgTimeline.Properties.ValueMember = "Value"
        frmMain.lkFinPkgTimeline.Properties.PopulateColumns()


        frmMain.lkFinPkgTimeline.Properties.Columns("Key").Visible = True
        frmMain.lkFinPkgTimeline.Properties.Columns("Value").Visible = False

        If frmMain.lkFinPkgTimeline.ItemIndex < 0 Then
            frmMain.lkFinPkgTimeline.ItemIndex = 6
        End If

    End Sub

    Public Sub GetDomains(ByVal Domain As String)


        Dim Domains As List(Of String) = New List(Of String)

        Domains.Add("APNET")
        Domains.Add("EUNET")
        Domains.Add("JPNET")
        Domains.Add("LANET")
        Domains.Add("NANET")
        'Domains.Add("nibr.novartis.net")
        'Domains.Add("novartis.net")

        If Not Domains.Contains(Domain) Then
            Domains.Add(Domain)
        End If

        frmMain.lkLoginDomains.Properties.DataSource = Domains
        'frmMain.lkLoginDomains.Properties.DisplayMember = "Key"
        'frmMain.lkLoginDomains.Properties.ValueMember = "Value"
        'frmMain.lkLoginDomains.Properties.PopulateColumns()


        'frmMain.lkLoginDomains.Properties.Columns("Key").Visible = True
        'frmMain.lkLoginDomains.Properties.Columns("Value").Visible = False

        frmMain.lkLoginDomains.EditValue = Domain

    End Sub

    Public Sub SetSenderInformation(ByVal PersonID As Decimal)
        Dim query As String
        Dim m_DataSet As DataSet

        query = "select first_name, last_name, company, department, mail_address, city, country_name, email, phone, person_id, account from v_person where person_id = " + PersonID.ToString("F0")

        m_DataSet = ExecuteQuery(query)

        'Database Error. Some values are nulls

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)) Then
            m_SenderFirstName = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Else
            m_SenderFirstName = ""
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(1)) Then
            m_SenderLastName = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(1)
        Else
            m_SenderLastName = ""
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(2)) Then
            m_SenderCompany = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(2)
        Else
            m_SenderCompany = ""
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(3)) Then
            m_SenderDepartment = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(3)
        Else
            m_SenderDepartment = ""
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(4)) Then
            m_SenderAddress = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(4)
        Else
            m_SenderAddress = ""
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(5)) Then
            m_SenderCity = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(5)
        Else
            m_SenderCity = ""
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(6)) Then
            m_SenderCountry = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(6)
        Else
            m_SenderCountry = ""
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(7)) Then
            m_SenderEmail = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(7)
        Else
            m_SenderEmail = ""
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(8)) Then
            m_SenderPhone = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(8)
        Else
            m_SenderPhone = ""
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(9)) Then
            m_SenderID = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(9)
        Else
            m_SenderID = ""
        End If


        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(10)) Then
            m_SenderAccountNumber = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(10)
        Else
            m_SenderAccountNumber = ""
        End If
    End Sub

    Public Sub SetReceiverInformation(ByVal PersonID As Decimal)
        Dim query As String
        Dim m_DataSet As DataSet

        query = "select first_name, last_name, company, department, mail_address, city, country_name, email, phone from v_person where person_id = " + PersonID.ToString("F0")

        m_DataSet = ExecuteQuery(query)

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)) Then
            m_ReceiverFirstName = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Else
            m_ReceiverFirstName = String.Empty
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(1)) Then
            m_ReceiverLastName = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(1)
        Else
            m_ReceiverLastName = String.Empty
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(2)) Then
            m_ReceiverCompany = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(2)
        Else
            m_ReceiverCompany = String.Empty
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(3)) Then
            m_ReceiverDepartment = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(3)
        Else
            m_ReceiverDepartment = String.Empty
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(4)) Then
            m_ReceiverAddress = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(4)
        Else
            m_ReceiverAddress = String.Empty
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(5)) Then
            m_ReceiverCity = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(5)
        Else
            m_ReceiverCity = String.Empty
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(6)) Then
            m_ReceiverCountry = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(6)
        Else
            m_ReceiverCountry = String.Empty
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(7)) Then
            m_ReceiverEmail = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(7)
        Else
            m_ReceiverEmail = String.Empty
        End If

        If Not IsDBNull(m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(8)) Then
            m_ReceiverPhone = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(8)
        Else
            m_ReceiverPhone = String.Empty
        End If
    End Sub

End Class
