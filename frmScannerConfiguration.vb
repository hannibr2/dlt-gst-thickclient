﻿'*********************************************************************************
' Company:          Novartis - NIBR IT
' Copyright:        Copyright © 2013
' Author:           Erik Hesse 
' Date:             01/10/2013
'
' Project:          IntelliScan       
' Application:      InstrumentNotificationTool
' File:             frmScannerConfiguration.vb
'
' Comment:         
'
' Purpose:          This form shows the scanner configuration document.
'
'
' History:        
'
' Date:        Ver:    Description:
'
'*********************************************************************************

Imports System.IO
Imports Common

Public Class frmScannerConfiguration

    Private m_strPath As String

    ''' <summary>
    ''' btnClose_Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

        Me.Close()

    End Sub

    ''' <summary>
    ''' frmReleaseNotes_Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmReleaseNotes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        m_strPath = Path.Combine(System.Windows.Forms.Application.StartupPath, "Resources\GSTTemplates\Documents\ScannerConfiguration.rtf")

        ' Load the relase notes word document
        Me.rtbNotes.LoadFile(m_strPath)

    End Sub

    ''' <summary>
    ''' btnExport_Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click

        Dim dlg As New SaveFileDialog()

        Try
            ' Get the file name from the user
            dlg.Filter = "Rich Text File|*.rtf|All Files|*.*"
            dlg.FileName = "ScannerConfiguration.rtf"

            ' Open the SaveFile dialog
            If dlg.ShowDialog() = DialogResult.OK Then
                If dlg.FileName.Length > 0 Then
                    File.Copy(m_strPath, dlg.FileName)
                End If
            End If
        Catch ex As Exception
            ' Write the exception to the exception log
            'Dim exception As New CException(Me.GetType().Name + ":RegisterRack not successful: " + ex.Message, ex)

        End Try

    End Sub

End Class