﻿Public Class Shipment

#Region "           Declarations        "


    Private m_shipment_id As Decimal
    Private m_shipment_name As String = String.Empty

    Private m_first_name As String = String.Empty
    Private m_last_name As String = String.Empty
    Private m_company As String = String.Empty
    Private m_state As String = String.Empty
    Private m_city As String = String.Empty
    Private m_country_name As String = String.Empty
    Private m_postalcode As String = String.Empty
    Private m_mail_address As String = String.Empty
    Private m_email As String = String.Empty
    Private m_phone As String = String.Empty
    Private m_department As String = String.Empty

    Private m_package_count As Integer
    Private m_container_count As Integer

    Private m_sender_first_name As String = String.Empty
    Private m_sender_last_name As String = String.Empty
    Private m_sender_company As String = String.Empty
    Private m_sender_department As String = String.Empty
    Private m_sender_city As String = String.Empty
    Private m_sender_state As String = String.Empty
    Private m_sender_country_name As String = String.Empty
    Private m_sender_postalcode As String = String.Empty
    Private m_sender_mail_address As String = String.Empty
    Private m_sender_email As String = String.Empty
    Private m_sender_phone As String = String.Empty

    Private m_cost_center As String = String.Empty

    Private m_courier As String = String.Empty
    Private m_courier_account As String = String.Empty
    Private m_external_reference_type_id As Integer
    Private m_external_reference As String = String.Empty

    Private m_priority_id As Integer
    Private m_priority As String




    Private m_packages As Packages
    Private m_containers As Containers

    Private m_shipmentcontents As ShipmentContents

    Private m_solution_container_count As Integer = 0
    Private m_powder_container_count As Integer = 0
    Private m_containers_counted As Boolean = False






#End Region

#Region "       Properties              "


    Public Property ShipmentID() As Decimal
        Get
            Return m_shipment_id
        End Get
        Set(ByVal value As Decimal)
            If m_shipment_id = value Then
                Return
            End If
            m_shipment_id = value
        End Set
    End Property

    Public Property ShipmentName() As String
        Get
            Return m_shipment_name
        End Get
        Set(ByVal value As String)
            If m_shipment_name = value Then
                Return
            End If
            m_shipment_name = value
        End Set
    End Property


    Public Property ReceiverFirstName() As String
        Get
            Return m_first_name
        End Get
        Set(ByVal value As String)
            If m_first_name = value Then
                Return
            End If
            m_first_name = value
        End Set
    End Property

    Public Property ReceiverLastName() As String
        Get
            Return m_last_name
        End Get
        Set(ByVal value As String)
            If m_last_name = value Then
                Return
            End If
            m_last_name = value
        End Set
    End Property

    Public Property ReceiverCompany() As String
        Get
            Return m_company
        End Get
        Set(ByVal value As String)
            If m_company = value Then
                Return
            End If
            m_company = value
        End Set
    End Property

    Public Property ReceiverDepartment As String
        Get
            Return m_department
        End Get
        Set(value As String)
            If m_department = value Then
                Return
            End If
            m_department = value
        End Set
    End Property

    Public Property ReceiverCity As String
        Get
            Return m_city
        End Get
        Set(value As String)
            If m_city = value Then
                Return
            End If
            m_city = value
        End Set
    End Property

    Public Property ReceiverState As String
        Get
            Return m_state
        End Get
        Set(value As String)
            If m_state = value Then
                Return
            End If
            m_state = value
        End Set
    End Property

    Public Property ReceiverCountryName() As String
        Get
            Return m_country_name
        End Get
        Set(ByVal value As String)
            If m_country_name = value Then
                Return
            End If
            m_country_name = value
        End Set
    End Property

    Public Property ReceiverPostalcode As String
        Get
            Return m_postalcode
        End Get
        Set(value As String)
            If m_postalcode = value Then
                Return
            End If
            m_postalcode = value
        End Set
    End Property

    Public Property ReceiverMailAddress() As String
        Get
            Return m_mail_address
        End Get
        Set(ByVal value As String)
            If m_mail_address = value Then
                Return
            End If
            m_mail_address = value
        End Set
    End Property

    Public Property ReceiverEmail As String
        Get
            Return m_email
        End Get
        Set(value As String)
            If m_email = value Then
                Return
            End If
            m_email = value
        End Set
    End Property

    Public Property ReceiverPhone As String
        Get
            Return m_phone
        End Get
        Set(value As String)
            If m_phone = value Then
                Return
            End If
            m_phone = value
        End Set
    End Property


    Public Property PackageCount() As Integer
        Get
            Return m_package_count
        End Get
        Set(ByVal value As Integer)
            If m_package_count = value Then
                Return
            End If
            m_package_count = value
        End Set
    End Property
    Public Property ContainerCount() As Integer
        Get
            Return m_container_count
        End Get
        Set(ByVal value As Integer)
            If m_container_count = value Then
                Return
            End If
            m_container_count = value
        End Set
    End Property



    Public Property SenderFirstName As String
        Get
            Return m_sender_first_name
        End Get
        Set(value As String)
            If m_sender_first_name = value Then
                Return
            End If
            m_sender_first_name = value
        End Set
    End Property

    Public Property SenderLastName As String
        Get
            Return m_sender_last_name
        End Get
        Set(value As String)
            If m_sender_last_name = value Then
                Return
            End If
            m_sender_last_name = value
        End Set
    End Property

    Public Property SenderCompany As String
        Get
            Return m_sender_company
        End Get
        Set(value As String)
            If m_sender_company = value Then
                Return
            End If
            m_sender_company = value
        End Set
    End Property

    Public Property SenderDepartment As String
        Get
            Return m_sender_department
        End Get
        Set(value As String)
            If m_sender_department = value Then
                Return
            End If
            m_sender_department = value
        End Set
    End Property

    Public Property SenderCity As String
        Get
            Return m_sender_city
        End Get
        Set(value As String)
            If m_sender_city = value Then
                Return
            End If
            m_sender_city = value
        End Set
    End Property

    Public Property SenderState As String
        Get
            Return m_sender_state
        End Get
        Set(value As String)
            If m_sender_state = value Then
                Return
            End If
            m_sender_state = value
        End Set
    End Property

    Public Property SenderCountryName As String
        Get
            Return m_sender_country_name
        End Get
        Set(value As String)
            If m_sender_country_name = value Then
                Return
            End If
            m_sender_country_name = value
        End Set
    End Property

    Public Property SenderPostalcode As String
        Get
            Return m_sender_postalcode
        End Get
        Set(value As String)
            If m_sender_postalcode = value Then
                Return
            End If
            m_sender_postalcode = value
        End Set
    End Property

    Public Property SenderMailAddress As String
        Get
            Return m_sender_mail_address
        End Get
        Set(value As String)
            If m_sender_mail_address = value Then
                Return
            End If
            m_sender_mail_address = value
        End Set
    End Property

    Public Property SenderEmail As String
        Get
            Return m_sender_email
        End Get
        Set(value As String)
            If m_sender_email = value Then
                Return
            End If
            m_sender_email = value
        End Set
    End Property
    Public Property SenderPhone As String
        Get
            Return m_sender_phone
        End Get
        Set(value As String)
            If m_sender_phone = value Then
                Return
            End If
            m_sender_phone = value
        End Set
    End Property

    Public Property ExternalReferenceTypeID As Integer
        Get
            Return m_external_reference_type_id
        End Get
        Set(value As Integer)
            If m_external_reference_type_id = value Then
                Return
            End If
            m_external_reference_type_id = value
        End Set
    End Property

    Public Property ExternalReference As String
        Get
            Return m_external_reference
        End Get
        Set(value As String)
            If m_external_reference = value Then
                Return
            End If
            m_external_reference = value
        End Set
    End Property

    Public Property PriorityID As Integer
        Get
            Return m_priority_id
        End Get
        Set(value As Integer)
            If m_priority_id = value Then
                Return
            End If
            m_priority_id = value
        End Set
    End Property

    Public Property Priority As String
        Get
            Return m_priority
        End Get
        Set(value As String)
            If m_priority = value Then
                Return
            End If
            m_priority = value
        End Set
    End Property

    Public Property CostCenter As String
        Get
            Return m_cost_center
        End Get
        Set(value As String)
            If m_cost_center = value Then
                Return
            End If
            m_cost_center = value
        End Set
    End Property

    Public Property Courier As String
        Get
            Return m_courier
        End Get
        Set(value As String)
            If m_courier = value Then
                Return
            End If
            m_courier = value
        End Set
    End Property
    Public Property CourierAccount As String
        Get
            Return m_courier_account
        End Get
        Set(value As String)
            If m_courier_account = value Then
                Return
            End If
            m_courier_account = value
        End Set
    End Property

    Public ReadOnly Property Packages() As Packages
        Get
            Dim PackageDAL As New Oracle_Package
            If m_packages Is Nothing Then
                m_packages = New Packages(PackageDAL.GetPackagesByShipmentID(ShipmentID))
            End If
            Return m_packages
        End Get
    End Property

    Public ReadOnly Property Containers() As Containers
        Get
            If m_containers Is Nothing Then
                m_containers = New Containers()
                For Each TempPackage As Package In Packages
                    If TempPackage.Containers.Count > 0 Then
                        m_containers.AddRange(TempPackage.Containers)
                    End If
                Next
            End If
            Return m_containers
        End Get
    End Property

    Public ReadOnly Property ShipmentContents() As ShipmentContents
        Get
            Dim ShipmentDAL As New Oracle_Shipment
            If m_shipmentcontents Is Nothing Then
                m_shipmentcontents = New ShipmentContents(ShipmentDAL.GetShipmentContentsByShipmentName(ShipmentName))
            End If
            Return m_shipmentcontents
        End Get
    End Property

    Public ReadOnly Property HasAttachments As Boolean
        Get
            Return Me.ShipmentContents.HasAttachments
        End Get
    End Property

    Public ReadOnly Property SolutionContainerCount() As Integer
        Get
            Return Me.ShipmentContents.SolutionCount
        End Get
    End Property

    Public ReadOnly Property PowderContainerCount() As Integer
        Get
            Return Me.ShipmentContents.PowderCount
        End Get
    End Property

    Public ReadOnly Property PlateContainerCount() As Integer
        Get
            Return Me.ShipmentContents.PlateCount
        End Get
    End Property
#End Region

    Private Sub SetContainerTypeCounts()



        For Each CurrentPackage As Package In Me.Packages
            m_solution_container_count = m_solution_container_count + CurrentPackage.SolutionContainerCount
            m_powder_container_count = m_powder_container_count + CurrentPackage.PowderContainerCount
        Next
        m_containers_counted = True
    End Sub

    Public Sub New()

    End Sub

    Public Sub New(ByVal ShipmentRow As DataRow)
        ShipmentID = ShipmentRow.Item("shipment_id")
        ShipmentName = ShipmentRow.Item("shipment_name").ToString()
        ReceiverFirstName = ShipmentRow.Item("first_name").ToString()
        ReceiverLastName = ShipmentRow.Item("last_name").ToString()
        ReceiverCompany = ShipmentRow.Item("company").ToString()
        ReceiverDepartment = ShipmentRow.Item("department").ToString()
        ReceiverCountryName = ShipmentRow.Item("country_name").ToString()
        ReceiverMailAddress = ShipmentRow.Item("mail_address").ToString()
        ReceiverCity = ShipmentRow.Item("city").ToString()
        ReceiverState = ShipmentRow.Item("state").ToString()
        ReceiverPhone = ShipmentRow.Item("phone").ToString()
        ReceiverEmail = ShipmentRow.Item("email").ToString()
        ReceiverPostalcode = ShipmentRow.Item("postalcode").ToString()
        ReceiverDepartment = ShipmentRow.Item("department").ToString()

        PackageCount = ShipmentRow.Item("package_count")
        ContainerCount = ShipmentRow.Item("container_count")
        SenderFirstName = ShipmentRow.Item("sender_first_name").ToString()
        SenderLastName = ShipmentRow.Item("sender_last_name").ToString()
        SenderCompany = ShipmentRow.Item("sender_company").ToString()
        SenderDepartment = ShipmentRow.Item("sender_department").ToString()
        SenderCountryName = ShipmentRow.Item("sender_country_name").ToString()
        SenderMailAddress = ShipmentRow.Item("sender_mail_address").ToString()
        SenderCity = ShipmentRow.Item("sender_city").ToString()
        SenderState = ShipmentRow.Item("sender_state").ToString()
        SenderPhone = ShipmentRow.Item("sender_phone").ToString()
        SenderEmail = ShipmentRow.Item("sender_email").ToString()
        SenderPostalcode = ShipmentRow.Item("sender_postalcode").ToString()

        ExternalReferenceTypeID = ShipmentRow.Item("external_reference_type_id")
        ExternalReference = ShipmentRow.Item("external_reference").ToString()


        Courier = ShipmentRow.Item("external_reference_type_name").ToString()

        Dim TempCourierAccount As String = String.Empty
        Dim TempCostCenter As String = String.Empty

        If Configuration.CostCenter IsNot Nothing Then
            TempCostCenter = Configuration.CostCenter
        End If


        If String.Compare(Courier, "FEDEX", True) = 0 Then
            If Configuration.FedExAccount IsNot Nothing Then
                TempCourierAccount = Configuration.FedExAccount
            End If
        ElseIf String.Compare(Courier, "QUICK INTERNATIONAL", True) Then
            If Configuration.QuickIntlAccount IsNot Nothing Then
                TempCourierAccount = Configuration.QuickIntlAccount
            End If
        End If

        CostCenter = TempCostCenter
        CourierAccount = TempCourierAccount

        PriorityID = ShipmentRow.Item("priority_id")
        Priority = ShipmentRow.Item("priority").ToString()

    End Sub
End Class
