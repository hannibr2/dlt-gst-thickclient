Imports System.Web.Services.Protocols
Imports System.Net
Imports System.Xml
Imports System.Text
Imports System.IO

Public Class QuickIntlTracking

    Public Function GetResponse() As List(Of QuickIntlResponse)
        Dim QuickIntlResponses As List(Of QuickIntlResponse) = New List(Of QuickIntlResponse)
        Dim request As HttpWebRequest = HttpWebRequest.Create("http://www.quickonline.com/cgi-bin/WebObjects/XMLProcessor.woa/wa")
        request.AllowAutoRedirect = False
        request.Method = "POST"
        request.ContentType = "application/x-www-form-urlencoded"

        Dim RequestXML As XmlDocument = CreateTrackRequest()

        Dim RequestStream As Stream = request.GetRequestStream()
        Dim SomeBytes() As Byte = Encoding.UTF8.GetBytes(RequestXML.OuterXml)

        RequestStream.Write(SomeBytes, 0, SomeBytes.Length)
        RequestStream.Close()

        Dim result As String = String.Empty

        Dim ResponseXML As XmlDocument = New XmlDocument()
        Using ResponseStream As Stream = request.GetResponse.GetResponseStream()
            Dim settings As XmlReaderSettings = New XmlReaderSettings() With {.ProhibitDtd = False}

            Using ResponseReader As XmlReader = XmlReader.Create(ResponseStream, settings)
                ResponseXML.Load(ResponseReader)
            End Using
        End Using

        QuickIntlResponses = ParseResponse(ResponseXML)

        Return QuickIntlResponses
    End Function

    Private Function CreateTrackRequest() As XmlDocument
        Dim TempDoc As XmlDocument = New XmlDocument

        TempDoc.AppendChild(TempDoc.CreateXmlDeclaration("1.0", "utf-8", "yes"))

        Dim TempDocType As XmlDocumentType = TempDoc.CreateDocumentType("TransactionRequest", Nothing, "http://www.quickonline.com/QuickOnline/XML/TransactionRequest.dtd", Nothing)
        TempDoc.AppendChild(TempDocType)

        Dim TempRequest As XmlNode = TempDoc.CreateElement("TransactionRequest")
        Dim TempRequestAttribute As XmlAttribute = TempDoc.CreateAttribute("TransactionType")
        TempRequestAttribute.Value = "ShipmentStatus"

        TempRequest.Attributes.Append(TempRequestAttribute)

        '<Version>1.0</Version>
        Dim TempNode As XmlNode = TempDoc.CreateElement("Version")
        TempNode.InnerText = "1.0"
        TempRequest.AppendChild(TempNode)

        '<Test/>
        TempNode = TempDoc.CreateElement("Test")
        TempRequest.AppendChild(TempNode)


        '<TransactionReference></TransactionReference>
        TempNode = TempDoc.CreateElement("TransactionReference")
        TempRequest.AppendChild(TempNode)

        '<QOLUserID>NOVARTIS_XML</QOLUserID>
        TempNode = TempDoc.CreateElement("QOLUserID")
        TempNode.InnerText = "NOVARTIS_XML"
        TempRequest.AppendChild(TempNode)

        '<QOLPassword>NHN526MGD</QOLPassword>
        TempNode = TempDoc.CreateElement("QOLPassword")
        TempNode.InnerText = "XXX"
        TempRequest.AppendChild(TempNode)

        '<ShipmentTrack>		
        '	<ProvideDetail/>
        '   <ProvideCharges/>
        '   <DisplayFinishedJobs/>                		
        '</ShipmentTrack>
        TempNode = TempDoc.CreateElement("ShipmentTrack")


        'TempNode.AppendChild(TempDoc.CreateElement("ProvideDetail"))
        'TempNode.AppendChild(TempDoc.CreateElement("ProvideCharges"))
        TempNode.AppendChild(TempDoc.CreateElement("ProvideStatusHistory"))
        'TempNode.AppendChild(TempDoc.CreateElement("DisplayFinishedJobs"))


        TempRequest.AppendChild(TempNode)

        TempDoc.AppendChild(TempRequest)
        Return TempDoc

    End Function

    Private Function ParseResponse(ByVal Response As XmlDocument) As List(Of QuickIntlResponse)

        'Dim navigator As XPath.XPathNavigator = Response.CreateNavigator()
        Dim Responses As New List(Of QuickIntlResponse)

        'Dim Shipments As XmlNodeList = Response.SelectNodes("TransactionResponse/Shipments/Shipment")

        For Each CurrentShipment As XmlNode In Response.SelectNodes("TransactionResponse/Shipments/Shipment")
            Dim CurrentResponse As New QuickIntlResponse

            CurrentResponse.GSTShipmentName = CurrentShipment.SelectSingleNode("ShipmentBOLNumber").InnerText
            CurrentResponse.TrackingCode = CurrentShipment.SelectSingleNode("ShipmentJobNumber").InnerText

            Dim StatusNavigator As XPath.XPathNavigator = CurrentShipment.CreateNavigator()

            For Each CurrentShipmentStatus As XmlNode In CurrentShipment.SelectNodes("StatusHistoryEvents")

                Dim StatusList As XmlNodeList = CurrentShipmentStatus.SelectNodes("HistoryEvent/EventDescription")

                For Each CurrentNode As XmlNode In StatusList

                    Select Case CurrentNode.InnerText
                        Case "Delivered"
                            CurrentResponse.IsDelivered = True
                        Case "Pick Up Confirmed"
                            CurrentResponse.IsPickedUp = True
                        Case "New Shipment"
                            CurrentResponse.IsScheduled = True
                        Case Else

                    End Select
                Next
            Next

            Responses.Add(CurrentResponse)
        Next
        Return Responses
    End Function
End Class
