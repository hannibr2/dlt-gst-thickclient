﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMainRibbon
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.rpLogin = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.rpgLogin = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.rpPackage = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.rpShipments = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.rpSettings = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.rpSampleTransfer = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.BtnRequestsNewASMRequest = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.ExpandCollapseItem.Name = ""
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.BarButtonItem1, Me.BarButtonItem2})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 7
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.rpLogin, Me.rpPackage, Me.rpShipments, Me.rpSettings, Me.rpSampleTransfer})
        Me.RibbonControl.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.[True]
        Me.RibbonControl.Size = New System.Drawing.Size(963, 144)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "BarButtonItem1"
        Me.BarButtonItem1.Id = 4
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Create New Package"
        Me.BarButtonItem2.Id = 6
        Me.BarButtonItem2.ImageIndex = 0
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'rpLogin
        '
        Me.rpLogin.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.rpgLogin})
        Me.rpLogin.Name = "rpLogin"
        Me.rpLogin.Text = "Login"
        '
        'rpgLogin
        '
        Me.rpgLogin.Name = "rpgLogin"
        Me.rpgLogin.Text = "Login"
        '
        'rpPackage
        '
        Me.rpPackage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup2})
        Me.rpPackage.Name = "rpPackage"
        Me.rpPackage.Text = "Package"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BarButtonItem2)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "RibbonPageGroup2"
        '
        'rpShipments
        '
        Me.rpShipments.Name = "rpShipments"
        Me.rpShipments.Text = "Shipments"
        '
        'rpSettings
        '
        Me.rpSettings.Name = "rpSettings"
        Me.rpSettings.Text = "Settings"
        '
        'rpSampleTransfer
        '
        Me.rpSampleTransfer.Name = "rpSampleTransfer"
        Me.rpSampleTransfer.Text = "Sample Transfer"
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 652)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(963, 31)
        '
        'BtnRequestsNewASMRequest
        '
        Me.BtnRequestsNewASMRequest.Caption = "Create New Request"
        Me.BtnRequestsNewASMRequest.Id = 64
        Me.BtnRequestsNewASMRequest.ImageIndex = 0
        Me.BtnRequestsNewASMRequest.LargeImageIndex = 0
        Me.BtnRequestsNewASMRequest.Name = "BtnRequestsNewASMRequest"
        '
        'frmMainRibbon
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(963, 683)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.Name = "frmMainRibbon"
        Me.Ribbon = Me.RibbonControl
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "RibbonForm1"
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents rpLogin As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents rpgLogin As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents rpPackage As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents rpShipments As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents rpSettings As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents rpSampleTransfer As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BtnRequestsNewASMRequest As DevExpress.XtraBars.BarButtonItem


End Class
