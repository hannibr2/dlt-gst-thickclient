Imports System.Collections.ObjectModel


Public Class ShipmentContents
    Inherits Collection(Of ShipmentContent)

    Private m_iOtherCount As Integer = 0
    Private m_iPlateCount As Integer = 0
    Private m_iPowderCount As Integer = 0
    Private m_iSolutionCount As Integer = 0
    Private m_bContainerTypeCount As Boolean = False

    Public ReadOnly Property PlateCount() As Integer
        Get
            If Not m_bContainerTypeCount Then
                SetContainerTypeCounts()
            End If
            Return m_iPlateCount
        End Get
    End Property

    Public ReadOnly Property PowderCount() As Integer
        Get
            If Not m_bContainerTypeCount Then
                SetContainerTypeCounts()
            End If
            Return m_iPowderCount
        End Get
    End Property

    Public ReadOnly Property SolutionCount() As Integer
        Get
            If Not m_bContainerTypeCount Then
                SetContainerTypeCounts()
            End If
            Return m_iSolutionCount
        End Get
    End Property

    Private Sub SetContainerTypeCounts()
        Dim CurrentContainer As ShipmentContent
        For Each CurrentContainer In Me
            If String.Compare(CurrentContainer.DeliveryFormTypeName, "Solution", True) = 0 Then
                m_iSolutionCount = m_iSolutionCount + 1
            ElseIf String.Compare(CurrentContainer.DeliveryFormTypeName, "Powder", True) = 0 Then
                m_iPowderCount = m_iPowderCount + 1
            ElseIf String.Compare(CurrentContainer.DeliveryFormTypeName, "Plate", True) = 0 Then
                m_iPowderCount = m_iPlateCount + 1
            Else
                m_iOtherCount = m_iOtherCount + 1
            End If
        Next
        m_bContainerTypeCount = True
    End Sub

    Public ReadOnly Property HasAttachments() As Boolean
        Get
            HasAttachments = False

            For Each CurrentItem As ShipmentContent In Me
                If CurrentItem.HasAttachments Then
                    HasAttachments = True
                    Exit For
                End If
            Next
            Return HasAttachments
        End Get
    End Property

    Public Sub New(ByVal ShipmentContents As DataTable)
        For Each Row As DataRow In ShipmentContents.Rows
            Me.Add(New ShipmentContent(Row))
        Next
    End Sub

    Public Sub New()

    End Sub

    Public Sub New(ByVal list As IList(Of ShipmentContent))
        MyBase.New(list)
    End Sub

End Class
