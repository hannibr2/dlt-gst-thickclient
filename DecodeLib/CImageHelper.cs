﻿//*********************************************************************************
// Company:         Novartis NITAS
// Copyright:       Copyright © 2013
// Author:          Erik Hesse
// Date:            04/02/2013
//
// Project:         Decode Library       
// Application:     DecodeLib
// File:            CDecodeLib.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
// Date:        Vers:   Description:
// 05-30-2013   1.0.1   Changed the CropImage functions to us the using statements.
//  
//*********************************************************************************
using System;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Microsoft.Win32;

namespace DecodeLib
{
	public class CImageHelper
	{
		private static string m_strSettingsFile;
		public string SettingsFile
		{
			set { m_strSettingsFile = value; }
			get { return m_strSettingsFile; }
		}

		/// <summary>
		/// CImageHelper constructor
		/// </summary>
		public CImageHelper()
		{
			m_strSettingsFile = "";
		}

		/// <summary>
		/// CropImage
		/// </summary>
		/// <param name="strSettingsXML"></param>
		/// <param name="strImageFile"></param>
		public static string CropImage(string strSettingsXML, string strImageFile)
		{
			m_strSettingsFile = strSettingsXML;

			return CropImage( strImageFile );
		}

		/// <summary>
		/// CropImage
		/// </summary>
		/// <param name="strImageFile"></param>
		public static string CropImage(string strImageFile)
		{
			String[] info = new String[4];

            //// Open and parse the settings XML file
            //XmlDocument docSetting = new XmlDocument();
            //docSetting.Load( m_strSettingsFile );

            //// Get the coordinates to define the crop rectangle
            //XmlNodeList ndList = docSetting.SelectSingleNode( "//Coordinates" ).ChildNodes;
            //foreach( XmlNode node in ndList )
            //{
            //    info = node.InnerText.Split( ',' );
            //}

            info = GetCropSettingsFromRegistry().Split(',');

			// Crop the image
			return CropImage( info, strImageFile );
		}

        /// <summary>
        ///  GetCropSettingsFromRegistry - Retrieve the crop settings from the machine registry.
        /// </summary>
        /// <returns></returns>
        public static string GetCropSettingsFromRegistry()
        {
                return (Registry.GetValue("HKEY_CURRENT_USER\\software\\classes\\Intelliscan", "Crop", null).ToString());
        }

		/// <summary>
		/// CropImage
		/// </summary>
		/// <param name="strImageFile"></param>
		/// <param name="baImage"></param>
        //public static byte[] CropImage( string strSettingsXML, byte[] baImage )
        //{
        //    String[] info = new String[4];

        //    // Open and parse the settings XML file
        //    XmlDocument docSetting = new XmlDocument();
        //    docSetting.Load( strSettingsXML );

        //    // Get the coordinates to define the crop rectangle
        //    XmlNodeList ndList = docSetting.SelectSingleNode( "//Coordinates" ).ChildNodes;
        //    foreach( XmlNode node in ndList )
        //    {
        //        info = node.InnerText.Split( ',' );
        //    }

        //    // Crop the image
        //    return CropImage( info, baImage );
        //}

		/// <summary>
		/// CropImage
		///		-	Crop the image depending on the Rectangle coordinates, width and Height.
		/// </summary>
		/// <param name="reqInfo"></param>
		/// <param name="imgPath"></param>
		/// <param name="iCount"></param>
		private static string CropImage( String[] reqInfo, String imgPath )
		{
            string strExt = string.Empty;
            string strFileName = string.Empty;

            // Create the a rectangle to use for the crop area
			Rectangle rect = new Rectangle( Convert.ToInt16( reqInfo[0] ), Convert.ToInt16( reqInfo[1] ), Convert.ToInt16( reqInfo[2] ), Convert.ToInt16( reqInfo[3] ) );

            // Create a Bitmap object of the full image
            using ( Bitmap bmpImage = new Bitmap( imgPath ) )
            {
                // Crop the image with the new rectangle
                using ( Bitmap bmpCrop = bmpImage.Clone( rect, bmpImage.PixelFormat ) )
                {
                    // Save cropped image in the same folder with a new name
                    strExt = Path.GetExtension( imgPath );
                    strFileName = imgPath.Remove( imgPath.Length - strExt.Length );
                    strFileName += "_cropped" + strExt;

                    // Save as jpeg
                    var encoderParameters = new EncoderParameters( 1 );
                    encoderParameters.Param[0] = new EncoderParameter( System.Drawing.Imaging.Encoder.Quality, 100L );
                    bmpCrop.Save( strFileName, GetEncoder( ImageFormat.Jpeg ), encoderParameters );
                }
            }

			// Return the cropped file name
            return strFileName;
		}

		/// <summary>
		/// CropImage
		/// </summary>
		/// <param name="reqInfo"></param>
		/// <param name="baImageArray"></param>
		/// <returns></returns>
		public static byte[] CropImage(byte[] baImageArray )
		{
            byte[] byteArray = new byte[0];
            String[] reqInfo = GetCropSettingsFromRegistry().Split(',');
            // Create the a rectangle to use for the crop area
			Rectangle rect = new Rectangle( Convert.ToInt16( reqInfo[0] ), Convert.ToInt16( reqInfo[1] ), Convert.ToInt16( reqInfo[2] ), Convert.ToInt16( reqInfo[3] ) );
            
            // Create a Bitmap object of the full image
            using ( Bitmap bmpImage = new Bitmap( Bitmap.FromStream( new MemoryStream( baImageArray ) ) ) )
            {
                // Crop the image with the new rectangle
                using ( Bitmap bmpCrop = bmpImage.Clone( rect, bmpImage.PixelFormat ) )
                {
                    // Convert the image to a byte array
                    byteArray =  ImageToByte( (Image)bmpCrop );
                }
            }

            return byteArray;
		}

		/// <summary>
		/// GetEncoder
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		private static ImageCodecInfo GetEncoder( ImageFormat format )
		{
			ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
			return codecs.Single( codec => codec.FormatID == format.Guid );
		}

		/// <summary>
		/// ImageToByte
		/// </summary>
		/// <param name="img"></param>
		/// <returns></returns>
		public static byte[] ImageToByte( Image img )
		{
			byte[] byteArray = new byte[0];
			using( MemoryStream stream = new MemoryStream() )
			{
				img.Save( stream, System.Drawing.Imaging.ImageFormat.Jpeg );
				stream.Close();

				byteArray = stream.ToArray();
			}
			return byteArray;
		}
	}
}
