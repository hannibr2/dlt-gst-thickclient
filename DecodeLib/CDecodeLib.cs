﻿//*********************************************************************************
// Company:         Novartis NITAS
// Copyright:       Copyright © 2013
// Author:          Erik Hesse
// Date:            04/02/2013
//
// Project:         Decode Library       
// Application:     DecodeLib
// File:            CDecodeLib.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
// Date:        Vers:   Description:
// 05-17-2013   3.1.2   Added production DecodeService endpoint and renamed the
//                      current service to DecodeServiceTEST. Also added a compiler
//                      to build with specific endpoints.
// 05-21-2013   3.1.3   The Barcode Decoding Service was modified.
//
//*********************************************************************************
using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Web;

#if Debug
using DecodeLib.DecodeServiceTEST;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
#elif Test || Int
using DecodeLib.DecodeServiceTEST;
using System.Diagnostics;
#else
using DecodeLib.DecodeServicePROD;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
#endif



namespace DecodeLib
{
	public class CDecodeLib
	{
        
        Stopwatch sw = new Stopwatch();

		public int InitDegreeRotaion { set; get; }
		public int StepRotation { set; get; }
		public int NumberOfAttemps { set; get; }
		public int TotalTubes { set; get; }
		public byte[] FileByteArray { set; get; }
		public string ImageFile { set; get; }
		public List<string> Output { set; get; }
        public List<string> ErrorOutput { set; get; }
        public List<string> FailedImages { set; get; }
        public TimeSpan DecodingBenchmark { get; set; }
        public ImageObject imageObject { get; set; }
        public int TubesFound { get; set; }
        public int RacksFound { get; set; }
        public class DecodeReturn
        {
		    public List<string> Output;
            public List<string> ErrorOutput;
            public List<string> FailedImages;
            public ImageObject imageObject;
        }

        public List<DecodeReturn> g_DecodeReturnList = new List<DecodeReturn>();

        public List<ImageObject> g_CdecodeLibImageObject = new List<ImageObject>();
        public List<string> g_OutputBarcodeList = new List<string>();
        DecodeReturn decoeReturn = new DecodeReturn();
        public List<string> g_FileNameList = new List<string>();

        /// <summary>
		/// CDecodeLib constructor
		/// </summary>
		public CDecodeLib()
		{
			this.InitDegreeRotaion = 13;
			this.StepRotation = 1;
			this.NumberOfAttemps = 1;
			this.TotalTubes = 96;
			this.ImageFile = "";
            this.DecodingBenchmark = new TimeSpan();
		}

        public static Byte[] MakeGrayscale(Byte[] imageData)
        {

            var ms = new MemoryStream(imageData);
            System.Drawing.Image original = Image.FromStream(ms);

            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            //create the grayscale ColorMatrix
            ColorMatrix colorMatrix = new ColorMatrix(
               new float[][] 
          {
             new float[] {.3f, .3f, .3f, 0, 0},
             new float[] {.59f, .59f, .59f, 0, 0},
             new float[] {.11f, .11f, .11f, 0, 0},
             new float[] {0, 0, 0, 1, 0},
             new float[] {0, 0, 0, 0, 1}
          });

            //create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            //set the color matrix attribute
            attributes.SetColorMatrix(colorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();
            return ImageToByte(newBitmap);
        }

        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        public Byte[] SetImageQuality(Byte[] inputBytes, int jpegQuality)
        {

            Image image;
            Byte[] outputBytes;
            using (var inputStream = new MemoryStream(inputBytes))
            {
                image = Image.FromStream(inputStream);
                ImageCodecInfo jpegEncoder = GetEncoderInfo("image/jpeg");
                var encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, jpegQuality);
              
                using (var outputStream = new MemoryStream())
                {
  
                    image.Save(outputStream, jpegEncoder, encoderParameters);
                    outputBytes = outputStream.ToArray();
                }
            }
            return outputBytes;
        }
        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }
		/// <summary>
		/// DecodeImage
		/// </summary>
		/// <param name="strImageFile"></param>
		/// <param name="iInitDegreeRotation"></param>
		/// <param name="iStepRotation"></param>
		/// <param name="iNumberOfAttempts"></param>
		/// <param name="iTotalTubes"></param>
		/// <returns></returns>
        public List<string> DecodeImage(string strImageFile, int iInitDegreeRotation, int iStepRotation, int iNumberOfAttempts, int iTotalTubes, ImageObject imageObject)
		{
			// Set the decode options
			this.ImageFile = strImageFile;
			this.InitDegreeRotaion = iInitDegreeRotation;
			this.StepRotation = iStepRotation;
			this.NumberOfAttemps = iNumberOfAttempts;
			this.TotalTubes = iTotalTubes;

			// Convert the file to a byte array
			this.ImageFile = strImageFile;
			this.FileByteArray = ConvertToBtyeArray( this.ImageFile );

			// Decode the image
			DecodeImage(imageObject, false);

			// Return the output list
			return this.Output;
		}

		/// <summary>
		/// DecodeImage
		/// </summary>
		/// <param name="strImageFile"></param>
        public List<DecodeReturn> DecodeImage(string strImageFile, ImageObject imageObject)
		{
			// Set the image file path
			this.ImageFile = strImageFile;

			// Concert the file to a byte array
			this.FileByteArray = ConvertToBtyeArray( this.ImageFile );
            List<ImageObject> returnList = new List<ImageObject>();
            List<DecodeReturn> decodedList = new List<DecodeReturn>();

            returnList = DecodeImage(imageObject, false);
            foreach (var item in returnList)
            {
               
                DecodeReturn decodeReturn = new DecodeReturn();
                decodeReturn.imageObject = item;
                decodedList.Add(decodeReturn);
            }

            return decodedList;

		}

		/// <summary>
		/// DecodeImage
		/// </summary>
		/// <param name="strImageFile"></param>
        public List<DecodeReturn> DecodeImage(byte[] baImage, ImageObject imageObject)
		{
			// Set the file byte array
			this.FileByteArray = baImage;

            List<ImageObject> returnList = new List<ImageObject>();
            List<DecodeReturn> decodedList = new List<DecodeReturn>();

            returnList = DecodeImage(imageObject, false);
            foreach (var item in returnList)
            {

                DecodeReturn decodeReturn = new DecodeReturn();
                decodeReturn.imageObject = item;
                decodedList.Add(decodeReturn);
            }

            return decodedList;
		}


        public ImageObject GetContextHandle()
        {
            DecodeServiceClient client = new DecodeServiceClient();
            ImageObject obj = client.GetDecodeContext(new ImageObject());
            return obj;
        }
        
        
        /// <summary>
        /// DecodeImage
        /// </summary>
        /// 
		public List<ImageObject> DecodeImage(ImageObject imageObject, bool MultipleRacks)
		{
            try
            {

                Stopwatch sw = new Stopwatch();

                sw.Start();

                DecodeServiceClient client = new DecodeServiceClient();

                if (imageObject != null)
                {
                    if (imageObject.StopDecoding)
                    {
                        client.CancelDecoding(imageObject);
                        return new List<ImageObject>();
                    }
                }

                ImageObject imageRequest = new ImageObject();
				// The service needs to be modified to remove this variable if not used
				imageRequest.ServerPath = "C:\\DecoderOutput\\";

                string dnsName = System.Net.Dns.GetHostName();
                string userName  = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

				// Set the decode options
                if (imageObject != null)
                    imageRequest.User_Name = imageObject.User_Name;
                else
                    imageRequest.User_Name = dnsName;
               
                imageRequest.Machine_Name = dnsName;

                imageRequest.ScannerDPI = Convert.ToInt16(ConfigurationManager.AppSettings["DPI"]);
                imageRequest.ScannerBrightness = Convert.ToInt16(ConfigurationManager.AppSettings["Scanner_Brightness"]);
                imageRequest.ScannerContrast = Convert.ToInt16(ConfigurationManager.AppSettings["Scanner_Contrast"]);
                imageRequest.DecodeBrighness = Convert.ToInt16(ConfigurationManager.AppSettings["Brightness"]);
                imageRequest.DecodeContrast = Convert.ToInt16(ConfigurationManager.AppSettings["Contrast"]);
                imageRequest.TimeOut = Convert.ToInt16(ConfigurationManager.AppSettings["Timeout"]);
                imageRequest.CropValue = Convert.ToInt16(ConfigurationManager.AppSettings["CropValue"]);
                imageRequest.StepRotation_1 = Convert.ToInt16(ConfigurationManager.AppSettings["StepRotation_1"]);
                imageRequest.StepRotation_2 = Convert.ToInt16(ConfigurationManager.AppSettings["StepRotation_2"]);
                imageRequest.EmptyThresholdHigh = Convert.ToInt16(ConfigurationManager.AppSettings["EmptyThresholdHigh"]);
                imageRequest.EmptyThresholdLow = Convert.ToInt16(ConfigurationManager.AppSettings["EmptyThresholdLow"]);
                imageRequest.MaxFinalRotation = Convert.ToInt16(ConfigurationManager.AppSettings["MaxFinalRotation"]);
                imageRequest.DecodeSharpen = (float)Convert.ToDouble(ConfigurationManager.AppSettings["SharpenThreshold"]);

				imageRequest.TotalTubes = this.TotalTubes;
               
				// Set the file byte array
				if( this.FileByteArray == null )
					throw new DecodeException( "Image file not specified" );
                imageRequest.ImagesByteArray = SetImageQuality(this.FileByteArray, Convert.ToInt16(ConfigurationManager.AppSettings["ImageQuality"]));
                
				// Decode the image
                client.InnerChannel.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt16(ConfigurationManager.AppSettings["Timeout"]) + 3);
                
                if (imageObject == null)
                {
                    imageObject = client.GetDecodeContext(new ImageObject());
                }

                imageRequest.decoderContext = imageObject.decoderContext;

                ImageObject imageRequestRack = new ImageObject();

                List<ImageObject> imageObjectList = new List<ImageObject>();
              
                if (MultipleRacks)
                {
                    imageRequestRack = client.GetRackCount(imageRequest);

                    RacksFound = imageRequestRack.RacksFound;

                    imageRequest.ImagesByteArray = imageRequestRack.ImagesByteArray3;

                    for (int imageIndex = 1; imageIndex <= 3; imageIndex++)
                    {

                        imageRequest = new ImageObject();

                        switch (imageIndex)
                        {
                            case 1:
                                imageRequest.ImagesByteArray = imageRequestRack.ImagesByteArray1;
                                imageRequest.OutputList = imageRequestRack.BarcodeListList[0];
                                imageRequest.TubesFound = imageRequestRack.TubeCountList[0];
                                imageRequest.Failedimages = imageRequestRack.FailedImageListList[0];
                                imageRequest.ErrorList = imageRequestRack.ErrorListList[0];
                                imageRequest.CropValue = 1;
                                break;
                            case 2:
                                imageRequest.ImagesByteArray = imageRequestRack.ImagesByteArray2;
                                imageRequest.OutputList = imageRequestRack.BarcodeListList[1];
                                imageRequest.TubesFound = imageRequestRack.TubeCountList[1];
                                imageRequest.Failedimages = imageRequestRack.FailedImageListList[1];
                                imageRequest.ErrorList = imageRequestRack.ErrorListList[1];
                                imageRequest.CropValue = 2;
                                break;
                            case 3:
                                imageRequest.ImagesByteArray = imageRequestRack.ImagesByteArray3;
                                imageRequest.OutputList = imageRequestRack.BarcodeListList[2];
                                imageRequest.TubesFound = imageRequestRack.TubeCountList[2];
                                imageRequest.Failedimages = imageRequestRack.FailedImageListList[2];
                                imageRequest.ErrorList = imageRequestRack.ErrorListList[2];
                                imageRequest.CropValue = 3;
                                break;
                            default:
                                break;
                        }

                        if (imageRequest.ImagesByteArray.Length == 0)
                            imageObjectList.Add(null);
                        else
                            imageObjectList.Add(imageRequest);
                    }
                }
                else
                {
                    ImageObject imageResp = client.DecodeBarcodes(imageRequest);
                    imageObjectList.Add(imageResp);
                    imageObjectList.Add(null);
                    imageObjectList.Add(null);
                }
                sw.Stop();
                if(MultipleRacks)
                {
                    try
                    {
                        //ExceptionsHandlerLib.MailServiceProd.MailService mailService = new ExceptionsHandlerLib.MailServiceProd.MailService();
                        //ExceptionsHandlerLib.MailServiceProd.MailType mailType = new ExceptionsHandlerLib.MailServiceProd.MailType();
                        //mailType.Subject = "Client Side MultiRack Scan";
                        //mailType.To = ConfigurationManager.AppSettings["UnhandledExceptionManager/EmailTo"];
                        //mailType.From = ConfigurationManager.AppSettings["UnhandledExceptionManager/EmailTo"];
                        //mailType.SecurityToken = ConfigurationManager.AppSettings["UnhandledExceptionManager/Token"];
                        //mailType.Body = "Total Client Side Time: [" + sw.Elapsed.ToString() + "] Number of Racks: [" + RacksFound.ToString() + "] DNS: [" + dnsName + "] User: [" + userName +"] ";
                        //mailService.SendMail(mailType);
                    }
                    catch { }
                }
                sw.Reset();
                return imageObjectList;
            }

            catch( Exception eX )
            {
                throw new DecodeException( "Error calling Decoding Service." 
                    + "\nMessage: " + eX.Message 
                    + "\nSource: " + eX.Source, eX );
            }
		}
		/// <summary>
		/// FormattedOutput
		///		-	Format the barcodes into a comma separated list of single quoted strings
		/// </summary>
		/// <returns></returns>
       
		public List<string> FormattedOutput()
		{
			
            List<string> BarcodeList = new List<string>();

            for (int rackIndex = 0; rackIndex < 3; rackIndex++)
            {
                string strBarcodes = "";
                string[] strBarcode;
                string[] barcodeOutput;
                if (g_CdecodeLibImageObject[rackIndex] != null)
                {
                    barcodeOutput = g_CdecodeLibImageObject[rackIndex].OutputList;

                    for (int i = 1; i <= this.TotalTubes; i++)
                    {
                        strBarcode = barcodeOutput[i].Split(':');
                        strBarcodes += strBarcode[1] + ",";
                    }
                    strBarcodes.Replace("Error", "");
                    BarcodeList.Add(strBarcodes.Remove(strBarcodes.Length - 1, 1));
                }
                else
                {
                    BarcodeList.Add(null);
                }
            }

            return BarcodeList;
		}

		/// <summary>
		/// ConvertToBtyeArray
		/// </summary>
		/// <param name="strFile"></param>
		/// <returns></returns>
		private byte[] ConvertToBtyeArray(string strFile)
		{
			try
			{
				// Convert the file to a byte array
				return File.ReadAllBytes( strFile );
			}
			catch( Exception eX )
			{
				throw new DecodeException( this.GetType().Name + ":Could not convert image file to byte array.", eX );
			}
		}
	}
}
