﻿//*********************************************************************************
// Company:         Novartis NITAS
// Copyright:       Copyright © 2013
// Author:          Erik Hesse
// Date:            04/02/2013
//
// Project:         Decode Library       
// Application:     DecodeLib
// File:            CDecodeLib.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
// Date:    Vers:   Description:
//   
//*********************************************************************************
using System;

namespace DecodeLib
{
	public class DecodeException : System.Exception
	{
		/// <summary>
		/// DecodeException constructor
		/// </summary>
		public DecodeException(string strMessage) : base(strMessage)
		{
		}

		/// <summary>
        /// CException constructor
		/// </summary>
		/// <param name="strMessage">Message string</param>
		/// <param name="objInner">Exception object</param>
		public DecodeException( string strMessage, Exception objInner )
            : base(strMessage, objInner)
		{
            // Create the message string
            strMessage += "\nSource:\n\t" + objInner.Source.ToString()
                        + "\nStack Trace:\n" + objInner.StackTrace.ToString();
		}
	}
}
