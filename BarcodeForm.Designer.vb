<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BarcodeForm
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.grdBarcodeResult1 = New DevExpress.XtraGrid.GridControl()
        Me.GrdViewBarcodeResult1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.BarcodeRowToolTip = New DevExpress.Utils.ToolTipController(Me.components)
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton()
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.btnRescan = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.lblTubeCount = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.lblTubeValid = New DevExpress.XtraEditors.LabelControl()
        Me.lblTubesInvalid = New DevExpress.XtraEditors.LabelControl()
        Me.lblTubesNotFound = New DevExpress.XtraEditors.LabelControl()
        CType(Me.grdBarcodeResult1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewBarcodeResult1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdBarcodeResult1
        '
        Me.grdBarcodeResult1.Location = New System.Drawing.Point(0, 54)
        Me.grdBarcodeResult1.MainView = Me.GrdViewBarcodeResult1
        Me.grdBarcodeResult1.Name = "grdBarcodeResult1"
        Me.grdBarcodeResult1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit2})
        Me.grdBarcodeResult1.Size = New System.Drawing.Size(905, 242)
        Me.grdBarcodeResult1.TabIndex = 23
        Me.grdBarcodeResult1.ToolTipController = Me.BarcodeRowToolTip
        Me.grdBarcodeResult1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GrdViewBarcodeResult1})
        '
        'GrdViewBarcodeResult1
        '
        Me.GrdViewBarcodeResult1.GridControl = Me.grdBarcodeResult1
        Me.GrdViewBarcodeResult1.Name = "GrdViewBarcodeResult1"
        Me.GrdViewBarcodeResult1.OptionsBehavior.Editable = False
        Me.GrdViewBarcodeResult1.OptionsCustomization.AllowFilter = False
        Me.GrdViewBarcodeResult1.OptionsCustomization.AllowGroup = False
        Me.GrdViewBarcodeResult1.OptionsCustomization.AllowSort = False
        Me.GrdViewBarcodeResult1.OptionsPrint.PrintDetails = True
        Me.GrdViewBarcodeResult1.OptionsView.ShowGroupPanel = False
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'BarcodeRowToolTip
        '
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(798, 314)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(107, 26)
        Me.btnOK.TabIndex = 25
        Me.btnOK.Text = "OK"
        '
        'RibbonControl
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.ExpandCollapseItem.Name = ""
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 1
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Size = New System.Drawing.Size(917, 49)
        Me.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden
        '
        'btnRescan
        '
        Me.btnRescan.Location = New System.Drawing.Point(685, 314)
        Me.btnRescan.Name = "btnRescan"
        Me.btnRescan.Size = New System.Drawing.Size(107, 26)
        Me.btnRescan.TabIndex = 27
        Me.btnRescan.Text = "Rescan"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(572, 314)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(107, 26)
        Me.btnCancel.TabIndex = 28
        Me.btnCancel.Text = "Cancel"
        '
        'lblTubeCount
        '
        Me.lblTubeCount.Location = New System.Drawing.Point(12, 302)
        Me.lblTubeCount.Name = "lblTubeCount"
        Me.lblTubeCount.Size = New System.Drawing.Size(79, 13)
        Me.lblTubeCount.TabIndex = 30
        Me.lblTubeCount.Text = "Tubes scanned: "
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.TextEdit4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.TextEdit3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.TextEdit2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.TextEdit1)
        Me.GroupControl1.Location = New System.Drawing.Point(236, 301)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(326, 50)
        Me.GroupControl1.TabIndex = 32
        Me.GroupControl1.Text = "Legend"
        '
        'LabelControl4
        '
        Me.LabelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl4.Location = New System.Drawing.Point(286, 26)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl4.TabIndex = 7
        Me.LabelControl4.Text = "Valid"
        '
        'TextEdit4
        '
        Me.TextEdit4.Enabled = False
        Me.TextEdit4.Location = New System.Drawing.Point(259, 22)
        Me.TextEdit4.MenuManager = Me.RibbonControl
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Green
        Me.TextEdit4.Properties.Appearance.Options.UseBackColor = True
        Me.TextEdit4.Size = New System.Drawing.Size(21, 20)
        Me.TextEdit4.TabIndex = 6
        '
        'LabelControl3
        '
        Me.LabelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl3.Location = New System.Drawing.Point(180, 26)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl3.TabIndex = 5
        Me.LabelControl3.Text = "Base Container"
        '
        'TextEdit3
        '
        Me.TextEdit3.Enabled = False
        Me.TextEdit3.Location = New System.Drawing.Point(153, 22)
        Me.TextEdit3.MenuManager = Me.RibbonControl
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Appearance.BackColor = System.Drawing.Color.BlueViolet
        Me.TextEdit3.Properties.Appearance.Options.UseBackColor = True
        Me.TextEdit3.Size = New System.Drawing.Size(21, 20)
        Me.TextEdit3.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl2.Location = New System.Drawing.Point(115, 26)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Invalid"
        '
        'TextEdit2
        '
        Me.TextEdit2.Enabled = False
        Me.TextEdit2.Location = New System.Drawing.Point(88, 23)
        Me.TextEdit2.MenuManager = Me.RibbonControl
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Yellow
        Me.TextEdit2.Properties.Appearance.Options.UseBackColor = True
        Me.TextEdit2.Size = New System.Drawing.Size(21, 20)
        Me.TextEdit2.TabIndex = 2
        '
        'LabelControl1
        '
        Me.LabelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl1.Location = New System.Drawing.Point(32, 26)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Not Found"
        '
        'TextEdit1
        '
        Me.TextEdit1.Enabled = False
        Me.TextEdit1.Location = New System.Drawing.Point(5, 23)
        Me.TextEdit1.MenuManager = Me.RibbonControl
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Red
        Me.TextEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.TextEdit1.Size = New System.Drawing.Size(21, 20)
        Me.TextEdit1.TabIndex = 0
        '
        'lblTubeValid
        '
        Me.lblTubeValid.Location = New System.Drawing.Point(12, 326)
        Me.lblTubeValid.Name = "lblTubeValid"
        Me.lblTubeValid.Size = New System.Drawing.Size(61, 13)
        Me.lblTubeValid.TabIndex = 34
        Me.lblTubeValid.Text = "Tubes valid: "
        '
        'lblTubesInvalid
        '
        Me.lblTubesInvalid.Location = New System.Drawing.Point(122, 302)
        Me.lblTubesInvalid.Name = "lblTubesInvalid"
        Me.lblTubesInvalid.Size = New System.Drawing.Size(69, 13)
        Me.lblTubesInvalid.TabIndex = 35
        Me.lblTubesInvalid.Text = "Tubes invalid: "
        '
        'lblTubesNotFound
        '
        Me.lblTubesNotFound.Location = New System.Drawing.Point(122, 327)
        Me.lblTubesNotFound.Name = "lblTubesNotFound"
        Me.lblTubesNotFound.Size = New System.Drawing.Size(86, 13)
        Me.lblTubesNotFound.TabIndex = 36
        Me.lblTubesNotFound.Text = "Tubes not found: "
        '
        'BarcodeForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(917, 354)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblTubesNotFound)
        Me.Controls.Add(Me.lblTubesInvalid)
        Me.Controls.Add(Me.lblTubeValid)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.lblTubeCount)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnRescan)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.grdBarcodeResult1)
        Me.Controls.Add(Me.RibbonControl)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "BarcodeForm"
        Me.Ribbon = Me.RibbonControl
        Me.Text = "Tube Barcode"
        CType(Me.grdBarcodeResult1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewBarcodeResult1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents grdBarcodeResult1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GrdViewBarcodeResult1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents btnRescan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblTubeCount As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents BarcodeRowToolTip As DevExpress.Utils.ToolTipController
    Friend WithEvents lblTubeValid As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTubesInvalid As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTubesNotFound As DevExpress.XtraEditors.LabelControl


End Class
