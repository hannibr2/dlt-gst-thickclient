﻿Imports System.Configuration
Imports ExceptionsHandlerLib

Module SingleContainerManagementCalls

    'Dim sw As Stopwatch
    Dim cMessageLog As New CMessageLog()
    Public Function IsValidSingleContainer(ByVal User As String, ByVal Barcode As String) As Boolean
        'sw.Start()

            
        Dim TempIsValid As Boolean = False

        Dim TempContainerRequest As New SMFSingleContainerManagementService.ContainerReq
        Dim TempRequestCaller As New SMFSingleContainerManagementService.Requester
        Dim TempRequestContainers As New List(Of SMFSingleContainerManagementService.Container)
        Dim TempRequestContainer As New SMFSingleContainerManagementService.Container
        Dim TempRequestContainerProperties As New List(Of SMFSingleContainerManagementService.Property)
        Dim TempRequestContainerProperty As New SMFSingleContainerManagementService.Property

        Dim TempContainerResponse As SMFSingleContainerManagementService.ContainerResp

        'Initialize Caller Values
        TempRequestCaller.UserID = User
        TempRequestCaller.ApplicationName = "GST"
        TempRequestCaller.Version = "1.0"
        TempRequestCaller.TestMode = False
        TempRequestCaller.ActionTarget = "GOS Only"

        'Initialize Containers
        TempRequestContainerProperty.Family = "Barcode"
        TempRequestContainerProperty.Value = Barcode

        TempRequestContainerProperties.Add(TempRequestContainerProperty)
        TempRequestContainer.Properties = TempRequestContainerProperties.ToArray()

        TempRequestContainers.Add(TempRequestContainer)

        TempContainerRequest.Caller = TempRequestCaller
        TempContainerRequest.Containers = TempRequestContainers.ToArray()

        Using client As New SMFSingleContainerManagementService.SingleContainerManagementClient
            client.ClientCredentials.UserName.UserName = SingleContainerCredentials.User
            client.ClientCredentials.UserName.Password = SingleContainerCredentials.Password

            TempContainerResponse = client.GetContainer(TempContainerRequest)

            Dim TempSampleFound As Boolean = False

            If Not TempContainerResponse.Callee Is Nothing AndAlso Not TempContainerResponse.Callee.HasError Then
                If Not TempContainerResponse.Containers Is Nothing Then
                    For Each TempResponseContainer As SMFSingleContainerManagementService.Container In TempContainerResponse.Containers

                        If Not TempResponseContainer.Properties Is Nothing Then
                            For Each TempProperty As SMFSingleContainerManagementService.Property In TempResponseContainer.Properties
                                If String.Compare(TempProperty.Family, "Sample", True) = 0 Then
                                    If Not String.IsNullOrEmpty(TempProperty.Value) Then
                                        TempIsValid = True
                                    Else
                                        TempIsValid = False
                                    End If

                                    TempSampleFound = True
                                    Exit For
                                End If
                            Next

                        Else
                            TempIsValid = False
                        End If

                        If TempSampleFound Then
                            Exit For
                        End If
                    Next
                Else
                    TempIsValid = False
                End If
            Else
                TempIsValid = False
            End If
        End Using
        'sw.Stop()
        cMessageLog.LogBenchMarks(System.Reflection.MethodInfo.GetCurrentMethod().Name.ToString() + " Barcode: " + Barcode, ConfigurationManager.AppSettings.Get("Environment").ToUpper)

        Return TempIsValid
    End Function

End Module
