﻿namespace ScanTest
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lstMessages = new System.Windows.Forms.ListBox();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.btnScanCropAndDecodeIamge = new System.Windows.Forms.Button();
            this.gp1 = new System.Windows.Forms.GroupBox();
            this.chkSetPoint2 = new System.Windows.Forms.CheckBox();
            this.chkSetPoint1 = new System.Windows.Forms.CheckBox();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.txt1x = new System.Windows.Forms.TextBox();
            this.btnReset1 = new System.Windows.Forms.Button();
            this.txt1y = new System.Windows.Forms.TextBox();
            this.txt2x = new System.Windows.Forms.TextBox();
            this.txt2y = new System.Windows.Forms.TextBox();
            this.btnLoadImage = new System.Windows.Forms.Button();
            this.btnUnloadImage = new System.Windows.Forms.Button();
            this.btnCropImage = new System.Windows.Forms.Button();
            this.btnUnloadScanner = new System.Windows.Forms.Button();
            this.btnInitScanner = new System.Windows.Forms.Button();
            this.btnSaveImage = new System.Windows.Forms.Button();
            this.btnGetImage = new System.Windows.Forms.Button();
            this.btnSelectDevice = new System.Windows.Forms.Button();
            this.cmbScanners = new System.Windows.Forms.ComboBox();
            this.btnScan = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxWIA_DPI = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSaveScannerSettings = new System.Windows.Forms.Button();
            this.textBoxWIA_Contrast = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxWIA_Brightness = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDecodeEmptyThresholdHigh = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDecodeEmptyThresholdLow = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDecodeContrast = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDecodeBrightness = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnSaveDecode = new System.Windows.Forms.Button();
            this.txtDecodeTimeOut = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDecodeThirdPassMaxRotation = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnDecode = new System.Windows.Forms.Button();
            this.txtDecodeSecondPassRotationStep = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDecodeFirstPassRotationStep = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDecodeAdaptiveSharpen = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gp1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(13, 166);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lstMessages);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.picImage);
            this.splitContainer1.Size = new System.Drawing.Size(793, 510);
            this.splitContainer1.SplitterDistance = 177;
            this.splitContainer1.TabIndex = 15;
            // 
            // lstMessages
            // 
            this.lstMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstMessages.FormattingEnabled = true;
            this.lstMessages.HorizontalScrollbar = true;
            this.lstMessages.Location = new System.Drawing.Point(0, 0);
            this.lstMessages.Name = "lstMessages";
            this.lstMessages.ScrollAlwaysVisible = true;
            this.lstMessages.Size = new System.Drawing.Size(177, 510);
            this.lstMessages.TabIndex = 9;
            // 
            // picImage
            // 
            this.picImage.Location = new System.Drawing.Point(3, 5);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(593, 502);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picImage.TabIndex = 8;
            this.picImage.TabStop = false;
            this.picImage.DoubleClick += new System.EventHandler(this.picImage_DoubleClick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(794, 163);
            this.tabControl1.TabIndex = 29;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.btnScanCropAndDecodeIamge);
            this.tabPage1.Controls.Add(this.gp1);
            this.tabPage1.Controls.Add(this.btnLoadImage);
            this.tabPage1.Controls.Add(this.btnUnloadImage);
            this.tabPage1.Controls.Add(this.btnCropImage);
            this.tabPage1.Controls.Add(this.btnUnloadScanner);
            this.tabPage1.Controls.Add(this.btnInitScanner);
            this.tabPage1.Controls.Add(this.btnSaveImage);
            this.tabPage1.Controls.Add(this.btnGetImage);
            this.tabPage1.Controls.Add(this.btnSelectDevice);
            this.tabPage1.Controls.Add(this.cmbScanners);
            this.tabPage1.Controls.Add(this.btnScan);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(786, 137);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Crop / Scan Settings";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 13);
            this.label4.TabIndex = 41;
            this.label4.Text = "Select a device driver";
            // 
            // btnScanCropAndDecodeIamge
            // 
            this.btnScanCropAndDecodeIamge.Location = new System.Drawing.Point(170, 108);
            this.btnScanCropAndDecodeIamge.Name = "btnScanCropAndDecodeIamge";
            this.btnScanCropAndDecodeIamge.Size = new System.Drawing.Size(326, 23);
            this.btnScanCropAndDecodeIamge.TabIndex = 40;
            this.btnScanCropAndDecodeIamge.Text = "Scan, Crop and Decode Image";
            this.btnScanCropAndDecodeIamge.UseVisualStyleBackColor = true;
            this.btnScanCropAndDecodeIamge.Click += new System.EventHandler(this.btnScanCropAndDecodeIamge_Click);
            // 
            // gp1
            // 
            this.gp1.Controls.Add(this.chkSetPoint2);
            this.gp1.Controls.Add(this.chkSetPoint1);
            this.gp1.Controls.Add(this.btnSaveSettings);
            this.gp1.Controls.Add(this.lbl2);
            this.gp1.Controls.Add(this.lbl1);
            this.gp1.Controls.Add(this.txt1x);
            this.gp1.Controls.Add(this.btnReset1);
            this.gp1.Controls.Add(this.txt1y);
            this.gp1.Controls.Add(this.txt2x);
            this.gp1.Controls.Add(this.txt2y);
            this.gp1.Location = new System.Drawing.Point(511, 5);
            this.gp1.Name = "gp1";
            this.gp1.Size = new System.Drawing.Size(241, 129);
            this.gp1.TabIndex = 39;
            this.gp1.TabStop = false;
            this.gp1.Text = "Crop Settings";
            // 
            // chkSetPoint2
            // 
            this.chkSetPoint2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkSetPoint2.AutoSize = true;
            this.chkSetPoint2.Location = new System.Drawing.Point(132, 68);
            this.chkSetPoint2.Name = "chkSetPoint2";
            this.chkSetPoint2.Size = new System.Drawing.Size(69, 23);
            this.chkSetPoint2.TabIndex = 28;
            this.chkSetPoint2.Text = "Set Point 2";
            this.chkSetPoint2.UseVisualStyleBackColor = true;
            this.chkSetPoint2.CheckedChanged += new System.EventHandler(this.chkSetPoint2_CheckedChanged);
            // 
            // chkSetPoint1
            // 
            this.chkSetPoint1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkSetPoint1.AutoSize = true;
            this.chkSetPoint1.Location = new System.Drawing.Point(18, 67);
            this.chkSetPoint1.Name = "chkSetPoint1";
            this.chkSetPoint1.Size = new System.Drawing.Size(69, 23);
            this.chkSetPoint1.TabIndex = 27;
            this.chkSetPoint1.Text = "Set Point 1";
            this.chkSetPoint1.UseVisualStyleBackColor = true;
            this.chkSetPoint1.CheckedChanged += new System.EventHandler(this.chkSetPoint1_CheckedChanged);
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Location = new System.Drawing.Point(123, 100);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(95, 23);
            this.btnSaveSettings.TabIndex = 25;
            this.btnSaveSettings.Text = "Save Settings";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Location = new System.Drawing.Point(129, 25);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(40, 13);
            this.lbl2.TabIndex = 23;
            this.lbl2.Text = "Point 2";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(12, 25);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(40, 13);
            this.lbl1.TabIndex = 22;
            this.lbl1.Text = "Point 1";
            // 
            // txt1x
            // 
            this.txt1x.Enabled = false;
            this.txt1x.Location = new System.Drawing.Point(13, 41);
            this.txt1x.Name = "txt1x";
            this.txt1x.Size = new System.Drawing.Size(37, 20);
            this.txt1x.TabIndex = 0;
            // 
            // btnReset1
            // 
            this.btnReset1.Location = new System.Drawing.Point(15, 100);
            this.btnReset1.Name = "btnReset1";
            this.btnReset1.Size = new System.Drawing.Size(75, 23);
            this.btnReset1.TabIndex = 21;
            this.btnReset1.Text = "Reset";
            this.btnReset1.UseVisualStyleBackColor = true;
            this.btnReset1.Click += new System.EventHandler(this.btnReset1_Click);
            // 
            // txt1y
            // 
            this.txt1y.Enabled = false;
            this.txt1y.Location = new System.Drawing.Point(56, 41);
            this.txt1y.Name = "txt1y";
            this.txt1y.Size = new System.Drawing.Size(37, 20);
            this.txt1y.TabIndex = 3;
            // 
            // txt2x
            // 
            this.txt2x.Enabled = false;
            this.txt2x.Location = new System.Drawing.Point(130, 41);
            this.txt2x.Name = "txt2x";
            this.txt2x.Size = new System.Drawing.Size(37, 20);
            this.txt2x.TabIndex = 6;
            // 
            // txt2y
            // 
            this.txt2y.Enabled = false;
            this.txt2y.Location = new System.Drawing.Point(173, 41);
            this.txt2y.Name = "txt2y";
            this.txt2y.Size = new System.Drawing.Size(37, 20);
            this.txt2y.TabIndex = 10;
            // 
            // btnLoadImage
            // 
            this.btnLoadImage.Location = new System.Drawing.Point(405, 10);
            this.btnLoadImage.Name = "btnLoadImage";
            this.btnLoadImage.Size = new System.Drawing.Size(91, 23);
            this.btnLoadImage.TabIndex = 38;
            this.btnLoadImage.Text = "Load Image";
            this.btnLoadImage.UseVisualStyleBackColor = true;
            this.btnLoadImage.Click += new System.EventHandler(this.btnLoadImage_Click);
            // 
            // btnUnloadImage
            // 
            this.btnUnloadImage.Location = new System.Drawing.Point(405, 44);
            this.btnUnloadImage.Name = "btnUnloadImage";
            this.btnUnloadImage.Size = new System.Drawing.Size(91, 23);
            this.btnUnloadImage.TabIndex = 37;
            this.btnUnloadImage.Text = "Unload Image";
            this.btnUnloadImage.UseVisualStyleBackColor = true;
            this.btnUnloadImage.Click += new System.EventHandler(this.btnUnloadImage_Click);
            // 
            // btnCropImage
            // 
            this.btnCropImage.Location = new System.Drawing.Point(405, 79);
            this.btnCropImage.Name = "btnCropImage";
            this.btnCropImage.Size = new System.Drawing.Size(91, 23);
            this.btnCropImage.TabIndex = 36;
            this.btnCropImage.Text = "Crop Image";
            this.btnCropImage.UseVisualStyleBackColor = true;
            this.btnCropImage.Click += new System.EventHandler(this.btnCropImage_Click);
            // 
            // btnUnloadScanner
            // 
            this.btnUnloadScanner.Location = new System.Drawing.Point(27, 80);
            this.btnUnloadScanner.Name = "btnUnloadScanner";
            this.btnUnloadScanner.Size = new System.Drawing.Size(120, 23);
            this.btnUnloadScanner.TabIndex = 35;
            this.btnUnloadScanner.Text = "Unload Scanner";
            this.btnUnloadScanner.UseVisualStyleBackColor = true;
            this.btnUnloadScanner.Click += new System.EventHandler(this.btnUnloadScanner_Click);
            // 
            // btnInitScanner
            // 
            this.btnInitScanner.Location = new System.Drawing.Point(170, 10);
            this.btnInitScanner.Name = "btnInitScanner";
            this.btnInitScanner.Size = new System.Drawing.Size(93, 23);
            this.btnInitScanner.TabIndex = 34;
            this.btnInitScanner.Text = "Init Scanner";
            this.btnInitScanner.UseVisualStyleBackColor = true;
            this.btnInitScanner.Click += new System.EventHandler(this.btnInitScanner_Click);
            // 
            // btnSaveImage
            // 
            this.btnSaveImage.Location = new System.Drawing.Point(295, 79);
            this.btnSaveImage.Name = "btnSaveImage";
            this.btnSaveImage.Size = new System.Drawing.Size(82, 23);
            this.btnSaveImage.TabIndex = 33;
            this.btnSaveImage.Text = "Save Image";
            this.btnSaveImage.UseVisualStyleBackColor = true;
            this.btnSaveImage.Click += new System.EventHandler(this.btnSaveImage_Click);
            // 
            // btnGetImage
            // 
            this.btnGetImage.Location = new System.Drawing.Point(295, 44);
            this.btnGetImage.Name = "btnGetImage";
            this.btnGetImage.Size = new System.Drawing.Size(82, 23);
            this.btnGetImage.TabIndex = 32;
            this.btnGetImage.Text = "Get Image";
            this.btnGetImage.UseVisualStyleBackColor = true;
            this.btnGetImage.Click += new System.EventHandler(this.btnGetImage_Click);
            // 
            // btnSelectDevice
            // 
            this.btnSelectDevice.Location = new System.Drawing.Point(170, 44);
            this.btnSelectDevice.Name = "btnSelectDevice";
            this.btnSelectDevice.Size = new System.Drawing.Size(93, 23);
            this.btnSelectDevice.TabIndex = 31;
            this.btnSelectDevice.Text = "Select Device";
            this.btnSelectDevice.UseVisualStyleBackColor = true;
            this.btnSelectDevice.Click += new System.EventHandler(this.btnSelectDevice_Click);
            // 
            // cmbScanners
            // 
            this.cmbScanners.FormattingEnabled = true;
            this.cmbScanners.Location = new System.Drawing.Point(24, 32);
            this.cmbScanners.Name = "cmbScanners";
            this.cmbScanners.Size = new System.Drawing.Size(121, 21);
            this.cmbScanners.TabIndex = 30;
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(170, 79);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(93, 23);
            this.btnScan.TabIndex = 29;
            this.btnScan.Text = "Scan";
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(786, 137);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Decoder Settings";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.textBoxWIA_DPI);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.btnSaveScannerSettings);
            this.groupBox2.Controls.Add(this.textBoxWIA_Contrast);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textBoxWIA_Brightness);
            this.groupBox2.Location = new System.Drawing.Point(363, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(167, 132);
            this.groupBox2.TabIndex = 43;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "WIA Scanner Settings";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Brightness";
            // 
            // textBoxWIA_DPI
            // 
            this.textBoxWIA_DPI.Location = new System.Drawing.Point(79, 71);
            this.textBoxWIA_DPI.Name = "textBoxWIA_DPI";
            this.textBoxWIA_DPI.Size = new System.Drawing.Size(32, 20);
            this.textBoxWIA_DPI.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "DPI\r\n";
            // 
            // btnSaveScannerSettings
            // 
            this.btnSaveScannerSettings.Location = new System.Drawing.Point(36, 103);
            this.btnSaveScannerSettings.Name = "btnSaveScannerSettings";
            this.btnSaveScannerSettings.Size = new System.Drawing.Size(75, 23);
            this.btnSaveScannerSettings.TabIndex = 15;
            this.btnSaveScannerSettings.Text = "Save";
            this.btnSaveScannerSettings.UseVisualStyleBackColor = true;
            this.btnSaveScannerSettings.Click += new System.EventHandler(this.btnSaveScannerSettings_Click);
            // 
            // textBoxWIA_Contrast
            // 
            this.textBoxWIA_Contrast.Location = new System.Drawing.Point(79, 48);
            this.textBoxWIA_Contrast.Name = "textBoxWIA_Contrast";
            this.textBoxWIA_Contrast.Size = new System.Drawing.Size(32, 20);
            this.textBoxWIA_Contrast.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 39);
            this.label9.TabIndex = 4;
            this.label9.Text = "Contrast %\r\n\r\n\r\n";
            // 
            // textBoxWIA_Brightness
            // 
            this.textBoxWIA_Brightness.Location = new System.Drawing.Point(78, 24);
            this.textBoxWIA_Brightness.Name = "textBoxWIA_Brightness";
            this.textBoxWIA_Brightness.Size = new System.Drawing.Size(33, 20);
            this.textBoxWIA_Brightness.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDecodeEmptyThresholdHigh);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtDecodeEmptyThresholdLow);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtDecodeContrast);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtDecodeBrightness);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.btnSaveDecode);
            this.groupBox1.Controls.Add(this.txtDecodeTimeOut);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtDecodeThirdPassMaxRotation);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnDecode);
            this.groupBox1.Controls.Add(this.txtDecodeSecondPassRotationStep);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtDecodeFirstPassRotationStep);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtDecodeAdaptiveSharpen);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 134);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Decode Settings";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtDecodeEmptyThresholdHigh
            // 
            this.txtDecodeEmptyThresholdHigh.Location = new System.Drawing.Point(303, 109);
            this.txtDecodeEmptyThresholdHigh.Name = "txtDecodeEmptyThresholdHigh";
            this.txtDecodeEmptyThresholdHigh.Size = new System.Drawing.Size(32, 20);
            this.txtDecodeEmptyThresholdHigh.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(174, 111);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Empty Threshold High\r\n";
            // 
            // txtDecodeEmptyThresholdLow
            // 
            this.txtDecodeEmptyThresholdLow.Location = new System.Drawing.Point(303, 84);
            this.txtDecodeEmptyThresholdLow.Name = "txtDecodeEmptyThresholdLow";
            this.txtDecodeEmptyThresholdLow.Size = new System.Drawing.Size(32, 20);
            this.txtDecodeEmptyThresholdLow.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(174, 87);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Empty Threshold Low";
            // 
            // txtDecodeContrast
            // 
            this.txtDecodeContrast.Location = new System.Drawing.Point(303, 60);
            this.txtDecodeContrast.Name = "txtDecodeContrast";
            this.txtDecodeContrast.Size = new System.Drawing.Size(32, 20);
            this.txtDecodeContrast.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(173, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 26);
            this.label7.TabIndex = 23;
            this.label7.Text = "Contrast\r\n\r\n";
            // 
            // txtDecodeBrightness
            // 
            this.txtDecodeBrightness.Location = new System.Drawing.Point(303, 37);
            this.txtDecodeBrightness.Name = "txtDecodeBrightness";
            this.txtDecodeBrightness.Size = new System.Drawing.Size(32, 20);
            this.txtDecodeBrightness.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(174, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 39);
            this.label11.TabIndex = 21;
            this.label11.Text = "Brightness\r\n\r\n\r\n";
            // 
            // btnSaveDecode
            // 
            this.btnSaveDecode.Location = new System.Drawing.Point(92, 107);
            this.btnSaveDecode.Name = "btnSaveDecode";
            this.btnSaveDecode.Size = new System.Drawing.Size(75, 23);
            this.btnSaveDecode.TabIndex = 20;
            this.btnSaveDecode.Text = "Save";
            this.btnSaveDecode.UseVisualStyleBackColor = true;
            this.btnSaveDecode.Click += new System.EventHandler(this.btnSaveDecode_Click);
            // 
            // txtDecodeTimeOut
            // 
            this.txtDecodeTimeOut.Location = new System.Drawing.Point(135, 85);
            this.txtDecodeTimeOut.Name = "txtDecodeTimeOut";
            this.txtDecodeTimeOut.Size = new System.Drawing.Size(32, 20);
            this.txtDecodeTimeOut.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Timeout";
            // 
            // txtDecodeThirdPassMaxRotation
            // 
            this.txtDecodeThirdPassMaxRotation.Location = new System.Drawing.Point(135, 62);
            this.txtDecodeThirdPassMaxRotation.Name = "txtDecodeThirdPassMaxRotation";
            this.txtDecodeThirdPassMaxRotation.Size = new System.Drawing.Size(32, 20);
            this.txtDecodeThirdPassMaxRotation.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Third pass max rotation";
            // 
            // btnDecode
            // 
            this.btnDecode.Location = new System.Drawing.Point(9, 107);
            this.btnDecode.Name = "btnDecode";
            this.btnDecode.Size = new System.Drawing.Size(75, 23);
            this.btnDecode.TabIndex = 15;
            this.btnDecode.Text = "Decode";
            this.btnDecode.UseVisualStyleBackColor = true;
            this.btnDecode.Click += new System.EventHandler(this.btnDecode_Click);
            // 
            // txtDecodeSecondPassRotationStep
            // 
            this.txtDecodeSecondPassRotationStep.Location = new System.Drawing.Point(135, 39);
            this.txtDecodeSecondPassRotationStep.Name = "txtDecodeSecondPassRotationStep";
            this.txtDecodeSecondPassRotationStep.Size = new System.Drawing.Size(32, 20);
            this.txtDecodeSecondPassRotationStep.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "Second pass rotation step\r\n\r\n";
            // 
            // txtDecodeFirstPassRotationStep
            // 
            this.txtDecodeFirstPassRotationStep.Location = new System.Drawing.Point(134, 15);
            this.txtDecodeFirstPassRotationStep.Name = "txtDecodeFirstPassRotationStep";
            this.txtDecodeFirstPassRotationStep.Size = new System.Drawing.Size(33, 20);
            this.txtDecodeFirstPassRotationStep.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "First pass rotation step\r\n";
            // 
            // txtDecodeAdaptiveSharpen
            // 
            this.txtDecodeAdaptiveSharpen.Location = new System.Drawing.Point(302, 14);
            this.txtDecodeAdaptiveSharpen.Name = "txtDecodeAdaptiveSharpen";
            this.txtDecodeAdaptiveSharpen.Size = new System.Drawing.Size(32, 20);
            this.txtDecodeAdaptiveSharpen.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(174, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Adaptive Sharpen";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 688);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Scan Test";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.gp1.ResumeLayout(false);
            this.gp1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox lstMessages;
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnScanCropAndDecodeIamge;
        private System.Windows.Forms.GroupBox gp1;
        private System.Windows.Forms.CheckBox chkSetPoint2;
        private System.Windows.Forms.CheckBox chkSetPoint1;
        private System.Windows.Forms.Button btnSaveSettings;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.TextBox txt1x;
        private System.Windows.Forms.Button btnReset1;
        private System.Windows.Forms.TextBox txt1y;
        private System.Windows.Forms.TextBox txt2x;
        private System.Windows.Forms.TextBox txt2y;
        private System.Windows.Forms.Button btnLoadImage;
        private System.Windows.Forms.Button btnUnloadImage;
        private System.Windows.Forms.Button btnCropImage;
        private System.Windows.Forms.Button btnUnloadScanner;
        private System.Windows.Forms.Button btnInitScanner;
        private System.Windows.Forms.Button btnSaveImage;
        private System.Windows.Forms.Button btnGetImage;
        private System.Windows.Forms.Button btnSelectDevice;
        private System.Windows.Forms.ComboBox cmbScanners;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDecodeEmptyThresholdHigh;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDecodeEmptyThresholdLow;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDecodeContrast;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDecodeBrightness;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnSaveDecode;
        private System.Windows.Forms.TextBox txtDecodeTimeOut;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDecodeThirdPassMaxRotation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnDecode;
        private System.Windows.Forms.TextBox txtDecodeSecondPassRotationStep;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDecodeFirstPassRotationStep;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDecodeAdaptiveSharpen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxWIA_DPI;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSaveScannerSettings;
        private System.Windows.Forms.TextBox textBoxWIA_Contrast;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxWIA_Brightness;
        private System.Windows.Forms.Label label10;
    }
}

