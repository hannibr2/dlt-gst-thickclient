﻿//*********************************************************************************
// Company:         Novartis NIBRIT/Delta
// Copyright:       Copyright © 2013
// Author:          Erik Hesse
// Date:            04/01/2013
//
//
// Project:         IntelliScan
// Application:     ScanTest
// File:            frmMain.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
// Date:        Ver:    Description
//
//************************************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using ScannerLib;
using DecodeLib;
using System.Configuration;
using System.Security.AccessControl;
using System.Security.Principal;
using Microsoft.Win32;

namespace ScanTest
{
	public partial class frmMain : Form
    {
        #region Member Variables
        private CScanner_Base m_objScanner;
		private string strImgPoint = String.Empty;
        private string m_strSettingsFile = String.Empty;
        private string m_strAppPath = String.Empty;
        #endregion

        #region Properties
        public string AppPath
        {
            set { m_strAppPath = value; }
        }
        public string SettingsFile
        {
            set { m_strSettingsFile = value; }
            get { return m_strSettingsFile; }
        }
        #endregion

        /// <summary>
		/// Form1
		/// </summary>
		public frmMain()
		{
			InitializeComponent();
//#if DEBUG
//            m_strSettingsFile = Path.Combine( Application.StartupPath, "Settings.xml" );
//#else
//            m_strSettingsFile = Path.Combine( Application.LocalUserAppDataPath, "Settings.xml" );
//#endif
		}

		/// <summary>
		/// frmMain_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmMain_Load( object sender, EventArgs e )
		{
           

            LoadAppConfig();
			// Populate the scanner dropdown control of scanner types
			foreach( string str in Enum.GetNames( typeof( ScannerTypes ) ) )
			{
				cmbScanners.Items.Add( str );
			}

			// Set for debugging
			cmbScanners.SelectedIndex = 2;
		}

        private void SaveScannerSettings()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //make changes
            config.AppSettings.Settings["Scanner_Brightness"].Value = textBoxWIA_Brightness.Text;
            config.AppSettings.Settings["Scanner_Contrast"].Value = textBoxWIA_Contrast.Text;
            config.AppSettings.Settings["DPI"].Value = textBoxWIA_DPI.Text;

            //save to apply changes
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
        private void SaveDecoderSettings()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //make changes
            config.AppSettings.Settings["TimeOut"].Value = txtDecodeTimeOut.Text;
            config.AppSettings.Settings["Brightness"].Value = txtDecodeBrightness.Text;
            config.AppSettings.Settings["Contrast"].Value = txtDecodeContrast.Text;
            config.AppSettings.Settings["StepRotation_1"].Value = txtDecodeFirstPassRotationStep.Text;
            config.AppSettings.Settings["StepRotation_2"].Value = txtDecodeSecondPassRotationStep.Text;
            config.AppSettings.Settings["MaxFinalRotation"].Value = txtDecodeThirdPassMaxRotation.Text;
            config.AppSettings.Settings["SharpenThreshold"].Value = txtDecodeAdaptiveSharpen.Text;
            config.AppSettings.Settings["EmptyThresholdLow"].Value = txtDecodeEmptyThresholdLow.Text;
            config.AppSettings.Settings["EmptyThresholdHigh"].Value = txtDecodeEmptyThresholdHigh.Text;

            //save to apply changes
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        private void LoadAppConfig()
        {
            txtDecodeBrightness.Text = ConfigurationManager.AppSettings["Brightness"].ToString();
            txtDecodeContrast.Text = ConfigurationManager.AppSettings["Contrast"].ToString();
            txtDecodeTimeOut.Text = ConfigurationManager.AppSettings["TimeOut"].ToString();
            txtDecodeFirstPassRotationStep.Text = ConfigurationManager.AppSettings["StepRotation_1"].ToString();
            txtDecodeSecondPassRotationStep.Text = ConfigurationManager.AppSettings["StepRotation_2"].ToString();
            txtDecodeThirdPassMaxRotation.Text = ConfigurationManager.AppSettings["MaxFinalRotation"].ToString(); 
            txtDecodeAdaptiveSharpen.Text = ConfigurationManager.AppSettings["SharpenThreshold"].ToString();
            txtDecodeEmptyThresholdLow.Text = ConfigurationManager.AppSettings["EmptyThresholdLow"].ToString();
            txtDecodeEmptyThresholdHigh.Text = ConfigurationManager.AppSettings["EmptyThresholdHigh"].ToString();
            textBoxWIA_Brightness.Text = ConfigurationManager.AppSettings["Scanner_Brightness"].ToString();
            textBoxWIA_Contrast.Text = ConfigurationManager.AppSettings["Scanner_Contrast"].ToString();
            textBoxWIA_DPI.Text = ConfigurationManager.AppSettings["DPI"].ToString();
        }

		/// <summary>
		/// WriteMessage
		/// </summary>
		/// <param name="strMsg"></param>
		private void WriteMessage( string strMsg )
		{
			lstMessages.Items.Add( strMsg );
			lstMessages.Refresh();
		}

		/// <summary>
		/// btnInitScanner_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnInitScanner_Click( object sender, EventArgs e )
		{
			InitScanner();
		}

		/// <summary>
		/// InitScanner
		/// </summary>
		private void InitScanner()
		{
			try
			{
				// Instantiate the selected scanner type
				switch( cmbScanners.SelectedIndex )
				{
					case (int)ScannerTypes.Simulator:
						m_objScanner = new CScanner_Simulator();
						WriteMessage( "CScanner_Simulator" );
						break;
					case (int)ScannerTypes.Twain:
						m_objScanner = new CScanner_Twain();
						WriteMessage( "CScanner_Twain" );
						break;
					case (int)ScannerTypes.WIA:
						m_objScanner = new CScanner_WIA();
						WriteMessage( "CScanner_WIA" );
						break;
					default:
						WriteMessage( "No scanner selected" );
						m_objScanner = null;
						break;
				}
				// Turn on events
				m_objScanner.Events = true;

				// Hookup the event
				m_objScanner.evtMessage += new MessageEvent( m_objScanner_evtMessage );

				// Initialize the scanner
				m_objScanner.Init();
			}
			catch( Exception eX )
			{
				System.Windows.Forms.MessageBox.Show( eX.Message );
			}
		}

		/// <summary>
		/// m_objScanner_evtMessage
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="strMsg"></param>
		void m_objScanner_evtMessage( object sender, string strMsg )
		{
			// Write the message to the message window
			WriteMessage( strMsg );
		}

		/// <summary>
		/// btnSelectDevice_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSelectDevice_Click( object sender, EventArgs e )
		{
			try
			{
				// Set the source device
				m_objScanner.SetSource();
			}
			catch( Exception eX )
			{
				System.Windows.Forms.MessageBox.Show( eX.Message );
			}
		}

		/// <summary>
		/// btnScan_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnScan_Click( object sender, EventArgs e )
		{
			try
			{
				// Scan the image
				m_objScanner.Scan();
			}
			catch( Exception eX )
			{
				System.Windows.Forms.MessageBox.Show( eX.Message );
			}
		}

		/// <summary>
		/// btnGetImage_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnGetImage_Click( object sender, EventArgs e )
		{
			// Get the image from the scanner and display on the form
			GetImage();
		}

		/// <summary>
		/// GetImage
		/// </summary>
		private void GetImage()
		{
			try
			{
				// Get the image from the scanner
				byte[] baImage = m_objScanner.GetImage();
				Image img = Image.FromStream( new System.IO.MemoryStream( baImage ) );
				picImage.Image = img;
			}
			catch( Exception eX )
			{
				System.Windows.Forms.MessageBox.Show( eX.Message );
			}
		}

		/// <summary>
		/// btnSaveImage_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSaveImage_Click( object sender, EventArgs e )
		{
			try
			{
				SaveFileDialog dlg = new SaveFileDialog();
				dlg.Filter = "Image Files|*.jpg";

				// Save the image to the specified file
				if( dlg.ShowDialog() == DialogResult.OK )
				{
					byte[] baImage = m_objScanner.GetImage();
					Image i = Image.FromStream( new System.IO.MemoryStream( baImage ) );
					i.Save( dlg.FileName );
				}

				// Write message to the message window
				WriteMessage( this.GetType().Name + ":btnSaveImage_Click" );
			}
			catch( Exception eX )
			{
				System.Windows.Forms.MessageBox.Show( eX.Message );
			}
		}

		/// <summary>
		/// btnUnloadScanner_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnUnloadScanner_Click( object sender, EventArgs e )
		{
			m_objScanner = null;

			WriteMessage( "Scanner unloaded" );
		}
        
        
        
		/// <summary>
		/// btnCropImage_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnCropImage_Click( object sender, EventArgs e )
		{
			OpenFileDialog OpenFileDlg = new OpenFileDialog();
			string strImageFilePath = "";

			// Unload displayed image
			UnloadImage();

            //// Get the settings file
            //OpenFileDlg.Filter = "Settings File|*.xml";
            //OpenFileDlg.InitialDirectory = m_strAppPath;
            //if( OpenFileDlg.ShowDialog() == DialogResult.OK && OpenFileDlg.FileName.Length > 0 )
            //    m_strSettingsFile = OpenFileDlg.FileName;
            //else
            //    return;

            // Get the image file
            OpenFileDlg.InitialDirectory = m_strAppPath;
            OpenFileDlg.Filter = "Image Files|*.jpg";
            if (OpenFileDlg.ShowDialog() == DialogResult.OK && OpenFileDlg.FileName.Length > 0)
                strImageFilePath = OpenFileDlg.FileName;
            else
                return;

            if (GetCropSettingsFromRegistry() == "")
            {
                MessageBox.Show("No crop settings found. Please create and save crop settings.", "Crop Settings Missing.");
                return;
            }

			// Crop the image
			// string strImage = CropImages( m_strSettingsFile, strImageFilePath );
			string strImage = DecodeLib.CImageHelper.CropImage( m_strSettingsFile, strImageFilePath );

			Image img = Image.FromStream( new System.IO.FileStream( strImage, FileMode.Open ) );
			picImage.Image = img;

			// Write message
			WriteMessage( "Image Cropped : [" + strImage + "]" );
		}

		/// <summary>
		/// btnDecode_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnDecode_Click( object sender, EventArgs e )
		{
			// Change the state of the button to show that the decoding is in progress
			btnDecode.Text = "Decoding...";
			btnDecode.Enabled = false;

			// Check if any images are loaded in the picture control and unload it
			UnloadImage();

			// Get the image file
			OpenFileDialog OpenFileDlg = new OpenFileDialog();
            OpenFileDlg.InitialDirectory = m_strAppPath;
			OpenFileDlg.Filter = "Image Files|*.jpg";
			if( OpenFileDlg.ShowDialog() == DialogResult.OK && OpenFileDlg.FileName.Length > 0 )
			{
				// Decode the image
				DecodeImage( OpenFileDlg.FileName );
			}

			// Reset the button state
			btnDecode.Text = "Decode";
			btnDecode.Enabled = true;
		}

		/// <summary>
		/// DecodeImage
		/// </summary>
		/// <param name="strImageFile"></param>
		private void DecodeImage( string strImageFile )
		{
			try
			{
				WriteMessage( "Decoding Image [" + strImageFile + "]..." );

				// Call the service to decode the image
				CDecodeLib objDecode = new CDecodeLib();
				objDecode.InitDegreeRotaion = Convert.ToInt16( txtDecodeAdaptiveSharpen.Text );
				objDecode.StepRotation = Convert.ToInt16( txtDecodeFirstPassRotationStep.Text );
				objDecode.NumberOfAttemps = Convert.ToInt16( txtDecodeSecondPassRotationStep.Text );
				objDecode.TotalTubes = 96;
                objDecode.g_DecodeReturnList = objDecode.DecodeImage(strImageFile, objDecode.imageObject);

                if (objDecode.FailedImages.Count() > 0)
                {
                    string msg = "The follwing barcodes were not decoded" + Environment.NewLine + Environment.NewLine;
                    foreach (string item in objDecode.FailedImages)
                    {
                        msg += item + Environment.NewLine;
                    }

                    MessageBox.Show(msg);
                }

				// Get the decoded barcodes
				List<string> lstOutput = new List<string>( objDecode.Output );

				WriteMessage( "Decoding Done..." );

				// Write the barcodes to the message window
				foreach( string str in lstOutput )
					WriteMessage( str );

			//	WriteMessage( objDecode.FormattedOutput() );
			}
                
			catch( Exception eX )
			{
				string strMessage = "\r\n" + eX.Message
						+ "\r\nSource:\r\n\t" + eX.InnerException.Source.ToString()
						+ "\r\nStack Trace:\r\n" + eX.InnerException.StackTrace.ToString();

				WriteMessage( "Decode failed:" + strMessage );
			}
		}

		/// <summary>
		/// DecodeImage
		/// </summary>
		/// <param name="baImage"></param>
		private void DecodeImage( byte[] baImage, bool CancelRequest )
		{
			try
			{
				WriteMessage( "Decoding Image from in memory object..." );

				// Call the service to decode the image
				CDecodeLib objDecode = new CDecodeLib();
				objDecode.InitDegreeRotaion = Convert.ToInt16( txtDecodeAdaptiveSharpen.Text );
				objDecode.StepRotation = Convert.ToInt16( txtDecodeFirstPassRotationStep.Text );
				objDecode.NumberOfAttemps = Convert.ToInt16( txtDecodeSecondPassRotationStep.Text );
				objDecode.TotalTubes = 96;
			    objDecode.g_DecodeReturnList =objDecode.DecodeImage( baImage, objDecode.imageObject );

               
				// Get the decoded barcodes
				List<string> lstOutput = new List<string>( objDecode.g_DecodeReturnList[0].imageObject.OutputList);

				WriteMessage( "Decoding Done..." );

				// Write the barcodes to the message window
				foreach( string str in lstOutput )
					WriteMessage( str );

				//WriteMessage( objDecode.FormattedOutput() );
			}
			catch( Exception eX )
			{
				string strMessage = "\n" + eX.Message
						+ "\nSource:\n\t" + eX.InnerException.Source.ToString()
						+ "\nStack Trace:\n" + eX.InnerException.StackTrace.ToString();

				WriteMessage( "Decode failed:" + strMessage );
			}
		}

		/// <summary>
		/// btnLoadImage_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnLoadImage_Click( object sender, EventArgs e )
		{
			// Get the image file
			OpenFileDialog OpenFileDlg = new OpenFileDialog();
            OpenFileDlg.InitialDirectory = m_strAppPath;
			OpenFileDlg.Filter = "Image Files|*.jpg";

			if( OpenFileDlg.ShowDialog() == DialogResult.OK && OpenFileDlg.FileName.Length > 0 )
			{
				Image img = Image.FromStream( new System.IO.FileStream( OpenFileDlg.FileName, FileMode.Open ) );
				picImage.Image = img;
			}
		}

		/// <summary>
		/// btnUnloadImage_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnUnloadImage_Click( object sender, EventArgs e )
		{
			UnloadImage();
		}

		/// <summary>
		/// UnloadImage
		/// </summary>
		private void UnloadImage()
		{
			if( picImage.Image != null )
			{
				picImage.Image.Dispose();
				picImage.Image = null;

				GC.Collect();
			}
		}

		/// <summary>
		/// btnScanCropAndDecodeIamge_Click
		///		-	Scan and Get the image from the scanner, crop, then decode
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnScanCropAndDecodeIamge_Click( object sender, EventArgs e )
		{
			try
			{
				btnScanCropAndDecodeIamge.Enabled = false;

				// Get the settings file
				OpenFileDialog OpenFileDlg = new OpenFileDialog();
				OpenFileDlg.Filter = "Settings File|*.xml";
                OpenFileDlg.InitialDirectory = m_strAppPath;

                //if( OpenFileDlg.ShowDialog() == DialogResult.OK && OpenFileDlg.FileName.Length > 0 )
                //    m_strSettingsFile = OpenFileDlg.FileName;
                //else
                //    return;

				// Scan the image
				WriteMessage( "Scanning Image..." );
				btnScanCropAndDecodeIamge.Text = "Scanning Image...";
				this.Refresh();
				InitScanner();
				m_objScanner.SetSource();
				m_objScanner.Scan();

				// Crop Image
				// Get the image from the scanner
				WriteMessage( "Cropping Image..." );
				btnScanCropAndDecodeIamge.Text = "Cropping Image...";
				this.Refresh();
				byte[] baIamge = m_objScanner.GetImage();

				// Crop the image in memory
				byte[] baNewIamge = DecodeLib.CImageHelper.CropImage(baIamge);

				// Decode the cropped image
				WriteMessage( "Decoding Image..." );
				btnScanCropAndDecodeIamge.Text = "Decoding Image...";
				this.Refresh();
				DecodeImage( baNewIamge, false );

				// Display the cropped image
				picImage.Image = Image.FromStream( new System.IO.MemoryStream( baNewIamge ) );
			}
			catch( Exception eX )
			{
				WriteMessage( eX.Message );
			}
			finally
			{
				btnScanCropAndDecodeIamge.Text = "Scan, Crop and Decode Image";
				btnScanCropAndDecodeIamge.Enabled = true;
			}
		}

		#region UI helper methods to set Crop Settings

		/// <summary>
		/// chkSetPoint1_CheckedChanged
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void chkSetPoint1_CheckedChanged( object sender, EventArgs e )
		{
			// Check if text box have point Coordinates
			if( txt1x.Text.Trim() != String.Empty && txt1y.Text.Trim() != String.Empty )
			{
				// Remove the events from labels
				//this.lbl1.Click -= new System.EventHandler( this.lbl1_Click );
			}
			else
			{
				Application.DoEvents();
				strImgPoint = "1";
				this.Cursor = Cursors.Cross;
				this.picImage.MouseMove += new System.Windows.Forms.MouseEventHandler( this.picImage_MouseMove );
			}
		}

		/// <summary>
		/// chkSetPoint2_CheckedChanged
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void chkSetPoint2_CheckedChanged( object sender, EventArgs e )
		{
			// Check if text box have point Coordinates
			if( txt2x.Text.Trim() != String.Empty && txt2y.Text.Trim() != String.Empty )
			{
				// Remove the events from labels
				//this.lbl2.Click -= new System.EventHandler( this.lbl2_Click );
			}
			else
			{
				Application.DoEvents();
				strImgPoint = "2";
				this.Cursor = Cursors.Cross;
				this.picImage.MouseMove += new System.Windows.Forms.MouseEventHandler( this.picImage_MouseMove );
			}
		}

		/// <summary>
        /// btnResetPoints_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnResetPoints_Click( object sender, EventArgs e )
		{
			ResetPoints();
		}

        /// <summary>
        /// ResetPoints
        /// </summary>
		private void ResetPoints()
		{	
			txt1x.Text = String.Empty;
			txt1y.Text = String.Empty;

			txt2x.Text = String.Empty;
			txt2y.Text = String.Empty;
		}

		/// <summary>
		/// btnSaveSettings_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSaveSettings_Click( object sender, EventArgs e )
		{
			//SaveSettings();
            SaveCropSettingsToRegistry();
		}


        private void SaveCropSettingsToRegistry()
        {
            List<String> lst = new List<String>();
            lst = CreateString();
            Registry.SetValue("HKEY_CURRENT_USER\\software\\classes\\Intelliscan", "Crop", lst[0]);
   
        }

        public static string GetCropSettingsFromRegistry()
        {
            try
            {
                return (Registry.GetValue("HKEY_CURRENT_USER\\software\\classes\\Intelliscan", "Crop", null).ToString());
            }
            catch
            {
                return "";
            }
        }


		/// <summary>
		/// picImage_DoubleClick
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void picImage_DoubleClick( object sender, EventArgs e )
		{
			this.picImage.MouseMove -= new System.Windows.Forms.MouseEventHandler( this.picImage_MouseMove );

			if( chkSetPoint1.Checked )
				chkSetPoint1.Checked = false;

			if( chkSetPoint2.Checked )
				chkSetPoint2.Checked = false;

			this.Cursor = Cursors.Default;
		}

		/// <summary>
		/// picImage_MouseMove
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void picImage_MouseMove( object sender, MouseEventArgs e )
		{
			Application.DoEvents();

			String valueX = e.X.ToString();
			String valueY = e.Y.ToString();

			switch( strImgPoint )
			{
				case "1":
					txt1x.Text = valueX;
					txt1y.Text = valueY;
					break;

				case "2":
					txt2x.Text = valueX;
					txt2y.Text = valueY;
					break;
			}
		}

		/// <summary>
		/// CalculateHeightAndWidth
		///		-	Calculate height and width for each rectangle
		/// </summary>
		/// <param name="x1"></param>
		/// <param name="y1"></param>
		/// <param name="x2"></param>
		/// <param name="y2"></param>
		/// <returns></returns>
		private int[] CalculateHeightAndWidth( int x1, int y1, int x2, int y2 )
		{
			int height, width;
			height = y2 - y1;
			width = x2 - x1;

			int[] calc = new int[2];
			calc[0] = width;
			calc[1] = height;

			return calc;
		}

		/// <summary>
		/// CreateString
		///		-	Create Co ordinates, width, height values String
		/// </summary>
		/// <returns></returns>
		private List<String> CreateString()
		{
			int x1, x2, y1, y2;
			List<String> lstString = new List<String>();
			int[] htWd = new int[2];

			//Get first Point co-ordinates
			x1 = Convert.ToInt16( txt1x.Text );
			x2 = Convert.ToInt16( txt2x.Text );
			y1 = Convert.ToInt16( txt1y.Text );
			y2 = Convert.ToInt16( txt2y.Text );
			htWd = CalculateHeightAndWidth( x1, y1, x2, y2 );

			//Add the value to List of strings
			lstString.Add( x1 + "," + y1 + "," + htWd[0] + "," + htWd[1] );

			return lstString;
		}

		/// <summary>
		/// CheckInt
		///		-	Check if given string is a number or not
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		private bool CheckInt( String s )
		{
			bool isNum = false;
			int i = 0;
			isNum = int.TryParse( s, out i );
			return isNum;
		}

		/// <summary>
		/// SaveSettings
		///		-	Save settings (point co ordinates in a file)
		/// </summary>
		private void SaveSettings()
		{
			// Get the file from the user
			SaveFileDialog SaveFileDlg = new SaveFileDialog();
			SaveFileDlg.InitialDirectory = m_strAppPath;
            SaveFileDlg.FileName = "CropSettings.xml";
            SaveFileDlg.Filter = "Settings File|*.xml";
			if( SaveFileDlg.ShowDialog() == DialogResult.OK && SaveFileDlg.FileName.Length > 0 )
			{
				m_strSettingsFile = SaveFileDlg.FileName;

				List<String> lst = new List<String>();
				lst = CreateString();

				//Create file 
				if( !File.Exists( m_strSettingsFile ) )
				{
					XmlDocument doc = new XmlDocument();
					XmlDeclaration dec = doc.CreateXmlDeclaration( "1.0", null, null );
					doc.AppendChild( dec );// Create the root element
					XmlElement root = doc.CreateElement( "SettingsAndPaths" );
					doc.AppendChild( root );
					doc.Save( m_strSettingsFile );
				}
				try
				{
					int i = 1;
					//Write settings to the file
					XmlDocument doc = new XmlDocument();
					doc.Load( m_strSettingsFile );

					XmlNode nd = doc.SelectSingleNode( "//Coordinates" );
					if( nd == null )
					{
						XmlNode ndRoot = doc.SelectSingleNode( "/SettingsAndPaths" );
						XmlElement ele = doc.CreateElement( "Coordinates" );
						ndRoot.AppendChild( ele );
						nd = ele;
					}
					else
					{
						nd.RemoveAll();
					}
					foreach( String s in lst )
					{
						XmlNode childNode = doc.CreateElement( "Point" + i.ToString() );
						childNode.InnerText = s;
						nd.AppendChild( childNode );
						i++;
					}
					doc.Save( m_strSettingsFile );
					MessageBox.Show( "Settings saved successfully" );
				}
				catch( Exception ex )
				{
					MessageBox.Show( ex.Message );
					MessageBox.Show( "Settings could not be saved successfully" );
				}
			}
		}
		#endregion

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnSaveScannerSettings_Click(object sender, EventArgs e)
        {
            SaveScannerSettings();
        }

        private void btnSaveDecode_Click(object sender, EventArgs e)
        {
            SaveDecoderSettings();
        }

        private void btnReset1_Click(object sender, EventArgs e)
        {

        }

      


	}
}
