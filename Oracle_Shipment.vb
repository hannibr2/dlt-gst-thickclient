﻿Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types

Public Class Oracle_Shipment
    Inherits Oracle_General

#Region "           Shipments           "

    Public Function AddShipment(ByVal TypeID As Integer, ByVal PriorityID As Integer, ByVal PhaseID As Integer, ByVal Note As String, ByVal SenderID As Decimal, ByVal ReceiverID As Double, ByVal ExtReferenceTypeID As Integer, ByVal FillingMaterialID As Integer, ByVal Workstation As String) As Decimal
        'ByVal ExtReference As String, "Removed from input parameters
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        Dim TempShipmentID As Long = -1

        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_shipment.addshipment", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_type_id", OracleDbType.Int64).Value = TypeID
        m_command.Parameters.Add("i_priority_id", OracleDbType.Int32).Value = PriorityID
        m_command.Parameters.Add("i_phase_id", OracleDbType.Int32).Value = PhaseID 'Package Wrapped
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Note
        m_command.Parameters.Add("i_sender_id", OracleDbType.Int64).Value = SenderID
        m_command.Parameters.Add("i_receiver_id", OracleDbType.Int64).Value = ReceiverID
        m_command.Parameters.Add("i_external_reference_type_id", OracleDbType.Int64).Value = ExtReferenceTypeID
        m_command.Parameters.Add("i_external_reference", OracleDbType.Varchar2).Value = DBNull.Value
        m_command.Parameters.Add("i_filling_material_id", OracleDbType.Decimal).Value = FillingMaterialID
        m_command.Parameters.Add("i_workstation", OracleDbType.Varchar2).Value = Workstation

        m_command.Parameters.Add("o_shipment_id", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_shipment_id").Value)
        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
            TempShipmentID = Convert.ToInt64(m_command.Parameters("o_shipment_id").Value.ToString())
        Else
            m_Trans.Rollback()
            Throw New Exception("Error adding shipment - " + m_command.Parameters("o_error_msg").Value.ToString())
        End If
        Return TempShipmentID
    End Function

    Public Sub ShowExistingShipments()
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select  * " & _
                    "from v_shipment vs " & _
                    "where vs.shipment_phase_id = 50201 "

            m_DataSet = ExecuteQuery(query)
            frmMain.grdExistingShipments.DataSource = m_DataSet.Tables(0)
        Catch
            Throw New Exception("Error getting existing shipments")
        End Try
    End Sub

    Public Sub ShowFinalizedShipments()
        Dim query As String
        Dim m_DataSet As DataSet

        Try

            'query = "select vs.shipment_id, vs.shipment_name, vs.first_name, vs.last_name, vs.company, vs.country_name, vs.mail_address, vs.city, vs.package_count, vs.container_count, vs.date_modified, vs.receiver_id,  " & _
            '        "vs.sender_first_name, vs.sender_last_name, vs.sender_company, vs.sender_country_name, vs.sender_mail_address, vs.sender_city " & _
            query = "select  * " & _
                    "from v_shipment_finalized_recent vs "

            m_DataSet = ExecuteQuery(query)
            frmMain.grdFinalizedShipments.DataSource = m_DataSet.Tables(0)
        Catch
            Throw New Exception("Error getting finalized shipments")
        End Try
    End Sub

    Public Sub ShowAnticipatedShipments(ByVal FulfillmentSite As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            Dim Address As String() = FulfillmentSite.Split(" ")


            'query = "select vs.shipment_id, vs.shipment_name, vs.first_name, vs.last_name, vs.company, vs.country_name, vs.mail_address, vs.city, vs.package_count, vs.container_count, vs.date_modified, vs.receiver_id,  " & _
            '        "vs.sender_first_name, vs.sender_last_name, vs.sender_company, vs.sender_country_name, vs.sender_mail_address, vs.sender_city " & _
            query = "select  * " & _
                    "from v_shipment vs " & _
                    "where vs.shipment_phase_id in ( 50202, 50203, 50204, 50205 ) "

            If Address.Count > 0 Then
                query += "and vs.city like '" & Address(0) & "%'"
            End If

            m_DataSet = ExecuteQuery(query)
            frmMain.grdAnticipatedShipments.DataSource = m_DataSet.Tables(0)
        Catch
            Throw New Exception("Error getting finalized shipments")
        End Try
    End Sub

    Public Function ShowShipmentByShipmentName(ByVal ShipmentName As String) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select  * " & _
                    "from v_shipment vs " & _
                    "where vs.shipment_name = '" & ShipmentName & "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)
        Catch
            Throw New Exception("Error getting existing shipments")
        End Try
    End Function

    Public Function ShowShipmentByShipmentID(ByVal ShipmentID As Decimal) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select  * " & _
                    "from v_shipment vs " & _
                    "where vs.shipment_id = '" & ShipmentID & "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)
        Catch
            Throw New Exception("Error getting existing shipments")
        End Try
    End Function

    Public Function ShipmentReferenceUpdate() As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = " select * " & _
                    " from v_shipment vs " & _
                    " where vs.shipment_phase_id <> 50206 " & _
                    " and vs.tracking_update_date < sysdate - 1/24 "


            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)
        Catch
            Throw New Exception("Error getting existing shipments")
        End Try
    End Function


    Public Sub UpdateShipmentOLD(ByVal ContainerID As Decimal, ByVal TypeID As Integer, ByVal Origin As String, ByVal Barcode As String, ByVal Ranking As Integer, ByVal PhaseID As Integer, ByVal Note As String, ByVal ReceiverID As Decimal)
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        Dim tempcontainerid As Object
        Dim tempparentid As Object
        Dim temptypeid As Object
        Dim temporigin As Object
        Dim tempranking As Object
        Dim tempphaseid As Object
        Dim tempnote As Object
        Dim tempreceiverid As Object



        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_container.updatecontainer", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans

        If ContainerID <> 0 Then
            m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = ContainerID.ToString("F0")
            tempcontainerid = ContainerID.ToString("F0")
        Else
            m_command.Parameters.Add("i_container_id", OracleDbType.Decimal).Value = DBNull.Value
            tempcontainerid = DBNull.Value
        End If

        'm_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ParentID.ToString("F0")
        'm_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = DBNull.Value
        'tempparentid = DBNull.Value
        m_command.Parameters.Add("i_parent_container_id", OracleDbType.Decimal).Value = ContainerID.ToString("F0")
        tempparentid = ContainerID.ToString("F0")

        If TypeID <> 0 Then
            m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = TypeID
            temptypeid = TypeID.ToString()
        Else
            m_command.Parameters.Add("i_type_id", OracleDbType.Decimal).Value = DBNull.Value
            temptypeid = DBNull.Value
        End If
        If Origin <> "" Then
            m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = Origin
            temporigin = Origin
        Else
            m_command.Parameters.Add("i_origin_code", OracleDbType.Varchar2).Value = DBNull.Value
            temporigin = DBNull.Value
        End If

        'm_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = Barcode
        m_command.Parameters.Add("i_barcode", OracleDbType.Varchar2).Value = DBNull.Value
        'If Ranking <> 0 Then
        '    m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = Ranking
        '    tempranking = Ranking.ToString()
        'Else
        m_command.Parameters.Add("i_ranking", OracleDbType.Decimal).Value = DBNull.Value
        tempranking = DBNull.Value
        'End If

        'If PhaseID <> 0 Then
        '    m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = PhaseID
        '    tempphaseid = PhaseID.ToString()
        'Else
        m_command.Parameters.Add("i_phase_id", OracleDbType.Decimal).Value = DBNull.Value
        tempphaseid = DBNull.Value
        'End If

        If Note <> "" Then
            m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = Note
            tempnote = Note
        Else
            m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = DBNull.Value
            tempnote = DBNull.Value
        End If

        If ReceiverID <> 0 Then
            m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = ReceiverID.ToString("F0")
            tempreceiverid = ReceiverID.ToString("F0")
        Else
            m_command.Parameters.Add("i_receiver_id", OracleDbType.Decimal).Value = DBNull.Value
            tempreceiverid = DBNull.Value
        End If

        'm_command.Parameters.Add("o_container_id", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_code", OracleDbType.Decimal).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        'Console.WriteLine(m_command.Parameters("o_container_id").Value)
        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            'ShowMessage("Error updating container - " + m_command.Parameters("o_error_msg").Value)
            Throw New Exception("Error updating package - " + m_command.Parameters("o_error_msg").Value + vbCrLf & _
                "Container ID = " + tempcontainerid.ToString() & _
                ", Parent Container ID = " + tempparentid.ToString() & _
                ", Type ID = " + temptypeid.ToString() & _
                ", Origin = " + temporigin.ToString() & _
                ", Barcode = " & _
                ", Ranking = " + tempranking.ToString() & _
                ", Phase ID = " + tempphaseid.ToString() & _
                ", Note = " + tempnote.ToString() & _
                ", Receiver ID = " + tempreceiverid.ToString()) '+ Barcode & _
        End If

    End Sub

    Public Sub UpdateActiveShipmentDetails(ByVal ShipmentID As Decimal)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            'query = "select vsp.parent_container_id, vsp.barcode, vpe.first_name, vpe.last_name, vpe.company, vpe.country_name, vpe.mail_address " & _
            '        "from v_shipment vs, v_shipment_package vsp, v_person vpe where vs.receiver_id = VPE.PERSON_ID " & _
            '        "and vs.shipment_name = vsp.shipment_name and vsp.shipment_name = '" + ShipmentBarcode + "'"

            query = "select parent_container_id, barcode, first_name, last_name, company, country_name, mail_address, city, site_code " & _
                "from v_shipment_package_details where shipment_id = " + ShipmentID.ToString("F0")

            'query = "select vsp.parent_container_id, vsp.barcode, vpe.first_name, vpe.last_name, vpe.company, vpe.country_name, vpe.mail_address " & _
            '        "from v_shipment vs, v_package vp, v_shipment_package vsp, v_person vpe where vp.receiver_id = VPE.PERSON_ID " & _
            '        "and vp.parent_container_id = vsp.parent_container_id and vs.shipment_name = vsp.shipment_name and vsp.shipment_name = '" + ShipmentBarcode + "'"

            m_DataSet = ExecuteQuery(query)
            frmMain.grdActiveShipmentDetails.DataSource = m_DataSet.Tables(0)

        Catch ex As Exception
            Throw New Exception("Error getting active shipment details")
        End Try
    End Sub

    Public Sub UpdateShipment(ByVal ShipmentID As Decimal, ByVal ExternalReferenceTypeID As Decimal, ByVal PriorityID As Decimal)
        'Dim m_command As OracleCommand
        'Dim m_Trans As OracleTransaction

        'm_Trans = DBConnection.BeginTransaction()

        'm_command = New OracleCommand("p_shipment.updateshipment", DBConnection)
        'm_command.CommandType = CommandType.StoredProcedure
        'm_command.Transaction = m_Trans

        'm_command.Parameters.Add("i_shipment_id", OracleDbType.Decimal).Value = ShipmentID.ToString("F0")
        'm_command.Parameters.Add("i_shipment_name", OracleDbType.Varchar2).Value = DBNull.Value
        'm_command.Parameters.Add("i_type_id", OracleDbType.Int64).Value = DBNull.Value
        'm_command.Parameters.Add("i_priority_id", OracleDbType.Int32).Value = DBNull.Value
        'm_command.Parameters.Add("i_phase_id", OracleDbType.Int32).Value = PhaseID
        'm_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = DBNull.Value
        'm_command.Parameters.Add("i_sender_id", OracleDbType.Int64).Value = DBNull.Value
        'm_command.Parameters.Add("i_receiver_id", OracleDbType.Int64).Value = DBNull.Value
        'm_command.Parameters.Add("i_external_reference_type_id", OracleDbType.Decimal).Value = ExternalReferenceTypeID.ToString("F0")
        'm_command.Parameters.Add("i_external_reference", OracleDbType.Varchar2).Value = ExternalReference
        'm_command.Parameters.Add("i_filling_material_id", OracleDbType.Decimal).Value = DBNull.Value

        'm_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        'm_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        'm_command.ExecuteNonQuery()

        'Console.WriteLine(m_command.Parameters("o_error_code").Value)
        'Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        'If m_command.Parameters("o_error_code").Value = 0 Then
        '    m_Trans.Commit()
        'Else
        '    m_Trans.Rollback()
        '    Throw New Exception("Error updating shipment reference - " + m_command.Parameters("o_error_msg").Value.ToString())
        'End If
    End Sub

    Public Sub UpdateShipmentReference(ByVal ShipmentID As Decimal, ByVal ExternalReference As String)
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_shipment.updateshipment", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans

        m_command.Parameters.Add("i_shipment_id", OracleDbType.Decimal).Value = ShipmentID.ToString("F0")
        m_command.Parameters.Add("i_shipment_name", OracleDbType.Varchar2).Value = DBNull.Value
        m_command.Parameters.Add("i_type_id", OracleDbType.Int64).Value = DBNull.Value
        m_command.Parameters.Add("i_priority_id", OracleDbType.Int32).Value = DBNull.Value
        m_command.Parameters.Add("i_phase_id", OracleDbType.Int32).Value = DBNull.Value
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = DBNull.Value
        m_command.Parameters.Add("i_sender_id", OracleDbType.Int64).Value = DBNull.Value
        m_command.Parameters.Add("i_receiver_id", OracleDbType.Int64).Value = DBNull.Value
        m_command.Parameters.Add("i_external_reference_type_id", OracleDbType.Decimal).Value = DBNull.Value
        m_command.Parameters.Add("i_external_reference", OracleDbType.Varchar2).Value = ExternalReference
        m_command.Parameters.Add("i_filling_material_id", OracleDbType.Decimal).Value = DBNull.Value

        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            Throw New Exception("Error updating shipment reference - " + m_command.Parameters("o_error_msg").Value.ToString())
        End If
    End Sub

    Public Sub UpdateShipmentReference(ByVal ShipmentID As Decimal, ByVal PhaseID As Integer, ByVal ExternalReferenceTypeID As Decimal, ByVal ExternalReference As String)
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_shipment.updateshipment", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans

        m_command.Parameters.Add("i_shipment_id", OracleDbType.Decimal).Value = ShipmentID.ToString("F0")
        m_command.Parameters.Add("i_shipment_name", OracleDbType.Varchar2).Value = DBNull.Value
        m_command.Parameters.Add("i_type_id", OracleDbType.Int64).Value = DBNull.Value
        m_command.Parameters.Add("i_priority_id", OracleDbType.Int32).Value = DBNull.Value
        m_command.Parameters.Add("i_phase_id", OracleDbType.Int32).Value = PhaseID
        m_command.Parameters.Add("i_note", OracleDbType.Varchar2).Value = DBNull.Value
        m_command.Parameters.Add("i_sender_id", OracleDbType.Int64).Value = DBNull.Value
        m_command.Parameters.Add("i_receiver_id", OracleDbType.Int64).Value = DBNull.Value
        m_command.Parameters.Add("i_external_reference_type_id", OracleDbType.Decimal).Value = ExternalReferenceTypeID.ToString("F0")
        m_command.Parameters.Add("i_external_reference", OracleDbType.Varchar2).Value = ExternalReference
        m_command.Parameters.Add("i_filling_material_id", OracleDbType.Decimal).Value = DBNull.Value

        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            Throw New Exception("Error updating shipment reference - " + m_command.Parameters("o_error_msg").Value.ToString())
        End If
    End Sub

    Public Sub UpdateShipmentTrackDate(ByVal ShipmentID As Double)

        Dim query As String
        Dim m_DataSet As DataSet
        Try
            query = "update t_shipment set tdate = sysdate where shipment_id = " + ShipmentID.ToString("F0")
            m_DataSet = ExecuteQuery(query)
        Catch err As Exception
            Console.WriteLine(err.ToString())
            'ShowMessage("Error finalizing the shipment")
            Throw New Exception("Error unfinalizing the shipment")
        End Try

    End Sub

    Public Function GetShipmentID(ByVal ShipmentName As String) As Decimal
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select shipment_id from t_shipment where shipment_name = '" + ShipmentName.ToUpper() + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
        Catch err As Exception
            Throw New Exception("Error getting Shipment ID")
            Return -1
        End Try
    End Function

    Public Function GetShipmentName(ByVal ShipmentID As Decimal) As String
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select shipment_name from t_shipment where shipment_id = " & ShipmentID.ToString("F0")

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0).ToString()
        Catch err As Exception
            Throw New Exception("Error getting Shipment ID")
            Return -1
        End Try
    End Function

    Public Function GetShipmentContentsByShipmentName(ByVal ShipmentName As String) As DataTable
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            
            query = "select shipment_barcode, package_barcode, container_barcode, sample_name, protocol_group_name, protocol_name, order_session_name, recipient, delivery_form_type_name, has_attachment " & _
                "from v_shipment_package_container where shipment_barcode = '" + ShipmentName.ToUpper() + "'"

            m_DataSet = ExecuteQuery(query)
            Return m_DataSet.Tables(0)

        Catch ex As Exception
            Throw New Exception("Error getting Shipment Contents by Shipment Name")
        End Try
    End Function

    ''' <summary>
    ''' Update the phase of shipment to 50201 (shipment built)
    ''' </summary>
    ''' <param name="ShipmentID"></param>
    ''' <param name="Workstation"></param>
    ''' <remarks></remarks>
    Public Sub UnfinalizeShipment(ByVal ShipmentID As Decimal, ByVal Workstation As String)
        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction


        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_shipment.unfinalizeshipment", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_shipment_id", OracleDbType.Int64).Value = ShipmentID
        m_command.Parameters.Add("i_workstation", OracleDbType.Varchar2).Value = Workstation

        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()

        Else
            m_Trans.Rollback()
            Throw New Exception("Error unfinalizing the shipment - " + m_command.Parameters("o_error_msg").Value.ToString())
        End If

    End Sub

    ''' <summary>
    ''' Update the phase of shipment to 50202 (shipment built)
    ''' </summary>
    ''' <param name="ShipmentID"></param>
    ''' <remarks></remarks>
    Public Sub FinalizeShipment(ByVal ShipmentID As Decimal, ByVal PriorityID As Decimal, ByVal ExternalReferenceTypeID As Decimal)

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction


        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_shipment.finalizeshipment", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_shipment_id", OracleDbType.Int64).Value = ShipmentID.ToString("F0")
        m_command.Parameters.Add("i_priority_id", OracleDbType.Int32).Value = PriorityID.ToString("F0")
        m_command.Parameters.Add("i_external_reference_type_id", OracleDbType.Decimal).Value = ExternalReferenceTypeID.ToString("F0")


        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()

        Else
            m_Trans.Rollback()
            Throw New Exception("Error finalizing the shipment - " + m_command.Parameters("o_error_msg").Value.ToString())
        End If

    End Sub

    ''' <summary>
    ''' Remove shipment
    ''' </summary>
    ''' <param name="ShipmentName"></param>
    ''' <remarks></remarks>
    Public Sub RemoveShipment(ByVal ShipmentName As String)

        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "delete from t_shipment where container_count = 0 and shipment_name = '" + ShipmentName + "'"

            m_DataSet = ExecuteQuery(query)
        Catch err As Exception
            Console.WriteLine(err.ToString())
            'ShowMessage("Error finalizing the shipment")
            Throw New Exception("Error finalizing the shipment")
        End Try

    End Sub

    ''' <summary>
    ''' Update the phase of shipment to 50206 (shipment arrived)
    ''' </summary>
    ''' <param name="ShipmentID"></param>
    ''' <remarks></remarks>
    Public Sub ArriveShipment(ByVal ShipmentID As Decimal)

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction


        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_shipment.arriveshipment", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_shipment_id", OracleDbType.Int64).Value = ShipmentID

        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()

        Else
            m_Trans.Rollback()
            Throw New Exception("Error arriving the shipment - " + m_command.Parameters("o_error_msg").Value)
        End If

    End Sub

    ''' <summary>
    ''' Update the phase of shipment to 50206 (shipment arrived)
    ''' </summary>
    ''' <param name="ShipmentID"></param>
    ''' <remarks></remarks>
    Public Sub AddShipmentComment(ByVal ShipmentID As Decimal, ByVal CommentID As Decimal)

        Dim m_command As OracleCommand
        Dim m_Trans As OracleTransaction

        m_Trans = DBConnection.BeginTransaction()

        m_command = New OracleCommand("p_shipment.addshipmentcomment", DBConnection)
        m_command.CommandType = CommandType.StoredProcedure
        m_command.Transaction = m_Trans
        m_command.Parameters.Add("i_shipment_id", OracleDbType.Int64).Value = ShipmentID
        m_command.Parameters.Add("i_comment_id", OracleDbType.Int64).Value = CommentID

        m_command.Parameters.Add("o_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output
        m_command.Parameters.Add("o_error_msg", OracleDbType.Varchar2, 1000).Direction = ParameterDirection.Output
        m_command.ExecuteNonQuery()

        Console.WriteLine(m_command.Parameters("o_error_code").Value)
        Console.WriteLine(m_command.Parameters("o_error_msg").Value)

        If m_command.Parameters("o_error_code").Value = 0 Then
            m_Trans.Commit()
        Else
            m_Trans.Rollback()
            Throw New Exception("Error adding shipment comment- " + m_command.Parameters("o_error_msg").Value)
        End If
    End Sub

    ''' <summary>
    ''' Get the current list of shipping carrier from SMF_SHIPPING
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetShippingCarrier(ByVal Carrier As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select carrier_name from v_carrier_display "

            m_DataSet = ExecuteQuery(query)
            'Create Shipment Tab
            frmMain.lkShippingCourier.Properties.DataSource = m_DataSet.Tables(0)
            frmMain.lkShippingCourier.Properties.DisplayMember = "CARRIER_NAME"
            frmMain.lkShippingCourier.Properties.ValueMember = "CARRIER_NAME"
            '


            If String.IsNullOrEmpty(Carrier) Then
                frmMain.lkShippingCourier.ItemIndex = 0
            Else
                frmMain.lkShippingCourier.EditValue = Carrier
            End If


            'View Shipment Tab
            'frmMain.lkViewShippingCourier.Properties.DataSource = m_DataSet.Tables(0)
            'frmMain.lkViewShippingCourier.Properties.DisplayMember = "CARRIER_NAME"
            'frmMain.lkViewShippingCourier.Properties.ValueMember = "CARRIER_NAME"
            'frmMain.lkShippingCourier.ItemIndex = 0
        Catch
            Throw New Exception("Error getting shipping carrier list")
        End Try
    End Sub

    ''' <summary>
    ''' Get the shipping priority from SMF_SHIPPING
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetShippingPriority(ByVal Priority As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select priority_name from v_priority"

            m_DataSet = ExecuteQuery(query)
            frmMain.lkShippingPriority.Properties.DataSource = m_DataSet.Tables(0)
            frmMain.lkShippingPriority.Properties.DisplayMember = "PRIORITY_NAME"
            frmMain.lkShippingPriority.Properties.ValueMember = "PRIORITY_NAME"

            If String.IsNullOrEmpty(Priority) Then
                frmMain.lkShippingPriority.ItemIndex = 0
            Else
                frmMain.lkShippingPriority.EditValue = Priority
            End If

        Catch
            Throw New Exception("Error getting shipping priority list")
        End Try
    End Sub

    ''' <summary>
    ''' Get the shipping comment from SMF_SHIPPING
    ''' </summary>
    ''' <remarks></remarks>
    Public Function GetShipmentComment() As List(Of KeyValuePair(Of String, Integer))
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select shipment_comment_id, shipment_comment_name from v_shipment_comment"
            m_DataSet = ExecuteQuery(query)


            Dim ShipmentComments As New List(Of KeyValuePair(Of String, Integer))

            Using TempShipmentCommentTable As DataTable = m_DataSet.Tables(0)
                For Each CurrentRow As DataRow In TempShipmentCommentTable.Rows
                    ShipmentComments.Add(New KeyValuePair(Of String, Integer)(CurrentRow.Item("SHIPMENT_COMMENT_NAME"), CInt(CurrentRow.Item("SHIPMENT_COMMENT_ID"))))
                Next
            End Using


            'frmShipmentComment.lkShipmentComment.Properties.DataSource = m_DataSet.Tables(0)
            'frmShipmentComment.lkShipmentComment.Properties.DisplayMember = "SHIPMENT_COMMENT_NAME"
            'frmShipmentComment.lkShipmentComment.Properties.ValueMember = "SHIPMENT_COMMENT_ID"
            'frmShipmentComment.lkShipmentComment.Properties.PopulateColumns()

            'ShippingComment.Properties.Columns("SHIPMENT_COMMENT_NAME").Visible = True
            'ShippingComment.Properties.Columns("SHIPMENT_COMMENT_ID").Visible = False

            'frmShipmentComment.lkShipmentComment.ItemIndex = 0
            Return ShipmentComments

        Catch err As Exception

            Throw New Exception("Error getting shipping comment")
        End Try
    End Function

    Public Function GetNumberOfCreatedShipments(ByVal Workstation As String) As Integer
        Dim query As String
        Dim m_DataSet As DataSet

        query = "select count(vs.shipment_id) " & _
                "from v_shipment vs " & _
                "     inner join t_shipment_workstation sw on sw.shipment_id = vs.shipment_id " & _
                "where upper(sw.workstation) = '" & Workstation.ToUpper() & "'"

        m_DataSet = ExecuteQuery(query)

        'Return the number of unique destinations in the view
        Return m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
    End Function
#End Region

    Public Function SuggestedShipmentReceiverID(ByVal ReceiverID As Double, ByVal SiteCode As String) As Decimal
        Dim query As String
        Dim m_DataSet As DataSet
        Dim ShipmentReceiverID As Double = -1

        Try
            'query = "select shipment_receiver_id " & _
            '        "from ( " & _
            '        "    select * " & _
            '        "    from (   " & _
            '        "        select vspd.shipment_receiver_id, vspd.shipment_create_date, 1 ranking  " & _
            '        "        from v_shipment_package_details vspd  " & _
            '        "        where vspd.package_receiver_id = " & ReceiverID & _
            '        "        union " & _
            '        "        select vspd.shipment_receiver_id, vspd.shipment_create_date, 2 ranking " & _
            '        "        from v_shipment_package_details vspd " & _
            '        "        where upper(vspd.site_code) = '" & SiteCode.ToUpper & "' ) " & _
            '        "    order by ranking asc,  shipment_create_date desc ) " & _
            '       "where rownum = 1 "

            query = " select shipment_receiver_id from( " & _
                    " select * from( " & _
                    " select * from( " & _
                    " select /*+first_rows(1)*/shipment_receiver_id, 1 ranking " & _
                    "   from V_SHIPMENT_PACKAGE_DETAILS " & _
                    " where package_receiver_id = " & ReceiverID & _
                    " order by shipment_create_date desc " & _
                    ") where rownum < 2 " & _
                    "union all " & _
                    "select * from( " & _
                    "select /*+first_rows(1)*/shipment_receiver_id, 2 ranking " & _
                    "  from V_SHIPMENT_PACKAGE_DETAILS " & _
                    " where upper(site_code) = '" & SiteCode.ToUpper & "' " & _
                    " order by shipment_create_date desc " & _
                    ") where rownum < 2 " & _
                    ") order by ranking asc " & _
                    ") where rownum < 2 "

            m_DataSet = ExecuteQuery(query)

            If (m_DataSet.Tables.Count > 0 AndAlso m_DataSet.Tables(0).Rows.Count > 0) Then
                ShipmentReceiverID = m_DataSet.Tables(0).Rows(0).ItemArray.GetValue(0)
            End If

        Catch err As Exception
            Throw New Exception("Error getting suggested shipment receiver")
            ShipmentReceiverID = -1
        End Try
        Return ShipmentReceiverID
    End Function

    Public Sub ShowShipmentDetails(ByVal Barcode As String)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select  * " & _
                    "from mv_container_history_details " & _
                    "where upper(shipment_name) =  '" & Barcode.ToUpper() & "' OR " & _
                    "upper(package_barcode) =  '" & Barcode.ToUpper() & "' OR " & _
                    "upper(barcode) =  '" & Barcode.ToUpper() & "'"

            m_DataSet = ExecuteQuery(query)
            frmMain.GridShipment.DataSource = m_DataSet.Tables(0)
            frmMain.GridViewShipmentDetails.BestFitColumns()
        Catch
            Throw New Exception("Error getting details")
        End Try
    End Sub

    Public Sub ShowShipmentDetails(ByVal SiteCode As String, ByVal FromDate As DateTime, ByVal ToDate As DateTime)
        Dim query As String
        Dim m_DataSet As DataSet

        Try
            query = "select  * " & _
                    "from mv_container_history_details " & _
                    "where upper(site_code) =  '" & SiteCode.ToUpper() & "' and " & _
                    "( container_available between to_date( '" & FromDate & "' , 'MM/DD/YYYY HH12:MI:SS AM') AND to_date( '" & ToDate & "' , 'MM/DD/YYYY HH12:MI:SS AM')  OR " & _
                    " container_arrived between to_date( '" & FromDate & "' , 'MM/DD/YYYY HH12:MI:SS AM') AND to_date( '" & ToDate & "' , 'MM/DD/YYYY HH12:MI:SS AM')  OR " & _
                    " container_delivered between to_date( '" & FromDate & "' , 'MM/DD/YYYY HH12:MI:SS AM') AND to_date( '" & ToDate & "' , 'MM/DD/YYYY HH12:MI:SS AM')  ) "

            m_DataSet = ExecuteQuery(query)
            frmMain.GridShipment.DataSource = m_DataSet.Tables(0)
            frmMain.GridViewShipmentDetails.BestFitColumns()
        Catch
            Throw New Exception("Error getting details")
        End Try
    End Sub

    Public Sub New()
        MyBase.New()
    End Sub

End Class
