﻿Public Class ValidationResult

    Dim m_IsValid As Boolean
    Dim m_ValidationMessages As List(Of String)

    Public Property IsValid() As Boolean
        Get
            Return m_IsValid
        End Get
        Set(ByVal value As Boolean)
            m_IsValid = value
        End Set
    End Property

    Public Property ValidationMessages() As List(Of String)
        Get
            Return m_ValidationMessages
        End Get
        Set(ByVal value As List(Of String))
            m_ValidationMessages = value
        End Set
    End Property

    Public Sub New()
        m_ValidationMessages = New List(Of String)
    End Sub
End Class
