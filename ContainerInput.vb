

Public Class ContainerInput

#Region "           Declarations            "

    'Required for database input
    Private m_ContainerID As Decimal
    Private m_ParentID As Decimal
    Private m_TypeID As Integer
    Private m_Origin As String
    Private m_Barcode As String
    Private m_Ranking As Integer
    Private m_PhaseID As Integer
    Private m_Note As String
    Private m_ReceiverID As Decimal

    'Validation Rules values
    Private m_site As String
    Private m_recipient As String
    Private m_destination As String
    Private m_content_type As String
    Private m_request_number As String
    Private m_delivery_form As String
    Private m_protocol As String
    Private m_destination_rack_group_name As String

    'Private m_validation_result As New ValidationResult


#End Region

#Region "           Properties          "

    Public Property ContainerID As Decimal
        Get
            Return m_ContainerID
        End Get
        Set(value As Decimal)
            If m_ContainerID = value Then
                Return
            End If
            m_ContainerID = value
        End Set
    End Property

    Public Property ParentID As Decimal
        Get
            Return m_ParentID
        End Get
        Set(value As Decimal)
            If m_ParentID = value Then
                Return
            End If
            m_ParentID = value
        End Set
    End Property
    Public Property TypeID As Integer
        Get
            Return m_TypeID
        End Get
        Set(value As Integer)
            If m_TypeID = value Then
                Return
            End If
            m_TypeID = value
        End Set
    End Property
    Public Property Origin As String
        Get
            Return m_Origin
        End Get
        Set(value As String)
            If m_Origin = value Then
                Return
            End If
            m_Origin = value
        End Set
    End Property
    Public Property Barcode As String
        Get
            Return m_Barcode
        End Get
        Set(value As String)
            If m_Barcode = value Then
                Return
            End If
            m_Barcode = value
        End Set
    End Property
    Public Property Ranking As Integer
        Get
            Return m_Ranking
        End Get
        Set(value As Integer)
            If m_Ranking = value Then
                Return
            End If
            m_Ranking = value
        End Set
    End Property
    Public Property PhaseID As Integer
        Get
            Return m_PhaseID
        End Get
        Set(value As Integer)
            If m_PhaseID = value Then
                Return
            End If
            m_PhaseID = value
        End Set
    End Property
    Public Property Note As String
        Get
            Return m_Note
        End Get
        Set(value As String)
            If m_Note = value Then
                Return
            End If
            m_Note = value
        End Set
    End Property
    Public Property ReceiverID As Decimal
        Get
            Return m_ReceiverID
        End Get
        Set(value As Decimal)
            If m_ReceiverID = value Then
                Return
            End If
            m_ReceiverID = value
        End Set
    End Property



    Public Property Site As String
        Get
            Return m_site
        End Get
        Set(value As String)
            If m_site = value Then
                Return
            End If
            m_site = value
        End Set
    End Property
    Public Property Recipient As String
        Get
            Return m_recipient
        End Get
        Set(value As String)
            If m_recipient = value Then
                Return
            End If
            m_recipient = value
        End Set
    End Property
    Public Property Destination As String
        Get
            Return m_destination
        End Get
        Set(value As String)
            If m_destination = value Then
                Return
            End If
            m_destination = value
        End Set
    End Property
    Public Property ContentType As String
        Get
            Return m_content_type
        End Get
        Set(value As String)
            If m_content_type = value Then
                Return
            End If
            m_content_type = value
        End Set
    End Property
    Public Property RequestNumber As String
        Get
            Return m_request_number
        End Get
        Set(value As String)
            If m_request_number = value Then
                Return
            End If
            m_request_number = value
        End Set
    End Property
    Public Property DeliveryForm As String
        Get
            Return m_delivery_form
        End Get
        Set(value As String)
            If m_delivery_form = value Then
                Return
            End If
            m_delivery_form = value
        End Set
    End Property
    Public Property Protocol As String
        Get
            Return m_protocol
        End Get
        Set(value As String)
            If m_protocol = value Then
                Return
            End If
            m_protocol = value
        End Set
    End Property
    Public Property DestinationRackGroupName As String
        Get
            Return m_destination_rack_group_name
        End Get
        Set(value As String)
            If m_destination_rack_group_name = value Then
                Return
            End If
            m_destination_rack_group_name = value
        End Set
    End Property

    'Public Property ValidationResult As ValidationResult
    '    Get
    '        Return m_validation_result
    '    End Get
    '    Set(value As ValidationResult)
    '        If m_validation_result Is value Then
    '            Return
    '        End If
    '        m_validation_result = value
    '    End Set
    'End Property


#End Region

#Region "           Constructors            "

    Public Sub New(ByVal ContainerID As Decimal, _
                   ByVal ParentID As Decimal, _
                   ByVal TypeID As Integer, _
                   ByVal Origin As String, _
                   ByVal Barcode As String, _
                   ByVal Ranking As Integer, _
                   ByVal PhaseID As Integer, _
                   ByVal Note As String, _
                   ByVal ReceiverID As Decimal, _
                   ByVal Site As String, _
                   ByVal Recipient As String, _
                   ByVal Destination As String, _
                   ByVal ContentType As String, _
                   ByVal RequestNumber As String, _
                   ByVal Deliveryform As String, _
                   ByVal Protocol As String, _
                   ByVal DestinationRackGroupName As String)
    End Sub

    Public Sub New(ByVal ObjectInput As DataRowView)

        Me.Barcode = ObjectInput.Item("BARCODE")

        Me.ContainerID = ObjectInput.Item("CONTAINER_ID")
        Me.ParentID = ObjectInput.Item("PARENT_CONTAINER_ID")
        Me.TypeID = ObjectInput.Item("TYPE_ID")
        Me.Origin = ObjectInput.Item("TYPE_NAME").ToString
        Me.Barcode = ObjectInput.Item("BARCODE").ToString
        Me.Ranking = ObjectInput.Item("RANKING")
        Me.PhaseID = ObjectInput.Item("PHASE_ID")
        Me.Note = String.Empty
        Me.ReceiverID = ObjectInput.Item("RECEIVER_ID")

        Me.Site = ObjectInput.Item("SITE_CODE").ToString
        Me.Recipient = ObjectInput.Item("EMAIL").ToString
        Me.Destination = ObjectInput.Item("MAIL_ADDRESS").ToString
        Me.ContentType = ObjectInput.Item("FACTORY_SERVICE").ToString
        Me.RequestNumber = ObjectInput.Item("ORDER_NAME").ToString
        Me.DeliveryForm = ObjectInput.Item("DELIVERY_FORM_TYPE_NAME").ToString
        Me.Protocol = ObjectInput.Item("PROTOCOL_NAME").ToString
        Me.DestinationRackGroupName = ObjectInput.Item("DESTINATION_RACK_GROUP_NAME").ToString

    End Sub

#End Region
End Class
