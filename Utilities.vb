﻿Module Utilities

    Private m_frmInfo As frmInfo
    Private m_frmComment As frmComment

    Public Sub ShowMessage(ByVal Message As String)
        m_frmInfo = New frmInfo()
        If Message.Length > 300 Then
            m_frmInfo.txtInfo.ScrollBars = ScrollBars.Vertical
        End If
        m_frmInfo.txtInfo.Text = Message
        m_frmInfo.txtInfo.Multiline = True
        m_frmInfo.txtInfo.ReadOnly = True
        m_frmInfo.txtInfo.Enabled = True
        m_frmInfo.ShowDialog()
    End Sub

    Public Sub AddComment(ByRef Comment As String)
        m_frmComment = New frmComment()
        m_frmComment.ShowDialog()
        Comment = m_frmComment.Comment
    End Sub

End Module
