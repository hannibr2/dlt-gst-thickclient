﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Global Shipping and Tracking")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Novartis Institutes for BioMedical Research, Inc.")> 
<Assembly: AssemblyProduct("Global Shipping and Tracking")> 
<Assembly: AssemblyCopyright("Copyright © NIBRI 2011")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("23cc94f1-d5fd-4c43-a429-954a862d1695")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.20")> 
<Assembly: AssemblyFileVersion("1.0.0.20")> 
