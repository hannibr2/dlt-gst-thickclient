﻿Public Class STIUser

    Private m_account As String
    Private m_email As String
    Private m_company As String
    Private m_person_type As String

    Private m_first_name As String
    Private m_last_name As String

    Public Property Account As String
        Get
            Return m_account
        End Get
        Set(value As String)
            If m_account = value Then
                Return
            End If
            m_account = value
        End Set
    End Property
    Public Property Email As String
        Get
            Return m_email
        End Get
        Set(value As String)
            If m_email = value Then
                Return
            End If
            m_email = value
        End Set
    End Property
    Public Property Company As String
        Get
            Return m_company
        End Get
        Set(value As String)
            If m_company = value Then
                Return
            End If
            m_company = value
        End Set
    End Property

    Public ReadOnly Property IsInternal As Boolean
        Get
            Return m_person_type = "internal"
        End Get
    End Property

    Public ReadOnly Property IsContractor As Boolean
        Get
            Return m_person_type = "contractor"
        End Get
    End Property

    Public ReadOnly Property IsSMG As Boolean
        Get
            Return String.Compare(m_person_type, "smg", True) = 0
        End Get
    End Property

    Public ReadOnly Property FullName() As String
        Get
            Return String.Format("{0}, {1}", m_SenderLastName, m_SenderFirstName)
        End Get
    End Property

    Public Sub New(ByVal Account As String, ByVal FirstName As String, ByVal LastName As String, ByVal Email As String, ByVal Company As String)
        m_account = Account
        m_first_name = FirstName
        m_last_name = LastName
        m_email = Email
        m_company = Company
        m_person_type = "smg"
    End Sub

    Public Sub New(ByVal UserRow As DataRow)

        If Not IsDBNull(UserRow.Item("account")) Then
            Account = UserRow.Item("account")
        End If

        If Not IsDBNull(UserRow.Item("first_name")) Then
            m_first_name = UserRow.Item("first_name")
        End If

        If Not IsDBNull(UserRow.Item("last_name")) Then
            m_last_name = UserRow.Item("last_name")
        End If

        If Not IsDBNull(UserRow.Item("email")) Then
            Email = UserRow.Item("email")
        End If

        If Not IsDBNull(UserRow.Item("company")) Then
            Company = UserRow.Item("company")
        End If

        If Not IsDBNull(UserRow.Item("person_type")) Then
            m_person_type = UserRow.Item("person_type")
        End If



    End Sub

End Class
