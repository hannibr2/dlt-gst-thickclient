﻿Imports System.Xml
Imports System.Xml.XPath

Public Class QuickIntlResponse

    Private m_sGSTShipmentName As String = String.Empty
    Private m_sTrackingCode As String = String.Empty
    Private m_bHasErrors As Boolean = True
    Private m_bIsScheduled As Boolean = False
    Private m_bIsPickedUp As Boolean = False
    Private m_bIsDelivered As Boolean = False

    Public Property GSTShipmentName As String
        Get
            Return m_sGSTShipmentName
        End Get
        Set(value As String)
            If m_sGSTShipmentName = value Then
                Return
            End If
            m_sGSTShipmentName = value
        End Set
    End Property

    Public Property TrackingCode As String
        Get
            Return m_sTrackingCode
        End Get
        Set(value As String)
            If m_sTrackingCode = value Then
                Return
            End If
            m_sTrackingCode = value
        End Set
    End Property
    Public Property HasErrors As Boolean
        Get
            Return m_bHasErrors
        End Get
        Set(value As Boolean)
            If m_bIsScheduled = value Then
                Return
            End If
            m_bIsScheduled = value
        End Set
    End Property

    Public Property IsScheduled As Boolean
        Get
            Return m_bIsScheduled
        End Get
        Set(value As Boolean)
            If m_bIsScheduled = value Then
                Return
            End If
            m_bIsScheduled = value
        End Set
    End Property
    Public Property IsPickedUp As Boolean
        Get
            Return m_bIsPickedUp
        End Get
        Set(value As Boolean)
            If m_bIsPickedUp = value Then
                Return
            End If
            m_bIsPickedUp = value
        End Set
    End Property
    Public Property IsDelivered As Boolean
        Get
            Return m_bIsDelivered
        End Get
        Set(value As Boolean)
            If m_bIsDelivered = value Then
                Return
            End If
            m_bIsDelivered = value
        End Set
    End Property

End Class
