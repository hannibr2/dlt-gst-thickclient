Public Class frmValidationRules

    Private m_SelectedRules As List(Of Integer)
    Private m_bCancel As Boolean = False
    Private m_bOk As Boolean = False

    Public ReadOnly Property Cancel() As Boolean
        Get
            Return m_bCancel
        End Get
    End Property

    Public ReadOnly Property Ok() As Boolean
        Get
            Return m_bOk
        End Get
    End Property

    Public Property SelectedRules As List(Of Integer)
        Get
            Return m_SelectedRules
        End Get
        Set(value As List(Of Integer))
            If m_SelectedRules Is value Then
                Return
            End If
            m_SelectedRules = value
        End Set
    End Property

    Public Sub New(ByVal DeliveryFormTypeName As String, ByVal DestinationRackGroupName As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        InitializePackageValidationCheckList(DeliveryFormTypeName, DestinationRackGroupName)

    End Sub

    Private Sub InitializePackageValidationCheckList(ByVal DeliveryForm As String, ByVal DestinationRackGroupName As String)

        If String.Compare(DeliveryForm, "Powder", True) = 0 Then
            ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
        ElseIf String.Compare(DeliveryForm, "Solution", True) = 0 Then
            If String.IsNullOrEmpty(DestinationRackGroupName) Then
                ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked
                ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked
                ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
                ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
                ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
                ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
                ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
            Else
                ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Unchecked
                ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Unchecked
                ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
                ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
                ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
                ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
                ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Checked
            End If
        ElseIf String.Compare(DeliveryForm, "Plate", True) = 0 Then
            ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
        Else
            ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked
            ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
            ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
        End If

    End Sub

    Private Function GetPackageValidationRules() As List(Of Integer)
        Dim CurrentSelectedRules As New List(Of Integer)
        Dim CurrentValidationRules As New PackageValidationRules

        If ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked Then
            CurrentSelectedRules.Add(CurrentValidationRules.RecipientRuleID)
        End If

        If ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked Then
            CurrentSelectedRules.Add(CurrentValidationRules.ProtocolRuleID)
        End If
        If ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Checked Then
            CurrentSelectedRules.Add(CurrentValidationRules.RequestNumberRuleID)
        End If
        If ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Checked Then
            CurrentSelectedRules.Add(CurrentValidationRules.SiteRuleID)
        End If
        If ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Checked Then
            CurrentSelectedRules.Add(CurrentValidationRules.DestinationRuleID)
        End If
        If ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Checked Then
            CurrentSelectedRules.Add(CurrentValidationRules.DeliveryFormRuleID)
        End If
        If ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Checked Then
            CurrentSelectedRules.Add(CurrentValidationRules.DestRackGroupNameRuleID)
        End If
        Return CurrentSelectedRules
    End Function

    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        Me.m_bCancel = False
        Me.m_bOk = True
        m_SelectedRules = GetPackageValidationRules()
        Me.Close()
        'Me.Dispose()
    End Sub



    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click

        Me.m_bCancel = True
        Me.m_bOk = False
        m_SelectedRules = Nothing
        Me.Close()
        'Me.Dispose()
    End Sub

    Private Sub btnPtclRcpRqNum_Click(sender As System.Object, e As System.EventArgs) Handles btnPtclRcpRqNum.Click
        ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked
        ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked
        ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Checked
        ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
    End Sub

    Private Sub btnDestRackGrp_Click(sender As System.Object, e As System.EventArgs) Handles btnDestRackGrp.Click
        ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Checked
    End Sub

    Private Sub btnProtocolRecipient_Click(sender As System.Object, e As System.EventArgs) Handles btnProtocolRecipient.Click
        ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Checked
        ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Checked
        ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
    End Sub

    Private Sub btnSite_Click(sender As System.Object, e As System.EventArgs) Handles btnSite.Click
        ChkLstPackageValidationRules.Items("Recipient").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Protocol").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Request Number").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Novartis Site").CheckState = CheckState.Checked
        ChkLstPackageValidationRules.Items("Destination").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Delivery Form").CheckState = CheckState.Unchecked
        ChkLstPackageValidationRules.Items("Dest. Rack Group Name").CheckState = CheckState.Unchecked
    End Sub
End Class