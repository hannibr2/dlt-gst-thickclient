Public Class frmShipmentComment

    Private m_SelectedComment As Integer
    Private m_bCancel As Boolean = False
    Private m_bOk As Boolean = False

    Public ReadOnly Property Cancel() As Boolean
        Get
            Return m_bCancel
        End Get
    End Property

    Public ReadOnly Property Ok() As Boolean
        Get
            Return m_bOk
        End Get
    End Property

    Public ReadOnly Property SelectedComment As Integer
        Get
            Return m_SelectedComment
        End Get
    End Property

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        InitializeShipmentComment()

    End Sub

    Private Sub InitializeShipmentComment()
        Dim ShipmentDAL As New Oracle_Shipment

        Dim ShipmentKeyValues As List(Of KeyValuePair(Of String, Integer)) = ShipmentDAL.GetShipmentComment()


        lkShipmentComment.Properties.DataSource = ShipmentKeyValues

        lkShipmentComment.Properties.DisplayMember = "Key"
        lkShipmentComment.Properties.ValueMember = "Value"
        lkShipmentComment.Properties.ForceInitialize()

        lkShipmentComment.Properties.PopulateColumns()

        lkShipmentComment.Properties.Columns("Key").Visible = True
        lkShipmentComment.Properties.Columns("Value").Visible = False
    End Sub

    Public Function GetShipmentComment() As Integer
        Dim CommentID As Integer = -1

        If Not String.IsNullOrEmpty(lkShipmentComment.EditValue) Then
            CommentID = lkShipmentComment.EditValue
        End If

        Return CommentID
    End Function

    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        Me.m_bCancel = False
        Me.m_bOk = True
        m_SelectedComment = GetShipmentComment()
        Me.Close()
        'Me.Dispose()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click

        Me.m_bCancel = True
        Me.m_bOk = False
        m_SelectedComment = -1
        Me.Close()
        'Me.Dispose()
    End Sub

End Class