Imports DevExpress.XtraGrid.Views.Grid

Public Class BarcodeForm
    Private m_frmPackageValidationRules As New PackageValidationRules
    Private m_ExistsDictionary As Dictionary(Of String, ScanValidationResult)
    Private m_bHasErrors As Boolean
    Private m_iTubeCount As Integer = -1

    Private m_bRescan As Boolean = False
    Private m_bCancel As Boolean = False
    Private m_bOk As Boolean = False

    Public ReadOnly Property Rescan() As Boolean
        Get
            Return m_bRescan
        End Get
    End Property

    Public ReadOnly Property Cancel() As Boolean
        Get
            Return m_bCancel
        End Get
    End Property

    Public ReadOnly Property Ok() As Boolean
        Get
            Return m_bOk
        End Get
    End Property

    Public Property TubeCount() As Integer
        Get
            Return m_iTubeCount
        End Get
        Set(ByVal value As Integer)
            m_iTubeCount = value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.m_bRescan = False
        Me.m_bCancel = True
        Me.m_bOk = False
        Me.Close()
        'Me.Dispose()
    End Sub

    Private Sub btnRescan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRescan.Click
        Me.m_bRescan = True
        Me.m_bCancel = False
        Me.m_bOk = False
        Me.Close()
        'Me.Dispose()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Me.m_bRescan = False
        Me.m_bCancel = False
        Me.m_bOk = True
        Me.Close()
        'Me.Dispose()
    End Sub

    Private Function CheckErrors(ByVal ValidateDictionary As Dictionary(Of String, ScanValidationResult)) As Boolean
        Dim HasErrors As Boolean = False
        Dim NotFoundCount As Integer = 0
        Dim InvalidCount As Integer = 0
        Dim ValidCount As Integer = 0
        For Each CurrentString As String In ValidateDictionary.Keys
            If Not String.IsNullOrEmpty(CurrentString) Then
                If Not ValidateDictionary(CurrentString).Exists Then
                    HasErrors = True
                    NotFoundCount += 1
                ElseIf Not ValidateDictionary(CurrentString).IsValid Then
                    HasErrors = True
                    InvalidCount += 1
                Else
                    ValidCount += 1
                End If
            End If
        Next

        Me.lblTubeValid.Text += ValidCount.ToString()
        Me.lblTubesInvalid.Text += InvalidCount.ToString()
        Me.lblTubesNotFound.Text += NotFoundCount.ToString()

        Return HasErrors
    End Function

    Public Sub New(ByVal ExistsDictionary As Dictionary(Of String, ScanValidationResult))

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        m_ExistsDictionary = ExistsDictionary

        m_bHasErrors = CheckErrors(m_ExistsDictionary)

        If m_bHasErrors Then
            btnOK.Visible = False
            btnCancel.Visible = True
            btnRescan.Visible = True
        Else
            btnOK.Visible = True
            btnCancel.Visible = True
            btnRescan.Visible = True
        End If

    End Sub

#Region "           Start of highlighting Code   - Commentted Out           "

    'Dim m_bfrm_ValRules As PackageValidationRules

    'Dim m_bfrm_ValSite As String
    'Dim m_bfrm_ValDestination As String
    'Dim m_bfrm_ValProtocol As String
    'Dim m_bfrm_ValRecipient As String

    'Dim m_bfrm_ValSet As Boolean = False

    Private Sub GrdViewBarcodeResult1_RowCellStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles GrdViewBarcodeResult1.RowCellStyle

        If Not String.IsNullOrEmpty(e.CellValue) Then
            Dim CurrentBarcode As String = e.CellValue

            If Not m_ExistsDictionary(CurrentBarcode).Exists Then
                e.Appearance.BackColor = Color.Red
            ElseIf Not m_ExistsDictionary(CurrentBarcode).IsValid Then
                e.Appearance.BackColor = Color.Yellow
            ElseIf m_ExistsDictionary(CurrentBarcode).IsBase Then
                e.Appearance.BackColor = Color.BlueViolet
            Else
                e.Appearance.BackColor = Color.Green
            End If
        End If

    End Sub


    'Private Sub GrdViewBarcodeResult1_RowCellStyle(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles GrdViewBarcodeResult3.RowCellStyle, GrdViewBarcodeResult2.RowCellStyle, GrdViewBarcodeResult1.RowCellStyle
    '    Dim IsValid As Boolean = True

    '    Try
    '        If Not String.IsNullOrEmpty(e.CellValue) Then
    '            Dim CurrentBarcode As String = e.CellValue
    '            Dim CurrentCellDataTable As DataTable = GetPendingByBarcode(CurrentBarcode)
    '            If Not CurrentCellDataTable Is Nothing AndAlso CurrentCellDataTable.Rows.Count > 0 Then

    '                If m_bfrm_ValSet Then
    '                    Dim IsSite As Boolean = True
    '                    Dim IsDestination As Boolean = True
    '                    Dim IsProtocol As Boolean = True
    '                    Dim IsRecipient As Boolean = True

    '                    If m_bfrm_ValRules.Site Then
    '                        If m_bfrm_ValSite = CurrentCellDataTable.Rows(0).Item(m_bfrm_ValRules.SiteColumn) Then
    '                            IsSite = True
    '                        Else
    '                            IsSite = False
    '                        End If
    '                    End If


    '                    If m_bfrm_ValRules.Destination Then
    '                        If m_bfrm_ValDestination = CurrentCellDataTable.Rows(0).Item(m_bfrm_ValRules.DestinationColumn) Then
    '                            IsDestination = True
    '                        Else
    '                            IsDestination = False
    '                        End If
    '                    End If

    '                    If m_bfrm_ValRules.Recipient Then
    '                        If m_bfrm_ValRecipient = CurrentCellDataTable.Rows(0).Item(m_bfrm_ValRules.RecipientColumn) Then
    '                            IsRecipient = True
    '                        Else
    '                            IsRecipient = False
    '                        End If
    '                    End If

    '                    If m_bfrm_ValRules.Protocol Then
    '                        If m_bfrm_ValProtocol = CurrentCellDataTable.Rows(0).Item(m_bfrm_ValRules.ProtocolColumn) Then
    '                            IsProtocol = True
    '                        Else
    '                            IsProtocol = False
    '                        End If
    '                    End If

    '                    IsValid = IsSite AndAlso IsDestination AndAlso IsRecipient AndAlso IsProtocol

    '                Else

    '                    m_bfrm_ValSet = True
    '                    If m_bfrm_ValRules.Site Then
    '                        m_bfrm_ValSite = CurrentCellDataTable.Rows(0).Item(m_bfrm_ValRules.SiteColumn)
    '                    End If

    '                    If m_bfrm_ValRules.Destination Then
    '                        m_bfrm_ValDestination = CurrentCellDataTable.Rows(0).Item(m_bfrm_ValRules.DestinationColumn)
    '                    End If

    '                    If m_bfrm_ValRules.Protocol Then
    '                        m_bfrm_ValProtocol = CurrentCellDataTable.Rows(0).Item(m_bfrm_ValRules.ProtocolColumn)
    '                    End If


    '                    If m_bfrm_ValRules.Recipient Then
    '                        m_bfrm_ValRecipient = CurrentCellDataTable.Rows(0).Item(m_bfrm_ValRules.RecipientColumn)
    '                    End If

    '                    IsValid = True

    '                End If

    '            End If
    '        End If

    '        If Not IsValid Then
    '            e.Appearance.BackColor = Color.Red
    '        End If


    '    Catch ex As Exception
    '        e.Appearance.BackColor = Color.Blue
    '    End Try


    'End Sub


    'Public Sub New(ByVal ValidationRules As PackageValidationRules)

    '    ' This call is required by the Windows Form Designer.
    '    InitializeComponent()

    '    ' Add any initialization after the InitializeComponent() call.

    '    m_bfrm_ValRules = ValidationRules
    'End Sub

#End Region

    Private Sub BarcodeRowToolTip_GetActiveObjectInfo(sender As System.Object, e As DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs) Handles BarcodeRowToolTip.GetActiveObjectInfo
        If Not e.SelectedControl Is grdBarcodeResult1 Then Return

        Dim info As DevExpress.Utils.ToolTipControlInfo = Nothing
        'Get the view at the current mouse position
        Dim view As GridView = grdBarcodeResult1.GetViewAt(e.ControlMousePosition)
        If view Is Nothing Then Return
        'Get the view's element information that resides at the current position
        Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = view.CalcHitInfo(e.ControlMousePosition)
        'Display a hint for row indicator cells
        If hi.HitTest = DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.RowIndicator Then
            'An object that uniquely identifies a row indicator cell
            Dim o As Object = hi.HitTest.ToString() + hi.RowHandle.ToString()
            Dim row As Integer = hi.RowHandle + 1
            Dim rowName As String

            Select Case row
                Case 1
                    rowName = "A"
                Case 2
                    rowName = "B"
                Case 3
                    rowName = "C"
                Case 4
                    rowName = "D"
                Case 5
                    rowName = "E"
                Case 6
                    rowName = "F"
                Case 7
                    rowName = "G"
                Case 8
                    rowName = "H"
                Case Else
                    rowName = "unk"
            End Select

            Dim text As String = rowName
            info = New DevExpress.Utils.ToolTipControlInfo(o, text)
        End If
        'Supply tooltip information if applicable, otherwise preserve default tooltip (if any)
        If Not info Is Nothing Then e.Info = info
    End Sub
End Class