﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel

Public Class ShipmentContent

    Private m_shipment_barcode As String = String.Empty
    Private m_package_barcode As String = String.Empty
    Private m_container_barcode As String = String.Empty
    Private m_sample_name As String = String.Empty
    Private m_protocol_group_name As String = String.Empty
    Private m_protocol_name As String = String.Empty
    Private m_order_session_name As String = String.Empty
    Private m_recipient As String = String.Empty
    Private m_delivery_form_type_name As String = String.Empty
    Private m_has_attachment As String = String.Empty

    Public Property ShipmentBarcode As String
        Get
            Return m_shipment_barcode
        End Get
        Set(value As String)
            If m_shipment_barcode = value Then
                Return
            End If
            m_shipment_barcode = value
        End Set
    End Property
    Public Property PackageBarcode As String
        Get
            Return m_package_barcode
        End Get
        Set(value As String)
            If m_package_barcode = value Then
                Return
            End If
            m_package_barcode = value
        End Set
    End Property
    Public Property ContainerBarcode As String
        Get
            Return m_container_barcode
        End Get
        Set(value As String)
            If m_container_barcode = value Then
                Return
            End If
            m_container_barcode = value
        End Set
    End Property
    Public Property SampleName As String
        Get
            Return m_sample_name
        End Get
        Set(value As String)
            If m_sample_name = value Then
                Return
            End If
            m_sample_name = value
        End Set
    End Property
    Public Property ProtocolGroupName As String
        Get
            Return m_protocol_group_name
        End Get
        Set(value As String)
            If m_protocol_group_name = value Then
                Return
            End If
            m_protocol_group_name = value
        End Set
    End Property
    Public Property ProtocolName As String
        Get
            Return m_protocol_name
        End Get
        Set(value As String)
            If m_protocol_name = value Then
                Return
            End If
            m_protocol_name = value
        End Set
    End Property
    Public Property OrderSessionName As String
        Get
            Return m_order_session_name
        End Get
        Set(value As String)
            If m_order_session_name = value Then
                Return
            End If
            m_order_session_name = value
        End Set
    End Property
    Public Property Recipient As String
        Get
            Return m_recipient
        End Get
        Set(value As String)
            If m_recipient = value Then
                Return
            End If
            m_recipient = value
        End Set
    End Property

    Public Property DeliveryFormTypeName As String
        Get
            Return m_delivery_form_type_name
        End Get
        Set(value As String)
            If m_delivery_form_type_name = value Then
                Return
            End If
            m_delivery_form_type_name = value
        End Set
    End Property

    Public ReadOnly Property HasAttachments() As Boolean
        Get
            HasAttachments = False
            If Not String.IsNullOrEmpty(m_has_attachment) Then

                If String.Compare(m_has_attachment, "TRUE", True) = 0 Then
                    HasAttachments = True
                Else
                    HasAttachments = False
                End If
            End If
            Return HasAttachments
        End Get
    End Property

    Public Sub New(ByVal ItemRow As DataRow)
        
        ShipmentBarcode = ItemRow.Item("shipment_barcode").ToString()
        PackageBarcode = ItemRow.Item("package_barcode").ToString()
        ContainerBarcode = ItemRow.Item("container_barcode").ToString()
        SampleName = ItemRow.Item("sample_name").ToString()
        ProtocolGroupName = ItemRow.Item("protocol_group_name").ToString()
        ProtocolName = ItemRow.Item("protocol_name").ToString()
        OrderSessionName = ItemRow.Item("order_session_name").ToString()
        Recipient = ItemRow.Item("recipient").ToString()
        DeliveryFormTypeName = ItemRow.Item("delivery_form_type_name").ToString()
        m_has_attachment = ItemRow.Item("has_attachment").ToString()

    End Sub

    Public Sub New()

    End Sub

End Class