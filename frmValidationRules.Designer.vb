<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmValidationRules
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton()
        Me.ChkLstPackageValidationRules = New DevExpress.XtraEditors.CheckedListBoxControl()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnProtocolRecipient = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPtclRcpRqNum = New DevExpress.XtraEditors.SimpleButton()
        Me.btnDestRackGrp = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSite = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChkLstPackageValidationRules, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.ExpandCollapseItem.Name = ""
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 1
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Size = New System.Drawing.Size(262, 27)
        Me.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(12, 285)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(107, 26)
        Me.btnOK.TabIndex = 20
        Me.btnOK.Text = "OK"
        '
        'ChkLstPackageValidationRules
        '
        Me.ChkLstPackageValidationRules.CheckOnClick = True
        Me.ChkLstPackageValidationRules.Items.AddRange(New DevExpress.XtraEditors.Controls.CheckedListBoxItem() {New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Protocol", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Recipient", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Novartis Site"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Destination"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Request Number"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Delivery Form"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Dest. Rack Group Name")})
        Me.ChkLstPackageValidationRules.Location = New System.Drawing.Point(47, 134)
        Me.ChkLstPackageValidationRules.Name = "ChkLstPackageValidationRules"
        Me.ChkLstPackageValidationRules.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.ChkLstPackageValidationRules.Size = New System.Drawing.Size(154, 132)
        Me.ChkLstPackageValidationRules.TabIndex = 22
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(143, 285)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(107, 26)
        Me.btnCancel.TabIndex = 24
        Me.btnCancel.Text = "Cancel"
        '
        'btnProtocolRecipient
        '
        Me.btnProtocolRecipient.Location = New System.Drawing.Point(12, 33)
        Me.btnProtocolRecipient.Name = "btnProtocolRecipient"
        Me.btnProtocolRecipient.Size = New System.Drawing.Size(107, 26)
        Me.btnProtocolRecipient.TabIndex = 26
        Me.btnProtocolRecipient.Text = "Protocol + Recipient"
        '
        'btnPtclRcpRqNum
        '
        Me.btnPtclRcpRqNum.Location = New System.Drawing.Point(12, 97)
        Me.btnPtclRcpRqNum.Name = "btnPtclRcpRqNum"
        Me.btnPtclRcpRqNum.Size = New System.Drawing.Size(238, 26)
        Me.btnPtclRcpRqNum.TabIndex = 27
        Me.btnPtclRcpRqNum.Text = "Protocol + Recipient + Request Number"
        '
        'btnDestRackGrp
        '
        Me.btnDestRackGrp.Location = New System.Drawing.Point(125, 33)
        Me.btnDestRackGrp.Name = "btnDestRackGrp"
        Me.btnDestRackGrp.Size = New System.Drawing.Size(125, 26)
        Me.btnDestRackGrp.TabIndex = 29
        Me.btnDestRackGrp.Text = "Dest. Rack Group Name"
        '
        'btnSite
        '
        Me.btnSite.Location = New System.Drawing.Point(70, 65)
        Me.btnSite.Name = "btnSite"
        Me.btnSite.Size = New System.Drawing.Size(107, 26)
        Me.btnSite.TabIndex = 31
        Me.btnSite.Text = "Site"
        '
        'frmValidationRules
        '
        Me.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(262, 319)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnSite)
        Me.Controls.Add(Me.btnDestRackGrp)
        Me.Controls.Add(Me.btnPtclRcpRqNum)
        Me.Controls.Add(Me.btnProtocolRecipient)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.ChkLstPackageValidationRules)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.RibbonControl)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmValidationRules"
        Me.Ribbon = Me.RibbonControl
        Me.Text = "Set Package Validation Rules"
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChkLstPackageValidationRules, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ChkLstPackageValidationRules As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnProtocolRecipient As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPtclRcpRqNum As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDestRackGrp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSite As DevExpress.XtraEditors.SimpleButton



End Class
