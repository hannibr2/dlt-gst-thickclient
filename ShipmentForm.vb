Imports System.IO
Imports System.Xml

Public Class ShipmentForm

    Private m_shipment As Shipment

    Private m_TextFields As New Collections.Generic.Dictionary(Of String, String)
    Private m_BoolFields As New Collections.Generic.Dictionary(Of String, Boolean)
    Private m_Printer As LabelerUtility.cLabeler
    Private m_PrinterForm As LabelerUtility.cShippingForm
    Private m_TemplateLocation As String = "Resources\GSTTemplates\Forms\Shipping Request Form v1.dotx" ' "Resources\GSTTemplates\Forms\Shipping Form.dotx"

    Private Sub btnCancelPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelPrint.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnOpenTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenTemplate.Click
        'Current template path is hardcoded in the labeler utility dll
        'No path setting is needed as the Set function in the labeler utility dll is not yet implemented
        Try
            Dim TempTemplatePath As String = Path.Combine(System.Windows.Forms.Application.StartupPath, m_TemplateLocation)

            'Configuration.ShipmentTemplatePath

            If File.Exists(TempTemplatePath) Then
                SetShipmentParams()
                m_PrinterForm = New LabelerUtility.cShippingForm(TempTemplatePath, m_TextFields, m_BoolFields)
                m_PrinterForm.OpenWord()
                m_PrinterForm = Nothing
            Else
                ShowMessage("Can't find form template" & vbCrLf & "Path: " & TempTemplatePath)
            End If

        Catch ex As Exception

            ShowMessage("Error Printing form. Please try again." & vbCrLf & "Error: " & ex.Message)
        End Try

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try

            Dim TempTemplatePath As String = Path.Combine(System.Windows.Forms.Application.StartupPath, m_TemplateLocation)

            'Configuration.ShipmentTemplatePath

            If File.Exists(TempTemplatePath) Then
                SetShipmentParams()
                m_PrinterForm = New LabelerUtility.cShippingForm(TempTemplatePath, m_TextFields, m_BoolFields)
                m_PrinterForm.PrintWord()
                m_PrinterForm = Nothing
            Else
                ShowMessage("Can't find form template" & vbCrLf & "Path:" & TempTemplatePath)
            End If

        Catch ex As Exception

            ShowMessage("Error Printing form. Please try again." & vbCrLf & "Error: " & ex.Message)

        End Try

    End Sub

    Private Sub SetShipmentParams()

        'Initialize the dictionaries
        m_TextFields.Clear()
        m_BoolFields.Clear()

        'Sender
        m_TextFields.Add("txtReqName", txtSenderDetailsFirst.Text + " " + txtSenderDetailsLast.Text)
        m_TextFields.Add("txtReqDept", txtSenderDetailsDept.Text)
        m_TextFields.Add("txtReqBldg", txtSenderAddress.Text)
        m_TextFields.Add("txtReqMTA", "  ")
        m_TextFields.Add("txtReqCostCtr", txtCostCenter.Text)
        m_TextFields.Add("txtReqShipDate", "3/29/11")
        m_TextFields.Add("txtReqTransit", txtRefNum.Text)
        m_TextFields.Add("txtReqPhone", txtSenderPhone.Text)
        m_TextFields.Add("txtReqShpAcct", "")
        m_TextFields.Add("txtReqEmail", txtSenderEmail.Text)

        'Receiver
        m_TextFields.Add("txtAddName", txtRecipientDetailsFirst.Text + " " + txtRecipientDetailsLast.Text)
        m_TextFields.Add("txtAddFax", " ")
        m_TextFields.Add("txtAddComp", txtRecipientDetailsCompany.Text)
        m_TextFields.Add("txtAddPhone", txtRecipientPhone.Text)
        m_TextFields.Add("txtAddStreet", txtRecipientAddress.Text)
        m_TextFields.Add("txtAddCity", txtRecipientCity.Text)
        m_TextFields.Add("txtAddState", txtRecipientState.Text)
        m_TextFields.Add("txtAddCountry", txtRecipientCountry.Text)
        m_TextFields.Add("txtAddZip", txtRecipientZip.Text)
        m_TextFields.Add("txtAddEmail", txtRecipientEmail.Text)

        'm_TextFields.Add("txtItem1", memoShipmentDescription.Text)
        'm_TextFields.Add("txtItem1Val", "")

        '                   Section II: Shipment Service

        m_TextFields.Add("txtCourName", txtCourier.Text)
        m_TextFields.Add("txtCourAcct", txtCourAcct.Text)
        m_TextFields.Add("txtRefNum", txtRefNum.Text)
        Dim TempBarcode As String = "*" & txtRefNum.Text & "*"
        m_TextFields.Add("txtRefBarcode", TempBarcode)
        '                   Add reference barcode


        m_TextFields.Add("txtShpDest", txtShpDest.Text)
        m_TextFields.Add("txtShpType", txtShpType.Text)
        m_TextFields.Add("txtShpVal", txtShpVal.Text)
        m_TextFields.Add("txtShpOrigin", txtSenderCountry.Text)


        '                   Section III: Shipment Contents

        m_TextFields.Add("txtSolQty", txtSolQty.Text)
        m_TextFields.Add("txtSolAmt", txtSolAmt.Text)
        m_TextFields.Add("txtSolType", txtSolType.Text)
        m_TextFields.Add("txtSolVal", txtSolVal.Text)

        m_TextFields.Add("txtSldQty", txtSldQty.Text)
        m_TextFields.Add("txtSldAmt", txtSldAmt.Text)
        m_TextFields.Add("txtSldType", txtSldType.Text)
        m_TextFields.Add("txtSldVal", txtSldVal.Text)



        'Signature
        m_TextFields.Add("txtSigner", txtSenderDetailsFirst.Text + " " + txtSenderDetailsLast.Text + " / " + txtSenderDetailsDept.Text)
        m_TextFields.Add("txtDate", Date.Now.ToShortDateString)

        m_BoolFields.Add("chkHasAddtlItems", False)

        'Packing Conditions
        m_BoolFields.Add("chkIsAmbient", chkIsAmbient.CheckState)
        m_BoolFields.Add("chkIsGelIce", chkIsGelIce.CheckState)
        m_BoolFields.Add("chkIsDryIce", chkIsDryIce.CheckState)
        m_BoolFields.Add("chkIsWetIce", chkIsWetIce.CheckState)

        Dim TempIsHaz As Boolean = chkIsHaz.CheckState

        'Hazardous
        m_BoolFields.Add("chkIsHzd", TempIsHaz)
        m_BoolFields.Add("chkIsNotHzd", Not TempIsHaz)

        'Not used Currently

        m_BoolFields.Add("chkIsNoXray", False)
        m_BoolFields.Add("chkIsFragile", True)

        m_BoolFields.Add("chkIsConsignee", True)
        m_BoolFields.Add("chkIsNotConsignee", False)

        m_BoolFields.Add("chkIsChem", False)
        m_BoolFields.Add("chkIsBiol", False)
        m_BoolFields.Add("chkIsRadioactive", False)


    End Sub

    Private Sub ShipmentForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim TempIndex As Integer

        txtSenderDetailsFirst.Text = m_shipment.SenderFirstName  'm_SenderFirstName
        txtSenderDetailsLast.Text = m_shipment.SenderLastName  'm_SenderLastName
        txtSenderDetailsCompany.Text = m_shipment.SenderCompany 'm_SenderCompany
        txtSenderDetailsDept.Text = m_shipment.SenderDepartment 'm_SenderDepartment
        txtSenderAddress.Text = m_shipment.SenderMailAddress  'm_SenderAddress

        'Check if City container ","
        Dim TempCity As String = m_shipment.SenderCity
        TempIndex = InStr(TempCity, ",")
        'if City doesn't contain ",", then use the return result for City and Province

        txtCostCenter.Text = m_shipment.CostCenter

        If TempIndex = 0 Then
            txtSenderCity.Text = TempCity  'm_SenderCity
            txtSenderState.Text = m_shipment.SenderState
        Else
            txtSenderCity.Text = TempCity.Substring(0, TempIndex - 1)
            txtSenderState.Text = TempCity.Substring(TempIndex + 1)
        End If

        txtSenderZip.Text = m_shipment.SenderPostalcode  'm_SenderZip
        txtSenderCountry.Text = m_shipment.SenderCountryName  'm_SenderCountry
        txtSenderEmail.Text = m_shipment.SenderEmail 'm_SenderEmail
        txtSenderPhone.Text = m_shipment.SenderPhone ' m_SenderPhone

        txtRecipientDetailsFirst.Text = m_shipment.ReceiverFirstName  'm_ReceiverFirstName
        txtRecipientDetailsLast.Text = m_shipment.ReceiverLastName 'm_ReceiverLastName
        txtRecipientDetailsCompany.Text = m_shipment.ReceiverCompany  'm_ReceiverCompany
        txtRecipientDetailsDept.Text = m_shipment.ReceiverDepartment  'm_ReceiverDepartment
        txtRecipientAddress.Text = m_shipment.ReceiverMailAddress   'm_ReceiverAddress

        'Check if City container ","
        'TempIndex = InStr(m_ReceiverCity, ",")

        TempCity = m_shipment.ReceiverCity
        TempIndex = InStr(TempCity, ",")
        'if City doesn't contain ",", then use the return result for City and Province
        If TempIndex = 0 Then
            txtRecipientCity.Text = TempCity
            txtRecipientState.Text = m_shipment.ReceiverState
        Else
            txtRecipientCity.Text = TempCity.Substring(0, TempIndex - 1)
            txtRecipientState.Text = TempCity.Substring(TempIndex + 1)
        End If

        txtRecipientZip.Text = m_shipment.ReceiverPostalcode ' m_ReceiverZip
        txtRecipientCountry.Text = m_shipment.ReceiverCountryName ' m_ReceiverCountry
        txtRecipientEmail.Text = m_shipment.ReceiverEmail  ' m_ReceiverEmail
        txtRecipientPhone.Text = m_shipment.ReceiverPhone ' m_ReceiverPhone

        'Set focus to txtCourier
        txtCourier.Text = m_shipment.Courier
        txtCourAcct.Text = m_shipment.CourierAccount
        txtShpType.Text = m_shipment.Priority

        txtRefNum.Text = m_shipment.ShipmentName   'm_ReferenceNumber
        txtShpDest.Text = m_shipment.ReceiverCountryName

        If m_shipment.SolutionContainerCount > 0 Then
            txtSolQty.Text = m_shipment.SolutionContainerCount 'm_SolutionContainerCount
            txtSolQty.Enabled = False
            txtSolAmt.Text = "100 uL"
            txtSolType.Text = "Matrix Tubes"
            txtSolVal.Text = "1"
            chkIsDryIce.CheckState = CheckState.Checked
        Else
            txtSolQty.Enabled = False
            txtSolAmt.Enabled = False
            txtSolType.Enabled = False
            txtSolVal.Enabled = False
        End If


        If m_shipment.PowderContainerCount > 0 Then
            txtSldQty.Text = m_shipment.PowderContainerCount  'm_PowderContainerCount
            txtSldQty.Enabled = False
            txtSldAmt.Text = "10 mg"
            txtSldType.Text = "Glass Tubes"
            txtSldVal.Text = "1"
        Else
            txtSldQty.Enabled = False
            txtSldAmt.Enabled = False
            txtSldType.Enabled = False
            txtSldVal.Enabled = False
        End If

        If m_shipment.HasAttachments Then
            btnAttachment.Visible = True
            ShowMessage("This shipment has containers with attachments. Remember to print the attachments.")
        Else
            btnAttachment.Visible = False
        End If

    End Sub

    Public Sub New(ByVal CurrentShipment As Shipment)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        m_shipment = CurrentShipment
    End Sub

    Private Sub btnAttachment_Click(sender As System.Object, e As System.EventArgs) Handles btnAttachment.Click
        Try
            'Dim Attachments As List(Of Byte())
            If m_shipment.HasAttachments Then
                GetChronosAttachments(m_shipment.Containers)
            End If
        Catch ex As Exception
            ShowMessage(ex.Message)
        End Try

    End Sub
End Class