﻿Public Class ScanValidationResult
    Private m_bExists As Boolean
    Private m_bIsBase As Boolean
    Private m_bIsValid As Boolean


    Public Property Exists() As Boolean
        Get
            Return m_bExists
        End Get
        Set(ByVal value As Boolean)
            If m_bExists = value Then
                Return
            End If
            m_bExists = value
        End Set
    End Property

    Public Property IsBase() As Boolean
        Get
            Return m_bIsBase
        End Get
        Set(ByVal value As Boolean)
            If m_bIsBase = value Then
                Return
            End If
            m_bIsBase = value
        End Set
    End Property

    Public Property IsValid() As Boolean
        Get
            Return m_bIsValid
        End Get
        Set(ByVal value As Boolean)
            If m_bIsValid = value Then
                Return
            End If
            m_bIsValid = value
        End Set
    End Property


End Class
