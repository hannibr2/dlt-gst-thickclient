﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel

Public Class Packages
    Inherits Collection(Of Package)

    Private m_containers As Containers

    Public ReadOnly Property HasAttachments() As Boolean
        Get
            HasAttachments = False

            For Each CurrentPackage As Package In Me
                If CurrentPackage.HasAttachments Then
                    HasAttachments = True
                    Exit For
                End If
            Next
            Return HasAttachments
        End Get
    End Property

    Public ReadOnly Property Containers As Containers
        Get
            If m_containers Is Nothing Then
                m_containers = New Containers()
                For Each CurrentPackage As Package In Me
                    m_containers.AddRange(CurrentPackage.Containers)
                Next
            End If
            Return m_containers
        End Get
    End Property

    Public Sub New(ByVal Packages As DataTable)
        For Each Row As DataRow In Packages.Rows
            Me.Add(New Package(Row))
        Next
    End Sub

    Public Sub New()

    End Sub

    Public Sub New(ByVal list As IList(Of Package))
        MyBase.New(list)

    End Sub

End Class
