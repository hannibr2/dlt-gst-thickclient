<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RecipientInfo
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.grdRecipientInfo = New DevExpress.XtraGrid.GridControl
        Me.GrdViewRecipientInfo = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colRECIPIENT_FIRSTNAME = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_LASTNAME = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_COMPANY = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_DEPARTMENT = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_ADDRESS = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_CITY = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_PROVINCE = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_POSTALCODE = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_COUNTRY = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_PHONE = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_EMAIL = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colRECIPIENT_PERSONTYPE = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar
        Me.txtRecipientFirstName = New DevExpress.XtraEditors.TextEdit
        Me.lblPeopleSearch = New DevExpress.XtraEditors.LabelControl
        Me.txtRecipientLastName = New DevExpress.XtraEditors.TextEdit
        Me.txtRecipientCompany = New DevExpress.XtraEditors.TextEdit
        Me.lblRecipientDetails = New DevExpress.XtraEditors.LabelControl
        Me.txtRecipientDetailsCompany = New DevExpress.XtraEditors.TextEdit
        Me.txtRecipientDetailsLast = New DevExpress.XtraEditors.TextEdit
        Me.txtRecipientDetailsFirst = New DevExpress.XtraEditors.TextEdit
        Me.txtRecipientAddress = New DevExpress.XtraEditors.TextEdit
        Me.txtRecipientDetailsDept = New DevExpress.XtraEditors.TextEdit
        Me.txtRecipientCountry = New DevExpress.XtraEditors.TextEdit
        Me.txtRecipientCity = New DevExpress.XtraEditors.TextEdit
        Me.txtRecipientEmail = New DevExpress.XtraEditors.TextEdit
        Me.txtRecipientPhone = New DevExpress.XtraEditors.TextEdit
        Me.btnAddRecipient = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancelUpdateRecipient = New DevExpress.XtraEditors.SimpleButton
        Me.btnUpdateRecipient = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveRecipient = New DevExpress.XtraEditors.SimpleButton
        Me.btnSearch = New DevExpress.XtraEditors.SimpleButton
        Me.txtRecipientState = New DevExpress.XtraEditors.TextEdit
        Me.txtPostalCode = New DevExpress.XtraEditors.TextEdit
        Me.lblRecipientDept = New DevExpress.XtraEditors.LabelControl
        Me.lblSearchCompany = New DevExpress.XtraEditors.LabelControl
        Me.lblSearchLastName = New DevExpress.XtraEditors.LabelControl
        Me.lblSearchFirstName = New DevExpress.XtraEditors.LabelControl
        Me.lblRecipientFirstName = New DevExpress.XtraEditors.LabelControl
        Me.lblRecipientLastName = New DevExpress.XtraEditors.LabelControl
        Me.lblRecipientCompany = New DevExpress.XtraEditors.LabelControl
        Me.lblRecipientEmail = New DevExpress.XtraEditors.LabelControl
        Me.lblRecipientPhone = New DevExpress.XtraEditors.LabelControl
        Me.lblRecipientCountry = New DevExpress.XtraEditors.LabelControl
        Me.lblRecipientPostalCode = New DevExpress.XtraEditors.LabelControl
        Me.lblRecipientState = New DevExpress.XtraEditors.LabelControl
        Me.lblRecipientCity = New DevExpress.XtraEditors.LabelControl
        Me.lblRecipientAddress = New DevExpress.XtraEditors.LabelControl
        Me.btnSelectRecipient = New DevExpress.XtraEditors.SimpleButton
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdRecipientInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewRecipientInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientFirstName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientLastName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientCompany.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientDetailsCompany.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientDetailsLast.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientDetailsFirst.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientDetailsDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientCountry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientCity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientPhone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecipientState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPostalCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.grdRecipientInfo
        Me.GridView3.Name = "GridView3"
        '
        'grdRecipientInfo
        '
        GridLevelNode1.LevelTemplate = Me.GridView3
        GridLevelNode1.RelationName = "Level1"
        Me.grdRecipientInfo.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.grdRecipientInfo.Location = New System.Drawing.Point(12, 77)
        Me.grdRecipientInfo.MainView = Me.GrdViewRecipientInfo
        Me.grdRecipientInfo.Name = "grdRecipientInfo"
        Me.grdRecipientInfo.Size = New System.Drawing.Size(818, 135)
        Me.grdRecipientInfo.TabIndex = 6
        Me.grdRecipientInfo.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GrdViewRecipientInfo, Me.GridView3})
        '
        'GrdViewRecipientInfo
        '
        Me.GrdViewRecipientInfo.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colRECIPIENT_FIRSTNAME, Me.colRECIPIENT_LASTNAME, Me.colRECIPIENT_COMPANY, Me.colRECIPIENT_DEPARTMENT, Me.colRECIPIENT_ADDRESS, Me.colRECIPIENT_CITY, Me.colRECIPIENT_PROVINCE, Me.colRECIPIENT_POSTALCODE, Me.colRECIPIENT_COUNTRY, Me.colRECIPIENT_PHONE, Me.colRECIPIENT_EMAIL, Me.colRECIPIENT_PERSONTYPE})
        Me.GrdViewRecipientInfo.GridControl = Me.grdRecipientInfo
        Me.GrdViewRecipientInfo.Name = "GrdViewRecipientInfo"
        Me.GrdViewRecipientInfo.OptionsView.ShowGroupPanel = False
        '
        'colRECIPIENT_FIRSTNAME
        '
        Me.colRECIPIENT_FIRSTNAME.Caption = "FIRST_NAME"
        Me.colRECIPIENT_FIRSTNAME.FieldName = "FIRST_NAME"
        Me.colRECIPIENT_FIRSTNAME.Name = "colRECIPIENT_FIRSTNAME"
        Me.colRECIPIENT_FIRSTNAME.Visible = True
        Me.colRECIPIENT_FIRSTNAME.VisibleIndex = 0
        '
        'colRECIPIENT_LASTNAME
        '
        Me.colRECIPIENT_LASTNAME.Caption = "LAST_NAME"
        Me.colRECIPIENT_LASTNAME.FieldName = "LAST_NAME"
        Me.colRECIPIENT_LASTNAME.Name = "colRECIPIENT_LASTNAME"
        Me.colRECIPIENT_LASTNAME.Visible = True
        Me.colRECIPIENT_LASTNAME.VisibleIndex = 1
        '
        'colRECIPIENT_COMPANY
        '
        Me.colRECIPIENT_COMPANY.Caption = "COMPANY"
        Me.colRECIPIENT_COMPANY.FieldName = "COMPANY"
        Me.colRECIPIENT_COMPANY.Name = "colRECIPIENT_COMPANY"
        Me.colRECIPIENT_COMPANY.Visible = True
        Me.colRECIPIENT_COMPANY.VisibleIndex = 2
        '
        'colRECIPIENT_DEPARTMENT
        '
        Me.colRECIPIENT_DEPARTMENT.Caption = "DEPARTMENT"
        Me.colRECIPIENT_DEPARTMENT.FieldName = "DEPARTMENT"
        Me.colRECIPIENT_DEPARTMENT.Name = "colRECIPIENT_DEPARTMENT"
        Me.colRECIPIENT_DEPARTMENT.Visible = True
        Me.colRECIPIENT_DEPARTMENT.VisibleIndex = 3
        '
        'colRECIPIENT_ADDRESS
        '
        Me.colRECIPIENT_ADDRESS.Caption = "ADDRESS"
        Me.colRECIPIENT_ADDRESS.FieldName = "MAIL_ADDRESS"
        Me.colRECIPIENT_ADDRESS.Name = "colRECIPIENT_ADDRESS"
        Me.colRECIPIENT_ADDRESS.Visible = True
        Me.colRECIPIENT_ADDRESS.VisibleIndex = 4
        '
        'colRECIPIENT_CITY
        '
        Me.colRECIPIENT_CITY.Caption = "CITY"
        Me.colRECIPIENT_CITY.FieldName = "CITY"
        Me.colRECIPIENT_CITY.Name = "colRECIPIENT_CITY"
        Me.colRECIPIENT_CITY.Visible = True
        Me.colRECIPIENT_CITY.VisibleIndex = 5
        '
        'colRECIPIENT_PROVINCE
        '
        Me.colRECIPIENT_PROVINCE.Caption = "PROVINCE"
        Me.colRECIPIENT_PROVINCE.FieldName = "PROVINCE"
        Me.colRECIPIENT_PROVINCE.Name = "colRECIPIENT_PROVINCE"
        Me.colRECIPIENT_PROVINCE.Visible = True
        Me.colRECIPIENT_PROVINCE.VisibleIndex = 6
        '
        'colRECIPIENT_POSTALCODE
        '
        Me.colRECIPIENT_POSTALCODE.Caption = "POSTAL_CODE"
        Me.colRECIPIENT_POSTALCODE.FieldName = "POSTALCODE"
        Me.colRECIPIENT_POSTALCODE.Name = "colRECIPIENT_POSTALCODE"
        Me.colRECIPIENT_POSTALCODE.Visible = True
        Me.colRECIPIENT_POSTALCODE.VisibleIndex = 7
        '
        'colRECIPIENT_COUNTRY
        '
        Me.colRECIPIENT_COUNTRY.Caption = "COUNTRY"
        Me.colRECIPIENT_COUNTRY.FieldName = "COUNTRY_NAME"
        Me.colRECIPIENT_COUNTRY.Name = "colRECIPIENT_COUNTRY"
        Me.colRECIPIENT_COUNTRY.Visible = True
        Me.colRECIPIENT_COUNTRY.VisibleIndex = 8
        '
        'colRECIPIENT_PHONE
        '
        Me.colRECIPIENT_PHONE.Caption = "PHONE"
        Me.colRECIPIENT_PHONE.FieldName = "PHONE"
        Me.colRECIPIENT_PHONE.Name = "colRECIPIENT_PHONE"
        Me.colRECIPIENT_PHONE.Visible = True
        Me.colRECIPIENT_PHONE.VisibleIndex = 9
        '
        'colRECIPIENT_EMAIL
        '
        Me.colRECIPIENT_EMAIL.Caption = "EMAIL"
        Me.colRECIPIENT_EMAIL.FieldName = "EMAIL"
        Me.colRECIPIENT_EMAIL.Name = "colRECIPIENT_EMAIL"
        Me.colRECIPIENT_EMAIL.Visible = True
        Me.colRECIPIENT_EMAIL.VisibleIndex = 10
        '
        'colRECIPIENT_PERSONTYPE
        '
        Me.colRECIPIENT_PERSONTYPE.Caption = "TYPE"
        Me.colRECIPIENT_PERSONTYPE.FieldName = "PERSON_TYPE"
        Me.colRECIPIENT_PERSONTYPE.Name = "colRECIPIENT_PERSONTYPE"
        Me.colRECIPIENT_PERSONTYPE.Visible = True
        Me.colRECIPIENT_PERSONTYPE.VisibleIndex = 11
        '
        'RibbonControl
        '
        '
        '
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.ExpandCollapseItem.Name = ""
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 1
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Size = New System.Drawing.Size(842, 49)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 460)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(842, 31)
        '
        'txtRecipientFirstName
        '
        Me.txtRecipientFirstName.EditValue = ""
        Me.txtRecipientFirstName.Location = New System.Drawing.Point(153, 51)
        Me.txtRecipientFirstName.Name = "txtRecipientFirstName"
        Me.txtRecipientFirstName.Size = New System.Drawing.Size(132, 20)
        Me.txtRecipientFirstName.TabIndex = 7
        Me.txtRecipientFirstName.Visible = False
        '
        'lblPeopleSearch
        '
        Me.lblPeopleSearch.Location = New System.Drawing.Point(12, 54)
        Me.lblPeopleSearch.Name = "lblPeopleSearch"
        Me.lblPeopleSearch.Size = New System.Drawing.Size(68, 13)
        Me.lblPeopleSearch.TabIndex = 8
        Me.lblPeopleSearch.Text = "People Search"
        Me.lblPeopleSearch.Visible = False
        '
        'txtRecipientLastName
        '
        Me.txtRecipientLastName.EditValue = ""
        Me.txtRecipientLastName.Location = New System.Drawing.Point(347, 51)
        Me.txtRecipientLastName.Name = "txtRecipientLastName"
        Me.txtRecipientLastName.Size = New System.Drawing.Size(132, 20)
        Me.txtRecipientLastName.TabIndex = 9
        Me.txtRecipientLastName.Visible = False
        '
        'txtRecipientCompany
        '
        Me.txtRecipientCompany.EditValue = ""
        Me.txtRecipientCompany.Location = New System.Drawing.Point(545, 51)
        Me.txtRecipientCompany.Name = "txtRecipientCompany"
        Me.txtRecipientCompany.Size = New System.Drawing.Size(132, 20)
        Me.txtRecipientCompany.TabIndex = 10
        Me.txtRecipientCompany.Visible = False
        '
        'lblRecipientDetails
        '
        Me.lblRecipientDetails.Location = New System.Drawing.Point(12, 229)
        Me.lblRecipientDetails.Name = "lblRecipientDetails"
        Me.lblRecipientDetails.Size = New System.Drawing.Size(79, 13)
        Me.lblRecipientDetails.TabIndex = 11
        Me.lblRecipientDetails.Text = "Recipient Details"
        '
        'txtRecipientDetailsCompany
        '
        Me.txtRecipientDetailsCompany.EditValue = ""
        Me.txtRecipientDetailsCompany.Location = New System.Drawing.Point(75, 283)
        Me.txtRecipientDetailsCompany.Name = "txtRecipientDetailsCompany"
        Me.txtRecipientDetailsCompany.Size = New System.Drawing.Size(131, 20)
        Me.txtRecipientDetailsCompany.TabIndex = 14
        '
        'txtRecipientDetailsLast
        '
        Me.txtRecipientDetailsLast.EditValue = ""
        Me.txtRecipientDetailsLast.Location = New System.Drawing.Point(295, 257)
        Me.txtRecipientDetailsLast.Name = "txtRecipientDetailsLast"
        Me.txtRecipientDetailsLast.Size = New System.Drawing.Size(131, 20)
        Me.txtRecipientDetailsLast.TabIndex = 13
        '
        'txtRecipientDetailsFirst
        '
        Me.txtRecipientDetailsFirst.EditValue = ""
        Me.txtRecipientDetailsFirst.Location = New System.Drawing.Point(75, 257)
        Me.txtRecipientDetailsFirst.Name = "txtRecipientDetailsFirst"
        Me.txtRecipientDetailsFirst.Size = New System.Drawing.Size(131, 20)
        Me.txtRecipientDetailsFirst.TabIndex = 12
        '
        'txtRecipientAddress
        '
        Me.txtRecipientAddress.EditValue = ""
        Me.txtRecipientAddress.Location = New System.Drawing.Point(75, 309)
        Me.txtRecipientAddress.Name = "txtRecipientAddress"
        Me.txtRecipientAddress.Size = New System.Drawing.Size(351, 20)
        Me.txtRecipientAddress.TabIndex = 16
        '
        'txtRecipientDetailsDept
        '
        Me.txtRecipientDetailsDept.EditValue = ""
        Me.txtRecipientDetailsDept.Location = New System.Drawing.Point(295, 283)
        Me.txtRecipientDetailsDept.Name = "txtRecipientDetailsDept"
        Me.txtRecipientDetailsDept.Size = New System.Drawing.Size(131, 20)
        Me.txtRecipientDetailsDept.TabIndex = 15
        '
        'txtRecipientCountry
        '
        Me.txtRecipientCountry.EditValue = ""
        Me.txtRecipientCountry.Location = New System.Drawing.Point(295, 361)
        Me.txtRecipientCountry.Name = "txtRecipientCountry"
        Me.txtRecipientCountry.Size = New System.Drawing.Size(131, 20)
        Me.txtRecipientCountry.TabIndex = 20
        '
        'txtRecipientCity
        '
        Me.txtRecipientCity.EditValue = ""
        Me.txtRecipientCity.Location = New System.Drawing.Point(75, 335)
        Me.txtRecipientCity.Name = "txtRecipientCity"
        Me.txtRecipientCity.Size = New System.Drawing.Size(131, 20)
        Me.txtRecipientCity.TabIndex = 18
        '
        'txtRecipientEmail
        '
        Me.txtRecipientEmail.EditValue = ""
        Me.txtRecipientEmail.Location = New System.Drawing.Point(295, 387)
        Me.txtRecipientEmail.Name = "txtRecipientEmail"
        Me.txtRecipientEmail.Size = New System.Drawing.Size(268, 20)
        Me.txtRecipientEmail.TabIndex = 22
        '
        'txtRecipientPhone
        '
        Me.txtRecipientPhone.EditValue = ""
        Me.txtRecipientPhone.Location = New System.Drawing.Point(75, 387)
        Me.txtRecipientPhone.Name = "txtRecipientPhone"
        Me.txtRecipientPhone.Size = New System.Drawing.Size(131, 20)
        Me.txtRecipientPhone.TabIndex = 21
        '
        'btnAddRecipient
        '
        Me.btnAddRecipient.Location = New System.Drawing.Point(15, 427)
        Me.btnAddRecipient.Name = "btnAddRecipient"
        Me.btnAddRecipient.Size = New System.Drawing.Size(132, 25)
        Me.btnAddRecipient.TabIndex = 23
        Me.btnAddRecipient.Text = "Add Recipient"
        Me.btnAddRecipient.Visible = False
        '
        'btnCancelUpdateRecipient
        '
        Me.btnCancelUpdateRecipient.Location = New System.Drawing.Point(432, 427)
        Me.btnCancelUpdateRecipient.Name = "btnCancelUpdateRecipient"
        Me.btnCancelUpdateRecipient.Size = New System.Drawing.Size(131, 25)
        Me.btnCancelUpdateRecipient.TabIndex = 24
        Me.btnCancelUpdateRecipient.Text = "Cancel"
        '
        'btnUpdateRecipient
        '
        Me.btnUpdateRecipient.Location = New System.Drawing.Point(153, 427)
        Me.btnUpdateRecipient.Name = "btnUpdateRecipient"
        Me.btnUpdateRecipient.Size = New System.Drawing.Size(131, 25)
        Me.btnUpdateRecipient.TabIndex = 25
        Me.btnUpdateRecipient.Text = "Update Recipient"
        Me.btnUpdateRecipient.Visible = False
        '
        'btnRemoveRecipient
        '
        Me.btnRemoveRecipient.Location = New System.Drawing.Point(295, 427)
        Me.btnRemoveRecipient.Name = "btnRemoveRecipient"
        Me.btnRemoveRecipient.Size = New System.Drawing.Size(131, 25)
        Me.btnRemoveRecipient.TabIndex = 26
        Me.btnRemoveRecipient.Text = "Remove Recipient"
        Me.btnRemoveRecipient.Visible = False
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(699, 48)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(131, 23)
        Me.btnSearch.TabIndex = 29
        Me.btnSearch.Text = "Search"
        Me.btnSearch.Visible = False
        '
        'txtRecipientState
        '
        Me.txtRecipientState.EditValue = ""
        Me.txtRecipientState.Location = New System.Drawing.Point(295, 335)
        Me.txtRecipientState.Name = "txtRecipientState"
        Me.txtRecipientState.Size = New System.Drawing.Size(131, 20)
        Me.txtRecipientState.TabIndex = 32
        '
        'txtPostalCode
        '
        Me.txtPostalCode.EditValue = ""
        Me.txtPostalCode.Location = New System.Drawing.Point(75, 361)
        Me.txtPostalCode.Name = "txtPostalCode"
        Me.txtPostalCode.Size = New System.Drawing.Size(131, 20)
        Me.txtPostalCode.TabIndex = 33
        '
        'lblRecipientDept
        '
        Me.lblRecipientDept.Location = New System.Drawing.Point(232, 286)
        Me.lblRecipientDept.Name = "lblRecipientDept"
        Me.lblRecipientDept.Size = New System.Drawing.Size(57, 13)
        Me.lblRecipientDept.TabIndex = 36
        Me.lblRecipientDept.Text = "Department"
        '
        'lblSearchCompany
        '
        Me.lblSearchCompany.Location = New System.Drawing.Point(494, 54)
        Me.lblSearchCompany.Name = "lblSearchCompany"
        Me.lblSearchCompany.Size = New System.Drawing.Size(45, 13)
        Me.lblSearchCompany.TabIndex = 37
        Me.lblSearchCompany.Text = "Company"
        Me.lblSearchCompany.Visible = False
        '
        'lblSearchLastName
        '
        Me.lblSearchLastName.Location = New System.Drawing.Point(295, 54)
        Me.lblSearchLastName.Name = "lblSearchLastName"
        Me.lblSearchLastName.Size = New System.Drawing.Size(46, 13)
        Me.lblSearchLastName.TabIndex = 38
        Me.lblSearchLastName.Text = "Lastname"
        Me.lblSearchLastName.Visible = False
        '
        'lblSearchFirstName
        '
        Me.lblSearchFirstName.Location = New System.Drawing.Point(100, 54)
        Me.lblSearchFirstName.Name = "lblSearchFirstName"
        Me.lblSearchFirstName.Size = New System.Drawing.Size(47, 13)
        Me.lblSearchFirstName.TabIndex = 39
        Me.lblSearchFirstName.Text = "Firstname"
        Me.lblSearchFirstName.Visible = False
        '
        'lblRecipientFirstName
        '
        Me.lblRecipientFirstName.Location = New System.Drawing.Point(12, 260)
        Me.lblRecipientFirstName.Name = "lblRecipientFirstName"
        Me.lblRecipientFirstName.Size = New System.Drawing.Size(47, 13)
        Me.lblRecipientFirstName.TabIndex = 40
        Me.lblRecipientFirstName.Text = "Firstname"
        '
        'lblRecipientLastName
        '
        Me.lblRecipientLastName.Location = New System.Drawing.Point(232, 260)
        Me.lblRecipientLastName.Name = "lblRecipientLastName"
        Me.lblRecipientLastName.Size = New System.Drawing.Size(46, 13)
        Me.lblRecipientLastName.TabIndex = 41
        Me.lblRecipientLastName.Text = "Lastname"
        '
        'lblRecipientCompany
        '
        Me.lblRecipientCompany.Location = New System.Drawing.Point(12, 286)
        Me.lblRecipientCompany.Name = "lblRecipientCompany"
        Me.lblRecipientCompany.Size = New System.Drawing.Size(45, 13)
        Me.lblRecipientCompany.TabIndex = 44
        Me.lblRecipientCompany.Text = "Company"
        '
        'lblRecipientEmail
        '
        Me.lblRecipientEmail.Location = New System.Drawing.Point(232, 390)
        Me.lblRecipientEmail.Name = "lblRecipientEmail"
        Me.lblRecipientEmail.Size = New System.Drawing.Size(24, 13)
        Me.lblRecipientEmail.TabIndex = 45
        Me.lblRecipientEmail.Text = "Email"
        '
        'lblRecipientPhone
        '
        Me.lblRecipientPhone.Location = New System.Drawing.Point(12, 390)
        Me.lblRecipientPhone.Name = "lblRecipientPhone"
        Me.lblRecipientPhone.Size = New System.Drawing.Size(30, 13)
        Me.lblRecipientPhone.TabIndex = 46
        Me.lblRecipientPhone.Text = "Phone"
        '
        'lblRecipientCountry
        '
        Me.lblRecipientCountry.Location = New System.Drawing.Point(232, 364)
        Me.lblRecipientCountry.Name = "lblRecipientCountry"
        Me.lblRecipientCountry.Size = New System.Drawing.Size(39, 13)
        Me.lblRecipientCountry.TabIndex = 47
        Me.lblRecipientCountry.Text = "Country"
        '
        'lblRecipientPostalCode
        '
        Me.lblRecipientPostalCode.Location = New System.Drawing.Point(12, 364)
        Me.lblRecipientPostalCode.Name = "lblRecipientPostalCode"
        Me.lblRecipientPostalCode.Size = New System.Drawing.Size(57, 13)
        Me.lblRecipientPostalCode.TabIndex = 48
        Me.lblRecipientPostalCode.Text = "Postal Code"
        '
        'lblRecipientState
        '
        Me.lblRecipientState.Location = New System.Drawing.Point(232, 338)
        Me.lblRecipientState.Name = "lblRecipientState"
        Me.lblRecipientState.Size = New System.Drawing.Size(26, 13)
        Me.lblRecipientState.TabIndex = 49
        Me.lblRecipientState.Text = "State"
        '
        'lblRecipientCity
        '
        Me.lblRecipientCity.Location = New System.Drawing.Point(12, 338)
        Me.lblRecipientCity.Name = "lblRecipientCity"
        Me.lblRecipientCity.Size = New System.Drawing.Size(19, 13)
        Me.lblRecipientCity.TabIndex = 50
        Me.lblRecipientCity.Text = "City"
        '
        'lblRecipientAddress
        '
        Me.lblRecipientAddress.Location = New System.Drawing.Point(12, 312)
        Me.lblRecipientAddress.Name = "lblRecipientAddress"
        Me.lblRecipientAddress.Size = New System.Drawing.Size(39, 13)
        Me.lblRecipientAddress.TabIndex = 51
        Me.lblRecipientAddress.Text = "Address"
        '
        'btnSelectRecipient
        '
        Me.btnSelectRecipient.Location = New System.Drawing.Point(569, 427)
        Me.btnSelectRecipient.Name = "btnSelectRecipient"
        Me.btnSelectRecipient.Size = New System.Drawing.Size(131, 25)
        Me.btnSelectRecipient.TabIndex = 54
        Me.btnSelectRecipient.Text = "Select Recipient"
        '
        'RecipientInfo
        '
        Me.AcceptButton = Me.btnSearch
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(842, 491)
        Me.Controls.Add(Me.btnSelectRecipient)
        Me.Controls.Add(Me.lblRecipientAddress)
        Me.Controls.Add(Me.lblRecipientCity)
        Me.Controls.Add(Me.lblRecipientState)
        Me.Controls.Add(Me.lblRecipientPostalCode)
        Me.Controls.Add(Me.lblRecipientCountry)
        Me.Controls.Add(Me.lblRecipientPhone)
        Me.Controls.Add(Me.lblRecipientEmail)
        Me.Controls.Add(Me.lblRecipientCompany)
        Me.Controls.Add(Me.lblRecipientLastName)
        Me.Controls.Add(Me.lblRecipientFirstName)
        Me.Controls.Add(Me.lblSearchFirstName)
        Me.Controls.Add(Me.lblSearchLastName)
        Me.Controls.Add(Me.lblSearchCompany)
        Me.Controls.Add(Me.lblRecipientDept)
        Me.Controls.Add(Me.txtPostalCode)
        Me.Controls.Add(Me.txtRecipientState)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.btnRemoveRecipient)
        Me.Controls.Add(Me.btnUpdateRecipient)
        Me.Controls.Add(Me.btnCancelUpdateRecipient)
        Me.Controls.Add(Me.btnAddRecipient)
        Me.Controls.Add(Me.txtRecipientEmail)
        Me.Controls.Add(Me.txtRecipientPhone)
        Me.Controls.Add(Me.txtRecipientCountry)
        Me.Controls.Add(Me.txtRecipientCity)
        Me.Controls.Add(Me.txtRecipientAddress)
        Me.Controls.Add(Me.txtRecipientDetailsDept)
        Me.Controls.Add(Me.txtRecipientDetailsCompany)
        Me.Controls.Add(Me.txtRecipientDetailsLast)
        Me.Controls.Add(Me.txtRecipientDetailsFirst)
        Me.Controls.Add(Me.lblRecipientDetails)
        Me.Controls.Add(Me.txtRecipientCompany)
        Me.Controls.Add(Me.txtRecipientLastName)
        Me.Controls.Add(Me.lblPeopleSearch)
        Me.Controls.Add(Me.txtRecipientFirstName)
        Me.Controls.Add(Me.grdRecipientInfo)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "RecipientInfo"
        Me.Ribbon = Me.RibbonControl
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "Recipient Information"
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdRecipientInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewRecipientInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientFirstName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientLastName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientCompany.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientDetailsCompany.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientDetailsLast.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientDetailsFirst.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientDetailsDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientCountry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientCity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientPhone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecipientState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPostalCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents grdRecipientInfo As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GrdViewRecipientInfo As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colRECIPIENT_FIRSTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENT_LASTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENT_COMPANY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENT_DEPARTMENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENT_COUNTRY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtRecipientFirstName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPeopleSearch As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtRecipientLastName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientCompany As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblRecipientDetails As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtRecipientDetailsCompany As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientDetailsLast As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientDetailsFirst As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientDetailsDept As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientCountry As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientCity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRecipientPhone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnAddRecipient As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancelUpdateRecipient As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnUpdateRecipient As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveRecipient As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSearch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colRECIPIENT_ADDRESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENT_CITY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENT_POSTALCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENT_PHONE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENT_EMAIL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENT_PERSONTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtRecipientState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPostalCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colRECIPIENT_PROVINCE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblRecipientDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSearchCompany As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSearchLastName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSearchFirstName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblRecipientFirstName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblRecipientLastName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblRecipientCompany As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblRecipientEmail As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblRecipientPhone As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblRecipientCountry As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblRecipientPostalCode As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblRecipientState As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblRecipientCity As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblRecipientAddress As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnSelectRecipient As DevExpress.XtraEditors.SimpleButton


End Class
