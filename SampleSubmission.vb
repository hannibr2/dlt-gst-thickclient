﻿Public Class SampleSubmission
    Private m_Barcode As String

    Public Property Barcode As String
        Get
            Return m_Barcode
        End Get
        Set(value As String)
            If m_Barcode = value Then
                Return
            End If
            m_Barcode = value
        End Set
    End Property



    Public Sub New(ByVal Barcode As String)
        m_Barcode = Barcode
    End Sub
End Class
