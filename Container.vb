﻿Public Class Container

#Region "           Declarations            "

    Private m_container_id As Decimal
    Private m_parent_container_id As Decimal
    Private m_type_id As Decimal
    Private m_type_name As String = String.Empty
    Private m_origin_id As Decimal
    Private m_barcode As String = String.Empty
    Private m_sample_name As String = String.Empty
    Private m_ranking As Integer
    Private m_container_count As Integer
    Private m_phase_id As Integer
    Private m_phase_name As String = String.Empty
    Private m_note As String = String.Empty
    Private m_receiver_id As Decimal
    Private m_last_name As String = String.Empty
    Private m_first_name As String = String.Empty
    Private m_account As String = String.Empty
    Private m_mail_address As String = String.Empty
    Private m_site_code As String = String.Empty
    Private m_city As String = String.Empty
    Private m_country_code As String = String.Empty
    Private m_country_name As String = String.Empty
    Private m_email As String = String.Empty
    Private m_order_name As String = String.Empty
    Private m_order_session_name As String = String.Empty
    Private m_mta_number As String
    Private m_has_attachment As String
    Private m_delivery_form_type_name As String = String.Empty
    Private m_factory_service As String
    Private m_protocol_group_name As String
    Private m_protocol_name As String

    Private m_dest_rack_group_name As String

    Private m_containers As Containers


#End Region

#Region "           Properties              "

    Public Property ContainerID() As Decimal
        Get
            Return m_container_id
        End Get
        Set(ByVal value As Decimal)
            If m_container_id = value Then
                Return
            End If
            m_container_id = value
        End Set
    End Property
    Public Property ParentContainerID() As Decimal
        Get
            Return m_parent_container_id
        End Get
        Set(ByVal value As Decimal)
            If m_parent_container_id = value Then
                Return
            End If
            m_parent_container_id = value
        End Set
    End Property
    Public WriteOnly Property TypeID() As Decimal
        'Get
        '    Return m_type_id
        'End Get
        Set(ByVal value As Decimal)
            If m_type_id = value Then
                Return
            End If
            m_type_id = value
        End Set
    End Property
    Public WriteOnly Property TypeName() As String
        'Get
        '    Return m_type_name
        'End Get
        Set(ByVal value As String)
            If m_type_name = value Then
                Return
            End If
            m_type_name = value
        End Set
    End Property
    Public WriteOnly Property OriginID() As Decimal
        'Get
        '    Return m_origin_id
        'End Get
        Set(ByVal value As Decimal)
            If m_origin_id = value Then
                Return
            End If
            m_origin_id = value
        End Set
    End Property
    Public Property Barcode() As String
        Get
            Return m_barcode
        End Get
        Set(ByVal value As String)
            If m_barcode = value Then
                Return
            End If
            m_barcode = value
        End Set
    End Property
    Public WriteOnly Property SampleName() As String
        'Get
        '    Return m_sample_name
        'End Get
        Set(ByVal value As String)
            If m_sample_name = value Then
                Return
            End If
            m_sample_name = value
        End Set
    End Property
    Public Property Ranking() As Integer
        Get
            Return m_ranking
        End Get
        Set(ByVal value As Integer)
            If m_ranking = value Then
                Return
            End If
            m_ranking = value
        End Set
    End Property
    Public WriteOnly Property ContainerCount() As Integer
        'Get
        '    Return m_container_count
        'End Get
        Set(ByVal value As Integer)
            If m_container_count = value Then
                Return
            End If
            m_container_count = value
        End Set
    End Property
    Public WriteOnly Property PhaseID() As Integer
        'Get
        '    Return m_phase_id
        'End Get
        Set(ByVal value As Integer)
            If m_phase_id = value Then
                Return
            End If
            m_phase_id = value
        End Set
    End Property
    Public WriteOnly Property PhaseName() As String
        'Get
        '    Return m_phase_name
        'End Get
        Set(ByVal value As String)
            If m_phase_name = value Then
                Return
            End If
            m_phase_name = value
        End Set
    End Property
    Public WriteOnly Property Note() As String
        'Get
        '    Return m_note
        'End Get
        Set(ByVal value As String)
            If m_note = value Then
                Return
            End If
            m_note = value
        End Set
    End Property
    Public WriteOnly Property ReceiverID() As Decimal
        'Get
        '    Return m_receiver_id
        'End Get
        Set(ByVal value As Decimal)
            If m_receiver_id = value Then
                Return
            End If
            m_receiver_id = value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return m_last_name
        End Get
        Set(ByVal value As String)
            If m_last_name = value Then
                Return
            End If
            m_last_name = value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return m_first_name
        End Get
        Set(ByVal value As String)
            If m_first_name = value Then
                Return
            End If
            m_first_name = value
        End Set
    End Property
    Public WriteOnly Property Account() As String
        'Get
        '    Return m_account
        'End Get
        Set(ByVal value As String)
            If m_account = value Then
                Return
            End If
            m_account = value
        End Set
    End Property
    Public Property MailAddress() As String
        Get
            Return m_mail_address
        End Get
        Set(ByVal value As String)
            If m_mail_address = value Then
                Return
            End If
            m_mail_address = value
        End Set
    End Property
    Public Property SiteCode() As String
        Get
            Return m_site_code
        End Get
        Set(ByVal value As String)
            If m_site_code = value Then
                Return
            End If
            m_site_code = value
        End Set
    End Property
    Public WriteOnly Property City() As String
        'Get
        '    Return m_city
        'End Get
        Set(ByVal value As String)
            If m_city = value Then
                Return
            End If
            m_city = value
        End Set
    End Property
    Public WriteOnly Property CountryCode() As String
        'Get
        '    Return m_country_code
        'End Get
        Set(ByVal value As String)
            If m_country_code = value Then
                Return
            End If
            m_country_code = value
        End Set
    End Property
    Public WriteOnly Property CountryName() As String
        'Get
        '    Return m_country_name
        'End Get
        Set(ByVal value As String)
            If m_country_name = value Then
                Return
            End If
            m_country_name = value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return m_email
        End Get
        Set(ByVal value As String)
            If m_email = value Then
                Return
            End If
            m_email = value
        End Set
    End Property
    Public Property OrderName() As String
        Get
            Return m_order_name
        End Get
        Set(ByVal value As String)
            If m_order_name = value Then
                Return
            End If
            m_order_name = value
        End Set
    End Property
    Public WriteOnly Property OrderSessionName() As String
        'Get
        '    Return m_order_session_name
        'End Get
        Set(ByVal value As String)
            If m_order_session_name = value Then
                Return
            End If
            m_order_session_name = value
        End Set
    End Property
    Public WriteOnly Property MTANumber() As String
        'Get
        '    Return m_mta_number
        'End Get
        Set(ByVal value As String)
            If m_mta_number = value Then
                Return
            End If
            m_mta_number = value
        End Set
    End Property
    Public ReadOnly Property HasAttachments() As Boolean
        Get
            HasAttachments = False
            If Not String.IsNullOrEmpty(m_has_attachment) Then

                If String.Compare(m_has_attachment, "TRUE", True) = 0 Then
                    HasAttachments = True
                Else
                    HasAttachments = False
                End If
            End If
            Return HasAttachments
        End Get
        'Set(ByVal value As Boolean)
        '    If m_has_attachment = value Then
        '        Return
        '    End If
        '    m_has_attachment = value
        'End Set
    End Property
    Public Property DeliveryFormTypeName() As String
        Get
            Return m_delivery_form_type_name
        End Get
        Set(ByVal value As String)
            If m_delivery_form_type_name = value Then
                Return
            End If
            m_delivery_form_type_name = value
        End Set
    End Property
    Public WriteOnly Property FactoryService() As String
        'Get
        '    Return m_factory_service
        'End Get
        Set(ByVal value As String)
            If m_factory_service = value Then
                Return
            End If
            m_factory_service = value
        End Set
    End Property
    Public WriteOnly Property ProtocolGroupName() As String
        'Get
        '    Return m_protocol_group_name
        'End Get
        Set(ByVal value As String)
            If m_protocol_group_name = value Then
                Return
            End If
            m_protocol_group_name = value
        End Set
    End Property
    Public Property ProtocolName() As String
        Get
            Return m_protocol_name
        End Get
        Set(ByVal value As String)
            If m_protocol_name = value Then
                Return
            End If
            m_protocol_name = value
        End Set
    End Property

    Public Property DestRackGroupName As String
        Get
            Return m_dest_rack_group_name
        End Get
        Set(value As String)
            If m_dest_rack_group_name = value Then
                Return
            End If
            m_dest_rack_group_name = value
        End Set
    End Property


#End Region

#Region "           Constructors            "

    Public Sub New(ByVal ItemRow As DataRow)
        ContainerID = ItemRow.Item("container_id")
        ParentContainerID = ItemRow.Item("parent_container_id")
        TypeID = ItemRow.Item("type_id")
        TypeName = ItemRow.Item("type_name").ToString()

        OriginID = ItemRow.Item("origin_id")
        Barcode = ItemRow.Item("barcode").ToString()
        SampleName = ItemRow.Item("sample_name").ToString()
        Ranking = ItemRow.Item("ranking")
        ContainerCount = ItemRow.Item("container_count")
        PhaseID = ItemRow.Item("phase_id")
        PhaseName = ItemRow.Item("phase_name").ToString()
        'Note = ItemRow.Item("note")
        ReceiverID = ItemRow.Item("receiver_id")
        LastName = ItemRow.Item("last_name").ToString()
        FirstName = ItemRow.Item("first_name").ToString()
        Account = ItemRow.Item("account").ToString()
        MailAddress = ItemRow.Item("mail_address").ToString()
        SiteCode = ItemRow.Item("site_code").ToString()
        City = ItemRow.Item("city").ToString()
        CountryCode = ItemRow.Item("country_code").ToString()
        CountryName = ItemRow.Item("country_name").ToString()
        Email = ItemRow.Item("email").ToString()
        OrderName = ItemRow.Item("order_name").ToString()
        OrderSessionName = ItemRow.Item("order_session_name").ToString()
        'MTANumber = ItemRow.Item("mta_number")
        m_has_attachment = ItemRow.Item("has_attachment").ToString
        DeliveryFormTypeName = ItemRow.Item("delivery_form_type_name").ToString()
        FactoryService = ItemRow.Item("factory_service").ToString()
        ProtocolGroupName = ItemRow.Item("protocol_group_name").ToString()
        ProtocolName = ItemRow.Item("protocol_name").ToString()
        DestRackGroupName = ItemRow.Item("destination_rack_group_name").ToString()
    End Sub

    Public Sub New()

    End Sub


#End Region

End Class
