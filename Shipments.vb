﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel

Public Class Shipments
    Inherits Collection(Of Shipment)

    Public Sub New(ByVal Shipments As DataTable)
        For Each Row As DataRow In Shipments.Rows
            Me.Add(New Shipment(Row))
        Next
    End Sub

    Public Sub New()

    End Sub

    Public Sub New(ByVal list As IList(Of Shipment))
        MyBase.New(list)

    End Sub

End Class
