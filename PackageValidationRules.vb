﻿Public Class PackageValidationRules

    Private m_bHasBaseRec As Boolean = False

    Private m_bSite As Boolean
    Private m_sSiteValue As String
    Private m_sSiteColumn As String = "SITE_CODE"


    Private m_bDestination As Boolean
    Private m_sDestinationValue As String
    Private m_sDestinationColumn As String = "MAIL_ADDRESS"
    
    Private m_bRecipient As Boolean
    Private m_sRecipientValue As String
    Private m_sRecipientColumn As String = "EMAIL"

    Private m_bContentType As Boolean
    Private m_sContentTypeValue As String
    Private m_sContentTypeColumn As String = "FACTORY_SERVICE"

    Private m_bRequestNumber As Boolean
    Private m_sRequestNumberValue As String
    Private m_sRequestNumberColumn As String = "ORDER_NAME"

    Private m_bDeliveryForm As Boolean
    Private m_sDeliveryFormValue As String
    Private m_sDeliveryFormColumn As String = "DELIVERY_FORM_TYPE_NAME"

    Private m_bProtocol As Boolean
    Private m_sProtocolValue As String
    Private m_sProtocolColumn As String = "PROTOCOL_NAME"

    Private m_bDestRackGroupName As Boolean
    Private m_sDestRackGroupNameValue As String
    Private m_sDestRackGroupNameColumn As String = "DESTINATION_RACK_GROUP_NAME"

    Private Const m_iProtocolRuleID As Integer = 1
    Private Const m_iRecipientRuleID As Integer = 2
    Private Const m_iSiteRuleID As Integer = 3
    Private Const m_iDestinationRuleID As Integer = 4
    Private Const m_iRequestNumberRuleID As Integer = 5
    Private Const m_iDeliveryFormRuleID As Integer = 6
    Private Const m_iDestRackGroupNameRuleID As Integer = 7




    Public Property HasBase() As Boolean
        Get
            Return m_bHasBaseRec
        End Get
        Set(ByVal value As Boolean)
            m_bHasBaseRec = value
        End Set
    End Property

    Public Property Site() As Boolean
        Get
            Return m_bSite
        End Get
        Set(ByVal value As Boolean)
            m_bSite = value
            If value Then
                m_sSiteValue = String.Empty
            End If
        End Set
    End Property

    Public Property SiteValue() As String
        Get
            Return m_sSiteValue
        End Get
        Set(ByVal value As String)
            m_sSiteValue = value
        End Set
    End Property

    Public ReadOnly Property SiteColumn() As String
        Get
            Return m_sSiteColumn
        End Get
    End Property



    Public Property Destination() As Boolean
        Get
            Return m_bDestination
        End Get
        Set(ByVal value As Boolean)
            m_bDestination = value
            If value Then
                m_sDestinationValue = String.Empty
            End If
        End Set
    End Property

    Public Property DestinationValue() As String
        Get
            Return m_sDestinationValue
        End Get
        Set(ByVal value As String)
            m_sDestinationValue = value
        End Set
    End Property

    Public ReadOnly Property DestinationColumn() As String
        Get
            Return m_sDestinationColumn
        End Get
    End Property


    Public Property Recipient() As Boolean
        Get
            Return m_bRecipient
        End Get
        Set(ByVal value As Boolean)
            m_bRecipient = value
            If Not value Then
                m_sRecipientValue = String.Empty
            End If
        End Set
    End Property

    Public Property RecipientValue() As String
        Get
            Return m_sRecipientValue
        End Get
        Set(ByVal value As String)
            m_sRecipientValue = value
        End Set
    End Property

    Public ReadOnly Property RecipientColumn() As String
        Get
            Return m_sRecipientColumn
        End Get
    End Property


    Public Property RequestNumber() As Boolean
        Get
            Return m_bRequestNumber
        End Get
        Set(ByVal value As Boolean)
            m_bRequestNumber = value
            If Not value Then
                m_sRequestNumberValue = String.Empty
            End If
        End Set
    End Property

    Public Property RequestNumberValue() As String
        Get
            Return m_sRequestNumberValue
        End Get
        Set(ByVal value As String)
            m_sRequestNumberValue = value
        End Set
    End Property

    Public ReadOnly Property RequestNumberColumn() As String
        Get
            Return m_sRequestNumberColumn
        End Get
    End Property

    Public Property DeliveryForm() As Boolean
        Get
            Return m_bDeliveryForm
        End Get
        Set(ByVal value As Boolean)
            m_bDeliveryForm = value
            If Not value Then
                m_sDeliveryFormValue = String.Empty
            End If
        End Set
    End Property

    Public Property DeliveryFormValue() As String
        Get
            Return m_sDeliveryFormValue
        End Get
        Set(ByVal value As String)
            m_sDeliveryFormValue = value
        End Set
    End Property

    Public ReadOnly Property DeliveryFormColumn() As String
        Get
            Return m_sDeliveryFormColumn
        End Get
    End Property

    Public Property Protocol() As Boolean
        Get
            Return m_bProtocol
        End Get
        Set(ByVal value As Boolean)
            m_bProtocol = value
            If Not value Then
                m_sProtocolValue = String.Empty
            End If
        End Set
    End Property

    Public Property ProtocolValue() As String
        Get
            Return m_sProtocolValue
        End Get
        Set(ByVal value As String)
            m_sProtocolValue = value
        End Set
    End Property

    Public ReadOnly Property ProtocolColumn() As String
        Get
            Return m_sProtocolColumn
        End Get
    End Property


    Public Property DestRackGroupName As Boolean
        Get
            Return m_bDestRackGroupName
        End Get
        Set(value As Boolean)
            If m_bDestRackGroupName = value Then
                Return
            End If
            m_bDestRackGroupName = value
        End Set
    End Property

    Public Property DestRackGroupNameValue As String
        Get
            Return m_sDestRackGroupNameValue
        End Get
        Set(value As String)
            If m_sDestRackGroupNameValue = value Then
                Return
            End If
            m_sDestRackGroupNameValue = value
        End Set
    End Property

    Public ReadOnly Property DestRackGroupNameColumn As String
        Get
            Return m_sDestRackGroupNameColumn
        End Get
    End Property



    Public ReadOnly Property Timeframe() As Integer
        Get
            Return frmMain.lkPendingTimeframe.EditValue
        End Get
    End Property


    Public ReadOnly Property ProtocolRuleID As Integer
        Get
            Return m_iProtocolRuleID
        End Get
    End Property

    Public ReadOnly Property RecipientRuleID As Integer
        Get
            Return m_iRecipientRuleID
        End Get
    End Property

    Public ReadOnly Property SiteRuleID As Integer
        Get
            Return m_iSiteRuleID
        End Get

    End Property
    Public ReadOnly Property DestinationRuleID As Integer
        Get
            Return m_iDestinationRuleID
        End Get

    End Property
    Public ReadOnly Property RequestNumberRuleID As Integer
        Get
            Return m_iRequestNumberRuleID
        End Get

    End Property
    Public ReadOnly Property DeliveryFormRuleID As Integer
        Get
            Return m_iDeliveryFormRuleID
        End Get

    End Property
    Public ReadOnly Property DestRackGroupNameRuleID As Integer
        Get
            Return m_iDestRackGroupNameRuleID
        End Get
    End Property

End Class
