﻿Namespace NibrIAMAuth

    Public Class IAMAccessDeniedException
        Inherits IAMException

        Public Sub New()
            MyBase.New("Access denied")
        End Sub


        Public Sub New(ByVal Message As String, ByVal InnerException As Exception)
            MyBase.New(Message, InnerException)
        End Sub


        Public Sub New(ByVal InnerException As Exception)
            MyBase.New("Access denied", InnerException)
        End Sub

    End Class

End Namespace
