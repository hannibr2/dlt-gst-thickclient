﻿Imports System
Imports System.Net

Imports System.Collections.Generic

Imports System.Runtime.CompilerServices

Imports System.Xml
Imports System.IO
Imports System.Text



Namespace NibrIAMAuth

    Module NibrIAMAuth

        Dim IAMUrl As String = "http://nebulacdn.na.novartis.net/sec/util/auth.php"
        'Dim Domain As String = "nanet"

        Dim InitHash As IDictionary(Of String, CookieFactory) = New Dictionary(Of String, CookieFactory)()
        Dim _CookieLifeTime As Integer = 86400000 ' 1 day in ms

        <Extension()> _
        Public Function GetAuthResponse(ByVal ExtendedWebRequest As HttpWebRequest) As WebResponse
            Try
                Dim RequestHost As String = ExtendedWebRequest.RequestUri.Authority

                If Not IsInitialized(RequestHost) Then
                    InitIAM(ExtendedWebRequest.RequestUri)
                End If
                Dim cF As CookieFactory = InitHash(RequestHost)

                If ExtendedWebRequest.CookieContainer Is Nothing Then
                    ExtendedWebRequest.CookieContainer = New CookieContainer
                    ExtendedWebRequest.CookieContainer.Add(cF.IAMCookie)
                Else
                    ExtendedWebRequest.CookieContainer.Add(cF.IAMCookie)
                End If
                Return ExtendedWebRequest.GetResponse
            Catch ex As WebException
                Throw New Exception("There was a loss of connectivity with server. Please retry last invoked activity.", ex)
            End Try
        End Function

        Public Function GetAuthRequestStream(ByVal extendedWebRequest As HttpWebRequest) As System.IO.Stream
            Try
                Dim RequestHost As String = extendedWebRequest.RequestUri.Authority

                If Not IsInitialized(RequestHost) Then
                    InitIAM(extendedWebRequest.RequestUri)
                End If

                Dim cF As CookieFactory = InitHash(RequestHost)

                If extendedWebRequest.CookieContainer Is Nothing Then
                    extendedWebRequest.CookieContainer = New CookieContainer
                    extendedWebRequest.CookieContainer.Add(cF.IAMCookie)
                Else
                    extendedWebRequest.CookieContainer.Add(cF.IAMCookie)
                End If
                Return extendedWebRequest.GetRequestStream
            Catch ex As WebException
                Throw New Exception("There was a loss of connectivity with server. Please retry last invoked activity.", ex)
            End Try
        End Function

        Private Function IsInitializedPassword(ByVal requestUri As Uri) As Boolean

            If (InitHash.ContainsKey(requestUri.Host)) Then
                Return CType(InitHash(requestUri.Host), CookieFactory).isNTLM
            Else
                Return False
            End If

        End Function

        Private Sub ClearCredentials(ByVal RequestUri As Uri)
            If IsInitializedPassword(RequestUri) Then
                CType(InitHash(RequestUri.Host), CookieFactory).NTLMOff()
            End If
        End Sub

        Private Function IsInitialized(ByVal RequestHost As String) As Boolean
            Return InitHash.ContainsKey(RequestHost)
        End Function

        Private Function IsInitialized(ByVal Uri As Uri) As Boolean
            Return InitHash.ContainsKey(Uri.Authority)
        End Function

        Public Sub InitIAM(ByVal RequestUri As Uri)
            Dim MyCookieFactory As CookieFactory = New CookieFactory(RequestUri, _CookieLifeTime)
            MyCookieFactory.GetAuthCookie()
            InitHash(RequestUri.Authority) = MyCookieFactory
        End Sub

        Public Sub InitIAM(ByVal RequestUri As Uri, ByVal Username As String, ByVal Password As String, ByVal Domain As String)
            Dim MyCookieFactory As CookieFactory = New CookieFactory(RequestUri, Username, Password, Domain, _CookieLifeTime)
            MyCookieFactory.GetAuthCookie()
            InitHash(RequestUri.Authority) = MyCookieFactory
        End Sub

        Public Function GetUser521(ByVal Username As String, ByVal Password As String, ByVal Domain As String) As String
            'Dim IAMUrl As String = "http://nebulacdn.na.novartis.net/sec/util/auth.php"
            'Dim Domain As String = "nanet"

            Dim ReturnString As String = String.Empty
            Dim Req As HttpWebRequest = CType(HttpWebRequest.Create(IAMUrl), HttpWebRequest)
            Req.Method = "POST"

            Try
                InitIAM(New Uri(IAMUrl), Username, Password, Domain)

                Dim res As HttpWebResponse = CType(Req.GetAuthResponse(), HttpWebResponse)
                Dim resCont As Stream = res.GetResponseStream()
                Dim tempString As String = String.Empty
                Dim count As Integer = 0
                Dim buf(8192) As Byte

                Dim sb As StringBuilder = New StringBuilder()

                Using reader As XmlReader = XmlReader.Create(resCont)
                    reader.ReadToFollowing("userid")
                    ReturnString = reader.ReadElementContentAsString()
                End Using

                resCont.Close()
                res.Close()
            Catch ex As Exception
                ReturnString = "Error"
            End Try
            Return ReturnString
        End Function

        Public Function LogoutUser() As Boolean
            Try
                ClearCredentials(New Uri(IAMUrl))
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

    End Module


End Namespace
