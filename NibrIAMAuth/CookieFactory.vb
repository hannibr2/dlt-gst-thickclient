﻿Imports System
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Timers


Namespace NibrIAMAuth
    Public Class CookieFactory
        Inherits Timer

#Region "       Declarations        "
        Private _IAMAuthUrl As String = "http://web.global.nibr.novartis.net/sts/getToken" '"http://iam.global.nibr.novartis.net"
        Private _IAMPasswordUrl As String = "http://web.global.nibr.novartis.net/sts/getToken" '"http://castsprod.nibr.novartis.net/test.asp"

        Private _IAMCookie As Cookie
        Private _preFetchTime As Integer = 3600000
        Private _staticTime As Boolean = False
        Private _isValid As Boolean = False
        Private usingNTLM As Boolean = False
        Private username As String = ""
        Private password As String = ""
        Private domain As String = ""

        'Original
        'http://iamprdcluster.eu.novartis.net:80/obrareq.cgi?wh%3Dnrusca-s4235%20wu%3D%2Ftest.asp%20wo%3D1%20rh%3Dhttp%3A%2F%2Fcastsprod.nibr.novartis.net%20ru%3D%252Ftest.asp
        'Private _IAMAuthUrl As String = "http://iamprodcluster.nibr.novartis.net/"
        'Private _IAMPasswordUrl As String = "http://castsprod.nibr.novartis.net/test.asp"

        'Private _IAMAuthUrl As String = "http://iamprdcluster.eu.nibr.novartis.net/"
        '"http://iamprodcluster.nibr.novartis.net/"
        'Private _IAMEuAuthUrl As String = "http://iamprdcluster.eu.nibr.novartis.net/"
        'Private _IAMPasswordUrl As String = "http://castsprod.nibr.novartis.net/test.asp"

        'Present
        'Private _IAMAuthUrl As String = "http://castsprodlinux.na.novartis.net/tokenService.php"
        'Private _IAMPasswordUrl As String = "http://nrusca-s4235/test.asp"

        'To be used soon
        'Private _IAMAuthUrl As String = "http://web.global.nibr.novartis.net/sts/checkToken"
        'Private _IAMPasswordUrl As String = "http://web.global.nibr.novartis.net/sts/getToken"


        'Private _IAMEuAuthUrl As String = "http://iamprdcluster.eu.nibr.novartis.net/"
        'Private _IAMPasswordUrl As String = "http://castsprod.nibr.novartis.net/test.asp"
        Private _IAMCookieName As String = "ObSSOCookie"
        Private _localIPList As IPAddress() = Dns.GetHostAddresses(System.Net.Dns.GetHostName())

        Private _BaseUri As Uri
#End Region

#Region "       Properties      "


        Public Property BaseUri() As Uri
            Get
                Return _BaseUri
            End Get
            Set(ByVal value As Uri)
                _BaseUri = value
            End Set
        End Property

        Public Property IAMCookie() As Cookie
            Get
                If _isValid Then
                    Return _IAMCookie
                Else
                    GetAuthCookie()
                    Return _IAMCookie
                End If
            End Get
            Set(ByVal value As Cookie)
                _IAMCookie = value
                _isValid = True
                If (_staticTime) Then
                    MyBase.Interval = _preFetchTime
                Else
                    Dim cookieTTL As Integer = Int(IAMCookie.Expires.Subtract(System.DateTime.Now).Ticks)
                    Dim tmpinterval As Integer = cookieTTL - _preFetchTime
                    If (tmpinterval - _preFetchTime >= 0) Then
                        MyBase.Interval = tmpinterval - _preFetchTime
                    Else
                        MyBase.Interval = _preFetchTime
                    End If
                End If
            End Set
        End Property

        Public ReadOnly Property isNTLM() As Boolean
            Get
                Return usingNTLM
            End Get
        End Property

#End Region

#Region "       Constructors        "
        Public Sub New(ByVal BaseHost As Uri)

            'NetworkChange.NetworkAddressChanged += new NetworkAddressChangedEventHandler(NetworkChange_NetworkAddressChanged);
            
            Me.BaseUri = BaseHost
            MyBase.AutoReset = True
            GetAuthCookie()
            'MyBase.Elapsed += New ElapsedEventHandler(InvalidateCookie)
            AddHandler MyBase.Elapsed, AddressOf InvalidateCookie
            MyBase.Start()
            MyBase.Enabled = True



        End Sub

        Public Sub New(ByVal BaseHost As Uri, ByVal Username As String, ByVal Password As String, ByVal Domain As String, ByVal MyInterval As Integer)
            'NetworkChange.NetworkAddressChanged += new NetworkAddressChangedEventHandler(NetworkChange_NetworkAddressChanged);
 
            SetUserPass(Username, Password, Domain)
            _staticTime = True
            _preFetchTime = MyInterval
            Me.BaseUri = BaseHost
            MyBase.AutoReset = True
            GetAuthCookie()
            'MyBase.Elapsed += New ElapsedEventHandler(AddressOf InvalidateCookie)
            AddHandler MyBase.Elapsed, AddressOf InvalidateCookie
            MyBase.Start()
            MyBase.Enabled = True
        End Sub

        Public Sub New(ByVal BaseHost As Uri, ByVal MyInterval As Integer)
            'NetworkChange.NetworkAddressChanged += new NetworkAddressChangedEventHandler(NetworkChange_NetworkAddressChanged);

            SetUserPass(Username, Password, Domain)
            _preFetchTime = MyInterval
            _staticTime = True
            Me.BaseUri = BaseHost
            MyBase.AutoReset = True
            GetAuthCookie()
            'MyBase.Elapsed += New ElapsedEventHandler(InvalidateCookie)
            AddHandler MyBase.Elapsed, AddressOf InvalidateCookie
            MyBase.Start()
            MyBase.Enabled = True
        End Sub

#End Region

#Region "       Members         "


        Public Sub SetUserPass(ByVal Username As String, ByVal Password As String, ByVal Domain As String)

            Me.username = Username
            Me.password = Password
            Me.domain = Domain
            Me.usingNTLM = True

        End Sub

        Public Sub NTLMOff()
            Me.username = ""
            Me.password = ""
            Me.domain = ""
            Me.usingNTLM = False
            Me.GetAuthCookie()
        End Sub

        Protected Friend Sub GetAuthCookie()

            Dim baseUrl As String = ""
            If (Not usingNTLM) Then
                baseUrl = BaseUri.Scheme & "://" & BaseUri.Authority

                For i As Integer = 0 To BaseUri.Segments.Length - 1
                    baseUrl += BaseUri.Segments(i).ToString
                Next
                baseUrl += "aFakeFileForIAMAuth404.html"
            Else
                baseUrl = _IAMPasswordUrl
            End If

            Dim tmpCC As CookieContainer = New CookieContainer()
            Dim tmpHttpReq As HttpWebRequest = CType(WebRequest.Create(baseUrl), HttpWebRequest) 'Cast as HTTPWebRequest
            tmpHttpReq.AllowAutoRedirect = True
            tmpHttpReq.CookieContainer = tmpCC

            If (Not usingNTLM) Then
                tmpHttpReq.UseDefaultCredentials = True
            Else

                Dim cc As CredentialCache = New CredentialCache()
                'cc.Add(New Uri(baseUrl), "NTLM", New NetworkCredential(Me.username, Me.password))
                'cc.Add(New Uri(_IAMAuthUrl), "NTLM", New NetworkCredential(Me.username, Me.password))
                'cc.Add(New Uri(baseUrl), "NTLM", New NetworkCredential(Me.username, Me.password, Me.domain))
                'cc.Add(New Uri(_IAMAuthUrl), "NTLM", New NetworkCredential(Me.username, Me.password, Me.domain))
                cc.Add(New Uri(baseUrl), "Basic", New NetworkCredential(Me.username, Me.password, Me.domain))
                'cc.Add(New Uri(_IAMAuthUrl), "Basic", New NetworkCredential(Me.username, Me.password, Me.domain))
                'cc.Add(New Uri(_IAMEuAuthUrl), "NTLM", New NetworkCredential(Me.username, Me.password, Me.domain))
                'cc.Add(new Uri(baseUrl), "Negotiate", new NetworkCredential(this.username, this.password, this.domain))
                'cc.Add(new Uri(iamBaseUrl), "Negotiate", new NetworkCredential(this.username, this.password, this.domain))
                tmpHttpReq.AllowAutoRedirect = True
                tmpHttpReq.UseDefaultCredentials = False
                tmpHttpReq.Credentials = cc
                tmpHttpReq.CookieContainer = tmpCC
            End If

            Try

                Dim resp As HttpWebResponse = tmpHttpReq.GetResponse() '(HttpWebResponse)
                resp.Close()

            Catch e As WebException



                If (TypeOf e.Response Is HttpWebResponse) Then
                    Dim code As HttpStatusCode = CType(e.Response, HttpWebResponse).StatusCode
                    Select Case code

                        Case HttpStatusCode.NotFound

                        Case HttpStatusCode.Unauthorized
                            Throw New IAMAuthenticationCredentialsNotFoundException(e)
                        Case HttpStatusCode.Forbidden
                            Throw New IAMAccessDeniedException(e)
                        Case Else
                            Throw e
                    End Select

                Else
                    Throw e
                End If

            End Try


            Dim retrievedCookie As Cookie = GetIAMCookieByName(tmpCC)
            If (retrievedCookie Is Nothing) Then
                Throw New IAMCookieException("IAM cookie not found. No cookie with name " + _IAMCookieName)
            End If
            IAMCookie = retrievedCookie
        End Sub

        Private Function GetIAMCookieByName(ByVal cc As CookieContainer) As Cookie
            Dim lstCookies As CookieCollection = cc.GetCookies(BaseUri)
            For Each cook As Cookie In lstCookies
                If (_IAMCookieName.Equals(cook.Name)) Then
                    Return cook
                End If
            Next
            Return Nothing
        End Function

        Private Sub NetworkChange_NetworkAddressChanged(ByVal sender As Object, ByVal e As EventArgs)
            updateIPList()
        End Sub

        Private Sub InvalidateCookie(ByVal Source As Object, ByVal e As ElapsedEventArgs)
            _isValid = False
        End Sub

        Private Sub UpdateIPList()
            Dim IPList As IPAddress() = Dns.GetHostAddresses(System.Net.Dns.GetHostName())

            If (_localIPList Is Nothing) Then
                _localIPList = New IPAddress() {}
            End If

            If IPList Is Nothing Then
                IPList = New IPAddress() {}
            End If

            'If Not _localIPList.SequenceEqual(IPList) Then
            If Not _localIPList.Equals(IPList) Then
                _localIPList = IPList
                _isValid = False
            End If

        End Sub

#End Region

    End Class
End Namespace
