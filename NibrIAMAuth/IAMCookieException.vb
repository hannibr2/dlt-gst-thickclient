﻿Namespace NibrIAMAuth

    Public Class IAMCookieException
        Inherits IAMException

        Public Sub New(ByVal Message As String)
            MyBase.New(Message)
        End Sub
    End Class

End Namespace
