﻿Namespace NibrIAMAuth

    Public Class IAMAuthenticationCredentialsNotFoundException
        Inherits IAMException



        Public Sub New()
            MyBase.New("Bad credentials")
        End Sub


        Public Sub New(ByVal Message As String, ByVal InnerException As Exception)
            MyBase.New(Message, InnerException)
        End Sub


        Public Sub New(ByVal InnerException As Exception)
            MyBase.New("Bad credentials", InnerException)
        End Sub

    End Class

End Namespace
