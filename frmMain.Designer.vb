<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode3 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode4 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode5 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode6 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode7 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode8 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode9 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode10 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode11 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode12 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode13 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode14 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode15 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode16 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode17 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.GridView8 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView9 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl5 = New DevExpress.XtraGrid.GridControl()
        Me.GridView10 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView11 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl6 = New DevExpress.XtraGrid.GridControl()
        Me.GridView12 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn21 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GrdViewActivePackageDetail = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdActivePackageDetails = New DevExpress.XtraGrid.GridControl()
        Me.GrdViewActivePackage = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colActPackContainerID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackBarcode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackFirst = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackLast = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackCompany = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackCity = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackCountry = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackSite = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackEmail = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackRequestNR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackDeliveryForm = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackProtocol = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActPackDestRackGroupName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ActivePackageDetailsToolTip = New DevExpress.Utils.ToolTipController(Me.components)
        Me.GrdViewExistingPackagesDetail = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdExistingPackages = New DevExpress.XtraGrid.GridControl()
        Me.menuGridRightClick = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.menuRemoveItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GrdViewExistingPackages = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPackageID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistPackBarcode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistPackItemCount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistPackDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistPackRecipientFirst = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistPackRecipientLast = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistPackCompany = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistPackCountry = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistPackCity = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistPackAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistPackLeavingDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GrdViewPendingItemsDetail = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdPendingItems = New DevExpress.XtraGrid.GridControl()
        Me.GrdViewPendingItems = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colContainerID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBARCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTYPE_NAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDESTINATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colRECIPIENTFIRSTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colRECIPIENTLASTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmailAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colREQUEST_NR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTYPE_ID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colRECEIVER_ID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPOUND = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFACTORY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDlvryForm = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSite = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colProtocol = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDestRackGroupName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFulfillmentDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeavingDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdFinalPackageDetails = New DevExpress.XtraGrid.GridControl()
        Me.GrdViewFinalPackage = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn56 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn57 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn58 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn59 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn60 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn61 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn62 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn63 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn64 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn65 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn66 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn67 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn68 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn69 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.FinalPackageToolTip = New DevExpress.Utils.ToolTipController(Me.components)
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdFinalizedPackages = New DevExpress.XtraGrid.GridControl()
        Me.menuFinalizedPackagesRightClick = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GrdViewFinalizedPackages = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn37 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn38 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn47 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn48 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn49 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn50 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn51 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn52 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn53 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn54 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn55 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn40 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GrdViewActiveShipmentDetails = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdActiveShipmentDetails = New DevExpress.XtraGrid.GridControl()
        Me.GrdViewActiveShipment = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colActShipContainerID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActShipPackageBarcode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActShipFirst = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActShipLast = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActShipCompany = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActShipAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActShipCountry = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActShipDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActShipMailAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActShipSiteCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colActShipCity = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ActiveShipmentToolTip = New DevExpress.Utils.ToolTipController(Me.components)
        Me.GridView13 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdExistingShipments = New DevExpress.XtraGrid.GridControl()
        Me.grdViewExistingShipments = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colExistShipShipmentID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipFirst = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipLast = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipCompany = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipCountry = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipPackageCount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipItemCount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipReceiverID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistSiteCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipCity = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipPriority = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistShipExtRefName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView15 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdPendingPackages = New DevExpress.XtraGrid.GridControl()
        Me.GrdViewPendingPackages = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPendPackContainerID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackBarcode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackReceiverID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackRecipientFirst = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackRecipientLast = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackCompany = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackCountry = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackItemCount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackMailAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackSiteCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPendPackCity = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView21 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdFinalizedShipments = New DevExpress.XtraGrid.GridControl()
        Me.altMenuGridRightClick = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.altMenuUnfinalize = New System.Windows.Forms.ToolStripMenuItem()
        Me.altMenuPrintForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.altMenuUpdateTracking = New System.Windows.Forms.ToolStripMenuItem()
        Me.GridViewFinalizedShipments = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colFinShipID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFinShipName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFinShipFirst = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFinShipLastName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFinShipCompany = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFinShipCountry = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFinShipPackageCount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFinShipItemCount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFinShipShipDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFinShipCity = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFinShipExtReference = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdAnticipatedShipments = New DevExpress.XtraGrid.GridControl()
        Me.GridViewAntShipments = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn22 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn23 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn24 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn25 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn26 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn27 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn28 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridViewRcvPkgContainers = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPkCntParentContainerID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCntBarcode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCntSampleName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCntRanking = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCntFirstName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCntLastName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCntMailAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCntSiteCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCntOrderName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCntProtocolGroupName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grdRcvPackageView = New DevExpress.XtraGrid.GridControl()
        Me.GridViewRcvPackage = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPkParentContainerID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkBarcode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkFirstName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkLastName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCompany = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkCountryName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkMailAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkSiteCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.bbtnCreatePackage = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnPackageMaintenance = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnLogin = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnCreateShipment = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnShipmentPlanner = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnViewDetails = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnSettings = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnSampleTransfer = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnReceiveShipment = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnSTISampleDropoff = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnSTIPackageDropoff = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnSTIPackagePickup = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnSTIBinMaintenance = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnSTILog = New DevExpress.XtraBars.BarButtonItem()
        Me.bbtnCreateInvPackage = New DevExpress.XtraBars.BarButtonItem()
        Me.rpLogin = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.rpPackage = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.rpShipment = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup8 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.rpStats = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.rpSettings = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup6 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.rpSampleTransfer = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup7 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup9 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup10 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup11 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup12 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup13 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridViewRcvShpPackages = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPkgParentContainerID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkgBarcode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkgFirstName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkgLastName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkgCompany = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkgCountryName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkgMailAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPkgSiteCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grdReceiveShipment = New DevExpress.XtraGrid.GridControl()
        Me.GridViewRcvShpContainers = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCntContainerID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCntParentContainerID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCntBarcode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grdCntSampleName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCntRanking = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCntLastName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCntFirstName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCntMailAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCntSiteCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCntCity = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCntCountryName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCntOrderName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridViewRcvShipment = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colShipmentID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colShipmentName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFirstName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCompany = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCountryName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMailAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPackageCount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colContainerCount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridView14 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridShipment = New DevExpress.XtraGrid.GridControl()
        Me.GridViewShipmentDetails = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.MainLayout = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControlMainGroup = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.SchedulerStorage1 = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.NavLinks = New DevExpress.XtraNavBar.NavBarGroup()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Request = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.btnImportFile = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckedListBoxControl1 = New DevExpress.XtraEditors.CheckedListBoxControl()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl9 = New DevExpress.XtraEditors.GroupControl()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.diaLabelTemplate = New System.Windows.Forms.FolderBrowserDialog()
        Me.diaShipmentTemplate = New System.Windows.Forms.OpenFileDialog()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.tabMain = New DevExpress.XtraTab.XtraTabControl()
        Me.TabCreatePackage = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl14 = New DevExpress.XtraEditors.GroupControl()
        Me.txtRemoveItem = New DevExpress.XtraEditors.TextEdit()
        Me.btnRemoveItem = New DevExpress.XtraEditors.SimpleButton()
        Me.grpAddPackageItem = New DevExpress.XtraEditors.GroupControl()
        Me.btn_LookupScan = New DevExpress.XtraEditors.SimpleButton()
        Me.btnScanItems = New DevExpress.XtraEditors.SimpleButton()
        Me.txtPackageItemBarcode = New DevExpress.XtraEditors.TextEdit()
        Me.GrpContent = New DevExpress.XtraEditors.GroupControl()
        Me.lkPackageOrigin = New DevExpress.XtraEditors.LookUpEdit()
        Me.lblPackageOrigin = New DevExpress.XtraEditors.LabelControl()
        Me.lblPackageContent = New DevExpress.XtraEditors.LabelControl()
        Me.lkPackageContent = New DevExpress.XtraEditors.LookUpEdit()
        Me.grpValidationRules = New DevExpress.XtraEditors.GroupControl()
        Me.ChkLstPackageValidationRules = New DevExpress.XtraEditors.CheckedListBoxControl()
        Me.grpFinalizePackage = New DevExpress.XtraEditors.GroupControl()
        Me.btnFinalizePackage = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.LayoutControl6 = New DevExpress.XtraLayout.LayoutControl()
        Me.grpExistingPackages = New DevExpress.XtraEditors.GroupControl()
        Me.grpPendingItems = New DevExpress.XtraEditors.GroupControl()
        Me.btnRefreshPackageGrids = New DevExpress.XtraEditors.SimpleButton()
        Me.btnShowAllPending = New DevExpress.XtraEditors.SimpleButton()
        Me.lkPendingTimeframe = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.grpActivePackageDetail = New DevExpress.XtraEditors.GroupControl()
        Me.lblPackageErrors = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlGroup7 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.SplitterItem6 = New DevExpress.XtraLayout.SplitterItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem7 = New DevExpress.XtraLayout.SplitterItem()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem5 = New DevExpress.XtraLayout.SplitterItem()
        Me.TabLogin = New DevExpress.XtraTab.XtraTabPage()
        Me.GrpActiveUser = New DevExpress.XtraEditors.GroupControl()
        Me.lkLoginDomains = New DevExpress.XtraEditors.LookUpEdit()
        Me.lblDomain = New DevExpress.XtraEditors.LabelControl()
        Me.lblUsername = New DevExpress.XtraEditors.LabelControl()
        Me.txtUserPassword = New DevExpress.XtraEditors.TextEdit()
        Me.lblPassword = New DevExpress.XtraEditors.LabelControl()
        Me.btnLogin = New DevExpress.XtraEditors.SimpleButton()
        Me.txtUsername = New DevExpress.XtraEditors.TextEdit()
        Me.TabCreateInvPackage = New DevExpress.XtraTab.XtraTabPage()
        Me.TabPackageMaintenance = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl7 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl6 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl19 = New DevExpress.XtraEditors.GroupControl()
        Me.txtReprintPackageLabel = New DevExpress.XtraEditors.TextEdit()
        Me.btnReprintPackageLabel = New DevExpress.XtraEditors.SimpleButton()
        Me.grpDeliverPackage = New DevExpress.XtraEditors.GroupControl()
        Me.txtDeliverPackageBarcode = New DevExpress.XtraEditors.TextEdit()
        Me.btnDeliverPackage = New DevExpress.XtraEditors.SimpleButton()
        Me.grpUnfinalizePackage = New DevExpress.XtraEditors.GroupControl()
        Me.txtFnlPackageBarcode = New DevExpress.XtraEditors.TextEdit()
        Me.btnUnfinalizePackage = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl()
        Me.LayoutControl8 = New DevExpress.XtraLayout.LayoutControl()
        Me.GroupControl15 = New DevExpress.XtraEditors.GroupControl()
        Me.grpFinalizedPackages = New DevExpress.XtraEditors.GroupControl()
        Me.lkFinPkgTimeline = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlGroup9 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem9 = New DevExpress.XtraLayout.SplitterItem()
        Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem8 = New DevExpress.XtraLayout.SplitterItem()
        Me.TabCreateShipment = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl9 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl8 = New DevExpress.XtraEditors.PanelControl()
        Me.lblShipmentType = New DevExpress.XtraEditors.LabelControl()
        Me.lkShipmentType = New DevExpress.XtraEditors.LookUpEdit()
        Me.grpFinalizeShipment = New DevExpress.XtraEditors.GroupControl()
        Me.lblPriority = New DevExpress.XtraEditors.LabelControl()
        Me.btnViewShipmentForm = New DevExpress.XtraEditors.SimpleButton()
        Me.lblShippingCourier = New DevExpress.XtraEditors.LabelControl()
        Me.lkShippingPriority = New DevExpress.XtraEditors.LookUpEdit()
        Me.btnFinalizeShipment = New DevExpress.XtraEditors.SimpleButton()
        Me.lkShippingCourier = New DevExpress.XtraEditors.LookUpEdit()
        Me.lblFillingMaterial = New DevExpress.XtraEditors.LabelControl()
        Me.grpShipmentRules = New DevExpress.XtraEditors.GroupControl()
        Me.ChkLstShipmentValidationRules = New DevExpress.XtraEditors.CheckedListBoxControl()
        Me.lkFillingMaterial = New DevExpress.XtraEditors.LookUpEdit()
        Me.grpAddPackage = New DevExpress.XtraEditors.GroupControl()
        Me.btnCreateShipment = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAddPackageShipment = New DevExpress.XtraEditors.SimpleButton()
        Me.txtShipmentPackageBarcode = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.LayoutControl10 = New DevExpress.XtraLayout.LayoutControl()
        Me.GroupControl12 = New DevExpress.XtraEditors.GroupControl()
        Me.grpExistingShipments = New DevExpress.XtraEditors.GroupControl()
        Me.grpPendingPackages = New DevExpress.XtraEditors.GroupControl()
        Me.btnShowPendingPackages = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRefreshShipment = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControlGroup11 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem11 = New DevExpress.XtraLayout.SplitterItem()
        Me.SplitterItem12 = New DevExpress.XtraLayout.SplitterItem()
        Me.LayoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem10 = New DevExpress.XtraLayout.SplitterItem()
        Me.TabShipmentPlanner = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.grpPendingShipments = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TabViewShipment = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl21 = New DevExpress.XtraEditors.GroupControl()
        Me.btnTXTExport = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExportXSL = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl20 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.dtEditFromDate = New DevExpress.XtraEditors.DateEdit()
        Me.lkSites = New DevExpress.XtraEditors.LookUpEdit()
        Me.dtEditToDate = New DevExpress.XtraEditors.DateEdit()
        Me.btnSiteContainers = New DevExpress.XtraEditors.SimpleButton()
        Me.grpViewShipmentInput = New DevExpress.XtraEditors.GroupControl()
        Me.btnContainerBarcodeLookup = New DevExpress.XtraEditors.SimpleButton()
        Me.txtViewShipment = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.grpShipmentInfo = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem4 = New DevExpress.XtraLayout.SplitterItem()
        Me.TabReceive = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl11 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl9 = New DevExpress.XtraEditors.PanelControl()
        Me.LayoutControl12 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl11 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl10 = New DevExpress.XtraEditors.GroupControl()
        Me.btnLookupShipment = New DevExpress.XtraEditors.SimpleButton()
        Me.txtReceivedBarcode = New DevExpress.XtraEditors.TextEdit()
        Me.lblReceivedBarcode = New DevExpress.XtraEditors.LabelControl()
        Me.grpReceiveActions = New DevExpress.XtraEditors.GroupControl()
        Me.btnReceiveComment = New DevExpress.XtraEditors.SimpleButton()
        Me.btnRepackageItems = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancelRecieve = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReceiveItems = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl10 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl11 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControlGroup13 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem14 = New DevExpress.XtraLayout.SplitterItem()
        Me.GroupControl13 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControlGroup12 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem13 = New DevExpress.XtraLayout.SplitterItem()
        Me.TabSettings = New DevExpress.XtraTab.XtraTabPage()
        Me.btnScannerHelp = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTestScan = New DevExpress.XtraEditors.SimpleButton()
        Me.cboScanner = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.cboPrinterDPI = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl18 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtQuickInternational = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtFedExAccount = New DevExpress.XtraEditors.TextEdit()
        Me.lkFulfillmentSite = New DevExpress.XtraEditors.LookUpEdit()
        Me.lblFulfillmentSite = New DevExpress.XtraEditors.LabelControl()
        Me.cboEnvironment = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.lblEnvironment = New DevExpress.XtraEditors.LabelControl()
        Me.btnBrowseShipmentTemplate = New DevExpress.XtraEditors.SimpleButton()
        Me.txtShipmentTemplate = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.btnBrowseLabelTemplate = New DevExpress.XtraEditors.SimpleButton()
        Me.txtLabelTemplatePath = New DevExpress.XtraEditors.TextEdit()
        Me.lblLabelTemplatePath = New DevExpress.XtraEditors.LabelControl()
        Me.btnSaveSettings = New DevExpress.XtraEditors.SimpleButton()
        Me.lblPrinterCOMport = New DevExpress.XtraEditors.LabelControl()
        Me.txtPrinterPort = New DevExpress.XtraEditors.TextEdit()
        Me.cboPrinter = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.lblPrinter = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCostCenter = New DevExpress.XtraEditors.TextEdit()
        Me.TabSampleTransfer = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl13 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl13 = New DevExpress.XtraEditors.PanelControl()
        Me.GrpSimpleLogin = New DevExpress.XtraEditors.GroupControl()
        Me.lblSimpleUsername = New DevExpress.XtraEditors.LabelControl()
        Me.btnSimpleLogin = New DevExpress.XtraEditors.SimpleButton()
        Me.txtSimpleUsername = New DevExpress.XtraEditors.TextEdit()
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl()
        Me.NavSTIFunctions = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavSTISampleDropoff = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavSTIPackageDropoff = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavSTIPackagePickup = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavSTIBinMaintenance = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavSTILog = New DevExpress.XtraNavBar.NavBarItem()
        Me.PanelControl12 = New DevExpress.XtraEditors.PanelControl()
        Me.tabSTIFunction = New DevExpress.XtraTab.XtraTabControl()
        Me.tabSTISampleDropoff = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl14 = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControl15 = New DevExpress.XtraLayout.LayoutControl()
        Me.GroupControl16 = New DevExpress.XtraEditors.GroupControl()
        Me.btnSTISampleSubmit = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtSTISampleSubmission = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl17 = New DevExpress.XtraEditors.GroupControl()
        Me.ListBoxControl1 = New DevExpress.XtraEditors.ListBoxControl()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem17 = New DevExpress.XtraLayout.SplitterItem()
        Me.txtSampleDropMessage = New DevExpress.XtraEditors.MemoEdit()
        Me.LayoutControlGroup15 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem16 = New DevExpress.XtraLayout.SplitterItem()
        Me.tabSTIPackageDropoff = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl16 = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControl17 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl15 = New DevExpress.XtraEditors.PanelControl()
        Me.GridPackageDropoff = New DevExpress.XtraGrid.GridControl()
        Me.grdViewPackageDropoff = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn29 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn30 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn31 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn32 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn33 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn34 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn41 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn35 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl14 = New DevExpress.XtraEditors.PanelControl()
        Me.btnTrackingURL = New DevExpress.XtraEditors.SimpleButton()
        Me.btnArrivedNoPhase = New DevExpress.XtraEditors.SimpleButton()
        Me.btnArrived = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReadyForPickup = New DevExpress.XtraEditors.SimpleButton()
        Me.grpPackageDropoff = New DevExpress.XtraEditors.GroupControl()
        Me.lblPackageDropoff = New DevExpress.XtraEditors.LabelControl()
        Me.btnPackageDropoff = New DevExpress.XtraEditors.SimpleButton()
        Me.txtPackageDropoff = New DevExpress.XtraEditors.TextEdit()
        Me.grpBinDropoff = New DevExpress.XtraEditors.GroupControl()
        Me.lblBinDropoff = New DevExpress.XtraEditors.LabelControl()
        Me.btnBinDropoff = New DevExpress.XtraEditors.SimpleButton()
        Me.txtBinDropoff = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup17 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtPckDropMessage = New DevExpress.XtraEditors.MemoEdit()
        Me.LayoutControlGroup16 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem18 = New DevExpress.XtraLayout.SplitterItem()
        Me.tabSTIPackagePickup = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl20 = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControl21 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl19 = New DevExpress.XtraEditors.PanelControl()
        Me.grpBinPickup = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.btnBinPickup = New DevExpress.XtraEditors.SimpleButton()
        Me.txtBinPickup = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl18 = New DevExpress.XtraEditors.PanelControl()
        Me.GridPackagePickup = New DevExpress.XtraGrid.GridControl()
        Me.GridViewPackagePickup = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn36 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn39 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn45 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn46 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup21 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem44 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem45 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.grpPreviousPackageHistory = New DevExpress.XtraEditors.GroupControl()
        Me.GridPackagePickupHistory = New DevExpress.XtraGrid.GridControl()
        Me.GridViewPackagePickupHistory = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn74 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn75 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn76 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn77 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtPckPickMessage = New DevExpress.XtraEditors.MemoEdit()
        Me.LayoutControlGroup20 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem41 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem42 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem43 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem19 = New DevExpress.XtraLayout.SplitterItem()
        Me.tabSTIBinMaintenance = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl18 = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControl19 = New DevExpress.XtraLayout.LayoutControl()
        Me.PanelControl17 = New DevExpress.XtraEditors.PanelControl()
        Me.GridBinMaintenance = New DevExpress.XtraGrid.GridControl()
        Me.GridViewBinMaintenance = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn70 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn71 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn72 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl16 = New DevExpress.XtraEditors.PanelControl()
        Me.grpBinMaintenance = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.btnEmptyBin = New DevExpress.XtraEditors.SimpleButton()
        Me.txtBinMaintain = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup19 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem40 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.MemoEdit2 = New DevExpress.XtraEditors.MemoEdit()
        Me.LayoutControlGroup18 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.tabSTILog = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.GridSTILog = New DevExpress.XtraGrid.GridControl()
        Me.GridViewSTILog = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup14 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem15 = New DevExpress.XtraLayout.SplitterItem()
        Me.STITimer = New System.Windows.Forms.Timer(Me.components)
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewActivePackageDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdActivePackageDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewActivePackage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewExistingPackagesDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdExistingPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.menuGridRightClick.SuspendLayout()
        CType(Me.GrdViewExistingPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewPendingItemsDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdPendingItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewPendingItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdFinalPackageDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewFinalPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdFinalizedPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.menuFinalizedPackagesRightClick.SuspendLayout()
        CType(Me.GrdViewFinalizedPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewActiveShipmentDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdActiveShipmentDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewActiveShipment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdExistingShipments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdViewExistingShipments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdPendingPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdViewPendingPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdFinalizedShipments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.altMenuGridRightClick.SuspendLayout()
        CType(Me.GridViewFinalizedShipments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdAnticipatedShipments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewAntShipments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewRcvPkgContainers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdRcvPackageView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewRcvPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewRcvShpPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdReceiveShipment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewRcvShpContainers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewRcvShipment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridShipment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewShipmentDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MainLayout, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlMainGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SchedulerStorage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox9.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.CheckedListBoxControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMain.SuspendLayout()
        Me.TabCreatePackage.SuspendLayout()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl5.SuspendLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.GroupControl14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl14.SuspendLayout()
        CType(Me.txtRemoveItem.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpAddPackageItem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpAddPackageItem.SuspendLayout()
        CType(Me.txtPackageItemBarcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrpContent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpContent.SuspendLayout()
        CType(Me.lkPackageOrigin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lkPackageContent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpValidationRules, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpValidationRules.SuspendLayout()
        CType(Me.ChkLstPackageValidationRules, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpFinalizePackage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpFinalizePackage.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.LayoutControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl6.SuspendLayout()
        CType(Me.grpExistingPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpExistingPackages.SuspendLayout()
        CType(Me.grpPendingItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPendingItems.SuspendLayout()
        CType(Me.lkPendingTimeframe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpActivePackageDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpActivePackageDetail.SuspendLayout()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabLogin.SuspendLayout()
        CType(Me.GrpActiveUser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpActiveUser.SuspendLayout()
        CType(Me.lkLoginDomains.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUserPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUsername.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPackageMaintenance.SuspendLayout()
        CType(Me.LayoutControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl7.SuspendLayout()
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl6.SuspendLayout()
        CType(Me.GroupControl19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl19.SuspendLayout()
        CType(Me.txtReprintPackageLabel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpDeliverPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpDeliverPackage.SuspendLayout()
        CType(Me.txtDeliverPackageBarcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpUnfinalizePackage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpUnfinalizePackage.SuspendLayout()
        CType(Me.txtFnlPackageBarcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        CType(Me.LayoutControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl8.SuspendLayout()
        CType(Me.GroupControl15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl15.SuspendLayout()
        CType(Me.grpFinalizedPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpFinalizedPackages.SuspendLayout()
        CType(Me.lkFinPkgTimeline.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabCreateShipment.SuspendLayout()
        CType(Me.LayoutControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl9.SuspendLayout()
        CType(Me.PanelControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl8.SuspendLayout()
        CType(Me.lkShipmentType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpFinalizeShipment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpFinalizeShipment.SuspendLayout()
        CType(Me.lkShippingPriority.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lkShippingCourier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpShipmentRules, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpShipmentRules.SuspendLayout()
        CType(Me.ChkLstShipmentValidationRules, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lkFillingMaterial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpAddPackage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpAddPackage.SuspendLayout()
        CType(Me.txtShipmentPackageBarcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        CType(Me.LayoutControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl10.SuspendLayout()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl12.SuspendLayout()
        CType(Me.grpExistingShipments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpExistingShipments.SuspendLayout()
        CType(Me.grpPendingPackages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPendingPackages.SuspendLayout()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabShipmentPlanner.SuspendLayout()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl4.SuspendLayout()
        CType(Me.grpPendingShipments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPendingShipments.SuspendLayout()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabViewShipment.SuspendLayout()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.GroupControl21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl21.SuspendLayout()
        CType(Me.GroupControl20, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl20.SuspendLayout()
        CType(Me.dtEditFromDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtEditFromDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lkSites.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtEditToDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtEditToDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpViewShipmentInput, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpViewShipmentInput.SuspendLayout()
        CType(Me.txtViewShipment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.grpShipmentInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpShipmentInfo.SuspendLayout()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabReceive.SuspendLayout()
        CType(Me.LayoutControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl11.SuspendLayout()
        CType(Me.PanelControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl9.SuspendLayout()
        CType(Me.LayoutControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl12.SuspendLayout()
        CType(Me.PanelControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl11.SuspendLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.txtReceivedBarcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpReceiveActions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpReceiveActions.SuspendLayout()
        CType(Me.PanelControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl10.SuspendLayout()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl11.SuspendLayout()
        CType(Me.LayoutControlGroup13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl13.SuspendLayout()
        CType(Me.LayoutControlGroup12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabSettings.SuspendLayout()
        CType(Me.cboScanner.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboPrinterDPI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl18.SuspendLayout()
        CType(Me.txtQuickInternational.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFedExAccount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lkFulfillmentSite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEnvironment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShipmentTemplate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLabelTemplatePath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrinterPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboPrinter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCostCenter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabSampleTransfer.SuspendLayout()
        CType(Me.LayoutControl13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl13.SuspendLayout()
        CType(Me.PanelControl13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl13.SuspendLayout()
        CType(Me.GrpSimpleLogin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpSimpleLogin.SuspendLayout()
        CType(Me.txtSimpleUsername.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl12.SuspendLayout()
        CType(Me.tabSTIFunction, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSTIFunction.SuspendLayout()
        Me.tabSTISampleDropoff.SuspendLayout()
        CType(Me.LayoutControl14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl14.SuspendLayout()
        CType(Me.LayoutControl15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl15.SuspendLayout()
        CType(Me.GroupControl16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl16.SuspendLayout()
        CType(Me.txtSTISampleSubmission.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl17.SuspendLayout()
        CType(Me.ListBoxControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSampleDropMessage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSTIPackageDropoff.SuspendLayout()
        CType(Me.LayoutControl16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl16.SuspendLayout()
        CType(Me.LayoutControl17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl17.SuspendLayout()
        CType(Me.PanelControl15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl15.SuspendLayout()
        CType(Me.GridPackageDropoff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdViewPackageDropoff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl14.SuspendLayout()
        CType(Me.grpPackageDropoff, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPackageDropoff.SuspendLayout()
        CType(Me.txtPackageDropoff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpBinDropoff, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpBinDropoff.SuspendLayout()
        CType(Me.txtBinDropoff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPckDropMessage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSTIPackagePickup.SuspendLayout()
        CType(Me.LayoutControl20, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl20.SuspendLayout()
        CType(Me.LayoutControl21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl21.SuspendLayout()
        CType(Me.PanelControl19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl19.SuspendLayout()
        CType(Me.grpBinPickup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpBinPickup.SuspendLayout()
        CType(Me.txtBinPickup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl18.SuspendLayout()
        CType(Me.GridPackagePickup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewPackagePickup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpPreviousPackageHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPreviousPackageHistory.SuspendLayout()
        CType(Me.GridPackagePickupHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewPackagePickupHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPckPickMessage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSTIBinMaintenance.SuspendLayout()
        CType(Me.LayoutControl18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl18.SuspendLayout()
        CType(Me.LayoutControl19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl19.SuspendLayout()
        CType(Me.PanelControl17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl17.SuspendLayout()
        CType(Me.GridBinMaintenance, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewBinMaintenance, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl16.SuspendLayout()
        CType(Me.grpBinMaintenance, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpBinMaintenance.SuspendLayout()
        CType(Me.txtBinMaintain.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSTILog.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.GridSTILog, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewSTILog, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridView7
        '
        Me.GridView7.GridControl = Me.GridControl4
        Me.GridView7.Name = "GridView7"
        '
        'GridControl4
        '
        GridLevelNode1.LevelTemplate = Me.GridView7
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl4.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl4.Location = New System.Drawing.Point(5, 23)
        Me.GridControl4.MainView = Me.GridView8
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(666, 128)
        Me.GridControl4.TabIndex = 5
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView8, Me.GridView7})
        '
        'GridView8
        '
        Me.GridView8.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5})
        Me.GridView8.GridControl = Me.GridControl4
        Me.GridView8.Name = "GridView8"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "REQUEST"
        Me.GridColumn1.FieldName = "REQUEST_NR"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "COMPOUND NAME"
        Me.GridColumn2.FieldName = "COMPOUND"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "BARCODE"
        Me.GridColumn3.FieldName = "BARCODE"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "RECIPIENT"
        Me.GridColumn4.FieldName = "RECIPIENT"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "DESTINATION"
        Me.GridColumn5.FieldName = "DESTINATION"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        '
        'GridView9
        '
        Me.GridView9.GridControl = Me.GridControl5
        Me.GridView9.Name = "GridView9"
        '
        'GridControl5
        '
        GridLevelNode2.LevelTemplate = Me.GridView9
        GridLevelNode2.RelationName = "Level1"
        Me.GridControl5.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.GridControl5.Location = New System.Drawing.Point(5, 23)
        Me.GridControl5.MainView = Me.GridView10
        Me.GridControl5.Name = "GridControl5"
        Me.GridControl5.Size = New System.Drawing.Size(666, 113)
        Me.GridControl5.TabIndex = 5
        Me.GridControl5.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView10, Me.GridView9})
        '
        'GridView10
        '
        Me.GridView10.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn11, Me.GridColumn12, Me.GridColumn13, Me.GridColumn14, Me.GridColumn15, Me.GridColumn16})
        Me.GridView10.GridControl = Me.GridControl5
        Me.GridView10.Name = "GridView10"
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "BARCODE"
        Me.GridColumn11.FieldName = "BARCODE"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 0
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "RECIPIENT"
        Me.GridColumn12.FieldName = "RECIPIENT"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 1
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "COMPANY"
        Me.GridColumn13.FieldName = "COMPANY"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 2
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "COUNTRY"
        Me.GridColumn14.FieldName = "COUNTRY"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 3
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "# OF ITEMS"
        Me.GridColumn15.FieldName = "NUMBEROFITEMS"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 4
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "DATE"
        Me.GridColumn16.FieldName = "DATE"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 5
        '
        'GridView11
        '
        Me.GridView11.GridControl = Me.GridControl6
        Me.GridView11.Name = "GridView11"
        '
        'GridControl6
        '
        GridLevelNode3.LevelTemplate = Me.GridView11
        GridLevelNode3.RelationName = "Level1"
        Me.GridControl6.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode3})
        Me.GridControl6.Location = New System.Drawing.Point(5, 50)
        Me.GridControl6.MainView = Me.GridView12
        Me.GridControl6.Name = "GridControl6"
        Me.GridControl6.Size = New System.Drawing.Size(666, 114)
        Me.GridControl6.TabIndex = 5
        Me.GridControl6.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView12, Me.GridView11})
        '
        'GridView12
        '
        Me.GridView12.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn17, Me.GridColumn18, Me.GridColumn19, Me.GridColumn20, Me.GridColumn21})
        Me.GridView12.GridControl = Me.GridControl6
        Me.GridView12.Name = "GridView12"
        '
        'GridColumn17
        '
        Me.GridColumn17.Caption = "REQUEST"
        Me.GridColumn17.FieldName = "REQUEST_NR"
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = True
        Me.GridColumn17.VisibleIndex = 0
        '
        'GridColumn18
        '
        Me.GridColumn18.Caption = "COMPOUND NAME"
        Me.GridColumn18.FieldName = "COMPOUND"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = True
        Me.GridColumn18.VisibleIndex = 1
        '
        'GridColumn19
        '
        Me.GridColumn19.Caption = "BARCODE"
        Me.GridColumn19.FieldName = "BARCODE"
        Me.GridColumn19.Name = "GridColumn19"
        Me.GridColumn19.Visible = True
        Me.GridColumn19.VisibleIndex = 2
        '
        'GridColumn20
        '
        Me.GridColumn20.Caption = "RECIPIENT"
        Me.GridColumn20.FieldName = "RECIPIENT"
        Me.GridColumn20.Name = "GridColumn20"
        Me.GridColumn20.Visible = True
        Me.GridColumn20.VisibleIndex = 3
        '
        'GridColumn21
        '
        Me.GridColumn21.Caption = "DESTINATION"
        Me.GridColumn21.FieldName = "DESTINATION"
        Me.GridColumn21.Name = "GridColumn21"
        Me.GridColumn21.Visible = True
        Me.GridColumn21.VisibleIndex = 4
        '
        'GrdViewActivePackageDetail
        '
        Me.GrdViewActivePackageDetail.GridControl = Me.grdActivePackageDetails
        Me.GrdViewActivePackageDetail.Name = "GrdViewActivePackageDetail"
        Me.GrdViewActivePackageDetail.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp
        Me.GrdViewActivePackageDetail.OptionsSelection.MultiSelect = True
        '
        'grdActivePackageDetails
        '
        Me.grdActivePackageDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdActivePackageDetails.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode4.LevelTemplate = Me.GrdViewActivePackageDetail
        GridLevelNode4.RelationName = "Level1"
        Me.grdActivePackageDetails.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode4})
        Me.grdActivePackageDetails.Location = New System.Drawing.Point(2, 24)
        Me.grdActivePackageDetails.MainView = Me.GrdViewActivePackage
        Me.grdActivePackageDetails.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdActivePackageDetails.Name = "grdActivePackageDetails"
        Me.grdActivePackageDetails.Size = New System.Drawing.Size(1178, 240)
        Me.grdActivePackageDetails.TabIndex = 5
        Me.grdActivePackageDetails.ToolTipController = Me.ActivePackageDetailsToolTip
        Me.grdActivePackageDetails.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GrdViewActivePackage, Me.GrdViewActivePackageDetail})
        '
        'GrdViewActivePackage
        '
        Me.GrdViewActivePackage.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colActPackContainerID, Me.colActPackBarcode, Me.colActPackFirst, Me.colActPackLast, Me.colActPackCompany, Me.colActPackAddress, Me.colActPackCity, Me.colActPackCountry, Me.colActPackSite, Me.colActPackEmail, Me.colActPackRequestNR, Me.colActPackDeliveryForm, Me.colActPackProtocol, Me.colActPackDestRackGroupName})
        Me.GrdViewActivePackage.GridControl = Me.grdActivePackageDetails
        Me.GrdViewActivePackage.Name = "GrdViewActivePackage"
        Me.GrdViewActivePackage.OptionsBehavior.Editable = False
        Me.GrdViewActivePackage.OptionsBehavior.ReadOnly = True
        Me.GrdViewActivePackage.OptionsSelection.MultiSelect = True
        Me.GrdViewActivePackage.OptionsView.ShowGroupPanel = False
        '
        'colActPackContainerID
        '
        Me.colActPackContainerID.Caption = "CONTAINER_ID"
        Me.colActPackContainerID.FieldName = "CONTAINER_ID"
        Me.colActPackContainerID.Name = "colActPackContainerID"
        Me.colActPackContainerID.OptionsColumn.ShowInCustomizationForm = False
        '
        'colActPackBarcode
        '
        Me.colActPackBarcode.Caption = "BARCODE"
        Me.colActPackBarcode.FieldName = "BARCODE"
        Me.colActPackBarcode.Name = "colActPackBarcode"
        Me.colActPackBarcode.Visible = True
        Me.colActPackBarcode.VisibleIndex = 0
        '
        'colActPackFirst
        '
        Me.colActPackFirst.Caption = "RECIPIENT_FIRSTNAME"
        Me.colActPackFirst.FieldName = "FIRST_NAME"
        Me.colActPackFirst.Name = "colActPackFirst"
        Me.colActPackFirst.Visible = True
        Me.colActPackFirst.VisibleIndex = 1
        '
        'colActPackLast
        '
        Me.colActPackLast.Caption = "RECIPIENT_LASTNAME"
        Me.colActPackLast.FieldName = "LAST_NAME"
        Me.colActPackLast.Name = "colActPackLast"
        Me.colActPackLast.Visible = True
        Me.colActPackLast.VisibleIndex = 2
        '
        'colActPackCompany
        '
        Me.colActPackCompany.Caption = "COMPANY"
        Me.colActPackCompany.FieldName = "COMPANY"
        Me.colActPackCompany.Name = "colActPackCompany"
        Me.colActPackCompany.Visible = True
        Me.colActPackCompany.VisibleIndex = 3
        '
        'colActPackAddress
        '
        Me.colActPackAddress.Caption = "ADDRESS"
        Me.colActPackAddress.FieldName = "MAIL_ADDRESS"
        Me.colActPackAddress.Name = "colActPackAddress"
        Me.colActPackAddress.Visible = True
        Me.colActPackAddress.VisibleIndex = 4
        '
        'colActPackCity
        '
        Me.colActPackCity.Caption = "CITY"
        Me.colActPackCity.FieldName = "CITY"
        Me.colActPackCity.Name = "colActPackCity"
        Me.colActPackCity.Visible = True
        Me.colActPackCity.VisibleIndex = 5
        '
        'colActPackCountry
        '
        Me.colActPackCountry.Caption = "COUNTRY"
        Me.colActPackCountry.FieldName = "COUNTRY_NAME"
        Me.colActPackCountry.Name = "colActPackCountry"
        Me.colActPackCountry.Visible = True
        Me.colActPackCountry.VisibleIndex = 6
        '
        'colActPackSite
        '
        Me.colActPackSite.Caption = "SITE"
        Me.colActPackSite.FieldName = "SITE_CODE"
        Me.colActPackSite.Name = "colActPackSite"
        Me.colActPackSite.Visible = True
        Me.colActPackSite.VisibleIndex = 7
        '
        'colActPackEmail
        '
        Me.colActPackEmail.Caption = "EMAIL"
        Me.colActPackEmail.FieldName = "EMAIL"
        Me.colActPackEmail.Name = "colActPackEmail"
        Me.colActPackEmail.Visible = True
        Me.colActPackEmail.VisibleIndex = 8
        '
        'colActPackRequestNR
        '
        Me.colActPackRequestNR.Caption = "REQUEST_NUMBER"
        Me.colActPackRequestNR.FieldName = "ORDER_NAME"
        Me.colActPackRequestNR.Name = "colActPackRequestNR"
        Me.colActPackRequestNR.Visible = True
        Me.colActPackRequestNR.VisibleIndex = 9
        '
        'colActPackDeliveryForm
        '
        Me.colActPackDeliveryForm.Caption = "DELIVERY_FORM"
        Me.colActPackDeliveryForm.FieldName = "DELIVERY_FORM_TYPE_NAME"
        Me.colActPackDeliveryForm.Name = "colActPackDeliveryForm"
        Me.colActPackDeliveryForm.Visible = True
        Me.colActPackDeliveryForm.VisibleIndex = 10
        '
        'colActPackProtocol
        '
        Me.colActPackProtocol.Caption = "PROTOCOL_NAME"
        Me.colActPackProtocol.FieldName = "PROTOCOL_NAME"
        Me.colActPackProtocol.Name = "colActPackProtocol"
        Me.colActPackProtocol.Visible = True
        Me.colActPackProtocol.VisibleIndex = 11
        '
        'colActPackDestRackGroupName
        '
        Me.colActPackDestRackGroupName.Caption = "DESTINATION_RACK_GROUP_NAME"
        Me.colActPackDestRackGroupName.FieldName = "DESTINATION_RACK_GROUP_NAME"
        Me.colActPackDestRackGroupName.Name = "colActPackDestRackGroupName"
        Me.colActPackDestRackGroupName.Visible = True
        Me.colActPackDestRackGroupName.VisibleIndex = 12
        '
        'ActivePackageDetailsToolTip
        '
        '
        'GrdViewExistingPackagesDetail
        '
        Me.GrdViewExistingPackagesDetail.GridControl = Me.grdExistingPackages
        Me.GrdViewExistingPackagesDetail.Name = "GrdViewExistingPackagesDetail"
        Me.GrdViewExistingPackagesDetail.OptionsSelection.MultiSelect = True
        '
        'grdExistingPackages
        '
        Me.grdExistingPackages.ContextMenuStrip = Me.menuGridRightClick
        Me.grdExistingPackages.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdExistingPackages.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode5.LevelTemplate = Me.GrdViewExistingPackagesDetail
        GridLevelNode5.RelationName = "Level1"
        Me.grdExistingPackages.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode5})
        Me.grdExistingPackages.Location = New System.Drawing.Point(2, 24)
        Me.grdExistingPackages.MainView = Me.GrdViewExistingPackages
        Me.grdExistingPackages.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdExistingPackages.Name = "grdExistingPackages"
        Me.grdExistingPackages.Size = New System.Drawing.Size(1178, 93)
        Me.grdExistingPackages.TabIndex = 5
        Me.grdExistingPackages.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GrdViewExistingPackages, Me.GrdViewExistingPackagesDetail})
        '
        'menuGridRightClick
        '
        Me.menuGridRightClick.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.menuGridRightClick.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuRemoveItem})
        Me.menuGridRightClick.Name = "menuGridRightClick"
        Me.menuGridRightClick.Size = New System.Drawing.Size(139, 30)
        '
        'menuRemoveItem
        '
        Me.menuRemoveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.menuRemoveItem.Name = "menuRemoveItem"
        Me.menuRemoveItem.Size = New System.Drawing.Size(138, 26)
        Me.menuRemoveItem.Text = "Remove"
        '
        'GrdViewExistingPackages
        '
        Me.GrdViewExistingPackages.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPackageID, Me.colExistPackBarcode, Me.colExistPackItemCount, Me.colExistPackDate, Me.colExistPackRecipientFirst, Me.colExistPackRecipientLast, Me.colExistPackCompany, Me.colExistPackCountry, Me.colExistPackCity, Me.colExistPackAddress, Me.colExistPackLeavingDate})
        Me.GrdViewExistingPackages.GridControl = Me.grdExistingPackages
        Me.GrdViewExistingPackages.Name = "GrdViewExistingPackages"
        Me.GrdViewExistingPackages.OptionsBehavior.Editable = False
        Me.GrdViewExistingPackages.OptionsBehavior.ReadOnly = True
        Me.GrdViewExistingPackages.OptionsView.ShowGroupPanel = False
        '
        'colPackageID
        '
        Me.colPackageID.Caption = "PACKAGE_ID"
        Me.colPackageID.FieldName = "PARENT_CONTAINER_ID"
        Me.colPackageID.Name = "colPackageID"
        Me.colPackageID.OptionsColumn.ShowInCustomizationForm = False
        '
        'colExistPackBarcode
        '
        Me.colExistPackBarcode.Caption = "BARCODE"
        Me.colExistPackBarcode.FieldName = "BARCODE"
        Me.colExistPackBarcode.Name = "colExistPackBarcode"
        Me.colExistPackBarcode.Visible = True
        Me.colExistPackBarcode.VisibleIndex = 0
        '
        'colExistPackItemCount
        '
        Me.colExistPackItemCount.Caption = "# OF ITEMS"
        Me.colExistPackItemCount.FieldName = "CONTAINER_COUNT"
        Me.colExistPackItemCount.Name = "colExistPackItemCount"
        Me.colExistPackItemCount.Visible = True
        Me.colExistPackItemCount.VisibleIndex = 1
        '
        'colExistPackDate
        '
        Me.colExistPackDate.Caption = "DATE"
        Me.colExistPackDate.DisplayFormat.FormatString = "dd-MMM-yy"
        Me.colExistPackDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colExistPackDate.FieldName = "DATE_MODIFIED"
        Me.colExistPackDate.Name = "colExistPackDate"
        Me.colExistPackDate.Visible = True
        Me.colExistPackDate.VisibleIndex = 2
        '
        'colExistPackRecipientFirst
        '
        Me.colExistPackRecipientFirst.Caption = "RECIPIENT_FIRSTNAME"
        Me.colExistPackRecipientFirst.FieldName = "FIRST_NAME"
        Me.colExistPackRecipientFirst.Name = "colExistPackRecipientFirst"
        Me.colExistPackRecipientFirst.Visible = True
        Me.colExistPackRecipientFirst.VisibleIndex = 3
        '
        'colExistPackRecipientLast
        '
        Me.colExistPackRecipientLast.Caption = "RECIPIENT_LASTNAME"
        Me.colExistPackRecipientLast.FieldName = "LAST_NAME"
        Me.colExistPackRecipientLast.Name = "colExistPackRecipientLast"
        Me.colExistPackRecipientLast.Visible = True
        Me.colExistPackRecipientLast.VisibleIndex = 4
        '
        'colExistPackCompany
        '
        Me.colExistPackCompany.Caption = "COMPANY"
        Me.colExistPackCompany.FieldName = "COMPANY"
        Me.colExistPackCompany.Name = "colExistPackCompany"
        Me.colExistPackCompany.Visible = True
        Me.colExistPackCompany.VisibleIndex = 5
        '
        'colExistPackCountry
        '
        Me.colExistPackCountry.Caption = "COUNTRY"
        Me.colExistPackCountry.FieldName = "COUNTRY_NAME"
        Me.colExistPackCountry.Name = "colExistPackCountry"
        Me.colExistPackCountry.Visible = True
        Me.colExistPackCountry.VisibleIndex = 6
        '
        'colExistPackCity
        '
        Me.colExistPackCity.Caption = "CITY"
        Me.colExistPackCity.FieldName = "CITY"
        Me.colExistPackCity.Name = "colExistPackCity"
        Me.colExistPackCity.Visible = True
        Me.colExistPackCity.VisibleIndex = 7
        '
        'colExistPackAddress
        '
        Me.colExistPackAddress.Caption = "MAIL_ADDRESS"
        Me.colExistPackAddress.FieldName = "MAIL_ADDRESS"
        Me.colExistPackAddress.Name = "colExistPackAddress"
        '
        'colExistPackLeavingDate
        '
        Me.colExistPackLeavingDate.Caption = "LEAVING_DATE"
        Me.colExistPackLeavingDate.FieldName = "LEAVING_DATE"
        Me.colExistPackLeavingDate.Name = "colExistPackLeavingDate"
        Me.colExistPackLeavingDate.OptionsColumn.ShowInCustomizationForm = False
        '
        'GrdViewPendingItemsDetail
        '
        Me.GrdViewPendingItemsDetail.GridControl = Me.grdPendingItems
        Me.GrdViewPendingItemsDetail.Name = "GrdViewPendingItemsDetail"
        '
        'grdPendingItems
        '
        Me.grdPendingItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdPendingItems.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode6.LevelTemplate = Me.GrdViewPendingItemsDetail
        GridLevelNode6.RelationName = "Level1"
        Me.grdPendingItems.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode6})
        Me.grdPendingItems.Location = New System.Drawing.Point(2, 24)
        Me.grdPendingItems.MainView = Me.GrdViewPendingItems
        Me.grdPendingItems.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdPendingItems.Name = "grdPendingItems"
        Me.grdPendingItems.Size = New System.Drawing.Size(1178, 156)
        Me.grdPendingItems.TabIndex = 5
        Me.grdPendingItems.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GrdViewPendingItems, Me.GrdViewPendingItemsDetail})
        '
        'GrdViewPendingItems
        '
        Me.GrdViewPendingItems.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colContainerID, Me.colBARCODE, Me.colTYPE_NAME, Me.colDESTINATION, Me.colRECIPIENTFIRSTNAME, Me.colRECIPIENTLASTNAME, Me.colEmailAddress, Me.colREQUEST_NR, Me.colTYPE_ID, Me.colRECEIVER_ID, Me.colCOMPOUND, Me.colFACTORY, Me.colDlvryForm, Me.colSite, Me.colAddress, Me.colProtocol, Me.colDestRackGroupName, Me.colFulfillmentDate, Me.colLeavingDate})
        Me.GrdViewPendingItems.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GrdViewPendingItems.GridControl = Me.grdPendingItems
        Me.GrdViewPendingItems.Name = "GrdViewPendingItems"
        Me.GrdViewPendingItems.OptionsBehavior.Editable = False
        Me.GrdViewPendingItems.OptionsBehavior.ReadOnly = True
        Me.GrdViewPendingItems.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GrdViewPendingItems.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.GrdViewPendingItems.OptionsSelection.EnableAppearanceHideSelection = False
        Me.GrdViewPendingItems.OptionsSelection.MultiSelect = True
        Me.GrdViewPendingItems.OptionsSelection.UseIndicatorForSelection = False
        Me.GrdViewPendingItems.OptionsView.ShowGroupPanel = False
        '
        'colContainerID
        '
        Me.colContainerID.Caption = "CONTAINER_ID"
        Me.colContainerID.FieldName = "CONTAINER_ID"
        Me.colContainerID.Name = "colContainerID"
        Me.colContainerID.OptionsColumn.ShowInCustomizationForm = False
        '
        'colBARCODE
        '
        Me.colBARCODE.Caption = "BARCODE"
        Me.colBARCODE.FieldName = "BARCODE"
        Me.colBARCODE.Name = "colBARCODE"
        Me.colBARCODE.Visible = True
        Me.colBARCODE.VisibleIndex = 0
        '
        'colTYPE_NAME
        '
        Me.colTYPE_NAME.Caption = "TYPE_NAME"
        Me.colTYPE_NAME.FieldName = "TYPE_NAME"
        Me.colTYPE_NAME.Name = "colTYPE_NAME"
        '
        'colDESTINATION
        '
        Me.colDESTINATION.Caption = "DESTINATION"
        Me.colDESTINATION.FieldName = "COUNTRY_NAME"
        Me.colDESTINATION.Name = "colDESTINATION"
        '
        'colRECIPIENTFIRSTNAME
        '
        Me.colRECIPIENTFIRSTNAME.Caption = "FIRST_NAME"
        Me.colRECIPIENTFIRSTNAME.FieldName = "FIRST_NAME"
        Me.colRECIPIENTFIRSTNAME.Name = "colRECIPIENTFIRSTNAME"
        Me.colRECIPIENTFIRSTNAME.Visible = True
        Me.colRECIPIENTFIRSTNAME.VisibleIndex = 1
        '
        'colRECIPIENTLASTNAME
        '
        Me.colRECIPIENTLASTNAME.Caption = "LAST_NAME"
        Me.colRECIPIENTLASTNAME.FieldName = "LAST_NAME"
        Me.colRECIPIENTLASTNAME.Name = "colRECIPIENTLASTNAME"
        Me.colRECIPIENTLASTNAME.Visible = True
        Me.colRECIPIENTLASTNAME.VisibleIndex = 2
        '
        'colEmailAddress
        '
        Me.colEmailAddress.Caption = "EMAIL"
        Me.colEmailAddress.FieldName = "EMAIL"
        Me.colEmailAddress.Name = "colEmailAddress"
        '
        'colREQUEST_NR
        '
        Me.colREQUEST_NR.Caption = "REQUEST_NUMBER"
        Me.colREQUEST_NR.FieldName = "ORDER_NAME"
        Me.colREQUEST_NR.Name = "colREQUEST_NR"
        Me.colREQUEST_NR.Visible = True
        Me.colREQUEST_NR.VisibleIndex = 3
        '
        'colTYPE_ID
        '
        Me.colTYPE_ID.Caption = "TYPE_ID"
        Me.colTYPE_ID.FieldName = "TYPE_ID"
        Me.colTYPE_ID.Name = "colTYPE_ID"
        '
        'colRECEIVER_ID
        '
        Me.colRECEIVER_ID.Caption = "RECEIVER_ID"
        Me.colRECEIVER_ID.FieldName = "RECEIVER_ID"
        Me.colRECEIVER_ID.Name = "colRECEIVER_ID"
        '
        'colCOMPOUND
        '
        Me.colCOMPOUND.Caption = "COMPOUND NAME"
        Me.colCOMPOUND.FieldName = "SAMPLE_NAME"
        Me.colCOMPOUND.Name = "colCOMPOUND"
        '
        'colFACTORY
        '
        Me.colFACTORY.Caption = "FACTORY"
        Me.colFACTORY.FieldName = "LOCATION_NAME"
        Me.colFACTORY.Name = "colFACTORY"
        '
        'colDlvryForm
        '
        Me.colDlvryForm.Caption = "DELIVERY FORM"
        Me.colDlvryForm.FieldName = "DELIVERY_FORM_TYPE_NAME"
        Me.colDlvryForm.Name = "colDlvryForm"
        '
        'colSite
        '
        Me.colSite.Caption = "SITE_CODE"
        Me.colSite.CustomizationCaption = "SITE"
        Me.colSite.FieldName = "SITE_CODE"
        Me.colSite.Name = "colSite"
        Me.colSite.Visible = True
        Me.colSite.VisibleIndex = 4
        '
        'colAddress
        '
        Me.colAddress.Caption = "MAIL_ADDRESS"
        Me.colAddress.FieldName = "MAIL_ADDRESS"
        Me.colAddress.Name = "colAddress"
        Me.colAddress.Visible = True
        Me.colAddress.VisibleIndex = 5
        '
        'colProtocol
        '
        Me.colProtocol.Caption = "PROTOCOL_NAME"
        Me.colProtocol.CustomizationCaption = "Protocol"
        Me.colProtocol.FieldName = "PROTOCOL_NAME"
        Me.colProtocol.Name = "colProtocol"
        Me.colProtocol.Visible = True
        Me.colProtocol.VisibleIndex = 6
        '
        'colDestRackGroupName
        '
        Me.colDestRackGroupName.Caption = "DESTINATION_RACK_GROUP_NAME"
        Me.colDestRackGroupName.FieldName = "DESTINATION_RACK_GROUP_NAME"
        Me.colDestRackGroupName.Name = "colDestRackGroupName"
        Me.colDestRackGroupName.Visible = True
        Me.colDestRackGroupName.VisibleIndex = 7
        '
        'colFulfillmentDate
        '
        Me.colFulfillmentDate.Caption = "FULFILLMENT_DATE"
        Me.colFulfillmentDate.DisplayFormat.FormatString = "dd-MMM-yy"
        Me.colFulfillmentDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colFulfillmentDate.FieldName = "FULFILLMENT_DATE"
        Me.colFulfillmentDate.Name = "colFulfillmentDate"
        Me.colFulfillmentDate.Visible = True
        Me.colFulfillmentDate.VisibleIndex = 8
        '
        'colLeavingDate
        '
        Me.colLeavingDate.Caption = "LEAVING_DATE"
        Me.colLeavingDate.FieldName = "LEAVING_DATE"
        Me.colLeavingDate.Name = "colLeavingDate"
        Me.colLeavingDate.OptionsColumn.ShowInCustomizationForm = False
        '
        'GridView6
        '
        Me.GridView6.GridControl = Me.grdFinalPackageDetails
        Me.GridView6.Name = "GridView6"
        Me.GridView6.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp
        Me.GridView6.OptionsSelection.MultiSelect = True
        '
        'grdFinalPackageDetails
        '
        Me.grdFinalPackageDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdFinalPackageDetails.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode7.LevelTemplate = Me.GridView6
        GridLevelNode7.RelationName = "Level1"
        Me.grdFinalPackageDetails.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode7})
        Me.grdFinalPackageDetails.Location = New System.Drawing.Point(2, 24)
        Me.grdFinalPackageDetails.MainView = Me.GrdViewFinalPackage
        Me.grdFinalPackageDetails.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdFinalPackageDetails.Name = "grdFinalPackageDetails"
        Me.grdFinalPackageDetails.Size = New System.Drawing.Size(1170, 307)
        Me.grdFinalPackageDetails.TabIndex = 5
        Me.grdFinalPackageDetails.ToolTipController = Me.FinalPackageToolTip
        Me.grdFinalPackageDetails.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GrdViewFinalPackage, Me.GridView6})
        '
        'GrdViewFinalPackage
        '
        Me.GrdViewFinalPackage.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn56, Me.GridColumn57, Me.GridColumn58, Me.GridColumn59, Me.GridColumn60, Me.GridColumn61, Me.GridColumn62, Me.GridColumn63, Me.GridColumn64, Me.GridColumn65, Me.GridColumn66, Me.GridColumn67, Me.GridColumn68, Me.GridColumn69})
        Me.GrdViewFinalPackage.GridControl = Me.grdFinalPackageDetails
        Me.GrdViewFinalPackage.Name = "GrdViewFinalPackage"
        Me.GrdViewFinalPackage.OptionsBehavior.Editable = False
        Me.GrdViewFinalPackage.OptionsBehavior.ReadOnly = True
        Me.GrdViewFinalPackage.OptionsSelection.MultiSelect = True
        Me.GrdViewFinalPackage.OptionsView.ShowGroupPanel = False
        '
        'GridColumn56
        '
        Me.GridColumn56.Caption = "CONTAINER_ID"
        Me.GridColumn56.FieldName = "CONTAINER_ID"
        Me.GridColumn56.Name = "GridColumn56"
        Me.GridColumn56.OptionsColumn.ShowInCustomizationForm = False
        '
        'GridColumn57
        '
        Me.GridColumn57.Caption = "BARCODE"
        Me.GridColumn57.FieldName = "BARCODE"
        Me.GridColumn57.Name = "GridColumn57"
        Me.GridColumn57.Visible = True
        Me.GridColumn57.VisibleIndex = 0
        '
        'GridColumn58
        '
        Me.GridColumn58.Caption = "RECIPIENT_FIRSTNAME"
        Me.GridColumn58.FieldName = "FIRST_NAME"
        Me.GridColumn58.Name = "GridColumn58"
        Me.GridColumn58.Visible = True
        Me.GridColumn58.VisibleIndex = 1
        '
        'GridColumn59
        '
        Me.GridColumn59.Caption = "RECIPIENT_LASTNAME"
        Me.GridColumn59.FieldName = "LAST_NAME"
        Me.GridColumn59.Name = "GridColumn59"
        Me.GridColumn59.Visible = True
        Me.GridColumn59.VisibleIndex = 2
        '
        'GridColumn60
        '
        Me.GridColumn60.Caption = "COMPANY"
        Me.GridColumn60.FieldName = "COMPANY"
        Me.GridColumn60.Name = "GridColumn60"
        Me.GridColumn60.Visible = True
        Me.GridColumn60.VisibleIndex = 3
        '
        'GridColumn61
        '
        Me.GridColumn61.Caption = "ADDRESS"
        Me.GridColumn61.FieldName = "MAIL_ADDRESS"
        Me.GridColumn61.Name = "GridColumn61"
        Me.GridColumn61.Visible = True
        Me.GridColumn61.VisibleIndex = 4
        '
        'GridColumn62
        '
        Me.GridColumn62.Caption = "CITY"
        Me.GridColumn62.FieldName = "CITY"
        Me.GridColumn62.Name = "GridColumn62"
        Me.GridColumn62.Visible = True
        Me.GridColumn62.VisibleIndex = 5
        '
        'GridColumn63
        '
        Me.GridColumn63.Caption = "COUNTRY"
        Me.GridColumn63.FieldName = "COUNTRY_NAME"
        Me.GridColumn63.Name = "GridColumn63"
        Me.GridColumn63.Visible = True
        Me.GridColumn63.VisibleIndex = 6
        '
        'GridColumn64
        '
        Me.GridColumn64.Caption = "SITE"
        Me.GridColumn64.FieldName = "SITE_CODE"
        Me.GridColumn64.Name = "GridColumn64"
        Me.GridColumn64.Visible = True
        Me.GridColumn64.VisibleIndex = 7
        '
        'GridColumn65
        '
        Me.GridColumn65.Caption = "EMAIL"
        Me.GridColumn65.FieldName = "EMAIL"
        Me.GridColumn65.Name = "GridColumn65"
        Me.GridColumn65.Visible = True
        Me.GridColumn65.VisibleIndex = 8
        '
        'GridColumn66
        '
        Me.GridColumn66.Caption = "REQUEST_NUMBER"
        Me.GridColumn66.FieldName = "ORDER_NAME"
        Me.GridColumn66.Name = "GridColumn66"
        Me.GridColumn66.Visible = True
        Me.GridColumn66.VisibleIndex = 9
        '
        'GridColumn67
        '
        Me.GridColumn67.Caption = "DELIVERY_FORM"
        Me.GridColumn67.FieldName = "DELIVERY_FORM_TYPE_NAME"
        Me.GridColumn67.Name = "GridColumn67"
        Me.GridColumn67.Visible = True
        Me.GridColumn67.VisibleIndex = 10
        '
        'GridColumn68
        '
        Me.GridColumn68.Caption = "PROTOCOL_NAME"
        Me.GridColumn68.FieldName = "PROTOCOL_NAME"
        Me.GridColumn68.Name = "GridColumn68"
        Me.GridColumn68.Visible = True
        Me.GridColumn68.VisibleIndex = 11
        '
        'GridColumn69
        '
        Me.GridColumn69.Caption = "DESTINATION_RACK_GROUP_NAME"
        Me.GridColumn69.FieldName = "DESTINATION_RACK_GROUP_NAME"
        Me.GridColumn69.Name = "GridColumn69"
        Me.GridColumn69.Visible = True
        Me.GridColumn69.VisibleIndex = 12
        '
        'FinalPackageToolTip
        '
        '
        'GridView5
        '
        Me.GridView5.GridControl = Me.grdFinalizedPackages
        Me.GridView5.Name = "GridView5"
        '
        'grdFinalizedPackages
        '
        Me.grdFinalizedPackages.ContextMenuStrip = Me.menuFinalizedPackagesRightClick
        Me.grdFinalizedPackages.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdFinalizedPackages.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode8.LevelTemplate = Me.GridView5
        GridLevelNode8.RelationName = "Level1"
        Me.grdFinalizedPackages.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode8})
        Me.grdFinalizedPackages.Location = New System.Drawing.Point(2, 24)
        Me.grdFinalizedPackages.MainView = Me.GrdViewFinalizedPackages
        Me.grdFinalizedPackages.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdFinalizedPackages.Name = "grdFinalizedPackages"
        Me.grdFinalizedPackages.Size = New System.Drawing.Size(1170, 217)
        Me.grdFinalizedPackages.TabIndex = 6
        Me.grdFinalizedPackages.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GrdViewFinalizedPackages, Me.GridView5})
        '
        'menuFinalizedPackagesRightClick
        '
        Me.menuFinalizedPackagesRightClick.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.menuFinalizedPackagesRightClick.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1})
        Me.menuFinalizedPackagesRightClick.Name = "menuGridRightClick"
        Me.menuFinalizedPackagesRightClick.Size = New System.Drawing.Size(155, 30)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(154, 26)
        Me.ToolStripMenuItem1.Text = "Print Label"
        '
        'GrdViewFinalizedPackages
        '
        Me.GrdViewFinalizedPackages.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn37, Me.GridColumn38, Me.GridColumn47, Me.GridColumn48, Me.GridColumn49, Me.GridColumn50, Me.GridColumn51, Me.GridColumn52, Me.GridColumn53, Me.GridColumn54, Me.GridColumn55, Me.GridColumn40})
        Me.GrdViewFinalizedPackages.GridControl = Me.grdFinalizedPackages
        Me.GrdViewFinalizedPackages.Name = "GrdViewFinalizedPackages"
        Me.GrdViewFinalizedPackages.OptionsBehavior.Editable = False
        Me.GrdViewFinalizedPackages.OptionsBehavior.ReadOnly = True
        Me.GrdViewFinalizedPackages.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GrdViewFinalizedPackages.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.GrdViewFinalizedPackages.OptionsSelection.EnableAppearanceHideSelection = False
        Me.GrdViewFinalizedPackages.OptionsSelection.MultiSelect = True
        Me.GrdViewFinalizedPackages.OptionsSelection.UseIndicatorForSelection = False
        Me.GrdViewFinalizedPackages.OptionsView.ShowGroupPanel = False
        '
        'GridColumn37
        '
        Me.GridColumn37.Caption = "CONTAINER_ID"
        Me.GridColumn37.FieldName = "PARENT_CONTAINER_ID"
        Me.GridColumn37.Name = "GridColumn37"
        Me.GridColumn37.OptionsColumn.ShowInCustomizationForm = False
        Me.GridColumn37.Width = 89
        '
        'GridColumn38
        '
        Me.GridColumn38.Caption = "BARCODE"
        Me.GridColumn38.FieldName = "BARCODE"
        Me.GridColumn38.Name = "GridColumn38"
        Me.GridColumn38.Visible = True
        Me.GridColumn38.VisibleIndex = 0
        Me.GridColumn38.Width = 78
        '
        'GridColumn47
        '
        Me.GridColumn47.Caption = "RECIPIENT_FIRSTNAME"
        Me.GridColumn47.FieldName = "FIRST_NAME"
        Me.GridColumn47.Name = "GridColumn47"
        Me.GridColumn47.OptionsColumn.ShowInCustomizationForm = False
        Me.GridColumn47.Visible = True
        Me.GridColumn47.VisibleIndex = 1
        Me.GridColumn47.Width = 78
        '
        'GridColumn48
        '
        Me.GridColumn48.Caption = "RECIPIENT_LASTNAME"
        Me.GridColumn48.FieldName = "LAST_NAME"
        Me.GridColumn48.Name = "GridColumn48"
        Me.GridColumn48.Visible = True
        Me.GridColumn48.VisibleIndex = 2
        Me.GridColumn48.Width = 78
        '
        'GridColumn49
        '
        Me.GridColumn49.Caption = "COMPANY"
        Me.GridColumn49.FieldName = "COMPANY"
        Me.GridColumn49.Name = "GridColumn49"
        Me.GridColumn49.Visible = True
        Me.GridColumn49.VisibleIndex = 3
        Me.GridColumn49.Width = 78
        '
        'GridColumn50
        '
        Me.GridColumn50.Caption = "COUNTRY"
        Me.GridColumn50.FieldName = "COUNTRY_NAME"
        Me.GridColumn50.Name = "GridColumn50"
        Me.GridColumn50.Visible = True
        Me.GridColumn50.VisibleIndex = 4
        Me.GridColumn50.Width = 78
        '
        'GridColumn51
        '
        Me.GridColumn51.Caption = "# OF ITEMS"
        Me.GridColumn51.FieldName = "CONTAINER_COUNT"
        Me.GridColumn51.Name = "GridColumn51"
        Me.GridColumn51.Visible = True
        Me.GridColumn51.VisibleIndex = 5
        Me.GridColumn51.Width = 78
        '
        'GridColumn52
        '
        Me.GridColumn52.Caption = "DATE"
        Me.GridColumn52.DisplayFormat.FormatString = "dd-MMM-yy"
        Me.GridColumn52.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn52.FieldName = "DATE_MODIFIED"
        Me.GridColumn52.Name = "GridColumn52"
        Me.GridColumn52.Visible = True
        Me.GridColumn52.VisibleIndex = 6
        Me.GridColumn52.Width = 88
        '
        'GridColumn53
        '
        Me.GridColumn53.Caption = "MAIL_ADDRESS"
        Me.GridColumn53.FieldName = "MAIL_ADDRESS"
        Me.GridColumn53.Name = "GridColumn53"
        Me.GridColumn53.Visible = True
        Me.GridColumn53.VisibleIndex = 7
        '
        'GridColumn54
        '
        Me.GridColumn54.Caption = "SITE_CODE"
        Me.GridColumn54.FieldName = "SITE_CODE"
        Me.GridColumn54.Name = "GridColumn54"
        Me.GridColumn54.Visible = True
        Me.GridColumn54.VisibleIndex = 8
        '
        'GridColumn55
        '
        Me.GridColumn55.Caption = "CITY"
        Me.GridColumn55.FieldName = "CITY"
        Me.GridColumn55.Name = "GridColumn55"
        Me.GridColumn55.Visible = True
        Me.GridColumn55.VisibleIndex = 9
        '
        'GridColumn40
        '
        Me.GridColumn40.Caption = "DATE_CREATED"
        Me.GridColumn40.DisplayFormat.FormatString = "dd-MMM-yy"
        Me.GridColumn40.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn40.FieldName = "DATE_CREATED"
        Me.GridColumn40.Name = "GridColumn40"
        Me.GridColumn40.Visible = True
        Me.GridColumn40.VisibleIndex = 10
        '
        'GrdViewActiveShipmentDetails
        '
        Me.GrdViewActiveShipmentDetails.GridControl = Me.grdActiveShipmentDetails
        Me.GrdViewActiveShipmentDetails.Name = "GrdViewActiveShipmentDetails"
        '
        'grdActiveShipmentDetails
        '
        Me.grdActiveShipmentDetails.ContextMenuStrip = Me.menuGridRightClick
        Me.grdActiveShipmentDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdActiveShipmentDetails.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode9.LevelTemplate = Me.GrdViewActiveShipmentDetails
        GridLevelNode9.RelationName = "Level1"
        Me.grdActiveShipmentDetails.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode9})
        Me.grdActiveShipmentDetails.Location = New System.Drawing.Point(2, 24)
        Me.grdActiveShipmentDetails.MainView = Me.GrdViewActiveShipment
        Me.grdActiveShipmentDetails.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdActiveShipmentDetails.Name = "grdActiveShipmentDetails"
        Me.grdActiveShipmentDetails.Size = New System.Drawing.Size(1177, 278)
        Me.grdActiveShipmentDetails.TabIndex = 5
        Me.grdActiveShipmentDetails.ToolTipController = Me.ActiveShipmentToolTip
        Me.grdActiveShipmentDetails.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GrdViewActiveShipment, Me.GrdViewActiveShipmentDetails})
        '
        'GrdViewActiveShipment
        '
        Me.GrdViewActiveShipment.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colActShipContainerID, Me.colActShipPackageBarcode, Me.colActShipFirst, Me.colActShipLast, Me.colActShipCompany, Me.colActShipAddress, Me.colActShipCountry, Me.colActShipDate, Me.colActShipMailAddress, Me.colActShipSiteCode, Me.colActShipCity})
        Me.GrdViewActiveShipment.GridControl = Me.grdActiveShipmentDetails
        Me.GrdViewActiveShipment.Name = "GrdViewActiveShipment"
        Me.GrdViewActiveShipment.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GrdViewActiveShipment.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GrdViewActiveShipment.OptionsBehavior.Editable = False
        Me.GrdViewActiveShipment.OptionsBehavior.ReadOnly = True
        Me.GrdViewActiveShipment.OptionsSelection.MultiSelect = True
        Me.GrdViewActiveShipment.OptionsView.ShowGroupPanel = False
        '
        'colActShipContainerID
        '
        Me.colActShipContainerID.Caption = "CONTAINER_ID"
        Me.colActShipContainerID.FieldName = "PARENT_CONTAINER_ID"
        Me.colActShipContainerID.Name = "colActShipContainerID"
        Me.colActShipContainerID.OptionsColumn.ShowInCustomizationForm = False
        '
        'colActShipPackageBarcode
        '
        Me.colActShipPackageBarcode.Caption = "PACKAGE_BARCODE"
        Me.colActShipPackageBarcode.FieldName = "BARCODE"
        Me.colActShipPackageBarcode.Name = "colActShipPackageBarcode"
        Me.colActShipPackageBarcode.Visible = True
        Me.colActShipPackageBarcode.VisibleIndex = 0
        '
        'colActShipFirst
        '
        Me.colActShipFirst.Caption = "RECIPIENT_FIRSTNAME"
        Me.colActShipFirst.FieldName = "FIRST_NAME"
        Me.colActShipFirst.Name = "colActShipFirst"
        Me.colActShipFirst.Visible = True
        Me.colActShipFirst.VisibleIndex = 1
        '
        'colActShipLast
        '
        Me.colActShipLast.Caption = "RECIPIENT_LASTNAME"
        Me.colActShipLast.FieldName = "LAST_NAME"
        Me.colActShipLast.Name = "colActShipLast"
        Me.colActShipLast.Visible = True
        Me.colActShipLast.VisibleIndex = 2
        '
        'colActShipCompany
        '
        Me.colActShipCompany.Caption = "COMPANY"
        Me.colActShipCompany.FieldName = "COMPANY"
        Me.colActShipCompany.Name = "colActShipCompany"
        Me.colActShipCompany.Visible = True
        Me.colActShipCompany.VisibleIndex = 3
        '
        'colActShipAddress
        '
        Me.colActShipAddress.Caption = "ADDRESS"
        Me.colActShipAddress.FieldName = "MAIL_ADDRESS"
        Me.colActShipAddress.Name = "colActShipAddress"
        Me.colActShipAddress.Visible = True
        Me.colActShipAddress.VisibleIndex = 4
        '
        'colActShipCountry
        '
        Me.colActShipCountry.Caption = "COUNTRY"
        Me.colActShipCountry.FieldName = "COUNTRY_NAME"
        Me.colActShipCountry.Name = "colActShipCountry"
        Me.colActShipCountry.Visible = True
        Me.colActShipCountry.VisibleIndex = 5
        '
        'colActShipDate
        '
        Me.colActShipDate.Caption = "DATE"
        Me.colActShipDate.DisplayFormat.FormatString = "dd-MMM-yy"
        Me.colActShipDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colActShipDate.FieldName = "DATE_MODIFIED"
        Me.colActShipDate.Name = "colActShipDate"
        '
        'colActShipMailAddress
        '
        Me.colActShipMailAddress.Caption = "MAIL_ADDRESS"
        Me.colActShipMailAddress.FieldName = "MAIL_ADDRESS"
        Me.colActShipMailAddress.Name = "colActShipMailAddress"
        Me.colActShipMailAddress.Visible = True
        Me.colActShipMailAddress.VisibleIndex = 6
        '
        'colActShipSiteCode
        '
        Me.colActShipSiteCode.Caption = "SITE_CODE"
        Me.colActShipSiteCode.FieldName = "SITE_CODE"
        Me.colActShipSiteCode.Name = "colActShipSiteCode"
        Me.colActShipSiteCode.Visible = True
        Me.colActShipSiteCode.VisibleIndex = 7
        '
        'colActShipCity
        '
        Me.colActShipCity.Caption = "CITY"
        Me.colActShipCity.FieldName = "CITY"
        Me.colActShipCity.Name = "colActShipCity"
        Me.colActShipCity.Visible = True
        Me.colActShipCity.VisibleIndex = 8
        '
        'ActiveShipmentToolTip
        '
        '
        'GridView13
        '
        Me.GridView13.GridControl = Me.grdExistingShipments
        Me.GridView13.Name = "GridView13"
        '
        'grdExistingShipments
        '
        Me.grdExistingShipments.ContextMenuStrip = Me.menuGridRightClick
        Me.grdExistingShipments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdExistingShipments.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode10.LevelTemplate = Me.GridView13
        GridLevelNode10.RelationName = "Level1"
        Me.grdExistingShipments.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode10})
        Me.grdExistingShipments.Location = New System.Drawing.Point(2, 24)
        Me.grdExistingShipments.MainView = Me.grdViewExistingShipments
        Me.grdExistingShipments.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdExistingShipments.Name = "grdExistingShipments"
        Me.grdExistingShipments.Size = New System.Drawing.Size(1177, 70)
        Me.grdExistingShipments.TabIndex = 5
        Me.grdExistingShipments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grdViewExistingShipments, Me.GridView13})
        '
        'grdViewExistingShipments
        '
        Me.grdViewExistingShipments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colExistShipShipmentID, Me.colExistShipName, Me.colExistShipFirst, Me.colExistShipLast, Me.colExistShipCompany, Me.colExistShipCountry, Me.colExistShipPackageCount, Me.colExistShipItemCount, Me.colExistShipDate, Me.colExistShipReceiverID, Me.colExistSiteCode, Me.colExistShipCity, Me.colExistShipPriority, Me.colExistShipExtRefName})
        Me.grdViewExistingShipments.GridControl = Me.grdExistingShipments
        Me.grdViewExistingShipments.Name = "grdViewExistingShipments"
        Me.grdViewExistingShipments.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grdViewExistingShipments.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grdViewExistingShipments.OptionsBehavior.Editable = False
        Me.grdViewExistingShipments.OptionsBehavior.ReadOnly = True
        Me.grdViewExistingShipments.OptionsView.ShowGroupPanel = False
        '
        'colExistShipShipmentID
        '
        Me.colExistShipShipmentID.Caption = "SHIPMENT_ID"
        Me.colExistShipShipmentID.FieldName = "SHIPMENT_ID"
        Me.colExistShipShipmentID.Name = "colExistShipShipmentID"
        Me.colExistShipShipmentID.OptionsColumn.ShowInCustomizationForm = False
        '
        'colExistShipName
        '
        Me.colExistShipName.Caption = "SHIPMENT"
        Me.colExistShipName.FieldName = "SHIPMENT_NAME"
        Me.colExistShipName.Name = "colExistShipName"
        Me.colExistShipName.Visible = True
        Me.colExistShipName.VisibleIndex = 0
        '
        'colExistShipFirst
        '
        Me.colExistShipFirst.Caption = "RECIPIENT_FIRSTNAME"
        Me.colExistShipFirst.FieldName = "FIRST_NAME"
        Me.colExistShipFirst.Name = "colExistShipFirst"
        Me.colExistShipFirst.Visible = True
        Me.colExistShipFirst.VisibleIndex = 1
        '
        'colExistShipLast
        '
        Me.colExistShipLast.Caption = "RECIPIENT_LASTNAME"
        Me.colExistShipLast.FieldName = "LAST_NAME"
        Me.colExistShipLast.Name = "colExistShipLast"
        Me.colExistShipLast.Visible = True
        Me.colExistShipLast.VisibleIndex = 2
        '
        'colExistShipCompany
        '
        Me.colExistShipCompany.Caption = "COMPANY"
        Me.colExistShipCompany.FieldName = "COMPANY"
        Me.colExistShipCompany.Name = "colExistShipCompany"
        Me.colExistShipCompany.Visible = True
        Me.colExistShipCompany.VisibleIndex = 3
        '
        'colExistShipCountry
        '
        Me.colExistShipCountry.Caption = "COUNTRY"
        Me.colExistShipCountry.FieldName = "COUNTRY_NAME"
        Me.colExistShipCountry.Name = "colExistShipCountry"
        Me.colExistShipCountry.Visible = True
        Me.colExistShipCountry.VisibleIndex = 4
        '
        'colExistShipPackageCount
        '
        Me.colExistShipPackageCount.Caption = "PACKAGE_COUNT"
        Me.colExistShipPackageCount.FieldName = "PACKAGE_COUNT"
        Me.colExistShipPackageCount.Name = "colExistShipPackageCount"
        Me.colExistShipPackageCount.Visible = True
        Me.colExistShipPackageCount.VisibleIndex = 5
        '
        'colExistShipItemCount
        '
        Me.colExistShipItemCount.Caption = "# OF ITEMS"
        Me.colExistShipItemCount.FieldName = "CONTAINER_COUNT"
        Me.colExistShipItemCount.Name = "colExistShipItemCount"
        Me.colExistShipItemCount.Visible = True
        Me.colExistShipItemCount.VisibleIndex = 6
        '
        'colExistShipDate
        '
        Me.colExistShipDate.Caption = "DATE"
        Me.colExistShipDate.DisplayFormat.FormatString = "dd-MMM-yy"
        Me.colExistShipDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colExistShipDate.FieldName = "DATE_MODIFIED"
        Me.colExistShipDate.Name = "colExistShipDate"
        Me.colExistShipDate.Visible = True
        Me.colExistShipDate.VisibleIndex = 7
        '
        'colExistShipReceiverID
        '
        Me.colExistShipReceiverID.Caption = "RECEIVER_ID"
        Me.colExistShipReceiverID.FieldName = "RECEIVER_ID"
        Me.colExistShipReceiverID.Name = "colExistShipReceiverID"
        '
        'colExistSiteCode
        '
        Me.colExistSiteCode.Caption = "SITE_CODE"
        Me.colExistSiteCode.FieldName = "SITE_CODE"
        Me.colExistSiteCode.Name = "colExistSiteCode"
        Me.colExistSiteCode.Visible = True
        Me.colExistSiteCode.VisibleIndex = 8
        '
        'colExistShipCity
        '
        Me.colExistShipCity.Caption = "CITY"
        Me.colExistShipCity.FieldName = "CITY"
        Me.colExistShipCity.Name = "colExistShipCity"
        Me.colExistShipCity.Visible = True
        Me.colExistShipCity.VisibleIndex = 9
        '
        'colExistShipPriority
        '
        Me.colExistShipPriority.Caption = "PRIORITY"
        Me.colExistShipPriority.FieldName = "PRIOIRTY"
        Me.colExistShipPriority.Name = "colExistShipPriority"
        '
        'colExistShipExtRefName
        '
        Me.colExistShipExtRefName.Caption = "CARRIER"
        Me.colExistShipExtRefName.FieldName = "EXTERNAL_REFERENCE_TYPE_NAME"
        Me.colExistShipExtRefName.Name = "colExistShipExtRefName"
        '
        'GridView15
        '
        Me.GridView15.GridControl = Me.grdPendingPackages
        Me.GridView15.Name = "GridView15"
        '
        'grdPendingPackages
        '
        Me.grdPendingPackages.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdPendingPackages.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode11.LevelTemplate = Me.GridView15
        GridLevelNode11.RelationName = "Level1"
        Me.grdPendingPackages.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode11})
        Me.grdPendingPackages.Location = New System.Drawing.Point(2, 24)
        Me.grdPendingPackages.MainView = Me.GrdViewPendingPackages
        Me.grdPendingPackages.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdPendingPackages.Name = "grdPendingPackages"
        Me.grdPendingPackages.Size = New System.Drawing.Size(1177, 141)
        Me.grdPendingPackages.TabIndex = 5
        Me.grdPendingPackages.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GrdViewPendingPackages, Me.GridView15})
        '
        'GrdViewPendingPackages
        '
        Me.GrdViewPendingPackages.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPendPackContainerID, Me.colPendPackBarcode, Me.colPendPackReceiverID, Me.colPendPackRecipientFirst, Me.colPendPackRecipientLast, Me.colPendPackCompany, Me.colPendPackCountry, Me.colPendPackItemCount, Me.colPendPackDate, Me.colPendPackMailAddress, Me.colPendPackSiteCode, Me.colPendPackCity})
        Me.GrdViewPendingPackages.GridControl = Me.grdPendingPackages
        Me.GrdViewPendingPackages.Name = "GrdViewPendingPackages"
        Me.GrdViewPendingPackages.OptionsBehavior.Editable = False
        Me.GrdViewPendingPackages.OptionsBehavior.ReadOnly = True
        Me.GrdViewPendingPackages.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GrdViewPendingPackages.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.GrdViewPendingPackages.OptionsSelection.EnableAppearanceHideSelection = False
        Me.GrdViewPendingPackages.OptionsSelection.MultiSelect = True
        Me.GrdViewPendingPackages.OptionsSelection.UseIndicatorForSelection = False
        Me.GrdViewPendingPackages.OptionsView.ShowGroupPanel = False
        '
        'colPendPackContainerID
        '
        Me.colPendPackContainerID.Caption = "CONTAINER_ID"
        Me.colPendPackContainerID.FieldName = "PARENT_CONTAINER_ID"
        Me.colPendPackContainerID.Name = "colPendPackContainerID"
        Me.colPendPackContainerID.OptionsColumn.ShowInCustomizationForm = False
        Me.colPendPackContainerID.Width = 89
        '
        'colPendPackBarcode
        '
        Me.colPendPackBarcode.Caption = "BARCODE"
        Me.colPendPackBarcode.FieldName = "BARCODE"
        Me.colPendPackBarcode.Name = "colPendPackBarcode"
        Me.colPendPackBarcode.Visible = True
        Me.colPendPackBarcode.VisibleIndex = 0
        Me.colPendPackBarcode.Width = 78
        '
        'colPendPackReceiverID
        '
        Me.colPendPackReceiverID.Caption = "RECEIVER_ID"
        Me.colPendPackReceiverID.FieldName = "RECEIVER_ID"
        Me.colPendPackReceiverID.Name = "colPendPackReceiverID"
        Me.colPendPackReceiverID.OptionsColumn.ShowInCustomizationForm = False
        '
        'colPendPackRecipientFirst
        '
        Me.colPendPackRecipientFirst.Caption = "RECIPIENT_FIRSTNAME"
        Me.colPendPackRecipientFirst.FieldName = "FIRST_NAME"
        Me.colPendPackRecipientFirst.Name = "colPendPackRecipientFirst"
        Me.colPendPackRecipientFirst.OptionsColumn.ShowInCustomizationForm = False
        Me.colPendPackRecipientFirst.Visible = True
        Me.colPendPackRecipientFirst.VisibleIndex = 1
        Me.colPendPackRecipientFirst.Width = 78
        '
        'colPendPackRecipientLast
        '
        Me.colPendPackRecipientLast.Caption = "RECIPIENT_LASTNAME"
        Me.colPendPackRecipientLast.FieldName = "LAST_NAME"
        Me.colPendPackRecipientLast.Name = "colPendPackRecipientLast"
        Me.colPendPackRecipientLast.Visible = True
        Me.colPendPackRecipientLast.VisibleIndex = 2
        Me.colPendPackRecipientLast.Width = 78
        '
        'colPendPackCompany
        '
        Me.colPendPackCompany.Caption = "COMPANY"
        Me.colPendPackCompany.FieldName = "COMPANY"
        Me.colPendPackCompany.Name = "colPendPackCompany"
        Me.colPendPackCompany.Visible = True
        Me.colPendPackCompany.VisibleIndex = 3
        Me.colPendPackCompany.Width = 78
        '
        'colPendPackCountry
        '
        Me.colPendPackCountry.Caption = "COUNTRY"
        Me.colPendPackCountry.FieldName = "COUNTRY_NAME"
        Me.colPendPackCountry.Name = "colPendPackCountry"
        Me.colPendPackCountry.Visible = True
        Me.colPendPackCountry.VisibleIndex = 4
        Me.colPendPackCountry.Width = 78
        '
        'colPendPackItemCount
        '
        Me.colPendPackItemCount.Caption = "# OF ITEMS"
        Me.colPendPackItemCount.FieldName = "CONTAINER_COUNT"
        Me.colPendPackItemCount.Name = "colPendPackItemCount"
        Me.colPendPackItemCount.Visible = True
        Me.colPendPackItemCount.VisibleIndex = 5
        Me.colPendPackItemCount.Width = 78
        '
        'colPendPackDate
        '
        Me.colPendPackDate.Caption = "DATE"
        Me.colPendPackDate.DisplayFormat.FormatString = "dd-MMM-yy"
        Me.colPendPackDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colPendPackDate.FieldName = "DATE_MODIFIED"
        Me.colPendPackDate.Name = "colPendPackDate"
        Me.colPendPackDate.Visible = True
        Me.colPendPackDate.VisibleIndex = 6
        Me.colPendPackDate.Width = 88
        '
        'colPendPackMailAddress
        '
        Me.colPendPackMailAddress.Caption = "MAIL_ADDRESS"
        Me.colPendPackMailAddress.FieldName = "MAIL_ADDRESS"
        Me.colPendPackMailAddress.Name = "colPendPackMailAddress"
        Me.colPendPackMailAddress.Visible = True
        Me.colPendPackMailAddress.VisibleIndex = 7
        '
        'colPendPackSiteCode
        '
        Me.colPendPackSiteCode.Caption = "SITE_CODE"
        Me.colPendPackSiteCode.FieldName = "SITE_CODE"
        Me.colPendPackSiteCode.Name = "colPendPackSiteCode"
        Me.colPendPackSiteCode.Visible = True
        Me.colPendPackSiteCode.VisibleIndex = 8
        '
        'colPendPackCity
        '
        Me.colPendPackCity.Caption = "CITY"
        Me.colPendPackCity.FieldName = "CITY"
        Me.colPendPackCity.Name = "colPendPackCity"
        Me.colPendPackCity.Visible = True
        Me.colPendPackCity.VisibleIndex = 9
        '
        'GridView21
        '
        Me.GridView21.GridControl = Me.grdFinalizedShipments
        Me.GridView21.Name = "GridView21"
        '
        'grdFinalizedShipments
        '
        Me.grdFinalizedShipments.ContextMenuStrip = Me.altMenuGridRightClick
        Me.grdFinalizedShipments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdFinalizedShipments.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode12.LevelTemplate = Me.GridView21
        GridLevelNode12.RelationName = "Level1"
        Me.grdFinalizedShipments.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode12})
        Me.grdFinalizedShipments.Location = New System.Drawing.Point(2, 24)
        Me.grdFinalizedShipments.MainView = Me.GridViewFinalizedShipments
        Me.grdFinalizedShipments.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdFinalizedShipments.Name = "grdFinalizedShipments"
        Me.grdFinalizedShipments.Size = New System.Drawing.Size(1408, 587)
        Me.grdFinalizedShipments.TabIndex = 5
        Me.grdFinalizedShipments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewFinalizedShipments, Me.GridView21})
        '
        'altMenuGridRightClick
        '
        Me.altMenuGridRightClick.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.altMenuGridRightClick.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.altMenuUnfinalize, Me.altMenuPrintForm, Me.altMenuUpdateTracking})
        Me.altMenuGridRightClick.Name = "menuGridRightClick"
        Me.altMenuGridRightClick.Size = New System.Drawing.Size(194, 82)
        '
        'altMenuUnfinalize
        '
        Me.altMenuUnfinalize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.altMenuUnfinalize.Name = "altMenuUnfinalize"
        Me.altMenuUnfinalize.Size = New System.Drawing.Size(193, 26)
        Me.altMenuUnfinalize.Text = "Unfinalize"
        '
        'altMenuPrintForm
        '
        Me.altMenuPrintForm.Name = "altMenuPrintForm"
        Me.altMenuPrintForm.Size = New System.Drawing.Size(193, 26)
        Me.altMenuPrintForm.Text = "Print Form"
        '
        'altMenuUpdateTracking
        '
        Me.altMenuUpdateTracking.Name = "altMenuUpdateTracking"
        Me.altMenuUpdateTracking.Size = New System.Drawing.Size(193, 26)
        Me.altMenuUpdateTracking.Text = "Update Tracking"
        '
        'GridViewFinalizedShipments
        '
        Me.GridViewFinalizedShipments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colFinShipID, Me.colFinShipName, Me.colFinShipFirst, Me.colFinShipLastName, Me.colFinShipCompany, Me.colFinShipCountry, Me.colFinShipPackageCount, Me.colFinShipItemCount, Me.colFinShipShipDate, Me.colFinShipCity, Me.colFinShipExtReference})
        Me.GridViewFinalizedShipments.GridControl = Me.grdFinalizedShipments
        Me.GridViewFinalizedShipments.Name = "GridViewFinalizedShipments"
        Me.GridViewFinalizedShipments.OptionsBehavior.Editable = False
        Me.GridViewFinalizedShipments.OptionsView.ShowGroupPanel = False
        '
        'colFinShipID
        '
        Me.colFinShipID.Caption = "SHIPMENT ID"
        Me.colFinShipID.FieldName = "SHIPMENT_ID"
        Me.colFinShipID.Name = "colFinShipID"
        Me.colFinShipID.OptionsColumn.ShowInCustomizationForm = False
        Me.colFinShipID.Width = 71
        '
        'colFinShipName
        '
        Me.colFinShipName.Caption = "SHIPMENT NAME"
        Me.colFinShipName.FieldName = "SHIPMENT_NAME"
        Me.colFinShipName.Name = "colFinShipName"
        Me.colFinShipName.Visible = True
        Me.colFinShipName.VisibleIndex = 0
        Me.colFinShipName.Width = 71
        '
        'colFinShipFirst
        '
        Me.colFinShipFirst.Caption = "FIRST NAME"
        Me.colFinShipFirst.FieldName = "FIRST_NAME"
        Me.colFinShipFirst.Name = "colFinShipFirst"
        Me.colFinShipFirst.Visible = True
        Me.colFinShipFirst.VisibleIndex = 1
        Me.colFinShipFirst.Width = 71
        '
        'colFinShipLastName
        '
        Me.colFinShipLastName.Caption = "LAST_NAME"
        Me.colFinShipLastName.FieldName = "LAST_NAME"
        Me.colFinShipLastName.Name = "colFinShipLastName"
        Me.colFinShipLastName.Visible = True
        Me.colFinShipLastName.VisibleIndex = 2
        Me.colFinShipLastName.Width = 71
        '
        'colFinShipCompany
        '
        Me.colFinShipCompany.Caption = "COMPANY"
        Me.colFinShipCompany.FieldName = "COMPANY"
        Me.colFinShipCompany.Name = "colFinShipCompany"
        Me.colFinShipCompany.Visible = True
        Me.colFinShipCompany.VisibleIndex = 3
        Me.colFinShipCompany.Width = 71
        '
        'colFinShipCountry
        '
        Me.colFinShipCountry.Caption = "COUNTRY"
        Me.colFinShipCountry.FieldName = "COUNTRY_NAME"
        Me.colFinShipCountry.Name = "colFinShipCountry"
        Me.colFinShipCountry.Visible = True
        Me.colFinShipCountry.VisibleIndex = 4
        Me.colFinShipCountry.Width = 71
        '
        'colFinShipPackageCount
        '
        Me.colFinShipPackageCount.Caption = "PACKAGE_COUNT"
        Me.colFinShipPackageCount.FieldName = "PACKAGE_COUNT"
        Me.colFinShipPackageCount.Name = "colFinShipPackageCount"
        Me.colFinShipPackageCount.Visible = True
        Me.colFinShipPackageCount.VisibleIndex = 5
        Me.colFinShipPackageCount.Width = 20
        '
        'colFinShipItemCount
        '
        Me.colFinShipItemCount.Caption = "CONTAINER_COUNT"
        Me.colFinShipItemCount.FieldName = "CONTAINER_COUNT"
        Me.colFinShipItemCount.Name = "colFinShipItemCount"
        Me.colFinShipItemCount.Visible = True
        Me.colFinShipItemCount.VisibleIndex = 6
        Me.colFinShipItemCount.Width = 20
        '
        'colFinShipShipDate
        '
        Me.colFinShipShipDate.Caption = "DATE_CREATED"
        Me.colFinShipShipDate.DisplayFormat.FormatString = "dd-MMM-yy"
        Me.colFinShipShipDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colFinShipShipDate.FieldName = "DATE_CREATED"
        Me.colFinShipShipDate.Name = "colFinShipShipDate"
        Me.colFinShipShipDate.Visible = True
        Me.colFinShipShipDate.VisibleIndex = 7
        Me.colFinShipShipDate.Width = 119
        '
        'colFinShipCity
        '
        Me.colFinShipCity.Caption = "CITY"
        Me.colFinShipCity.FieldName = "CITY"
        Me.colFinShipCity.Name = "colFinShipCity"
        Me.colFinShipCity.Visible = True
        Me.colFinShipCity.VisibleIndex = 8
        Me.colFinShipCity.Width = 129
        '
        'colFinShipExtReference
        '
        Me.colFinShipExtReference.Caption = "EXTERNAL_REFERENCE"
        Me.colFinShipExtReference.FieldName = "EXTERNAL_REFERENCE"
        Me.colFinShipExtReference.Name = "colFinShipExtReference"
        Me.colFinShipExtReference.Visible = True
        Me.colFinShipExtReference.VisibleIndex = 9
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.grdAnticipatedShipments
        Me.GridView2.Name = "GridView2"
        '
        'grdAnticipatedShipments
        '
        Me.grdAnticipatedShipments.ContextMenuStrip = Me.menuGridRightClick
        Me.grdAnticipatedShipments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdAnticipatedShipments.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode13.LevelTemplate = Me.GridView2
        GridLevelNode13.RelationName = "Level1"
        Me.grdAnticipatedShipments.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode13})
        Me.grdAnticipatedShipments.Location = New System.Drawing.Point(2, 24)
        Me.grdAnticipatedShipments.MainView = Me.GridViewAntShipments
        Me.grdAnticipatedShipments.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdAnticipatedShipments.Name = "grdAnticipatedShipments"
        Me.grdAnticipatedShipments.Size = New System.Drawing.Size(1408, 144)
        Me.grdAnticipatedShipments.TabIndex = 5
        Me.grdAnticipatedShipments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewAntShipments, Me.GridView2})
        '
        'GridViewAntShipments
        '
        Me.GridViewAntShipments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn22, Me.GridColumn23, Me.GridColumn24, Me.GridColumn25, Me.GridColumn26, Me.GridColumn27, Me.GridColumn28})
        Me.GridViewAntShipments.GridControl = Me.grdAnticipatedShipments
        Me.GridViewAntShipments.Name = "GridViewAntShipments"
        Me.GridViewAntShipments.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewAntShipments.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewAntShipments.OptionsBehavior.Editable = False
        Me.GridViewAntShipments.OptionsBehavior.ReadOnly = True
        Me.GridViewAntShipments.OptionsView.ShowGroupPanel = False
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "SHIPMENT_ID"
        Me.GridColumn6.FieldName = "SHIPMENT_ID"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.ShowInCustomizationForm = False
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "SHIPMENT"
        Me.GridColumn7.FieldName = "SHIPMENT_NAME"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "RECIPIENT_FIRSTNAME"
        Me.GridColumn8.FieldName = "FIRST_NAME"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 1
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "RECIPIENT_LASTNAME"
        Me.GridColumn9.FieldName = "LAST_NAME"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 2
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "COMPANY"
        Me.GridColumn10.FieldName = "COMPANY"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 3
        '
        'GridColumn22
        '
        Me.GridColumn22.Caption = "COUNTRY"
        Me.GridColumn22.FieldName = "COUNTRY_NAME"
        Me.GridColumn22.Name = "GridColumn22"
        Me.GridColumn22.Visible = True
        Me.GridColumn22.VisibleIndex = 4
        '
        'GridColumn23
        '
        Me.GridColumn23.Caption = "PACKAGE_COUNT"
        Me.GridColumn23.FieldName = "PACKAGE_COUNT"
        Me.GridColumn23.Name = "GridColumn23"
        Me.GridColumn23.Visible = True
        Me.GridColumn23.VisibleIndex = 5
        '
        'GridColumn24
        '
        Me.GridColumn24.Caption = "# OF ITEMS"
        Me.GridColumn24.FieldName = "CONTAINER_COUNT"
        Me.GridColumn24.Name = "GridColumn24"
        Me.GridColumn24.Visible = True
        Me.GridColumn24.VisibleIndex = 6
        '
        'GridColumn25
        '
        Me.GridColumn25.Caption = "DATE"
        Me.GridColumn25.DisplayFormat.FormatString = "dd-MMM-yy"
        Me.GridColumn25.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn25.FieldName = "DATE_MODIFIED"
        Me.GridColumn25.Name = "GridColumn25"
        Me.GridColumn25.Visible = True
        Me.GridColumn25.VisibleIndex = 7
        '
        'GridColumn26
        '
        Me.GridColumn26.Caption = "RECEIVER_ID"
        Me.GridColumn26.FieldName = "RECEIVER_ID"
        Me.GridColumn26.Name = "GridColumn26"
        '
        'GridColumn27
        '
        Me.GridColumn27.Caption = "SITE_CODE"
        Me.GridColumn27.FieldName = "SITE_CODE"
        Me.GridColumn27.Name = "GridColumn27"
        Me.GridColumn27.Visible = True
        Me.GridColumn27.VisibleIndex = 8
        '
        'GridColumn28
        '
        Me.GridColumn28.Caption = "CITY"
        Me.GridColumn28.FieldName = "CITY"
        Me.GridColumn28.Name = "GridColumn28"
        Me.GridColumn28.Visible = True
        Me.GridColumn28.VisibleIndex = 9
        '
        'GridViewRcvPkgContainers
        '
        Me.GridViewRcvPkgContainers.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPkCntParentContainerID, Me.colPkCntBarcode, Me.colPkCntSampleName, Me.colPkCntRanking, Me.colPkCntFirstName, Me.colPkCntLastName, Me.colPkCntMailAddress, Me.colPkCntSiteCode, Me.colPkCntOrderName, Me.colPkCntProtocolGroupName})
        Me.GridViewRcvPkgContainers.GridControl = Me.grdRcvPackageView
        Me.GridViewRcvPkgContainers.Name = "GridViewRcvPkgContainers"
        '
        'colPkCntParentContainerID
        '
        Me.colPkCntParentContainerID.Caption = "ParentContainerID"
        Me.colPkCntParentContainerID.FieldName = "ParentContainerID"
        Me.colPkCntParentContainerID.Name = "colPkCntParentContainerID"
        Me.colPkCntParentContainerID.Visible = True
        Me.colPkCntParentContainerID.VisibleIndex = 0
        '
        'colPkCntBarcode
        '
        Me.colPkCntBarcode.Caption = "Barcode"
        Me.colPkCntBarcode.FieldName = "Barcode"
        Me.colPkCntBarcode.Name = "colPkCntBarcode"
        Me.colPkCntBarcode.Visible = True
        Me.colPkCntBarcode.VisibleIndex = 1
        '
        'colPkCntSampleName
        '
        Me.colPkCntSampleName.Caption = "SampleName"
        Me.colPkCntSampleName.FieldName = "SampleName"
        Me.colPkCntSampleName.Name = "colPkCntSampleName"
        Me.colPkCntSampleName.Visible = True
        Me.colPkCntSampleName.VisibleIndex = 2
        '
        'colPkCntRanking
        '
        Me.colPkCntRanking.Caption = "Ranking"
        Me.colPkCntRanking.FieldName = "Ranking"
        Me.colPkCntRanking.Name = "colPkCntRanking"
        Me.colPkCntRanking.Visible = True
        Me.colPkCntRanking.VisibleIndex = 3
        '
        'colPkCntFirstName
        '
        Me.colPkCntFirstName.Caption = "FirstName"
        Me.colPkCntFirstName.FieldName = "FirstName"
        Me.colPkCntFirstName.Name = "colPkCntFirstName"
        Me.colPkCntFirstName.Visible = True
        Me.colPkCntFirstName.VisibleIndex = 4
        '
        'colPkCntLastName
        '
        Me.colPkCntLastName.Caption = "LastName"
        Me.colPkCntLastName.FieldName = "LastName"
        Me.colPkCntLastName.Name = "colPkCntLastName"
        Me.colPkCntLastName.Visible = True
        Me.colPkCntLastName.VisibleIndex = 5
        '
        'colPkCntMailAddress
        '
        Me.colPkCntMailAddress.Caption = "MailAddress"
        Me.colPkCntMailAddress.FieldName = "MailAddress"
        Me.colPkCntMailAddress.Name = "colPkCntMailAddress"
        Me.colPkCntMailAddress.Visible = True
        Me.colPkCntMailAddress.VisibleIndex = 6
        '
        'colPkCntSiteCode
        '
        Me.colPkCntSiteCode.Caption = "SiteCode"
        Me.colPkCntSiteCode.FieldName = "SiteCode"
        Me.colPkCntSiteCode.Name = "colPkCntSiteCode"
        Me.colPkCntSiteCode.Visible = True
        Me.colPkCntSiteCode.VisibleIndex = 7
        '
        'colPkCntOrderName
        '
        Me.colPkCntOrderName.Caption = "OrderName"
        Me.colPkCntOrderName.FieldName = "OrderName"
        Me.colPkCntOrderName.Name = "colPkCntOrderName"
        Me.colPkCntOrderName.Visible = True
        Me.colPkCntOrderName.VisibleIndex = 8
        '
        'colPkCntProtocolGroupName
        '
        Me.colPkCntProtocolGroupName.Caption = "ProtocolGroupName"
        Me.colPkCntProtocolGroupName.FieldName = "ProtocolGroupName"
        Me.colPkCntProtocolGroupName.Name = "colPkCntProtocolGroupName"
        '
        'grdRcvPackageView
        '
        Me.grdRcvPackageView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdRcvPackageView.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode14.LevelTemplate = Me.GridViewRcvPkgContainers
        GridLevelNode14.RelationName = "Level1"
        Me.grdRcvPackageView.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode14})
        Me.grdRcvPackageView.Location = New System.Drawing.Point(2, 24)
        Me.grdRcvPackageView.MainView = Me.GridViewRcvPackage
        Me.grdRcvPackageView.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdRcvPackageView.MenuManager = Me.RibbonControl
        Me.grdRcvPackageView.Name = "grdRcvPackageView"
        Me.grdRcvPackageView.Size = New System.Drawing.Size(1133, 376)
        Me.grdRcvPackageView.TabIndex = 0
        Me.grdRcvPackageView.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewRcvPackage, Me.GridView1, Me.GridViewRcvPkgContainers})
        '
        'GridViewRcvPackage
        '
        Me.GridViewRcvPackage.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPkParentContainerID, Me.colPkBarcode, Me.colPkFirstName, Me.colPkLastName, Me.colPkCompany, Me.colPkCountryName, Me.colPkMailAddress, Me.colPkSiteCode})
        Me.GridViewRcvPackage.GridControl = Me.grdRcvPackageView
        Me.GridViewRcvPackage.Name = "GridViewRcvPackage"
        '
        'colPkParentContainerID
        '
        Me.colPkParentContainerID.Caption = "ParentContainerID"
        Me.colPkParentContainerID.FieldName = "ParentContainerID"
        Me.colPkParentContainerID.Name = "colPkParentContainerID"
        Me.colPkParentContainerID.Visible = True
        Me.colPkParentContainerID.VisibleIndex = 0
        '
        'colPkBarcode
        '
        Me.colPkBarcode.Caption = "Barcode"
        Me.colPkBarcode.FieldName = "Barcode"
        Me.colPkBarcode.Name = "colPkBarcode"
        Me.colPkBarcode.Visible = True
        Me.colPkBarcode.VisibleIndex = 1
        '
        'colPkFirstName
        '
        Me.colPkFirstName.Caption = "FirstName"
        Me.colPkFirstName.FieldName = "FirstName"
        Me.colPkFirstName.Name = "colPkFirstName"
        Me.colPkFirstName.Visible = True
        Me.colPkFirstName.VisibleIndex = 2
        '
        'colPkLastName
        '
        Me.colPkLastName.Caption = "FirstName"
        Me.colPkLastName.FieldName = "FirstName"
        Me.colPkLastName.Name = "colPkLastName"
        Me.colPkLastName.Visible = True
        Me.colPkLastName.VisibleIndex = 3
        '
        'colPkCompany
        '
        Me.colPkCompany.Caption = "Company"
        Me.colPkCompany.FieldName = "ReceiverCompany"
        Me.colPkCompany.Name = "colPkCompany"
        Me.colPkCompany.Visible = True
        Me.colPkCompany.VisibleIndex = 4
        '
        'colPkCountryName
        '
        Me.colPkCountryName.Caption = "CountryName"
        Me.colPkCountryName.FieldName = "ReceiverCountryName"
        Me.colPkCountryName.Name = "colPkCountryName"
        Me.colPkCountryName.Visible = True
        Me.colPkCountryName.VisibleIndex = 5
        '
        'colPkMailAddress
        '
        Me.colPkMailAddress.Caption = "MailAddress"
        Me.colPkMailAddress.FieldName = "ReceiverMailAddress"
        Me.colPkMailAddress.Name = "colPkMailAddress"
        Me.colPkMailAddress.Visible = True
        Me.colPkMailAddress.VisibleIndex = 6
        '
        'colPkSiteCode
        '
        Me.colPkSiteCode.Caption = "SiteCode"
        Me.colPkSiteCode.FieldName = "ReceiverSiteCode"
        Me.colPkSiteCode.Name = "colPkSiteCode"
        Me.colPkSiteCode.Visible = True
        Me.colPkSiteCode.VisibleIndex = 7
        '
        'RibbonControl
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.ExpandCollapseItem.Name = ""
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.bbtnCreatePackage, Me.bbtnPackageMaintenance, Me.bbtnLogin, Me.bbtnCreateShipment, Me.bbtnShipmentPlanner, Me.bbtnViewDetails, Me.bbtnSettings, Me.bbtnSampleTransfer, Me.bbtnReceiveShipment, Me.bbtnSTISampleDropoff, Me.bbtnSTIPackageDropoff, Me.bbtnSTIPackagePickup, Me.bbtnSTIBinMaintenance, Me.bbtnSTILog, Me.bbtnCreateInvPackage})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.RibbonControl.MaxItemId = 18
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.rpLogin, Me.rpPackage, Me.rpShipment, Me.rpStats, Me.rpSettings, Me.rpSampleTransfer})
        Me.RibbonControl.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.[True]
        Me.RibbonControl.Size = New System.Drawing.Size(1442, 155)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'bbtnCreatePackage
        '
        Me.bbtnCreatePackage.Caption = "Create Package"
        Me.bbtnCreatePackage.Id = 2
        Me.bbtnCreatePackage.Name = "bbtnCreatePackage"
        '
        'bbtnPackageMaintenance
        '
        Me.bbtnPackageMaintenance.Caption = "Package Maintenance"
        Me.bbtnPackageMaintenance.Id = 3
        Me.bbtnPackageMaintenance.Name = "bbtnPackageMaintenance"
        '
        'bbtnLogin
        '
        Me.bbtnLogin.Caption = "Login"
        Me.bbtnLogin.Id = 4
        Me.bbtnLogin.Name = "bbtnLogin"
        '
        'bbtnCreateShipment
        '
        Me.bbtnCreateShipment.Caption = "Create Shipment"
        Me.bbtnCreateShipment.Id = 5
        Me.bbtnCreateShipment.Name = "bbtnCreateShipment"
        '
        'bbtnShipmentPlanner
        '
        Me.bbtnShipmentPlanner.Caption = "Shipment Planner"
        Me.bbtnShipmentPlanner.Id = 6
        Me.bbtnShipmentPlanner.Name = "bbtnShipmentPlanner"
        '
        'bbtnViewDetails
        '
        Me.bbtnViewDetails.Caption = "View Details"
        Me.bbtnViewDetails.Id = 7
        Me.bbtnViewDetails.Name = "bbtnViewDetails"
        '
        'bbtnSettings
        '
        Me.bbtnSettings.Caption = "Settings"
        Me.bbtnSettings.Id = 8
        Me.bbtnSettings.Name = "bbtnSettings"
        '
        'bbtnSampleTransfer
        '
        Me.bbtnSampleTransfer.Caption = "Sample Transfer"
        Me.bbtnSampleTransfer.Id = 9
        Me.bbtnSampleTransfer.Name = "bbtnSampleTransfer"
        '
        'bbtnReceiveShipment
        '
        Me.bbtnReceiveShipment.Caption = "Receive Shipment"
        Me.bbtnReceiveShipment.Id = 10
        Me.bbtnReceiveShipment.Name = "bbtnReceiveShipment"
        '
        'bbtnSTISampleDropoff
        '
        Me.bbtnSTISampleDropoff.Caption = "Sample Dropoff"
        Me.bbtnSTISampleDropoff.Id = 12
        Me.bbtnSTISampleDropoff.Name = "bbtnSTISampleDropoff"
        '
        'bbtnSTIPackageDropoff
        '
        Me.bbtnSTIPackageDropoff.Caption = "Package Dropoff"
        Me.bbtnSTIPackageDropoff.Id = 13
        Me.bbtnSTIPackageDropoff.Name = "bbtnSTIPackageDropoff"
        '
        'bbtnSTIPackagePickup
        '
        Me.bbtnSTIPackagePickup.Caption = "Package Pickup"
        Me.bbtnSTIPackagePickup.Id = 14
        Me.bbtnSTIPackagePickup.Name = "bbtnSTIPackagePickup"
        '
        'bbtnSTIBinMaintenance
        '
        Me.bbtnSTIBinMaintenance.Caption = "Bin Maintenance"
        Me.bbtnSTIBinMaintenance.Id = 15
        Me.bbtnSTIBinMaintenance.Name = "bbtnSTIBinMaintenance"
        '
        'bbtnSTILog
        '
        Me.bbtnSTILog.Caption = "Log"
        Me.bbtnSTILog.Id = 16
        Me.bbtnSTILog.Name = "bbtnSTILog"
        '
        'bbtnCreateInvPackage
        '
        Me.bbtnCreateInvPackage.Caption = "Create Inventory Package"
        Me.bbtnCreateInvPackage.Id = 17
        Me.bbtnCreateInvPackage.Name = "bbtnCreateInvPackage"
        Me.bbtnCreateInvPackage.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'rpLogin
        '
        Me.rpLogin.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup2})
        Me.rpLogin.Name = "rpLogin"
        Me.rpLogin.Text = "Login"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bbtnLogin)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "Login"
        '
        'rpPackage
        '
        Me.rpPackage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1})
        Me.rpPackage.Name = "rpPackage"
        Me.rpPackage.Text = "Package"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.bbtnCreatePackage)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.bbtnCreateInvPackage)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.bbtnPackageMaintenance)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "Package Actions"
        '
        'rpShipment
        '
        Me.rpShipment.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup3, Me.RibbonPageGroup4, Me.RibbonPageGroup8})
        Me.rpShipment.Name = "rpShipment"
        Me.rpShipment.Text = "Shipment"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.bbtnCreateShipment)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.Text = "Create"
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.ItemLinks.Add(Me.bbtnShipmentPlanner)
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        Me.RibbonPageGroup4.Text = "Shipment Planner"
        '
        'RibbonPageGroup8
        '
        Me.RibbonPageGroup8.ItemLinks.Add(Me.bbtnReceiveShipment)
        Me.RibbonPageGroup8.Name = "RibbonPageGroup8"
        Me.RibbonPageGroup8.Text = "Receive"
        '
        'rpStats
        '
        Me.rpStats.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup5})
        Me.rpStats.Name = "rpStats"
        Me.rpStats.Text = "Statistics"
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.bbtnViewDetails)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.Text = "View Details"
        '
        'rpSettings
        '
        Me.rpSettings.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup6})
        Me.rpSettings.Name = "rpSettings"
        Me.rpSettings.Text = "Settings"
        '
        'RibbonPageGroup6
        '
        Me.RibbonPageGroup6.ItemLinks.Add(Me.bbtnSettings)
        Me.RibbonPageGroup6.Name = "RibbonPageGroup6"
        Me.RibbonPageGroup6.Text = "Settings"
        '
        'rpSampleTransfer
        '
        Me.rpSampleTransfer.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup7, Me.RibbonPageGroup9, Me.RibbonPageGroup10, Me.RibbonPageGroup11, Me.RibbonPageGroup12, Me.RibbonPageGroup13})
        Me.rpSampleTransfer.Name = "rpSampleTransfer"
        Me.rpSampleTransfer.Text = "Sample Transfer"
        '
        'RibbonPageGroup7
        '
        Me.RibbonPageGroup7.ItemLinks.Add(Me.bbtnSampleTransfer)
        Me.RibbonPageGroup7.Name = "RibbonPageGroup7"
        Me.RibbonPageGroup7.Text = "Sample Transfer"
        '
        'RibbonPageGroup9
        '
        Me.RibbonPageGroup9.ItemLinks.Add(Me.bbtnSTISampleDropoff)
        Me.RibbonPageGroup9.Name = "RibbonPageGroup9"
        Me.RibbonPageGroup9.Text = "Sample Dropoff"
        '
        'RibbonPageGroup10
        '
        Me.RibbonPageGroup10.ItemLinks.Add(Me.bbtnSTIPackageDropoff)
        Me.RibbonPageGroup10.Name = "RibbonPageGroup10"
        Me.RibbonPageGroup10.Text = "Package Dropoff"
        '
        'RibbonPageGroup11
        '
        Me.RibbonPageGroup11.ItemLinks.Add(Me.bbtnSTIPackagePickup)
        Me.RibbonPageGroup11.Name = "RibbonPageGroup11"
        Me.RibbonPageGroup11.Text = "Package Pickup"
        '
        'RibbonPageGroup12
        '
        Me.RibbonPageGroup12.ItemLinks.Add(Me.bbtnSTIBinMaintenance)
        Me.RibbonPageGroup12.Name = "RibbonPageGroup12"
        Me.RibbonPageGroup12.Text = "Maintenance"
        '
        'RibbonPageGroup13
        '
        Me.RibbonPageGroup13.ItemLinks.Add(Me.bbtnSTILog)
        Me.RibbonPageGroup13.Name = "RibbonPageGroup13"
        Me.RibbonPageGroup13.Text = "Log"
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 823)
        Me.RibbonStatusBar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(1442, 31)
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.grdRcvPackageView
        Me.GridView1.Name = "GridView1"
        '
        'GridViewRcvShpPackages
        '
        Me.GridViewRcvShpPackages.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPkgParentContainerID, Me.colPkgBarcode, Me.colPkgFirstName, Me.colPkgLastName, Me.colPkgCompany, Me.colPkgCountryName, Me.colPkgMailAddress, Me.colPkgSiteCode})
        Me.GridViewRcvShpPackages.GridControl = Me.grdReceiveShipment
        Me.GridViewRcvShpPackages.Name = "GridViewRcvShpPackages"
        '
        'colPkgParentContainerID
        '
        Me.colPkgParentContainerID.Caption = "ParentContainerID"
        Me.colPkgParentContainerID.FieldName = "ParentContainerID"
        Me.colPkgParentContainerID.Name = "colPkgParentContainerID"
        Me.colPkgParentContainerID.Visible = True
        Me.colPkgParentContainerID.VisibleIndex = 0
        '
        'colPkgBarcode
        '
        Me.colPkgBarcode.Caption = "Barcode"
        Me.colPkgBarcode.FieldName = "Barcode"
        Me.colPkgBarcode.Name = "colPkgBarcode"
        Me.colPkgBarcode.Visible = True
        Me.colPkgBarcode.VisibleIndex = 1
        '
        'colPkgFirstName
        '
        Me.colPkgFirstName.Caption = "FirstName"
        Me.colPkgFirstName.FieldName = "FirstName"
        Me.colPkgFirstName.Name = "colPkgFirstName"
        Me.colPkgFirstName.Visible = True
        Me.colPkgFirstName.VisibleIndex = 2
        '
        'colPkgLastName
        '
        Me.colPkgLastName.Caption = "LastName"
        Me.colPkgLastName.FieldName = "LastName"
        Me.colPkgLastName.Name = "colPkgLastName"
        Me.colPkgLastName.Visible = True
        Me.colPkgLastName.VisibleIndex = 3
        '
        'colPkgCompany
        '
        Me.colPkgCompany.Caption = "Company"
        Me.colPkgCompany.FieldName = "Company"
        Me.colPkgCompany.Name = "colPkgCompany"
        Me.colPkgCompany.Visible = True
        Me.colPkgCompany.VisibleIndex = 4
        '
        'colPkgCountryName
        '
        Me.colPkgCountryName.Caption = "CountryName"
        Me.colPkgCountryName.FieldName = "CountryName"
        Me.colPkgCountryName.Name = "colPkgCountryName"
        Me.colPkgCountryName.Visible = True
        Me.colPkgCountryName.VisibleIndex = 5
        '
        'colPkgMailAddress
        '
        Me.colPkgMailAddress.Caption = "MailAddress"
        Me.colPkgMailAddress.FieldName = "MailAddress"
        Me.colPkgMailAddress.Name = "colPkgMailAddress"
        Me.colPkgMailAddress.Visible = True
        Me.colPkgMailAddress.VisibleIndex = 6
        '
        'colPkgSiteCode
        '
        Me.colPkgSiteCode.Caption = "SiteCode"
        Me.colPkgSiteCode.FieldName = "SiteCode"
        Me.colPkgSiteCode.Name = "colPkgSiteCode"
        Me.colPkgSiteCode.Visible = True
        Me.colPkgSiteCode.VisibleIndex = 7
        '
        'grdReceiveShipment
        '
        Me.grdReceiveShipment.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode15.LevelTemplate = Me.GridViewRcvShpPackages
        GridLevelNode15.RelationName = "Level1"
        GridLevelNode16.LevelTemplate = Me.GridViewRcvShpContainers
        GridLevelNode16.RelationName = "Level2"
        Me.grdReceiveShipment.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode15, GridLevelNode16})
        Me.grdReceiveShipment.Location = New System.Drawing.Point(6, 30)
        Me.grdReceiveShipment.MainView = Me.GridViewRcvShipment
        Me.grdReceiveShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grdReceiveShipment.Name = "grdReceiveShipment"
        Me.grdReceiveShipment.Size = New System.Drawing.Size(679, 320)
        Me.grdReceiveShipment.TabIndex = 0
        Me.grdReceiveShipment.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewRcvShpContainers, Me.GridViewRcvShipment, Me.GridView4, Me.GridView3, Me.GridViewRcvShpPackages})
        Me.grdReceiveShipment.Visible = False
        '
        'GridViewRcvShpContainers
        '
        Me.GridViewRcvShpContainers.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCntContainerID, Me.colCntParentContainerID, Me.colCntBarcode, Me.grdCntSampleName, Me.colCntRanking, Me.colCntLastName, Me.colCntFirstName, Me.colCntMailAddress, Me.colCntSiteCode, Me.colCntCity, Me.colCntCountryName, Me.colCntOrderName})
        Me.GridViewRcvShpContainers.GridControl = Me.grdReceiveShipment
        Me.GridViewRcvShpContainers.Name = "GridViewRcvShpContainers"
        '
        'colCntContainerID
        '
        Me.colCntContainerID.Caption = "ContainerID"
        Me.colCntContainerID.FieldName = "ContainerID"
        Me.colCntContainerID.Name = "colCntContainerID"
        Me.colCntContainerID.Visible = True
        Me.colCntContainerID.VisibleIndex = 0
        '
        'colCntParentContainerID
        '
        Me.colCntParentContainerID.Caption = "ParentContainerID"
        Me.colCntParentContainerID.FieldName = "ParentConatinerID"
        Me.colCntParentContainerID.Name = "colCntParentContainerID"
        '
        'colCntBarcode
        '
        Me.colCntBarcode.Caption = "Barcode"
        Me.colCntBarcode.FieldName = "Barcode"
        Me.colCntBarcode.Name = "colCntBarcode"
        Me.colCntBarcode.Visible = True
        Me.colCntBarcode.VisibleIndex = 1
        '
        'grdCntSampleName
        '
        Me.grdCntSampleName.Caption = "SampleName"
        Me.grdCntSampleName.FieldName = "SampleName"
        Me.grdCntSampleName.Name = "grdCntSampleName"
        Me.grdCntSampleName.Visible = True
        Me.grdCntSampleName.VisibleIndex = 2
        '
        'colCntRanking
        '
        Me.colCntRanking.Caption = "Ranking"
        Me.colCntRanking.FieldName = "Ranking"
        Me.colCntRanking.Name = "colCntRanking"
        Me.colCntRanking.Visible = True
        Me.colCntRanking.VisibleIndex = 3
        '
        'colCntLastName
        '
        Me.colCntLastName.Caption = "LastName"
        Me.colCntLastName.FieldName = "LastName"
        Me.colCntLastName.Name = "colCntLastName"
        Me.colCntLastName.Visible = True
        Me.colCntLastName.VisibleIndex = 4
        '
        'colCntFirstName
        '
        Me.colCntFirstName.Caption = "FirstName"
        Me.colCntFirstName.FieldName = "FirstName"
        Me.colCntFirstName.Name = "colCntFirstName"
        Me.colCntFirstName.Visible = True
        Me.colCntFirstName.VisibleIndex = 5
        '
        'colCntMailAddress
        '
        Me.colCntMailAddress.Caption = "MailAddress"
        Me.colCntMailAddress.FieldName = "MailAddress"
        Me.colCntMailAddress.Name = "colCntMailAddress"
        Me.colCntMailAddress.Visible = True
        Me.colCntMailAddress.VisibleIndex = 6
        '
        'colCntSiteCode
        '
        Me.colCntSiteCode.Caption = "SiteCode"
        Me.colCntSiteCode.FieldName = "SiteCode"
        Me.colCntSiteCode.Name = "colCntSiteCode"
        Me.colCntSiteCode.Visible = True
        Me.colCntSiteCode.VisibleIndex = 7
        '
        'colCntCity
        '
        Me.colCntCity.Caption = "City"
        Me.colCntCity.FieldName = "City"
        Me.colCntCity.Name = "colCntCity"
        '
        'colCntCountryName
        '
        Me.colCntCountryName.Caption = "CountryName"
        Me.colCntCountryName.FieldName = "CountryName"
        Me.colCntCountryName.Name = "colCntCountryName"
        '
        'colCntOrderName
        '
        Me.colCntOrderName.Caption = "OrderName"
        Me.colCntOrderName.FieldName = "OrderName"
        Me.colCntOrderName.Name = "colCntOrderName"
        Me.colCntOrderName.Visible = True
        Me.colCntOrderName.VisibleIndex = 8
        '
        'GridViewRcvShipment
        '
        Me.GridViewRcvShipment.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colShipmentID, Me.colShipmentName, Me.colFirstName, Me.colLastName, Me.colCompany, Me.colCountryName, Me.colMailAddress, Me.colPackageCount, Me.colContainerCount})
        Me.GridViewRcvShipment.GridControl = Me.grdReceiveShipment
        Me.GridViewRcvShipment.Name = "GridViewRcvShipment"
        '
        'colShipmentID
        '
        Me.colShipmentID.Caption = "ShipmentID"
        Me.colShipmentID.FieldName = "ShipmentID"
        Me.colShipmentID.Name = "colShipmentID"
        Me.colShipmentID.Visible = True
        Me.colShipmentID.VisibleIndex = 0
        '
        'colShipmentName
        '
        Me.colShipmentName.Caption = "ShipmentName"
        Me.colShipmentName.FieldName = "ShipmentName"
        Me.colShipmentName.Name = "colShipmentName"
        Me.colShipmentName.Visible = True
        Me.colShipmentName.VisibleIndex = 1
        '
        'colFirstName
        '
        Me.colFirstName.Caption = "FirstName"
        Me.colFirstName.FieldName = "ReceiverFirstName"
        Me.colFirstName.Name = "colFirstName"
        Me.colFirstName.Visible = True
        Me.colFirstName.VisibleIndex = 2
        '
        'colLastName
        '
        Me.colLastName.Caption = "LastName"
        Me.colLastName.FieldName = "ReceiverLastName"
        Me.colLastName.Name = "colLastName"
        Me.colLastName.Visible = True
        Me.colLastName.VisibleIndex = 3
        '
        'colCompany
        '
        Me.colCompany.Caption = "Company"
        Me.colCompany.FieldName = "ReceiverCompany"
        Me.colCompany.Name = "colCompany"
        Me.colCompany.Visible = True
        Me.colCompany.VisibleIndex = 4
        '
        'colCountryName
        '
        Me.colCountryName.Caption = "CountryName"
        Me.colCountryName.FieldName = "ReceiverCountryName"
        Me.colCountryName.Name = "colCountryName"
        Me.colCountryName.Visible = True
        Me.colCountryName.VisibleIndex = 5
        '
        'colMailAddress
        '
        Me.colMailAddress.Caption = "MailAddress"
        Me.colMailAddress.FieldName = "ReceiverMailAddress"
        Me.colMailAddress.Name = "colMailAddress"
        Me.colMailAddress.Visible = True
        Me.colMailAddress.VisibleIndex = 7
        '
        'colPackageCount
        '
        Me.colPackageCount.Caption = "PackageCount"
        Me.colPackageCount.Name = "colPackageCount"
        Me.colPackageCount.Visible = True
        Me.colPackageCount.VisibleIndex = 6
        '
        'colContainerCount
        '
        Me.colContainerCount.Caption = "ContainerCount"
        Me.colContainerCount.FieldName = "ContainerCount"
        Me.colContainerCount.Name = "colContainerCount"
        Me.colContainerCount.Visible = True
        Me.colContainerCount.VisibleIndex = 8
        '
        'GridView4
        '
        Me.GridView4.GridControl = Me.grdReceiveShipment
        Me.GridView4.Name = "GridView4"
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.grdReceiveShipment
        Me.GridView3.Name = "GridView3"
        '
        'GridView14
        '
        Me.GridView14.GridControl = Me.GridShipment
        Me.GridView14.Name = "GridView14"
        '
        'GridShipment
        '
        Me.GridShipment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridShipment.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        GridLevelNode17.LevelTemplate = Me.GridView14
        GridLevelNode17.RelationName = "Level1"
        Me.GridShipment.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode17})
        Me.GridShipment.Location = New System.Drawing.Point(2, 24)
        Me.GridShipment.MainView = Me.GridViewShipmentDetails
        Me.GridShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GridShipment.Name = "GridShipment"
        Me.GridShipment.Size = New System.Drawing.Size(1197, 583)
        Me.GridShipment.TabIndex = 5
        Me.GridShipment.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewShipmentDetails, Me.GridView14})
        '
        'GridViewShipmentDetails
        '
        Me.GridViewShipmentDetails.GridControl = Me.GridShipment
        Me.GridViewShipmentDetails.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridViewShipmentDetails.Name = "GridViewShipmentDetails"
        Me.GridViewShipmentDetails.OptionsBehavior.Editable = False
        Me.GridViewShipmentDetails.OptionsBehavior.ReadOnly = True
        Me.GridViewShipmentDetails.OptionsPrint.PrintDetails = True
        Me.GridViewShipmentDetails.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridViewShipmentDetails.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.GridViewShipmentDetails.OptionsSelection.EnableAppearanceHideSelection = False
        Me.GridViewShipmentDetails.OptionsSelection.MultiSelect = True
        Me.GridViewShipmentDetails.OptionsSelection.UseIndicatorForSelection = False
        Me.GridViewShipmentDetails.OptionsView.ColumnAutoWidth = False
        Me.GridViewShipmentDetails.OptionsView.ShowGroupPanel = False
        '
        'MainLayout
        '
        Me.MainLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainLayout.Location = New System.Drawing.Point(0, 155)
        Me.MainLayout.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MainLayout.Name = "MainLayout"
        Me.MainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(181, 85, 249, 350)
        Me.MainLayout.Root = Me.LayoutControlMainGroup
        Me.MainLayout.Size = New System.Drawing.Size(1442, 668)
        Me.MainLayout.TabIndex = 5
        Me.MainLayout.Text = "MainLayout"
        '
        'LayoutControlMainGroup
        '
        Me.LayoutControlMainGroup.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlMainGroup.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlMainGroup.Name = "LayoutControlGroup1"
        Me.LayoutControlMainGroup.Size = New System.Drawing.Size(1442, 668)
        Me.LayoutControlMainGroup.Text = "LayoutControlGroup1"
        Me.LayoutControlMainGroup.TextVisible = False
        '
        'NavLinks
        '
        Me.NavLinks.Caption = "Links"
        Me.NavLinks.Expanded = True
        Me.NavLinks.Name = "NavLinks"
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.OptionsItemText.TextToControlDistance = 5
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1101, 635)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.Button4)
        Me.GroupBox11.Location = New System.Drawing.Point(3, 310)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(218, 57)
        Me.GroupBox11.TabIndex = 10
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Print Label"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(6, 20)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(188, 23)
        Me.Button4.TabIndex = 0
        Me.Button4.Text = "Print"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.DataGridView3)
        Me.GroupBox10.Location = New System.Drawing.Point(227, 310)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(663, 169)
        Me.GroupBox10.TabIndex = 9
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Active Package Details"
        '
        'DataGridView3
        '
        Me.DataGridView3.AllowUserToOrderColumns = True
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn27, Me.Column6})
        Me.DataGridView3.Location = New System.Drawing.Point(7, 19)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(643, 143)
        Me.DataGridView3.TabIndex = 0
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Request"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "NVP-Number"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Barcode"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.HeaderText = "Recipient"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        '
        'Column6
        '
        Me.Column6.HeaderText = "Destination"
        Me.Column6.Name = "Column6"
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.DataGridView2)
        Me.GroupBox9.Location = New System.Drawing.Point(227, 158)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(663, 146)
        Me.GroupBox9.TabIndex = 8
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Existing Packages"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToOrderColumns = True
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn7, Me.Column4, Me.Column5})
        Me.DataGridView2.Location = New System.Drawing.Point(7, 20)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(643, 112)
        Me.DataGridView2.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Barcode"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Recipient"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Company"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Country"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'Column4
        '
        Me.Column4.HeaderText = "# of Items"
        Me.Column4.Name = "Column4"
        '
        'Column5
        '
        Me.Column5.HeaderText = "Date"
        Me.Column5.Name = "Column5"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Button3)
        Me.GroupBox8.Location = New System.Drawing.Point(3, 247)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(218, 57)
        Me.GroupBox8.TabIndex = 7
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "5.  Finalize Package"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(6, 20)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(188, 23)
        Me.Button3.TabIndex = 0
        Me.Button3.Text = "Finalize"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.TextBox5)
        Me.GroupBox6.Controls.Add(Me.TextBox4)
        Me.GroupBox6.Controls.Add(Me.TextBox3)
        Me.GroupBox6.Controls.Add(Me.TextBox2)
        Me.GroupBox6.Controls.Add(Me.DataGridView1)
        Me.GroupBox6.Location = New System.Drawing.Point(227, 3)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(663, 148)
        Me.GroupBox6.TabIndex = 5
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Pending Items"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(325, 20)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(100, 22)
        Me.TextBox5.TabIndex = 6
        Me.TextBox5.Text = "# of Domestic"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(219, 20)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(100, 22)
        Me.TextBox4.TabIndex = 6
        Me.TextBox4.Text = "# of International"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(113, 19)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(100, 22)
        Me.TextBox3.TabIndex = 2
        Me.TextBox3.Text = "# of Destinations"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(7, 20)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 22)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Text = "# of Pending Items"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Request, Me.Column1, Me.Column2, Me.Column3, Me.DataGridViewTextBoxColumn26})
        Me.DataGridView1.Location = New System.Drawing.Point(7, 45)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(643, 91)
        Me.DataGridView1.TabIndex = 0
        '
        'Request
        '
        Me.Request.HeaderText = "Request"
        Me.Request.Name = "Request"
        '
        'Column1
        '
        Me.Column1.HeaderText = "NVP-number"
        Me.Column1.Name = "Column1"
        '
        'Column2
        '
        Me.Column2.HeaderText = "Barcode"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "Recipient"
        Me.Column3.Name = "Column3"
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "Destination"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.CheckedListBox1)
        Me.GroupBox5.Location = New System.Drawing.Point(3, 56)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(216, 94)
        Me.GroupBox5.TabIndex = 4
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "2.   Validation Rules"
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Items.AddRange(New Object() {"Novartis Site", "Destination", "Request Number", "Delivery Form"})
        Me.CheckedListBox1.Location = New System.Drawing.Point(6, 19)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(188, 55)
        Me.CheckedListBox1.TabIndex = 2
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ComboBox1)
        Me.GroupBox4.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(216, 47)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "1.  Package Content"
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"CPH Products", "NCA Inventory Stock", "SMRT Products", "Solar Plates", "ASP Plates", "Miscellaneous Items"})
        Me.ComboBox1.Location = New System.Drawing.Point(6, 19)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(188, 25)
        Me.ComboBox1.TabIndex = 1
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Location = New System.Drawing.Point(4, 373)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(221, 106)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Process Step"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(14, 33)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(173, 17)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "2.  Adjust Validation Rules"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(14, 86)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(165, 17)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "5.  Finalize the Container"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(195, 17)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "4.  Add Items to the Container"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 62)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(0, 17)
        Me.Label4.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(14, 51)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(272, 17)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "3.  Select Recipient  or  Existing Container"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(149, 17)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "1.  Select content type"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.GridControl4)
        Me.GroupControl1.Location = New System.Drawing.Point(188, 332)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(676, 160)
        Me.GroupControl1.TabIndex = 8
        Me.GroupControl1.Text = "Active Package Details"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.GridControl5)
        Me.GroupControl2.Location = New System.Drawing.Point(188, 181)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(676, 145)
        Me.GroupControl2.TabIndex = 7
        Me.GroupControl2.Text = "Existing Packages"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.GridControl6)
        Me.GroupControl3.Controls.Add(Me.TextEdit1)
        Me.GroupControl3.Controls.Add(Me.TextEdit2)
        Me.GroupControl3.Controls.Add(Me.TextEdit3)
        Me.GroupControl3.Controls.Add(Me.TextEdit4)
        Me.GroupControl3.Location = New System.Drawing.Point(188, 3)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(676, 172)
        Me.GroupControl3.TabIndex = 6
        Me.GroupControl3.Text = "Pending Items"
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = "# of Pending Items"
        Me.TextEdit1.Location = New System.Drawing.Point(5, 23)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(147, 22)
        Me.TextEdit1.TabIndex = 4
        '
        'TextEdit2
        '
        Me.TextEdit2.EditValue = "# of Destinations"
        Me.TextEdit2.Location = New System.Drawing.Point(158, 24)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(147, 22)
        Me.TextEdit2.TabIndex = 3
        '
        'TextEdit3
        '
        Me.TextEdit3.EditValue = "# of International"
        Me.TextEdit3.Location = New System.Drawing.Point(311, 24)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Size = New System.Drawing.Size(147, 22)
        Me.TextEdit3.TabIndex = 2
        '
        'TextEdit4
        '
        Me.TextEdit4.EditValue = "# of Domestic"
        Me.TextEdit4.Location = New System.Drawing.Point(464, 23)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Size = New System.Drawing.Size(147, 22)
        Me.TextEdit4.TabIndex = 1
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.TextEdit5)
        Me.GroupControl4.Controls.Add(Me.btnImportFile)
        Me.GroupControl4.Location = New System.Drawing.Point(3, 242)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(164, 75)
        Me.GroupControl4.TabIndex = 5
        Me.GroupControl4.Text = "4. Add Items"
        '
        'TextEdit5
        '
        Me.TextEdit5.EditValue = "Scan Item Barcode"
        Me.TextEdit5.Location = New System.Drawing.Point(6, 23)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Size = New System.Drawing.Size(147, 22)
        Me.TextEdit5.TabIndex = 3
        '
        'btnImportFile
        '
        Me.btnImportFile.Location = New System.Drawing.Point(5, 46)
        Me.btnImportFile.Name = "btnImportFile"
        Me.btnImportFile.Size = New System.Drawing.Size(148, 25)
        Me.btnImportFile.TabIndex = 2
        Me.btnImportFile.Text = "Import File"
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.MemoEdit1)
        Me.GroupControl5.Location = New System.Drawing.Point(3, 386)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(164, 110)
        Me.GroupControl5.TabIndex = 4
        Me.GroupControl5.Text = "Workflow"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.EditValue = "1. Select content type" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "2. Adjust Validation Rules" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "3. Select Recipient or" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "     " & _
    "Existing Package" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "4. Add Items to Package" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "5. Finalize Package"
        Me.MemoEdit1.Location = New System.Drawing.Point(5, 23)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.MemoEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.MemoEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.MemoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MemoEdit1.Size = New System.Drawing.Size(149, 78)
        Me.MemoEdit1.TabIndex = 1
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.CheckedListBoxControl1)
        Me.GroupControl6.Location = New System.Drawing.Point(3, 64)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(164, 84)
        Me.GroupControl6.TabIndex = 3
        Me.GroupControl6.Text = "2. Validation Rules"
        '
        'CheckedListBoxControl1
        '
        Me.CheckedListBoxControl1.Items.AddRange(New DevExpress.XtraEditors.Controls.CheckedListBoxItem() {New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Novartis Site"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Destination"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Request Number"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Delivery Form")})
        Me.CheckedListBoxControl1.Location = New System.Drawing.Point(5, 23)
        Me.CheckedListBoxControl1.Name = "CheckedListBoxControl1"
        Me.CheckedListBoxControl1.Size = New System.Drawing.Size(154, 54)
        Me.CheckedListBoxControl1.TabIndex = 0
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.SimpleButton2)
        Me.GroupControl7.Location = New System.Drawing.Point(3, 323)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(164, 56)
        Me.GroupControl7.TabIndex = 2
        Me.GroupControl7.Text = "5. Finalize Package"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(5, 23)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(148, 25)
        Me.SimpleButton2.TabIndex = 2
        Me.SimpleButton2.Text = "Finalize"
        '
        'GroupControl8
        '
        Me.GroupControl8.Controls.Add(Me.TextEdit6)
        Me.GroupControl8.Controls.Add(Me.SimpleButton3)
        Me.GroupControl8.Location = New System.Drawing.Point(3, 154)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(164, 82)
        Me.GroupControl8.TabIndex = 1
        Me.GroupControl8.Text = "3. Recipient/Package"
        '
        'TextEdit6
        '
        Me.TextEdit6.EditValue = "Scan Package Barcode"
        Me.TextEdit6.Location = New System.Drawing.Point(5, 24)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Size = New System.Drawing.Size(147, 22)
        Me.TextEdit6.TabIndex = 4
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(5, 50)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(148, 25)
        Me.SimpleButton3.TabIndex = 3
        Me.SimpleButton3.Text = "Select Recipient"
        '
        'GroupControl9
        '
        Me.GroupControl9.Controls.Add(Me.ComboBoxEdit1)
        Me.GroupControl9.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(164, 55)
        Me.GroupControl9.TabIndex = 0
        Me.GroupControl9.Text = "1. Package Content"
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(5, 24)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Properties.Items.AddRange(New Object() {"CPH Products", "NCA Inventory Stock", "SMRT Products", "SOLAR Plates", "ASP Plates", "Misc Items"})
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(154, 22)
        Me.ComboBoxEdit1.TabIndex = 0
        '
        'diaShipmentTemplate
        '
        Me.diaShipmentTemplate.Filter = "DOTX Files (*.dotx)|*.dotx"
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(1081, 615)
        Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'tabMain
        '
        Me.tabMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabMain.Location = New System.Drawing.Point(0, 155)
        Me.tabMain.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedTabPage = Me.TabCreatePackage
        Me.tabMain.Size = New System.Drawing.Size(1442, 668)
        Me.tabMain.TabIndex = 8
        Me.tabMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.TabLogin, Me.TabCreatePackage, Me.TabCreateInvPackage, Me.TabPackageMaintenance, Me.TabCreateShipment, Me.TabShipmentPlanner, Me.TabViewShipment, Me.TabReceive, Me.TabSettings, Me.TabSampleTransfer})
        '
        'TabCreatePackage
        '
        Me.TabCreatePackage.Controls.Add(Me.LayoutControl5)
        Me.TabCreatePackage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TabCreatePackage.Name = "TabCreatePackage"
        Me.TabCreatePackage.Size = New System.Drawing.Size(1436, 637)
        Me.TabCreatePackage.Text = "Create Package"
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Controls.Add(Me.PanelControl4)
        Me.LayoutControl5.Controls.Add(Me.PanelControl3)
        Me.LayoutControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl5.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(796, 224, 250, 350)
        Me.LayoutControl5.Root = Me.LayoutControlGroup6
        Me.LayoutControl5.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControl5.TabIndex = 504
        Me.LayoutControl5.Text = "LayoutControl5"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.SimpleButton1)
        Me.PanelControl4.Controls.Add(Me.GroupControl14)
        Me.PanelControl4.Controls.Add(Me.grpAddPackageItem)
        Me.PanelControl4.Controls.Add(Me.GrpContent)
        Me.PanelControl4.Controls.Add(Me.grpValidationRules)
        Me.PanelControl4.Controls.Add(Me.grpFinalizePackage)
        Me.PanelControl4.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl4.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(193, 613)
        Me.PanelControl4.TabIndex = 5
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(13, 537)
        Me.SimpleButton1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(173, 31)
        Me.SimpleButton1.TabIndex = 504
        Me.SimpleButton1.Text = "Load Items"
        Me.SimpleButton1.Visible = False
        '
        'GroupControl14
        '
        Me.GroupControl14.Controls.Add(Me.txtRemoveItem)
        Me.GroupControl14.Controls.Add(Me.btnRemoveItem)
        Me.GroupControl14.Location = New System.Drawing.Point(6, 422)
        Me.GroupControl14.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupControl14.Name = "GroupControl14"
        Me.GroupControl14.Size = New System.Drawing.Size(191, 96)
        Me.GroupControl14.TabIndex = 501
        Me.GroupControl14.Text = "Remove Item"
        '
        'txtRemoveItem
        '
        Me.txtRemoveItem.EditValue = ""
        Me.txtRemoveItem.Location = New System.Drawing.Point(8, 30)
        Me.txtRemoveItem.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtRemoveItem.Name = "txtRemoveItem"
        Me.txtRemoveItem.Size = New System.Drawing.Size(171, 22)
        Me.txtRemoveItem.TabIndex = 20
        '
        'btnRemoveItem
        '
        Me.btnRemoveItem.Location = New System.Drawing.Point(6, 62)
        Me.btnRemoveItem.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnRemoveItem.Name = "btnRemoveItem"
        Me.btnRemoveItem.Size = New System.Drawing.Size(173, 31)
        Me.btnRemoveItem.TabIndex = 19
        Me.btnRemoveItem.Text = "Remove Item"
        '
        'grpAddPackageItem
        '
        Me.grpAddPackageItem.Controls.Add(Me.btn_LookupScan)
        Me.grpAddPackageItem.Controls.Add(Me.btnScanItems)
        Me.grpAddPackageItem.Controls.Add(Me.txtPackageItemBarcode)
        Me.grpAddPackageItem.Location = New System.Drawing.Point(6, 6)
        Me.grpAddPackageItem.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpAddPackageItem.Name = "grpAddPackageItem"
        Me.grpAddPackageItem.Size = New System.Drawing.Size(191, 132)
        Me.grpAddPackageItem.TabIndex = 5
        Me.grpAddPackageItem.Text = "Select Items"
        '
        'btn_LookupScan
        '
        Me.btn_LookupScan.Location = New System.Drawing.Point(7, 55)
        Me.btn_LookupScan.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btn_LookupScan.Name = "btn_LookupScan"
        Me.btn_LookupScan.Size = New System.Drawing.Size(173, 31)
        Me.btn_LookupScan.TabIndex = 18
        Me.btn_LookupScan.Text = "Select Item"
        '
        'btnScanItems
        '
        Me.btnScanItems.Location = New System.Drawing.Point(7, 90)
        Me.btnScanItems.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnScanItems.Name = "btnScanItems"
        Me.btnScanItems.Size = New System.Drawing.Size(173, 31)
        Me.btnScanItems.TabIndex = 17
        Me.btnScanItems.Text = "Scan Items"
        '
        'txtPackageItemBarcode
        '
        Me.txtPackageItemBarcode.EditValue = "Scan Item Barcode"
        Me.txtPackageItemBarcode.Location = New System.Drawing.Point(7, 28)
        Me.txtPackageItemBarcode.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPackageItemBarcode.Name = "txtPackageItemBarcode"
        Me.txtPackageItemBarcode.Size = New System.Drawing.Size(171, 22)
        Me.txtPackageItemBarcode.TabIndex = 16
        '
        'GrpContent
        '
        Me.GrpContent.Controls.Add(Me.lkPackageOrigin)
        Me.GrpContent.Controls.Add(Me.lblPackageOrigin)
        Me.GrpContent.Controls.Add(Me.lblPackageContent)
        Me.GrpContent.Controls.Add(Me.lkPackageContent)
        Me.GrpContent.Location = New System.Drawing.Point(7, 638)
        Me.GrpContent.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GrpContent.Name = "GrpContent"
        Me.GrpContent.Size = New System.Drawing.Size(191, 138)
        Me.GrpContent.TabIndex = 503
        Me.GrpContent.Text = "Package Content/Origin"
        Me.GrpContent.Visible = False
        '
        'lkPackageOrigin
        '
        Me.lkPackageOrigin.EditValue = ""
        Me.lkPackageOrigin.Location = New System.Drawing.Point(6, 100)
        Me.lkPackageOrigin.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkPackageOrigin.Name = "lkPackageOrigin"
        Me.lkPackageOrigin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkPackageOrigin.Properties.NullText = ""
        Me.lkPackageOrigin.Properties.ShowFooter = False
        Me.lkPackageOrigin.Properties.ShowHeader = False
        Me.lkPackageOrigin.Properties.ShowLines = False
        Me.lkPackageOrigin.Properties.ShowPopupShadow = False
        Me.lkPackageOrigin.Size = New System.Drawing.Size(176, 22)
        Me.lkPackageOrigin.TabIndex = 11
        Me.lkPackageOrigin.Tag = "Origin"
        '
        'lblPackageOrigin
        '
        Me.lblPackageOrigin.Location = New System.Drawing.Point(6, 76)
        Me.lblPackageOrigin.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblPackageOrigin.Name = "lblPackageOrigin"
        Me.lblPackageOrigin.Size = New System.Drawing.Size(34, 16)
        Me.lblPackageOrigin.TabIndex = 44
        Me.lblPackageOrigin.Text = "Origin"
        '
        'lblPackageContent
        '
        Me.lblPackageContent.Location = New System.Drawing.Point(6, 28)
        Me.lblPackageContent.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblPackageContent.Name = "lblPackageContent"
        Me.lblPackageContent.Size = New System.Drawing.Size(44, 16)
        Me.lblPackageContent.TabIndex = 43
        Me.lblPackageContent.Text = "Content"
        '
        'lkPackageContent
        '
        Me.lkPackageContent.EditValue = ""
        Me.lkPackageContent.Location = New System.Drawing.Point(6, 44)
        Me.lkPackageContent.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkPackageContent.Name = "lkPackageContent"
        Me.lkPackageContent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkPackageContent.Properties.NullText = ""
        Me.lkPackageContent.Properties.ShowFooter = False
        Me.lkPackageContent.Properties.ShowHeader = False
        Me.lkPackageContent.Properties.ShowLines = False
        Me.lkPackageContent.Properties.ShowPopupShadow = False
        Me.lkPackageContent.Size = New System.Drawing.Size(176, 22)
        Me.lkPackageContent.TabIndex = 10
        Me.lkPackageContent.Tag = "Content"
        '
        'grpValidationRules
        '
        Me.grpValidationRules.Controls.Add(Me.ChkLstPackageValidationRules)
        Me.grpValidationRules.Location = New System.Drawing.Point(6, 156)
        Me.grpValidationRules.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpValidationRules.Name = "grpValidationRules"
        Me.grpValidationRules.Size = New System.Drawing.Size(191, 186)
        Me.grpValidationRules.TabIndex = 3
        Me.grpValidationRules.Text = "Validation Rules"
        '
        'ChkLstPackageValidationRules
        '
        Me.ChkLstPackageValidationRules.CheckOnClick = True
        Me.ChkLstPackageValidationRules.Enabled = False
        Me.ChkLstPackageValidationRules.Items.AddRange(New DevExpress.XtraEditors.Controls.CheckedListBoxItem() {New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Protocol", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Recipient", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Novartis Site"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Destination"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Request Number"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Delivery Form"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Dest. Rack Group Name")})
        Me.ChkLstPackageValidationRules.Location = New System.Drawing.Point(6, 28)
        Me.ChkLstPackageValidationRules.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.ChkLstPackageValidationRules.Name = "ChkLstPackageValidationRules"
        Me.ChkLstPackageValidationRules.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.ChkLstPackageValidationRules.Size = New System.Drawing.Size(180, 149)
        Me.ChkLstPackageValidationRules.TabIndex = 12
        '
        'grpFinalizePackage
        '
        Me.grpFinalizePackage.Controls.Add(Me.btnFinalizePackage)
        Me.grpFinalizePackage.Location = New System.Drawing.Point(6, 348)
        Me.grpFinalizePackage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpFinalizePackage.Name = "grpFinalizePackage"
        Me.grpFinalizePackage.Size = New System.Drawing.Size(191, 69)
        Me.grpFinalizePackage.TabIndex = 2
        Me.grpFinalizePackage.Text = "Finalize Package"
        '
        'btnFinalizePackage
        '
        Me.btnFinalizePackage.Location = New System.Drawing.Point(6, 28)
        Me.btnFinalizePackage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnFinalizePackage.Name = "btnFinalizePackage"
        Me.btnFinalizePackage.Size = New System.Drawing.Size(173, 31)
        Me.btnFinalizePackage.TabIndex = 19
        Me.btnFinalizePackage.Text = "Finalize / Print Label"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.LayoutControl6)
        Me.PanelControl3.Location = New System.Drawing.Point(214, 12)
        Me.PanelControl3.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(1210, 613)
        Me.PanelControl3.TabIndex = 4
        '
        'LayoutControl6
        '
        Me.LayoutControl6.Controls.Add(Me.grpExistingPackages)
        Me.LayoutControl6.Controls.Add(Me.grpPendingItems)
        Me.LayoutControl6.Controls.Add(Me.grpActivePackageDetail)
        Me.LayoutControl6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl6.Location = New System.Drawing.Point(2, 2)
        Me.LayoutControl6.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl6.Name = "LayoutControl6"
        Me.LayoutControl6.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(597, 250, 250, 350)
        Me.LayoutControl6.Root = Me.LayoutControlGroup7
        Me.LayoutControl6.Size = New System.Drawing.Size(1206, 609)
        Me.LayoutControl6.TabIndex = 0
        Me.LayoutControl6.Text = "LayoutControl6"
        '
        'grpExistingPackages
        '
        Me.grpExistingPackages.Controls.Add(Me.grdExistingPackages)
        Me.grpExistingPackages.Location = New System.Drawing.Point(12, 203)
        Me.grpExistingPackages.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpExistingPackages.Name = "grpExistingPackages"
        Me.grpExistingPackages.Size = New System.Drawing.Size(1182, 119)
        Me.grpExistingPackages.TabIndex = 7
        Me.grpExistingPackages.Text = "Existing Packages"
        '
        'grpPendingItems
        '
        Me.grpPendingItems.Controls.Add(Me.btnRefreshPackageGrids)
        Me.grpPendingItems.Controls.Add(Me.btnShowAllPending)
        Me.grpPendingItems.Controls.Add(Me.lkPendingTimeframe)
        Me.grpPendingItems.Controls.Add(Me.LabelControl3)
        Me.grpPendingItems.Controls.Add(Me.grdPendingItems)
        Me.grpPendingItems.Location = New System.Drawing.Point(12, 12)
        Me.grpPendingItems.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpPendingItems.Name = "grpPendingItems"
        Me.grpPendingItems.Size = New System.Drawing.Size(1182, 182)
        Me.grpPendingItems.TabIndex = 6
        Me.grpPendingItems.Text = "Pending Items"
        '
        'btnRefreshPackageGrids
        '
        Me.btnRefreshPackageGrids.Location = New System.Drawing.Point(210, 0)
        Me.btnRefreshPackageGrids.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnRefreshPackageGrids.Name = "btnRefreshPackageGrids"
        Me.btnRefreshPackageGrids.Size = New System.Drawing.Size(148, 23)
        Me.btnRefreshPackageGrids.TabIndex = 504
        Me.btnRefreshPackageGrids.Text = "Refresh Package Grids"
        '
        'btnShowAllPending
        '
        Me.btnShowAllPending.Location = New System.Drawing.Point(672, 0)
        Me.btnShowAllPending.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnShowAllPending.Name = "btnShowAllPending"
        Me.btnShowAllPending.Size = New System.Drawing.Size(117, 25)
        Me.btnShowAllPending.TabIndex = 16
        Me.btnShowAllPending.Text = "Show All Pending"
        '
        'lkPendingTimeframe
        '
        Me.lkPendingTimeframe.Location = New System.Drawing.Point(516, 0)
        Me.lkPendingTimeframe.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkPendingTimeframe.MenuManager = Me.RibbonControl
        Me.lkPendingTimeframe.Name = "lkPendingTimeframe"
        Me.lkPendingTimeframe.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkPendingTimeframe.Properties.ShowFooter = False
        Me.lkPendingTimeframe.Properties.ShowHeader = False
        Me.lkPendingTimeframe.Properties.ShowLines = False
        Me.lkPendingTimeframe.Size = New System.Drawing.Size(117, 22)
        Me.lkPendingTimeframe.TabIndex = 503
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(450, 4)
        Me.LabelControl3.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(63, 16)
        Me.LabelControl3.TabIndex = 502
        Me.LabelControl3.Text = "Timeframe"
        '
        'grpActivePackageDetail
        '
        Me.grpActivePackageDetail.Controls.Add(Me.lblPackageErrors)
        Me.grpActivePackageDetail.Controls.Add(Me.grdActivePackageDetails)
        Me.grpActivePackageDetail.Location = New System.Drawing.Point(12, 331)
        Me.grpActivePackageDetail.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpActivePackageDetail.Name = "grpActivePackageDetail"
        Me.grpActivePackageDetail.Size = New System.Drawing.Size(1182, 266)
        Me.grpActivePackageDetail.TabIndex = 8
        Me.grpActivePackageDetail.Text = "Active Package Details"
        '
        'lblPackageErrors
        '
        Me.lblPackageErrors.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblPackageErrors.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblPackageErrors.Location = New System.Drawing.Point(383, 5)
        Me.lblPackageErrors.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblPackageErrors.Name = "lblPackageErrors"
        Me.lblPackageErrors.Size = New System.Drawing.Size(207, 17)
        Me.lblPackageErrors.TabIndex = 6
        Me.lblPackageErrors.Text = "Package contains invalid items"
        Me.lblPackageErrors.Visible = False
        '
        'LayoutControlGroup7
        '
        Me.LayoutControlGroup7.CustomizationFormText = "LayoutControlGroup7"
        Me.LayoutControlGroup7.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup7.GroupBordersVisible = False
        Me.LayoutControlGroup7.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.SplitterItem6, Me.LayoutControlItem13, Me.LayoutControlItem12, Me.LayoutControlItem14, Me.SplitterItem7})
        Me.LayoutControlGroup7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup7.Name = "Root"
        Me.LayoutControlGroup7.Size = New System.Drawing.Size(1206, 609)
        Me.LayoutControlGroup7.Text = "Root"
        Me.LayoutControlGroup7.TextVisible = False
        '
        'SplitterItem6
        '
        Me.SplitterItem6.AllowHotTrack = True
        Me.SplitterItem6.CustomizationFormText = "SplitterItem6"
        Me.SplitterItem6.Location = New System.Drawing.Point(0, 186)
        Me.SplitterItem6.Name = "SplitterItem6"
        Me.SplitterItem6.Size = New System.Drawing.Size(1186, 5)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.grpActivePackageDetail
        Me.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13"
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 319)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(1186, 270)
        Me.LayoutControlItem13.Text = "LayoutControlItem13"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextToControlDistance = 0
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.grpPendingItems
        Me.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12"
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(1186, 186)
        Me.LayoutControlItem12.Text = "LayoutControlItem12"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextToControlDistance = 0
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.grpExistingPackages
        Me.LayoutControlItem14.CustomizationFormText = "LayoutControlItem14"
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 191)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(1186, 123)
        Me.LayoutControlItem14.Text = "LayoutControlItem14"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextToControlDistance = 0
        Me.LayoutControlItem14.TextVisible = False
        '
        'SplitterItem7
        '
        Me.SplitterItem7.AllowHotTrack = True
        Me.SplitterItem7.CustomizationFormText = "SplitterItem7"
        Me.SplitterItem7.Location = New System.Drawing.Point(0, 314)
        Me.SplitterItem7.Name = "SplitterItem7"
        Me.SplitterItem7.Size = New System.Drawing.Size(1186, 5)
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.CustomizationFormText = "Root"
        Me.LayoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup6.GroupBordersVisible = False
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem9, Me.LayoutControlItem10, Me.SplitterItem5})
        Me.LayoutControlGroup6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup6.Name = "Root"
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControlGroup6.Text = "Root"
        Me.LayoutControlGroup6.TextVisible = False
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.PanelControl3
        Me.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9"
        Me.LayoutControlItem9.Location = New System.Drawing.Point(202, 0)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(1214, 617)
        Me.LayoutControlItem9.Text = "LayoutControlItem9"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextToControlDistance = 0
        Me.LayoutControlItem9.TextVisible = False
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.PanelControl4
        Me.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10"
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(197, 617)
        Me.LayoutControlItem10.Text = "LayoutControlItem10"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextToControlDistance = 0
        Me.LayoutControlItem10.TextVisible = False
        '
        'SplitterItem5
        '
        Me.SplitterItem5.AllowHotTrack = True
        Me.SplitterItem5.CustomizationFormText = "SplitterItem5"
        Me.SplitterItem5.Location = New System.Drawing.Point(197, 0)
        Me.SplitterItem5.Name = "SplitterItem5"
        Me.SplitterItem5.Size = New System.Drawing.Size(5, 617)
        '
        'TabLogin
        '
        Me.TabLogin.Controls.Add(Me.GrpActiveUser)
        Me.TabLogin.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TabLogin.Name = "TabLogin"
        Me.TabLogin.Size = New System.Drawing.Size(1436, 637)
        Me.TabLogin.Text = "Login"
        '
        'GrpActiveUser
        '
        Me.GrpActiveUser.Controls.Add(Me.lkLoginDomains)
        Me.GrpActiveUser.Controls.Add(Me.lblDomain)
        Me.GrpActiveUser.Controls.Add(Me.lblUsername)
        Me.GrpActiveUser.Controls.Add(Me.txtUserPassword)
        Me.GrpActiveUser.Controls.Add(Me.lblPassword)
        Me.GrpActiveUser.Controls.Add(Me.btnLogin)
        Me.GrpActiveUser.Controls.Add(Me.txtUsername)
        Me.GrpActiveUser.Location = New System.Drawing.Point(112, 81)
        Me.GrpActiveUser.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GrpActiveUser.Name = "GrpActiveUser"
        Me.GrpActiveUser.Size = New System.Drawing.Size(279, 199)
        Me.GrpActiveUser.TabIndex = 0
        Me.GrpActiveUser.Text = "Active User:"
        '
        'lkLoginDomains
        '
        Me.lkLoginDomains.Location = New System.Drawing.Point(75, 39)
        Me.lkLoginDomains.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkLoginDomains.MenuManager = Me.RibbonControl
        Me.lkLoginDomains.Name = "lkLoginDomains"
        Me.lkLoginDomains.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkLoginDomains.Properties.ShowFooter = False
        Me.lkLoginDomains.Properties.ShowHeader = False
        Me.lkLoginDomains.Properties.ShowLines = False
        Me.lkLoginDomains.Size = New System.Drawing.Size(112, 22)
        Me.lkLoginDomains.TabIndex = 1000
        Me.lkLoginDomains.TabStop = False
        '
        'lblDomain
        '
        Me.lblDomain.Location = New System.Drawing.Point(14, 43)
        Me.lblDomain.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblDomain.Name = "lblDomain"
        Me.lblDomain.Size = New System.Drawing.Size(43, 16)
        Me.lblDomain.TabIndex = 47
        Me.lblDomain.Text = "Domain"
        '
        'lblUsername
        '
        Me.lblUsername.Location = New System.Drawing.Point(14, 75)
        Me.lblUsername.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(58, 16)
        Me.lblUsername.TabIndex = 45
        Me.lblUsername.Text = "Username"
        '
        'txtUserPassword
        '
        Me.txtUserPassword.Location = New System.Drawing.Point(75, 102)
        Me.txtUserPassword.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtUserPassword.Name = "txtUserPassword"
        Me.txtUserPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtUserPassword.Size = New System.Drawing.Size(112, 22)
        Me.txtUserPassword.TabIndex = 1002
        '
        'lblPassword
        '
        Me.lblPassword.Location = New System.Drawing.Point(14, 106)
        Me.lblPassword.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(55, 16)
        Me.lblPassword.TabIndex = 43
        Me.lblPassword.Text = "Password"
        '
        'btnLogin
        '
        Me.btnLogin.Location = New System.Drawing.Point(14, 130)
        Me.btnLogin.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(173, 31)
        Me.btnLogin.TabIndex = 1003
        Me.btnLogin.Text = "Login"
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(75, 71)
        Me.txtUsername.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(112, 22)
        Me.txtUsername.TabIndex = 1001
        '
        'TabCreateInvPackage
        '
        Me.TabCreateInvPackage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TabCreateInvPackage.Name = "TabCreateInvPackage"
        Me.TabCreateInvPackage.PageVisible = False
        Me.TabCreateInvPackage.Size = New System.Drawing.Size(1436, 637)
        Me.TabCreateInvPackage.Text = "Create Inventory Package"
        '
        'TabPackageMaintenance
        '
        Me.TabPackageMaintenance.Controls.Add(Me.LayoutControl7)
        Me.TabPackageMaintenance.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TabPackageMaintenance.Name = "TabPackageMaintenance"
        Me.TabPackageMaintenance.PageVisible = False
        Me.TabPackageMaintenance.Size = New System.Drawing.Size(1436, 637)
        Me.TabPackageMaintenance.Text = "Package Maintenance"
        '
        'LayoutControl7
        '
        Me.LayoutControl7.Controls.Add(Me.PanelControl6)
        Me.LayoutControl7.Controls.Add(Me.PanelControl5)
        Me.LayoutControl7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl7.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl7.Name = "LayoutControl7"
        Me.LayoutControl7.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(729, 272, 250, 350)
        Me.LayoutControl7.Root = Me.LayoutControlGroup8
        Me.LayoutControl7.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControl7.TabIndex = 504
        Me.LayoutControl7.Text = "LayoutControl7"
        '
        'PanelControl6
        '
        Me.PanelControl6.Controls.Add(Me.GroupControl19)
        Me.PanelControl6.Controls.Add(Me.grpDeliverPackage)
        Me.PanelControl6.Controls.Add(Me.grpUnfinalizePackage)
        Me.PanelControl6.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl6.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl6.Name = "PanelControl6"
        Me.PanelControl6.Size = New System.Drawing.Size(201, 613)
        Me.PanelControl6.TabIndex = 5
        '
        'GroupControl19
        '
        Me.GroupControl19.Controls.Add(Me.txtReprintPackageLabel)
        Me.GroupControl19.Controls.Add(Me.btnReprintPackageLabel)
        Me.GroupControl19.Location = New System.Drawing.Point(7, 311)
        Me.GroupControl19.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupControl19.Name = "GroupControl19"
        Me.GroupControl19.Size = New System.Drawing.Size(191, 96)
        Me.GroupControl19.TabIndex = 504
        Me.GroupControl19.Text = "Reprint Package Label"
        '
        'txtReprintPackageLabel
        '
        Me.txtReprintPackageLabel.EditValue = ""
        Me.txtReprintPackageLabel.Location = New System.Drawing.Point(8, 30)
        Me.txtReprintPackageLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtReprintPackageLabel.Name = "txtReprintPackageLabel"
        Me.txtReprintPackageLabel.Size = New System.Drawing.Size(171, 22)
        Me.txtReprintPackageLabel.TabIndex = 20
        '
        'btnReprintPackageLabel
        '
        Me.btnReprintPackageLabel.Location = New System.Drawing.Point(6, 62)
        Me.btnReprintPackageLabel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnReprintPackageLabel.Name = "btnReprintPackageLabel"
        Me.btnReprintPackageLabel.Size = New System.Drawing.Size(173, 31)
        Me.btnReprintPackageLabel.TabIndex = 19
        Me.btnReprintPackageLabel.Text = "Reprint Package Label"
        '
        'grpDeliverPackage
        '
        Me.grpDeliverPackage.Controls.Add(Me.txtDeliverPackageBarcode)
        Me.grpDeliverPackage.Controls.Add(Me.btnDeliverPackage)
        Me.grpDeliverPackage.Location = New System.Drawing.Point(6, 116)
        Me.grpDeliverPackage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpDeliverPackage.Name = "grpDeliverPackage"
        Me.grpDeliverPackage.Size = New System.Drawing.Size(191, 96)
        Me.grpDeliverPackage.TabIndex = 503
        Me.grpDeliverPackage.Text = "Deliver Package"
        '
        'txtDeliverPackageBarcode
        '
        Me.txtDeliverPackageBarcode.EditValue = ""
        Me.txtDeliverPackageBarcode.Location = New System.Drawing.Point(8, 30)
        Me.txtDeliverPackageBarcode.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtDeliverPackageBarcode.Name = "txtDeliverPackageBarcode"
        Me.txtDeliverPackageBarcode.Size = New System.Drawing.Size(171, 22)
        Me.txtDeliverPackageBarcode.TabIndex = 20
        '
        'btnDeliverPackage
        '
        Me.btnDeliverPackage.Location = New System.Drawing.Point(6, 62)
        Me.btnDeliverPackage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnDeliverPackage.Name = "btnDeliverPackage"
        Me.btnDeliverPackage.Size = New System.Drawing.Size(173, 31)
        Me.btnDeliverPackage.TabIndex = 19
        Me.btnDeliverPackage.Text = "Deliver Package"
        '
        'grpUnfinalizePackage
        '
        Me.grpUnfinalizePackage.Controls.Add(Me.txtFnlPackageBarcode)
        Me.grpUnfinalizePackage.Controls.Add(Me.btnUnfinalizePackage)
        Me.grpUnfinalizePackage.Location = New System.Drawing.Point(6, 6)
        Me.grpUnfinalizePackage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpUnfinalizePackage.Name = "grpUnfinalizePackage"
        Me.grpUnfinalizePackage.Size = New System.Drawing.Size(191, 96)
        Me.grpUnfinalizePackage.TabIndex = 502
        Me.grpUnfinalizePackage.Text = "Unfinalize Package"
        '
        'txtFnlPackageBarcode
        '
        Me.txtFnlPackageBarcode.EditValue = ""
        Me.txtFnlPackageBarcode.Location = New System.Drawing.Point(8, 30)
        Me.txtFnlPackageBarcode.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtFnlPackageBarcode.Name = "txtFnlPackageBarcode"
        Me.txtFnlPackageBarcode.Size = New System.Drawing.Size(171, 22)
        Me.txtFnlPackageBarcode.TabIndex = 20
        '
        'btnUnfinalizePackage
        '
        Me.btnUnfinalizePackage.Location = New System.Drawing.Point(6, 62)
        Me.btnUnfinalizePackage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnUnfinalizePackage.Name = "btnUnfinalizePackage"
        Me.btnUnfinalizePackage.Size = New System.Drawing.Size(173, 31)
        Me.btnUnfinalizePackage.TabIndex = 19
        Me.btnUnfinalizePackage.Text = "Unfinalize Package"
        '
        'PanelControl5
        '
        Me.PanelControl5.Controls.Add(Me.LayoutControl8)
        Me.PanelControl5.Location = New System.Drawing.Point(222, 12)
        Me.PanelControl5.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(1202, 613)
        Me.PanelControl5.TabIndex = 4
        '
        'LayoutControl8
        '
        Me.LayoutControl8.Controls.Add(Me.GroupControl15)
        Me.LayoutControl8.Controls.Add(Me.grpFinalizedPackages)
        Me.LayoutControl8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl8.Location = New System.Drawing.Point(2, 2)
        Me.LayoutControl8.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl8.Name = "LayoutControl8"
        Me.LayoutControl8.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(300, 344, 250, 350)
        Me.LayoutControl8.Root = Me.LayoutControlGroup9
        Me.LayoutControl8.Size = New System.Drawing.Size(1198, 609)
        Me.LayoutControl8.TabIndex = 0
        Me.LayoutControl8.Text = "LayoutControl8"
        '
        'GroupControl15
        '
        Me.GroupControl15.Controls.Add(Me.grdFinalPackageDetails)
        Me.GroupControl15.Location = New System.Drawing.Point(12, 264)
        Me.GroupControl15.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupControl15.Name = "GroupControl15"
        Me.GroupControl15.Size = New System.Drawing.Size(1174, 333)
        Me.GroupControl15.TabIndex = 503
        Me.GroupControl15.Text = "Package Details"
        '
        'grpFinalizedPackages
        '
        Me.grpFinalizedPackages.Controls.Add(Me.lkFinPkgTimeline)
        Me.grpFinalizedPackages.Controls.Add(Me.LabelControl9)
        Me.grpFinalizedPackages.Controls.Add(Me.grdFinalizedPackages)
        Me.grpFinalizedPackages.Location = New System.Drawing.Point(12, 12)
        Me.grpFinalizedPackages.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpFinalizedPackages.Name = "grpFinalizedPackages"
        Me.grpFinalizedPackages.Size = New System.Drawing.Size(1174, 243)
        Me.grpFinalizedPackages.TabIndex = 18
        Me.grpFinalizedPackages.Text = "Finalized Packages"
        '
        'lkFinPkgTimeline
        '
        Me.lkFinPkgTimeline.Location = New System.Drawing.Point(1021, 1)
        Me.lkFinPkgTimeline.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkFinPkgTimeline.MenuManager = Me.RibbonControl
        Me.lkFinPkgTimeline.Name = "lkFinPkgTimeline"
        Me.lkFinPkgTimeline.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkFinPkgTimeline.Properties.ShowFooter = False
        Me.lkFinPkgTimeline.Properties.ShowHeader = False
        Me.lkFinPkgTimeline.Properties.ShowLines = False
        Me.lkFinPkgTimeline.Size = New System.Drawing.Size(117, 22)
        Me.lkFinPkgTimeline.TabIndex = 505
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(938, 5)
        Me.LabelControl9.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl9.TabIndex = 504
        Me.LabelControl9.Text = "Date Created"
        '
        'LayoutControlGroup9
        '
        Me.LayoutControlGroup9.CustomizationFormText = "Root"
        Me.LayoutControlGroup9.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup9.GroupBordersVisible = False
        Me.LayoutControlGroup9.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem16, Me.LayoutControlItem17, Me.SplitterItem9})
        Me.LayoutControlGroup9.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup9.Name = "Root"
        Me.LayoutControlGroup9.Size = New System.Drawing.Size(1198, 609)
        Me.LayoutControlGroup9.Text = "Root"
        Me.LayoutControlGroup9.TextVisible = False
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.GroupControl15
        Me.LayoutControlItem16.CustomizationFormText = "LayoutControlItem16"
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 252)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(1178, 337)
        Me.LayoutControlItem16.Text = "LayoutControlItem16"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextToControlDistance = 0
        Me.LayoutControlItem16.TextVisible = False
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.grpFinalizedPackages
        Me.LayoutControlItem17.CustomizationFormText = "LayoutControlItem17"
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(1178, 247)
        Me.LayoutControlItem17.Text = "LayoutControlItem17"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextToControlDistance = 0
        Me.LayoutControlItem17.TextVisible = False
        '
        'SplitterItem9
        '
        Me.SplitterItem9.AllowHotTrack = True
        Me.SplitterItem9.CustomizationFormText = "SplitterItem9"
        Me.SplitterItem9.Location = New System.Drawing.Point(0, 247)
        Me.SplitterItem9.Name = "SplitterItem9"
        Me.SplitterItem9.Size = New System.Drawing.Size(1178, 5)
        '
        'LayoutControlGroup8
        '
        Me.LayoutControlGroup8.CustomizationFormText = "Root"
        Me.LayoutControlGroup8.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup8.GroupBordersVisible = False
        Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem11, Me.LayoutControlItem15, Me.SplitterItem8})
        Me.LayoutControlGroup8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup8.Name = "Root"
        Me.LayoutControlGroup8.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControlGroup8.Text = "Root"
        Me.LayoutControlGroup8.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.PanelControl5
        Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
        Me.LayoutControlItem11.Location = New System.Drawing.Point(210, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(1206, 617)
        Me.LayoutControlItem11.Text = "LayoutControlItem11"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextToControlDistance = 0
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.PanelControl6
        Me.LayoutControlItem15.CustomizationFormText = "LayoutControlItem15"
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(205, 617)
        Me.LayoutControlItem15.Text = "LayoutControlItem15"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextToControlDistance = 0
        Me.LayoutControlItem15.TextVisible = False
        '
        'SplitterItem8
        '
        Me.SplitterItem8.AllowHotTrack = True
        Me.SplitterItem8.CustomizationFormText = "SplitterItem8"
        Me.SplitterItem8.Location = New System.Drawing.Point(205, 0)
        Me.SplitterItem8.Name = "SplitterItem8"
        Me.SplitterItem8.Size = New System.Drawing.Size(5, 617)
        '
        'TabCreateShipment
        '
        Me.TabCreateShipment.Controls.Add(Me.LayoutControl9)
        Me.TabCreateShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TabCreateShipment.Name = "TabCreateShipment"
        Me.TabCreateShipment.PageVisible = False
        Me.TabCreateShipment.Size = New System.Drawing.Size(1436, 637)
        Me.TabCreateShipment.Text = "Create Shipment"
        '
        'LayoutControl9
        '
        Me.LayoutControl9.Controls.Add(Me.PanelControl8)
        Me.LayoutControl9.Controls.Add(Me.PanelControl7)
        Me.LayoutControl9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl9.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl9.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl9.Name = "LayoutControl9"
        Me.LayoutControl9.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(615, 251, 250, 350)
        Me.LayoutControl9.Root = Me.LayoutControlGroup10
        Me.LayoutControl9.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControl9.TabIndex = 22
        Me.LayoutControl9.Text = "LayoutControl9"
        '
        'PanelControl8
        '
        Me.PanelControl8.Controls.Add(Me.lblShipmentType)
        Me.PanelControl8.Controls.Add(Me.lkShipmentType)
        Me.PanelControl8.Controls.Add(Me.grpFinalizeShipment)
        Me.PanelControl8.Controls.Add(Me.lblFillingMaterial)
        Me.PanelControl8.Controls.Add(Me.grpShipmentRules)
        Me.PanelControl8.Controls.Add(Me.lkFillingMaterial)
        Me.PanelControl8.Controls.Add(Me.grpAddPackage)
        Me.PanelControl8.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl8.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl8.Name = "PanelControl8"
        Me.PanelControl8.Size = New System.Drawing.Size(194, 613)
        Me.PanelControl8.TabIndex = 5
        '
        'lblShipmentType
        '
        Me.lblShipmentType.Location = New System.Drawing.Point(12, 615)
        Me.lblShipmentType.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblShipmentType.Name = "lblShipmentType"
        Me.lblShipmentType.Size = New System.Drawing.Size(86, 16)
        Me.lblShipmentType.TabIndex = 49
        Me.lblShipmentType.Text = "Shipment Type"
        Me.lblShipmentType.Visible = False
        '
        'lkShipmentType
        '
        Me.lkShipmentType.EditValue = ""
        Me.lkShipmentType.Location = New System.Drawing.Point(12, 636)
        Me.lkShipmentType.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkShipmentType.Name = "lkShipmentType"
        Me.lkShipmentType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkShipmentType.Properties.NullText = ""
        Me.lkShipmentType.Properties.ShowFooter = False
        Me.lkShipmentType.Properties.ShowHeader = False
        Me.lkShipmentType.Properties.ShowLines = False
        Me.lkShipmentType.Properties.ShowPopupShadow = False
        Me.lkShipmentType.Size = New System.Drawing.Size(180, 22)
        Me.lkShipmentType.TabIndex = 32
        Me.lkShipmentType.Visible = False
        '
        'grpFinalizeShipment
        '
        Me.grpFinalizeShipment.Controls.Add(Me.lblPriority)
        Me.grpFinalizeShipment.Controls.Add(Me.btnViewShipmentForm)
        Me.grpFinalizeShipment.Controls.Add(Me.lblShippingCourier)
        Me.grpFinalizeShipment.Controls.Add(Me.lkShippingPriority)
        Me.grpFinalizeShipment.Controls.Add(Me.btnFinalizeShipment)
        Me.grpFinalizeShipment.Controls.Add(Me.lkShippingCourier)
        Me.grpFinalizeShipment.Location = New System.Drawing.Point(6, 235)
        Me.grpFinalizeShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpFinalizeShipment.Name = "grpFinalizeShipment"
        Me.grpFinalizeShipment.Size = New System.Drawing.Size(191, 201)
        Me.grpFinalizeShipment.TabIndex = 10
        Me.grpFinalizeShipment.Text = "Finalize Shipment"
        '
        'lblPriority
        '
        Me.lblPriority.Location = New System.Drawing.Point(6, 76)
        Me.lblPriority.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblPriority.Name = "lblPriority"
        Me.lblPriority.Size = New System.Drawing.Size(40, 16)
        Me.lblPriority.TabIndex = 45
        Me.lblPriority.Text = "Priority"
        '
        'btnViewShipmentForm
        '
        Me.btnViewShipmentForm.Location = New System.Drawing.Point(6, 124)
        Me.btnViewShipmentForm.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnViewShipmentForm.Name = "btnViewShipmentForm"
        Me.btnViewShipmentForm.Size = New System.Drawing.Size(173, 31)
        Me.btnViewShipmentForm.TabIndex = 41
        Me.btnViewShipmentForm.Text = "Finalize and Print Forms"
        '
        'lblShippingCourier
        '
        Me.lblShippingCourier.Location = New System.Drawing.Point(6, 30)
        Me.lblShippingCourier.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblShippingCourier.Name = "lblShippingCourier"
        Me.lblShippingCourier.Size = New System.Drawing.Size(95, 16)
        Me.lblShippingCourier.TabIndex = 44
        Me.lblShippingCourier.Text = "Shipping Courier"
        '
        'lkShippingPriority
        '
        Me.lkShippingPriority.EditValue = ""
        Me.lkShippingPriority.Location = New System.Drawing.Point(6, 92)
        Me.lkShippingPriority.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkShippingPriority.Name = "lkShippingPriority"
        Me.lkShippingPriority.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkShippingPriority.Properties.NullText = ""
        Me.lkShippingPriority.Properties.ShowFooter = False
        Me.lkShippingPriority.Properties.ShowHeader = False
        Me.lkShippingPriority.Properties.ShowLines = False
        Me.lkShippingPriority.Properties.ShowPopupShadow = False
        Me.lkShippingPriority.Size = New System.Drawing.Size(180, 22)
        Me.lkShippingPriority.TabIndex = 31
        '
        'btnFinalizeShipment
        '
        Me.btnFinalizeShipment.Location = New System.Drawing.Point(6, 160)
        Me.btnFinalizeShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnFinalizeShipment.Name = "btnFinalizeShipment"
        Me.btnFinalizeShipment.Size = New System.Drawing.Size(173, 31)
        Me.btnFinalizeShipment.TabIndex = 40
        Me.btnFinalizeShipment.Text = "Finalize"
        '
        'lkShippingCourier
        '
        Me.lkShippingCourier.EditValue = ""
        Me.lkShippingCourier.Location = New System.Drawing.Point(6, 49)
        Me.lkShippingCourier.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkShippingCourier.Name = "lkShippingCourier"
        Me.lkShippingCourier.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkShippingCourier.Properties.NullText = ""
        Me.lkShippingCourier.Properties.ShowFooter = False
        Me.lkShippingCourier.Properties.ShowHeader = False
        Me.lkShippingCourier.Properties.ShowLines = False
        Me.lkShippingCourier.Properties.ShowPopupShadow = False
        Me.lkShippingCourier.Size = New System.Drawing.Size(180, 22)
        Me.lkShippingCourier.TabIndex = 30
        '
        'lblFillingMaterial
        '
        Me.lblFillingMaterial.Location = New System.Drawing.Point(13, 668)
        Me.lblFillingMaterial.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblFillingMaterial.Name = "lblFillingMaterial"
        Me.lblFillingMaterial.Size = New System.Drawing.Size(83, 16)
        Me.lblFillingMaterial.TabIndex = 47
        Me.lblFillingMaterial.Text = "Filling Material"
        Me.lblFillingMaterial.Visible = False
        '
        'grpShipmentRules
        '
        Me.grpShipmentRules.Controls.Add(Me.ChkLstShipmentValidationRules)
        Me.grpShipmentRules.Location = New System.Drawing.Point(6, 146)
        Me.grpShipmentRules.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpShipmentRules.Name = "grpShipmentRules"
        Me.grpShipmentRules.Size = New System.Drawing.Size(191, 79)
        Me.grpShipmentRules.TabIndex = 11
        Me.grpShipmentRules.Text = "Validation Rules"
        '
        'ChkLstShipmentValidationRules
        '
        Me.ChkLstShipmentValidationRules.CheckOnClick = True
        Me.ChkLstShipmentValidationRules.Items.AddRange(New DevExpress.XtraEditors.Controls.CheckedListBoxItem() {New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Same Destination"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Same Company Same Site", System.Windows.Forms.CheckState.Checked)})
        Me.ChkLstShipmentValidationRules.Location = New System.Drawing.Point(6, 22)
        Me.ChkLstShipmentValidationRules.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.ChkLstShipmentValidationRules.Name = "ChkLstShipmentValidationRules"
        Me.ChkLstShipmentValidationRules.Size = New System.Drawing.Size(180, 54)
        Me.ChkLstShipmentValidationRules.TabIndex = 34
        '
        'lkFillingMaterial
        '
        Me.lkFillingMaterial.EditValue = ""
        Me.lkFillingMaterial.Location = New System.Drawing.Point(13, 684)
        Me.lkFillingMaterial.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkFillingMaterial.Name = "lkFillingMaterial"
        Me.lkFillingMaterial.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkFillingMaterial.Properties.NullText = ""
        Me.lkFillingMaterial.Properties.ShowFooter = False
        Me.lkFillingMaterial.Properties.ShowHeader = False
        Me.lkFillingMaterial.Properties.ShowLines = False
        Me.lkFillingMaterial.Properties.ShowPopupShadow = False
        Me.lkFillingMaterial.Size = New System.Drawing.Size(180, 22)
        Me.lkFillingMaterial.TabIndex = 33
        Me.lkFillingMaterial.Visible = False
        '
        'grpAddPackage
        '
        Me.grpAddPackage.Controls.Add(Me.btnCreateShipment)
        Me.grpAddPackage.Controls.Add(Me.btnAddPackageShipment)
        Me.grpAddPackage.Controls.Add(Me.txtShipmentPackageBarcode)
        Me.grpAddPackage.Location = New System.Drawing.Point(6, 6)
        Me.grpAddPackage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpAddPackage.Name = "grpAddPackage"
        Me.grpAddPackage.Size = New System.Drawing.Size(191, 133)
        Me.grpAddPackage.TabIndex = 21
        Me.grpAddPackage.Text = "Add Packages"
        '
        'btnCreateShipment
        '
        Me.btnCreateShipment.Location = New System.Drawing.Point(7, 96)
        Me.btnCreateShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnCreateShipment.Name = "btnCreateShipment"
        Me.btnCreateShipment.Size = New System.Drawing.Size(173, 31)
        Me.btnCreateShipment.TabIndex = 36
        Me.btnCreateShipment.Text = "Create Empty Shipment"
        '
        'btnAddPackageShipment
        '
        Me.btnAddPackageShipment.Location = New System.Drawing.Point(6, 60)
        Me.btnAddPackageShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnAddPackageShipment.Name = "btnAddPackageShipment"
        Me.btnAddPackageShipment.Size = New System.Drawing.Size(173, 31)
        Me.btnAddPackageShipment.TabIndex = 41
        Me.btnAddPackageShipment.Text = "Add Package"
        '
        'txtShipmentPackageBarcode
        '
        Me.txtShipmentPackageBarcode.Location = New System.Drawing.Point(7, 28)
        Me.txtShipmentPackageBarcode.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtShipmentPackageBarcode.Name = "txtShipmentPackageBarcode"
        Me.txtShipmentPackageBarcode.Size = New System.Drawing.Size(171, 22)
        Me.txtShipmentPackageBarcode.TabIndex = 38
        '
        'PanelControl7
        '
        Me.PanelControl7.Controls.Add(Me.LayoutControl10)
        Me.PanelControl7.Location = New System.Drawing.Point(215, 12)
        Me.PanelControl7.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(1209, 613)
        Me.PanelControl7.TabIndex = 4
        '
        'LayoutControl10
        '
        Me.LayoutControl10.Controls.Add(Me.GroupControl12)
        Me.LayoutControl10.Controls.Add(Me.grpExistingShipments)
        Me.LayoutControl10.Controls.Add(Me.grpPendingPackages)
        Me.LayoutControl10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl10.Location = New System.Drawing.Point(2, 2)
        Me.LayoutControl10.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl10.Name = "LayoutControl10"
        Me.LayoutControl10.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(889, 237, 250, 350)
        Me.LayoutControl10.Root = Me.LayoutControlGroup11
        Me.LayoutControl10.Size = New System.Drawing.Size(1205, 609)
        Me.LayoutControl10.TabIndex = 0
        Me.LayoutControl10.Text = "LayoutControl10"
        '
        'GroupControl12
        '
        Me.GroupControl12.Controls.Add(Me.grdActiveShipmentDetails)
        Me.GroupControl12.Location = New System.Drawing.Point(12, 293)
        Me.GroupControl12.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupControl12.Name = "GroupControl12"
        Me.GroupControl12.Size = New System.Drawing.Size(1181, 304)
        Me.GroupControl12.TabIndex = 18
        Me.GroupControl12.Text = "Active Shipment Details"
        '
        'grpExistingShipments
        '
        Me.grpExistingShipments.Controls.Add(Me.grdExistingShipments)
        Me.grpExistingShipments.Location = New System.Drawing.Point(12, 188)
        Me.grpExistingShipments.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpExistingShipments.Name = "grpExistingShipments"
        Me.grpExistingShipments.Size = New System.Drawing.Size(1181, 96)
        Me.grpExistingShipments.TabIndex = 17
        Me.grpExistingShipments.Text = "Existing Shipments"
        '
        'grpPendingPackages
        '
        Me.grpPendingPackages.Controls.Add(Me.btnShowPendingPackages)
        Me.grpPendingPackages.Controls.Add(Me.btnRefreshShipment)
        Me.grpPendingPackages.Controls.Add(Me.grdPendingPackages)
        Me.grpPendingPackages.Location = New System.Drawing.Point(12, 12)
        Me.grpPendingPackages.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpPendingPackages.Name = "grpPendingPackages"
        Me.grpPendingPackages.Size = New System.Drawing.Size(1181, 167)
        Me.grpPendingPackages.TabIndex = 16
        Me.grpPendingPackages.Text = "Pending Packages"
        '
        'btnShowPendingPackages
        '
        Me.btnShowPendingPackages.Location = New System.Drawing.Point(531, 0)
        Me.btnShowPendingPackages.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnShowPendingPackages.Name = "btnShowPendingPackages"
        Me.btnShowPendingPackages.Size = New System.Drawing.Size(117, 25)
        Me.btnShowPendingPackages.TabIndex = 17
        Me.btnShowPendingPackages.Text = "Show All Pending"
        '
        'btnRefreshShipment
        '
        Me.btnRefreshShipment.Location = New System.Drawing.Point(198, 0)
        Me.btnRefreshShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnRefreshShipment.Name = "btnRefreshShipment"
        Me.btnRefreshShipment.Size = New System.Drawing.Size(148, 23)
        Me.btnRefreshShipment.TabIndex = 6
        Me.btnRefreshShipment.Text = "Refresh Shipment Grids"
        '
        'LayoutControlGroup11
        '
        Me.LayoutControlGroup11.CustomizationFormText = "Root"
        Me.LayoutControlGroup11.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup11.GroupBordersVisible = False
        Me.LayoutControlGroup11.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem20, Me.LayoutControlItem21, Me.LayoutControlItem22, Me.SplitterItem11, Me.SplitterItem12})
        Me.LayoutControlGroup11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup11.Name = "Root"
        Me.LayoutControlGroup11.Size = New System.Drawing.Size(1205, 609)
        Me.LayoutControlGroup11.Text = "Root"
        Me.LayoutControlGroup11.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.GroupControl12
        Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 281)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(1185, 308)
        Me.LayoutControlItem20.Text = "LayoutControlItem20"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextToControlDistance = 0
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.grpExistingShipments
        Me.LayoutControlItem21.CustomizationFormText = "LayoutControlItem21"
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 176)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(1185, 100)
        Me.LayoutControlItem21.Text = "LayoutControlItem21"
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextToControlDistance = 0
        Me.LayoutControlItem21.TextVisible = False
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.grpPendingPackages
        Me.LayoutControlItem22.CustomizationFormText = "LayoutControlItem22"
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(1185, 171)
        Me.LayoutControlItem22.Text = "LayoutControlItem22"
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextToControlDistance = 0
        Me.LayoutControlItem22.TextVisible = False
        '
        'SplitterItem11
        '
        Me.SplitterItem11.AllowHotTrack = True
        Me.SplitterItem11.CustomizationFormText = "SplitterItem11"
        Me.SplitterItem11.Location = New System.Drawing.Point(0, 276)
        Me.SplitterItem11.Name = "SplitterItem11"
        Me.SplitterItem11.Size = New System.Drawing.Size(1185, 5)
        '
        'SplitterItem12
        '
        Me.SplitterItem12.AllowHotTrack = True
        Me.SplitterItem12.CustomizationFormText = "SplitterItem12"
        Me.SplitterItem12.Location = New System.Drawing.Point(0, 171)
        Me.SplitterItem12.Name = "SplitterItem12"
        Me.SplitterItem12.Size = New System.Drawing.Size(1185, 5)
        '
        'LayoutControlGroup10
        '
        Me.LayoutControlGroup10.CustomizationFormText = "Root"
        Me.LayoutControlGroup10.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup10.GroupBordersVisible = False
        Me.LayoutControlGroup10.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem18, Me.LayoutControlItem19, Me.SplitterItem10})
        Me.LayoutControlGroup10.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup10.Name = "Root"
        Me.LayoutControlGroup10.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControlGroup10.Text = "Root"
        Me.LayoutControlGroup10.TextVisible = False
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.PanelControl7
        Me.LayoutControlItem18.CustomizationFormText = "LayoutControlItem18"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(203, 0)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(1213, 617)
        Me.LayoutControlItem18.Text = "LayoutControlItem18"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextToControlDistance = 0
        Me.LayoutControlItem18.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.PanelControl8
        Me.LayoutControlItem19.CustomizationFormText = "LayoutControlItem19"
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(198, 617)
        Me.LayoutControlItem19.Text = "LayoutControlItem19"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextToControlDistance = 0
        Me.LayoutControlItem19.TextVisible = False
        '
        'SplitterItem10
        '
        Me.SplitterItem10.AllowHotTrack = True
        Me.SplitterItem10.CustomizationFormText = "SplitterItem10"
        Me.SplitterItem10.Location = New System.Drawing.Point(198, 0)
        Me.SplitterItem10.Name = "SplitterItem10"
        Me.SplitterItem10.Size = New System.Drawing.Size(5, 617)
        '
        'TabShipmentPlanner
        '
        Me.TabShipmentPlanner.Controls.Add(Me.LayoutControl4)
        Me.TabShipmentPlanner.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TabShipmentPlanner.Name = "TabShipmentPlanner"
        Me.TabShipmentPlanner.PageVisible = False
        Me.TabShipmentPlanner.Size = New System.Drawing.Size(1436, 637)
        Me.TabShipmentPlanner.Text = "Shipment Planner"
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.grpPendingShipments)
        Me.LayoutControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl4.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1043, 426, 250, 350)
        Me.LayoutControl4.Root = Me.LayoutControlGroup5
        Me.LayoutControl4.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControl4.TabIndex = 19
        Me.LayoutControl4.Text = "LayoutControl4"
        '
        'grpPendingShipments
        '
        Me.grpPendingShipments.Controls.Add(Me.grdFinalizedShipments)
        Me.grpPendingShipments.Location = New System.Drawing.Point(12, 12)
        Me.grpPendingShipments.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpPendingShipments.Name = "grpPendingShipments"
        Me.grpPendingShipments.Size = New System.Drawing.Size(1412, 613)
        Me.grpPendingShipments.TabIndex = 18
        Me.grpPendingShipments.Text = "Finalized Shipments"
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.CustomizationFormText = "LayoutControlGroup5"
        Me.LayoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup5.GroupBordersVisible = False
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem8})
        Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControlGroup5.Text = "LayoutControlGroup5"
        Me.LayoutControlGroup5.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.grpPendingShipments
        Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8"
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(1416, 617)
        Me.LayoutControlItem8.Text = "LayoutControlItem8"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextToControlDistance = 0
        Me.LayoutControlItem8.TextVisible = False
        '
        'TabViewShipment
        '
        Me.TabViewShipment.Controls.Add(Me.LayoutControl3)
        Me.TabViewShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TabViewShipment.Name = "TabViewShipment"
        Me.TabViewShipment.Size = New System.Drawing.Size(1436, 637)
        Me.TabViewShipment.Text = "View Details"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.PanelControl2)
        Me.LayoutControl3.Controls.Add(Me.PanelControl1)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl3.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(660, 287, 250, 350)
        Me.LayoutControl3.Root = Me.LayoutControlGroup4
        Me.LayoutControl3.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "LayoutControl3"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.GroupControl21)
        Me.PanelControl2.Controls.Add(Me.GroupControl20)
        Me.PanelControl2.Controls.Add(Me.grpViewShipmentInput)
        Me.PanelControl2.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(198, 613)
        Me.PanelControl2.TabIndex = 5
        '
        'GroupControl21
        '
        Me.GroupControl21.Controls.Add(Me.btnTXTExport)
        Me.GroupControl21.Controls.Add(Me.btnExportXSL)
        Me.GroupControl21.Location = New System.Drawing.Point(7, 374)
        Me.GroupControl21.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupControl21.Name = "GroupControl21"
        Me.GroupControl21.Size = New System.Drawing.Size(191, 106)
        Me.GroupControl21.TabIndex = 25
        Me.GroupControl21.Text = "Export Grid"
        '
        'btnTXTExport
        '
        Me.btnTXTExport.Location = New System.Drawing.Point(5, 68)
        Me.btnTXTExport.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnTXTExport.Name = "btnTXTExport"
        Me.btnTXTExport.Size = New System.Drawing.Size(173, 31)
        Me.btnTXTExport.TabIndex = 43
        Me.btnTXTExport.Text = "Export as TXT"
        '
        'btnExportXSL
        '
        Me.btnExportXSL.Location = New System.Drawing.Point(5, 30)
        Me.btnExportXSL.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnExportXSL.Name = "btnExportXSL"
        Me.btnExportXSL.Size = New System.Drawing.Size(173, 31)
        Me.btnExportXSL.TabIndex = 42
        Me.btnExportXSL.Text = "Export as XSL"
        '
        'GroupControl20
        '
        Me.GroupControl20.Controls.Add(Me.LabelControl13)
        Me.GroupControl20.Controls.Add(Me.LabelControl12)
        Me.GroupControl20.Controls.Add(Me.LabelControl11)
        Me.GroupControl20.Controls.Add(Me.dtEditFromDate)
        Me.GroupControl20.Controls.Add(Me.lkSites)
        Me.GroupControl20.Controls.Add(Me.dtEditToDate)
        Me.GroupControl20.Controls.Add(Me.btnSiteContainers)
        Me.GroupControl20.Location = New System.Drawing.Point(7, 118)
        Me.GroupControl20.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupControl20.Name = "GroupControl20"
        Me.GroupControl20.Size = New System.Drawing.Size(191, 249)
        Me.GroupControl20.TabIndex = 24
        Me.GroupControl20.Text = "View Received Containers"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(8, 153)
        Me.LabelControl13.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(45, 16)
        Me.LabelControl13.TabIndex = 508
        Me.LabelControl13.Text = "To Date"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(8, 97)
        Me.LabelControl12.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(60, 16)
        Me.LabelControl12.TabIndex = 507
        Me.LabelControl12.Text = "From Date"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(8, 42)
        Me.LabelControl11.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(22, 16)
        Me.LabelControl11.TabIndex = 506
        Me.LabelControl11.Text = "Site"
        '
        'dtEditFromDate
        '
        Me.dtEditFromDate.EditValue = Nothing
        Me.dtEditFromDate.Location = New System.Drawing.Point(7, 121)
        Me.dtEditFromDate.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtEditFromDate.MenuManager = Me.RibbonControl
        Me.dtEditFromDate.Name = "dtEditFromDate"
        Me.dtEditFromDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtEditFromDate.Properties.Mask.EditMask = "g"
        Me.dtEditFromDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dtEditFromDate.Size = New System.Drawing.Size(173, 22)
        Me.dtEditFromDate.TabIndex = 505
        '
        'lkSites
        '
        Me.lkSites.Location = New System.Drawing.Point(8, 65)
        Me.lkSites.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkSites.MenuManager = Me.RibbonControl
        Me.lkSites.Name = "lkSites"
        Me.lkSites.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkSites.Properties.ShowFooter = False
        Me.lkSites.Properties.ShowHeader = False
        Me.lkSites.Properties.ShowLines = False
        Me.lkSites.Size = New System.Drawing.Size(171, 22)
        Me.lkSites.TabIndex = 504
        '
        'dtEditToDate
        '
        Me.dtEditToDate.EditValue = Nothing
        Me.dtEditToDate.Location = New System.Drawing.Point(8, 176)
        Me.dtEditToDate.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtEditToDate.MenuManager = Me.RibbonControl
        Me.dtEditToDate.Name = "dtEditToDate"
        Me.dtEditToDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtEditToDate.Properties.Mask.EditMask = "g"
        Me.dtEditToDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dtEditToDate.Size = New System.Drawing.Size(171, 22)
        Me.dtEditToDate.TabIndex = 43
        '
        'btnSiteContainers
        '
        Me.btnSiteContainers.Location = New System.Drawing.Point(7, 212)
        Me.btnSiteContainers.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSiteContainers.Name = "btnSiteContainers"
        Me.btnSiteContainers.Size = New System.Drawing.Size(173, 31)
        Me.btnSiteContainers.TabIndex = 41
        Me.btnSiteContainers.Text = "View Received Container"
        '
        'grpViewShipmentInput
        '
        Me.grpViewShipmentInput.Controls.Add(Me.btnContainerBarcodeLookup)
        Me.grpViewShipmentInput.Controls.Add(Me.txtViewShipment)
        Me.grpViewShipmentInput.Location = New System.Drawing.Point(6, 6)
        Me.grpViewShipmentInput.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpViewShipmentInput.Name = "grpViewShipmentInput"
        Me.grpViewShipmentInput.Size = New System.Drawing.Size(191, 105)
        Me.grpViewShipmentInput.TabIndex = 23
        Me.grpViewShipmentInput.Text = "View Container"
        '
        'btnContainerBarcodeLookup
        '
        Me.btnContainerBarcodeLookup.Location = New System.Drawing.Point(6, 60)
        Me.btnContainerBarcodeLookup.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnContainerBarcodeLookup.Name = "btnContainerBarcodeLookup"
        Me.btnContainerBarcodeLookup.Size = New System.Drawing.Size(173, 31)
        Me.btnContainerBarcodeLookup.TabIndex = 41
        Me.btnContainerBarcodeLookup.Text = "View Container"
        '
        'txtViewShipment
        '
        Me.txtViewShipment.Location = New System.Drawing.Point(7, 28)
        Me.txtViewShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtViewShipment.Name = "txtViewShipment"
        Me.txtViewShipment.Size = New System.Drawing.Size(171, 22)
        Me.txtViewShipment.TabIndex = 38
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.grpShipmentInfo)
        Me.PanelControl1.Location = New System.Drawing.Point(219, 12)
        Me.PanelControl1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1205, 613)
        Me.PanelControl1.TabIndex = 4
        '
        'grpShipmentInfo
        '
        Me.grpShipmentInfo.AutoSize = True
        Me.grpShipmentInfo.Controls.Add(Me.GridShipment)
        Me.grpShipmentInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpShipmentInfo.Location = New System.Drawing.Point(2, 2)
        Me.grpShipmentInfo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpShipmentInfo.Name = "grpShipmentInfo"
        Me.grpShipmentInfo.Size = New System.Drawing.Size(1201, 609)
        Me.grpShipmentInfo.TabIndex = 18
        Me.grpShipmentInfo.Text = "Container Details"
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.CustomizationFormText = "LayoutControlGroup4"
        Me.LayoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup4.GroupBordersVisible = False
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem6, Me.LayoutControlItem7, Me.SplitterItem4})
        Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup4.Name = "Root"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControlGroup4.Text = "Root"
        Me.LayoutControlGroup4.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.PanelControl1
        Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
        Me.LayoutControlItem6.Location = New System.Drawing.Point(207, 0)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(1209, 617)
        Me.LayoutControlItem6.Text = "LayoutControlItem6"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextToControlDistance = 0
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.PanelControl2
        Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(202, 617)
        Me.LayoutControlItem7.Text = "LayoutControlItem7"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextToControlDistance = 0
        Me.LayoutControlItem7.TextVisible = False
        '
        'SplitterItem4
        '
        Me.SplitterItem4.AllowHotTrack = True
        Me.SplitterItem4.CustomizationFormText = "SplitterItem4"
        Me.SplitterItem4.Location = New System.Drawing.Point(202, 0)
        Me.SplitterItem4.Name = "SplitterItem4"
        Me.SplitterItem4.Size = New System.Drawing.Size(5, 617)
        '
        'TabReceive
        '
        Me.TabReceive.Controls.Add(Me.LayoutControl11)
        Me.TabReceive.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TabReceive.Name = "TabReceive"
        Me.TabReceive.Size = New System.Drawing.Size(1436, 637)
        Me.TabReceive.Text = "Receive"
        '
        'LayoutControl11
        '
        Me.LayoutControl11.Controls.Add(Me.PanelControl9)
        Me.LayoutControl11.Controls.Add(Me.GroupControl13)
        Me.LayoutControl11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl11.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl11.Name = "LayoutControl11"
        Me.LayoutControl11.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(620, 406, 250, 350)
        Me.LayoutControl11.Root = Me.LayoutControlGroup12
        Me.LayoutControl11.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControl11.TabIndex = 23
        Me.LayoutControl11.Text = "LayoutControl11"
        '
        'PanelControl9
        '
        Me.PanelControl9.Controls.Add(Me.LayoutControl12)
        Me.PanelControl9.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl9.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl9.Name = "PanelControl9"
        Me.PanelControl9.Size = New System.Drawing.Size(1412, 434)
        Me.PanelControl9.TabIndex = 19
        '
        'LayoutControl12
        '
        Me.LayoutControl12.Controls.Add(Me.PanelControl11)
        Me.LayoutControl12.Controls.Add(Me.PanelControl10)
        Me.LayoutControl12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl12.Location = New System.Drawing.Point(2, 2)
        Me.LayoutControl12.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl12.Name = "LayoutControl12"
        Me.LayoutControl12.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(648, 205, 250, 350)
        Me.LayoutControl12.Root = Me.LayoutControlGroup13
        Me.LayoutControl12.Size = New System.Drawing.Size(1408, 430)
        Me.LayoutControl12.TabIndex = 0
        Me.LayoutControl12.Text = "LayoutControl12"
        '
        'PanelControl11
        '
        Me.PanelControl11.Controls.Add(Me.GroupControl10)
        Me.PanelControl11.Controls.Add(Me.grpReceiveActions)
        Me.PanelControl11.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl11.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl11.Name = "PanelControl11"
        Me.PanelControl11.Size = New System.Drawing.Size(234, 406)
        Me.PanelControl11.TabIndex = 5
        '
        'GroupControl10
        '
        Me.GroupControl10.Controls.Add(Me.btnLookupShipment)
        Me.GroupControl10.Controls.Add(Me.txtReceivedBarcode)
        Me.GroupControl10.Controls.Add(Me.lblReceivedBarcode)
        Me.GroupControl10.Location = New System.Drawing.Point(6, 6)
        Me.GroupControl10.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(233, 123)
        Me.GroupControl10.TabIndex = 5
        Me.GroupControl10.Text = "Lookup Shipment"
        '
        'btnLookupShipment
        '
        Me.btnLookupShipment.Location = New System.Drawing.Point(30, 87)
        Me.btnLookupShipment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnLookupShipment.Name = "btnLookupShipment"
        Me.btnLookupShipment.Size = New System.Drawing.Size(171, 30)
        Me.btnLookupShipment.TabIndex = 5
        Me.btnLookupShipment.Text = "Lookup"
        '
        'txtReceivedBarcode
        '
        Me.txtReceivedBarcode.Location = New System.Drawing.Point(30, 58)
        Me.txtReceivedBarcode.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtReceivedBarcode.Name = "txtReceivedBarcode"
        Me.txtReceivedBarcode.Size = New System.Drawing.Size(171, 22)
        Me.txtReceivedBarcode.TabIndex = 3
        '
        'lblReceivedBarcode
        '
        Me.lblReceivedBarcode.Location = New System.Drawing.Point(30, 36)
        Me.lblReceivedBarcode.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblReceivedBarcode.Name = "lblReceivedBarcode"
        Me.lblReceivedBarcode.Size = New System.Drawing.Size(101, 16)
        Me.lblReceivedBarcode.TabIndex = 4
        Me.lblReceivedBarcode.Text = "Received Barcode"
        '
        'grpReceiveActions
        '
        Me.grpReceiveActions.Controls.Add(Me.btnReceiveComment)
        Me.grpReceiveActions.Controls.Add(Me.btnRepackageItems)
        Me.grpReceiveActions.Controls.Add(Me.btnCancelRecieve)
        Me.grpReceiveActions.Controls.Add(Me.btnReceiveItems)
        Me.grpReceiveActions.Location = New System.Drawing.Point(6, 137)
        Me.grpReceiveActions.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpReceiveActions.Name = "grpReceiveActions"
        Me.grpReceiveActions.Size = New System.Drawing.Size(233, 182)
        Me.grpReceiveActions.TabIndex = 22
        Me.grpReceiveActions.Text = "Item Actions"
        '
        'btnReceiveComment
        '
        Me.btnReceiveComment.Location = New System.Drawing.Point(30, 69)
        Me.btnReceiveComment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnReceiveComment.Name = "btnReceiveComment"
        Me.btnReceiveComment.Size = New System.Drawing.Size(171, 31)
        Me.btnReceiveComment.TabIndex = 44
        Me.btnReceiveComment.Text = "Receive and Comment"
        '
        'btnRepackageItems
        '
        Me.btnRepackageItems.Location = New System.Drawing.Point(30, 105)
        Me.btnRepackageItems.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnRepackageItems.Name = "btnRepackageItems"
        Me.btnRepackageItems.Size = New System.Drawing.Size(171, 31)
        Me.btnRepackageItems.TabIndex = 43
        Me.btnRepackageItems.Text = "Repackage"
        '
        'btnCancelRecieve
        '
        Me.btnCancelRecieve.Location = New System.Drawing.Point(30, 143)
        Me.btnCancelRecieve.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnCancelRecieve.Name = "btnCancelRecieve"
        Me.btnCancelRecieve.Size = New System.Drawing.Size(171, 31)
        Me.btnCancelRecieve.TabIndex = 42
        Me.btnCancelRecieve.Text = "Cancel"
        '
        'btnReceiveItems
        '
        Me.btnReceiveItems.Location = New System.Drawing.Point(30, 31)
        Me.btnReceiveItems.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnReceiveItems.Name = "btnReceiveItems"
        Me.btnReceiveItems.Size = New System.Drawing.Size(171, 31)
        Me.btnReceiveItems.TabIndex = 41
        Me.btnReceiveItems.Text = "Receive"
        '
        'PanelControl10
        '
        Me.PanelControl10.Controls.Add(Me.GroupControl11)
        Me.PanelControl10.Location = New System.Drawing.Point(255, 12)
        Me.PanelControl10.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl10.Name = "PanelControl10"
        Me.PanelControl10.Size = New System.Drawing.Size(1141, 406)
        Me.PanelControl10.TabIndex = 4
        '
        'GroupControl11
        '
        Me.GroupControl11.Controls.Add(Me.grdRcvPackageView)
        Me.GroupControl11.Controls.Add(Me.grdReceiveShipment)
        Me.GroupControl11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl11.Location = New System.Drawing.Point(2, 2)
        Me.GroupControl11.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupControl11.Name = "GroupControl11"
        Me.GroupControl11.Size = New System.Drawing.Size(1137, 402)
        Me.GroupControl11.TabIndex = 6
        Me.GroupControl11.Text = "Scanned Shipment/Package Details"
        '
        'LayoutControlGroup13
        '
        Me.LayoutControlGroup13.CustomizationFormText = "Root"
        Me.LayoutControlGroup13.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup13.GroupBordersVisible = False
        Me.LayoutControlGroup13.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem25, Me.LayoutControlItem26, Me.SplitterItem14})
        Me.LayoutControlGroup13.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup13.Name = "Root"
        Me.LayoutControlGroup13.Size = New System.Drawing.Size(1408, 430)
        Me.LayoutControlGroup13.Text = "Root"
        Me.LayoutControlGroup13.TextVisible = False
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.PanelControl10
        Me.LayoutControlItem25.CustomizationFormText = "LayoutControlItem25"
        Me.LayoutControlItem25.Location = New System.Drawing.Point(243, 0)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(1145, 410)
        Me.LayoutControlItem25.Text = "LayoutControlItem25"
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextToControlDistance = 0
        Me.LayoutControlItem25.TextVisible = False
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.PanelControl11
        Me.LayoutControlItem26.CustomizationFormText = "LayoutControlItem26"
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(238, 410)
        Me.LayoutControlItem26.Text = "LayoutControlItem26"
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem26.TextToControlDistance = 0
        Me.LayoutControlItem26.TextVisible = False
        '
        'SplitterItem14
        '
        Me.SplitterItem14.AllowHotTrack = True
        Me.SplitterItem14.CustomizationFormText = "SplitterItem14"
        Me.SplitterItem14.Location = New System.Drawing.Point(238, 0)
        Me.SplitterItem14.Name = "SplitterItem14"
        Me.SplitterItem14.Size = New System.Drawing.Size(5, 410)
        '
        'GroupControl13
        '
        Me.GroupControl13.Controls.Add(Me.grdAnticipatedShipments)
        Me.GroupControl13.Location = New System.Drawing.Point(12, 455)
        Me.GroupControl13.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupControl13.Name = "GroupControl13"
        Me.GroupControl13.Size = New System.Drawing.Size(1412, 170)
        Me.GroupControl13.TabIndex = 18
        Me.GroupControl13.Text = "Anticipated Shipments"
        '
        'LayoutControlGroup12
        '
        Me.LayoutControlGroup12.CustomizationFormText = "Root"
        Me.LayoutControlGroup12.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup12.GroupBordersVisible = False
        Me.LayoutControlGroup12.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem23, Me.LayoutControlItem24, Me.SplitterItem13})
        Me.LayoutControlGroup12.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup12.Name = "Root"
        Me.LayoutControlGroup12.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControlGroup12.Text = "Root"
        Me.LayoutControlGroup12.TextVisible = False
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.GroupControl13
        Me.LayoutControlItem23.CustomizationFormText = "LayoutControlItem23"
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 443)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(1416, 174)
        Me.LayoutControlItem23.Text = "LayoutControlItem23"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextToControlDistance = 0
        Me.LayoutControlItem23.TextVisible = False
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.PanelControl9
        Me.LayoutControlItem24.CustomizationFormText = "LayoutControlItem24"
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(1416, 438)
        Me.LayoutControlItem24.Text = "LayoutControlItem24"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem24.TextToControlDistance = 0
        Me.LayoutControlItem24.TextVisible = False
        '
        'SplitterItem13
        '
        Me.SplitterItem13.AllowHotTrack = True
        Me.SplitterItem13.CustomizationFormText = "SplitterItem13"
        Me.SplitterItem13.Location = New System.Drawing.Point(0, 438)
        Me.SplitterItem13.Name = "SplitterItem13"
        Me.SplitterItem13.Size = New System.Drawing.Size(1416, 5)
        '
        'TabSettings
        '
        Me.TabSettings.Controls.Add(Me.btnScannerHelp)
        Me.TabSettings.Controls.Add(Me.btnTestScan)
        Me.TabSettings.Controls.Add(Me.cboScanner)
        Me.TabSettings.Controls.Add(Me.LabelControl14)
        Me.TabSettings.Controls.Add(Me.cboPrinterDPI)
        Me.TabSettings.Controls.Add(Me.LabelControl10)
        Me.TabSettings.Controls.Add(Me.GroupControl18)
        Me.TabSettings.Controls.Add(Me.lkFulfillmentSite)
        Me.TabSettings.Controls.Add(Me.lblFulfillmentSite)
        Me.TabSettings.Controls.Add(Me.cboEnvironment)
        Me.TabSettings.Controls.Add(Me.lblEnvironment)
        Me.TabSettings.Controls.Add(Me.btnBrowseShipmentTemplate)
        Me.TabSettings.Controls.Add(Me.txtShipmentTemplate)
        Me.TabSettings.Controls.Add(Me.LabelControl2)
        Me.TabSettings.Controls.Add(Me.btnBrowseLabelTemplate)
        Me.TabSettings.Controls.Add(Me.txtLabelTemplatePath)
        Me.TabSettings.Controls.Add(Me.lblLabelTemplatePath)
        Me.TabSettings.Controls.Add(Me.btnSaveSettings)
        Me.TabSettings.Controls.Add(Me.lblPrinterCOMport)
        Me.TabSettings.Controls.Add(Me.txtPrinterPort)
        Me.TabSettings.Controls.Add(Me.cboPrinter)
        Me.TabSettings.Controls.Add(Me.lblPrinter)
        Me.TabSettings.Controls.Add(Me.LabelControl1)
        Me.TabSettings.Controls.Add(Me.txtCostCenter)
        Me.TabSettings.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TabSettings.Name = "TabSettings"
        Me.TabSettings.PageVisible = False
        Me.TabSettings.Size = New System.Drawing.Size(1436, 637)
        Me.TabSettings.Text = "Settings"
        '
        'btnScannerHelp
        '
        Me.btnScannerHelp.Location = New System.Drawing.Point(601, 219)
        Me.btnScannerHelp.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnScannerHelp.Name = "btnScannerHelp"
        Me.btnScannerHelp.Size = New System.Drawing.Size(173, 31)
        Me.btnScannerHelp.TabIndex = 38
        Me.btnScannerHelp.Text = "Scanner Help"
        '
        'btnTestScan
        '
        Me.btnTestScan.Location = New System.Drawing.Point(412, 220)
        Me.btnTestScan.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnTestScan.Name = "btnTestScan"
        Me.btnTestScan.Size = New System.Drawing.Size(173, 31)
        Me.btnTestScan.TabIndex = 37
        Me.btnTestScan.Text = "Configure / Test Scanner"
        '
        'cboScanner
        '
        Me.cboScanner.EditValue = "WIA_SINGLE"
        Me.cboScanner.Location = New System.Drawing.Point(204, 228)
        Me.cboScanner.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboScanner.Name = "cboScanner"
        Me.cboScanner.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboScanner.Properties.Items.AddRange(New Object() {"WIA_SINGLE", "Simulator"})
        Me.cboScanner.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cboScanner.Size = New System.Drawing.Size(171, 22)
        Me.cboScanner.TabIndex = 36
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(33, 231)
        Me.LabelControl14.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(85, 16)
        Me.LabelControl14.TabIndex = 35
        Me.LabelControl14.Text = "Scanner Driver"
        '
        'cboPrinterDPI
        '
        Me.cboPrinterDPI.Location = New System.Drawing.Point(204, 103)
        Me.cboPrinterDPI.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboPrinterDPI.Name = "cboPrinterDPI"
        Me.cboPrinterDPI.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboPrinterDPI.Properties.Items.AddRange(New Object() {"600", "300"})
        Me.cboPrinterDPI.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cboPrinterDPI.Size = New System.Drawing.Size(171, 22)
        Me.cboPrinterDPI.TabIndex = 34
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(33, 107)
        Me.LabelControl10.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(61, 16)
        Me.LabelControl10.TabIndex = 33
        Me.LabelControl10.Text = "Printer DPI"
        '
        'GroupControl18
        '
        Me.GroupControl18.Controls.Add(Me.LabelControl8)
        Me.GroupControl18.Controls.Add(Me.txtQuickInternational)
        Me.GroupControl18.Controls.Add(Me.LabelControl6)
        Me.GroupControl18.Controls.Add(Me.txtFedExAccount)
        Me.GroupControl18.Location = New System.Drawing.Point(33, 330)
        Me.GroupControl18.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupControl18.Name = "GroupControl18"
        Me.GroupControl18.Size = New System.Drawing.Size(416, 151)
        Me.GroupControl18.TabIndex = 32
        Me.GroupControl18.Text = "Accounts"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(7, 86)
        Me.LabelControl8.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(107, 16)
        Me.LabelControl8.TabIndex = 20
        Me.LabelControl8.Text = "Quick International"
        '
        'txtQuickInternational
        '
        Me.txtQuickInternational.Location = New System.Drawing.Point(178, 82)
        Me.txtQuickInternational.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtQuickInternational.Name = "txtQuickInternational"
        Me.txtQuickInternational.Size = New System.Drawing.Size(171, 22)
        Me.txtQuickInternational.TabIndex = 19
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(6, 43)
        Me.LabelControl6.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(38, 16)
        Me.LabelControl6.TabIndex = 18
        Me.LabelControl6.Text = "Fed Ex"
        '
        'txtFedExAccount
        '
        Me.txtFedExAccount.Location = New System.Drawing.Point(177, 39)
        Me.txtFedExAccount.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtFedExAccount.Name = "txtFedExAccount"
        Me.txtFedExAccount.Size = New System.Drawing.Size(171, 22)
        Me.txtFedExAccount.TabIndex = 17
        '
        'lkFulfillmentSite
        '
        Me.lkFulfillmentSite.EditValue = ""
        Me.lkFulfillmentSite.Location = New System.Drawing.Point(204, 182)
        Me.lkFulfillmentSite.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lkFulfillmentSite.Name = "lkFulfillmentSite"
        Me.lkFulfillmentSite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lkFulfillmentSite.Properties.NullText = ""
        Me.lkFulfillmentSite.Properties.ShowFooter = False
        Me.lkFulfillmentSite.Properties.ShowHeader = False
        Me.lkFulfillmentSite.Properties.ShowLines = False
        Me.lkFulfillmentSite.Properties.ShowPopupShadow = False
        Me.lkFulfillmentSite.Size = New System.Drawing.Size(180, 22)
        Me.lkFulfillmentSite.TabIndex = 31
        '
        'lblFulfillmentSite
        '
        Me.lblFulfillmentSite.Location = New System.Drawing.Point(33, 185)
        Me.lblFulfillmentSite.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblFulfillmentSite.Name = "lblFulfillmentSite"
        Me.lblFulfillmentSite.Size = New System.Drawing.Size(85, 16)
        Me.lblFulfillmentSite.TabIndex = 27
        Me.lblFulfillmentSite.Text = "Fulfillment Site"
        '
        'cboEnvironment
        '
        Me.cboEnvironment.Location = New System.Drawing.Point(819, 585)
        Me.cboEnvironment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboEnvironment.Name = "cboEnvironment"
        Me.cboEnvironment.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEnvironment.Properties.Items.AddRange(New Object() {"PROD", "INT", "TEST"})
        Me.cboEnvironment.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cboEnvironment.Size = New System.Drawing.Size(171, 22)
        Me.cboEnvironment.TabIndex = 25
        Me.cboEnvironment.Visible = False
        '
        'lblEnvironment
        '
        Me.lblEnvironment.Location = New System.Drawing.Point(647, 588)
        Me.lblEnvironment.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblEnvironment.Name = "lblEnvironment"
        Me.lblEnvironment.Size = New System.Drawing.Size(71, 16)
        Me.lblEnvironment.TabIndex = 24
        Me.lblEnvironment.Text = "Environment"
        Me.lblEnvironment.Visible = False
        '
        'btnBrowseShipmentTemplate
        '
        Me.btnBrowseShipmentTemplate.Location = New System.Drawing.Point(1200, 527)
        Me.btnBrowseShipmentTemplate.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnBrowseShipmentTemplate.Name = "btnBrowseShipmentTemplate"
        Me.btnBrowseShipmentTemplate.Size = New System.Drawing.Size(121, 25)
        Me.btnBrowseShipmentTemplate.TabIndex = 23
        Me.btnBrowseShipmentTemplate.Text = "Browse..."
        Me.btnBrowseShipmentTemplate.Visible = False
        '
        'txtShipmentTemplate
        '
        Me.txtShipmentTemplate.Location = New System.Drawing.Point(818, 527)
        Me.txtShipmentTemplate.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtShipmentTemplate.Name = "txtShipmentTemplate"
        Me.txtShipmentTemplate.Size = New System.Drawing.Size(376, 22)
        Me.txtShipmentTemplate.TabIndex = 22
        Me.txtShipmentTemplate.Visible = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(646, 530)
        Me.LabelControl2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(175, 16)
        Me.LabelControl2.TabIndex = 21
        Me.LabelControl2.Text = "Shipment Form Template Path"
        Me.LabelControl2.Visible = False
        '
        'btnBrowseLabelTemplate
        '
        Me.btnBrowseLabelTemplate.Location = New System.Drawing.Point(1202, 468)
        Me.btnBrowseLabelTemplate.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnBrowseLabelTemplate.Name = "btnBrowseLabelTemplate"
        Me.btnBrowseLabelTemplate.Size = New System.Drawing.Size(121, 25)
        Me.btnBrowseLabelTemplate.TabIndex = 20
        Me.btnBrowseLabelTemplate.Text = "Browse..."
        Me.btnBrowseLabelTemplate.Visible = False
        '
        'txtLabelTemplatePath
        '
        Me.txtLabelTemplatePath.Location = New System.Drawing.Point(819, 468)
        Me.txtLabelTemplatePath.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtLabelTemplatePath.Name = "txtLabelTemplatePath"
        Me.txtLabelTemplatePath.Size = New System.Drawing.Size(376, 22)
        Me.txtLabelTemplatePath.TabIndex = 19
        Me.txtLabelTemplatePath.Visible = False
        '
        'lblLabelTemplatePath
        '
        Me.lblLabelTemplatePath.Location = New System.Drawing.Point(647, 471)
        Me.lblLabelTemplatePath.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblLabelTemplatePath.Name = "lblLabelTemplatePath"
        Me.lblLabelTemplatePath.Size = New System.Drawing.Size(117, 16)
        Me.lblLabelTemplatePath.TabIndex = 18
        Me.lblLabelTemplatePath.Text = "Label Template Path"
        Me.lblLabelTemplatePath.Visible = False
        '
        'btnSaveSettings
        '
        Me.btnSaveSettings.Location = New System.Drawing.Point(120, 500)
        Me.btnSaveSettings.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSaveSettings.Name = "btnSaveSettings"
        Me.btnSaveSettings.Size = New System.Drawing.Size(173, 31)
        Me.btnSaveSettings.TabIndex = 17
        Me.btnSaveSettings.Text = "Save Settings"
        '
        'lblPrinterCOMport
        '
        Me.lblPrinterCOMport.Location = New System.Drawing.Point(33, 142)
        Me.lblPrinterCOMport.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblPrinterCOMport.Name = "lblPrinterCOMport"
        Me.lblPrinterCOMport.Size = New System.Drawing.Size(149, 16)
        Me.lblPrinterCOMport.TabIndex = 16
        Me.lblPrinterCOMport.Text = "Printer COM Port Number "
        '
        'txtPrinterPort
        '
        Me.txtPrinterPort.Location = New System.Drawing.Point(204, 138)
        Me.txtPrinterPort.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPrinterPort.Name = "txtPrinterPort"
        Me.txtPrinterPort.Size = New System.Drawing.Size(171, 22)
        Me.txtPrinterPort.TabIndex = 15
        '
        'cboPrinter
        '
        Me.cboPrinter.Location = New System.Drawing.Point(204, 71)
        Me.cboPrinter.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboPrinter.Name = "cboPrinter"
        Me.cboPrinter.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboPrinter.Properties.Items.AddRange(New Object() {"Brother", "Zebra", "Other"})
        Me.cboPrinter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cboPrinter.Size = New System.Drawing.Size(171, 22)
        Me.cboPrinter.TabIndex = 14
        '
        'lblPrinter
        '
        Me.lblPrinter.Location = New System.Drawing.Point(33, 75)
        Me.lblPrinter.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblPrinter.Name = "lblPrinter"
        Me.lblPrinter.Size = New System.Drawing.Size(38, 16)
        Me.lblPrinter.TabIndex = 13
        Me.lblPrinter.Text = "Printer"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(33, 38)
        Me.LabelControl1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(164, 16)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Override Default Cost Center"
        '
        'txtCostCenter
        '
        Me.txtCostCenter.Location = New System.Drawing.Point(204, 34)
        Me.txtCostCenter.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtCostCenter.Name = "txtCostCenter"
        Me.txtCostCenter.Size = New System.Drawing.Size(171, 22)
        Me.txtCostCenter.TabIndex = 1
        '
        'TabSampleTransfer
        '
        Me.TabSampleTransfer.Controls.Add(Me.LayoutControl13)
        Me.TabSampleTransfer.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TabSampleTransfer.Name = "TabSampleTransfer"
        Me.TabSampleTransfer.Size = New System.Drawing.Size(1436, 637)
        Me.TabSampleTransfer.Text = "Sample Transfer"
        '
        'LayoutControl13
        '
        Me.LayoutControl13.Controls.Add(Me.PanelControl13)
        Me.LayoutControl13.Controls.Add(Me.PanelControl12)
        Me.LayoutControl13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl13.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl13.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl13.Name = "LayoutControl13"
        Me.LayoutControl13.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(404, 245, 250, 350)
        Me.LayoutControl13.Root = Me.LayoutControlGroup14
        Me.LayoutControl13.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControl13.TabIndex = 1010
        Me.LayoutControl13.Text = "LayoutControl13"
        '
        'PanelControl13
        '
        Me.PanelControl13.Controls.Add(Me.GrpSimpleLogin)
        Me.PanelControl13.Controls.Add(Me.NavBarControl1)
        Me.PanelControl13.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl13.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl13.Name = "PanelControl13"
        Me.PanelControl13.Size = New System.Drawing.Size(240, 613)
        Me.PanelControl13.TabIndex = 5
        '
        'GrpSimpleLogin
        '
        Me.GrpSimpleLogin.Controls.Add(Me.lblSimpleUsername)
        Me.GrpSimpleLogin.Controls.Add(Me.btnSimpleLogin)
        Me.GrpSimpleLogin.Controls.Add(Me.txtSimpleUsername)
        Me.GrpSimpleLogin.Location = New System.Drawing.Point(6, 6)
        Me.GrpSimpleLogin.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GrpSimpleLogin.Name = "GrpSimpleLogin"
        Me.GrpSimpleLogin.Size = New System.Drawing.Size(233, 110)
        Me.GrpSimpleLogin.TabIndex = 0
        Me.GrpSimpleLogin.Text = "Simple Login"
        '
        'lblSimpleUsername
        '
        Me.lblSimpleUsername.Location = New System.Drawing.Point(34, 33)
        Me.lblSimpleUsername.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblSimpleUsername.Name = "lblSimpleUsername"
        Me.lblSimpleUsername.Size = New System.Drawing.Size(58, 16)
        Me.lblSimpleUsername.TabIndex = 1004
        Me.lblSimpleUsername.Text = "Username"
        '
        'btnSimpleLogin
        '
        Me.btnSimpleLogin.Location = New System.Drawing.Point(34, 63)
        Me.btnSimpleLogin.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSimpleLogin.Name = "btnSimpleLogin"
        Me.btnSimpleLogin.Size = New System.Drawing.Size(173, 31)
        Me.btnSimpleLogin.TabIndex = 1006
        Me.btnSimpleLogin.Text = "Login"
        '
        'txtSimpleUsername
        '
        Me.txtSimpleUsername.Location = New System.Drawing.Point(94, 30)
        Me.txtSimpleUsername.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtSimpleUsername.Name = "txtSimpleUsername"
        Me.txtSimpleUsername.Size = New System.Drawing.Size(112, 22)
        Me.txtSimpleUsername.TabIndex = 1005
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.NavSTIFunctions
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavSTIFunctions})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.NavSTISampleDropoff, Me.NavSTIPackageDropoff, Me.NavSTIPackagePickup, Me.NavSTIBinMaintenance, Me.NavSTILog})
        Me.NavBarControl1.Location = New System.Drawing.Point(6, 124)
        Me.NavBarControl1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.OptionsNavPane.ExpandedWidth = 233
        Me.NavBarControl1.Size = New System.Drawing.Size(233, 181)
        Me.NavBarControl1.TabIndex = 1009
        Me.NavBarControl1.Text = "NavBarControl1"
        '
        'NavSTIFunctions
        '
        Me.NavSTIFunctions.Caption = "Sample Transfer Services"
        Me.NavSTIFunctions.Expanded = True
        Me.NavSTIFunctions.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.NavSTISampleDropoff), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavSTIPackageDropoff), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavSTIPackagePickup), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavSTIBinMaintenance), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavSTILog)})
        Me.NavSTIFunctions.Name = "NavSTIFunctions"
        '
        'NavSTISampleDropoff
        '
        Me.NavSTISampleDropoff.Caption = "Sample Dropoff"
        Me.NavSTISampleDropoff.Name = "NavSTISampleDropoff"
        '
        'NavSTIPackageDropoff
        '
        Me.NavSTIPackageDropoff.Caption = "Package Dropoff"
        Me.NavSTIPackageDropoff.Name = "NavSTIPackageDropoff"
        '
        'NavSTIPackagePickup
        '
        Me.NavSTIPackagePickup.Caption = "Package Pickup"
        Me.NavSTIPackagePickup.Name = "NavSTIPackagePickup"
        '
        'NavSTIBinMaintenance
        '
        Me.NavSTIBinMaintenance.Caption = "Bin Maintenance"
        Me.NavSTIBinMaintenance.Name = "NavSTIBinMaintenance"
        '
        'NavSTILog
        '
        Me.NavSTILog.Caption = "Log"
        Me.NavSTILog.Name = "NavSTILog"
        '
        'PanelControl12
        '
        Me.PanelControl12.Controls.Add(Me.tabSTIFunction)
        Me.PanelControl12.Location = New System.Drawing.Point(261, 12)
        Me.PanelControl12.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.PanelControl12.Name = "PanelControl12"
        Me.PanelControl12.Size = New System.Drawing.Size(1163, 613)
        Me.PanelControl12.TabIndex = 4
        '
        'tabSTIFunction
        '
        Me.tabSTIFunction.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabSTIFunction.Location = New System.Drawing.Point(2, 2)
        Me.tabSTIFunction.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.tabSTIFunction.Name = "tabSTIFunction"
        Me.tabSTIFunction.SelectedTabPage = Me.tabSTISampleDropoff
        Me.tabSTIFunction.Size = New System.Drawing.Size(1159, 609)
        Me.tabSTIFunction.TabIndex = 1008
        Me.tabSTIFunction.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabSTISampleDropoff, Me.tabSTIPackageDropoff, Me.tabSTIPackagePickup, Me.tabSTIBinMaintenance, Me.tabSTILog})
        '
        'tabSTISampleDropoff
        '
        Me.tabSTISampleDropoff.Controls.Add(Me.LayoutControl14)
        Me.tabSTISampleDropoff.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.tabSTISampleDropoff.Name = "tabSTISampleDropoff"
        Me.tabSTISampleDropoff.Size = New System.Drawing.Size(1153, 578)
        Me.tabSTISampleDropoff.Text = "Sample Dropoff"
        '
        'LayoutControl14
        '
        Me.LayoutControl14.Controls.Add(Me.LayoutControl15)
        Me.LayoutControl14.Controls.Add(Me.txtSampleDropMessage)
        Me.LayoutControl14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl14.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl14.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl14.Name = "LayoutControl14"
        Me.LayoutControl14.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(939, 216, 250, 350)
        Me.LayoutControl14.Root = Me.LayoutControlGroup15
        Me.LayoutControl14.Size = New System.Drawing.Size(1153, 578)
        Me.LayoutControl14.TabIndex = 1016
        Me.LayoutControl14.Text = "LayoutControl14"
        '
        'LayoutControl15
        '
        Me.LayoutControl15.Controls.Add(Me.GroupControl16)
        Me.LayoutControl15.Controls.Add(Me.GroupControl17)
        Me.LayoutControl15.Location = New System.Drawing.Point(12, 91)
        Me.LayoutControl15.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl15.Name = "LayoutControl15"
        Me.LayoutControl15.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(666, 486, 250, 350)
        Me.LayoutControl15.Root = Me.Root
        Me.LayoutControl15.Size = New System.Drawing.Size(1129, 475)
        Me.LayoutControl15.TabIndex = 1016
        Me.LayoutControl15.Text = "LayoutControl15"
        '
        'GroupControl16
        '
        Me.GroupControl16.Controls.Add(Me.btnSTISampleSubmit)
        Me.GroupControl16.Controls.Add(Me.LabelControl7)
        Me.GroupControl16.Controls.Add(Me.txtSTISampleSubmission)
        Me.GroupControl16.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl16.Name = "GroupControl16"
        Me.GroupControl16.Size = New System.Drawing.Size(324, 451)
        Me.GroupControl16.TabIndex = 1013
        Me.GroupControl16.Text = "Sample Dropoff"
        '
        'btnSTISampleSubmit
        '
        Me.btnSTISampleSubmit.Location = New System.Drawing.Point(68, 59)
        Me.btnSTISampleSubmit.Name = "btnSTISampleSubmit"
        Me.btnSTISampleSubmit.Size = New System.Drawing.Size(145, 23)
        Me.btnSTISampleSubmit.TabIndex = 1010
        Me.btnSTISampleSubmit.Text = "Sample Submit"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(18, 36)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(93, 16)
        Me.LabelControl7.TabIndex = 1012
        Me.LabelControl7.Text = "Sample Barcode"
        '
        'txtSTISampleSubmission
        '
        Me.txtSTISampleSubmission.Location = New System.Drawing.Point(113, 33)
        Me.txtSTISampleSubmission.MenuManager = Me.RibbonControl
        Me.txtSTISampleSubmission.Name = "txtSTISampleSubmission"
        Me.txtSTISampleSubmission.Size = New System.Drawing.Size(152, 22)
        Me.txtSTISampleSubmission.TabIndex = 1011
        '
        'GroupControl17
        '
        Me.GroupControl17.Controls.Add(Me.ListBoxControl1)
        Me.GroupControl17.Location = New System.Drawing.Point(345, 12)
        Me.GroupControl17.Name = "GroupControl17"
        Me.GroupControl17.Size = New System.Drawing.Size(772, 451)
        Me.GroupControl17.TabIndex = 1014
        Me.GroupControl17.Text = "Samples Added"
        '
        'ListBoxControl1
        '
        Me.ListBoxControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListBoxControl1.Location = New System.Drawing.Point(2, 24)
        Me.ListBoxControl1.Name = "ListBoxControl1"
        Me.ListBoxControl1.Size = New System.Drawing.Size(768, 425)
        Me.ListBoxControl1.TabIndex = 1008
        '
        'Root
        '
        Me.Root.CustomizationFormText = "Root"
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem31, Me.LayoutControlItem32, Me.SplitterItem17})
        Me.Root.Location = New System.Drawing.Point(0, 0)
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(1129, 475)
        Me.Root.Text = "Root"
        Me.Root.TextVisible = False
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.GroupControl16
        Me.LayoutControlItem31.CustomizationFormText = "LayoutControlItem31"
        Me.LayoutControlItem31.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(328, 455)
        Me.LayoutControlItem31.Text = "LayoutControlItem31"
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem31.TextToControlDistance = 0
        Me.LayoutControlItem31.TextVisible = False
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.GroupControl17
        Me.LayoutControlItem32.CustomizationFormText = "LayoutControlItem32"
        Me.LayoutControlItem32.Location = New System.Drawing.Point(333, 0)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(776, 455)
        Me.LayoutControlItem32.Text = "LayoutControlItem32"
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem32.TextToControlDistance = 0
        Me.LayoutControlItem32.TextVisible = False
        '
        'SplitterItem17
        '
        Me.SplitterItem17.AllowHotTrack = True
        Me.SplitterItem17.CustomizationFormText = "SplitterItem17"
        Me.SplitterItem17.Location = New System.Drawing.Point(328, 0)
        Me.SplitterItem17.Name = "SplitterItem17"
        Me.SplitterItem17.Size = New System.Drawing.Size(5, 455)
        '
        'txtSampleDropMessage
        '
        Me.txtSampleDropMessage.Location = New System.Drawing.Point(12, 12)
        Me.txtSampleDropMessage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtSampleDropMessage.MenuManager = Me.RibbonControl
        Me.txtSampleDropMessage.Name = "txtSampleDropMessage"
        Me.txtSampleDropMessage.Properties.ReadOnly = True
        Me.txtSampleDropMessage.Size = New System.Drawing.Size(1129, 70)
        Me.txtSampleDropMessage.StyleController = Me.LayoutControl14
        Me.txtSampleDropMessage.TabIndex = 1015
        '
        'LayoutControlGroup15
        '
        Me.LayoutControlGroup15.CustomizationFormText = "Root"
        Me.LayoutControlGroup15.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup15.GroupBordersVisible = False
        Me.LayoutControlGroup15.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem29, Me.LayoutControlItem30, Me.SplitterItem16})
        Me.LayoutControlGroup15.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup15.Name = "Root"
        Me.LayoutControlGroup15.Size = New System.Drawing.Size(1153, 578)
        Me.LayoutControlGroup15.Text = "Root"
        Me.LayoutControlGroup15.TextVisible = False
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.txtSampleDropMessage
        Me.LayoutControlItem29.CustomizationFormText = "LayoutControlItem29"
        Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(1133, 74)
        Me.LayoutControlItem29.Text = "LayoutControlItem29"
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem29.TextToControlDistance = 0
        Me.LayoutControlItem29.TextVisible = False
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.LayoutControl15
        Me.LayoutControlItem30.CustomizationFormText = "LayoutControlItem30"
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 79)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(1133, 479)
        Me.LayoutControlItem30.Text = "LayoutControlItem30"
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem30.TextToControlDistance = 0
        Me.LayoutControlItem30.TextVisible = False
        '
        'SplitterItem16
        '
        Me.SplitterItem16.AllowHotTrack = True
        Me.SplitterItem16.CustomizationFormText = "SplitterItem16"
        Me.SplitterItem16.Location = New System.Drawing.Point(0, 74)
        Me.SplitterItem16.Name = "SplitterItem16"
        Me.SplitterItem16.Size = New System.Drawing.Size(1133, 5)
        '
        'tabSTIPackageDropoff
        '
        Me.tabSTIPackageDropoff.Controls.Add(Me.LayoutControl16)
        Me.tabSTIPackageDropoff.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.tabSTIPackageDropoff.Name = "tabSTIPackageDropoff"
        Me.tabSTIPackageDropoff.Size = New System.Drawing.Size(1153, 578)
        Me.tabSTIPackageDropoff.Text = "Package Dropoff"
        '
        'LayoutControl16
        '
        Me.LayoutControl16.Controls.Add(Me.LayoutControl17)
        Me.LayoutControl16.Controls.Add(Me.txtPckDropMessage)
        Me.LayoutControl16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl16.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl16.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl16.Name = "LayoutControl16"
        Me.LayoutControl16.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(654, 224, 250, 350)
        Me.LayoutControl16.Root = Me.LayoutControlGroup16
        Me.LayoutControl16.Size = New System.Drawing.Size(1153, 578)
        Me.LayoutControl16.TabIndex = 5
        Me.LayoutControl16.Text = "LayoutControl16"
        '
        'LayoutControl17
        '
        Me.LayoutControl17.Controls.Add(Me.PanelControl15)
        Me.LayoutControl17.Controls.Add(Me.PanelControl14)
        Me.LayoutControl17.Location = New System.Drawing.Point(12, 93)
        Me.LayoutControl17.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl17.Name = "LayoutControl17"
        Me.LayoutControl17.Root = Me.LayoutControlGroup17
        Me.LayoutControl17.Size = New System.Drawing.Size(1129, 473)
        Me.LayoutControl17.TabIndex = 5
        Me.LayoutControl17.Text = "LayoutControl17"
        '
        'PanelControl15
        '
        Me.PanelControl15.Controls.Add(Me.GridPackageDropoff)
        Me.PanelControl15.Location = New System.Drawing.Point(338, 12)
        Me.PanelControl15.Name = "PanelControl15"
        Me.PanelControl15.Size = New System.Drawing.Size(779, 449)
        Me.PanelControl15.TabIndex = 5
        '
        'GridPackageDropoff
        '
        Me.GridPackageDropoff.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridPackageDropoff.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GridPackageDropoff.Location = New System.Drawing.Point(2, 2)
        Me.GridPackageDropoff.MainView = Me.grdViewPackageDropoff
        Me.GridPackageDropoff.MenuManager = Me.RibbonControl
        Me.GridPackageDropoff.Name = "GridPackageDropoff"
        Me.GridPackageDropoff.Size = New System.Drawing.Size(775, 445)
        Me.GridPackageDropoff.TabIndex = 3
        Me.GridPackageDropoff.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grdViewPackageDropoff})
        '
        'grdViewPackageDropoff
        '
        Me.grdViewPackageDropoff.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn29, Me.GridColumn30, Me.GridColumn31, Me.GridColumn32, Me.GridColumn33, Me.GridColumn34, Me.GridColumn41, Me.GridColumn35})
        Me.grdViewPackageDropoff.GridControl = Me.GridPackageDropoff
        Me.grdViewPackageDropoff.Name = "grdViewPackageDropoff"
        Me.grdViewPackageDropoff.OptionsView.ShowGroupPanel = False
        '
        'GridColumn29
        '
        Me.GridColumn29.Caption = "Bin Name"
        Me.GridColumn29.FieldName = "BIN_NAME"
        Me.GridColumn29.Name = "GridColumn29"
        Me.GridColumn29.Visible = True
        Me.GridColumn29.VisibleIndex = 0
        '
        'GridColumn30
        '
        Me.GridColumn30.Caption = "Workstation"
        Me.GridColumn30.FieldName = "WORKSTATION"
        Me.GridColumn30.Name = "GridColumn30"
        '
        'GridColumn31
        '
        Me.GridColumn31.Caption = "Mail Address"
        Me.GridColumn31.FieldName = "MAIL_ADDRESS"
        Me.GridColumn31.Name = "GridColumn31"
        '
        'GridColumn32
        '
        Me.GridColumn32.Caption = "Site Code"
        Me.GridColumn32.FieldName = "SITE_CODE"
        Me.GridColumn32.Name = "GridColumn32"
        '
        'GridColumn33
        '
        Me.GridColumn33.Caption = "Site"
        Me.GridColumn33.FieldName = "SITE"
        Me.GridColumn33.Name = "GridColumn33"
        '
        'GridColumn34
        '
        Me.GridColumn34.Caption = "Package Count"
        Me.GridColumn34.FieldName = "PACKAGE_COUNT"
        Me.GridColumn34.Name = "GridColumn34"
        Me.GridColumn34.Visible = True
        Me.GridColumn34.VisibleIndex = 1
        '
        'GridColumn41
        '
        Me.GridColumn41.Caption = "Last Name"
        Me.GridColumn41.FieldName = "LAST_NAME"
        Me.GridColumn41.Name = "GridColumn41"
        Me.GridColumn41.Visible = True
        Me.GridColumn41.VisibleIndex = 3
        '
        'GridColumn35
        '
        Me.GridColumn35.Caption = "First Name"
        Me.GridColumn35.FieldName = "FIRST_NAME"
        Me.GridColumn35.Name = "GridColumn35"
        Me.GridColumn35.Visible = True
        Me.GridColumn35.VisibleIndex = 2
        '
        'PanelControl14
        '
        Me.PanelControl14.Controls.Add(Me.btnTrackingURL)
        Me.PanelControl14.Controls.Add(Me.btnArrivedNoPhase)
        Me.PanelControl14.Controls.Add(Me.btnArrived)
        Me.PanelControl14.Controls.Add(Me.btnReadyForPickup)
        Me.PanelControl14.Controls.Add(Me.grpPackageDropoff)
        Me.PanelControl14.Controls.Add(Me.grpBinDropoff)
        Me.PanelControl14.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl14.Name = "PanelControl14"
        Me.PanelControl14.Size = New System.Drawing.Size(322, 449)
        Me.PanelControl14.TabIndex = 4
        '
        'btnTrackingURL
        '
        Me.btnTrackingURL.Location = New System.Drawing.Point(63, 398)
        Me.btnTrackingURL.Name = "btnTrackingURL"
        Me.btnTrackingURL.Size = New System.Drawing.Size(148, 25)
        Me.btnTrackingURL.TabIndex = 1010
        Me.btnTrackingURL.Text = "Tracking URL"
        Me.btnTrackingURL.Visible = False
        '
        'btnArrivedNoPhase
        '
        Me.btnArrivedNoPhase.Location = New System.Drawing.Point(63, 357)
        Me.btnArrivedNoPhase.Name = "btnArrivedNoPhase"
        Me.btnArrivedNoPhase.Size = New System.Drawing.Size(148, 25)
        Me.btnArrivedNoPhase.TabIndex = 1009
        Me.btnArrivedNoPhase.Text = "Arrived No Phase"
        Me.btnArrivedNoPhase.Visible = False
        '
        'btnArrived
        '
        Me.btnArrived.Location = New System.Drawing.Point(63, 326)
        Me.btnArrived.Name = "btnArrived"
        Me.btnArrived.Size = New System.Drawing.Size(148, 25)
        Me.btnArrived.TabIndex = 1008
        Me.btnArrived.Text = "Arrived"
        Me.btnArrived.Visible = False
        '
        'btnReadyForPickup
        '
        Me.btnReadyForPickup.Location = New System.Drawing.Point(63, 295)
        Me.btnReadyForPickup.Name = "btnReadyForPickup"
        Me.btnReadyForPickup.Size = New System.Drawing.Size(148, 25)
        Me.btnReadyForPickup.TabIndex = 1007
        Me.btnReadyForPickup.Text = "Ready For Pickup"
        Me.btnReadyForPickup.Visible = False
        '
        'grpPackageDropoff
        '
        Me.grpPackageDropoff.Controls.Add(Me.lblPackageDropoff)
        Me.grpPackageDropoff.Controls.Add(Me.btnPackageDropoff)
        Me.grpPackageDropoff.Controls.Add(Me.txtPackageDropoff)
        Me.grpPackageDropoff.Location = New System.Drawing.Point(5, 7)
        Me.grpPackageDropoff.Name = "grpPackageDropoff"
        Me.grpPackageDropoff.Size = New System.Drawing.Size(272, 89)
        Me.grpPackageDropoff.TabIndex = 1
        Me.grpPackageDropoff.Text = "Package Dropoff"
        '
        'lblPackageDropoff
        '
        Me.lblPackageDropoff.Location = New System.Drawing.Point(29, 27)
        Me.lblPackageDropoff.Name = "lblPackageDropoff"
        Me.lblPackageDropoff.Size = New System.Drawing.Size(97, 16)
        Me.lblPackageDropoff.TabIndex = 1004
        Me.lblPackageDropoff.Text = "Package Barcode"
        '
        'btnPackageDropoff
        '
        Me.btnPackageDropoff.Location = New System.Drawing.Point(58, 58)
        Me.btnPackageDropoff.Name = "btnPackageDropoff"
        Me.btnPackageDropoff.Size = New System.Drawing.Size(148, 25)
        Me.btnPackageDropoff.TabIndex = 1006
        Me.btnPackageDropoff.Text = "Lookup"
        '
        'txtPackageDropoff
        '
        Me.txtPackageDropoff.Location = New System.Drawing.Point(123, 24)
        Me.txtPackageDropoff.Name = "txtPackageDropoff"
        Me.txtPackageDropoff.Size = New System.Drawing.Size(96, 22)
        Me.txtPackageDropoff.TabIndex = 1005
        '
        'grpBinDropoff
        '
        Me.grpBinDropoff.Controls.Add(Me.lblBinDropoff)
        Me.grpBinDropoff.Controls.Add(Me.btnBinDropoff)
        Me.grpBinDropoff.Controls.Add(Me.txtBinDropoff)
        Me.grpBinDropoff.Location = New System.Drawing.Point(5, 102)
        Me.grpBinDropoff.Name = "grpBinDropoff"
        Me.grpBinDropoff.Size = New System.Drawing.Size(272, 89)
        Me.grpBinDropoff.TabIndex = 2
        Me.grpBinDropoff.Text = "Bin Dropoff"
        '
        'lblBinDropoff
        '
        Me.lblBinDropoff.Location = New System.Drawing.Point(29, 27)
        Me.lblBinDropoff.Name = "lblBinDropoff"
        Me.lblBinDropoff.Size = New System.Drawing.Size(63, 16)
        Me.lblBinDropoff.TabIndex = 1004
        Me.lblBinDropoff.Text = "Bin Dropoff"
        '
        'btnBinDropoff
        '
        Me.btnBinDropoff.Location = New System.Drawing.Point(58, 58)
        Me.btnBinDropoff.Name = "btnBinDropoff"
        Me.btnBinDropoff.Size = New System.Drawing.Size(148, 25)
        Me.btnBinDropoff.TabIndex = 1006
        Me.btnBinDropoff.Text = "Dropoff"
        '
        'txtBinDropoff
        '
        Me.txtBinDropoff.Location = New System.Drawing.Point(123, 24)
        Me.txtBinDropoff.Name = "txtBinDropoff"
        Me.txtBinDropoff.Size = New System.Drawing.Size(96, 22)
        Me.txtBinDropoff.TabIndex = 1005
        '
        'LayoutControlGroup17
        '
        Me.LayoutControlGroup17.CustomizationFormText = "LayoutControlGroup17"
        Me.LayoutControlGroup17.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup17.GroupBordersVisible = False
        Me.LayoutControlGroup17.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem35, Me.LayoutControlItem36})
        Me.LayoutControlGroup17.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup17.Name = "LayoutControlGroup17"
        Me.LayoutControlGroup17.Size = New System.Drawing.Size(1129, 473)
        Me.LayoutControlGroup17.Text = "LayoutControlGroup17"
        Me.LayoutControlGroup17.TextVisible = False
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.PanelControl14
        Me.LayoutControlItem35.CustomizationFormText = "LayoutControlItem35"
        Me.LayoutControlItem35.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(326, 453)
        Me.LayoutControlItem35.Text = "LayoutControlItem35"
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem35.TextToControlDistance = 0
        Me.LayoutControlItem35.TextVisible = False
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.PanelControl15
        Me.LayoutControlItem36.CustomizationFormText = "LayoutControlItem36"
        Me.LayoutControlItem36.Location = New System.Drawing.Point(326, 0)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(783, 453)
        Me.LayoutControlItem36.Text = "LayoutControlItem36"
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem36.TextToControlDistance = 0
        Me.LayoutControlItem36.TextVisible = False
        '
        'txtPckDropMessage
        '
        Me.txtPckDropMessage.Location = New System.Drawing.Point(12, 12)
        Me.txtPckDropMessage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPckDropMessage.MenuManager = Me.RibbonControl
        Me.txtPckDropMessage.Name = "txtPckDropMessage"
        Me.txtPckDropMessage.Properties.ReadOnly = True
        Me.txtPckDropMessage.Size = New System.Drawing.Size(1129, 72)
        Me.txtPckDropMessage.StyleController = Me.LayoutControl16
        Me.txtPckDropMessage.TabIndex = 4
        '
        'LayoutControlGroup16
        '
        Me.LayoutControlGroup16.CustomizationFormText = "Root"
        Me.LayoutControlGroup16.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup16.GroupBordersVisible = False
        Me.LayoutControlGroup16.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem33, Me.LayoutControlItem34, Me.SplitterItem18})
        Me.LayoutControlGroup16.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup16.Name = "Root"
        Me.LayoutControlGroup16.Size = New System.Drawing.Size(1153, 578)
        Me.LayoutControlGroup16.Text = "Root"
        Me.LayoutControlGroup16.TextVisible = False
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.Control = Me.txtPckDropMessage
        Me.LayoutControlItem33.CustomizationFormText = "LayoutControlItem33"
        Me.LayoutControlItem33.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(1133, 76)
        Me.LayoutControlItem33.Text = "LayoutControlItem33"
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem33.TextToControlDistance = 0
        Me.LayoutControlItem33.TextVisible = False
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.LayoutControl17
        Me.LayoutControlItem34.CustomizationFormText = "LayoutControlItem34"
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 81)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(1133, 477)
        Me.LayoutControlItem34.Text = "LayoutControlItem34"
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem34.TextToControlDistance = 0
        Me.LayoutControlItem34.TextVisible = False
        '
        'SplitterItem18
        '
        Me.SplitterItem18.AllowHotTrack = True
        Me.SplitterItem18.CustomizationFormText = "SplitterItem18"
        Me.SplitterItem18.Location = New System.Drawing.Point(0, 76)
        Me.SplitterItem18.Name = "SplitterItem18"
        Me.SplitterItem18.Size = New System.Drawing.Size(1133, 5)
        '
        'tabSTIPackagePickup
        '
        Me.tabSTIPackagePickup.Controls.Add(Me.LayoutControl20)
        Me.tabSTIPackagePickup.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.tabSTIPackagePickup.Name = "tabSTIPackagePickup"
        Me.tabSTIPackagePickup.Size = New System.Drawing.Size(1153, 578)
        Me.tabSTIPackagePickup.Text = "Package Pickup"
        '
        'LayoutControl20
        '
        Me.LayoutControl20.Controls.Add(Me.LayoutControl21)
        Me.LayoutControl20.Controls.Add(Me.grpPreviousPackageHistory)
        Me.LayoutControl20.Controls.Add(Me.txtPckPickMessage)
        Me.LayoutControl20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl20.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl20.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl20.Name = "LayoutControl20"
        Me.LayoutControl20.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(270, 296, 250, 350)
        Me.LayoutControl20.Root = Me.LayoutControlGroup20
        Me.LayoutControl20.Size = New System.Drawing.Size(1153, 578)
        Me.LayoutControl20.TabIndex = 8
        Me.LayoutControl20.Text = "LayoutControl20"
        '
        'LayoutControl21
        '
        Me.LayoutControl21.Controls.Add(Me.PanelControl19)
        Me.LayoutControl21.Controls.Add(Me.PanelControl18)
        Me.LayoutControl21.Location = New System.Drawing.Point(12, 88)
        Me.LayoutControl21.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl21.Name = "LayoutControl21"
        Me.LayoutControl21.Root = Me.LayoutControlGroup21
        Me.LayoutControl21.Size = New System.Drawing.Size(1129, 209)
        Me.LayoutControl21.TabIndex = 6
        Me.LayoutControl21.Text = "LayoutControl21"
        '
        'PanelControl19
        '
        Me.PanelControl19.Controls.Add(Me.grpBinPickup)
        Me.PanelControl19.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl19.Name = "PanelControl19"
        Me.PanelControl19.Size = New System.Drawing.Size(266, 185)
        Me.PanelControl19.TabIndex = 5
        '
        'grpBinPickup
        '
        Me.grpBinPickup.Controls.Add(Me.LabelControl4)
        Me.grpBinPickup.Controls.Add(Me.btnBinPickup)
        Me.grpBinPickup.Controls.Add(Me.txtBinPickup)
        Me.grpBinPickup.Location = New System.Drawing.Point(5, 5)
        Me.grpBinPickup.Name = "grpBinPickup"
        Me.grpBinPickup.Size = New System.Drawing.Size(227, 89)
        Me.grpBinPickup.TabIndex = 3
        Me.grpBinPickup.Text = "Package Pickup"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(5, 27)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(134, 16)
        Me.LabelControl4.TabIndex = 1004
        Me.LabelControl4.Text = "Bin Barcode"
        '
        'btnBinPickup
        '
        Me.btnBinPickup.Location = New System.Drawing.Point(58, 58)
        Me.btnBinPickup.Name = "btnBinPickup"
        Me.btnBinPickup.Size = New System.Drawing.Size(148, 25)
        Me.btnBinPickup.TabIndex = 1006
        Me.btnBinPickup.Text = "Pickup"
        '
        'txtBinPickup
        '
        Me.txtBinPickup.Location = New System.Drawing.Point(123, 24)
        Me.txtBinPickup.Name = "txtBinPickup"
        Me.txtBinPickup.Size = New System.Drawing.Size(96, 22)
        Me.txtBinPickup.TabIndex = 1005
        '
        'PanelControl18
        '
        Me.PanelControl18.Controls.Add(Me.GridPackagePickup)
        Me.PanelControl18.Location = New System.Drawing.Point(282, 12)
        Me.PanelControl18.Name = "PanelControl18"
        Me.PanelControl18.Size = New System.Drawing.Size(835, 185)
        Me.PanelControl18.TabIndex = 4
        '
        'GridPackagePickup
        '
        Me.GridPackagePickup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridPackagePickup.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GridPackagePickup.Location = New System.Drawing.Point(2, 2)
        Me.GridPackagePickup.MainView = Me.GridViewPackagePickup
        Me.GridPackagePickup.MenuManager = Me.RibbonControl
        Me.GridPackagePickup.Name = "GridPackagePickup"
        Me.GridPackagePickup.Size = New System.Drawing.Size(831, 181)
        Me.GridPackagePickup.TabIndex = 4
        Me.GridPackagePickup.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewPackagePickup})
        '
        'GridViewPackagePickup
        '
        Me.GridViewPackagePickup.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn36, Me.GridColumn39, Me.GridColumn45, Me.GridColumn46})
        Me.GridViewPackagePickup.GridControl = Me.GridPackagePickup
        Me.GridViewPackagePickup.Name = "GridViewPackagePickup"
        Me.GridViewPackagePickup.OptionsView.ShowGroupPanel = False
        '
        'GridColumn36
        '
        Me.GridColumn36.Caption = "Bin Name"
        Me.GridColumn36.FieldName = "BIN_NAME"
        Me.GridColumn36.Name = "GridColumn36"
        Me.GridColumn36.Visible = True
        Me.GridColumn36.VisibleIndex = 0
        '
        'GridColumn39
        '
        Me.GridColumn39.Caption = "Package Barcode"
        Me.GridColumn39.FieldName = "PACKAGE_BARCODE"
        Me.GridColumn39.Name = "GridColumn39"
        Me.GridColumn39.Visible = True
        Me.GridColumn39.VisibleIndex = 1
        '
        'GridColumn45
        '
        Me.GridColumn45.Caption = "Recipient"
        Me.GridColumn45.FieldName = "PACKAGE_RECIPIENT"
        Me.GridColumn45.Name = "GridColumn45"
        Me.GridColumn45.Visible = True
        Me.GridColumn45.VisibleIndex = 2
        '
        'GridColumn46
        '
        Me.GridColumn46.Caption = "Priority"
        Me.GridColumn46.FieldName = "PRIORITY"
        Me.GridColumn46.Name = "GridColumn46"
        '
        'LayoutControlGroup21
        '
        Me.LayoutControlGroup21.CustomizationFormText = "LayoutControlGroup21"
        Me.LayoutControlGroup21.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup21.GroupBordersVisible = False
        Me.LayoutControlGroup21.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem44, Me.LayoutControlItem45})
        Me.LayoutControlGroup21.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup21.Name = "LayoutControlGroup21"
        Me.LayoutControlGroup21.Size = New System.Drawing.Size(1129, 209)
        Me.LayoutControlGroup21.Text = "LayoutControlGroup21"
        Me.LayoutControlGroup21.TextVisible = False
        '
        'LayoutControlItem44
        '
        Me.LayoutControlItem44.Control = Me.PanelControl18
        Me.LayoutControlItem44.CustomizationFormText = "LayoutControlItem44"
        Me.LayoutControlItem44.Location = New System.Drawing.Point(270, 0)
        Me.LayoutControlItem44.Name = "LayoutControlItem44"
        Me.LayoutControlItem44.Size = New System.Drawing.Size(839, 189)
        Me.LayoutControlItem44.Text = "LayoutControlItem44"
        Me.LayoutControlItem44.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem44.TextToControlDistance = 0
        Me.LayoutControlItem44.TextVisible = False
        '
        'LayoutControlItem45
        '
        Me.LayoutControlItem45.Control = Me.PanelControl19
        Me.LayoutControlItem45.CustomizationFormText = "LayoutControlItem45"
        Me.LayoutControlItem45.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem45.Name = "LayoutControlItem45"
        Me.LayoutControlItem45.Size = New System.Drawing.Size(270, 189)
        Me.LayoutControlItem45.Text = "LayoutControlItem45"
        Me.LayoutControlItem45.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem45.TextToControlDistance = 0
        Me.LayoutControlItem45.TextVisible = False
        '
        'grpPreviousPackageHistory
        '
        Me.grpPreviousPackageHistory.Controls.Add(Me.GridPackagePickupHistory)
        Me.grpPreviousPackageHistory.Location = New System.Drawing.Point(12, 306)
        Me.grpPreviousPackageHistory.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpPreviousPackageHistory.Name = "grpPreviousPackageHistory"
        Me.grpPreviousPackageHistory.Size = New System.Drawing.Size(1129, 260)
        Me.grpPreviousPackageHistory.TabIndex = 7
        Me.grpPreviousPackageHistory.Text = "Previous Package Pickups"
        '
        'GridPackagePickupHistory
        '
        Me.GridPackagePickupHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridPackagePickupHistory.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GridPackagePickupHistory.Location = New System.Drawing.Point(2, 24)
        Me.GridPackagePickupHistory.MainView = Me.GridViewPackagePickupHistory
        Me.GridPackagePickupHistory.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GridPackagePickupHistory.MenuManager = Me.RibbonControl
        Me.GridPackagePickupHistory.Name = "GridPackagePickupHistory"
        Me.GridPackagePickupHistory.Size = New System.Drawing.Size(1125, 234)
        Me.GridPackagePickupHistory.TabIndex = 6
        Me.GridPackagePickupHistory.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewPackagePickupHistory})
        '
        'GridViewPackagePickupHistory
        '
        Me.GridViewPackagePickupHistory.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn74, Me.GridColumn75, Me.GridColumn76, Me.GridColumn77})
        Me.GridViewPackagePickupHistory.GridControl = Me.GridPackagePickupHistory
        Me.GridViewPackagePickupHistory.Name = "GridViewPackagePickupHistory"
        Me.GridViewPackagePickupHistory.OptionsView.ShowGroupPanel = False
        '
        'GridColumn74
        '
        Me.GridColumn74.Caption = "Intended recipient"
        Me.GridColumn74.FieldName = "INTENDED_RECIPIENT"
        Me.GridColumn74.Name = "GridColumn74"
        Me.GridColumn74.Visible = True
        Me.GridColumn74.VisibleIndex = 0
        '
        'GridColumn75
        '
        Me.GridColumn75.Caption = "Actual Recipient"
        Me.GridColumn75.FieldName = "ACTUAL_RECIPIENT"
        Me.GridColumn75.Name = "GridColumn75"
        Me.GridColumn75.Visible = True
        Me.GridColumn75.VisibleIndex = 1
        '
        'GridColumn76
        '
        Me.GridColumn76.Caption = "Barcode"
        Me.GridColumn76.FieldName = "BARCODE"
        Me.GridColumn76.Name = "GridColumn76"
        Me.GridColumn76.Visible = True
        Me.GridColumn76.VisibleIndex = 2
        '
        'GridColumn77
        '
        Me.GridColumn77.Caption = "Timestamp"
        Me.GridColumn77.FieldName = "DATE_CREATED"
        Me.GridColumn77.Name = "GridColumn77"
        Me.GridColumn77.Visible = True
        Me.GridColumn77.VisibleIndex = 3
        '
        'txtPckPickMessage
        '
        Me.txtPckPickMessage.Location = New System.Drawing.Point(12, 12)
        Me.txtPckPickMessage.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPckPickMessage.MenuManager = Me.RibbonControl
        Me.txtPckPickMessage.Name = "txtPckPickMessage"
        Me.txtPckPickMessage.Properties.ReadOnly = True
        Me.txtPckPickMessage.Size = New System.Drawing.Size(1129, 72)
        Me.txtPckPickMessage.StyleController = Me.LayoutControl20
        Me.txtPckPickMessage.TabIndex = 5
        '
        'LayoutControlGroup20
        '
        Me.LayoutControlGroup20.CustomizationFormText = "Root"
        Me.LayoutControlGroup20.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup20.GroupBordersVisible = False
        Me.LayoutControlGroup20.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem41, Me.LayoutControlItem42, Me.LayoutControlItem43, Me.SplitterItem19})
        Me.LayoutControlGroup20.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup20.Name = "Root"
        Me.LayoutControlGroup20.Size = New System.Drawing.Size(1153, 578)
        Me.LayoutControlGroup20.Text = "Root"
        Me.LayoutControlGroup20.TextVisible = False
        '
        'LayoutControlItem41
        '
        Me.LayoutControlItem41.Control = Me.txtPckPickMessage
        Me.LayoutControlItem41.CustomizationFormText = "LayoutControlItem41"
        Me.LayoutControlItem41.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem41.Name = "LayoutControlItem41"
        Me.LayoutControlItem41.Size = New System.Drawing.Size(1133, 76)
        Me.LayoutControlItem41.Text = "LayoutControlItem41"
        Me.LayoutControlItem41.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem41.TextToControlDistance = 0
        Me.LayoutControlItem41.TextVisible = False
        '
        'LayoutControlItem42
        '
        Me.LayoutControlItem42.Control = Me.LayoutControl21
        Me.LayoutControlItem42.CustomizationFormText = "LayoutControlItem42"
        Me.LayoutControlItem42.Location = New System.Drawing.Point(0, 76)
        Me.LayoutControlItem42.Name = "LayoutControlItem42"
        Me.LayoutControlItem42.Size = New System.Drawing.Size(1133, 213)
        Me.LayoutControlItem42.Text = "LayoutControlItem42"
        Me.LayoutControlItem42.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem42.TextToControlDistance = 0
        Me.LayoutControlItem42.TextVisible = False
        '
        'LayoutControlItem43
        '
        Me.LayoutControlItem43.Control = Me.grpPreviousPackageHistory
        Me.LayoutControlItem43.CustomizationFormText = "LayoutControlItem43"
        Me.LayoutControlItem43.Location = New System.Drawing.Point(0, 294)
        Me.LayoutControlItem43.Name = "LayoutControlItem43"
        Me.LayoutControlItem43.Size = New System.Drawing.Size(1133, 264)
        Me.LayoutControlItem43.Text = "LayoutControlItem43"
        Me.LayoutControlItem43.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem43.TextToControlDistance = 0
        Me.LayoutControlItem43.TextVisible = False
        '
        'SplitterItem19
        '
        Me.SplitterItem19.AllowHotTrack = True
        Me.SplitterItem19.CustomizationFormText = "SplitterItem19"
        Me.SplitterItem19.Location = New System.Drawing.Point(0, 289)
        Me.SplitterItem19.Name = "SplitterItem19"
        Me.SplitterItem19.Size = New System.Drawing.Size(1133, 5)
        '
        'tabSTIBinMaintenance
        '
        Me.tabSTIBinMaintenance.Controls.Add(Me.LayoutControl18)
        Me.tabSTIBinMaintenance.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.tabSTIBinMaintenance.Name = "tabSTIBinMaintenance"
        Me.tabSTIBinMaintenance.Size = New System.Drawing.Size(1153, 578)
        Me.tabSTIBinMaintenance.Text = "Bin Maintenance"
        '
        'LayoutControl18
        '
        Me.LayoutControl18.Controls.Add(Me.LayoutControl19)
        Me.LayoutControl18.Controls.Add(Me.MemoEdit2)
        Me.LayoutControl18.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl18.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl18.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl18.Name = "LayoutControl18"
        Me.LayoutControl18.Root = Me.LayoutControlGroup18
        Me.LayoutControl18.Size = New System.Drawing.Size(1153, 578)
        Me.LayoutControl18.TabIndex = 8
        Me.LayoutControl18.Text = "LayoutControl18"
        '
        'LayoutControl19
        '
        Me.LayoutControl19.Controls.Add(Me.PanelControl17)
        Me.LayoutControl19.Controls.Add(Me.PanelControl16)
        Me.LayoutControl19.Location = New System.Drawing.Point(12, 93)
        Me.LayoutControl19.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl19.Name = "LayoutControl19"
        Me.LayoutControl19.Root = Me.LayoutControlGroup19
        Me.LayoutControl19.Size = New System.Drawing.Size(1129, 473)
        Me.LayoutControl19.TabIndex = 8
        Me.LayoutControl19.Text = "LayoutControl19"
        '
        'PanelControl17
        '
        Me.PanelControl17.Controls.Add(Me.GridBinMaintenance)
        Me.PanelControl17.Location = New System.Drawing.Point(290, 12)
        Me.PanelControl17.Name = "PanelControl17"
        Me.PanelControl17.Size = New System.Drawing.Size(827, 449)
        Me.PanelControl17.TabIndex = 5
        '
        'GridBinMaintenance
        '
        Me.GridBinMaintenance.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridBinMaintenance.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GridBinMaintenance.Location = New System.Drawing.Point(2, 2)
        Me.GridBinMaintenance.MainView = Me.GridViewBinMaintenance
        Me.GridBinMaintenance.MenuManager = Me.RibbonControl
        Me.GridBinMaintenance.Name = "GridBinMaintenance"
        Me.GridBinMaintenance.Size = New System.Drawing.Size(823, 445)
        Me.GridBinMaintenance.TabIndex = 5
        Me.GridBinMaintenance.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewBinMaintenance})
        '
        'GridViewBinMaintenance
        '
        Me.GridViewBinMaintenance.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn70, Me.GridColumn71, Me.GridColumn72})
        Me.GridViewBinMaintenance.GridControl = Me.GridBinMaintenance
        Me.GridViewBinMaintenance.Name = "GridViewBinMaintenance"
        Me.GridViewBinMaintenance.OptionsView.ShowGroupPanel = False
        '
        'GridColumn70
        '
        Me.GridColumn70.Caption = "Bin Name"
        Me.GridColumn70.FieldName = "BIN_NAME"
        Me.GridColumn70.Name = "GridColumn70"
        Me.GridColumn70.Visible = True
        Me.GridColumn70.VisibleIndex = 0
        '
        'GridColumn71
        '
        Me.GridColumn71.Caption = "Package Barcode"
        Me.GridColumn71.FieldName = "PACKAGE_BARCODE"
        Me.GridColumn71.Name = "GridColumn71"
        Me.GridColumn71.Visible = True
        Me.GridColumn71.VisibleIndex = 1
        '
        'GridColumn72
        '
        Me.GridColumn72.Caption = "Recipient"
        Me.GridColumn72.FieldName = "PACKAGE_RECIPIENT"
        Me.GridColumn72.Name = "GridColumn72"
        Me.GridColumn72.Visible = True
        Me.GridColumn72.VisibleIndex = 2
        '
        'PanelControl16
        '
        Me.PanelControl16.Controls.Add(Me.grpBinMaintenance)
        Me.PanelControl16.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl16.Name = "PanelControl16"
        Me.PanelControl16.Size = New System.Drawing.Size(274, 449)
        Me.PanelControl16.TabIndex = 4
        '
        'grpBinMaintenance
        '
        Me.grpBinMaintenance.Controls.Add(Me.LabelControl5)
        Me.grpBinMaintenance.Controls.Add(Me.btnEmptyBin)
        Me.grpBinMaintenance.Controls.Add(Me.txtBinMaintain)
        Me.grpBinMaintenance.Location = New System.Drawing.Point(5, 5)
        Me.grpBinMaintenance.Name = "grpBinMaintenance"
        Me.grpBinMaintenance.Size = New System.Drawing.Size(227, 89)
        Me.grpBinMaintenance.TabIndex = 6
        Me.grpBinMaintenance.Text = "Bin Maintenance"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(5, 27)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(67, 16)
        Me.LabelControl5.TabIndex = 1004
        Me.LabelControl5.Text = "Bin Barcode"
        '
        'btnEmptyBin
        '
        Me.btnEmptyBin.Location = New System.Drawing.Point(58, 58)
        Me.btnEmptyBin.Name = "btnEmptyBin"
        Me.btnEmptyBin.Size = New System.Drawing.Size(148, 25)
        Me.btnEmptyBin.TabIndex = 1006
        Me.btnEmptyBin.Text = "Reset Bin"
        '
        'txtBinMaintain
        '
        Me.txtBinMaintain.Location = New System.Drawing.Point(123, 24)
        Me.txtBinMaintain.Name = "txtBinMaintain"
        Me.txtBinMaintain.Size = New System.Drawing.Size(96, 22)
        Me.txtBinMaintain.TabIndex = 1005
        '
        'LayoutControlGroup19
        '
        Me.LayoutControlGroup19.CustomizationFormText = "LayoutControlGroup19"
        Me.LayoutControlGroup19.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup19.GroupBordersVisible = False
        Me.LayoutControlGroup19.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem39, Me.LayoutControlItem40})
        Me.LayoutControlGroup19.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup19.Name = "LayoutControlGroup19"
        Me.LayoutControlGroup19.Size = New System.Drawing.Size(1129, 473)
        Me.LayoutControlGroup19.Text = "LayoutControlGroup19"
        Me.LayoutControlGroup19.TextVisible = False
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.PanelControl16
        Me.LayoutControlItem39.CustomizationFormText = "LayoutControlItem39"
        Me.LayoutControlItem39.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(278, 453)
        Me.LayoutControlItem39.Text = "LayoutControlItem39"
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem39.TextToControlDistance = 0
        Me.LayoutControlItem39.TextVisible = False
        '
        'LayoutControlItem40
        '
        Me.LayoutControlItem40.Control = Me.PanelControl17
        Me.LayoutControlItem40.CustomizationFormText = "LayoutControlItem40"
        Me.LayoutControlItem40.Location = New System.Drawing.Point(278, 0)
        Me.LayoutControlItem40.Name = "LayoutControlItem40"
        Me.LayoutControlItem40.Size = New System.Drawing.Size(831, 453)
        Me.LayoutControlItem40.Text = "LayoutControlItem40"
        Me.LayoutControlItem40.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem40.TextToControlDistance = 0
        Me.LayoutControlItem40.TextVisible = False
        '
        'MemoEdit2
        '
        Me.MemoEdit2.Location = New System.Drawing.Point(12, 12)
        Me.MemoEdit2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MemoEdit2.MenuManager = Me.RibbonControl
        Me.MemoEdit2.Name = "MemoEdit2"
        Me.MemoEdit2.Properties.ReadOnly = True
        Me.MemoEdit2.Size = New System.Drawing.Size(1129, 77)
        Me.MemoEdit2.StyleController = Me.LayoutControl18
        Me.MemoEdit2.TabIndex = 7
        '
        'LayoutControlGroup18
        '
        Me.LayoutControlGroup18.CustomizationFormText = "LayoutControlGroup18"
        Me.LayoutControlGroup18.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup18.GroupBordersVisible = False
        Me.LayoutControlGroup18.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem37, Me.LayoutControlItem38})
        Me.LayoutControlGroup18.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup18.Name = "LayoutControlGroup18"
        Me.LayoutControlGroup18.Size = New System.Drawing.Size(1153, 578)
        Me.LayoutControlGroup18.Text = "LayoutControlGroup18"
        Me.LayoutControlGroup18.TextVisible = False
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.MemoEdit2
        Me.LayoutControlItem37.CustomizationFormText = "LayoutControlItem37"
        Me.LayoutControlItem37.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(1133, 81)
        Me.LayoutControlItem37.Text = "LayoutControlItem37"
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem37.TextToControlDistance = 0
        Me.LayoutControlItem37.TextVisible = False
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.Control = Me.LayoutControl19
        Me.LayoutControlItem38.CustomizationFormText = "LayoutControlItem38"
        Me.LayoutControlItem38.Location = New System.Drawing.Point(0, 81)
        Me.LayoutControlItem38.Name = "LayoutControlItem38"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(1133, 477)
        Me.LayoutControlItem38.Text = "LayoutControlItem38"
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem38.TextToControlDistance = 0
        Me.LayoutControlItem38.TextVisible = False
        '
        'tabSTILog
        '
        Me.tabSTILog.Controls.Add(Me.LayoutControl1)
        Me.tabSTILog.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.tabSTILog.Name = "tabSTILog"
        Me.tabSTILog.Size = New System.Drawing.Size(1153, 578)
        Me.tabSTILog.Text = "Log"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.GridSTILog)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup2
        Me.LayoutControl1.Size = New System.Drawing.Size(1153, 578)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'GridSTILog
        '
        Me.GridSTILog.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GridSTILog.Location = New System.Drawing.Point(12, 12)
        Me.GridSTILog.MainView = Me.GridViewSTILog
        Me.GridSTILog.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GridSTILog.MenuManager = Me.RibbonControl
        Me.GridSTILog.Name = "GridSTILog"
        Me.GridSTILog.Size = New System.Drawing.Size(1129, 554)
        Me.GridSTILog.TabIndex = 4
        Me.GridSTILog.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewSTILog})
        '
        'GridViewSTILog
        '
        Me.GridViewSTILog.GridControl = Me.GridSTILog
        Me.GridViewSTILog.Name = "GridViewSTILog"
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CustomizationFormText = "LayoutControlGroup2"
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = False
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1153, 578)
        Me.LayoutControlGroup2.Text = "LayoutControlGroup2"
        Me.LayoutControlGroup2.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.GridSTILog
        Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1133, 558)
        Me.LayoutControlItem1.Text = "LayoutControlItem1"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextToControlDistance = 0
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlGroup14
        '
        Me.LayoutControlGroup14.CustomizationFormText = "Root"
        Me.LayoutControlGroup14.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup14.GroupBordersVisible = False
        Me.LayoutControlGroup14.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem27, Me.LayoutControlItem28, Me.SplitterItem15})
        Me.LayoutControlGroup14.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup14.Name = "Root"
        Me.LayoutControlGroup14.Size = New System.Drawing.Size(1436, 637)
        Me.LayoutControlGroup14.Text = "Root"
        Me.LayoutControlGroup14.TextVisible = False
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.PanelControl12
        Me.LayoutControlItem27.CustomizationFormText = "LayoutControlItem27"
        Me.LayoutControlItem27.Location = New System.Drawing.Point(249, 0)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(1167, 617)
        Me.LayoutControlItem27.Text = "LayoutControlItem27"
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem27.TextToControlDistance = 0
        Me.LayoutControlItem27.TextVisible = False
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.PanelControl13
        Me.LayoutControlItem28.CustomizationFormText = "LayoutControlItem28"
        Me.LayoutControlItem28.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(244, 617)
        Me.LayoutControlItem28.Text = "LayoutControlItem28"
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem28.TextToControlDistance = 0
        Me.LayoutControlItem28.TextVisible = False
        '
        'SplitterItem15
        '
        Me.SplitterItem15.AllowHotTrack = True
        Me.SplitterItem15.CustomizationFormText = "SplitterItem15"
        Me.SplitterItem15.Location = New System.Drawing.Point(244, 0)
        Me.SplitterItem15.Name = "SplitterItem15"
        Me.SplitterItem15.Size = New System.Drawing.Size(5, 617)
        '
        'STITimer
        '
        Me.STITimer.Interval = 1000
        '
        'BackgroundWorker1
        '
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1442, 854)
        Me.Controls.Add(Me.tabMain)
        Me.Controls.Add(Me.MainLayout)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmMain"
        Me.Ribbon = Me.RibbonControl
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "Global Shipping and Tracking"
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewActivePackageDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdActivePackageDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewActivePackage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewExistingPackagesDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdExistingPackages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.menuGridRightClick.ResumeLayout(False)
        CType(Me.GrdViewExistingPackages, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewPendingItemsDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdPendingItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewPendingItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdFinalPackageDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewFinalPackage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdFinalizedPackages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.menuFinalizedPackagesRightClick.ResumeLayout(False)
        CType(Me.GrdViewFinalizedPackages, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewActiveShipmentDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdActiveShipmentDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewActiveShipment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdExistingShipments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdViewExistingShipments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdPendingPackages, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdViewPendingPackages, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdFinalizedShipments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.altMenuGridRightClick.ResumeLayout(False)
        CType(Me.GridViewFinalizedShipments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdAnticipatedShipments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewAntShipments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewRcvPkgContainers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdRcvPackageView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewRcvPackage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewRcvShpPackages, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdReceiveShipment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewRcvShpContainers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewRcvShipment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridShipment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewShipmentDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MainLayout, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlMainGroup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SchedulerStorage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox10.ResumeLayout(False)
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox9.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        CType(Me.CheckedListBoxControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMain.ResumeLayout(False)
        Me.TabCreatePackage.ResumeLayout(False)
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl5.ResumeLayout(False)
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.GroupControl14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl14.ResumeLayout(False)
        CType(Me.txtRemoveItem.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpAddPackageItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpAddPackageItem.ResumeLayout(False)
        CType(Me.txtPackageItemBarcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrpContent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpContent.ResumeLayout(False)
        Me.GrpContent.PerformLayout()
        CType(Me.lkPackageOrigin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lkPackageContent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpValidationRules, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpValidationRules.ResumeLayout(False)
        CType(Me.ChkLstPackageValidationRules, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpFinalizePackage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpFinalizePackage.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        CType(Me.LayoutControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl6.ResumeLayout(False)
        CType(Me.grpExistingPackages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpExistingPackages.ResumeLayout(False)
        CType(Me.grpPendingItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPendingItems.ResumeLayout(False)
        Me.grpPendingItems.PerformLayout()
        CType(Me.lkPendingTimeframe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpActivePackageDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpActivePackageDetail.ResumeLayout(False)
        Me.grpActivePackageDetail.PerformLayout()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabLogin.ResumeLayout(False)
        CType(Me.GrpActiveUser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpActiveUser.ResumeLayout(False)
        Me.GrpActiveUser.PerformLayout()
        CType(Me.lkLoginDomains.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUserPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUsername.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPackageMaintenance.ResumeLayout(False)
        CType(Me.LayoutControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl7.ResumeLayout(False)
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl6.ResumeLayout(False)
        CType(Me.GroupControl19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl19.ResumeLayout(False)
        CType(Me.txtReprintPackageLabel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpDeliverPackage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpDeliverPackage.ResumeLayout(False)
        CType(Me.txtDeliverPackageBarcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpUnfinalizePackage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpUnfinalizePackage.ResumeLayout(False)
        CType(Me.txtFnlPackageBarcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        CType(Me.LayoutControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl8.ResumeLayout(False)
        CType(Me.GroupControl15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl15.ResumeLayout(False)
        CType(Me.grpFinalizedPackages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpFinalizedPackages.ResumeLayout(False)
        Me.grpFinalizedPackages.PerformLayout()
        CType(Me.lkFinPkgTimeline.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabCreateShipment.ResumeLayout(False)
        CType(Me.LayoutControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl9.ResumeLayout(False)
        CType(Me.PanelControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl8.ResumeLayout(False)
        Me.PanelControl8.PerformLayout()
        CType(Me.lkShipmentType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpFinalizeShipment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpFinalizeShipment.ResumeLayout(False)
        Me.grpFinalizeShipment.PerformLayout()
        CType(Me.lkShippingPriority.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lkShippingCourier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpShipmentRules, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpShipmentRules.ResumeLayout(False)
        CType(Me.ChkLstShipmentValidationRules, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lkFillingMaterial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpAddPackage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpAddPackage.ResumeLayout(False)
        CType(Me.txtShipmentPackageBarcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        CType(Me.LayoutControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl10.ResumeLayout(False)
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl12.ResumeLayout(False)
        CType(Me.grpExistingShipments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpExistingShipments.ResumeLayout(False)
        CType(Me.grpPendingPackages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPendingPackages.ResumeLayout(False)
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabShipmentPlanner.ResumeLayout(False)
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl4.ResumeLayout(False)
        CType(Me.grpPendingShipments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPendingShipments.ResumeLayout(False)
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabViewShipment.ResumeLayout(False)
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.GroupControl21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl21.ResumeLayout(False)
        CType(Me.GroupControl20, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl20.ResumeLayout(False)
        Me.GroupControl20.PerformLayout()
        CType(Me.dtEditFromDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtEditFromDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lkSites.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtEditToDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtEditToDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpViewShipmentInput, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpViewShipmentInput.ResumeLayout(False)
        CType(Me.txtViewShipment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.grpShipmentInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpShipmentInfo.ResumeLayout(False)
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabReceive.ResumeLayout(False)
        CType(Me.LayoutControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl11.ResumeLayout(False)
        CType(Me.PanelControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl9.ResumeLayout(False)
        CType(Me.LayoutControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl12.ResumeLayout(False)
        CType(Me.PanelControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl11.ResumeLayout(False)
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        Me.GroupControl10.PerformLayout()
        CType(Me.txtReceivedBarcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpReceiveActions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpReceiveActions.ResumeLayout(False)
        CType(Me.PanelControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl10.ResumeLayout(False)
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl11.ResumeLayout(False)
        CType(Me.LayoutControlGroup13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl13.ResumeLayout(False)
        CType(Me.LayoutControlGroup12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabSettings.ResumeLayout(False)
        Me.TabSettings.PerformLayout()
        CType(Me.cboScanner.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboPrinterDPI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl18.ResumeLayout(False)
        Me.GroupControl18.PerformLayout()
        CType(Me.txtQuickInternational.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFedExAccount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lkFulfillmentSite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEnvironment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShipmentTemplate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLabelTemplatePath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrinterPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboPrinter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCostCenter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabSampleTransfer.ResumeLayout(False)
        CType(Me.LayoutControl13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl13.ResumeLayout(False)
        CType(Me.PanelControl13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl13.ResumeLayout(False)
        CType(Me.GrpSimpleLogin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpSimpleLogin.ResumeLayout(False)
        Me.GrpSimpleLogin.PerformLayout()
        CType(Me.txtSimpleUsername.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl12.ResumeLayout(False)
        CType(Me.tabSTIFunction, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSTIFunction.ResumeLayout(False)
        Me.tabSTISampleDropoff.ResumeLayout(False)
        CType(Me.LayoutControl14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl14.ResumeLayout(False)
        CType(Me.LayoutControl15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl15.ResumeLayout(False)
        CType(Me.GroupControl16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl16.ResumeLayout(False)
        Me.GroupControl16.PerformLayout()
        CType(Me.txtSTISampleSubmission.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl17.ResumeLayout(False)
        CType(Me.ListBoxControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSampleDropMessage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSTIPackageDropoff.ResumeLayout(False)
        CType(Me.LayoutControl16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl16.ResumeLayout(False)
        CType(Me.LayoutControl17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl17.ResumeLayout(False)
        CType(Me.PanelControl15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl15.ResumeLayout(False)
        CType(Me.GridPackageDropoff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdViewPackageDropoff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl14.ResumeLayout(False)
        CType(Me.grpPackageDropoff, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPackageDropoff.ResumeLayout(False)
        Me.grpPackageDropoff.PerformLayout()
        CType(Me.txtPackageDropoff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpBinDropoff, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpBinDropoff.ResumeLayout(False)
        Me.grpBinDropoff.PerformLayout()
        CType(Me.txtBinDropoff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPckDropMessage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSTIPackagePickup.ResumeLayout(False)
        CType(Me.LayoutControl20, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl20.ResumeLayout(False)
        CType(Me.LayoutControl21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl21.ResumeLayout(False)
        CType(Me.PanelControl19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl19.ResumeLayout(False)
        CType(Me.grpBinPickup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpBinPickup.ResumeLayout(False)
        Me.grpBinPickup.PerformLayout()
        CType(Me.txtBinPickup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl18.ResumeLayout(False)
        CType(Me.GridPackagePickup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewPackagePickup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpPreviousPackageHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPreviousPackageHistory.ResumeLayout(False)
        CType(Me.GridPackagePickupHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewPackagePickupHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPckPickMessage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSTIBinMaintenance.ResumeLayout(False)
        CType(Me.LayoutControl18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl18.ResumeLayout(False)
        CType(Me.LayoutControl19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl19.ResumeLayout(False)
        CType(Me.PanelControl17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl17.ResumeLayout(False)
        CType(Me.GridBinMaintenance, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewBinMaintenance, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl16.ResumeLayout(False)
        CType(Me.grpBinMaintenance, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpBinMaintenance.ResumeLayout(False)
        Me.grpBinMaintenance.PerformLayout()
        CType(Me.txtBinMaintain.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSTILog.ResumeLayout(False)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.GridSTILog, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewSTILog, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents MainLayout As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView3 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Request As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    'Friend WithEvents LocatorBeacon1 As DevExpress.CodeRush.PlugInCore.LocatorBeacon
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView8 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl5 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView9 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView10 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl6 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView11 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView12 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnImportFile As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckedListBoxControl1 As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SchedulerStorage1 As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents menuGridRightClick As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents menuRemoveItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents diaLabelTemplate As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents diaShipmentTemplate As System.Windows.Forms.OpenFileDialog
    Friend WithEvents altMenuGridRightClick As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents altMenuUnfinalize As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents altMenuPrintForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlMainGroup As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents bbtnCreatePackage As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpLogin As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents rpPackage As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents rpShipment As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents rpStats As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents rpSettings As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbtnPackageMaintenance As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbtnLogin As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents rpSampleTransfer As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents bbtnCreateShipment As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbtnShipmentPlanner As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbtnViewDetails As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbtnSettings As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbtnSampleTransfer As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup6 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup7 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents NavLinks As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents tabMain As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents TabCreatePackage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl14 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtRemoveItem As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnRemoveItem As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grpActivePackageDetail As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblPackageErrors As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdActivePackageDetails As DevExpress.XtraGrid.GridControl
    Friend WithEvents GrdViewActivePackageDetail As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GrdViewActivePackage As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colActPackContainerID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackBarcode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackFirst As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackLast As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackCompany As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackCity As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackCountry As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackSite As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackEmail As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackRequestNR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackDeliveryForm As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackProtocol As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActPackDestRackGroupName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grpExistingPackages As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdExistingPackages As DevExpress.XtraGrid.GridControl
    Friend WithEvents GrdViewExistingPackagesDetail As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GrdViewExistingPackages As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPackageID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistPackBarcode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistPackItemCount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistPackDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistPackRecipientFirst As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistPackRecipientLast As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistPackCompany As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistPackCountry As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistPackCity As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistPackAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grpPendingItems As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRefreshPackageGrids As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnShowAllPending As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lkPendingTimeframe As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdPendingItems As DevExpress.XtraGrid.GridControl
    Friend WithEvents GrdViewPendingItemsDetail As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GrdViewPendingItems As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colContainerID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBARCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTYPE_NAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDESTINATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENTFIRSTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECIPIENTLASTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmailAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colREQUEST_NR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTYPE_ID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRECEIVER_ID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPOUND As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFACTORY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDlvryForm As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSite As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colProtocol As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDestRackGroupName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grpAddPackageItem As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btn_LookupScan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnScanItems As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtPackageItemBarcode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grpValidationRules As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ChkLstPackageValidationRules As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents grpFinalizePackage As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnFinalizePackage As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TabLogin As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GrpActiveUser As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lkLoginDomains As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblDomain As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblUsername As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtUserPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPassword As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnLogin As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtUsername As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TabPackageMaintenance As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl15 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdFinalPackageDetails As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GrdViewFinalPackage As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn56 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn57 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn58 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn59 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn60 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn61 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn62 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn63 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn64 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn65 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn66 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn67 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn68 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn69 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grpUnfinalizePackage As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtFnlPackageBarcode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnUnfinalizePackage As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grpFinalizedPackages As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdFinalizedPackages As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GrdViewFinalizedPackages As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn37 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn38 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn47 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn48 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn49 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn50 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn51 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn52 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn53 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn54 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn55 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TabCreateShipment As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl12 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdActiveShipmentDetails As DevExpress.XtraGrid.GridControl
    Friend WithEvents GrdViewActiveShipmentDetails As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GrdViewActiveShipment As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colActShipContainerID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActShipPackageBarcode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActShipFirst As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActShipLast As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActShipCompany As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActShipAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActShipCountry As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActShipDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActShipMailAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colActShipSiteCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grpAddPackage As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCreateShipment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddPackageShipment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtShipmentPackageBarcode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grpExistingShipments As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdExistingShipments As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView13 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grdViewExistingShipments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colExistShipShipmentID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipFirst As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipLast As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipCompany As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipCountry As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipPackageCount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipItemCount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipReceiverID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistSiteCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipCity As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grpPendingPackages As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRefreshShipment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grdPendingPackages As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView15 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GrdViewPendingPackages As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPendPackContainerID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackBarcode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackReceiverID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackRecipientFirst As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackRecipientLast As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackCompany As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackCountry As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackItemCount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackMailAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackSiteCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPendPackCity As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grpShipmentRules As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ChkLstShipmentValidationRules As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents grpFinalizeShipment As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnViewShipmentForm As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFinalizeShipment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblShipmentType As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lkShipmentType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblFillingMaterial As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lkFillingMaterial As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblPriority As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblShippingCourier As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lkShippingPriority As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lkShippingCourier As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents TabShipmentPlanner As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grpPendingShipments As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdFinalizedShipments As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView21 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridViewFinalizedShipments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colFinShipID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFinShipName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFinShipFirst As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFinShipLastName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFinShipCompany As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFinShipCountry As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFinShipPackageCount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFinShipItemCount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFinShipShipDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFinShipCity As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TabReceive As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grpReceiveActions As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCancelRecieve As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReceiveItems As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl13 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdAnticipatedShipments As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridViewAntShipments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn22 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn23 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn24 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn25 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn26 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn27 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn28 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl11 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grdRcvPackageView As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewRcvPkgContainers As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPkCntParentContainerID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCntBarcode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCntSampleName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCntRanking As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCntFirstName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCntLastName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCntMailAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCntSiteCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCntOrderName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCntProtocolGroupName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridViewRcvPackage As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPkParentContainerID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkBarcode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkFirstName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkLastName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCompany As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkCountryName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkMailAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkSiteCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grdReceiveShipment As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewRcvShpPackages As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPkgParentContainerID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkgBarcode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkgFirstName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkgLastName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkgCompany As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkgCountryName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkgMailAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPkgSiteCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridViewRcvShpContainers As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCntContainerID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCntParentContainerID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCntBarcode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grdCntSampleName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCntRanking As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCntLastName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCntFirstName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCntMailAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCntSiteCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCntCity As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCntCountryName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCntOrderName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridViewRcvShipment As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colShipmentID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colShipmentName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFirstName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCompany As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCountryName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMailAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPackageCount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colContainerCount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl10 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnLookupShipment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtReceivedBarcode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblReceivedBarcode As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TabSettings As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents lkFulfillmentSite As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblFulfillmentSite As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboEnvironment As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents lblEnvironment As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnBrowseShipmentTemplate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtShipmentTemplate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnBrowseLabelTemplate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtLabelTemplatePath As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblLabelTemplatePath As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnSaveSettings As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblPrinterCOMport As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtPrinterPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cboPrinter As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents lblPrinter As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCostCenter As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TabSampleTransfer As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavSTIFunctions As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavSTISampleDropoff As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavSTIPackageDropoff As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavSTIPackagePickup As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavSTIBinMaintenance As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents tabSTIFunction As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tabSTISampleDropoff As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents txtSampleDropMessage As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl17 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ListBoxControl1 As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents GroupControl16 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnSTISampleSubmit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSTISampleSubmission As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tabSTIPackageDropoff As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents txtPckDropMessage As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GridPackageDropoff As DevExpress.XtraGrid.GridControl
    Friend WithEvents grdViewPackageDropoff As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn29 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn30 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn31 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn32 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn33 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn34 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grpBinDropoff As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblBinDropoff As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnBinDropoff As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtBinDropoff As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grpPackageDropoff As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblPackageDropoff As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnPackageDropoff As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtPackageDropoff As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tabSTIPackagePickup As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grpPreviousPackageHistory As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridPackagePickupHistory As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewPackagePickupHistory As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn74 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn75 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn76 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn77 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtPckPickMessage As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GridPackagePickup As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewPackagePickup As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn36 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn39 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn45 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn46 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grpBinPickup As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnBinPickup As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtBinPickup As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tabSTIBinMaintenance As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents MemoEdit2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents grpBinMaintenance As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnEmptyBin As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtBinMaintain As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridBinMaintenance As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewBinMaintenance As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn70 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn71 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn72 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GrpSimpleLogin As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblSimpleUsername As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnSimpleLogin As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtSimpleUsername As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TabViewShipment As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents grpViewShipmentInput As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnContainerBarcodeLookup As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtViewShipment As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents grpShipmentInfo As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridShipment As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView14 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridViewShipmentDetails As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem4 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GrpContent As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lkPackageOrigin As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblPackageOrigin As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPackageContent As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lkPackageContent As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutControl6 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup7 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem5 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents SplitterItem6 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents SplitterItem7 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl7 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents PanelControl6 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutControl8 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup9 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem9 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem8 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControl9 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents PanelControl8 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutControl10 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup11 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem11 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents SplitterItem12 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem10 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControl11 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents PanelControl9 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutControl12 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents PanelControl11 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl10 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutControlGroup13 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem14 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControlGroup12 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem13 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControl13 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents PanelControl13 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl12 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutControl14 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControl15 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem17 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControlGroup15 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem16 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControl16 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControl17 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents PanelControl15 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl14 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutControlGroup17 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup16 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem18 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControlGroup14 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem15 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControl18 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControl19 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents PanelControl17 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl16 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutControlGroup19 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem40 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup18 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl20 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControl21 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents PanelControl19 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl18 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LayoutControlGroup21 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem44 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem45 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup20 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem41 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem42 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem43 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem19 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl18 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtQuickInternational As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtFedExAccount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnArrived As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReadyForPickup As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnArrivedNoPhase As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bbtnReceiveShipment As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup8 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents btnTrackingURL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colExistShipPriority As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistShipExtRefName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnShowPendingPackages As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colActShipCity As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnRepackageItems As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colLeavingDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistPackLeavingDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents menuFinalizedPackagesRightClick As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnReceiveComment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn40 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lkFinPkgTimeline As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grpDeliverPackage As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtDeliverPackageBarcode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnDeliverPackage As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cboPrinterDPI As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents altMenuUpdateTracking As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colFinShipExtReference As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents bbtnSTISampleDropoff As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbtnSTIPackageDropoff As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbtnSTIPackagePickup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbtnSTIBinMaintenance As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup9 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup10 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup11 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup12 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents STITimer As System.Windows.Forms.Timer
    Friend WithEvents GroupControl19 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtReprintPackageLabel As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnReprintPackageLabel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn41 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn35 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl20 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lkSites As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents dtEditToDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents btnSiteContainers As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dtEditFromDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl21 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnExportXSL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTXTExport As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ActivePackageDetailsToolTip As DevExpress.Utils.ToolTipController
    Friend WithEvents FinalPackageToolTip As DevExpress.Utils.ToolTipController
    Friend WithEvents ActiveShipmentToolTip As DevExpress.Utils.ToolTipController
    Friend WithEvents colFulfillmentDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tabSTILog As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents GridSTILog As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewSTILog As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bbtnSTILog As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup13 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents NavSTILog As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents bbtnCreateInvPackage As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TabCreateInvPackage As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cboScanner As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnTestScan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnScannerHelp As DevExpress.XtraEditors.SimpleButton


End Class
