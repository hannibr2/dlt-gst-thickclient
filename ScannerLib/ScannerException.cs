﻿//*********************************************************************************
// Company:         Novartis NITAS
// Copyright:       Copyright © 2013
// Author:          Erik Hesse
// Date:            04/02/2013
//
// Project:         Scanner Library       
// Application:     ScannerLib
// File:            ScannerException.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
//   
//*********************************************************************************
using System;

namespace ScannerLib
{
	public class ScannerException : System.Exception
	{
		/// <summary>
		/// ScannerException constructor
		/// </summary>
		public ScannerException(string strMessage) : base(strMessage)
		{
		}

		/// <summary>
        /// ScannerException constructor
		/// </summary>
		/// <param name="strMessage">Message string</param>
		/// <param name="objInner">Exception object</param>
		public ScannerException( string strMessage, Exception objInner )
            : base(strMessage, objInner)
		{
            // Create the message string
            strMessage += "\nSource:\n\t" + objInner.Source.ToString()
                        + "\nStack Trace:\n" + objInner.StackTrace.ToString();
		}
	}
}
