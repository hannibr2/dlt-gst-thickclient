﻿//*********************************************************************************
// Company:         Novartis NITAS
// Copyright:       Copyright © 2013
// Author:          Erik Hesse
// Date:            02/20/2013
//
// Project:         Scanner Library       
// Application:     ScannerLib
// File:            CScanner_WIA.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
//   
//*********************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using WIA;

namespace ScannerLib
{
    public class CScanner_WIA : CScanner_Base
    {
        public override event MessageEvent evtMessage;

        // Image FormatID(s )
        private const string wiaFormatBMP = "{B96B3CAB-0728-11D3-9D7B-0000F81EF32E}";
        private const string wiaFormatPNG = "{B96B3CAF-0728-11D3-9D7B-0000F81EF32E}";
        private const string wiaFormatGIF = "{B96B3CB0-0728-11D3-9D7B-0000F81EF32E}";
        private const string wiaFormatJPEG = "{B96B3CAE-0728-11D3-9D7B-0000F81EF32E}";
        private const string wiaFormatTIFF = "{B96B3CB1-0728-11D3-9D7B-0000F81EF32E}";
        
        private WIA.Device m_Device;
        private WIA.Vector m_Vector;

        /// <summary>
        /// CScanner_WIA constructor
        /// </summary>
        public CScanner_WIA()
        {
            m_Device = null;
            m_Vector = null;
        }

        /// <summary>
        /// Init
        /// </summary>
        public override void Init()
        {
			if(base.Events)
				evtMessage( this, this.GetType().Name + ":Init" );
        }

        /// <summary>
        /// SetSource
        /// </summary>
        public override void SetSource()
        {
            try
            {
                WIA.CommonDialog dlg = new WIA.CommonDialog();

                // Select the device
                m_Device = dlg.ShowSelectDevice( WiaDeviceType.ScannerDeviceType, false, false );
            }
            catch( Exception eX )
            {
                throw new ScannerException( this.GetType().Name + ":SetSource [" + eX.Message + "]", eX );
            }

			if(base.Events)
				evtMessage( this, this.GetType().Name + ":SetSource" );
        }

        /// <summary>
        /// Scan
        /// </summary>
        public override void Scan()
        {
            try
            {
                WIA.CommonDialog WiaCommonDialog = new WIA.CommonDialog();
                WIA.DeviceManager manager = new DeviceManager();

                // Set the device
                foreach( DeviceInfo info in manager.DeviceInfos )
                {
                    if( info.DeviceID == this.m_Device.DeviceID )
                    {
                        WIA.Properties infoProp = null;
                        infoProp = info.Properties;

                        //connect to scanner
                        this.m_Device = info.Connect();

                        break;
                    }
                }

                // Set the item properties
                WIA.ImageFile img = null;
                WIA.Item item = m_Device.Items[1] as WIA.Item;
                foreach (WIA.Item myItem in m_Device.Items)
                {
                    foreach (WIA.Property itemProperty in item.Properties)
                    {
                        Object tempProperty;

                        if (itemProperty.Name.Equals("Brightness"))
                        {

                            tempProperty = ConfigurationManager.AppSettings["Scanner_Brightness"].ToString();
                            ((IProperty)itemProperty).set_Value(ref tempProperty);
                        }

                        if (itemProperty.Name.Equals("Contrast"))
                        {
                            tempProperty = ConfigurationManager.AppSettings["Scanner_Contrast"].ToString();

                            ((IProperty)itemProperty).set_Value(ref tempProperty);

                        }

                        if (itemProperty.Name.Equals("Horizontal Resolution"))
                        {
                            tempProperty = ConfigurationManager.AppSettings["DPI"].ToString(); ;
                            ((IProperty)itemProperty).set_Value(ref tempProperty);
                        }
                        else if (itemProperty.Name.Equals("Vertical Resolution"))
                        {
                            tempProperty = ConfigurationManager.AppSettings["DPI"].ToString(); ;
                            ((IProperty)itemProperty).set_Value(ref tempProperty);
                        }
                    }
                }

                // Scan the image
                img = (WIA.ImageFile)WiaCommonDialog.ShowTransfer( item, wiaFormatJPEG, false );

                // Get the image data
                m_Vector = img.FileData;
            }
            catch( Exception eX )
            {
                throw new ScannerException( this.GetType().Name + ":Scan [" + eX.Message + "]", eX );
            }

			if(base.Events)
				evtMessage( this, this.GetType().Name + ":Scan" );
        }

        ////

        /// <summary>
        /// GetImage
        /// </summary>
        /// <returns></returns>
        public override byte[] GetImage()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":GetImage" );

            return (byte[])m_Vector.get_BinaryData();
        }

        /// <summary>
        /// SaveSettings
        /// </summary>
        public override void SaveSettings()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":SaveSettings" );
        }

        /// <summary>
        /// LoadSettings
        /// </summary>
        public override void LoadSettings()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":LoadSettings" );
        }
    }
}
