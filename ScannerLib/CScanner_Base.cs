﻿//*********************************************************************************
// Company:         Novartis NITAS
// Copyright:       Copyright © 2013
// Author:          Erik Hesse
// Date:            02/20/2013
//
// Project:         Scanner Library       
// Application:     ScannerLib
// File:            CScanner_Base.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
//   
//*********************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerLib
{
    /// <summary>
    /// ScannerTypes
    /// </summary>
    public enum ScannerTypes
    {
        Simulator = 0,
        Twain = 1,
        WIA = 2
    }

	/// <summary>
	/// MessageEvent delegate
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="strMsg"></param>
	public delegate void MessageEvent(object sender, string strMsg);

    /// <summary>
    /// IScanner_Base
    /// </summary>
    public abstract class CScanner_Base
    {
		private bool m_bEvents = false;
		public bool Events
		{
			set { m_bEvents = value; }
			get { return m_bEvents; }
		}

        // Events
        public abstract event MessageEvent evtMessage;

        // Enums
        public ScannerTypes eScannerTypes;

        // Abstract Method definitions
        public abstract void SetSource();
        public abstract void Init();
        public abstract void Scan();
        public abstract byte[] GetImage();
        public abstract void SaveSettings();
        public abstract void LoadSettings();
    }
}
