﻿//*********************************************************************************
// Company:         Novartis NITAS
// Copyright:       Copyright © 2013
// Author:          Erik Hesse
// Date:            02/20/2013
//
// Project:         Scanner Library       
// Application:     ScannerLib
// File:            CScanner_Simulator.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
//   
//*********************************************************************************
using System;
using System.IO;

namespace ScannerLib
{
    public class CScanner_Simulator : CScanner_Base
    {
        public override event MessageEvent evtMessage;

        /// <summary>
        /// CScanner_Simulator constructor
        /// </summary>
        public CScanner_Simulator()
        {
        }

        /// <summary>
        /// Init
        /// </summary>
        public override void Init()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":Init" );
        }

        /// <summary>
        /// SetSource
        /// </summary>
        public override void SetSource()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":SetSource" );
        }

        /// <summary>
        /// Scan
        /// </summary>
        public override void Scan()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":Scan" );
        }

        /// <summary>
        /// GetImage
        /// </summary>
        /// <returns></returns>
        public override byte[] GetImage()
        {
			string strAppPath ="";
            string strSampleImage = "";

            try
            {
                strAppPath = System.IO.Path.GetDirectoryName( System.Reflection.Assembly.GetExecutingAssembly().Location );
                strSampleImage = strAppPath + "\\small.jpg";
                if( base.Events )
                    evtMessage( this, this.GetType().Name + ":GetImage" );
            }
            catch( Exception eX )
            {
                throw new ScannerException( this.GetType().Name + ":GetImage failed [" + eX.Message + "]", eX );
            }
			return File.ReadAllBytes( strSampleImage );
        }

        /// <summary>
        /// SaveSettings
        /// </summary>
        public override void SaveSettings()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":SaveSettings" );
        }

        /// <summary>
        /// LoadSettings
        /// </summary>
        public override void LoadSettings()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":LoadSettings" );
        }
    }
}
