//*********************************************************************************
// Code taken from http://www.codeproject.com/Articles/1376/NET-TWAIN-image-scanner
//
//*********************************************************************************
//*********************************************************************************
// Company:         Novartis NIBRIT/Delta
// Copyright:       Copyright � 2013
// Author:          Erik Hesse
// Date:            04/01/2013
//
//
// Project:         IntelliScan
// Application:     ScannerLib
// File:            TwainLib.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
// Date:        Ver:    Description
//
//*********************************************************************************
using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Xml;

namespace TwainLib
{

	public class Twain
	{
		private const short CountryUSA = 1;
		private const short LanguageUSA = 13;

		private IntPtr hwnd;
		private TwIdentity appid;
		private TwIdentity srcds;
		private TwEvent evtmsg;
		private WINMSG winmsg;

		#region Dll Imports

		// ------ DSM entry point DAT_ variants:
		[DllImport( "twain_32.dll", EntryPoint = "#1" )]
		private static extern TwRC DSMparent( [In, Out] TwIdentity origin, IntPtr zeroptr, TwDG dg, TwDAT dat, TwMSG msg, ref IntPtr refptr );
		[DllImport( "twain_32.dll", EntryPoint = "#1" )]
		private static extern TwRC DSMident( [In, Out] TwIdentity origin, IntPtr zeroptr, TwDG dg, TwDAT dat, TwMSG msg, [In, Out] TwIdentity idds );
		[DllImport( "twain_32.dll", EntryPoint = "#1" )]
		private static extern TwRC DSMstatus( [In, Out] TwIdentity origin, IntPtr zeroptr, TwDG dg, TwDAT dat, TwMSG msg, [In, Out] TwStatus dsmstat );

		// ------ DSM entry point DAT_ variants to DS:
		[DllImport( "twain_32.dll", EntryPoint = "#1" )]
		private static extern TwRC DSuserif( [In, Out] TwIdentity origin, [In, Out] TwIdentity dest, TwDG dg, TwDAT dat, TwMSG msg, TwUserInterface guif );
		[DllImport( "twain_32.dll", EntryPoint = "#1" )]
		private static extern TwRC DSevent( [In, Out] TwIdentity origin, [In, Out] TwIdentity dest, TwDG dg, TwDAT dat, TwMSG msg, ref TwEvent evt );
		[DllImport( "twain_32.dll", EntryPoint = "#1" )]
		private static extern TwRC DSstatus( [In, Out] TwIdentity origin, [In] TwIdentity dest, TwDG dg, TwDAT dat, TwMSG msg, [In, Out] TwStatus dsmstat );
		[DllImport( "twain_32.dll", EntryPoint = "#1" )]
		private static extern TwRC DScap( [In, Out] TwIdentity origin, [In] TwIdentity dest, TwDG dg, TwDAT dat, TwMSG msg, [In, Out] TwCapability capa );
		[DllImport( "twain_32.dll", EntryPoint = "#1" )]
		private static extern TwRC DSiinf( [In, Out] TwIdentity origin, [In] TwIdentity dest, TwDG dg, TwDAT dat, TwMSG msg, [In, Out] TwImageInfo imginf );
		[DllImport( "twain_32.dll", EntryPoint = "#1" )]
		private static extern TwRC DSixfer( [In, Out] TwIdentity origin, [In] TwIdentity dest, TwDG dg, TwDAT dat, TwMSG msg, ref IntPtr hbitmap );
		[DllImport( "twain_32.dll", EntryPoint = "#1" )]
		private static extern TwRC DSpxfer( [In, Out] TwIdentity origin, [In] TwIdentity dest, TwDG dg, TwDAT dat, TwMSG msg, [In, Out] TwPendingXfers pxfr );

		[DllImport( "kernel32.dll", ExactSpelling = true )]
		internal static extern IntPtr GlobalAlloc( int flags, int size );
		[DllImport( "kernel32.dll", ExactSpelling = true )]
		internal static extern IntPtr GlobalLock( IntPtr handle );
		[DllImport( "kernel32.dll", ExactSpelling = true )]
		internal static extern bool GlobalUnlock( IntPtr handle );
		[DllImport( "kernel32.dll", ExactSpelling = true )]
		internal static extern IntPtr GlobalFree( IntPtr handle );

		[DllImport( "user32.dll", ExactSpelling = true )]
		private static extern int GetMessagePos();
		[DllImport( "user32.dll", ExactSpelling = true )]
		private static extern int GetMessageTime();

		[DllImport( "gdi32.dll", ExactSpelling = true )]
		private static extern int GetDeviceCaps( IntPtr hDC, int nIndex );
		[DllImport( "gdi32.dll", CharSet = CharSet.Auto )]
		private static extern IntPtr CreateDC( string szdriver, string szdevice, string szoutput, IntPtr devmode );
		[DllImport( "gdi32.dll", ExactSpelling = true )]
		private static extern bool DeleteDC( IntPtr hdc );

		#endregion
		
		/// <summary>
		/// Twain constructor
		/// </summary>
		public Twain()
		{
			appid = new TwIdentity();
			appid.Id = IntPtr.Zero;
			appid.Version.MajorNum = 1;
			appid.Version.MinorNum = 1;
			appid.Version.Language = LanguageUSA;
			appid.Version.Country = CountryUSA;
			appid.Version.Info = "Hack 1";
			appid.ProtocolMajor = TwProtocol.Major;
			appid.ProtocolMinor = TwProtocol.Minor;
			appid.SupportedGroups = (int)( TwDG.Image | TwDG.Control );
			appid.Manufacturer = "NETMaster";
			appid.ProductFamily = "Freeware";
			appid.ProductName = "Hack";

			srcds = new TwIdentity();
			srcds.Id = IntPtr.Zero;

			evtmsg.EventPtr = Marshal.AllocHGlobal( Marshal.SizeOf( winmsg ) );
		}

		/// <summary>
		/// Twain destructor
		/// </summary>
		~Twain()
		{
			Marshal.FreeHGlobal( evtmsg.EventPtr );
		}

		/// <summary>
		/// Init
		/// </summary>
		/// <param name="hwndp"></param>
		public void Init( IntPtr hwndp )
		{
			Finish();
			TwRC rc = DSMparent( appid, IntPtr.Zero, TwDG.Control, TwDAT.Parent, TwMSG.OpenDSM, ref hwndp );
			if( rc == TwRC.Success )
			{
				rc = DSMident( appid, IntPtr.Zero, TwDG.Control, TwDAT.Identity, TwMSG.GetDefault, srcds );
				if( rc == TwRC.Success )
					hwnd = hwndp;
				else
					rc = DSMparent( appid, IntPtr.Zero, TwDG.Control, TwDAT.Parent, TwMSG.CloseDSM, ref hwndp );
			}
		}

		/// <summary>
		/// Select
		/// </summary>
		/// <returns></returns>
		public bool Select()
		{
			TwRC rc;
			CloseSrc();
			if( appid.Id == IntPtr.Zero )
			{
				Init( hwnd );
				if( appid.Id == IntPtr.Zero )
					return false;
			}
			//rc = DSMident( appid, IntPtr.Zero, TwDG.Control, TwDAT.Identity, TwMSG.UserSelect, srcds );
			rc = DSMident( appid, IntPtr.Zero, TwDG.Control, TwDAT.Identity, TwMSG.GetFirst, srcds );
			return true;
		}

		/// <summary>
		/// Acquire
		/// </summary>
		public void Acquire()
		{
			TwRC rc;
			CloseSrc();
			if( appid.Id == IntPtr.Zero )
			{
				Init( hwnd );
				if( appid.Id == IntPtr.Zero )
					return;
			}
			rc = DSMident( appid, IntPtr.Zero, TwDG.Control, TwDAT.Identity, TwMSG.OpenDS, srcds );
			if( rc != TwRC.Success )
				return;

			TwCapability cap = new TwCapability( TwCap.XferCount, 1 );
			rc = DScap( appid, srcds, TwDG.Control, TwDAT.Capability, TwMSG.Set, cap );
			if( rc != TwRC.Success )
			{
				CloseSrc();
				return;
			}

			TwUserInterface guif = new TwUserInterface();
			guif.ShowUI = 1;
			guif.ModalUI = 1;
			guif.ParentHand = hwnd;
			rc = DSuserif( appid, srcds, TwDG.Control, TwDAT.UserInterface, TwMSG.EnableDS, guif );
			if( rc != TwRC.Success )
			{
				CloseSrc();
				return;
			}
		}

		/// <summary>
		/// TransferPictures
		/// </summary>
		/// <returns></returns>
		public ArrayList TransferPictures()
		{
			ArrayList pics = new ArrayList();
			if( srcds.Id == IntPtr.Zero )
				return pics;

			TwRC rc;
			IntPtr hbitmap = IntPtr.Zero;
			TwPendingXfers pxfr = new TwPendingXfers();

			do
			{
				pxfr.Count = 0;
				hbitmap = IntPtr.Zero;

				TwImageInfo iinf = new TwImageInfo();
				rc = DSiinf( appid, srcds, TwDG.Image, TwDAT.ImageInfo, TwMSG.Get, iinf );
				if( rc != TwRC.Success )
				{
					CloseSrc();
					return pics;
				}

				rc = DSixfer( appid, srcds, TwDG.Image, TwDAT.ImageNativeXfer, TwMSG.Get, ref hbitmap );
				if( rc != TwRC.XferDone )
				{
					CloseSrc();
					return pics;
				}

				rc = DSpxfer( appid, srcds, TwDG.Control, TwDAT.PendingXfers, TwMSG.EndXfer, pxfr );
				if( rc != TwRC.Success )
				{
					CloseSrc();
					return pics;
				}

				pics.Add( hbitmap );
			}
			while( pxfr.Count != 0 );

			rc = DSpxfer( appid, srcds, TwDG.Control, TwDAT.PendingXfers, TwMSG.Reset, pxfr );
			return pics;
		}

		/// <summary>
		/// PassMessage
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
		public TwainCommand PassMessage( ref Message m )
		{
			if( srcds.Id == IntPtr.Zero )
				return TwainCommand.Not;

			int pos = GetMessagePos();

			winmsg.hwnd = m.HWnd;
			winmsg.message = m.Msg;
			winmsg.wParam = m.WParam;
			winmsg.lParam = m.LParam;
			winmsg.time = GetMessageTime();
			winmsg.x = (short)pos;
			winmsg.y = (short)( pos >> 16 );

			Marshal.StructureToPtr( winmsg, evtmsg.EventPtr, false );
			evtmsg.Message = 0;
			TwRC rc = DSevent( appid, srcds, TwDG.Control, TwDAT.Event, TwMSG.ProcessEvent, ref evtmsg );
			if( rc == TwRC.NotDSEvent )
				return TwainCommand.Not;
			if( evtmsg.Message == (short)TwMSG.XFerReady )
				return TwainCommand.TransferReady;
			if( evtmsg.Message == (short)TwMSG.CloseDSReq )
				return TwainCommand.CloseRequest;
			if( evtmsg.Message == (short)TwMSG.CloseDSOK )
				return TwainCommand.CloseOk;
			if( evtmsg.Message == (short)TwMSG.DeviceEvent )
				return TwainCommand.DeviceEvent;

			return TwainCommand.Null;
		}

		/// <summary>
		/// CloseSrc
		/// </summary>
		public void CloseSrc()
		{
			TwRC rc;
			if( srcds.Id != IntPtr.Zero )
			{
				TwUserInterface guif = new TwUserInterface();
				rc = DSuserif( appid, srcds, TwDG.Control, TwDAT.UserInterface, TwMSG.DisableDS, guif );
				rc = DSMident( appid, IntPtr.Zero, TwDG.Control, TwDAT.Identity, TwMSG.CloseDS, srcds );
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public void Finish()
		{
			TwRC rc;
			CloseSrc();
			if( appid.Id != IntPtr.Zero )
				rc = DSMparent( appid, IntPtr.Zero, TwDG.Control, TwDAT.Parent, TwMSG.CloseDSM, ref hwnd );
			appid.Id = IntPtr.Zero;
		}

		/// <summary>
		/// ScreenBitDepth
		/// </summary>
		public static int ScreenBitDepth
		{
			get
			{
				IntPtr screenDC = CreateDC( "DISPLAY", null, null, IntPtr.Zero );
				int bitDepth = GetDeviceCaps( screenDC, 12 );
				bitDepth *= GetDeviceCaps( screenDC, 14 );
				DeleteDC( screenDC );
				return bitDepth;
			}
		}

		[StructLayout( LayoutKind.Sequential, Pack = 2 )]
		internal class BITMAPINFOHEADER
		{
			public int biSize;
			public int biWidth;
			public int biHeight;
			public short biPlanes;
			public short biBitCount;
			public int biCompression;
			public int biSizeImage;
			public int biXPelsPerMeter;
			public int biYPelsPerMeter;
			public int biClrUsed;
			public int biClrImportant;
		}

		/// <summary>
		/// GetPixelInfo
		/// </summary>
		/// <param name="bmpptr"></param>
		/// <returns></returns>
		public IntPtr GetPixelInfo( IntPtr bmpptr )
		{
			BITMAPINFOHEADER bmi = new BITMAPINFOHEADER();
			Marshal.PtrToStructure( bmpptr, bmi );

			if( bmi.biSizeImage == 0 )
				bmi.biSizeImage = ( ( ( ( bmi.biWidth * bmi.biBitCount ) + 31 ) & ~31 ) >> 3 ) * bmi.biHeight;

			int p = bmi.biClrUsed;
			if( ( p == 0 ) && ( bmi.biBitCount <= 8 ) )
				p = 1 << bmi.biBitCount;
			p = ( p * 4 ) + bmi.biSize + (int)bmpptr;

			return (IntPtr)p;
		}

		[DllImport( "gdiplus.dll", ExactSpelling = true )]
		internal static extern int GdipCreateBitmapFromGdiDib( IntPtr bminfo, IntPtr pixdat, ref IntPtr image );
		[DllImport( "gdiplus.dll", ExactSpelling = true, CharSet = CharSet.Unicode )]
		internal static extern int GdipSaveImageToFile( IntPtr image, string filename, [In] ref Guid clsid, IntPtr encparams );
		[DllImport( "gdiplus.dll", ExactSpelling = true )]
		internal static extern int GdipDisposeImage( IntPtr image );

		private static ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

		/// <summary>
		/// GetCodecClsid
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="clsid"></param>
		/// <returns></returns>
		private static bool GetCodecClsid( string filename, out Guid clsid )
		{
			clsid = Guid.Empty;
			string ext = Path.GetExtension( filename );
			if( ext == null )
				return false;
			ext = "*" + ext.ToUpper();
			foreach( ImageCodecInfo codec in codecs )
			{
				if( codec.FilenameExtension.IndexOf( ext ) >= 0 )
				{
					clsid = codec.Clsid;
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// SaveDIBAs
		/// </summary>
		/// <param name="picname"></param>
		/// <param name="bminfo"></param>
		/// <param name="pixdat"></param>
		/// <returns></returns>
		public static bool SaveDIBAs( string picname, IntPtr bminfo, IntPtr pixdat )
		{
			SaveFileDialog sd = new SaveFileDialog();
			sd.InitialDirectory = "C:";
			sd.FileName = picname;
			sd.Title = "Save bitmap as...";
			sd.Filter = "Bitmap file (*.bmp)|*.bmp|JPEG file (*.jpg)|*.jpg|PNG file (*.png)|*.png|All files (*.*)|*.*";
			sd.FilterIndex = 1;
			if( sd.ShowDialog() != DialogResult.OK )
				return false;

			Guid clsid;
			if( !GetCodecClsid( sd.FileName, out clsid ) )
			{
				MessageBox.Show( "Unknown picture format for extension " + Path.GetExtension( sd.FileName ),
								"Image Codec", MessageBoxButtons.OK, MessageBoxIcon.Information );
				return false;
			}

			IntPtr img = IntPtr.Zero;
			int st = GdipCreateBitmapFromGdiDib( bminfo, pixdat, ref img );
			if( ( st != 0 ) || ( img == IntPtr.Zero ) )
				return false;

			st = GdipSaveImageToFile( img, sd.FileName, ref clsid, IntPtr.Zero );
			SavePathToSettings( sd.FileName );
			GdipDisposeImage( img );
			return st == 0;
		}

		/// <summary>
		/// SavePathToSettings
		/// </summary>
		/// <param name="fPath"></param>
		public static void SavePathToSettings( String fPath )
		{
			String settings = Path.Combine( Environment.CurrentDirectory, "Settings.xml" );
			XmlDocument doc = new XmlDocument();
			doc.Load( settings );

			XmlNode node = doc.SelectSingleNode( "//ImagePath" );
			node.InnerText = fPath;

			doc.Save( settings );
		}
	}
}