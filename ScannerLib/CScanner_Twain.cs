﻿//*********************************************************************************
// Company:         Novartis NITAS
// Copyright:       Copyright © 2013
// Author:          Erik Hesse
// Date:            02/20/2013
//
// Project:         Scanner Library       
// Application:     ScannerLib
// File:            CScanner_Twain.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
//   
//*********************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScannerLib
{
    public class CScanner_Twain : CScanner_Base
    {
        public override event MessageEvent evtMessage;

        // Private member variables
        private frmTwainScanner m_frmScanner;

        /// <summary>
        /// CScanner_Twain constructor
        /// </summary>
        public CScanner_Twain()
        {
            m_frmScanner = new frmTwainScanner();
        }

        /// <summary>
        /// Init
        /// </summary>
        public override void Init()
        {
            try
            {
                m_frmScanner.InitScanner();
            }
            catch( Exception eX )
            {
                throw new ScannerException( this.GetType().Name + ":Init [" + eX.Message + "]", eX );
            }

            if(base.Events)
				evtMessage( this, this.GetType().Name + ":Init" );
        }

        /// <summary>
        /// SetSource
        /// </summary>
        public override void SetSource()
        {
            try
            {
                m_frmScanner.SetSource();
            }
            catch(Exception eX)
            {
                throw new ScannerException( this.GetType().Name + ":SetSource [" + eX.Message + "]", eX );
            }

			if( base.Events )
				evtMessage( this, this.GetType().Name + ":SetSource" );
        }

        /// <summary>
        /// Scan
        /// </summary>
        public override void Scan()
        {
            try
            {
                m_frmScanner.AcquireScan();
            }
            catch( Exception eX )
            {
                throw new ScannerException( this.GetType().Name + ":Scan [" + eX.Message + "]", eX );
            }

			if( base.Events )
				evtMessage( this, this.GetType().Name + ":Scan" );
        }

        /// <summary>
        /// GetImage
        /// </summary>
        /// <returns></returns>
        public override byte[] GetImage()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":GetImage" );

            return m_frmScanner.m_baImage;
        }

        /// <summary>
        /// SaveSettings
        /// </summary>
        public override void SaveSettings()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":SaveSettings" );
        }

        /// <summary>
        /// LoadSettings
        /// </summary>
        public override void LoadSettings()
        {
			if( base.Events )
				evtMessage( this, this.GetType().Name + ":LoadSettings" );
        }
    }
}
