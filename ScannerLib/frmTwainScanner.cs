﻿//*********************************************************************************
// Company:         Novartis NIBRIT/Delta
// Copyright:       Copyright © 2013
// Author:          Erik Hesse
// Date:            04/01/2013
//
//
// Project:         IntelliScan
// Application:     ScannerLib
// File:            frmTwainScanner.cs
//
// Comment:         
//
// Purpose:         
//
//
// History:        
//
// Date:        Ver:    Description
//
//*********************************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using TwainLib;

namespace ScannerLib
{
	public partial class frmTwainScanner : Form, IMessageFilter
	{
		// Private member variables
		private bool m_bMsgFilter;


		// Public member variables
		public Twain tw;
		public byte[] m_baImage;

		/// <summary>
		/// frmTwainScanner constructor
		/// </summary>
		public frmTwainScanner()
		{
			tw = new Twain();
		}

		/// <summary>
		/// InitScanner
		/// </summary>
		public void InitScanner()
		{
            try
            {
                tw.Init( this.Handle );
            }
            catch( Exception eX )
            {
                throw new ScannerException( this.GetType().Name + ":InitScanner [" + eX.Message + "]", eX );
            }
		}

		/// <summary>
		/// SetSource
		/// </summary>
		public void SetSource()
		{
            try
            {
                tw.Select();
            }
            catch( Exception eX )
            {
                throw new ScannerException( this.GetType().Name + ":SetSource [" + eX.Message + "]", eX );
            }
		}

		/// <summary>
		/// AcquireScan
		/// </summary>
		public void AcquireScan()
		{
            try
            {
                if( !m_bMsgFilter )
                {
                    this.Enabled = false;
                    m_bMsgFilter = true;
                    Application.AddMessageFilter( this );
                }

                //tw.SetSettigns();		

                tw.Acquire();

                // wait until processing is finished
                while( this.Enabled == false )
                {
                    Application.DoEvents();
                }
            }
            catch( Exception eX )
            {
                throw new ScannerException( this.GetType().Name + ":AcquireScan [" + eX.Message + "]", eX );
            }
		}

		/// <summary>
		/// PreFilterMessage
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
		bool IMessageFilter.PreFilterMessage( ref Message m )
		{
            try
            {
                TwainCommand cmd = tw.PassMessage( ref m );
                if( cmd == TwainCommand.Not )
                    return false;

                switch( cmd )
                {
                    case TwainCommand.CloseRequest:
                        {
                            EndingScan();
                            tw.CloseSrc();
                            break;
                        }
                    case TwainCommand.CloseOk:
                        {
                            EndingScan();
                            tw.CloseSrc();
                            break;
                        }
                    case TwainCommand.DeviceEvent:
                        {
                            break;
                        }
                    case TwainCommand.TransferReady:
                        {
                            ArrayList pics = tw.TransferPictures();
                            EndingScan();
                            tw.CloseSrc();

                            IntPtr img = (IntPtr)pics[0];
                            IntPtr bmpptr = Twain.GlobalLock( img );
                            IntPtr pixptr = tw.GetPixelInfo( bmpptr );

                            // Convert the image to a bitmap then save
                            IntPtr newImg = IntPtr.Zero;
                            int st = Twain.GdipCreateBitmapFromGdiDib( bmpptr, pixptr, ref newImg );

                            System.Drawing.Bitmap bmp = null;
                            bmp = (System.Drawing.Bitmap)typeof( System.Drawing.Bitmap ).InvokeMember(
                                "FromGDIplus",
                                System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.InvokeMethod,
                                null,
                                null,
                                new object[] { newImg } );
                            //bmp.Save( "C:\\temp\\newimage.bmp" );

                            //// Convert to byte array from the Bitmap
                            //System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
                            //m_baImage = (byte[])converter.ConvertTo( bmp, typeof( byte[] ) );       
                            //System.Drawing.Image iImage = System.Drawing.Image.FromStream( new System.IO.MemoryStream( m_baImage ) );
                            //iImage.Save( "C:\\temp\\newimage1.bmp" );

                            // Convert to JPEG format in a byte array
                            byte[] byteArray = new byte[0];
                            using( System.IO.MemoryStream stream = new System.IO.MemoryStream() )
                            {
                                bmp.Save( stream, System.Drawing.Imaging.ImageFormat.Jpeg );
                                m_baImage = stream.ToArray();
                            }

                            //iImage = System.Drawing.Image.FromStream( new System.IO.MemoryStream( m_baImage ) );
                            //iImage.Save("C:\\temp\\newimage2.jpg" );

                            ////save image to disk
                            //Twain.SaveDIBAs( "", bmpptr, pixptr );

                            // free memory
                            Twain.GlobalFree( img );

                            break;
                        }
                }
            }
            catch( Exception eX )
            {
                throw new ScannerException( this.GetType().Name + ":PreFilterMessage [" + eX.Message + "]", eX );
            }

			return true;
		}

		/// <summary>
		/// EndingScan
		/// </summary>
		private void EndingScan()
		{
            try
            {
                if( m_bMsgFilter )
                {
                    Application.RemoveMessageFilter( this );
                    m_bMsgFilter = false;
                    this.Enabled = true;
                    this.Activate();
                }
            }
            catch( Exception eX )
            {
                throw new ScannerException( this.GetType().Name + ":EndingScan [" + eX.Message + "]", eX );
            }
		}
	}
}
