﻿Module UserInfo

    Public m_sUserID As String
    Public m_sUserPassword As String
    Public m_sUserDA As String
    Public m_sUserAddress As String
    Public m_sUserCountryName As String
    Public m_sUserSite As String
    Public m_sUserSiteCode As String
    Public m_sUserFullName As String
    Public m_sUserName As String

    Public Property UserSiteCode() As String
        Get
            Return m_sUserSiteCode
        End Get
        Set(ByVal value As String)
            m_sUserSiteCode = value
        End Set
    End Property

    Public Property UserSite() As String
        Get
            Return m_sUserSite
        End Get
        Set(ByVal value As String)
            m_sUserSite = value
        End Set
    End Property

    Public Property UserCountryName() As String
        Get
            Return m_sUserCountryName
        End Get
        Set(ByVal value As String)
            m_sUserCountryName = value
        End Set
    End Property

    Public Property UserFullName() As String
        Get
            Return m_sUserFullName
        End Get
        Set(ByVal value As String)
            m_sUserFullName = value
        End Set
    End Property

    Public Property UserID() As String
        Get
            Return m_sUserID
        End Get
        Set(ByVal value As String)
            m_sUserID = value
        End Set
    End Property

    Public Property UserPassword() As String
        Get
            Return m_sUserPassword
        End Get
        Set(ByVal value As String)
            m_sUserPassword = value
        End Set
    End Property

    'Public Property UserDA() As String
    '    Get
    '        Return m_sUserDA
    '    End Get
    '    Set(ByVal value As String)
    '        m_sUserDA = value
    '    End Set
    'End Property

    Public Property UserAddress() As String
        Get
            Return m_sUserAddress
        End Get
        Set(ByVal value As String)
            m_sUserAddress = value
        End Set
    End Property

End Module
